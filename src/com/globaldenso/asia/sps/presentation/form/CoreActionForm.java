/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.struts.action.ActionForm;

import com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class CoreActionForm.
 * 
 * @author CSI
 * @version 1.00
 */
public abstract class CoreActionForm extends ActionForm {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5409049600445571252L;

    /** The method. */
    private String method;
    
    /** The row. */
    private Integer row;
    
    /** The current page. */
    private String currentPage;
    
    /** The total count. */
    private Integer totalCount;

    /** The chk update flag. */
    private String chkUpdateFlag;
    
    /** The creator. */
    private String creator;
    
    /** The updater. */
    private String updater;
    
    /** The target page. */
    private String targetPage;
    
    /** The sort by. */
    private String sortBy;
    
    /** The locale. */
    private Locale locale;
    
    /** The application message. */
    private String applicationMessage;
    
    /** The type of message. */
    private String messageType;
    
    /** The From. */
    private int from;
    
    /** The To. */
    private int to;
    
    /** The Pages. */
    private String pages;
    
    /** The Max. */
    private int max;
    
    /** The Count. */
    private int count;
    
    /** The Page Number. */
    private int pageNo;
    
    /** The First Pages. */
    private String firstPages;
    
    /** The First Count. */
    private int firstCount;
    
    /** The Max Per Pages. */
    private int maxPerPages;
    
    /** The application message list. */
    private List<ApplicationMessageDomain> applicationMessageList;
    
    /** The list of menu that menu that user has permission. */
    private List<SpsMMenuDomain> menuCodelist;
    
    /** The user login information */
    private String userLogin;
    
    /**
     * Instantiates a new core action form.
     */
    public CoreActionForm(){
        super();
        this.applicationMessageList = new ArrayList<ApplicationMessageDomain>();
    }
    
    /**
     * Reset form.
     */
    public void resetForm(){
        totalCount = null;
        currentPage = Constants.PAGE_FIRST;
        targetPage = Constants.PAGE_FIRST;
        applicationMessageList = null;
        sortBy = Constants.EMPTY_STRING;
        max = 0;
        maxPerPages = 0;
    }
    /**
     * Sets the menu code.
     * 
     * @param menuCode the new menu code.
     */
    public void setMenuCode(SpsMMenuDomain menuCode) {
        this.menuCodelist.add(menuCode);
    }
    
    /**
     * Gets the menu code.
     * 
     * @param index the index
     * @return the menu code.
     */
    public SpsMMenuDomain getMenuCode(int index) {
        if (null == menuCodelist) {
            menuCodelist = new ArrayList<SpsMMenuDomain>();
        }
        while (menuCodelist.size() <= index) {
            menuCodelist.add(new SpsMMenuDomain());
        }
        return menuCodelist.get(index);
    }

    /**
     * setMethod is used for getting value of attribute named "method".
     * 
     * @return the method
     */
    public String getMethod() {
        return method;
    }

    /**
     * setMethod is used for setting value of attribute named "method".
     * @param method - target method to handle request in Struts DispacthAction.
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * getTotalCount().
     * 
     * @return total count of result that match with searching criteria.
     */
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     * Sets the total count.
     * 
     * @param totalCount the new total count
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * Gets the row.
     * 
     * @return Maximum number of row that can be displayed in table.
     */
    public Integer getRow() {
        return row;
    }

    /**
     * Sets the row.
     * 
     * @param row the new row
     */
    public void setRow(Integer row) {
        this.row = row;
    }
    
    /**
     * Gets the current page.
     * 
     * @return Current page number that is being displayed data.
     */
    public String getCurrentPage() {
        return currentPage;
    }
    
    /**
     * Set current page that is being displayed on screen.
     * In case there is error occur while paging to next screen,
     * the current page will be forwarded to display user.
     * 
     * @param currentPage the new current page
     */
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * getFirstRow().
     * 
     * @return Index of the first row.
     */
    public Integer getFirstRow() {
        return (Integer.parseInt(targetPage) * row) - row + 1;
    }

    /**
     * getLastRow().
     * 
     * @return Index of last row.
     */
    public Integer getLastRow() {
        return Integer.parseInt(targetPage) * row;
    }

    /**
    * Gets the chk update flag.
    * 
    * @return the chk update flag
    */
    public String getChkUpdateFlag() {
        return chkUpdateFlag;
    }

    /**
     * Sets the chk update flag.
     * 
     * @param chkUpdateFlag the new chk update flag
     */
    public void setChkUpdateFlag(String chkUpdateFlag) {
        this.chkUpdateFlag = chkUpdateFlag;
    }

    /**
     * Gets the creator.
     * 
     * @return the creator
     */
    public String getCreator() {
        return creator;
    }

    /**
     * Sets the creator.
     * 
     * @param creator the new creator
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * Gets the updater.
     * 
     * @return the updater
     */
    public String getUpdater() {
        return updater;
    }

    /**
     * Sets the updater.
     * 
     * @param updater the new updater
     */
    public void setUpdater(String updater) {
        this.updater = updater;
    }

    /**
     * Gets the target page.
     * 
     * @return Target page when user clicked on page navigator.
     */
    public String getTargetPage() {
        return this.targetPage;
    }

    /**
    * Set target page when user clicked on page navigator.
    * 
    * @param targetPage the new target page
    */
    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }

    /**
    * Gets the sort by.
    * 
    * @return Sort by when user clicked on page navigator.
    */
    public String getSortBy() {
        return sortBy;
    }

    /**
    * Set sort by in page when user clicked on header column.
    * 
    * @param sortBy the new sort by
    */
    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    /**
    * Gets the locale.
    * 
    * @return locale.
    */
    public Locale getLocale() {
        return locale;
    }
 
    /**
    * Set default locale.
    * 
    * @param locale the new locale
    */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }
    
    /**
     * <p>Getter method for application message.</p>
     *
     * @return the application message
     */
    public String getApplicationMessage() {
        return applicationMessage;
    }

    /**
     * <p>Setter method for application message.</p>
     *
     * @param applicationMessage Set for application message
     */
    public void setApplicationMessage(String applicationMessage) {
        this.applicationMessage = applicationMessage;
    }

    /**
     * <p>Getter method for type of message.</p>
     *
     * @return the type of message
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * <p>Setter method for type of message.</p>
     *
     * @param messageType Set for type of message
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }
    
    /**
     * <p>Getter method for From.</p>
     *
     * @return the From
     */
    public int getFrom() {
        return from;
    }

    /**
     * <p>Setter method for From.</p>
     *
     * @param from Set for From
     */
    public void setFrom(int from) {
        this.from = from;
    }

    /**
     * <p>Getter method for To.</p>
     *
     * @return the To
     */
    public int getTo() {
        return to;
    }

    /**
     * <p>Setter method for To.</p>
     *
     * @param to Set for To
     */
    public void setTo(int to) {
        this.to = to;
    }

    /**
     * <p>Getter method for Pages.</p>
     *
     * @return the Pages
     */
    public String getPages() {
        return pages;
    }

    /**
     * <p>Setter method for Pages.</p>
     *
     * @param pages Set for Pages
     */
    public void setPages(String pages) {
        this.pages = pages;
    }

    /**
     * <p>Getter method for Max.</p>
     *
     * @return the Max
     */
    public int getMax() {
        return max;
    }

    /**
     * <p>Setter method for Max.</p>
     *
     * @param max Set for Max
     */
    public void setMax(int max) {
        this.max = max;
    }

    /**
     * <p>Getter method for Count.</p>
     *
     * @return the Count
     */
    public int getCount() {
        return count;
    }

    /**
     * <p>Setter method for Count.</p>
     *
     * @param count Set for Count
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * <p>Getter method for Page Number.</p>
     *
     * @return the Page Number
     */
    public int getPageNo() {
        return pageNo;
    }

    /**
     * <p>Setter method for Page Number.</p>
     *
     * @param pageNo Set for Page Number
     */
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    /**
     * <p>Getter method for First Pages.</p>
     *
     * @return the First Pages
     */
    public String getFirstPages() {
        return firstPages;
    }

    /**
     * <p>Setter method for First Pages.</p>
     *
     * @param firstPages Set for First Pages
     */
    public void setFirstPages(String firstPages) {
        this.firstPages = firstPages;
    }

    /**
     * <p>Getter method for First Count.</p>
     *
     * @return the First Count
     */
    public int getFirstCount() {
        return firstCount;
    }

    /**
     * <p>Setter method for First Count.</p>
     *
     * @param firstCount Set for First Count
     */
    public void setFirstCount(int firstCount) {
        this.firstCount = firstCount;
    }

    /**
     * <p>Getter method for Max Per Pages.</p>
     *
     * @return the Max Per Pages
     */
    public int getMaxPerPages() {
        return maxPerPages;
    }

    /**
     * <p>Setter method for Max Per Pages.</p>
     *
     * @param maxPerPages Set for Max Per Pages
     */
    public void setMaxPerPages(int maxPerPages) {
        this.maxPerPages = maxPerPages;
    }

    /**
     * <p>Getter method for application message list.</p>
     *
     * @return the application message list
     */
    public List<ApplicationMessageDomain> getApplicationMessageList() {
        return applicationMessageList;
    }

    /**
     * <p>Setter method for application message list.</p>
     *
     * @param applicationMessageList Set for application message list
     */
    public void setApplicationMessageList(List<ApplicationMessageDomain> applicationMessageList) {
        this.applicationMessageList = applicationMessageList;
    }

    /**
     * Get method for menu domain List.
     * @return the spsMMenuDomainlist
     */
    public List<SpsMMenuDomain> getMenuCodelist() {
        return menuCodelist;
    }
    /**
     * Set method for menu domain List.
     * @param menuCodelist the menu domain List.
     */
    public void setMenuCodelist(List<SpsMMenuDomain> menuCodelist) {
        this.menuCodelist = menuCodelist;
    }

    /**
     * Get method for user login information.
     * @return the userLogin
     */
    public String getUserLogin() {
        return userLogin;
    }

    /**
     * Set method for user login information.
     * @param userLogin the user login information.
     */
    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }
    
    /**
     * Sets the applicationMessage.
     * 
     * @param applicationMessage the new applicationMessage.
     */
    public void setApplicationMessageItem(ApplicationMessageDomain applicationMessage) {
        this.applicationMessageList.add(applicationMessage);
    }
    
    /**
     * Gets the applicationMessage.
     * 
     * @param index the index
     * @return the Plant Supplier.
     */
    public ApplicationMessageDomain getApplicationMessageItem(int index) {
        if (applicationMessageList == null) {
            applicationMessageList = new ArrayList<ApplicationMessageDomain>();
        }
        while (applicationMessageList.size() <= index) {
            applicationMessageList.add(new ApplicationMessageDomain());
        }
        return applicationMessageList.get(index);
    }

}
