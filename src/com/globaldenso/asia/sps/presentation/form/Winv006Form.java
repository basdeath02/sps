/*
 * ModifyDate Developmentcompany Describe 
 * 2014/06/11 CSI Karnrawee      Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.upload.FormFile;

import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WINV006Form.
 * @author CSI
 */
public class Winv006Form extends CoreActionForm {
    
    /** Generated serial version UID. */
    private static final long serialVersionUID = 4748376793923671870L;

    /** The file data. */
    private FormFile fileData;
    
    /** The comment. */
    private String comment;
    
    /** The supplier code. */
    private String sCd;
    
    /** The vendor code. */
    private String vendorCd;

    /** The supplier plant code. */
    private String sPcd;
    
    /** The supplierCodeList code. */
    private List<MiscellaneousDomain> sCdList;

    /** The supplierPlantCodeList code. */
    private List<PlantSupplierDomain> sPcdList;
    
    /** The supplier Code Criteria. */
    private String sCdCriteria;
    
    /** The file name. */
    private String fileName;
    
    /** The DENSO code. */
    private String dCd;
    
    /** The DENSO Plant code. */
    private String dPcd;
    
    /** The company Denso List. */
    private List<MiscellaneousDomain> companyDensoList;
    
    /** The Denso Plant Code List. */
    private List<PlantDensoDomain> dPcdList;
    
    /** The Message when cannot click reset. */
    private String cannotResetMessage;
    
    /**
     * <p>Getter method for supplierCodeList.</p>
     *
     * @return the supplierCodeList
     */
    
    /** The default constructor. */
    public Winv006Form() {
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.presentation.form.CoreActionForm#resetForm()
     */
    public void resetForm() {
        fileData = null;
        fileName = Constants.EMPTY_STRING;
        comment = Constants.EMPTY_STRING;
        vendorCd = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        sCdList = null;
        sPcdList = null;
        sCdCriteria = Constants.EMPTY_STRING;
        companyDensoList = null;
        dPcd = null;
        dCd = null;
    }
    /**
     * <p>Getter method for file data.</p>
     *
     * @return the file data
     */
    public FormFile getFileData() {
        return fileData;
    }

    /**
     * <p>Setter method for file data.</p>
     *
     * @param fileData Set for file data
     */
    public void setFileData(FormFile fileData) {
        this.fileData = fileData;
    }

    /**
     * <p>Getter method for comment.</p>
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * <p>Setter method for comment.</p>
     *
     * @param comment Set for comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }
    
    /**
     * <p>Getter method for supplier code.</p>
     *
     * @return the supplier code
     */
    public String getSCd() {
        return sCd;
    }
    
    /**
     * <p>Setter method for supplier code.</p>
     *
     * @param sCd Set for supplier code
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * <p>Getter method for supplier plan code.</p>
     *
     * @return the supplier plan code
     */
    public String getSPcd() {
        return sPcd;
    }
    
    /**
     * <p>Setter method for supplier plan code.</p>
     *
     * @param sPcd Set for supplier plan code
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }


    /**
     * <p>Getter method for supplierPlantCodeList.</p>
     *
     * @return the supplierPlantCodeList
     */
    public List<MiscellaneousDomain> getSCdList() {
        return sCdList;
    }

    /**
     * <p>Setter method for supplierCodeList.</p>
     *
     * @param sCdList Set for supplierCodeList
     */
    public void setSCdList(List<MiscellaneousDomain> sCdList) {
        this.sCdList = sCdList;
    }

    /**
     * <p>
     * Getter method for supplierCodeCriteria.
     * </p>
     * 
     * @return the supplierCodeCriteria
     */
    public String getSCdCriteria() {
        return sCdCriteria;
    }

    /**
     * <p>
     * Setter method for supplierCodeCriteria.
     * </p>
     * 
     * @param sCdCriteria Set for supplierCodeCriteria
     */
    public void setSCdCriteria(String sCdCriteria) {
        this.sCdCriteria = sCdCriteria;
    }
    /**
     * Sets the company supplier.
     * 
     * @param companySupplier the new company supplier.
     */
    public void setCompanySupplierItem(MiscellaneousDomain companySupplier) {
        this.sCdList.add(companySupplier);
    }
    
    /**
     * Gets the company supplier.
     * 
     * @param index the index
     * @return the company supplier.
     */
    public MiscellaneousDomain getCompanySupplierItem(int index) {
        if (sCdList == null) {
            sCdList = new ArrayList<MiscellaneousDomain>();
        }
        while (sCdList.size() <= index) {
            sCdList.add(new MiscellaneousDomain());
        }
        return sCdList.get(index);
    }
    
    /**
     * <p>Getter method for fileName.</p>
     *
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * <p>Setter method for fileName.</p>
     *
     * @param fileName Set for fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for companyDensoList.</p>
     *
     * @return the companyDensoList
     */
    public List<MiscellaneousDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * <p>Setter method for companyDensoList.</p>
     *
     * @param companyDensoList Set for companyDensoList
     */
    public void setCompanyDensoList(List<MiscellaneousDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }

    /**
     * <p>Getter method for dPcdList.</p>
     *
     * @return the dPcdList
     */
    public List<PlantDensoDomain> getDPcdList() {
        return dPcdList;
    }

    /**
     * <p>Setter method for dPcdList.</p>
     *
     * @param dPcdList Set for dPcdList
     */
    public void setDPcdList(List<PlantDensoDomain> dPcdList) {
        this.dPcdList = dPcdList;
    }

    /**
     * <p>Getter method for sPcdList.</p>
     *
     * @return the sPcdList
     */
    public List<PlantSupplierDomain> getSPcdList() {
        return sPcdList;
    }

    /**
     * <p>Setter method for sPcdList.</p>
     *
     * @param sPcdList Set for sPcdList
     */
    public void setSPcdList(List<PlantSupplierDomain> sPcdList) {
        this.sPcdList = sPcdList;
    }

    /**
     * <p>Getter method for cannotResetMessage.</p>
     *
     * @return the cannotResetMessage
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }

    /**
     * <p>Setter method for cannotResetMessage.</p>
     *
     * @param cannotResetMessage Set for cannotResetMessage
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }

}
