/*
 * ModifyDate Development company   Describe 
 * 2014/08/20 CSI Parichat          Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.AsnDetailInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class Wshp008Form.
 * @author CSI
 */
public class Wshp008Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7111134570980508668L;
    
    /** The mode. */
    private String mode;
    
    /** The asnNo. */
    private String asnNo;
    
    /** The asn status. */
    private String asnStatus;
    
    /** The actual etd from. */
    private String actualEtdFrom;
    
    /** The actual etd to. */
    private String actualEtdTo;
    
    /** The vendor code. */
    private String vendorCd;
   
    /** The supplier plant code. */
    private String sPcd;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plant code. */
    private String dPcd;
    
    /** The plan eta from. */
    private String planEtaFrom;
    
    /** The plan eta to. */
    private String planEtaTo;
    
    /** The sps do no. */
    private String spsDoNo;
    
    /** The trans. */
    private String trans;
    
    /** The pdf file id. */
    private String pdfFileId;
    
    /** The selected dCd. */
    private String selectedDCd;
    
    /** The selected sps do no. */
    private String selectedSpsDoNo;
    
    /** The selected do id. */
    private String selectedDoId;
    
    /** The selected rev. */
    private String selectedRev;
    
    /** The selected route. */
    private String selectedRoute;
    
    /** The selected del. */
    private String selectedDel;
    
    /** The selected vencor cd. */
    private String selectedVendorCd;
    
    /** The selected sPcd. */
    private String selectedSPcd;
    
    /** The selected dPcd. */
    private String selectedDPcd;
    
    /** The selected asnNo. */
    private String selectedAsnNo;
    
    /** The selected actualEtd. */
    private String selectedActualEtd;
    
    /** The error message for operating reset button. */
    private String cannotResetMessage;
    
    /** The error message for operating download button. */
    private String cannotDownloadMessage;
    
    /** The asn status list. */
    private List<MiscellaneousDomain> asnStatusList;
    
    /** The trans list. */
    private List<MiscellaneousDomain> transList;
    
    /** The List of Supplier Company. */
    private List<CompanySupplierDomain> companySupplierList;

    /** The List of DENSO Company. */
    private List<CompanyDensoDomain> companyDensoList;

    /** The List of DENSO Plant. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The List of Supplier Plant. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The List of ASN Detail Information. */
    private List<AsnDetailInformationReturnDomain> asnDetailInformationList;
    
    /** The default Constructor */
    public Wshp008Form() {
        companySupplierList = new ArrayList<CompanySupplierDomain>();
        companyDensoList = new ArrayList<CompanyDensoDomain>();
        plantDensoList = new ArrayList<PlantDensoDomain>();
        plantSupplierList = new ArrayList<PlantSupplierDomain>();
        asnDetailInformationList = new ArrayList<AsnDetailInformationReturnDomain>();
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        mode = Constants.EMPTY_STRING;
        actualEtdFrom = Constants.EMPTY_STRING;
        actualEtdTo = Constants.EMPTY_STRING;
        planEtaFrom = Constants.EMPTY_STRING;
        planEtaTo = Constants.EMPTY_STRING;
        vendorCd = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        asnStatus = Constants.EMPTY_STRING;
        trans = Constants.EMPTY_STRING;
        asnNo = Constants.EMPTY_STRING;
        spsDoNo = Constants.EMPTY_STRING;
        supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        densoAuthenList = new ArrayList<SpsMPlantDensoDomain>(); 
        asnStatusList = new ArrayList<MiscellaneousDomain>();
        transList = new ArrayList<MiscellaneousDomain>();
    }
    
    /**
     * <p>Setter method for asn detail information.</p>
     * 
     * @param items the new asn detail information return domain
     */
    public void setAsnDetailInformation(AsnDetailInformationReturnDomain items) {
        this.asnDetailInformationList.add(items);
    }
    
    /**
     * Gets the asn detail information.
     * 
     * @param index the index
     * @return the asn detail information return domain
     */
    public AsnDetailInformationReturnDomain getAsnDetailInformation(int index){
        if (asnDetailInformationList == null) {
            asnDetailInformationList = new ArrayList<AsnDetailInformationReturnDomain>();
        }
        while (asnDetailInformationList.size() <= index) {
            asnDetailInformationList.add(new AsnDetailInformationReturnDomain());
        }
        return asnDetailInformationList.get(index);
    }
    
    /**
     * <p>Setter method for company supplier item.</p>
     * 
     * @param items the new company supplier domain
     */
    public void setCompanySupplierItem(CompanySupplierDomain items) {
        this.companySupplierList.add(items);
    }
    
    /**
     * Gets the company supplier item.
     * 
     * @param index the index
     * @return the company supplier domain.
     */
    public CompanySupplierDomain getCompanySupplierItem(int index){
        if(companySupplierList == null){
            companySupplierList = new ArrayList<CompanySupplierDomain>();
        }
        while(companySupplierList.size() <= index){
            companySupplierList.add(new CompanySupplierDomain());
        }
        return companySupplierList.get(index);
    }
    
    /**
     * <p>Setter method for denso company item.</p>
     * 
     * @param items the new company denso domain
     */
    public void setCompanyDensoItem(CompanyDensoDomain items) {
        this.companyDensoList.add(items);
    }
    
    /**
     * Gets the denso company item.
     * 
     * @param index the index
     * @return the company denso domain
     */
    public CompanyDensoDomain getCompanyDensoItem(int index){
        if(companyDensoList == null){
            companyDensoList = new ArrayList<CompanyDensoDomain>();
        }
        while(companyDensoList.size() <= index){
            companyDensoList.add(new CompanyDensoDomain());
        }
        return companyDensoList.get(index);
    }
    
    /**
     * <p>Setter method for asn status item.</p>
     * 
     * @param items the new misc domain
     */
    public void setAsnStatusItem(MiscellaneousDomain items) {
        this.asnStatusList.add(items);
    }
    
    /**
     * Gets the asn status item.
     * 
     * @param index the index
     * @return the misc domain result
     */
    public MiscellaneousDomain getAsnStatusItem(int index){
        if(asnStatusList == null){
            asnStatusList = new ArrayList<MiscellaneousDomain>();
        }
        while(asnStatusList.size() <= index){
            asnStatusList.add(new MiscellaneousDomain());
        }
        return asnStatusList.get(index);
    }
    
    /**
     * <p>Setter method for trans item.</p>
     * 
     * @param items the new misc domain
     */
    public void setTransItem(MiscellaneousDomain items) {
        this.transList.add(items);
    }
    
    /**
     * Gets the trans item.
     * 
     * @param index the index
     * @return the misc domain result
     */
    public MiscellaneousDomain getTransItem(int index){
        if(transList == null){
            transList = new ArrayList<MiscellaneousDomain>();
        }
        while(transList.size() <= index){
            transList.add(new MiscellaneousDomain());
        }
        return transList.get(index);
    }
    
    /**
     * Sets the Supplier Authen.
     * 
     * @param supplierAuthen the new Supplier Authen.
     */
    public void setSupplierAuthen(SpsMPlantSupplierDomain supplierAuthen) {
        this.supplierAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new Supplier Authen.
     * 
     * @param index the index
     * @return the Supplier Authen.
     */
    public SpsMPlantSupplierDomain getSupplierAuthen(int index) {
        if (supplierAuthenList == null) {
            supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        }
        while (supplierAuthenList.size() <= index) {
            supplierAuthenList.add(new SpsMPlantSupplierDomain());
        }
        return supplierAuthenList.get(index);
    }
    
    /**
     * Sets the DENSO Authen.
     * 
     * @param supplierAuthen the new DENSO Authen.
     */
    public void setDensoAuthen(SpsMPlantDensoDomain supplierAuthen) {
        this.densoAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new DENSO Authen.
     * 
     * @param index the index
     * @return the DENSO Authen.
     */
    public SpsMPlantDensoDomain getDensoAuthen(int index) {
        if (densoAuthenList == null) {
            densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        }
        while (densoAuthenList.size() <= index) {
            densoAuthenList.add(new SpsMPlantDensoDomain());
        }
        return densoAuthenList.get(index);
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Gets the vendor code
     * 
     * @return the vendor code
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * Sets the vendor code
     * 
     * @param vendorCd the vendor code
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * Gets the supplier plant code.
     * 
     * @return the supplier plant code
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Sets the supplier plant.
     * 
     * @param sPcd the supplier plant
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Gets the denso code.
     * 
     * @return the denso code
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the denso code.
     * 
     * @param dCd the denso code
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the denso plant code.
     * 
     * @return the denso plant code
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Sets the denso plant code.
     * 
     * @param dPcd the denso plant code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Gets the asn no.
     * 
     * @return the asn no
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Sets the asn no.
     * 
     * @param asnNo the asn no
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * Gets the asn status.
     * 
     * @return the asn status
     */
    public String getAsnStatus() {
        return asnStatus;
    }

    /**
     * Sets the asn status.
     * 
     * @param asnStatus the asn status
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }
    
    /**
     * Gets the actual etd from.
     * 
     * @return the actual etd from
     */
    public String getActualEtdFrom() {
        return actualEtdFrom;
    }

    /**
     * Sets the actual etd from.
     * 
     * @param actualEtdFrom the actual etd from
     */
    public void setActualEtdFrom(String actualEtdFrom) {
        this.actualEtdFrom = actualEtdFrom;
    }

    /**
     * Gets the actual etd to.
     * 
     * @return the actual etd to
     */
    public String getActualEtdTo() {
        return actualEtdTo;
    }

    /**
     * Sets the actual etd to.
     * 
     * @param actualEtdTo the actual etd to
     */
    public void setActualEtdTo(String actualEtdTo) {
        this.actualEtdTo = actualEtdTo;
    }

    /**
     * Gets the plan eta from.
     * 
     * @return the plan eta from
     */
    public String getPlanEtaFrom() {
        return planEtaFrom;
    }

    /**
     * Sets the plan eta from.
     * 
     * @param planEtaFrom the plan eta from
     */
    public void setPlanEtaFrom(String planEtaFrom) {
        this.planEtaFrom = planEtaFrom;
    }

    /**
     * Gets the plan eta to.
     * 
     * @return the plan eta to
     */
    public String getPlanEtaTo() {
        return planEtaTo;
    }

    /**
     * Sets the plan eta to.
     * 
     * @param planEtaTo the plan eta to
     */
    public void setPlanEtaTo(String planEtaTo) {
        this.planEtaTo = planEtaTo;
    }

    /**
     * Gets the sps do no.
     * 
     * @return the sps do no
     */
    public String getSpsDoNo() {
        return spsDoNo;
    }

    /**
     * Sets the sps do no.
     * 
     * @param spsDoNo the sps do no
     */
    public void setSpsDoNo(String spsDoNo) {
        this.spsDoNo = spsDoNo;
    }

    /**
     * Gets the trans.
     * 
     * @return the trans
     */
    public String getTrans() {
        return trans;
    }

    /**
     * Sets the trans.
     * 
     * @param trans the trans
     */
    public void setTrans(String trans) {
        this.trans = trans;
    }
    
    /**
     * Gets the pdf file id.
     * 
     * @return the pdf file id
     */
    public String getPdfFileId() {
        return pdfFileId;
    }

    /**
     * Sets the pdf file id.
     * 
     * @param pdfFileId the pdf file id
     */
    public void setPdfFileId(String pdfFileId) {
        this.pdfFileId = pdfFileId;
    }

    /**
     * Gets the selected dCd.
     * 
     * @return the selected dCd
     */
    public String getSelectedDCd() {
        return selectedDCd;
    }

    /**
     * Sets the selected dCd.
     * 
     * @param selectedDCd the selected dCd
     */
    public void setSelectedDCd(String selectedDCd) {
        this.selectedDCd = selectedDCd;
    }

    /**
    * Gets the selected do id.
    * 
    * @return the selected do id
    */
    public String getSelectedDoId() {
        return selectedDoId;
    }

   /**
    * Sets the selected do id.
    * 
    * @param selectedDoId the selected do id
    */
    public void setSelectedDoId(String selectedDoId) {
        this.selectedDoId = selectedDoId;
    }
    
    /**
     * Gets the selected rev.
     * 
     * @return the selected rev
     */
    public String getSelectedRev() {
        return selectedRev;
    }

    /**
     * Sets the selected rev.
     * 
     * @param selectedRev the selected rev
     */
    public void setSelectedRev(String selectedRev) {
        this.selectedRev = selectedRev;
    }

    /**
     * Gets the selected route.
     * 
     * @return the selected route
     */
    public String getSelectedRoute() {
        return selectedRoute;
    }

    /**
     * Sets the selected route.
     * 
     * @param selectedRoute the selected route
     */
    public void setSelectedRoute(String selectedRoute) {
        this.selectedRoute = selectedRoute;
    }

    /**
     * Gets the selected del.
     * 
     * @return the selected del
     */
    public String getSelectedDel() {
        return selectedDel;
    }

    /**
     * Sets the selected del.
     * 
     * @param selectedDel the selected del
     */
    public void setSelectedDel(String selectedDel) {
        this.selectedDel = selectedDel;
    }
    
    /**
     * Gets the selected vendor cd.
     * 
     * @return the selected vendor cd
     */
    public String getSelectedVendorCd() {
        return selectedVendorCd;
    }

    /**
     * Sets the selected vendor cd.
     * 
     * @param selectedVendorCd the selected vendor cd
     */
    public void setSelectedVendorCd(String selectedVendorCd) {
        this.selectedVendorCd = selectedVendorCd;
    }

    /**
     * Gets the selected sPcd.
     * 
     * @return the selected sPcd
     */
    public String getSelectedSPcd() {
        return selectedSPcd;
    }

    /**
     * Sets the selected sPcd.
     * 
     * @param selectedSPcd the selected sPcd
     */
    public void setSelectedSPcd(String selectedSPcd) {
        this.selectedSPcd = selectedSPcd;
    }

    /**
     * Gets the selected dPcd.
     * 
     * @return the selected dPcd
     */
    public String getSelectedDPcd() {
        return selectedDPcd;
    }

    /**
     * Sets the selected dPcd.
     * 
     * @param selectedDPcd the selected dPcd
     */
    public void setSelectedDPcd(String selectedDPcd) {
        this.selectedDPcd = selectedDPcd;
    }

    /**
     * Gets the selected asnNo.
     * 
     * @return the selected asnNo
     */
    public String getSelectedAsnNo() {
        return selectedAsnNo;
    }

    /**
     * Sets the selected asnNo.
     * 
     * @param selectedAsnNo the selected asnNo
     */
    public void setSelectedAsnNo(String selectedAsnNo) {
        this.selectedAsnNo = selectedAsnNo;
    }

    /**
     * Gets the selected actual etd.
     * 
     * @return the selected actual etd
     */
    public String getSelectedActualEtd() {
        return selectedActualEtd;
    }

    /**
     * Sets the selected actual etd.
     * 
     * @param selectedActualEtd the selected actual etd
     */
    public void setSelectedActualEtd(String selectedActualEtd) {
        this.selectedActualEtd = selectedActualEtd;
    }

    /**
    * Gets the selected sps do no.
    * 
    * @return the selected sps do no
    */
    public String getSelectedSpsDoNo() {
        return selectedSpsDoNo;
    }

   /**
    * Sets the selected sps do no.
    * 
    * @param selectedSpsDoNo the selected sps do no
    */
    public void setSelectedSpsDoNo(String selectedSpsDoNo) {
        this.selectedSpsDoNo = selectedSpsDoNo;
    }
    
    /**
     * Gets the cannot reset message.
     * 
     * @return the cannot reset message
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }
    
    /**
     * Sets the cannot reset message.
     * 
     * @param cannotResetMessage the cannot reset message
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }
    
    /**
     * Gets the cannot download message.
     * 
     * @return the cannot download message
     */
    public String getCannotDownloadMessage() {
        return cannotDownloadMessage;
    }

    /**
     * Sets the cannot download message.
     * 
     * @param cannotDownloadMessage the cannot download message
     */
    public void setCannotDownloadMessage(String cannotDownloadMessage) {
        this.cannotDownloadMessage = cannotDownloadMessage;
    }

    /**
     * Gets the asn status list.
     * 
     * @return the asn status list
     */
    public List<MiscellaneousDomain> getAsnStatusList() {
        return asnStatusList;
    }

    /**
     * Sets the asn status list.
     * 
     * @param asnStatusList the asn status list
     */
    public void setAsnStatusList(List<MiscellaneousDomain> asnStatusList) {
        this.asnStatusList = asnStatusList;
    }

    /**
     * Gets the trans list.
     * 
     * @return the trans list
     */
    public List<MiscellaneousDomain> getTransList() {
        return transList;
    }

    /**
     * Sets the trans list.
     * 
     * @param transList the trans list
     */
    public void setTransList(List<MiscellaneousDomain> transList) {
        this.transList = transList;
    }

    /**
     * Gets the company supplier code list.
     * 
     * @return the company supplier code list
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * Sets the company supplier code list.
     * 
     * @param companySupplierList the company supplier code list
     */
    public void setCompanySupplierList(List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * Gets the company denso list.
     * 
     * @return the company denso list
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * Sets the company denso list.
     * 
     * @param companyDensoList the company denso list
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }

    /**
     * Gets the plant denso list.
     * 
     * @return the plant denso list
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * Sets the plant denso list.
     * 
     * @param plantDensoList the plant denso list
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * Gets the plant supplier list.
     * 
     * @return the plant supplier list
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * Sets the plant supplier list.
     * 
     * @param plantSupplierList the plant supplier list
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }
    
    /**
     * Gets the supplierAuthenList.
     * 
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * Sets the supplierAuthenList.
     * 
     * @param supplierAuthenList the supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * Gets the densoAuthenList.
     * 
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * Sets the densoAuthenList.
     * 
     * @param densoAuthenList the densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * Gets the asn detail information list.
     * 
     * @return the asn detail information lis
     */
    public List<AsnDetailInformationReturnDomain> getAsnDetailInformationList() {
        return asnDetailInformationList;
    }

    /**
     * Sets the asn detail information lis.
     * 
     * @param asnDetailInformationList the asn detail information lis
     */
    public void setAsnDetailInformationList(
        List<AsnDetailInformationReturnDomain> asnDetailInformationList) {
        this.asnDetailInformationList = asnDetailInformationList;
    }
}