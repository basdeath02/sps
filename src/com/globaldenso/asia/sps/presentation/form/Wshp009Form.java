/*
 * ModifyDate Development company   Describe 
 * 2014/07/21 CSI Parichat          Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WSHP001Form.
 * @author CSI
 */
public class Wshp009Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4433073585865132423L;
    
    /** The file id. */
    private String fileId;
    
    /** The Actual ETD From. */
    private String actualEtdFrom;
    
    /** The Actual ETD To. */
    private String actualEtdTo;
    
    /** The Actual ETA From. */
    private String actualEtaFrom;
    
    /** The Actual ETA To. */
    private String actualEtaTo;
    
    /** The Invoice Date From. */
    private String invoiceDateFrom;
    
    /** The Invoice Date TO. */
    private String invoiceDateTo;
    
    /** The S.CD. */
    private String sCd;
    
    /** The V.CD. */
    private String vendorCd;
    
    /** The S.P.CD. */
    private String sPcd;
    
    /** The D.CD. */
    private String dCd;
    
    /** The D.P.CD. */
    private String dPcd;
    
    /** The ASN No. */
    private String asnNo;
    
    /** The ASN Status. */
    private String asnStatus;
    
    /** The Invoice No. */
    private String invoiceNo;
    
    /** The Invoice status. */
    private String invoiceStatus;
    
    /** The S.Part No. */
    private String sPn;
    
    /** The D. Part No. */
    private String dPn;
    
    /** The mode. */
    private String mode;
    
    /** The select Invoice ID. */
    private String invoiceIdCriteria;
    
    /** The select Invoice Date. */
    private String selectInvoiceDate;
    
    /** The select CN Date. */
    private String selectCnDate;
    
    /** The select Invoice RCV Status. */
    private String rcvStatusCriteria;
    
    /** The select Invoice No. */
    private String invoiceNoCriteria;
    
    /** The select CN No. */
    private String cnNoCriteria;
    
    /** The select Denso Code. */
    private String dCdCriteria;
    
    /** The select Denso Plant Code. */
    private String dPcdCriteria;
    
    /** The select Supplier Code. */
    private String sCdCriteria;
    
    /** The select Supplier Plant Code. */
    private String sPcdCriteria;
    
    /** The error message for operating reset button. */
    private String cannotResetMessage;
    
    /** The error message for operating download button. */
    private String cannotDownloadMessage;
    
    /** The selected ASN No. */
    private String selectedAsnNo;
    
    /** The selected DENSO Part No. */
    private String selectedDCd;
    
    /** The supplier code list. */
    private List<CompanySupplierDomain> companySupplierList;
    
    /** The supplier plant code list. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The Denso Code list. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The Denso Plant Code list. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The ASN Status list. */
    private List<MiscellaneousDomain> asnStatusList;
    
    /** The Invoice Status list. */
    private List<MiscellaneousDomain> invoiceStatusList;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;
    
    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The ASN Status list. */
    private List<AsnProgressInformationReturnDomain> asnProgressList;
    
    /** The default Constructor */
    public Wshp009Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        actualEtdFrom = Constants.EMPTY_STRING;
        actualEtdTo = Constants.EMPTY_STRING;
        actualEtaFrom = Constants.EMPTY_STRING;
        actualEtaTo = Constants.EMPTY_STRING;
        invoiceDateFrom = Constants.EMPTY_STRING;
        invoiceDateTo = Constants.EMPTY_STRING;
        sCd = Constants.EMPTY_STRING;
        vendorCd = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        asnStatus = Constants.EMPTY_STRING;
        asnNo = Constants.EMPTY_STRING;
        invoiceNo = Constants.EMPTY_STRING;
        sPn = Constants.EMPTY_STRING;
        dPn = Constants.EMPTY_STRING;
    } // end resetForm
    
    /**
     * <p>Setter method for supplier code.</p>
     * 
     * @param items the new misc domain
     */
    public void setCompanySupplier(CompanySupplierDomain items) {
        this.companySupplierList.add(items);
    }
    
    /**
     * Gets the supplier code.
     * 
     * @param index the index
     * @return the misc domain.
     */
    public CompanySupplierDomain getCompanySupplier(int index){
        if(null == companySupplierList){
            companySupplierList = new ArrayList<CompanySupplierDomain>();
        }
        while(companySupplierList.size() <= index){
            companySupplierList.add(new CompanySupplierDomain());
        }
        return companySupplierList.get(index);
    }
    
    /**
     * <p>Setter method for supplier code.</p>
     * 
     * @param items the new misc domain
     */
    public void setPlantSupplier(PlantSupplierDomain items) {
        this.plantSupplierList.add(items);
    }
    
    /**
     * Gets the supplier code.
     * 
     * @param index the index
     * @return the misc domain.
     */
    public PlantSupplierDomain getPlantSupplier(int index){
        if(null == plantSupplierList){
            plantSupplierList = new ArrayList<PlantSupplierDomain>();
        }
        while(plantSupplierList.size() <= index){
            plantSupplierList.add(new PlantSupplierDomain());
        }
        return plantSupplierList.get(index);
    }
    
    /**
     * <p>Setter method for denso code.</p>
     * 
     * @param items the new misc domain
     */
    public void setCompanyDenso(CompanyDensoDomain items) {
        this.companyDensoList.add(items);
    }
    
    /**
     * Gets the denso code.
     * 
     * @param index the index
     * @return the misc domain.
     */
    public CompanyDensoDomain getCompanyDenso(int index){
        if(null == companyDensoList){
            companyDensoList = new ArrayList<CompanyDensoDomain>();
        }
        while(companyDensoList.size() <= index){
            companyDensoList.add(new CompanyDensoDomain());
        }
        return companyDensoList.get(index);
    }
    
    /**
     * <p>Setter method for denso code.</p>
     * 
     * @param items the new misc domain
     */
    public void setPlantDenso(PlantDensoDomain items) {
        this.plantDensoList.add(items);
    }
    
    /**
     * Gets the denso code.
     * 
     * @param index the index
     * @return the misc domain.
     */
    public PlantDensoDomain getPlantDenso(int index){
        if(null == plantDensoList){
            plantDensoList = new ArrayList<PlantDensoDomain>();
        }
        while(plantDensoList.size() <= index){
            plantDensoList.add(new PlantDensoDomain());
        }
        return plantDensoList.get(index);
    }
    
    /**
     * <p>Setter method for asnStatusItem.</p>
     * 
     * @param items the new miscellaneous domain
     */
    public void setAsnStatusItem(MiscellaneousDomain items) {
        this.asnStatusList.add(items);
    }
    
    /**
     * Gets the asnStatusItem.
     * 
     * @param index the index
     * @return the miscellaneous domain
     */
    public MiscellaneousDomain getAsnStatusItem(int index){
        if(null == asnStatusList){
            asnStatusList = new ArrayList<MiscellaneousDomain>();
        }
        while(asnStatusList.size() <= index){
            asnStatusList.add(new MiscellaneousDomain());
        }
        return asnStatusList.get(index);
    }
    
    /**
     * <p>Setter method for invoiceStatusItem.</p>
     * 
     * @param items the new miscellaneous domain
     */
    public void setInvoiceStatusItem(MiscellaneousDomain items){
        this.invoiceStatusList.add(items);
    }
    
    /**
     * Gets the invoiceStatusItem.
     * 
     * @param index the index
     * @return the miscellaneous domain
     */
    public MiscellaneousDomain getInvoiceStatusItem(int index){
        if(null == invoiceStatusList){
            invoiceStatusList = new ArrayList<MiscellaneousDomain>();
        }
        while(invoiceStatusList.size() <= index){
            invoiceStatusList.add(new MiscellaneousDomain());
        }
        return invoiceStatusList.get(index);
    }
    
    /**
     * Sets the Supplier Authen.
     * 
     * @param supplierAuthen the new Supplier Authen.
     */
    public void setSupplierAuthen(SpsMPlantSupplierDomain supplierAuthen) {
        this.supplierAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new Supplier Authen.
     * 
     * @param index the index
     * @return the Supplier Authen.
     */
    public SpsMPlantSupplierDomain getSupplierAuthen(int index) {
        if (supplierAuthenList == null) {
            supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        }
        while (supplierAuthenList.size() <= index) {
            supplierAuthenList.add(new SpsMPlantSupplierDomain());
        }
        return supplierAuthenList.get(index);
    }
    
    /**
     * Sets the DENSO Authen.
     * 
     * @param supplierAuthen the new DENSO Authen.
     */
    public void setDensoAuthen(SpsMPlantDensoDomain supplierAuthen) {
        this.densoAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new DENSO Authen.
     * 
     * @param index the index
     * @return the DENSO Authen.
     */
    public SpsMPlantDensoDomain getDensoAuthen(int index) {
        if (densoAuthenList == null) {
            densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        }
        while (densoAuthenList.size() <= index) {
            densoAuthenList.add(new SpsMPlantDensoDomain());
        }
        return densoAuthenList.get(index);
    }

    /**
     * <p>Getter method for fileId.</p>
     *
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * <p>Setter method for fileId.</p>
     *
     * @param fileId Set for fileId
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    /**
     * <p>Getter method for actualEtdFrom.</p>
     *
     * @return the actualEtdFrom
     */
    public String getActualEtdFrom() {
        return actualEtdFrom;
    }

    /**
     * <p>Setter method for actualEtdFrom.</p>
     *
     * @param actualEtdFrom Set for actualEtdFrom
     */
    public void setActualEtdFrom(String actualEtdFrom) {
        this.actualEtdFrom = actualEtdFrom;
    }

    /**
     * <p>Getter method for actualEtdTo.</p>
     *
     * @return the actualEtdTo
     */
    public String getActualEtdTo() {
        return actualEtdTo;
    }

    /**
     * <p>Setter method for actualEtdTo.</p>
     *
     * @param actualEtdTo Set for actualEtdTo
     */
    public void setActualEtdTo(String actualEtdTo) {
        this.actualEtdTo = actualEtdTo;
    }

    /**
     * <p>Getter method for actualEtaFrom.</p>
     *
     * @return the actualEtaFrom
     */
    public String getActualEtaFrom() {
        return actualEtaFrom;
    }

    /**
     * <p>Setter method for actualEtaFrom.</p>
     *
     * @param actualEtaFrom Set for actualEtaFrom
     */
    public void setActualEtaFrom(String actualEtaFrom) {
        this.actualEtaFrom = actualEtaFrom;
    }

    /**
     * <p>Getter method for actualEtaTo.</p>
     *
     * @return the actualEtaTo
     */
    public String getActualEtaTo() {
        return actualEtaTo;
    }

    /**
     * <p>Setter method for actualEtaTo.</p>
     *
     * @param actualEtaTo Set for actualEtaTo
     */
    public void setActualEtaTo(String actualEtaTo) {
        this.actualEtaTo = actualEtaTo;
    }

    /**
     * <p>Getter method for invoiceDateFrom.</p>
     *
     * @return the invoiceDateFrom
     */
    public String getInvoiceDateFrom() {
        return invoiceDateFrom;
    }

    /**
     * <p>Setter method for invoiceDateFrom.</p>
     *
     * @param invoiceDateFrom Set for invoiceDateFrom
     */
    public void setInvoiceDateFrom(String invoiceDateFrom) {
        this.invoiceDateFrom = invoiceDateFrom;
    }

    /**
     * <p>Getter method for invoiceDateTo.</p>
     *
     * @return the invoiceDateTo
     */
    public String getInvoiceDateTo() {
        return invoiceDateTo;
    }

    /**
     * <p>Setter method for invoiceDateTo.</p>
     *
     * @param invoiceDateTo Set for invoiceDateTo
     */
    public void setInvoiceDateTo(String invoiceDateTo) {
        this.invoiceDateTo = invoiceDateTo;
    }

    /**
     * <p>Getter method for sCd.</p>
     *
     * @return the sCd
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * <p>Setter method for sCd.</p>
     *
     * @param sCd Set for sCd
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }

    /**
     * <p>Getter method for sPcd.</p>
     *
     * @return the sPcd
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * <p>Setter method for sPcd.</p>
     *
     * @param sPcd Set for sPcd
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    
    /**
     * <p>Getter method for asnNo.</p>
     *
     * @return the asnNo
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * <p>Setter method for asnNo.</p>
     *
     * @param asnNo Set for asnNo
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }

    /**
     * <p>Getter method for asnStatus.</p>
     *
     * @return the asnStatus
     */
    public String getAsnStatus() {
        return asnStatus;
    }

    /**
     * <p>Setter method for asnStatus.</p>
     *
     * @param asnStatus Set for asnStatus
     */
    public void setAsnStatus(String asnStatus) {
        this.asnStatus = asnStatus;
    }

    /**
     * <p>Getter method for invoiceNo.</p>
     *
     * @return the invoiceNo
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }

    /**
     * <p>Setter method for invoiceNo.</p>
     *
     * @param invoiceNo Set for invoiceNo
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    /**
     * <p>Getter method for invoiceStatus.</p>
     *
     * @return the invoiceStatus
     */
    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    /**
     * <p>Setter method for invoiceStatus.</p>
     *
     * @param invoiceStatus Set for invoiceStatus
     */
    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }
    
    /**
     * <p>Getter method for sPn.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>Setter method for sPn.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }
    
    /**
     * <p>Getter method for companySupplierList.</p>
     *
     * @return the companySupplierList
     */
    public List<CompanySupplierDomain> getCompanySupplierList() {
        return companySupplierList;
    }

    /**
     * <p>Setter method for companySupplierList.</p>
     *
     * @param companySupplierList Set for companySupplierList
     */
    public void setCompanySupplierList(
        List<CompanySupplierDomain> companySupplierList) {
        this.companySupplierList = companySupplierList;
    }

    /**
     * <p>Getter method for plantSupplierList.</p>
     *
     * @return the plantSupplierList
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }

    /**
     * <p>Setter method for plantSupplierList.</p>
     *
     * @param plantSupplierList Set for plantSupplierList
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }

    /**
     * <p>Getter method for companyDensoList.</p>
     *
     * @return the companyDensoList
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * <p>Setter method for companyDensoList.</p>
     *
     * @param companyDensoList Set for companyDensoList
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }

    /**
     * <p>Getter method for plantDensoList.</p>
     *
     * @return the plantDensoList
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }

    /**
     * <p>Setter method for plantDensoList.</p>
     *
     * @param plantDensoList Set for plantDensoList
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }

    /**
     * <p>Getter method for asnStatusList.</p>
     *
     * @return the asnStatusList
     */
    public List<MiscellaneousDomain> getAsnStatusList() {
        return asnStatusList;
    }

    /**
     * <p>Setter method for asnStatusList.</p>
     *
     * @param asnStatusList Set for asnStatusList
     */
    public void setAsnStatusList(List<MiscellaneousDomain> asnStatusList) {
        this.asnStatusList = asnStatusList;
    }
    
    /**
     * <p>Getter method for invoiceStatusList.</p>
     *
     * @return the invoiceStatusList
     */
    public List<MiscellaneousDomain> getInvoiceStatusList() {
        return invoiceStatusList;
    }

    /**
     * <p>Setter method for invoiceStatusList.</p>
     *
     * @param invoiceStatusList Set for invoiceStatusList
     */
    public void setInvoiceStatusList(List<MiscellaneousDomain> invoiceStatusList) {
        this.invoiceStatusList = invoiceStatusList;
    }

    /**
     * <p>Getter method for asnProgressList.</p>
     *
     * @return the asnProgressList
     */
    public List<AsnProgressInformationReturnDomain> getAsnProgressList() {
        return asnProgressList;
    }

    /**
     * <p>Setter method for asnProgressList.</p>
     *
     * @param asnProgressList Set for asnProgressList
     */
    public void setAsnProgressList(List<AsnProgressInformationReturnDomain> asnProgressList) {
        this.asnProgressList = asnProgressList;
    }

    /**
     * <p>Getter method for mode.</p>
     *
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * <p>Setter method for mode.</p>
     *
     * @param mode Set for mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * <p>Getter method for invoiceIdCriteria.</p>
     *
     * @return the invoiceIdCriteria
     */
    public String getInvoiceIdCriteria() {
        return invoiceIdCriteria;
    }

    /**
     * <p>Setter method for invoiceIdCriteria.</p>
     *
     * @param invoiceIdCriteria Set for invoiceIdCriteria
     */
    public void setInvoiceIdCriteria(String invoiceIdCriteria) {
        this.invoiceIdCriteria = invoiceIdCriteria;
    }

    /**
     * <p>Getter method for selectInvoiceDate.</p>
     *
     * @return the selectInvoiceDate
     */
    public String getSelectInvoiceDate() {
        return selectInvoiceDate;
    }

    /**
     * <p>Setter method for selectInvoiceDate.</p>
     *
     * @param selectInvoiceDate Set for selectInvoiceDate
     */
    public void setSelectInvoiceDate(String selectInvoiceDate) {
        this.selectInvoiceDate = selectInvoiceDate;
    }

    /**
     * <p>Getter method for selectCnDate.</p>
     *
     * @return the selectCnDate
     */
    public String getSelectCnDate() {
        return selectCnDate;
    }

    /**
     * <p>Setter method for selectCnDate.</p>
     *
     * @param selectCnDate Set for selectCnDate
     */
    public void setSelectCnDate(String selectCnDate) {
        this.selectCnDate = selectCnDate;
    }

    /**
     * <p>Getter method for rcvStatusCriteria.</p>
     *
     * @return the rcvStatusCriteria
     */
    public String getRcvStatusCriteria() {
        return rcvStatusCriteria;
    }

    /**
     * <p>Setter method for rcvStatusCriteria.</p>
     *
     * @param rcvStatusCriteria Set for rcvStatusCriteria
     */
    public void setRcvStatusCriteria(String rcvStatusCriteria) {
        this.rcvStatusCriteria = rcvStatusCriteria;
    }

    /**
     * <p>Getter method for invoiceNoCriteria.</p>
     *
     * @return the invoiceNoCriteria
     */
    public String getInvoiceNoCriteria() {
        return invoiceNoCriteria;
    }

    /**
     * <p>Setter method for invoiceNoCriteria.</p>
     *
     * @param invoiceNoCriteria Set for invoiceNoCriteria
     */
    public void setInvoiceNoCriteria(String invoiceNoCriteria) {
        this.invoiceNoCriteria = invoiceNoCriteria;
    }

    /**
     * <p>Getter method for cnNoCriteria.</p>
     *
     * @return the cnNoCriteria
     */
    public String getCnNoCriteria() {
        return cnNoCriteria;
    }

    /**
     * <p>Setter method for cnNoCriteria.</p>
     *
     * @param cnNoCriteria Set for cnNoCriteria
     */
    public void setCnNoCriteria(String cnNoCriteria) {
        this.cnNoCriteria = cnNoCriteria;
    }

    /**
     * <p>Getter method for dCdCriteria.</p>
     *
     * @return the dCdCriteria
     */
    public String getDCdCriteria() {
        return dCdCriteria;
    }

    /**
     * <p>Setter method for dCdCriteria.</p>
     *
     * @param dCdCriteria Set for dCdCriteria
     */
    public void setDCdCriteria(String dCdCriteria) {
        this.dCdCriteria = dCdCriteria;
    }

    /**
     * <p>Getter method for dPcdCriteria.</p>
     *
     * @return the dPcdCriteria
     */
    public String getDPcdCriteria() {
        return dPcdCriteria;
    }

    /**
     * <p>Setter method for dPcdCriteria.</p>
     *
     * @param dPcdCriteria Set for dPcdCriteria
     */
    public void setDPcdCriteria(String dPcdCriteria) {
        this.dPcdCriteria = dPcdCriteria;
    }

    /**
     * <p>Getter method for sCdCriteria.</p>
     *
     * @return the sCdCriteria
     */
    public String getSCdCriteria() {
        return sCdCriteria;
    }

    /**
     * <p>Setter method for sCdCriteria.</p>
     *
     * @param sCdCriteria Set for sCdCriteria
     */
    public void setSCdCriteria(String sCdCriteria) {
        this.sCdCriteria = sCdCriteria;
    }

    /**
     * <p>Getter method for sPcdCriteria.</p>
     *
     * @return the sPcdCriteria
     */
    public String getSPcdCriteria() {
        return sPcdCriteria;
    }

    /**
     * <p>Setter method for sPcdCriteria.</p>
     *
     * @param sPcdCriteria Set for sPcdCriteria
     */
    public void setSPcdCriteria(String sPcdCriteria) {
        this.sPcdCriteria = sPcdCriteria;
    }
    
    /**
     * Gets the cannot reset message.
     * 
     * @return the cannot reset message
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }
    
    /**
     * Sets the cannot reset message.
     * 
     * @param cannotResetMessage the cannot reset message
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }
    
    /**
     * Gets the cannot download message.
     * 
     * @return the cannot download message
     */
    public String getCannotDownloadMessage() {
        return cannotDownloadMessage;
    }

    /**
     * Sets the cannot download message.
     * 
     * @param cannotDownloadMessage the cannot download message
     */
    public void setCannotDownloadMessage(String cannotDownloadMessage) {
        this.cannotDownloadMessage = cannotDownloadMessage;
    }

    /**
     * Gets the selectedAsnNo.
     * 
     * @return the selectedAsnNo
     */
    public String getSelectedAsnNo() {
        return selectedAsnNo;
    }

    /**
     * Sets the selectedAsnNo.
     * 
     * @param selectedAsnNo the selectedAsnNo
     */
    public void setSelectedAsnNo(String selectedAsnNo) {
        this.selectedAsnNo = selectedAsnNo;
    }

    /**
     * Gets the selectedDCd.
     * 
     * @return the selectedDCd
     */
    public String getSelectedDCd() {
        return selectedDCd;
    }

    /**
     * Sets the selectedDCd.
     * 
     * @param selectedDCd the selectedDCd
     */
    public void setSelectedDCd(String selectedDCd) {
        this.selectedDCd = selectedDCd;
    }
    
    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }
}