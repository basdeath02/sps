/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

/**
 * The Class Wcom001Form.
 * @author CSI
 */
public class Wcom001Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 627552226332803804L;
    
    /** The txb user code. */
    private String userName;
    
    /** The txb password. */
    private String password;
   
    /** The change flag. */
    private String changeFlag;

    /**
     * Instantiates a new wCO m001 form.
     */
    public Wcom001Form(){
    }
    
    /**
     * Gets the user name.
     * 
     * @return the user name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the user name.
     * 
     * @param userName the new user name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Gets the password.
     * 
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password.
     * 
     * @param password the new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the change flag.
     * 
     * @return the change flag
     */
    public String getChangeFlag() {
        return changeFlag;
    }

    /**
     * Sets the change flag.
     * 
     * @param changeFlag the new change flag
     */
    public void setChangeFlag(String changeFlag) {
        this.changeFlag = changeFlag;
    }
}
