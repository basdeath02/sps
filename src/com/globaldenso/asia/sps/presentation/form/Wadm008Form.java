/*
 * ModifyDate Development company     Describe 
 * 2014/06/17 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDetailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WADM008Form.
 * @author CSI
 */
public class Wadm008Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7183550553430454137L;
    
    /** The DSC ID. */
    private String dscId;
    
    /** The Role Code Selected. */
    private String roleCdSelected;
       
    /** The Company DENSO Code. */
    private String dCd;
    
    /** The Plant DENSO. */
    private String dPcd;
    
    /** The first name. */
    private String firstName;
    
    /** The middle name.*/
    private String middleName;
    
    /** The last name.*/
    private String lastName;
    
    /** The User name. */
    private String densoUserName;
      
    /** The Effective start. */
    private String effectStart;
    
    /** The Effective end. */
    private String effectEnd;  
    
    /** The department. */
    private String department; 
    
    /** The role name. */
    private String roleName; 
    
    /** The role code. */
    private String roleCode;
        
    /** The Email. */
    private String email; 
    
    /** The Telephone. */
    private String telephone; 
    
    /** The Assign By. */
    private String assignBy; 
    
    /** The Assign Date. */
    private String assignDate;
    
    /** The Mode. */
    private String mode;
    
    /** The Return Mode. */
    private String returnMode;
    
    /** The Role Name Selected. */
    private String roleNameSelected;
    
    /** The page type. */
    private String pageType;
    
    /*** Sequence Number */
    private String seqNo;
    
    /** The List of user DENSO role detail. */
    private List<UserRoleDetailDomain> userRoleDetailList;
    
    /** The List of Company DENSO domain. */
    private List<CompanyDensoDomain> companyDensoList;
    
    /** The List of Plant DENSO domain. */
    private List<PlantDensoDomain> plantDensoList;
    
    /** The List of role domain. */
    private List<SpsMRoleDomain> roleList;
    
    /** The count of delete. */
    private String countDelete;
    
    /** The The last update date time for screen. */
    private String lastUpdateDatetimeScreen;
    
    /** Error message if user cannot click OK in Assign Role popup. */
    private String cannotOkMessage;
    
    /** Error message if user cannot click Cancel in Assign Role popup. */
    private String cannotCancelMessage;
    
    /** Error message if user cannot click Update in Assign Role */
    private String cannotUpdateMessage;
    
    /** Error message if user cannot click Add in Assign Role */
    private String cannotAddMessage;
    
    /** The default Constructor */
    public Wadm008Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        dCd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        email = Constants.EMPTY_STRING;
        assignBy = Constants.EMPTY_STRING;
        dscId = Constants.EMPTY_STRING;
        telephone = Constants.EMPTY_STRING;
        roleName = Constants.EMPTY_STRING;
        firstName = Constants.EMPTY_STRING;
        middleName = Constants.EMPTY_STRING;
        lastName = Constants.EMPTY_STRING;
        department = Constants.EMPTY_STRING;
        roleCode = Constants.EMPTY_STRING;
        densoUserName = Constants.EMPTY_STRING;
        countDelete = Constants.STR_ZERO;
        userRoleDetailList = new ArrayList<UserRoleDetailDomain>();
    }

    /**
     * Sets the DENSO Role item.
     * 
     * @param items the new DENSO Role item.
     */
    public void setRoleDenso(UserRoleDetailDomain items) {
        this.userRoleDetailList.add(items);
    }
    
    /**
     * Gets the DENSO Role item.
     * 
     * @param index the index
     * @return the DENSO Role item.
     */
    public UserRoleDetailDomain getRoleDenso(int index){
        if (userRoleDetailList == null) {
            userRoleDetailList = new ArrayList<UserRoleDetailDomain>();
        }
        while (userRoleDetailList.size() <= index) {
            userRoleDetailList.add(new UserRoleDetailDomain());
        }
        return userRoleDetailList.get(index);
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode(Register & Edit).
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode(Register & Edit).
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Gets the First Name.
     * 
     * @return the First Name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the First Name.
     * 
     * @param firstName the First Name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the Last Name.
     * 
     * @return the Last Name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the Last Name.
     * 
     * @param lastName the Last Name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the Department.
     * 
     * @return the Department.
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Sets the Department.
     * 
     * @param department the Department.
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * Gets the email.
     * 
     * @return the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     * 
     * @param email the email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the Telephone.
     * 
     * @return the Telephone.
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Sets the Telephone.
     * 
     * @param telephone the Telephone.
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    
    /**
     * Gets the Middle Name.
     * 
     * @return the Middle Name.
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the Middle Name.
     * 
     * @param middleName the Middle Name.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    
    /**
     * Gets the Role Name.
     * 
     * @return the Role Name.
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * Sets the Role Name.
     * 
     * @param roleName the Role Name.
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * Gets the Role Code.
     * 
     * @return the Role Code.
     */
    public String getRoleCode() {
        return roleCode;
    }

    /**
     * Sets the Role Code.
     * 
     * @param roleCode the Role Code.
     */
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    /**
     * Gets the Assign By.
     * 
     * @return the Assign By.
     */
    public String getAssignBy() {
        return assignBy;
    }

    /**
     * Sets the Assign By.
     * 
     * @param assignBy the Assign By.
     */
    public void setAssignBy(String assignBy) {
        this.assignBy = assignBy;
    }

    /**
     * Gets the Assign Date.
     * 
     * @return the Assign Date.
     */
    public String getAssignDate() {
        return assignDate;
    }

    /**
     * Sets the Assign Date.
     * 
     * @param assignDate the Assign Date.
     */
    public void setAssignDate(String assignDate) {
        this.assignDate = assignDate;
    }

    /**
     * Gets the page Type.
     * 
     * @return the page Type.
     */
    public String getPageType() {
        return pageType;
    }

    /**
     * Sets the page Type.
     * 
     * @param pageType the page Type.
     */
    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    /**
     * Gets the Role Name Selected.
     * 
     * @return the Role Name Selected.
     */
    public String getRoleNameSelected() {
        return roleNameSelected;
    }

    /**
     * Sets the Role Name Selected.
     * 
     * @param roleNameSelected the Role Name Selected.
     */
    public void setRoleNameSelected(String roleNameSelected) {
        this.roleNameSelected = roleNameSelected;
    }

    /**
     * Gets the effect Start date on Screen. 
     * 
     * @return the effect Start date on Screen. 
     */
    public String getEffectStart() {
        return effectStart;
    }

    /**
     * Sets the effect Start date on Screen. 
     * 
     * @param effectStart the effect Start date on Screen. 
     */
    public void setEffectStart(String effectStart) {
        this.effectStart = effectStart;
    }

    /**
     * Gets the effect End date on Screen. 
     * 
     * @return the effect End date on Screen. 
     */
    public String getEffectEnd() {
        return effectEnd;
    }

    /**
     * Sets the effect End date on Screen. 
     * 
     * @param effectEnd the effect End date on Screen. 
     */
    public void setEffectEnd(String effectEnd) {
        this.effectEnd = effectEnd;
    }
    /**
     * Gets the role Code Selected.
     * 
     * @return the role Code Selected.
     */
    public String getRoleCdSelected() {
        return roleCdSelected;
    }
    /**
     * Sets the role Code Selected.
     * 
     * @param roleCdSelected the role Code Selected.
     */
    public void setRoleCdSelected(String roleCdSelected) {
        this.roleCdSelected = roleCdSelected;
    }
    
    /**
     * Gets the Company DENSO Code.
     * 
     * @return the Company DENSO Code.
     */
    public String getDCd() {
        return dCd;
    }
    /**
     * Sets the Company DENSO Code.
     * 
     * @param dCd the Company DENSO Code.
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the plant DENSO Code.
     * 
     * @return the plant DENSO Code.
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * Sets the plant DENSO Code.
     * 
     * @param dPcd the plant DENSO Code.
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    
    /**
     * Gets the User name.
     * 
     * @return the User name.
     */
    public String getDensoUserName() {
        return densoUserName;
    }

    /**
     * Sets the User name.
     * 
     * @param densoUserName the User name.
     */
    public void setDensoUserName(String densoUserName) {
        this.densoUserName = densoUserName;
    }
    
    /**
     * Gets the list of Company DENSO
     * 
     * @return the list of Company DENSO
     */
    public List<CompanyDensoDomain> getCompanyDensoList() {
        return companyDensoList;
    }
    /**
     * Sets the list of Company DENSO
     * 
     * @param companyDensoList the list of Company DENSO
     */
    public void setCompanyDensoList(List<CompanyDensoDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }
    /**
     * Gets the list of plant DENSO
     * 
     * @return the list of plant DENSO
     */
    public List<PlantDensoDomain> getPlantDensoList() {
        return plantDensoList;
    }
    /**
     * Sets the list of plant DENSO
     * 
     * @param plantDensoList the list of plant DENSO
     */
    public void setPlantDensoList(List<PlantDensoDomain> plantDensoList) {
        this.plantDensoList = plantDensoList;
    }
    /**
     * Gets the DSC ID.
     * 
     * @return the DSC ID.
     */
    public String getDscId() {
        return dscId;
    }
    /**
     * Sets the DSC ID.
     * 
     * @param dscId the DSC ID.
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }
    /**
     * Gets the list of User DENSO Role.
     * 
     * @return the list of User DENSO Role.
     */
    public List<UserRoleDetailDomain> getUserRoleDetailList() {
        return userRoleDetailList;
    }
    /**
     * Sets the list of User DENSO Role.
     * 
     * @param userRoleDetailList the list of User DENSO Role.
     */
    public void setUserRoleDetailList(List<UserRoleDetailDomain> userRoleDetailList) {
        this.userRoleDetailList = userRoleDetailList;
    }
    
    /**
     * Gets the count of delete.
     * 
     * @return the count of delete.
     */
    public String getCountDelete() {
        return countDelete;
    }

    /**
     * Sets the count of delete.
     * 
     * @param countDelete the count of delete.
     */
    public void setCountDelete(String countDelete) {
        this.countDelete = countDelete;
    }
    
    /**
     * Gets the list of role domain.
     * 
     * @return the list of role domain.
     */
    public List<SpsMRoleDomain> getRoleList() {
        return roleList;
    }
    /**
     * Sets the list of role domain.
     * 
     * @param roleList the list of role domain.
     */
    public void setRoleList(List<SpsMRoleDomain> roleList) {
        this.roleList = roleList;
    }
    /**
     * Getter last update date time for screen.
     * 
     * @return the last update date time for screen.
     */
    public String getLastUpdateDatetimeScreen() {
        return lastUpdateDatetimeScreen;
    }
    /**
     * Setter last update date time for screen.
     * 
     * @param lastUpdateDatetimeScreen Set last update date time for screen.
     */
    public void setLastUpdateDatetimeScreen(String lastUpdateDatetimeScreen) {
        this.lastUpdateDatetimeScreen = lastUpdateDatetimeScreen;
    }
    /**
     * Getter return mode.
     * 
     * @return the return mode.
     */
    public String getReturnMode() {
        return returnMode;
    }
    /**
     * Setter return mode.
     * 
     * @param returnMode Set the return mode.
     */
    public void setReturnMode(String returnMode) {
        this.returnMode = returnMode;
    }
    
    /**
     * Get method for seqNo.
     * @return the seqNo
     */
    public String getSeqNo() {
        return seqNo;
    }

    /**
     * Set method for seqNo.
     * @param seqNo the seqNo to set
     */
    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }
    
    /**
     * <p>Getter method for cannotOkMessage.</p>
     *
     * @return the cannotOkMessage
     */
    public String getCannotOkMessage() {
        return cannotOkMessage;
    }

    /**
     * <p>Setter method for cannotOkMessage.</p>
     *
     * @param cannotOkMessage Set for cannotOkMessage
     */
    public void setCannotOkMessage(String cannotOkMessage) {
        this.cannotOkMessage = cannotOkMessage;
    }

    /**
     * <p>Getter method for cannotCancelMessage.</p>
     *
     * @return the cannotCancelMessage
     */
    public String getCannotCancelMessage() {
        return cannotCancelMessage;
    }

    /**
     * <p>Setter method for cannotCancelMessage.</p>
     *
     * @param cannotCancelMessage Set for cannotCancelMessage
     */
    public void setCannotCancelMessage(String cannotCancelMessage) {
        this.cannotCancelMessage = cannotCancelMessage;
    }

    /**
     * <p>Getter method for cannotUpdateMessage.</p>
     *
     * @return the cannotUpdateMessage
     */
    public String getCannotUpdateMessage() {
        return cannotUpdateMessage;
    }

    /**
     * <p>Setter method for cannotUpdateMessage.</p>
     *
     * @param cannotUpdateMessage Set for cannotUpdateMessage
     */
    public void setCannotUpdateMessage(String cannotUpdateMessage) {
        this.cannotUpdateMessage = cannotUpdateMessage;
    }

    /**
     * <p>Getter method for cannotAddMessage.</p>
     *
     * @return the cannotAddMessage
     */
    public String getCannotAddMessage() {
        return cannotAddMessage;
    }

    /**
     * <p>Setter method for cannotAddMessage.</p>
     *
     * @param cannotAddMessage Set for cannotAddMessage
     */
    public void setCannotAddMessage(String cannotAddMessage) {
        this.cannotAddMessage = cannotAddMessage;
    }
}