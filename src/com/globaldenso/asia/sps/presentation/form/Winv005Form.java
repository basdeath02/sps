/*
 * ModifyDate Developmentcompany Describe 
 * 2014/07/07 CSI Karnrawee      Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WINV005Form.
 * @author CSI
 */
public class Winv005Form extends CoreActionForm {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 8720900529081611615L;
    
    /** The invoice date from. */
    private String invoiceDateFrom;
    
    /** The invoice date to. */
    private String invoiceDateTo;
    
    /** The cn date from. */
    private String cnDateFrom;
    
    /** The cn date to. */
    private String cnDateTo;
    
    /** The supplier code. */
    private String sCd;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier plant code. */
    private String sPcd;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plant code. */
    private String dPcd;
    
    /** The invoice status.*/
    private String invoiceStatus;
    
    /** The invoice no.*/
    private String invoiceNo;
    
    /** The cn no.*/
    private String cnNo;
    
    /** The asn no.*/
    private String asnNo;
    
    /** The denso part no.*/
    private String dPn;
    
    /** The supplier part no.*/
    private String sPn;
    
    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The status index.*/
    private String statusIndex;
    
    /** The file Id.*/
    private String fileId;
    
    /** The session Id. */
    private String sessionId;
    
    /** The list of invoice information domain.*/
    private List<PriceDifferenceInformationDomain> priceDifferenceInformationForDisplayList;
    
    /** The invoice no for forwarding page.*/
    private String invoiceNoForward;
    
    /** The Supplier Code List.*/
    private List<CompanySupplierDomain> sCdList; 
    
    /** The Supplier Plant Code List.*/
    private List<SpsMMiscDomain> sPcdList;
    
    /** The d Code List.*/
    private List<CompanyDensoDomain> dCdList;
    
    /** The d Plant Code List.*/
    private List<SpsMMiscDomain> dPcdList;
    
    /** The invoice Status List.*/
    private List<MiscellaneousDomain> invoiceStatusList;
    
    /** The Message when cannot click reset. */
    private String cannotResetMessage;
    
    /** The Message when cannot click download. */
    private String cannotDownloadMessage;
    
    /** The default constructor. */
    public Winv005Form() {
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
    } 
    
    /**
     * reset Form.
     */
    public void resetForm() {
        vendorCd = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        invoiceStatus = Constants.EMPTY_STRING;
        statusIndex = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        priceDifferenceInformationForDisplayList = 
            new ArrayList<PriceDifferenceInformationDomain>();
        sCdList = new ArrayList<CompanySupplierDomain>();
        dCdList = new ArrayList<CompanyDensoDomain>();
        invoiceStatusList = new ArrayList<MiscellaneousDomain>();
    }
    
    /**
     * <p>Getter method for invoice date from.</p>
     *
     * @return the invoice date from
     */
    public String getInvoiceDateFrom() {
        return invoiceDateFrom;
    }
    
    /**
     * <p>Setter method for invoice date from.</p>
     *
     * @param invoiceDateFrom Set for invoice date from
     */
    public void setInvoiceDateFrom(String invoiceDateFrom) {
        this.invoiceDateFrom = invoiceDateFrom;
    }
    
    /**
     * <p>Getter method for invoice date to.</p>
     *
     * @return the invoice date to
     */
    public String getInvoiceDateTo() {
        return invoiceDateTo;
    }

    /**
     * <p>Setter method for invoice date to.</p>
     *
     * @param invoiceDateTo Set for invoice date to
     */
    public void setInvoiceDateTo(String invoiceDateTo) {
        this.invoiceDateTo = invoiceDateTo;
    }
    
    /**
     * <p>Getter method for cn date from.</p>
     *
     * @return the cn date from
     */
    public String getCnDateFrom() {
        return cnDateFrom;
    }
    
    /**
     * <p>Setter method for cn date from.</p>
     *
     * @param cnDateFrom Set for cn date to
     */
    public void setCnDateFrom(String cnDateFrom) {
        this.cnDateFrom = cnDateFrom;
    }
    
    /**
     * <p>Getter method for cn date to.</p>
     *
     * @return the cn date to
     */
    public String getCnDateTo() {
        return cnDateTo;
    }

    /**
     * <p>Setter method for cn date to.</p>
     *
     * @param cnDateTo Set for cn date to
     */
    public void setCnDateTo(String cnDateTo) {
        this.cnDateTo = cnDateTo;
    }
    
    /**
     * <p>Getter method for supplier code.</p>
     *
     * @return the supplier code
     */
    public String getDCd() {
        return dCd;
    }
    
    /**
     * <p>Setter method for denso code.</p>
     *
     * @param dCd Set for denso code
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }
    
    /**
     * <p>Getter method for invoice no.</p>
     *
     * @return the invoice no
     */
    public String getInvoiceNo() {
        return invoiceNo;
    }
    /**
     * <p>Setter method for invoice no.</p>
     *
     * @param invoiceNo Set for invoice no
     */
    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }
    
    /**
     * <p>Getter method for cn no.</p>
     *
     * @return the cn no
     */
    public String getCnNo() {
        return cnNo;
    }
    
    /**
     * <p>Setter method for cn no.</p>
     *
     * @param cnNo Set for cn noe
     */
    public void setCnNo(String cnNo) {
        this.cnNo = cnNo;
    }
    
    /**
     * <p>Getter method for asn no.</p>
     *
     * @return the asn no
     */
    public String getAsnNo() {
        return asnNo;
    }
    
    /**
     * <p>Setter method for asn no.</p>
     *
     * @param asnNo Set for asn no
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }
    
    /**
     * <p>Getter method for denso part no.</p>
     *
     * @return the denso part no
     */
    public String getDPn() {
        return dPn;
    }
    
    /**
     * <p>Setter method for denso part no.</p>
     *
     * @param dPn Set for denso part no
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }
    
    /**
     * <p>Getter method for supplier part no.</p>
     *
     * @return the supplier part no
     */
    public String getSPn() {
        return sPn;
    }
    
    /**
     * <p>Setter method for supplier part no.</p>
     *
     * @param sPn Set for supplier part no
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }
    
    /**
     * <p>Getter method for supplier plan code.</p>
     *
     * @return the supplier plan code
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * <p>Setter method for denso plan code.</p>
     *
     * @param dPcd Set for denso plan code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    
    /**
     * <p>Getter method for invoice status.</p>
     *
     * @return the invoice status
     */
    public String getInvoiceStatus() {
        return invoiceStatus;
    }
    /**
     * <p>Setter method for invoice status.</p>
     *
     * @param invoiceStatus Set for invoice status
     */
    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }
    
    /**
     * <p>Getter method for supplier code.</p>
     *
     * @return the supplier code
     */
    public String getSCd() {
        return sCd;
    }
    
    /**
     * <p>Setter method for supplier code.</p>
     *
     * @param sCd Set for supplier code
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * <p>Getter method for supplier plan code.</p>
     *
     * @return the supplier plan code
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * <p>Setter method for supplier plan code.</p>
     *
     * @param sPcd Set for supplier plan code
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    
    /**
     * <p>Getter method for sessionId.</p>
     *
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * <p>Setter method for sessionId.</p>
     *
     * @param sessionId Set for sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * <p>Getter method for invoice information list.</p>
     *
     * @return the invoice information list
     */
    public List<PriceDifferenceInformationDomain> getPriceDifferenceInformationForDisplayList() {
        return priceDifferenceInformationForDisplayList;
    }
    
    /**
     * <p>Setter method for invoice information for display list.</p>
     *
     * @param priceDifferenceInformationForDisplayList Set for invoice information for display list
     */
    public void setPriceDifferenceInformationForDisplayList(List<PriceDifferenceInformationDomain> 
    priceDifferenceInformationForDisplayList) {
        this.priceDifferenceInformationForDisplayList = priceDifferenceInformationForDisplayList;
    }
    
    /**
     * Gets the invoice no for forwarding page.
     * 
     * @return the invoice no for forwarding page.
     */
    public String getInvoiceNoForward() {
        return invoiceNoForward;
    }
    
    /**
     * Sets the invoice no for forwarding page.
     * 
     * @param invoiceNoForward the invoice no for forwarding page.
     */  
    public void setInvoiceNoForward(String invoiceNoForward) {
        this.invoiceNoForward = invoiceNoForward;
    }
    
    /**
     * <p>Getter method for invoiceStatusList.</p>
     *
     * @return the invoiceStatusList
     */
    public List<MiscellaneousDomain> getInvoiceStatusList() {
        return invoiceStatusList;
    }

    /**
     * <p>Setter method for invoiceStatusList.</p>
     *
     * @param invoiceStatusList Set for invoiceStatusList
     */
    public void setInvoiceStatusList(List<MiscellaneousDomain> invoiceStatusList) {
        this.invoiceStatusList = invoiceStatusList;
    }

    /**
     * <p>Getter method for supplierCodeList.</p>
     *
     * @return the supplierCodeList
     */
    public List<CompanySupplierDomain> getSCdList() {
        return sCdList;
    }

    /**
     * <p>Setter method for supplierCodeList.</p>
     *
     * @param sCdList Set for supplierCodeList
     */
    public void setSCdList(List<CompanySupplierDomain> sCdList) {
        this.sCdList = sCdList;
    }

    /**
     * <p>Getter method for supplierPlantCodeList.</p>
     *
     * @return the supplierPlantCodeList
     */
    public List<SpsMMiscDomain> getSPcdList() {
        return sPcdList;
    }

    /**
     * <p>Setter method for supplierPlantCodeList.</p>
     *
     * @param sPcdList Set for supplierPlantCodeList
     */
    public void setSPcdList(List<SpsMMiscDomain> sPcdList) {
        this.sPcdList = sPcdList;
    }

    /**
     * <p>Getter method for densoCodeList.</p>
     *
     * @return the densoCodeList
     */
    public List<CompanyDensoDomain> getDCdList() {
        return dCdList;
    }

    /**
     * <p>Setter method for densoCodeList.</p>
     *
     * @param dCdList Set for densoCodeList
     */
    public void setDCdList(List<CompanyDensoDomain> dCdList) {
        this.dCdList = dCdList;
    }

    /**
     * <p>Getter method for densoPlantCodeList.</p>
     *
     * @return the densoPlantCodeList
     */
    public List<SpsMMiscDomain> getDPcdList() {
        return dPcdList;
    }

    /**
     * <p>Setter method for densoPlantCodeList.</p>
     *
     * @param dPcdList Set for densoPlantCodeList
     */
    public void setDPcdList(List<SpsMMiscDomain> dPcdList) {
        this.dPcdList = dPcdList;
    }

    /**
     * <p>Getter method for fileId.</p>
     *
     * @return the fileId
     */
    public String getFileId() {
        return fileId;
    }

    /**
     * <p>Setter method for fileId.</p>
     *
     * @param fileId Set for fileId
     */
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
    
    /**
     * <p>Getter method for statusIndex.</p>
     *
     * @return the statusIndex
     */
    public String getStatusIndex() {
        return statusIndex;
    }

    /**
     * <p>Setter method for statusIndex.</p>
     *
     * @param statusIndex Set for statusIndex
     */
    public void setStatusIndex(String statusIndex) {
        this.statusIndex = statusIndex;
    }

    /**
     * Sets the company supplier.
     * 
     * @param companySupplier the new company supplier.
     */
    public void setCompanySupplierItem(CompanySupplierDomain companySupplier) {
        this.sCdList.add(companySupplier);
    }
    
    /**
     * Gets the company supplier.
     * 
     * @param index the index
     * @return the company supplier.
     */
    public CompanySupplierDomain getCompanySupplierItem(int index) {
        if (sCdList == null) {
            sCdList = new ArrayList<CompanySupplierDomain>();
        }
        while (sCdList.size() <= index) {
            sCdList.add(new CompanySupplierDomain());
        }
        return sCdList.get(index);
    }
    
    /**
     * Sets the company DENSO.
     * 
     * @param companyDenso the new company DENSO.
     */
    public void setCompanyDensoItem(CompanyDensoDomain companyDenso) {
        this.dCdList.add(companyDenso);
    }
    
    /**
     * Gets the company DENSO.
     * 
     * @param index the index
     * @return the company DENSO.
     */
    public CompanyDensoDomain getCompanyDensoItem(int index) {
        if (dCdList == null) {
            dCdList = new ArrayList<CompanyDensoDomain>();
        }
        while (dCdList.size() <= index) {
            dCdList.add(new CompanyDensoDomain());
        }
        return dCdList.get(index);
    }
    
    /**
     * <p>
     * Setter method for invoice information.
     * </p>
     * 
     * @param items the new invoice information
     */
    public void setPriceDifferenceInformation(PriceDifferenceInformationDomain items) {
        this.priceDifferenceInformationForDisplayList.add(items);
    }

    /**
     * Gets the invoice information item.
     * 
     * @param index the index
     * @return the invoice information item.
     */
    public PriceDifferenceInformationDomain getPriceDifferenceInformation(int index) {
        if (priceDifferenceInformationForDisplayList == null) {
            priceDifferenceInformationForDisplayList = 
                new ArrayList<PriceDifferenceInformationDomain>();
        }
        while (priceDifferenceInformationForDisplayList.size() <= index) {
            priceDifferenceInformationForDisplayList
                .add(new PriceDifferenceInformationDomain());
        }
        return priceDifferenceInformationForDisplayList.get(index);
    }

    /**
     * <p>Getter method for cannotResetMessage.</p>
     *
     * @return the cannotResetMessage
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }

    /**
     * <p>Setter method for cannotResetMessage.</p>
     *
     * @param cannotResetMessage Set for cannotResetMessage
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }

    /**
     * <p>Getter method for cannotDownloadMessage.</p>
     *
     * @return the cannotDownloadMessage
     */
    public String getCannotDownloadMessage() {
        return cannotDownloadMessage;
    }

    /**
     * <p>Setter method for cannotDownloadMessage.</p>
     *
     * @param cannotDownloadMessage Set for cannotDownloadMessage
     */
    public void setCannotDownloadMessage(String cannotDownloadMessage) {
        this.cannotDownloadMessage = cannotDownloadMessage;
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

    /**
     * Sets the Supplier Authen.
     * 
     * @param supplierAuthen the new Supplier Authen.
     */
    public void setSupplierAuthen(SpsMPlantSupplierDomain supplierAuthen) {
        this.supplierAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new Supplier Authen.
     * 
     * @param index the index
     * @return the Supplier Authen.
     */
    public SpsMPlantSupplierDomain getSupplierAuthen(int index) {
        if (supplierAuthenList == null) {
            supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        }
        while (supplierAuthenList.size() <= index) {
            supplierAuthenList.add(new SpsMPlantSupplierDomain());
        }
        return supplierAuthenList.get(index);
    }
    
    /**
     * Sets the DENSO Authen.
     * 
     * @param supplierAuthen the new DENSO Authen.
     */
    public void setDensoAuthen(SpsMPlantDensoDomain supplierAuthen) {
        this.densoAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new DENSO Authen.
     * 
     * @param index the index
     * @return the DENSO Authen.
     */
    public SpsMPlantDensoDomain getDensoAuthen(int index) {
        if (densoAuthenList == null) {
            densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        }
        while (densoAuthenList.size() <= index) {
            densoAuthenList.add(new SpsMPlantDensoDomain());
        }
        return densoAuthenList.get(index);
    }
    
}
