/*
 * ModifyDate Development company   Describe 
 * 2014/08/15 CSI Parichat          Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.upload.FormFile;

import com.globaldenso.asia.sps.business.domain.AsnUploadingReturnDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class Wshp006Form.
 * @author CSI
 */
public class Wshp006Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7893087921328839912L;

    /** The mode. */
    private String mode;
    
    /** The session id. */
    private String sessionId;
    
    /** The upload dsc id. */
    private String uploadDscId;
    
    /** The file data. */
    private FormFile fileData;
    
    /** The file name upload. */
    private String fileNameUpload;
    
    /** The total record. */
    private String totalRecord;
    
    /** The correct record. */
    private String correctRecord;
    
    /** The warning record. */
    private String warningRecord;
    
    /** The error record. */
    private String errorRecord;
    
    /** The asn no. */
    private String asnNo;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier plant code. */
    private String sPcd;
    
    /** The denso code. */
    private String dCd;
    
    /** The denso plant code. */
    private String dPcd;
    
    /** The has error flag. */
    private String hasErrorFlag;
    
    /** The error message for operating reset button. */
    private String cannotResetMessage;
    
    /** The tmp upload asn list. */
    private List<AsnUploadingReturnDomain> tmpUploadAsnList;
    
    /** The default Constructor */
    public Wshp006Form() {
        tmpUploadAsnList = new ArrayList<AsnUploadingReturnDomain>();
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        mode = Constants.EMPTY_STRING;
        sessionId = Constants.EMPTY_STRING;
        uploadDscId = Constants.EMPTY_STRING;
        fileData = null;
        fileNameUpload = Constants.EMPTY_STRING;
        totalRecord = Constants.EMPTY_STRING;
        correctRecord = Constants.EMPTY_STRING;
        warningRecord = Constants.EMPTY_STRING;
        errorRecord = Constants.EMPTY_STRING;
        asnNo = Constants.EMPTY_STRING;
        vendorCd = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        hasErrorFlag = Constants.STR_ZERO;
        tmpUploadAsnList = null;
    }
    
    /**
     * <p>Setter method for tmp upload ASN result.</p>
     * 
     * @param items the new asn uploading return domain
     */
    public void setTmpUploadAsnResult(AsnUploadingReturnDomain items) {
        this.tmpUploadAsnList.add(items);
    }
    
    /**
     * Gets the tmp upload ASN result.
     * 
     * @param index the index
     * @return the asn uploading return domain
     */
    public AsnUploadingReturnDomain getTmpUploadAsnResult(int index){
        if (tmpUploadAsnList == null) {
            tmpUploadAsnList = new ArrayList<AsnUploadingReturnDomain>();
        }
        while (tmpUploadAsnList.size() <= index) {
            tmpUploadAsnList.add(new AsnUploadingReturnDomain());
        }
        return tmpUploadAsnList.get(index);
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode
     */
    public String getMode() {
        return mode;
    }
    
    /**
     * Sets the mode.
     * 
     * @param mode the mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }
    
    /**
     * Gets the session id.
     * 
     * @return the session id
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Sets the session id.
     * 
     * @param sessionId the session id
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    
    /**
     * Gets the upload dsc id.
     * 
     * @return the upload dsc id
     */
    public String getUploadDscId() {
        return uploadDscId;
    }

    /**
     * Sets the upload dsc id.
     * 
     * @param uploadDscId the upload dsc id
     */
    public void setUploadDscId(String uploadDscId) {
        this.uploadDscId = uploadDscId;
    }

    /**
     * Gets the file data.
     * 
     * @return the file data
     */
    public FormFile getFileData() {
        return fileData;
    }
    
    /**
     * Sets the file data.
     * 
     * @param fileData the file data
     */
    public void setFileData(FormFile fileData) {
        this.fileData = fileData;
    }
    
    /**
     * Gets the file name upload.
     * 
     * @return the file name upload
     */
    public String getFileNameUpload() {
        return fileNameUpload;
    }

    /**
     * Sets the file name upload.
     * 
     * @param fileNameUpload the file name upload
     */
    public void setFileNameUpload(String fileNameUpload) {
        this.fileNameUpload = fileNameUpload;
    }

    /**
     * Gets the total record.
     * 
     * @return the total record
     */
    public String getTotalRecord() {
        return totalRecord;
    }
    
    /**
     * Sets the total record.
     * 
     * @param totalRecord the total record
     */
    public void setTotalRecord(String totalRecord) {
        this.totalRecord = totalRecord;
    }
    
    /**
     * Gets the correct record.
     * 
     * @return the correct record
     */
    public String getCorrectRecord() {
        return correctRecord;
    }
    
    /**
     * Sets the correct record.
     * 
     * @param correctRecord the correct record
     */
    public void setCorrectRecord(String correctRecord) {
        this.correctRecord = correctRecord;
    }

    /**
     * Gets the warning record.
     * 
     * @return the warning record
     */
    public String getWarningRecord() {
        return warningRecord;
    }

    /**
     * Sets the warning record.
     * 
     * @param warningRecord the warning record
     */
    public void setWarningRecord(String warningRecord) {
        this.warningRecord = warningRecord;
    }

    /**
     * Gets the error record.
     * 
     * @return the error record
     */
    public String getErrorRecord() {
        return errorRecord;
    }

    /**
     * Sets the error record.
     * 
     * @param errorRecord the error record
     */
    public void setErrorRecord(String errorRecord) {
        this.errorRecord = errorRecord;
    }

    /**
     * Gets the asn no.
     * 
     * @return the asn no
     */
    public String getAsnNo() {
        return asnNo;
    }

    /**
     * Sets the asn no.
     * 
     * @param asnNo the asn no
     */
    public void setAsnNo(String asnNo) {
        this.asnNo = asnNo;
    }
    
    /**
     * Gets the vendor code.
     * 
     * @return the vendor code
     */
    public String getVendorCd() {
        return vendorCd;
    }
    
    /**
     * Sets the vendor code.
     * 
     * @param vendorCd the vendor code
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * Gets the supplier plant code.
     * 
     * @return the supplier plant code
     */
    public String getSPcd() {
        return sPcd;
    }

    /**
     * Sets the supplier plant code.
     * 
     * @param sPcd the supplier plant code
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Gets the denso code.
     * 
     * @return the denso code
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the denso code.
     * 
     * @param dCd the denso code
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the denso plant code.
     * 
     * @return the denso plant code
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * Sets the denso plant code.
     * 
     * @param dPcd the denso plant code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }
    
    /**
     * Gets the has error flag.
     * 
     * @return the has error flag
     */
    public String getHasErrorFlag() {
        return hasErrorFlag;
    }

    /**
     * Sets the has error flag.
     * 
     * @param hasErrorFlag the has error flag
     */
    public void setHasErrorFlag(String hasErrorFlag) {
        this.hasErrorFlag = hasErrorFlag;
    }
    
    /**
     * Gets the cannot reset message.
     * 
     * @return the cannot reset message
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }
    
    /**
     * Sets the cannot reset message.
     * 
     * @param cannotResetMessage the cannot reset message
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }

    /**
     * Gets the temp upload asn list.
     * 
     * @return the temp upload asn list
     */
    public List<AsnUploadingReturnDomain> getTmpUploadAsnList() {
        return tmpUploadAsnList;
    }

    /**
     * Sets the temp upload asn list.
     * 
     * @param tmpUploadAsnList the temp upload asn list
     */
    public void setTmpUploadAsnList(List<AsnUploadingReturnDomain> tmpUploadAsnList) {
        this.tmpUploadAsnList = tmpUploadAsnList;
    }
}