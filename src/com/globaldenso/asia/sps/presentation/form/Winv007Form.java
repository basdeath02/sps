/*
 * ModifyDate Developmentcompany Describe 
 * 2014/06/17 CSI Karnrawee      Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.FileUploadForDisplayDomain;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WINV007Form.
 * @author CSI
 */
public class Winv007Form extends CoreActionForm {

    /**
     * <p>Type in the role of the field.</p>
     */
    private static final long serialVersionUID = 8720900529081611615L;
    
    /** The update date. */
    private String updateDateFrom;
    
    /** The update date. */
    private String updateDateTo;
    
    /** The supplier code. */
    private String sCd;
    
    /** The vendor code. */
    private String vendorCd;
    
    /** The supplier plant code. */
    private String sPcd;
    
    /** The file name.*/
    private String fileNameCriteria;
    
    /** The list of file upload domain.*/
    private List<FileUploadForDisplayDomain> fileUploadForDisplayList;
    
    /** The file upload ID. */
    private String fileId;

    /** The Company Supplier and Plant Supplier list that user has permission. */
    private List<SpsMPlantSupplierDomain> supplierAuthenList;

    /** The Company DENSO and Plant DENSO list that user has permission. */
    private List<SpsMPlantDensoDomain> densoAuthenList;
    
    /** The supplierCodeList */
    private List<MiscellaneousDomain> sCdList;
    
    /** The supplierPlanCodeList */
    private List<SpsMMiscDomain> sPcdList;
    
    /** The DENSO code. */
    private String dCd;
    
    /** The DENSO plant code. */
    private String dPcd;
    
    /** The company Denso List. */
    private List<MiscellaneousDomain> companyDensoList;
    
    /** The Denso Plant Code List. */
    private List<PlantDensoDomain> dPcdList;
    
    /** The Message when cannot click reset. */
    private String cannotResetMessage;
    
    /** The Message when cannot click download. */
    private String cannotDownloadMessage;
    
    /** The default constructor. */
    public Winv007Form() {
        this.densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        this.supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.globaldenso.asia.sps.presentation.form.CoreActionForm#resetForm()
     */
    public void resetForm() {
        updateDateFrom = Constants.EMPTY_STRING;
        updateDateTo = Constants.EMPTY_STRING;
        vendorCd = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        sCdList = null;
        sPcdList = null;
        fileNameCriteria = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        companyDensoList = null;
        dPcdList = null;
    }
    /**
     * <p>Getter method for update date from.</p>
     *
     * @return the update date from
     */
    public String getUpdateDateFrom() {
        return updateDateFrom;
    }
    
    /**
     * <p>Setter method for update date from.</p>
     *
     * @param updateDateFrom Set for update date to
     */
    public void setUpdateDateFrom(String updateDateFrom) {
        this.updateDateFrom = updateDateFrom;
    }
    
    /**
     * <p>Getter method for update date to.</p>
     *
     * @return the update date to
     */
    public String getUpdateDateTo() {
        return updateDateTo;
    }

    /**
     * <p>Setter method for update date to.</p>
     *
     * @param updateDateTo Set for update date to
     */
    public void setUpdateDateTo(String updateDateTo) {
        this.updateDateTo = updateDateTo;
    }

    /**
     * <p>Getter method for supplier code.</p>
     *
     * @return the supplier code
     */
    public String getSCd() {
        return sCd;
    }
    
    /**
     * <p>Setter method for supplier code.</p>
     *
     * @param sCd Set for supplier code
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    
    /**
     * <p>Getter method for vendorCd.</p>
     *
     * @return the vendorCd
     */
    public String getVendorCd() {
        return vendorCd;
    }

    /**
     * <p>Setter method for vendorCd.</p>
     *
     * @param vendorCd Set for vendorCd
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * <p>Getter method for supplier plan code.</p>
     *
     * @return the supplier plan code
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * <p>Setter method for supplier plan code.</p>
     *
     * @param sPcd Set for supplier plan code
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }
    
    /**
     * <p>Getter method for file name criteria.</p>
     *
     * @return the file name criteria
     */
    public String getFileNameCriteria() {
        return fileNameCriteria;
    }
    
    /**
     * <p>Setter method for file name criteria.</p>
     *
     * @param fileNameCriteria Set for file name criteria
     */
    public void setFileNameCriteria(String fileNameCriteria) {
        this.fileNameCriteria = fileNameCriteria;
    }
    
    /**
     * <p>Getter method for file upload list.</p>
     *
     * @return the file upload list
     */
    public List<FileUploadForDisplayDomain> getFileUploadForDisplayList() {
        return fileUploadForDisplayList;
    }
    
    /**
     * <p>Setter method for file upload for display  list.</p>
     *
     * @param fileUploadForDisplayList Set for file upload for display list
     */
    public void setFileUploadForDisplayList(List<FileUploadForDisplayDomain> 
    fileUploadForDisplayList) {
        this.fileUploadForDisplayList = fileUploadForDisplayList;
    }
    
    /**
     * Gets the file ID.
     * 
     * @return the file ID.
     */
    public String getFileId() {
        return fileId;
    }
    
    /**
     * Sets the file ID.
     * 
     * @param fileId the file ID.
     */  
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

   

    

    /**
     * <p>Getter method for supplierPlanCodeList.</p>
     *
     * @return the supplierPlanCodeList
     */
    public List<SpsMMiscDomain> getSPcdList() {
        return sPcdList;
    }

    /**
     * <p>Setter method for supplierPlanCodeList.</p>
     *
     * @param sPcdList Set for supplierPlanCodeList
     */
    public void setSPcdList(
        List<SpsMMiscDomain> sPcdList){
        this.sPcdList = sPcdList;
    }
    
    /**
     * <p>Getter method for dCd.</p>
     *
     * @return the dCd
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * <p>Setter method for dCd.</p>
     *
     * @param dCd Set for dCd
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * <p>Getter method for dPcd.</p>
     *
     * @return the dPcd
     */
    public String getDPcd() {
        return dPcd;
    }

    /**
     * <p>Setter method for dPcd.</p>
     *
     * @param dPcd Set for dPcd
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * <p>Getter method for companyDensoList.</p>
     *
     * @return the companyDensoList
     */
    public List<MiscellaneousDomain> getCompanyDensoList() {
        return companyDensoList;
    }

    /**
     * <p>Setter method for companyDensoList.</p>
     *
     * @param companyDensoList Set for companyDensoList
     */
    public void setCompanyDensoList(List<MiscellaneousDomain> companyDensoList) {
        this.companyDensoList = companyDensoList;
    }

    /**
     * <p>Getter method for dPcdList.</p>
     *
     * @return the dPcdList
     */
    public List<PlantDensoDomain> getDPcdList() {
        return dPcdList;
    }

    /**
     * <p>Setter method for dPcdList.</p>
     *
     * @param dPcdList Set for dPcdList
     */
    public void setDPcdList(List<PlantDensoDomain> dPcdList) {
        this.dPcdList = dPcdList;
    }

    /**
     * <p>Getter method for sCdList.</p>
     *
     * @return the sCdList
     */
    public List<MiscellaneousDomain> getSCdList() {
        return sCdList;
    }

    /**
     * <p>Setter method for sCdList.</p>
     *
     * @param sCdList Set for sCdList
     */
    public void setSCdList(List<MiscellaneousDomain> sCdList) {
        this.sCdList = sCdList;
    }

    /**
     * <p>Getter method for cannotResetMessage.</p>
     *
     * @return the cannotResetMessage
     */
    public String getCannotResetMessage() {
        return cannotResetMessage;
    }

    /**
     * <p>Setter method for cannotResetMessage.</p>
     *
     * @param cannotResetMessage Set for cannotResetMessage
     */
    public void setCannotResetMessage(String cannotResetMessage) {
        this.cannotResetMessage = cannotResetMessage;
    }

    /**
     * <p>Getter method for cannotDownloadMessage.</p>
     *
     * @return the cannotDownloadMessage
     */
    public String getCannotDownloadMessage() {
        return cannotDownloadMessage;
    }

    /**
     * <p>Setter method for cannotDownloadMessage.</p>
     *
     * @param cannotDownloadMessage Set for cannotDownloadMessage
     */
    public void setCannotDownloadMessage(String cannotDownloadMessage) {
        this.cannotDownloadMessage = cannotDownloadMessage;
    }

    /**
     * Sets the Supplier Authen.
     * 
     * @param supplierAuthen the new Supplier Authen.
     */
    public void setSupplierAuthen(SpsMPlantSupplierDomain supplierAuthen) {
        this.supplierAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new Supplier Authen.
     * 
     * @param index the index
     * @return the Supplier Authen.
     */
    public SpsMPlantSupplierDomain getSupplierAuthen(int index) {
        if (supplierAuthenList == null) {
            supplierAuthenList = new ArrayList<SpsMPlantSupplierDomain>();
        }
        while (supplierAuthenList.size() <= index) {
            supplierAuthenList.add(new SpsMPlantSupplierDomain());
        }
        return supplierAuthenList.get(index);
    }
    
    /**
     * Sets the DENSO Authen.
     * 
     * @param supplierAuthen the new DENSO Authen.
     */
    public void setDensoAuthen(SpsMPlantDensoDomain supplierAuthen) {
        this.densoAuthenList.add(supplierAuthen);
    }
    
    /**
     * Gets the new DENSO Authen.
     * 
     * @param index the index
     * @return the DENSO Authen.
     */
    public SpsMPlantDensoDomain getDensoAuthen(int index) {
        if (densoAuthenList == null) {
            densoAuthenList = new ArrayList<SpsMPlantDensoDomain>();
        }
        while (densoAuthenList.size() <= index) {
            densoAuthenList.add(new SpsMPlantDensoDomain());
        }
        return densoAuthenList.get(index);
    }

    /**
     * <p>Getter method for supplierAuthenList.</p>
     *
     * @return the supplierAuthenList
     */
    public List<SpsMPlantSupplierDomain> getSupplierAuthenList() {
        return supplierAuthenList;
    }

    /**
     * <p>Setter method for supplierAuthenList.</p>
     *
     * @param supplierAuthenList Set for supplierAuthenList
     */
    public void setSupplierAuthenList(
        List<SpsMPlantSupplierDomain> supplierAuthenList) {
        this.supplierAuthenList = supplierAuthenList;
    }

    /**
     * <p>Getter method for densoAuthenList.</p>
     *
     * @return the densoAuthenList
     */
    public List<SpsMPlantDensoDomain> getDensoAuthenList() {
        return densoAuthenList;
    }

    /**
     * <p>Setter method for densoAuthenList.</p>
     *
     * @param densoAuthenList Set for densoAuthenList
     */
    public void setDensoAuthenList(List<SpsMPlantDensoDomain> densoAuthenList) {
        this.densoAuthenList = densoAuthenList;
    }

}
