/*
 * ModifyDate Development company     Describe 
 * 2014/08/18 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.List;

import com.globaldenso.asia.sps.business.domain.PurchaseOrderNotificationDomain;

/**
 * The Class Word005Form.
 * @author CSI
 */
public class Word005Form extends CoreActionForm {

    /** The Generated Serial Version UID. */
    private static final long serialVersionUID = -4204429699010485075L;

    /** The DSC_ID. */
    private String dscId;
    
    /** The PO ID. */
    private String poId;
    
    /** The Supplier Part No. */
    private String sPn;
    
    /** The DENSO Part No. */
    private String dPn;
    
    /** The Period Type. */
    private String periodType;
    
    /** The Period Type Name. */
    private String periodTypeName;
    
    /** The PO Type. */
    private String poType;

    /** The SPS PO number. */
    private String spsPoNo;
    
    /** Is user can press Return button. */
    private String returnFlag;
    
    /** Error message when user cannot press return. */
    private String cannotReturnMessage;

    /** The Purchase Order Notification List. */
    private List<PurchaseOrderNotificationDomain> purchaseOrderNotificationList;
    
    /** The Default Constructor. */
    public Word005Form() {
        super();
    }

    /**
     * <p>Getter method for dscId.</p>
     *
     * @return the dscId
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * <p>Setter method for dscId.</p>
     *
     * @param dscId Set for dscId
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * <p>Getter method for poId.</p>
     *
     * @return the poId
     */
    public String getPoId() {
        return poId;
    }

    /**
     * <p>Setter method for poId.</p>
     *
     * @param poId Set for poId
     */
    public void setPoId(String poId) {
        this.poId = poId;
    }

    /**
     * <p>Getter method for sPn.</p>
     *
     * @return the sPn
     */
    public String getSPn() {
        return sPn;
    }

    /**
     * <p>Setter method for sPn.</p>
     *
     * @param sPn Set for sPn
     */
    public void setSPn(String sPn) {
        this.sPn = sPn;
    }

    /**
     * <p>Getter method for dPn.</p>
     *
     * @return the dPn
     */
    public String getDPn() {
        return dPn;
    }

    /**
     * <p>Setter method for dPn.</p>
     *
     * @param dPn Set for dPn
     */
    public void setDPn(String dPn) {
        this.dPn = dPn;
    }

    /**
     * <p>Getter method for periodType.</p>
     *
     * @return the periodType
     */
    public String getPeriodType() {
        return periodType;
    }

    /**
     * <p>Setter method for periodType.</p>
     *
     * @param periodType Set for periodType
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    /**
     * <p>Getter method for periodTypeName.</p>
     *
     * @return the periodTypeName
     */
    public String getPeriodTypeName() {
        return periodTypeName;
    }

    /**
     * <p>Setter method for periodTypeName.</p>
     *
     * @param periodTypeName Set for periodTypeName
     */
    public void setPeriodTypeName(String periodTypeName) {
        this.periodTypeName = periodTypeName;
    }

    /**
     * <p>Getter method for poType.</p>
     *
     * @return the poType
     */
    public String getPoType() {
        return poType;
    }

    /**
     * <p>Setter method for poType.</p>
     *
     * @param poType Set for poType
     */
    public void setPoType(String poType) {
        this.poType = poType;
    }

    /**
     * <p>Getter method for purchaseOrderNotificationList.</p>
     *
     * @return the purchaseOrderNotificationList
     */
    public List<PurchaseOrderNotificationDomain> getPurchaseOrderNotificationList() {
        return purchaseOrderNotificationList;
    }

    /**
     * <p>Setter method for purchaseOrderNotificationList.</p>
     *
     * @param purchaseOrderNotificationList Set for purchaseOrderNotificationList
     */
    public void setPurchaseOrderNotificationList(
        List<PurchaseOrderNotificationDomain> purchaseOrderNotificationList) {
        this.purchaseOrderNotificationList = purchaseOrderNotificationList;
    }

    /**
     * <p>Getter method for spsPoNo.</p>
     *
     * @return the spsPoNo
     */
    public String getSpsPoNo() {
        return spsPoNo;
    }

    /**
     * <p>Setter method for spsPoNo.</p>
     *
     * @param spsPoNo Set for spsPoNo
     */
    public void setSpsPoNo(String spsPoNo) {
        this.spsPoNo = spsPoNo;
    }

    /**
     * <p>Getter method for returnFlag.</p>
     *
     * @return the returnFlag
     */
    public String getReturnFlag() {
        return returnFlag;
    }

    /**
     * <p>Setter method for returnFlag.</p>
     *
     * @param returnFlag Set for returnFlag
     */
    public void setReturnFlag(String returnFlag) {
        this.returnFlag = returnFlag;
    }

    /**
     * <p>Getter method for cannotReturnMessage.</p>
     *
     * @return the cannotReturnMessage
     */
    public String getCannotReturnMessage() {
        return cannotReturnMessage;
    }

    /**
     * <p>Setter method for cannotReturnMessage.</p>
     *
     * @param cannotReturnMessage Set for cannotReturnMessage
     */
    public void setCannotReturnMessage(String cannotReturnMessage) {
        this.cannotReturnMessage = cannotReturnMessage;
    }
    
}
