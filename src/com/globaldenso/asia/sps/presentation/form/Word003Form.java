/*
 * ModifyDate Development company     Describe 
 * 2014/07/02 CSI Phakaporn           Create
 * 2016/02/16 CSI Akat                [IN054]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDetailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WORD003Form.
 * @author CSI
 */
public class Word003Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7183550553430454137L;

    /** The Purchase Order ID. */
    private String poId;

    /** The SPS PO number. */
    private String spsPoNo;

    /** The Period Type. */
    private String periodType;
    
    /** The Period Type Name. */
    private String periodTypeName;

    /** The Purchase Order Type. */
    private String poType;
    
    /** The Supplier Company code. */
    private String sCd;
       
    /** The Denso Company Code. */
    private String dCd;
   
    /** The SPS PO number selected. */
    private String spsPoNoSelected;
    
    /** The CIGMA PO */
    private String cigmaPoNo;
    
    /** The Supplier Plant code. */
    private String sPcd;
    
    /** The Denso Plant code. */
    private String dPcd;
    
    /** The issued date from. */
    private String issuedDateFrom;
    
    /** The issued date to. */
    private String issuedDateTo;
    
    /** The Purchase Order Status. */
    private String poStatus;
    
    /** The View PDF. */
    private String viewPdf;
    
    /** The PDF Original. */
    private String pdfOriginal;
    
    /** The PDF Change. */
    private String pdfChange;
      
    /** The List of PO Status */
    private List<MiscellaneousDomain> poStatusList;
    
    /** The List of PO Type */
    private List<MiscellaneousDomain> poTypeList;
    
    /** The List of Period Type */
    private List<MiscellaneousDomain> periodTypeList;
    
    /** The List of View PDF */
    private List<MiscellaneousDomain> viewPdfList;
    
    /** The List of PO domain. */
    private List<PurchaseOrderDetailDomain> poDetailList;
    
    /** The page type. */
    private String pageType;
    
    /** The Mode. */
    private String mode;
    
    /** The selected supplier part No. (S_PART_NO). */
    private String selectedSPn;
    
    /** The selected denso part No. (D_PART_NO). */
    private String selectedDPn;
    
    /** The selected action mode. */
    private String selectedActionMode;
    
    /** The flag for enable preview pending button. */
    private String enablePreviewPending;
    
    /** Error message if user cannot click Preview Pending. */
    private String cannotPreviewPendingMessage;
    
    // [IN054] add DENSO Reply button
    /** The flag for enable DENSO Reply button. */
    private String enableDensoReply;

    /** The default Constructor */
    public Word003Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        sCd = Constants.EMPTY_STRING;
        dCd = Constants.EMPTY_STRING;
        spsPoNo = Constants.EMPTY_STRING;
        cigmaPoNo = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        dPcd = Constants.EMPTY_STRING;
        issuedDateFrom = Constants.EMPTY_STRING;
        issuedDateTo = Constants.EMPTY_STRING;
        poDetailList = new ArrayList<PurchaseOrderDetailDomain>();
    }
    
    /**
     * <p>Getter method for enablePreviewPending.</p>
     *
     * @return the enablePreviewPending
     */
    public String getEnablePreviewPending() {
        return enablePreviewPending;
    }

    /**
     * <p>Setter method for enablePreviewPending.</p>
     *
     * @param enablePreviewPending Set for enablePreviewPending
     */
    public void setEnablePreviewPending(String enablePreviewPending) {
        this.enablePreviewPending = enablePreviewPending;
    }

    /**
     * Gets the supplier Company code
     * 
     * @return the supplier Company code
     */
    public String getSCd() {
        return sCd;
    }

    /**
     * Sets the supplier Company code
     * 
     * @param sCd the supplier Company code
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }

    /**
     * Gets the denso Company code
     * 
     * @return the denso Company code
     */
    public String getDCd() {
        return dCd;
    }

    /**
     * Sets the denso Company code
     * 
     * @param dCd the denso Company code
     */
    public void setDCd(String dCd) {
        this.dCd = dCd;
    }

    /**
     * Gets the SPS Purchase Order number.
     * 
     * @return the SPS Purchase Order number.
     */
    public String getSpsPoNo() {
        return spsPoNo;
    }
    /**
     * Sets the SPS Purchase Order number.
     * 
     * @param spsPoNo the SPS Purchase Order number.
     */
    public void setSpsPoNo(String spsPoNo) {
        this.spsPoNo = spsPoNo;
    }

    /**
     * Gets the cigma purchase order number.
     * 
     * @return the cigma purchase order number.
     */
    public String getCigmaPoNo() {
        return cigmaPoNo;
    }
    /**
     * Sets the cigma purchase order number.
     * 
     * @param cigmaPoNo the cigma purchase order number.
     */
    public void setCigmaPoNo(String cigmaPoNo) {
        this.cigmaPoNo = cigmaPoNo;
    }

    /**
     * Gets the supplier plant code.
     * 
     * @return the supplier plant code.
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * Sets the supplier plant code.
     * 
     * @param sPcd the supplier plant code.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Gets the denso plant code
     * 
     * @return the denso plant code
     */
    public String getDPcd() {
        return dPcd;
    }
    /**
     * Sets the denso plant code
     * 
     * @param dPcd the denso plant code
     */
    public void setDPcd(String dPcd) {
        this.dPcd = dPcd;
    }

    /**
     * Gets the issue date.  
     * 
     * @return the issue date.  
     */
    public String getIssuedDateFrom() {
        return issuedDateFrom;
    }
    /**
     * Sets the issue date.  
     * 
     * @param issuedDateFrom the issue date.  
     */
    public void setIssuedDateFrom(String issuedDateFrom) {
        this.issuedDateFrom = issuedDateFrom;
    }

    /**
     * Gets the issue date.  
     * 
     * @return the issue date.  
     */
    public String getIssuedDateTo() {
        return issuedDateTo;
    }
    /**
     * Sets the issue date.  
     * 
     * @param issuedDateTo the issue date.  
     */
    public void setIssuedDateTo(String issuedDateTo) {
        this.issuedDateTo = issuedDateTo;
    }

    /**
     * Gets the list of purchase order status combo box.
     * 
     * @return the list of purchase order status combo box.
     */
    public List<MiscellaneousDomain> getPoStatusList() {
        return poStatusList;
    }
    /**
     * Sets the list of purchase order status combo box.
     * 
     * @param poStatusList the list of purchase order status combo box.
     */
    public void setPoStatusList(List<MiscellaneousDomain> poStatusList) {
        this.poStatusList = poStatusList;
    }

    /**
     * Gets the list of Purchase order type combo box.
     * 
     * @return the list of Purchase order type combo box.
     */
    public List<MiscellaneousDomain> getPoTypeList() {
        return poTypeList;
    }
    /**
     * Sets the list of Purchase order type combo box.
     * 
     * @param poTypeList the list of Purchase order type combo box.
     */
    public void setPoTypeList(List<MiscellaneousDomain> poTypeList) {
        this.poTypeList = poTypeList;
    }

    /**
     * Gets the list of period type combo box.
     * 
     * @return the list of period type combo box.
     */
    public List<MiscellaneousDomain> getPeriodTypeList() {
        return periodTypeList;
    }
    /**
     * Sets the list of period type combo box.
     * 
     * @param periodTypeList the list of period type combo box.
     */
    public void setPeriodTypeList(List<MiscellaneousDomain> periodTypeList) {
        this.periodTypeList = periodTypeList;
    }

    /**
     * Gets the list of View PDF combo box.
     * 
     * @return the list of View PDF combo box.
     */
    public List<MiscellaneousDomain> getViewPdfList() {
        return viewPdfList;
    }
    /**
     * Sets the list of View PDF combo box.
     * 
     * @param viewPdfList the list of View PDF combo box.
     */
    public void setViewPdfList(List<MiscellaneousDomain> viewPdfList) {
        this.viewPdfList = viewPdfList;
    }

    /**
     * <p>Getter method for poDetailList.</p>
     *
     * @return the poDetailList
     */
    public List<PurchaseOrderDetailDomain> getPoDetailList() {
        return poDetailList;
    }

    /**
     * <p>Setter method for poDetailList.</p>
     *
     * @param poDetailList Set for poDetailList
     */
    public void setPoDetailList(List<PurchaseOrderDetailDomain> poDetailList) {
        this.poDetailList = poDetailList;
    }

    /**
     * Gets the purchase order status.
     * 
     * @return the purchase order status.
     */
    public String getPoStatus() {
        return poStatus;
    }
    /**
     * Sets the purchase order status.
     * 
     * @param poStatus the purchase order status.
     */
    public void setPoStatus(String poStatus) {
        this.poStatus = poStatus;
    }
    /**
     * Gets the purchase order type.
     * 
     * @return the purchase order type.
     */
    public String getPoType() {
        return poType;
    }
    /**
     * Sets the purchase order type.
     * 
     * @param poType the purchase order type.
     */
    public void setPoType(String poType) {
        this.poType = poType;
    }
    /**
     * Gets the period Type.
     * 
     * @return the period Type.
     */
    public String getPeriodType() {
        return periodType;
    }
    /**
     * Sets the period Type.
     * 
     * @param periodType the period Type.
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }
    /**
     * Gets the View PDF flag.
     * 
     * @return the View PDF flag.
     */
    public String getViewPdf() {
        return viewPdf;
    }
    /**
     * Sets the View PDF flag.
     * 
     * @param viewPdf the View PDF flag.
     */
    public void setViewPdf(String viewPdf) {
        this.viewPdf = viewPdf;
    }
    /**
     * Gets the PDF Original Flag.
     * 
     * @return the PDF Original Flag.
     */
    public String getPdfOriginal() {
        return pdfOriginal;
    }
    /**
     * Sets the PDF Original Flag.
     * 
     * @param pdfOriginal the PDF Original Flag.
     */
    public void setPdfOriginal(String pdfOriginal) {
        this.pdfOriginal = pdfOriginal;
    }
    /**
     * Gets the PDF Change Flag.
     * 
     * @return the PDF Change Flag.
     */
    public String getPdfChange() {
        return pdfChange;
    }
    /**
     * Sets the PDF Change Flag.
     * 
     * @param pdfChange the PDF Change Flag.
     */
    public void setPdfChange(String pdfChange) {
        this.pdfChange = pdfChange;
    }
    /**
     * Gets the Purchase Order ID.
     * 
     * @return the Purchase Order ID.
     */
    public String getPoId() {
        return poId;
    }
    /**
     * Sets the Purchase Order ID.
     * 
     * @param poId the Purchase Order ID.
     */
    public void setPoId(String poId) {
        this.poId = poId;
    }
    /**
     * Gets the Period Type Name.
     * 
     * @return the Period Type Name.
     */
    public String getPeriodTypeName() {
        return periodTypeName;
    }
    /**
     * Sets the Period Type Name.
     * 
     * @param periodTypeName the Period Type Name.
     */
    public void setPeriodTypeName(String periodTypeName) {
        this.periodTypeName = periodTypeName;
    }
    
    /**
     * Gets the page Type.
     * 
     * @return the page Type.
     */
    public String getPageType() {
        return pageType;
    }

    /**
     * Sets the page Type.
     * 
     * @param pageType the page Type.
     */
    public void setPageType(String pageType) {
        this.pageType = pageType;
    }
    
    /**
     * Gets the mode.
     * 
     * @return the mode(View & Edit).
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode(View & Edit).
     */
    public void setMode(String mode) {
        this.mode = mode;
    }
    /**
     * Gets the SPS Purchase Order No. Selected.
     * 
     * @return the SPS Purchase Order No. Selected.
     */
    public String getSpsPoNoSelected() {
        return spsPoNoSelected;
    }

    /**
     * Sets the SPS Purchase Order No. Selected.
     * 
     * @param spsPoNoSelected the SPS Purchase Order No. Selected.
     */
    public void setSpsPoNoSelected(String spsPoNoSelected) {
        this.spsPoNoSelected = spsPoNoSelected;
    }
    
    /**
     * Sets the Purchase Order Detail.
     * 
     * @param purchaseOrderDetailDomain the new Purchase Order Detail.
     */
    public void setPoDetail(PurchaseOrderDetailDomain purchaseOrderDetailDomain) {
        this.poDetailList.add(purchaseOrderDetailDomain);
    }
    
    /**
     * Gets the Purchase Order Detail.
     * 
     * @param index the index
     * @return the Purchase Order Detail.
     */
    public PurchaseOrderDetailDomain getPoDetail(int index) {
        if (poDetailList == null) {
            poDetailList = new ArrayList<PurchaseOrderDetailDomain>();
        }
        while (poDetailList.size() <= index) {
            poDetailList.add(new PurchaseOrderDetailDomain());
        }
        return poDetailList.get(index);
    }

    /**
     * <p>Getter method for selectedSPn.</p>
     *
     * @return the selectedSPn
     */
    public String getSelectedSPn() {
        return selectedSPn;
    }

    /**
     * <p>Setter method for selectedSPn.</p>
     *
     * @param selectedSPn Set for selectedSPn
     */
    public void setSelectedSPn(String selectedSPn) {
        this.selectedSPn = selectedSPn;
    }

    /**
     * <p>Getter method for selectedDPn.</p>
     *
     * @return the selectedDPn
     */
    public String getSelectedDPn() {
        return selectedDPn;
    }

    /**
     * <p>Setter method for selectedDPn.</p>
     *
     * @param selectedDPn Set for selectedDPn
     */
    public void setSelectedDPn(String selectedDPn) {
        this.selectedDPn = selectedDPn;
    }

    /**
     * <p>Getter method for selectedActionMode.</p>
     *
     * @return the selectedActionMode
     */
    public String getSelectedActionMode() {
        return selectedActionMode;
    }

    /**
     * <p>Setter method for selectedActionMode.</p>
     *
     * @param selectedActionMode Set for selectedActionMode
     */
    public void setSelectedActionMode(String selectedActionMode) {
        this.selectedActionMode = selectedActionMode;
    }

    /**
     * <p>Getter method for cannotPreviewPendingMessage.</p>
     *
     * @return the cannotPreviewPendingMessage
     */
    public String getCannotPreviewPendingMessage() {
        return cannotPreviewPendingMessage;
    }

    /**
     * <p>Setter method for cannotPreviewPendingMessage.</p>
     *
     * @param cannotPreviewPendingMessage Set for cannotPreviewPendingMessage
     */
    public void setCannotPreviewPendingMessage(String cannotPreviewPendingMessage) {
        this.cannotPreviewPendingMessage = cannotPreviewPendingMessage;
    }

    // Start : [IN054] add DENSO Reply button
    /**
     * <p>Getter method for enableDensoReply.</p>
     *
     * @return the enableDensoReply
     */
    public String getEnableDensoReply() {
        return enableDensoReply;
    }

    /**
     * <p>Setter method for enableDensoReply.</p>
     *
     * @param enableDensoReply Set for enableDensoReply
     */
    public void setEnableDensoReply(String enableDensoReply) {
        this.enableDensoReply = enableDensoReply;
    }
    // End : [IN054] add DENSO Reply button
}