/*
 * ModifyDate Development company     Describe 
 * 2014/06/07 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation.form;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDetailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * The Class WADM004Form.
 * @author CSI
 */
public class Wadm004Form extends CoreActionForm {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7183550553430454137L;
    
    /** The DSC ID. */
    private String dscId;
        
    /** The Role Code Selected. */
    private String roleCdSelected;
       
    /** The Company Supplier Code. */
    private String sCd;
    
    /** The Company Supplier Code Name. */
    private String supplierCodeName;
   
    /** The Plant Supplier. */
    private String sPcd;
    
    /** The Vendor Code. */
    private String vendorCd;
    
    /** The first name. */
    private String firstName;
    
    /** The middle name.*/
    private String middleName;
    
    /** The last name.*/
    private String lastName;
    
    /** The DENSO owner. */
    private String densoOwner;
    
    /** The Effective start. */
    private String effectStart;
    
    /** The Effective end. */
    private String effectEnd;  
    
    /** The department. */
    private String department; 
    
    /** The role name. */
    private String roleName; 
    
    /** The role code. */
    private String roleCode;
        
    /** The Email. */
    private String email; 
    
    /** The Telephone. */
    private String telephone; 
    
    /** The Assign By. */
    private String assignBy; 
    
    /** The Assign Date. */
    private String assignDate;
    
    /** The Email For. */
    private String emailFor;
    
    /** The Mode. */
    private String mode;
    
    /** The Return Mode. */
    private String returnMode;
    
    /** The User name. */
    private String supplierUserName;
    
    /** The Role Name Selected. */
    private String roleNameSelected;
    
    /** The Company Name. */
    private String companyName;
    
    /** The page type. */
    private String pageType;
    
    /** The count of delete. */
    private String countDelete;
    
    /** The last update date time. */
    private Timestamp lastUpdateDatetime;
    
    /*** Sequence Number */
    private String seqNo;
    
    /** The isDcompany flag. */
    private Boolean isDCompany;
    
    /** The List of User Role Supplier detail. */
    private List<UserRoleDetailDomain> userRoleDetailList;
    
    /** The List of role domain. */
    private List<SpsMRoleDomain> roleList;
    
    /** The List of plant supplier code domain. */
    private List<PlantSupplierDomain> plantSupplierList;
    
    /** The The last update date time for screen. */
    private String lastUpdateDatetimeScreen;

    /** Error message if user cannot click OK in Assign Role popup. */
    private String cannotOkMessage;

    /** Error message if user cannot click Cancel in Assign Role popup. */
    private String cannotCancelMessage;

    /** Error message if user cannot click Update in Assign Role */
    private String cannotUpdateMessage;

    /** Error message if user cannot click Add in Assign Role */
    private String cannotAddMessage;
    
    /** The default Constructor */
    public Wadm004Form() {
    }
    
    /**
     * reset Form.
     */
    public void resetForm(){
        sCd = Constants.EMPTY_STRING;
        sPcd = Constants.EMPTY_STRING;
        email = Constants.EMPTY_STRING;
        emailFor = Constants.EMPTY_STRING;
        assignBy = Constants.EMPTY_STRING;
        densoOwner = Constants.EMPTY_STRING;
        dscId = Constants.EMPTY_STRING;
        telephone = Constants.EMPTY_STRING;
        roleName = Constants.EMPTY_STRING;
        firstName = Constants.EMPTY_STRING;
        middleName = Constants.EMPTY_STRING;
        lastName = Constants.EMPTY_STRING;
        department = Constants.EMPTY_STRING;
        roleCode = Constants.EMPTY_STRING;
        countDelete = Constants.STR_ZERO;
        supplierUserName = Constants.EMPTY_STRING;
        companyName = Constants.EMPTY_STRING;
        supplierCodeName = Constants.EMPTY_STRING;
        userRoleDetailList = new ArrayList<UserRoleDetailDomain>();
        roleList = new ArrayList<SpsMRoleDomain>();
        plantSupplierList = new ArrayList<PlantSupplierDomain>();
    }
    /**
     * Sets the Role Supplier item.
     * 
     * @param items the new Role Supplier item
     */
    public void setRoleSupplier(UserRoleDetailDomain items) {
        this.userRoleDetailList.add(items);
    }
    
    /**
     * Gets the Role Supplier item.
     * 
     * @param index the index
     * @return the Role Supplier item.
     */
    public UserRoleDetailDomain getRoleSupplier(int index){
        if (userRoleDetailList == null) {
            userRoleDetailList = new ArrayList<UserRoleDetailDomain>();
        }
        while (userRoleDetailList.size() <= index) {
            userRoleDetailList.add(new UserRoleDetailDomain());
        }
        return userRoleDetailList.get(index);
    }
    /**
     * Gets the Company Supplier Code.
     * 
     * @return the Company Supplier Code.
     */
    public String getSCd() {
        return sCd;
    }
    /**
     * Sets the Company Supplier Code.
     * 
     * @param sCd the Company Supplier Code.
     */
    public void setSCd(String sCd) {
        this.sCd = sCd;
    }
    /**
     * Gets the plant supplier.
     * 
     * @return the plant supplier.
     */
    public String getSPcd() {
        return sPcd;
    }
    /**
     * Sets the plant supplier.
     * 
     * @param sPcd the plant supplier.
     */
    public void setSPcd(String sPcd) {
        this.sPcd = sPcd;
    }

    /**
     * Gets the mode.
     * 
     * @return the mode(Register & Edit).
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets the mode.
     * 
     * @param mode the mode(Register & Edit).
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Gets the DSC ID
     * 
     * @return the DSC ID
     */
    public String getDscId() {
        return dscId;
    }

    /**
     * Sets the DSC ID
     * 
     * @param dscId the DSC ID
     */
    public void setDscId(String dscId) {
        this.dscId = dscId;
    }

    /**
     * Gets the First Name.
     * 
     * @return the First Name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the First Name.
     * 
     * @param firstName the First Name.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the Last Name.
     * 
     * @return the Last Name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the Last Name.
     * 
     * @param lastName the Last Name.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the list of user role supplier.
     * 
     * @return the list of user role supplier.
     */
    public List<UserRoleDetailDomain> getUserRoleDetailList() {
        return userRoleDetailList;
    }

    /**
     * Sets the list of user role supplier.
     * 
     * @param userRoleDetailList the list of user role supplier.
     */
    public void setUserRoleDetailList(List<UserRoleDetailDomain> userRoleDetailList) {
        this.userRoleDetailList = userRoleDetailList;
    }

    /**
     * Gets the Department.
     * 
     * @return the Department.
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Sets the Department.
     * 
     * @param department the Department.
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * Gets the email.
     * 
     * @return the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     * 
     * @param email the email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the Telephone.
     * 
     * @return the Telephone.
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Sets the Telephone.
     * 
     * @param telephone the Telephone.
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    
    /**
     * Gets the Middle Name.
     * 
     * @return the Middle Name.
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the Middle Name.
     * 
     * @param middleName the Middle Name.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Gets the DENSO Owner.
     * 
     * @return the DENSO Owner.
     */
    public String getDensoOwner() {
        return densoOwner;
    }

    /**
     * Sets the DENSO Owner.
     * 
     * @param densoOwner the DENSO Owner.
     */
    public void setDensoOwner(String densoOwner) {
        this.densoOwner = densoOwner;
    }

    /**
     * Gets the Role Name.
     * 
     * @return the Role Name.
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * Sets the Role Name.
     * 
     * @param roleName the Role Name.
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * Gets the Role Code.
     * 
     * @return the Role Code.
     */
    public String getRoleCode() {
        return roleCode;
    }

    /**
     * Sets the Role Code.
     * 
     * @param roleCode the Role Code.
     */
    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    /**
     * Gets the Assign By.
     * 
     * @return the Assign By.
     */
    public String getAssignBy() {
        return assignBy;
    }

    /**
     * Sets the Assign By.
     * 
     * @param assignBy the Assign By.
     */
    public void setAssignBy(String assignBy) {
        this.assignBy = assignBy;
    }

    /**
     * Gets the Assign Date.
     * 
     * @return the Assign Date.
     */
    public String getAssignDate() {
        return assignDate;
    }

    /**
     * Sets the Assign Date.
     * 
     * @param assignDate the Assign Date.
     */
    public void setAssignDate(String assignDate) {
        this.assignDate = assignDate;
    }

    /**
     * Gets the Email For.
     * 
     * @return the Email For.
     */
    public String getEmailFor() {
        return emailFor;
    }

    /**
     * Sets the Email For.
     * 
     * @param emailFor the Email For.
     */
    public void setEmailFor(String emailFor) {
        this.emailFor = emailFor;
    }

    /**
     * Gets the User name.
     * 
     * @return the User name.
     */
    public String getSupplierUserName() {
        return supplierUserName;
    }

    /**
     * Sets the User name.
     * 
     * @param supplierUserName the User name.
     */
    public void setSupplierUserName(String supplierUserName) {
        this.supplierUserName = supplierUserName;
    }

    /**
     * Gets the page Type.
     * 
     * @return the page Type.
     */
    public String getPageType() {
        return pageType;
    }

    /**
     * Sets the page Type.
     * 
     * @param pageType the page Type.
     */
    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    /**
     * Gets the Role Name Selected.
     * 
     * @return the Role Name Selected.
     */
    public String getRoleNameSelected() {
        return roleNameSelected;
    }

    /**
     * Sets the Role Name Selected.
     * 
     * @param roleNameSelected the Role Name Selected.
     */
    public void setRoleNameSelected(String roleNameSelected) {
        this.roleNameSelected = roleNameSelected;
    }

    /**
     * Gets the effect Start date on Screen. 
     * 
     * @return the effect Start date on Screen. 
     */
    public String getEffectStart() {
        return effectStart;
    }

    /**
     * Sets the effect Start date on Screen. 
     * 
     * @param effectStart the effect Start date on Screen. 
     */
    public void setEffectStart(String effectStart) {
        this.effectStart = effectStart;
    }

    /**
     * Gets the effect End date on Screen. 
     * 
     * @return the effect End date on Screen. 
     */
    public String getEffectEnd() {
        return effectEnd;
    }

    /**
     * Sets the effect End date on Screen. 
     * 
     * @param effectEnd the effect End date on Screen. 
     */
    public void setEffectEnd(String effectEnd) {
        this.effectEnd = effectEnd;
    }
    
    /**
     * Gets the count of delete.
     * 
     * @return the count of delete.
     */
    public String getCountDelete() {
        return countDelete;
    }

    /**
     * Sets the count of delete.
     * 
     * @param countDelete the count of delete.
     */
    public void setCountDelete(String countDelete) {
        this.countDelete = countDelete;
    }
    /**
     * Gets the list of role domain.
     * 
     * @return the list of role domain.
     */
    public List<SpsMRoleDomain> getRoleList() {
        return roleList;
    }
    /**
     * Sets the list of role domain.
     * 
     * @param roleList the list of role domain.
     */
    public void setRoleList(List<SpsMRoleDomain> roleList) {
        this.roleList = roleList;
    }
    /**
     * Gets the list of plant supplier.
     * 
     * @return the list of plant supplier.
     */
    public List<PlantSupplierDomain> getPlantSupplierList() {
        return plantSupplierList;
    }
    /**
     * Sets the list of plant supplier.
     * 
     * @param plantSupplierList the list of plant supplier.
     */
    public void setPlantSupplierList(List<PlantSupplierDomain> plantSupplierList) {
        this.plantSupplierList = plantSupplierList;
    }
    /**
     * Gets the role code selected.
     * 
     * @return the role code selected.
     */
    public String getRoleCdSelected() {
        return roleCdSelected;
    }
    /**
     * Sets the role code selected.
     * 
     * @param roleCdSelected the role code selected.
     */
    public void setRoleCdSelected(String roleCdSelected) {
        this.roleCdSelected = roleCdSelected;
    }
    
    /**
     * Getter method of "lastUpdateDatetime"
     * 
     * @return the lastUpdateDatetime
     */
    public Timestamp getLastUpdateDatetime() {
        return lastUpdateDatetime;
    }

    /**
     * Setter method of "lastUpdateDatetime"
     * 
     * @param lastUpdateDatetime Set in "lastUpdateDatetime".
     */
    public void setLastUpdateDatetime(Timestamp lastUpdateDatetime) {
        this.lastUpdateDatetime = lastUpdateDatetime;
    }
    /**
     * Getter last update date time for screen.
     * 
     * @return the last update date time for screen.
     */
    public String getLastUpdateDatetimeScreen() {
        return lastUpdateDatetimeScreen;
    }
    /**
     * Setter last update date time for screen.
     * 
     * @param lastUpdateDatetimeScreen Set last update date time for screen.
     */
    public void setLastUpdateDatetimeScreen(String lastUpdateDatetimeScreen) {
        this.lastUpdateDatetimeScreen = lastUpdateDatetimeScreen;
    }
    /**
     * Getter return mode.
     * 
     * @return the return mode.
     */
    public String getReturnMode() {
        return returnMode;
    }
    /**
     * Setter return mode.
     * 
     * @param returnMode Set the return mode.
     */
    public void setReturnMode(String returnMode) {
        this.returnMode = returnMode;
    }
    /**
     * Gets the Vendor Code.
     * 
     * @return the Vendor Code.
     */
    public String getVendorCd() {
        return vendorCd;
    }
    /**
     * Sets the Vendor Code.
     * 
     * @param vendorCd the Vendor Code.
     */
    public void setVendorCd(String vendorCd) {
        this.vendorCd = vendorCd;
    }
    
    /**
     * Get method for seqNo.
     * @return the seqNo
     */
    public String getSeqNo() {
        return seqNo;
    }

    /**
     * Set method for seqNo.
     * @param seqNo the seqNo to set
     */
    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }
    /**
     * Get method for companyName.
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }
    /**
     * Set method for companyName.
     * @param companyName the company Name
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    /**
     * Gets the isDcompany flag.
     * 
     * @return the isDcompany flag.
     */
    public Boolean getIsDCompany() {
        return isDCompany;
    }
    /**
     * Sets the isDcompany flag.
     * 
     * @param isDCompany the isDcompany flag.
     */
    public void setIsDCompany(Boolean isDCompany) {
        this.isDCompany = isDCompany;
    }
    /**
     * Gets the supplier code name.
     * 
     * @return the supplier code name.
     */
    public String getSupplierCodeName() {
        return supplierCodeName;
    }
    /**
     * Sets the supplier code name.
     * 
     * @param supplierCodeName the supplier code name.
     */
    public void setSupplierCodeName(String supplierCodeName) {
        this.supplierCodeName = supplierCodeName;
    }

    /**
     * <p>Getter method for cannotOkMessage.</p>
     *
     * @return the cannotOkMessage
     */
    public String getCannotOkMessage() {
        return cannotOkMessage;
    }

    /**
     * <p>Setter method for cannotOkMessage.</p>
     *
     * @param cannotOkMessage Set for cannotOkMessage
     */
    public void setCannotOkMessage(String cannotOkMessage) {
        this.cannotOkMessage = cannotOkMessage;
    }

    /**
     * <p>Getter method for cannotCancelMessage.</p>
     *
     * @return the cannotCancelMessage
     */
    public String getCannotCancelMessage() {
        return cannotCancelMessage;
    }

    /**
     * <p>Setter method for cannotCancelMessage.</p>
     *
     * @param cannotCancelMessage Set for cannotCancelMessage
     */
    public void setCannotCancelMessage(String cannotCancelMessage) {
        this.cannotCancelMessage = cannotCancelMessage;
    }

    /**
     * <p>Getter method for cannotUpdateMessage.</p>
     *
     * @return the cannotUpdateMessage
     */
    public String getCannotUpdateMessage() {
        return cannotUpdateMessage;
    }

    /**
     * <p>Setter method for cannotUpdateMessage.</p>
     *
     * @param cannotUpdateMessage Set for cannotUpdateMessage
     */
    public void setCannotUpdateMessage(String cannotUpdateMessage) {
        this.cannotUpdateMessage = cannotUpdateMessage;
    }

    /**
     * <p>Getter method for cannotAddMessage.</p>
     *
     * @return the cannotAddMessage
     */
    public String getCannotAddMessage() {
        return cannotAddMessage;
    }

    /**
     * <p>Setter method for cannotAddMessage.</p>
     *
     * @param cannotAddMessage Set for cannotAddMessage
     */
    public void setCannotAddMessage(String cannotAddMessage) {
        this.cannotAddMessage = cannotAddMessage;
    }
    
}