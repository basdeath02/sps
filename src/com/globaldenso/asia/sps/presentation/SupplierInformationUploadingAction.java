/*
 * ModifyDate Development company     Describe 
 * 2014/08/24 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Word001Form;
import com.globaldenso.asia.sps.business.service.SupplierInformationUploadingFacadeService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.SupplierInfoUploadDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;


/**
 * The Class SupplierInformationUploadingAction.
 * <p>
 * Receive value from SupplierInformationUploadingAction for initial value and send to 
 * supplierInformationUploadingFacadeServiceImpl for manipulate data.
 * </p>
 * <ul>
 * <li>Method initial : doInitial</li>
 * </ul>                                 
 * 
 * @author CSI
 */
public class SupplierInformationUploadingAction extends CoreAction {


    /** The Supplier Information Uploading Facade service. */
    private SupplierInformationUploadingFacadeService supplierInformationUploadingFacadeService;
    /**
     * Instantiates a new SupplierUserUploadingAction.
     */
    public SupplierInformationUploadingAction() {
        super();
    }
    
    /**
     * Sets the Supplier Information Uploading Facade Service.
     * 
     * @param supplierInformationUploadingFacadeService the Supplier Information Uploading 
     * Facade Service.
     */
    public void setSupplierInformationUploadingFacadeService(
        SupplierInformationUploadingFacadeService supplierInformationUploadingFacadeService) {
        this.supplierInformationUploadingFacadeService = supplierInformationUploadingFacadeService;
    }

    /**
     * Do initial.
     * <p>Initial supplier user uploading screen when load page.</p>
     * <ul>
     * <li>In case click MainMenu (Supplier UserInformation) link, Reset button</li>
     * </ul>
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Word001Form form = (Word001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        
        try{
            /*Call Facade Service to clear upload Supplier information in temporary table*/
            /*Set value to argDomain.*/
            SupplierInfoUploadDomain criteria = new SupplierInfoUploadDomain();
            criteria.setUserDscId(null);
            criteria.setSessionCode(null);
            supplierInformationUploadingFacadeService.transactInitial(criteria);
            
            /*Set value of domain to ActionForm*/
            form.resetForm();

            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD001_BRESET)) 
                {
                    this.supplierInformationUploadingFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD001_BRESET, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotResetMessage(applicationException.getMessage());
            }
            
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD001_BEXPORT)) 
                {
                    this.supplierInformationUploadingFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD001_BEXPORT, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotExportMessage(applicationException.getMessage());
            }
            
        }catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
        }
        
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * Do upload
     * <p>validate CSV file that uploaded.</p>
     * <ul>
     * <li>In case click Upload button.</li>
     * </ul>
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward  doUpload(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Word001Form form = (Word001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String firstPages = form.getFirstPages();
        String pages = form.getPages();
        HttpSession session = request.getSession();
        String sessionCode = session.getId();
        String message = new String();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        List<GroupUploadErrorDomain> uploadErrorDetailList = 
            new ArrayList<GroupUploadErrorDomain>();
        SupplierInfoUploadDomain resultDomain = new SupplierInfoUploadDomain();
        SupplierInfoUploadDomain criteria = new SupplierInfoUploadDomain();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        String userDscId = userLoginDomain.getDscId();
        FormFile fileData = form.getFileData();
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD001_BUPLOAD)) 
                {
                    this.supplierInformationUploadingFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD001_BUPLOAD, locale);
                }
                
                if(userLoginDomain.getUserType().equals(SupplierPortalConstant.USER_TYPE_DENSO)){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E9_0003,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
                if(Constants.PAGE_SEARCH.equals(firstPages)){
                    criteria.setPageNumber(Constants.ONE);
                }else{
                    criteria.setPageNumber(form.getPageNo());
                }
                /*Validate do not choose file*/
                if(StringUtil.checkNullOrEmpty(form.getFileData().getFileName())){
                    MessageUtil.throwsApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_W6_0006, 
                        SupplierPortalConstant.LBL_UPLOAD_FILE);
                }
                
                /*Validate file size*/
                if(Constants.ZERO == form.getFileData().getFileSize()){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0014, 
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }

                int maxCSVFileSize = ContextParams.getMaxCsvFileSize();
                if(maxCSVFileSize < form.getFileData().getFileSize()){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0015, 
                        SupplierPortalConstant.ERROR_CD_SP_90_0002, 
                        new String[]{String.valueOf(maxCSVFileSize)});
                }

                String fileName = form.getFileData().getFileName();
                if(!Constants.REPORT_CSV_FORMAT.equalsIgnoreCase(
                    fileName.substring(fileName.lastIndexOf(Constants.SYMBOL_DOT)
                        + Constants.ONE, fileName.length()).toLowerCase())){
                    MessageUtil.throwsApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0013, 
                        SupplierPortalConstant.LBL_CSV);
                }

                /*Delete temporary table data*/
                criteria.setUserDscId(userDscId);
                criteria.setSessionCode(sessionCode);
                supplierInformationUploadingFacadeService
                    .deleteUploadTempTable(userDscId, sessionCode);

                /*Call Facade Service to upload supplier user information to system.*/
                FileManagementDomain fileManagementDomain = new FileManagementDomain();
                fileManagementDomain.setFileData(form.getFileData().getInputStream());
                fileManagementDomain.setFileName(form.getFileData().getFileName());

                DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
                dataScopeControlDomain.setLocale(locale);
                dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
                dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain
                    .getUserRoleDomainList());
                dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
                /*Set value to argDomain.*/
                criteria.setFileManagementDomain(fileManagementDomain);
                criteria.setUserDscId(userDscId);
                criteria.setSessionCode(sessionCode);
                criteria.setDataScopeControlDomain(dataScopeControlDomain);
                criteria.setCompanySupplierOwner(userLoginDomain.getUserSupplierDetailDomain()
                    .getSpsMUserSupplierDomain().getSCd());

                resultDomain = supplierInformationUploadingFacadeService
                    .transactUploadSupplierInformation(criteria);
                uploadErrorDetailList = resultDomain.getUploadErrorDetailDomain();
                /*Set value of domain to ActionForm*/
                if(null == resultDomain.getErrorMessageList() 
                    ||  resultDomain.getErrorMessageList().size() <= Constants.ZERO){
                    form.setTotalRecord(resultDomain.getTotalRecord());
                    form.setCorrectRecord(resultDomain.getCorrectRecord());
                    form.setWarningRecord(resultDomain.getWarningRecord());
                    form.setIncorrectRecord(resultDomain.getInCorrectRecord());
                    form.setUploadErrorDetailDomain(uploadErrorDetailList);
                    if(null != resultDomain.getCompanyName()){
                        form.setCompanySupplier(resultDomain.getCompanyName());
                    }
                    if(null != resultDomain.getPlantSupplierList()){
                        form.setPlantSupplierList(resultDomain.getPlantSupplierList());
                        form.setMaxPlantSupplierList(resultDomain.getPlantSupplierList().size());
                    }
                    SpsPagingUtil.setFormForPaging(resultDomain, form);
                    /*set message for upload success.*/
                    message = MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_I6_0010);
                    applicationMessageList.add(new ApplicationMessageDomain(
                        Constants.MESSAGE_SUCCESS, message));
                }
            } catch (ApplicationException applicationException) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
            } catch (Exception e) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            } finally {
                form.setApplicationMessageList(applicationMessageList);
                form.getFileData().destroy();
                form.setFileName(form.getFileData().getFileName());
                form.setFileData(fileData);
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Register.
     * <p>Register data of CSV file uploaded.</p>
     * <ul>
     * <li>In case click Register button.</li>
     * </ul>
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doRegister(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Word001Form form = (Word001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        HttpSession session = request.getSession();
        String sessionCode = session.getId();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        String userDscId = userLoginDomain.getDscId();
        try{
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD001_BREGISTER)) 
            {
                this.supplierInformationUploadingFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD001_BREGISTER, locale);
            }
            
            /*Call Facade to move upload data from temporary to actual table*/
            SupplierInfoUploadDomain criteria = new SupplierInfoUploadDomain();
            /*Set value to argDomain.*/
            criteria.setUserDscId(userDscId);
            criteria.setSessionCode(sessionCode);
            supplierInformationUploadingFacadeService.transactRegisterSupplierInformation(criteria);
            
            /*Delete temporary table data*/
            criteria.setUserDscId(userDscId);
            criteria.setSessionCode(sessionCode);
            supplierInformationUploadingFacadeService.transactInitial(criteria);
            
            /*Delete temporary table with current date -2 */
            SupplierInfoUploadDomain newCriteria = new SupplierInfoUploadDomain();
            supplierInformationUploadingFacadeService.transactInitial(newCriteria);
            
            /*Set value of domain to ActionForm*/
            form.resetForm();
            form.setPlantSupplierList(null);
            
            /*Set import success massage*/
            String message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale,
                SupplierPortalConstant.ERROR_CD_SP_I6_0004);
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_SUCCESS,
                message));
            
        }catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
        }
        
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Paging.
     * <p>search data when load page.</p>
     * <ul>
     * <li>In case click Paging (Link).</li>
     * </ul>
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doPaging(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Word001Form form = (Word001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        HttpSession session = request.getSession();
        String sessionCode = session.getId();
        String firstPages = form.getFirstPages();
        String pages = form.getPages();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        List<GroupUploadErrorDomain> resultList = new ArrayList<GroupUploadErrorDomain>();
        SupplierInfoUploadDomain resultDomain = new SupplierInfoUploadDomain();
        SupplierInfoUploadDomain criteria = new SupplierInfoUploadDomain();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        String userDscId = userLoginDomain.getDscId();
        
        if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
            try{
                if(Constants.PAGE_SEARCH.equals(firstPages)){
                    criteria.setPageNumber(Constants.ONE);
                }else{
                    criteria.setPageNumber(form.getPageNo());
                }
                /*Call Facade Service to get summary error from upload to show on screen.*/

                /*Set value to argDomain.*/
                criteria.setUserDscId(userDscId);
                criteria.setSessionCode(sessionCode);
                resultDomain = supplierInformationUploadingFacadeService.searchUploadErrorList(
                    criteria);
                resultList = resultDomain.getUploadErrorDetailDomain();
                if(null != resultList){
                    form.setTotalRecord(resultDomain.getTotalRecord());
                    form.setCorrectRecord(resultDomain.getCorrectRecord());
                    form.setWarningRecord(resultDomain.getWarningRecord());
                    form.setIncorrectRecord(resultDomain.getInCorrectRecord());
                    form.setPlantSupplier(null);
                    form.setPlantSupplierList(resultDomain.getPlantSupplierList());
                    form.setMaxPlantSupplierList(resultDomain.getPlantSupplierList().size());
                    SpsPagingUtil.setFormForPaging(resultDomain, form);
                    form.setUploadErrorDetailDomain(resultList);
                }
            }catch (ApplicationException applicationException) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
            } catch (Exception e) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }finally {
                form.setApplicationMessageList(applicationMessageList);
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Download.
     * 
     * <p>
     * download data that meet criteria values.
     * </p>
     * 
     * <li>In case click download button, system will load data that meet to criteria 
     * and export all data to CSV file.</li>
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doExport(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception{
        
        Word001Form form = (Word001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String message = new String();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        SupplierInfoUploadDomain supplierInfoUploadDomain = 
            new SupplierInfoUploadDomain();
        Calendar cal = Calendar.getInstance();
        String fileName = StringUtil.appendsString(
            Constants.SUPPLIER_INFO_LIST_FILE_NAME,
            Constants.SYMBOL_UNDER_SCORE,
            DateUtil.format(cal.getTime(), DateUtil.PATTERN_YYYYMMDD_HHMM));
        ServletOutputStream outputStream = null;
        HttpSession session = request.getSession();
        String sessionCode = session.getId();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        String userDscId = userLoginDomain.getDscId();
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        supplierInfoUploadDomain.setLocale(locale);
        supplierInfoUploadDomain.setSessionCode(sessionCode);
        supplierInfoUploadDomain.setUserDscId(userDscId);
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        supplierInfoUploadDomain.setDataScopeControlDomain(dataScopeControlDomain);
        try{
            supplierInfoUploadDomain = supplierInformationUploadingFacadeService
                .searchSupplierInformationCsv(supplierInfoUploadDomain);
            StringBuffer resultStr = supplierInfoUploadDomain.getResultString();
            if(null != resultStr ){
                byte[] output = resultStr.toString().getBytes();
                setHttpHeaderForCsv(response, fileName);
                outputStream = response.getOutputStream();
                outputStream.write(output, Constants.ZERO, output.length);
                outputStream.flush();
                message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                    SupplierPortalConstant.ERROR_CD_SP_I6_0005);
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_SUCCESS,
                    message));
                return null;
            }else{
                errorMessageList = supplierInfoUploadDomain.getErrorMessageList();
            }
            
        }catch (ApplicationException applicationException) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        }catch (Exception e) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(null != outputStream){
                outputStream.close();
            }
            form.setApplicationMessageList(errorMessageList);
        }
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**
     * Gets the key method map.
     * 
     * @return Map
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put(SupplierPortalConstant.ACTION_RESET, SupplierPortalConstant.METHOD_DO_INITIAL);
        map.put(SupplierPortalConstant.ACTION_UPLOAD, SupplierPortalConstant.METHOD_DO_UPLOAD);
        map.put(SupplierPortalConstant.ACTION_REGISTER, SupplierPortalConstant.METHOD_DO_REGISTER);
        map.put(SupplierPortalConstant.ACTION_EXPORT, SupplierPortalConstant.METHOD_DO_EXPORT);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_PAGING,
            SupplierPortalConstant.METHOD_DO_PAGING);
        return map;
    }
    
    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return the UserLoginDomain.
     */
    private UserLoginDomain setUserLoginInformation() {
        
        UserLoginDomain userLoginDomain = (UserLoginDomain)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }
    
    /**
     * Get roles for supplier user.
     * <p>Get roles  from role screen of user login.</p>
     * 
     * @param userLoginDomain the UserLoginDomain
     * @return the RoleScreenDomain
     */
    private RoleScreenDomain setRoleScreen(UserLoginDomain userLoginDomain) {
        List<RoleScreenDomain> roleScreenList = userLoginDomain.getRoleScreenDomainList();
        for(RoleScreenDomain roleScreen : roleScreenList){
            if(SupplierPortalConstant
                .SCREEN_ID_WORD001.equals(roleScreen.getSpsMScreenDomain().getScreenCd())){
                return roleScreen;
            }
        }
        return null;
    }
 
}