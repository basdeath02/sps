/*
 * ModifyDate Development company     Describe 
 * 2014/07/31 CSI Chatchai            Create
 * 2015/12/25 CSI Akat                [ADDITIONAL]
 * 2015/08/22 Netband U.Rungsiwut     SPS phase II
 * 2018/04/11 Netband U.Rungsiwut     Generate D/O PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.InputStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationResultMasterDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.KanbanOrderInformationFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Word007Form;

/**
 * <p>
 * Kanban Order Information Action class.
 * </p>
 * 
 * @author CSI
 * @version 1.00
 */
public class KanbanOrderInformationAction extends CoreAction {

    /**
     * <p>
     * Call Facade Service KanbanOrderInformation.
     * </p>
     */
    private KanbanOrderInformationFacadeService kanbanOrderInformationFacadeService;

    /** The default Constructor */
    public KanbanOrderInformationAction() {
        super();
    }

    /**
     * <p>
     * Setter method for kanbanOrderInformationFacadeService.
     * </p>
     * 
     * @param kanbanOrderInformationFacadeService Set for
     *            kanbanOrderInformationFacadeService
     */
    public void setKanbanOrderInformationFacadeService(
        KanbanOrderInformationFacadeService kanbanOrderInformationFacadeService) {
        this.kanbanOrderInformationFacadeService = kanbanOrderInformationFacadeService;
    }

    /**
     * <p>
     * Initial method.
     * </p>
     * 
     * @param actionMapping actionMapping
     * @param actionForm actionForm
     * @param request request
     * @param response response
     * @return ActionForward
     * @throws Exception Exception
     */
    public ActionForward doInitial(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Word007Form form = (Word007Form)actionForm;
        form.resetForm();
        
        /** Call private action method to get current user login and roles from DENSO-context. */
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        
        UserLoginDomain userLoginDomain = this.getLoginUser();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        DataScopeControlDomain dataScopeControlDomain
            = this.getDataScopeControlForUserLogin(userLoginDomain);
        dataScopeControlDomain.setLocale(locale);

        KanbanOrderInformationDomain kanbanOrderInformationDomain = 
            new KanbanOrderInformationDomain();
        
        form.setUserLogin(setUserLogin());

        /** Call Façade Service KANBANOrderInformationFacadeService searchInitial(). */
        try {
            kanbanOrderInformationDomain = kanbanOrderInformationFacadeService
                .searchInitial(dataScopeControlDomain);

            /** Set return value of previous doInitial to ActionForm */
            Date date = new Date(DateUtil.getDateTime());
            Date delissueDateFrom = DateUtil.addDays(date, Constants.MINUS_ONE);
            
            String addissueDateFromFormat = DateUtil.format(delissueDateFrom, 
                DateUtil.PATTERN_YYYYMMDD_SLASH);
            String issueDateFromFormat = DateUtil.format(date, 
                DateUtil.PATTERN_YYYYMMDD_SLASH);

            form.setIssueDateFrom(addissueDateFromFormat);
            form.setIssueDateTo(issueDateFromFormat);
            form.setDeliveryDateFrom(null);
            form.setDeliveryDateTo(null);
            form.setDeliveryTimeFrom(null);
            form.setDeliveryTimeTo(null);
            form.setShipDateFrom(null);
            form.setShipDateTo(null);
            form.setRouteNo(null);
            form.setDel(null);
            form.setSpsDoNo(null);
            form.setCigmaDoNo(null);

            // [ADDITIONAL] : Mr. Sittichai request to set default Revision combobox to 'Latest'
            form.setRevision(Constants.STR_ONE);

            form.setCompanySupplierList(kanbanOrderInformationDomain.getCompanySupplierList());
            form.setCompanyDensoList(kanbanOrderInformationDomain.getCompanyDensoList());
            form.setPlantSupplierList(kanbanOrderInformationDomain.getPlantSupplierList());
            form.setPlantDensoList(kanbanOrderInformationDomain.getPlantDensoList());
            form.setRevisionList(kanbanOrderInformationDomain.getRevisionList());
            form.setShipmentStatusList(kanbanOrderInformationDomain.getShipmentStatusList());
            form.setTransportModeList(kanbanOrderInformationDomain.getTransportModeList());

            form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
                kanbanOrderInformationDomain.getDensoSupplierRelationList(), 
                SupplierPortalConstant.SCREEN_ID_WORD007));
            form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
                kanbanOrderInformationDomain.getDensoSupplierRelationList(), 
                SupplierPortalConstant.SCREEN_ID_WORD007));
            
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD007_BRESET)) 
                {
                    this.kanbanOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD007_BRESET, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotResetMessage(applicationException.getMessage());
            }

            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD007_BDOWNLOAD)) 
                {
                    this.kanbanOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD007_BDOWNLOAD, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotDownloadCsvMessage(applicationException.getMessage());
            }
            
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil
                .getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{e.getClass().toString(), e.getMessage()});
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            form.setApplicationMessageList(errorMessageList);
        }

        return actionMapping.findForward(Constants.ACTION_FWD_INITIAL);
    }

    /**
     * <p>
     * Search method.
     * </p>
     * 
     * @param actionMapping actionMapping
     * @param actionForm actionForm
     * @param request request
     * @param response response
     * @return List
     * @throws Exception Exception
     */
    public ActionForward doSearch(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Word007Form form = (Word007Form)actionForm;
        KanbanOrderInformationDomain criteria = new KanbanOrderInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);

        UserLoginDomain userLoginDomain = this.getLoginUser();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        DataScopeControlDomain dataScopeControlDomain
            = this.getDataScopeControlForUserLogin(userLoginDomain);
        dataScopeControlDomain.setLocale(locale);
        
        form.setUserLogin(setUserLogin());

        try {
            form.setMode(SupplierPortalConstant.METHOD_DO_SEARCH);

            String firstPages = form.getFirstPages();
            String pages = form.getPages();
            
            /** Call Façade Service KANBANOrderInformationFacadeService searchKANBANOrderInformation(). */
            if (Constants.PAGE_SEARCH.equals(firstPages)
                || Constants.PAGE_CLICK.equals(pages)) {
                
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WORD007_BSEARCH)) 
                {
                    this.kanbanOrderInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WORD007_BSEARCH, locale);
                }
                
                setParameterCriteria(form, firstPages, criteria);
                criteria.setLocale(locale);
                
                KanbanOrderInformationResultMasterDomain kanbanOrderInformationResultMasterDomain = 
                    new KanbanOrderInformationResultMasterDomain();
                kanbanOrderInformationResultMasterDomain = kanbanOrderInformationFacadeService
                    .searchKanbanOrderInformation(criteria);
                
                if (null != kanbanOrderInformationResultMasterDomain.getErrorMessageList()) {
                    errorMessageList
                        = kanbanOrderInformationResultMasterDomain.getErrorMessageList();
                } else {
                    form.setKanbanInformation(kanbanOrderInformationResultMasterDomain
                        .getKanbanOrderInformationResultDomain());
                    SpsPagingUtil.setFormForPaging(criteria, form);
                }
            }
        
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_90_0002,
                new String[]{e.getClass().toString(), e.getMessage()});
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
        } finally {
            form.setApplicationMessageList(errorMessageList);

            Set<String> applicationMessageSet = new LinkedHashSet<String>();
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * <p>
     * Select SCd method.
     * </p>
     * 
     * @param actionMapping the actionMapping
     * @param actionForm the actionForm
     * @param request the request
     * @param response the response
     * @return the ActionForward
     * @throws Exception the Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSCd(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception
    {
        Word007Form form = (Word007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        
        /** Call private action method to get current user login information and roles from DENSO-context. */
        UserLoginDomain userLoginDomain = this.getLoginUser();
        DataScopeControlDomain dataScopeControlDomain
            = this.getDataScopeControlForUserLogin(userLoginDomain);
        dataScopeControlDomain.setLocale(locale);

        /** Call Façade Service KANBANOrderInformationFacadeService changeSelectSupplierCompany() to get the list of supplier plant code by supplier company code. */
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        plantSupplierWithScopeDomain.getPlantSupplierDomain()
            .setVendorCd(form.getVendorCd());
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierWithScopeDomain.setLocale(locale);

        List<PlantSupplierDomain> plantSupplierDomain = new ArrayList<PlantSupplierDomain>();
        form.setUserLogin(setUserLogin());
        
        try {
            plantSupplierDomain = kanbanOrderInformationFacadeService
                .searchSelectedCompanySupplier(plantSupplierWithScopeDomain);

            printJson(response, new JsonResult(
                (List<Object>)(Object)plantSupplierDomain), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        
        return null;
    }

    /**
     * Search Supplier Company Code by selected Supplier Plant Code filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word007Form form = (Word007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanySupplierDomain> companyList = null;
        
        // 1. Call private action method to get current user login and roles from DENSO-context
        UserLoginDomain userLogin = this.getLoginUser();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        // 2. Get roles for supplier user
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        dataScopeControl.setLocale(locale);

        try{
            /* 3. Call Façade Service PurchaseOrderInformationFacadeService
             * searchCompanyBySelectSupplierPlant() to get the list of supplier company code by
             * supplier plant code
             * */
            PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getSPcd())) {
                plantSupplier.getPlantSupplierDomain().setSPcd(form.getSPcd());
            }
            plantSupplier.setDataScopeControlDomain(dataScopeControl);
            companyList = this.kanbanOrderInformationFacadeService.searchSelectedPlantSupplier(
                plantSupplier);

            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }

        return null;
    }

    /**
     * <p>
     * Select DCd method.
     * </p>
     * 
     * @param actionMapping the actionMapping
     * @param actionForm the actionForm
     * @param request the request
     * @param response the response
     * @return ActionForward
     * @throws Exception Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception
    {
        Word007Form form = (Word007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        
        /** Call private action method to get current user login information and roles from DENSO-context. */
        UserLoginDomain userLoginDomain = this.getLoginUser();
        DataScopeControlDomain dataScopeControlDomain
            = this.getDataScopeControlForUserLogin(userLoginDomain);
        dataScopeControlDomain.setLocale(locale);

        /** Call Façade Service KANBANOrderInformationFacadeService changeSelectDENSOCompany() to get the list of DENSO plant code by DENSO company code. */
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(form.getDCd());
        plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantDensoWithScopeDomain.setLocale(locale);

        List<PlantDensoDomain> plantDensoDomain = new ArrayList<PlantDensoDomain>();
        form.setUserLogin(setUserLogin());
        
        try {
            plantDensoDomain = kanbanOrderInformationFacadeService
                .searchSelectedCompanyDenso(plantDensoWithScopeDomain);

            printJson(response, new JsonResult(
                (List<Object>)(Object)plantDensoDomain), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        
        return null;
    }
    
    /**
     * Search DENSO company Code by selected DENSO plant Code and filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word007Form form = (Word007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanyDensoDomain> companyList = null;
        
        // 1. Call private action method to get current user login and roles from DENSO-context
        UserLoginDomain userLogin = this.getLoginUser();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        // 2. Get roles for supplier user
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        dataScopeControl.setLocale(locale);

        try{
            /* 3. Call Façade Service PurchaseOrderInformationFacadeService
             * changeSelectSupplierCompany() to get the list of supplier plant code by supplier
             * company code
             * */
            PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getDPcd())) {
                plantDenso.getPlantDensoDomain().setDPcd(form.getDPcd());
            }
            plantDenso.setDataScopeControlDomain(dataScopeControl);
            companyList = this.kanbanOrderInformationFacadeService.searchSelectedPlantDenso(
                plantDenso);

            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }

        return null;
    }
    
    /**
     * <p>
     * Download CSV method.
     * </p>
     * 
     * @param actionMapping the action Mapping
     * @param actionForm the action Form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the Exception
     */
    public ActionForward doDownload(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception
    {
        Word007Form form = (Word007Form)actionForm;
        KanbanOrderInformationDomain criteria = new KanbanOrderInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        ServletOutputStream outputStream = null;
        
        UserLoginDomain userLoginDomain = this.getLoginUser();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        DataScopeControlDomain dataScopeControlDomain
            = this.getDataScopeControlForUserLogin(userLoginDomain);
        dataScopeControlDomain.setLocale(locale);
        
        form.setUserLogin(setUserLogin());
        
        try {
            form.setMode(SupplierPortalConstant.METHOD_DO_SEARCH);
            
            String firstPages = form.getFirstPages();
            String pages = form.getPages();
            
            if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
                setParameterCriteria(form, firstPages, criteria);
                criteria.setLocale(locale);
                
                KanbanOrderInformationResultMasterDomain kanbanOrderInformationResultMasterDomain
                    = kanbanOrderInformationFacadeService.searchKanbanOrderInformationCsv(criteria);
                if (null != kanbanOrderInformationResultMasterDomain.getErrorMessageList()) {
                    errorMessageList
                        = kanbanOrderInformationResultMasterDomain.getErrorMessageList();
                } else {
                    form.setKanbanInformation(kanbanOrderInformationResultMasterDomain
                        .getKanbanOrderInformationResultDomain());
                    
                    /** Get created CSV file from KANBANOrderInformationFacadeService. */
                    String fileName = null;
                    StringBuffer resultStr
                        = kanbanOrderInformationResultMasterDomain.getResultString();
                    if (null != resultStr) {
                        Calendar cal = Calendar.getInstance();
                        fileName = StringUtil.appendsString(
                            Constants.KANBAN_ORDER_INFORMATION_LIST,
                            Constants.SYMBOL_UNDER_SCORE,
                            DateUtil.format(cal.getTime(), DateUtil.PATTERN_YYYYMMDD_HHMM));
                        
                        byte[] output = resultStr.toString().getBytes();
                        setHttpHeaderForCsv(response, fileName);
                        outputStream = response.getOutputStream();
                        outputStream.write(output, Constants.ZERO,
                            output.length);
                        outputStream.flush();
                        
                    } else {
                        errorMessageList = kanbanOrderInformationResultMasterDomain
                            .getErrorMessageList();
                    }
                }
            }
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(locale, 
                SupplierPortalConstant.ERROR_CD_SP_80_0012);
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                message));
        } finally {
            if (null != outputStream) {
                outputStream.close();
            }
            form.setApplicationMessageList(errorMessageList);
            
            Set<String> applicationMessageSet = new LinkedHashSet<String>();
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
        if (null != form.getApplicationMessageList()
            && Constants.ZERO != form.getApplicationMessageList().size()) {
            return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
        } else {
            return null;
        }
    }

    /**
     * <p>
     * Download PDF method.
     * </p>
     * 
     * @param actionMapping actionMapping
     * @param actionForm actionForm
     * @param request request
     * @param response response
     * @return ActionForward
     * @throws Exception Exception
     */
    public ActionForward doDownloadPdf(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Word007Form form = (Word007Form)actionForm;
        KanbanOrderInformationDomain kanbanOrderInformationDomain = 
            new KanbanOrderInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        FileManagementDomain fileManagementDomain = null;
        ServletOutputStream outputStream = null;
        
        form.setUserLogin(setUserLogin());

        /** Call Façade Service KANBANOrderInformationFacadeService findFileName() to get PDF file name. */
        try {

            UserLoginDomain userLoginDomain = this.getLoginUser();
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD007_LVIEWPDF)) 
            {
                this.kanbanOrderInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD007_LVIEWPDF, locale);
            }
            
            kanbanOrderInformationDomain.setPdfFileId(form.getPdfFileId());
            kanbanOrderInformationDomain.setLocale(locale);

            /** to get PDF information. */
            fileManagementDomain = kanbanOrderInformationFacadeService.searchGenerateDo(kanbanOrderInformationDomain);
            
            super.setHttpHeaderForFileManagement(response, request, fileManagementDomain.getFileName());
            outputStream = response.getOutputStream();
            FileUtil.writeInputStreamToOutputStream(fileManagementDomain.getFileData(), response.getOutputStream());

//            fileManagementDomain = kanbanOrderInformationFacadeService.searchKanbanOrderReport(
//                kanbanOrderInformationDomain, response.getOutputStream());
            form.setMessageType(Constants.MESSAGE_SUCCESS);
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            if (null != outputStream) {
                outputStream.close();
            }
            form.setApplicationMessageList(errorMessageList);

            Set<String> applicationMessageSet = new LinkedHashSet<String>();
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
       
        if(null != form.getApplicationMessageList()
            && Constants.ZERO != form.getApplicationMessageList().size()){
            return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
        }else{
            return null;
        }
    }
    /**
     * <p>
     * Download Kanban PDF.
     * </p>
     * 
     * @param actionMapping actionMapping
     * @param actionForm actionForm
     * @param request request
     * @param response response
     * @return ActionForward
     * @throws Exception Exception
     */
    public ActionForward doDownloadOneWayKanbanPdf(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Word007Form form = (Word007Form)actionForm;
        KanbanOrderInformationDomain kanbanOrderInformationDomain = 
            new KanbanOrderInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        ServletOutputStream outputStream = null;
        
        form.setUserLogin(setUserLogin());

        /** Call Façade Service KANBANOrderInformationFacadeService findFileName() to get PDF file name. */
        try {

            UserLoginDomain userLoginDomain = this.getLoginUser();
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD007_LVIEWPDF)) 
            {
                this.kanbanOrderInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD007_LVIEWPDF, locale);
            }
            

            /** Call Façade Service DeliveryOrderInformationFacadeService searchFileName() to get PDF file name */
            kanbanOrderInformationDomain.setDoId(form.getDoId());
            kanbanOrderInformationDomain.setLocale(locale);
            
            InputStream input = kanbanOrderInformationFacadeService.createOneWayKanbanTagReport(kanbanOrderInformationDomain);
            
            if(!StringUtil.checkNullOrEmpty(input)){
                if(form.getPdfType().equals(Constants.STR_KB)){
                    if(form.getPdfFileKbTagPrintDate().equals(Constants.STR_N)){
                        kanbanOrderInformationFacadeService
                        .updateDateTimeKanbanPdf(kanbanOrderInformationDomain);
                    }
                }
                StringBuffer fileName = new StringBuffer();
                fileName.append(SupplierPortalConstant.ONE_WAY_KANBAN_TAG);
                fileName.append(kanbanOrderInformationDomain.getDoId());
                fileName.append(Constants.SYMBOL_DOT);
                fileName.append(Constants.REPORT_PDF_FORMAT);
                
                this.setHttpHeaderForFileManagement(response, request, fileName.toString());
                FileUtil.writeInputStreamToOutputStream(input, response.getOutputStream());
                return null;
            }
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0017);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            if (null != outputStream) {
                outputStream.close();
            }
            form.setApplicationMessageList(errorMessageList);

            Set<String> applicationMessageSet = new LinkedHashSet<String>();
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
       
        if(null != form.getApplicationMessageList()
            && Constants.ZERO != form.getApplicationMessageList().size()){
            return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
        }else{
            return null;
        }
    }
    /**
     * <p>
     * Click link more method.
     * </p>
     * 
     * @param actionMapping actionMapping
     * @param actionForm actionForm
     * @param request request
     * @param response response
     * @return ActionForward
     * @throws Exception Exception
     */
    public ActionForward doClickLinkMore(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Word007Form form = (Word007Form)actionForm;
        KanbanOrderInformationDomain kanbanOrderInformationDomain = 
            new KanbanOrderInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        FileManagementDomain fileManagementDomain = null;
        ServletOutputStream outputStream = null;
        
        form.setUserLogin(setUserLogin());
        
        /** Call Façade Service KANBANOrderInformationFacadeService findFileName() to get PDF file name. */
        try {
            String legendFileId = ContextParams.getLegendFileId();
            kanbanOrderInformationDomain.setPdfFileId(legendFileId);
            kanbanOrderInformationDomain.setLocale(locale);
            fileManagementDomain = kanbanOrderInformationFacadeService
                .searchFileName(kanbanOrderInformationDomain);
            String pdfFileName = fileManagementDomain.getFileName();
            
            outputStream = response.getOutputStream();

            super
                .setHttpHeaderForFileManagement(response, request, pdfFileName);

            /** Call Façade Service KANBANOrderInformationFacadeService searchLegendInfo() to get PDF file. */
            fileManagementDomain = kanbanOrderInformationFacadeService.searchLegendInfo(
                kanbanOrderInformationDomain, outputStream);
            form.setMessageType(Constants.MESSAGE_SUCCESS);
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            if (null != outputStream) {
                outputStream.close();
            }
            form.setApplicationMessageList(errorMessageList);

            Set<String> applicationMessageSet = new LinkedHashSet<String>();
            applicationMessageSet.addAll(this.setPlantCombobox(form));
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }

        if(null != form.getApplicationMessageList()
            && Constants.ZERO != form.getApplicationMessageList().size()){
            return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
        }else{
            return null;
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.apache.struts.actions.LookupDispatchAction#getKeyMethodMap()
     */
    @Override
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap = new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_SEARCH,
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE,
            SupplierPortalConstant.METHOD_DO_CLICK_LINK_MORE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD_ONE_WAY_KANBAN_PDF,
                SupplierPortalConstant.METHOD_DO_DOWNLOAD_ONE_WAY_KANBAN_PDF);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD_PDF,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD_PDF);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_SCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_SPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_DPCD);

        return keyMethodMap;
    }

    /**
     * <p>
     * Set parameter criteria method.
     * </p>
     * 
     * @param form form
     * @param firstPages firstPages
     * @param criteria criteria
     */
    private void setParameterCriteria(Word007Form form, String firstPages,
        KanbanOrderInformationDomain criteria) {

        if (!StringUtil.checkNullOrEmpty(form.getDeliveryDateFrom())) {
            criteria.setDeliveryDateFrom(form.getDeliveryDateFrom());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDeliveryDateTo())) {
            criteria.setDeliveryDateTo(form.getDeliveryDateTo());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDeliveryTimeFrom())) {
            criteria.setDeliveryTimeFrom(form.getDeliveryTimeFrom());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDeliveryTimeTo())) {
            criteria.setDeliveryTimeTo(form.getDeliveryTimeTo());
        }
        if (!StringUtil.checkNullOrEmpty(form.getSCd())) {
            criteria.setSCd(form.getSCd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getVendorCd())) {
            criteria.setVendorCd(form.getVendorCd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getSPcd())) {
            criteria.setSPcd(form.getSPcd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDCd())) {
            criteria.setDCd(form.getDCd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDPcd())) {
            criteria.setDPcd(form.getDPcd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getShipmentStatus())) {
            criteria.setShipmentStatus(form.getShipmentStatus());
        }
        if (!StringUtil.checkNullOrEmpty(form.getTransportMode())) {
            criteria.setTransportMode(form.getTransportMode());
        }
        if (!StringUtil.checkNullOrEmpty(form.getRouteNo())) {
            criteria.setRouteNo(form.getRouteNo().toUpperCase().trim());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDel())) {
            criteria.setDel(form.getDel().toUpperCase().trim());
        }
        if (!StringUtil.checkNullOrEmpty(form.getShipDateFrom())) {
            criteria.setShipDateFrom(form.getShipDateFrom());
        }
        if (!StringUtil.checkNullOrEmpty(form.getShipDateTo())) {
            criteria.setShipDateTo(form.getShipDateTo());
        }
        if (!StringUtil.checkNullOrEmpty(form.getIssueDateFrom())) {
            criteria.setIssueDateFrom(form.getIssueDateFrom());
        }
        if (!StringUtil.checkNullOrEmpty(form.getIssueDateTo())) {
            criteria.setIssueDateTo(form.getIssueDateTo());
        }
        if (!StringUtil.checkNullOrEmpty(form.getRevision())) {
            criteria.setRevision(form.getRevision());
        }
        if (!StringUtil.checkNullOrEmpty(form.getSpsDoNo())) {
            criteria.setSpsDoNo(form.getSpsDoNo().toUpperCase().trim());
        }
        if (!StringUtil.checkNullOrEmpty(form.getCigmaDoNo())) {
            criteria.setCigmaDoNo(form.getCigmaDoNo().toUpperCase().trim());
        }
        if (Constants.PAGE_SEARCH.equals(firstPages)) {
            criteria.setPageNumber(Constants.ONE);
        } else {
            criteria.setPageNumber(form.getPageNo());
        }

        criteria.setSupplierAuthenList(form.getSupplierAuthenList());
        criteria.setDensoAuthenList(form.getDensoAuthenList());
        
    }

    /**
     * Set value inside Supplier Plant and DENSO Plant combo box.
     * @param form the Word007Form
     * @return set of application message
     * */
    private Set<String> setPlantCombobox(Word007Form form) {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getLoginUser();
        Set<String> applicationMessageSet = new LinkedHashSet<String>();
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        dataScopeControl.setLocale(locale);
        
        if (!Strings.judgeBlank(form.getVendorCd())) {
            List<PlantSupplierDomain> plantList = null;
            
            try{
                PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
                if (!Constants.MISC_CODE_ALL.equals(form.getVendorCd())) {
                    plantSupplier.getPlantSupplierDomain().setVendorCd(
                        form.getVendorCd());
                }
                plantSupplier.setDataScopeControlDomain(dataScopeControl);
                plantList = this.kanbanOrderInformationFacadeService
                    .searchSelectedCompanySupplier(plantSupplier);
                
                form.setPlantSupplierList(plantList);
            } catch (Exception e) {
                applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001
                    , new String[]{e.getClass().toString(), e.getMessage()}));
            }
        }
        
        if (!Strings.judgeBlank(form.getDCd())) {
            List<PlantDensoDomain> plantList = null;
            
            try{
                PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
                if (!Constants.MISC_CODE_ALL.equals(form.getDCd())) {
                    plantDenso.getPlantDensoDomain().setDCd(form.getDCd());
                }
                plantDenso.setDataScopeControlDomain(dataScopeControl);
                plantList = this.kanbanOrderInformationFacadeService
                    .searchSelectedCompanyDenso(plantDenso);
                form.setPlantDensoList(plantList);
            } catch (Exception e) {
                applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001
                    , new String[]{e.getClass().toString(), e.getMessage()}));
            }
        }
        
        return applicationMessageSet;
    }
    
    /**
     * Get Data Scope Control for current user login.
     * @param userLogin User Login Information
     * @return Data Scope Control
     * */
    private DataScopeControlDomain getDataScopeControlForUserLogin(UserLoginDomain userLogin) {
        DataScopeControlDomain dataScopeControl = new DataScopeControlDomain();
        dataScopeControl.setUserType(userLogin.getUserType());
        for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WORD007.equals(
                roleScreen.getSpsMScreenDomain().getScreenCd()))
            {
                dataScopeControl.setUserRoleDomainList(roleScreen.getUserRoleDomainList());
            }
        }
        dataScopeControl.setDensoSupplierRelationDomainList(null);
        return dataScopeControl;
    }
    
    /**
     * <p>
     * Get login user method.
     * </p>
     * 
     * @return UserLoginDomain
     */
    private UserLoginDomain getLoginUser() {
        return (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
    }
}
