/*
 * ModifyDate Developmentcompany Describe 
 * 2014/07/21 CSI Parichat       Create
 * 2017/08/25 Netband Rungsiwut  SPS Phase II
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.AcknowledgedDoInformationFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Wshp001Form;

/**
 * The Class AcknowledgedDoInformationAction.
 * <p>Receive value from Wshp001_AcknowledgedDoInformation.jsp for initial value and send to 
 * acknowledgedDoInformationFacadeService for search</p>
 * <ul>
 * <li>Method search   : doInitial</li>
 * <li>Method search   : doInitialReturn</li>
 * <li>Method search   : doSearch</li>
 * <li>Method search   : doDownload</li>
 * <li>Method search   : doClickLinkMore</li>
 * <li>Method search   : doClickLinkShipNotice</li>
 * <li>Method search   : doGroupAsn</li>
 * <li>Method search   : doSelectSCd</li>
 * <li>Method search   : doSelectSPcd</li>
 * <li>Method search   : doSelectDCd</li>
 * <li>Method search   : doSelectDPcd</li>
 * </ul>
 *
 * @author CSI
 */
public class AcknowledgedDoInformationAction extends CoreAction {

    /** The acknowledged do information facade service. */
    private AcknowledgedDoInformationFacadeService acknowledgedDoInformationFacadeService;
    
    /** The default constructor. */
    public AcknowledgedDoInformationAction() {
        super();
    }

    /**
     * Set the acknowledged do information facade service.
     * 
     * @param acknowledgedDoInformationFacadeService the acknowledged do information facade service to set
     */
    public void setAcknowledgedDoInformationFacadeService(
        AcknowledgedDoInformationFacadeService acknowledgedDoInformationFacadeService) {
        this.acknowledgedDoInformationFacadeService = acknowledgedDoInformationFacadeService;
    }

    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp001Form form = (Wshp001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        AcknowledgedDoInformationDomain acknowledgedDoInformation = null;
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        try{
            acknowledgedDoInformation = new AcknowledgedDoInformationDomain();
            acknowledgedDoInformation.setLocale(locale);
            form.resetForm();
            this.setComboboxInitial(form, acknowledgedDoInformation);
            this.setInitialCriteria(form);
            
            try{
                if(!userLogin.getButtonLinkNameList().contains(RoleTypeConstants.WSHP001_BRESET)){
                    this.acknowledgedDoInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WSHP001_BRESET, locale);
                }
            }catch(ApplicationException ex){
                form.setCannotResetMessage(ex.getMessage());
            }
        }catch(ApplicationException e){
            errorMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(Constants.ZERO < errorMessageList.size()){
                form.setApplicationMessageList(errorMessageList);
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do initial return.
     * <ul>
     * <li>Initial form when return from other screens.</li>
     * </ul>
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitialReturn(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp001Form form = (Wshp001Form)actionForm;
        Wshp001Form sessionForm = (Wshp001Form)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_WSHP001_FORM);
        //Clear general area
        DensoContext.get().removeGeneralArea(SupplierPortalConstant.SESSION_WSHP001_FORM);
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        AcknowledgedDoInformationDomain acknowledgedDoInformation = null;
        acknowledgedDoInformation = new AcknowledgedDoInformationDomain();
        acknowledgedDoInformation.setLocale(locale);
        try{
            //form.setFirstPages(Constants.PAGE_SEARCH);
            form.setPages(Constants.PAGE_CLICK);
            form.setPageNo(sessionForm.getPageNo());
            form.setDeliveryDateFrom(sessionForm.getDeliveryDateFrom());
            form.setDeliveryDateTo(sessionForm.getDeliveryDateTo());
            form.setDeliveryTimeFrom(sessionForm.getDeliveryTimeFrom());
            form.setDeliveryTimeTo(sessionForm.getDeliveryTimeTo());
            form.setVendorCd(sessionForm.getVendorCd());
            form.setSPcd(sessionForm.getSPcd());
            form.setDCd(sessionForm.getDCd());
            form.setDPcd(sessionForm.getDPcd());
            form.setShipmentStatus(sessionForm.getShipmentStatus());
            form.setTrans(sessionForm.getTrans());
            form.setRouteNo(sessionForm.getRouteNo());
            form.setDel(sessionForm.getDel());
            form.setShipDateFrom(sessionForm.getShipDateFrom());
            form.setShipDateTo(sessionForm.getShipDateTo());
            form.setIssueDateFrom(sessionForm.getIssueDateFrom());
            form.setIssueDateTo(sessionForm.getIssueDateTo());
            form.setOrderMethod(sessionForm.getOrderMethod());
            form.setSpsDoNo(sessionForm.getSpsDoNo());
            form.setCigmaDoNo(sessionForm.getCigmaDoNo());
            form.setCompanySupplierList(sessionForm.getCompanySupplierList());
            form.setCompanyDensoList(sessionForm.getCompanyDensoList());
            form.setDensoAuthenList(sessionForm.getDensoAuthenList());
            form.setSupplierAuthenList(sessionForm.getSupplierAuthenList());
            form.setCountGroupAsn(sessionForm.getCountGroupAsn());
            form.setCannotResetMessage(sessionForm.getCannotResetMessage());
            form.setApplicationMessageList(null);
            form.setReceiveByScan(sessionForm.getReceiveByScan());
            form.setReceiveByScanList(sessionForm.getReceiveByScanList());
            form.setLtFlag(sessionForm.getLtFlag());
            form.setLtFlagList(sessionForm.getLtFlagList());
            form.setIsPrintOneWayKanbanTag(sessionForm.getIsPrintOneWayKanbanTag());
            
            //Clear Selected CheckBox
            DensoContext.get().putGeneralArea(Constants.MAP_KEY_SELECTED_DO, 
                new HashMap<String, AcknowledgedDoInformationReturnDomain>());
            form.setCountGroupAsn(String.valueOf(Constants.ZERO));
            
            this.setComboboxInitial(form, acknowledgedDoInformation);
            this.setPlantCombobox(form);
            
            List<AcknowledgedDoInformationReturnDomain> acknowledgedDoInformationList
                =  this.searchAcknowledgedDoInformation(form, acknowledgedDoInformation);
            
            if(null != acknowledgedDoInformationList 
                && Constants.ZERO < acknowledgedDoInformationList.size()){
                form.setAcknowledgedDoInformationList(acknowledgedDoInformationList);
                SpsPagingUtil.setFormForPaging(acknowledgedDoInformation, form);
            }
            if(Constants.ZERO < acknowledgedDoInformation.getErrorMessageList().size()){
                errorMessageList.addAll(acknowledgedDoInformation.getErrorMessageList());
            }
        }catch(ApplicationException e){
            errorMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(Constants.ZERO < errorMessageList.size()){
                form.setApplicationMessageList(errorMessageList);
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do search.
     * <p>Search data by criteria from screen.</p>
     * <ul>
     * <li>In case click search button, system search data and count by criteria 
     * if count result size is 0 return message to screen "data not found".</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings("unchecked")
    public ActionForward doSearch(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp001Form form = (Wshp001Form)actionForm;
        String firstPage = form.getFirstPages();
        String page = form.getPages();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDoInformationList = null;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AcknowledgedDoInformationDomain criteria = new AcknowledgedDoInformationDomain();
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        if(Constants.PAGE_SEARCH.equals(firstPage) || Constants.PAGE_CLICK.equals(page)){
            try {
                if(!userLogin.getButtonLinkNameList().contains(RoleTypeConstants.WSHP001_BSEARCH)){
                    this.acknowledgedDoInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WSHP001_BSEARCH, locale);
                }
                if(Constants.PAGE_SEARCH.equals(firstPage)){
                    DensoContext.get().putGeneralArea(Constants.MAP_KEY_SELECTED_DO,
                        new HashMap<String, AsnInformationDomain>());
                    form.setCountGroupAsn(String.valueOf(Constants.ZERO));
                }else{
                    //Keep selected D/O when page search or click
                    this.doSetSelectedDoId(form);
                }
                criteria.setLocale(locale);
                
                acknowledgedDoInformationList 
                    = this.searchAcknowledgedDoInformation(form, criteria);
                if(null != acknowledgedDoInformationList 
                    && Constants.ZERO < acknowledgedDoInformationList.size())
                {
                    form.setAcknowledgedDoInformationList(acknowledgedDoInformationList);
                    SpsPagingUtil.setFormForPaging(criteria, form);
                    
                    Map<String, AcknowledgedDoInformationReturnDomain> selectedDoMap = 
                        (HashMap<String, AcknowledgedDoInformationReturnDomain>)
                        DensoContext.get().getGeneralArea(Constants.MAP_KEY_SELECTED_DO);
                    
                    for(AcknowledgedDoInformationReturnDomain item
                        : acknowledgedDoInformationList){
                        String doId = item.getDeliveryOrderDomain().getDoId().toString();
                        if(selectedDoMap.containsKey(doId)){
                            item.setSelectedDoId(doId);
                        }
                    }
                }else{
                    errorMessageList = criteria.getErrorMessageList();
                }
            }catch(ApplicationException e){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    e.getMessage()));
            }catch(Exception e){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }finally{
                this.setPlantCombobox(form);
                form.setApplicationMessageList(errorMessageList);
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do view pdf.
     * <p>Download PDF file from screen.</p>
     * <ul>
     * <li>In case click pdf icon, system search file id and return report in PDF file format.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownload(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp001Form form = (Wshp001Form)actionForm;
        OutputStream outputStream = null;
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        FileManagementDomain fileManagementDomain = null;
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        try{
            if(!userLogin.getButtonLinkNameList().contains(RoleTypeConstants.WSHP001_LVIEWPDF)){
                this.acknowledgedDoInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP001_LVIEWPDF, locale);
            }
            
            AcknowledgedDoInformationDomain acknowledgedDoInformation 
                = new AcknowledgedDoInformationDomain();
            acknowledgedDoInformation.setPdfFileId(form.getPdfFileId());
            acknowledgedDoInformation.setLocale(locale);
            
            /** Call Façade Service DeliveryOrderInformationFacadeService searchDeliveryOrderReport() to get PDF file. */
            fileManagementDomain = acknowledgedDoInformationFacadeService
                .searchGenerateDo(acknowledgedDoInformation);
            
            super.setHttpHeaderForFileManagement(response, request, fileManagementDomain.getFileName());
            outputStream = response.getOutputStream();
            FileUtil.writeInputStreamToOutputStream(fileManagementDomain.getFileData(), response.getOutputStream());
//            FileManagementDomain fileManagementDomain = acknowledgedDoInformationFacadeService
//                .searchFileName(acknowledgedDoInformation);
//            String pdfFileName = fileManagementDomain.getFileName();
//            
//            super.setHttpHeaderForFileManagement(response, request, pdfFileName);
//            outputStream = response.getOutputStream();
//            acknowledgedDoInformationFacadeService.searchDeliveryOrderReport(
//                acknowledgedDoInformation, outputStream);
//            outputStream.flush();
            return null;
        }catch(ApplicationException e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(null != outputStream){
                outputStream.close();
            }
        }
        //this.setPlantCombobox(form);
        form.setApplicationMessageList(errorMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    /**
     * <p>
     * Download Kanban PDF.
     * </p>
     * 
     * @param actionMapping actionMapping
     * @param actionForm actionForm
     * @param request request
     * @param response response
     * @return ActionForward
     * @throws Exception Exception
     */
    public ActionForward doDownloadOneWayKanbanPdf(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Wshp001Form form = (Wshp001Form)actionForm;
        OutputStream outputStream = null;
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());

        try {
            if(!userLogin.getButtonLinkNameList().contains(RoleTypeConstants.WSHP001_LVIEWPDF)){
                this.acknowledgedDoInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP001_LVIEWPDF, locale);
            }
            AcknowledgedDoInformationDomain acknowledgedDoInformation = new AcknowledgedDoInformationDomain();
            
            /** Call Façade Service DeliveryOrderInformationFacadeService searchFileName() to get PDF file name */
            acknowledgedDoInformation.setDoId(form.getDoId());
            acknowledgedDoInformation.setLocale(locale);
            
            InputStream input = acknowledgedDoInformationFacadeService.createOneWayKanbanTagReport(acknowledgedDoInformation);
            
            if(!StringUtil.checkNullOrEmpty(input)){
                if(form.getPdfType().equals(Constants.STR_KB)){
                    if(form.getPdfFileKbTagPrintDate().equals(Constants.STR_N)){
                        acknowledgedDoInformationFacadeService
                        .updateDateTimeKanbanPdf(acknowledgedDoInformation);
                    }
                }
                StringBuffer fileName = new StringBuffer();
                fileName.append(SupplierPortalConstant.ONE_WAY_KANBAN_TAG);
                fileName.append(acknowledgedDoInformation.getDoId());
                fileName.append(Constants.SYMBOL_DOT);
                fileName.append(Constants.REPORT_PDF_FORMAT);
                
                this.setHttpHeaderForFileManagement(response, request, fileName.toString());
                FileUtil.writeInputStreamToOutputStream(input, response.getOutputStream());
                return null;
            }
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0017);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally{
            if (null != outputStream) {
                outputStream.close();
            }
            form.setApplicationMessageList(errorMessageList);

            Set<String> applicationMessageSet = new LinkedHashSet<String>();
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
        
        if (null != form.getApplicationMessageList()
            && Constants.ZERO != form.getApplicationMessageList().size()) {
            return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
        } else {
            return null;
        }
    }
    
    /**
     * Do download legend file.
     * <p>Download Legend Information PDF file.</p>
     * <ul>
     * <li>In case click pdf icon, system search file id and return report in PDF file format.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doClickLinkMore (ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp001Form form = (Wshp001Form)actionForm;
        OutputStream outputStream = null;
        AcknowledgedDoInformationDomain acknowledgedDoInformation = 
            new AcknowledgedDoInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        try{
            String legendFileId = ContextParams.getLegendFileId();
            acknowledgedDoInformation.setPdfFileId(legendFileId);
            FileManagementDomain fileManagementDomain = 
                acknowledgedDoInformationFacadeService.searchFileName(acknowledgedDoInformation);
            String pdfFileName = fileManagementDomain.getFileName();
            
            super.setHttpHeaderForFileManagement(response, request, pdfFileName);
            outputStream = response.getOutputStream();
            acknowledgedDoInformationFacadeService.searchLegendInfo(
                acknowledgedDoInformation, outputStream);
            outputStream.flush();
            return null;
        }catch(ApplicationException e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(null != outputStream){
                outputStream.close();
            }
        }
        this.setPlantCombobox(form);
        this.setInitialCriteria(form);
        form.getApplicationMessageList().addAll(errorMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do view ship notice.
     * <p>View ship notice.</p>
     * <ul>
     * <li>In case click link on search result, the system group selected D/O from checkbox.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doClickLinkShipNotice (ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp001Form form = (Wshp001Form)actionForm;
        StringBuffer forward = new StringBuffer();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        try{
            if(!userLogin.getButtonLinkNameList().contains(RoleTypeConstants.WSHP001_LSHIPNOTICE)){
                this.acknowledgedDoInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP001_LSHIPNOTICE, locale);
            }
            
            forward.append(SupplierPortalConstant.URL_WSHP003_ACTION);
            forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
            forward.append(SupplierPortalConstant.METHOD_DO_INITIAL);
            forward.append(SupplierPortalConstant.URL_PARAM_DSCID);
            forward.append(userLogin.getDscId());
            forward.append(SupplierPortalConstant.URL_PARAM_DOID);
            forward.append(form.getSelectedDoId());
            forward.append(SupplierPortalConstant.URL_PARAM_DCD);
            forward.append(form.getSelectedDCd());
            forward.append(SupplierPortalConstant.URL_PARAM_SPSDONO);
            forward.append(form.getSelectedSpsDoNo());
            forward.append(SupplierPortalConstant.URL_PARAM_REVISION);
            forward.append(form.getSelectedRev());
            forward.append(SupplierPortalConstant.URL_PARAM_ROUTENO);
            forward.append(form.getSelectedRoute());
            forward.append(SupplierPortalConstant.URL_PARAM_DEL);
            forward.append(form.getSelectedDel());
            forward.append(SupplierPortalConstant.URL_PARAM_SCREENID);
            forward.append(SupplierPortalConstant.SCREEN_ID_WSHP001);
            DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WSHP001_FORM, form);
            return new ActionForward(forward.toString(), true);
        }catch(ApplicationException e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        
        this.setPlantCombobox(form);
        form.getApplicationMessageList().addAll(errorMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do group asn.
     * <p>Group asn and create asn data with selected SPS D/O.</p>
     * <ul>
     * <li>In case click on group asn button, the system links to WSHP003 : Inquiry DO Detail screen.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings({Constants.SUPPRESS_WARNINGS_UNCHECKED})
    public ActionForward doGroupAsn(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp001Form form = (Wshp001Form)actionForm;
        boolean isValidGroupAsn = true;
        StringBuilder forward = new StringBuilder();
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        AcknowledgedDoInformationDomain acknowledgedDoInfo 
            = new AcknowledgedDoInformationDomain();
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDoInformationList 
            = new ArrayList<AcknowledgedDoInformationReturnDomain>();
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        acknowledgedDoInfo.setLocale(locale);
        
        InquiryDoDetailDomain inquiryDODetailDomain = new InquiryDoDetailDomain();
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        try{
            if(!userLogin.getButtonLinkNameList().contains(RoleTypeConstants.WSHP001_BGROUPASN)){
                this.acknowledgedDoInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP001_BGROUPASN, locale);
            }
            
            this.doSetSelectedDoId(form);
            Map<String, AcknowledgedDoInformationReturnDomain> selectedDoMap 
                = (HashMap<String, AcknowledgedDoInformationReturnDomain>)DensoContext.get()
                    .getGeneralArea(Constants.MAP_KEY_SELECTED_DO);
            
            if(null != selectedDoMap && !selectedDoMap.isEmpty())
            {
                Iterator<String> iterator = selectedDoMap.keySet().iterator();
                while(iterator.hasNext()){
                    String keyStr = (String)iterator.next();
                    acknowledgedDoInformationList.add(selectedDoMap.get(keyStr));
                }
                
                if(Constants.ZERO < acknowledgedDoInformationList.size())
                {
                    acknowledgedDoInfo.setAcknowledgedDoInformationList(
                        acknowledgedDoInformationList);
                    acknowledgedDoInformationFacadeService.searchValidateGroupAsn(
                        acknowledgedDoInfo);
                    
                    inquiryDODetailDomain.setDensoAuthenList(form.getDensoAuthenList());
                    inquiryDODetailDomain.setSupplierAuthenList(form.getSupplierAuthenList());
                    acknowledgedDoInformationFacadeService.isAmountPartGroupByRcvLaneOverQrCode(acknowledgedDoInfo, inquiryDODetailDomain);
                    if(null != acknowledgedDoInfo.getErrorMessageList() 
                        && Constants.ZERO < acknowledgedDoInfo.getErrorMessageList().size()){
                        isValidGroupAsn = false;
                        applicationMessageList.addAll(acknowledgedDoInfo.getErrorMessageList());
                    }
                    if(null != acknowledgedDoInfo.getWarningMessageList() 
                        && Constants.ZERO < acknowledgedDoInfo.getWarningMessageList().size()){
                        applicationMessageList.addAll(acknowledgedDoInfo.getWarningMessageList());
                    }
                    if(isValidGroupAsn)
                    {
                        forward.append(SupplierPortalConstant.URL_WSHP004_ACTION);
                        forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
                        forward.append(SupplierPortalConstant.METHOD_DO_INITIAL);
                        forward.append(SupplierPortalConstant.URL_PARAM_DSCID);
                        forward.append(userLogin.getDscId());
                        forward.append(SupplierPortalConstant.URL_PARAM_SCREENID);
                        forward.append(SupplierPortalConstant.SCREEN_ID_WSHP001);
                        forward.append(SupplierPortalConstant.URL_PARAM_TEMPORARY_MODE);
                        forward.append(Constants.STR_ONE);
                        if(Constants.ZERO < applicationMessageList.size()){
                            form.setApplicationMessageList(applicationMessageList);
                        }
                        DensoContext.get().putGeneralArea(
                            SupplierPortalConstant.SESSION_WSHP001_FORM, form);
                        return new ActionForward(forward.toString(), true);
                    }
                }
            }
        }catch(ApplicationException e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                e.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessage(
                        locale, SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
        }
        
        this.setPlantCombobox(form);
        if(Constants.ZERO < applicationMessageList.size()){
            form.setApplicationMessageList(applicationMessageList);
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * <p>Denso company code combo box change in criteria area of screen.</p>
     * <ul>
     * <li>when company combo box change load new denso plant code
     * for relate company selected.
     * </li>
     * </ul> 
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp001Form form = (Wshp001Form)actionForm;
        List<PlantDensoDomain> plantDensoList = null;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        try{
            UserLoginDomain userLogin = this.getUserLoginInformation();
            DataScopeControlDomain dataScopeControlDomain
                = this.getDataScopeControlForUserLogin(userLogin);
            
            PlantDensoWithScopeDomain plantDensoWithScope = new PlantDensoWithScopeDomain();
            if(!Constants.MISC_CODE_ALL.equals(form.getDCd())){
                plantDensoWithScope.getPlantDensoDomain().setDCd(form.getDCd());
            }
            plantDensoWithScope.setLocale(locale);
            plantDensoWithScope.setDataScopeControlDomain(dataScopeControlDomain);
            plantDensoList = acknowledgedDoInformationFacadeService
                .searchSelectedCompanyDenso(plantDensoWithScope);
            
            printJson(response, new JsonResult((List<Object>)(Object)plantDensoList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * <p>Supplier company code combo box change in criteria area of screen.</p>
     * <ul>
     * <li>when supplier company code combo box change load new supplier plant code
     * for relate company selected.
     * </li>
     * </ul> 
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp001Form form = (Wshp001Form)actionForm;
        List<PlantSupplierDomain> plantSupplierList = null;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        try{
            UserLoginDomain userLogin = this.getUserLoginInformation();
            DataScopeControlDomain dataScopeControlDomain
                = this.getDataScopeControlForUserLogin(userLogin);
            
            PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
            if(!Constants.MISC_CODE_ALL.equals(form.getVendorCd())){
                plantSupplier.getPlantSupplierDomain().setVendorCd(form.getVendorCd());
            }
            plantSupplier.setLocale(locale);
            plantSupplier.setDataScopeControlDomain(dataScopeControlDomain);
            plantSupplierList = acknowledgedDoInformationFacadeService
                .searchSelectedCompanySupplier(plantSupplier);
            
            printJson(response, new JsonResult((List<Object>)(Object)plantSupplierList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * Search company DENSO Code by selected plant DENSO Code and filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp001Form form = (Wshp001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanyDensoDomain> companyList = null;
        
        // 1. Call private action method to get current user login and roles from DENSO-context
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        // 2. Get roles for supplier user
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        
        try{
            /* 3. Call Facade Service AcknowledgedDoInformationFacadeService
             * searchSelectedPlantDenso() to get the list of supplier plant code by supplier
             * company code
             * */
            PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getDPcd())) {
                plantDenso.getPlantDensoDomain().setDPcd(form.getDPcd());
            }
            plantDenso.setLocale(locale);
            plantDenso.setDataScopeControlDomain(dataScopeControl);
            companyList = this.acknowledgedDoInformationFacadeService.searchSelectedPlantDenso(
                plantDenso);
            
            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * Search Supplier Company Code by selected Supplier Plant Code filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp001Form form = (Wshp001Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanySupplierDomain> companyList = null;
        
        // 1. Call private action method to get current user login and roles from DENSO-context
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        // 2. Get roles for supplier user
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        
        try{
            /* 3. Call Facade Service AcknowledgedDoInformationFacadeService
             * searchSelectedPlantSupplier() to get the list of supplier company code by
             * supplier plant code
             * */
            PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getSPcd())) {
                plantSupplier.getPlantSupplierDomain().setSPcd(form.getSPcd());
            }
            plantSupplier.setLocale(locale);
            plantSupplier.setDataScopeControlDomain(dataScopeControl);
            companyList = this.acknowledgedDoInformationFacadeService.searchSelectedPlantSupplier(
                plantSupplier);
            
            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap()
    {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL_RETURN,
            SupplierPortalConstant.METHOD_DO_INITIAL_RETURN);
        keyMethodMap.put(SupplierPortalConstant.ACTION_SEARCH,
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD_PDF,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD_ONE_WAY_KANBAN_PDF,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD_ONE_WAY_KANBAN_PDF);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE,
            SupplierPortalConstant.METHOD_DO_CLICK_LINK_MORE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_LINK,
            SupplierPortalConstant.METHOD_DO_CLINK_LINK_SHIP_NOTICE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GROUP_ASN,
            SupplierPortalConstant.METHOD_DO_GROUP_ASN);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_SCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SPCD,
            SupplierPortalConstant.METHOD_DO_SELECT_SPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD,
            SupplierPortalConstant.METHOD_DO_SELECT_DPCD);
        return keyMethodMap;
    }

    /**
     * Do set selected do id
     * @param form the form
     * @throws Exception Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    private void doSetSelectedDoId(Wshp001Form form) throws Exception
    {
        List<AcknowledgedDoInformationReturnDomain> itemList 
            = form.getAcknowledgedDoInformationList();
        Map<String, AcknowledgedDoInformationReturnDomain> selectedDoMap 
            = (HashMap<String, AcknowledgedDoInformationReturnDomain>)
                DensoContext.get().getGeneralArea(Constants.MAP_KEY_SELECTED_DO);
        for(AcknowledgedDoInformationReturnDomain item : itemList)
        {
            String doId = String.valueOf(item.getDeliveryOrderDomain().getDoId());
            if(!StringUtil.checkNullOrEmpty(item.getSelectedDoId())){
                if(!selectedDoMap.containsKey(doId)){
                    selectedDoMap.put(doId, item);
                }
            }else{
                if(selectedDoMap.containsKey(doId)){
                    selectedDoMap.remove(doId);
                }
            }
        }
        
    }
    
    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return userLoginDomain the user login domain 
     */
    private UserLoginDomain getUserLoginInformation()
    {
        UserLoginDomain userLogin = null;
        userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLogin;
    }
    
    /**
     * Get Data Scope Control for current user login.
     * @param userLogin User Login Information
     * @return Data Scope Control
     * */
    private DataScopeControlDomain getDataScopeControlForUserLogin(UserLoginDomain userLogin)
    {
        DataScopeControlDomain dataScopeControl = new DataScopeControlDomain();
        dataScopeControl.setUserType(userLogin.getUserType());
        for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList())
        {
            if (SupplierPortalConstant.SCREEN_ID_WSHP001.equals(
                roleScreen.getSpsMScreenDomain().getScreenCd()))
            {
                dataScopeControl.setUserRoleDomainList(roleScreen.getUserRoleDomainList());
            }
        }
        dataScopeControl.setDensoSupplierRelationDomainList(null);
        return dataScopeControl;
    }
    
    /**
     * <p>Initial value to combo box when on load page</p>
     * <ul>
     * <li>if action is initial new all list and set to combo box result area.</li>
     * <li>else user select data set data to combo box result area.</li>
     * </ul>
     * 
     * @param form the form
     * @param acknowledgedDoInformation the acknowledgedDoInformation domain
     * @throws ApplicationException ApplicationException
     */
    private void setComboboxInitial(Wshp001Form form, AcknowledgedDoInformationDomain 
        acknowledgedDoInformation) throws ApplicationException
    {
        //initial screen
        UserLoginDomain userLogin = this.getUserLoginInformation();
        DataScopeControlDomain dataScopeControlDomain
            = this.getDataScopeControlForUserLogin(userLogin);
        
        acknowledgedDoInformation.setDataScopeControlDomain(dataScopeControlDomain);
        acknowledgedDoInformationFacadeService.searchInitial(acknowledgedDoInformation);
        
        form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
            acknowledgedDoInformation.getDataScopeControlDomain()
                .getDensoSupplierRelationDomainList(), SupplierPortalConstant.SCREEN_ID_WSHP001));
        form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
            acknowledgedDoInformation.getDataScopeControlDomain()
                .getDensoSupplierRelationDomainList(), SupplierPortalConstant.SCREEN_ID_WSHP001));
        
        form.setCompanySupplierList(acknowledgedDoInformation.getCompanySupplierList());
        form.setCompanyDensoList(acknowledgedDoInformation.getCompanyDensoList());
        form.setPlantSupplierList(acknowledgedDoInformation.getPlantSupplierList());
        form.setPlantDensoList(acknowledgedDoInformation.getPlantDensoList());
        form.setShipmentStatusList(acknowledgedDoInformation.getShipmentStatusList());
        form.setTransList(acknowledgedDoInformation.getTransList());
        form.setOrderMethodList(acknowledgedDoInformation.getOrderMethodList());
        form.setReceiveByScanList(acknowledgedDoInformation.getReceiveByScanList());
        form.setLtFlagList(acknowledgedDoInformation.getLtFlagList());
    }
    
    /**
     * Set value inside Supplier Plant and DENSO Plant combo box.
     * @param form the Wshp001Form
     * */
    private void setPlantCombobox(Wshp001Form form)
    {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginInformation();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        
        if (!Strings.judgeBlank(form.getVendorCd())){
            List<PlantSupplierDomain> plantSupplierList = null;
            try{
                PlantSupplierWithScopeDomain plantSupplierWithScope
                    = new PlantSupplierWithScopeDomain();
                
                if (!Constants.MISC_CODE_ALL.equals(form.getVendorCd())) {
                    plantSupplierWithScope.getPlantSupplierDomain().setVendorCd(form.getVendorCd());
                }
                plantSupplierWithScope.setLocale(locale);
                plantSupplierWithScope.setDataScopeControlDomain(dataScopeControl);
                plantSupplierList = acknowledgedDoInformationFacadeService
                    .searchSelectedCompanySupplier(plantSupplierWithScope);
                form.setPlantSupplierList(plantSupplierList);
            }catch(ApplicationException e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    e.getMessage()));
            }catch(Exception e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }
        }
        
        if (!Strings.judgeBlank(form.getDCd())){
            List<PlantDensoDomain> plantDensoList = null;
            try{
                PlantDensoWithScopeDomain plantDensoWithScope
                    = new PlantDensoWithScopeDomain();
                if(!Constants.MISC_CODE_ALL.equals(form.getDCd())){
                    plantDensoWithScope.getPlantDensoDomain().setDCd(form.getDCd());
                }
                plantDensoWithScope.setLocale(locale);
                plantDensoWithScope.setDataScopeControlDomain(dataScopeControl);
                plantDensoList = this.acknowledgedDoInformationFacadeService
                    .searchSelectedCompanyDenso(plantDensoWithScope);
                form.setPlantDensoList(plantDensoList);
            }catch(ApplicationException e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    e.getMessage()));
            }catch (Exception e) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }
        }
    }
    
    /**
     * <p>Search acknowledged do information</p>
     * <ul>
     * <li>Search acknowledged do information.</li>
     * </ul>
     * 
     * @param form the form
     * @param criteria the acknowledgedDoInformation domain
     * @return the list of acknowledged do information
     * @throws ApplicationException ApplicationException
     */
    private List<AcknowledgedDoInformationReturnDomain> searchAcknowledgedDoInformation (
        Wshp001Form form, AcknowledgedDoInformationDomain criteria)
        throws ApplicationException
    {
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDoInformationList = null;
        String firstPage = form.getFirstPages();
        
        if(Constants.PAGE_SEARCH.equals(firstPage)){
            criteria.setPageNumber(Constants.ONE);
        }else{
            criteria.setPageNumber(form.getPageNo());
        }
        
        if(!StringUtil.checkNullOrEmpty(form.getDeliveryDateFrom().trim())){
            criteria.setDeliveryDateFrom(form.getDeliveryDateFrom().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getDeliveryDateTo().trim())){
            criteria.setDeliveryDateTo(form.getDeliveryDateTo().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getDeliveryTimeFrom().trim())){
            criteria.setDeliveryTimeFrom(form.getDeliveryTimeFrom().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getDeliveryTimeTo().trim())){
            criteria.setDeliveryTimeTo(form.getDeliveryTimeTo().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getVendorCd())){
            criteria.setVendorCd(form.getVendorCd());
        }
        if(!StringUtil.checkNullOrEmpty(form.getSPcd())){
            criteria.setSPcd(form.getSPcd());
        }
        if(!StringUtil.checkNullOrEmpty(form.getDCd())){
            criteria.setDCd(form.getDCd());
        }
        if(!StringUtil.checkNullOrEmpty(form.getDPcd())){
            criteria.setDPcd(form.getDPcd());
        }
        if(!StringUtil.checkNullOrEmpty(form.getShipmentStatus().trim())){
            criteria.setShipmentStatus(form.getShipmentStatus().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getTrans().trim())){
            criteria.setTrans(form.getTrans().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getRouteNo().trim())){
            criteria.setRouteNo(form.getRouteNo().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getDel().trim())){
            criteria.setDel(form.getDel().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getShipDateFrom().trim())){
            criteria.setShipDateFrom(form.getShipDateFrom().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getShipDateTo().trim())){
            criteria.setShipDateTo(form.getShipDateTo().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getIssueDateFrom().trim())){
            criteria.setIssueDateFrom(form.getIssueDateFrom().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getIssueDateTo().trim())){
            criteria.setIssueDateTo(form.getIssueDateTo().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getOrderMethod())){
            criteria.setOrderMethod(form.getOrderMethod());
        }
        if(!StringUtil.checkNullOrEmpty(form.getReceiveByScan())){
            criteria.setReceiveByScan(form.getReceiveByScan());
        }
        if(!StringUtil.checkNullOrEmpty(form.getLtFlag())){
            criteria.setLtFlag(form.getLtFlag());
        }
        if(!StringUtil.checkNullOrEmpty(form.getSpsDoNo().trim())){
            criteria.setSpsDoNo(form.getSpsDoNo().toUpperCase().trim());
        }
        if(!StringUtil.checkNullOrEmpty(form.getCigmaDoNo().trim())){
            criteria.setCigmaDoNo(form.getCigmaDoNo().toUpperCase().trim());
        }
        criteria.setDensoAuthenList(form.getDensoAuthenList());
        criteria.setSupplierAuthenList(form.getSupplierAuthenList());
        
        acknowledgedDoInformationList
            = acknowledgedDoInformationFacadeService.searchAcknowledgedDoInformation(criteria);
        return acknowledgedDoInformationList;
    }
    
    /**
     * Sets Initial data for search criteria.
     * @param form the Wshp001Form
     * */
    private void setInitialCriteria(Wshp001Form form){
        String currentDateTimeStr = DateUtil.format(
            Calendar.getInstance().getTime(), DateUtil.PATTERN_YYYYMMDD_SLASH);
        form.setDeliveryDateFrom(currentDateTimeStr);
        form.setDeliveryDateTo(currentDateTimeStr);
        form.setDeliveryTimeFrom(Constants.EMPTY_STRING);
        form.setDeliveryTimeTo(Constants.EMPTY_STRING);
        form.setVendorCd(Constants.EMPTY_STRING);
        form.setSPcd(Constants.EMPTY_STRING);
        form.setDCd(Constants.EMPTY_STRING);
        form.setDPcd(Constants.EMPTY_STRING);
        form.setShipmentStatus(Constants.EMPTY_STRING);
        form.setTrans(Constants.EMPTY_STRING);
        form.setRouteNo(Constants.EMPTY_STRING);
        form.setDel(Constants.EMPTY_STRING);
        form.setShipDateFrom(Constants.EMPTY_STRING);
        form.setShipDateTo(Constants.EMPTY_STRING);
        form.setIssueDateFrom(Constants.EMPTY_STRING);
        form.setIssueDateTo(Constants.EMPTY_STRING);
        form.setOrderMethod(Constants.EMPTY_STRING);
        form.setReceiveByScan(Constants.EMPTY_STRING);
        form.setLtFlag(Constants.EMPTY_STRING);
        form.setSpsDoNo(Constants.EMPTY_STRING);
        form.setCigmaDoNo(Constants.EMPTY_STRING);
        form.setAcknowledgedDoInformationList(null);
    }
}