/*
 * ModifyDate Development company     Describe 
 * 2014/07/30 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Wadm007Form;
import com.globaldenso.asia.sps.business.service.DensoUserUploadingFacadeService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoUserUploadingDomain;
import com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;


/**
 * The Class DensoUserUploadingAction.
 * <p>
 * Receive value from SupplierUserUploadingAction for initial value and send to 
 * supplierUserUploadingFacadeServiceImpl for manipulate data.
 * </p>
 * <ul>
 * <li>Method initial : doInitial</li>
 * </ul>
 * 
 * @author CSI
 */
public class DensoUserUploadingAction extends CoreAction {


    /** The DENSO User Uploading Facade service. */
    private DensoUserUploadingFacadeService densoUserUploadingFacadeService;
   

    /**
     * Instantiates a new DensoUserUploadingAction.
     */
    public DensoUserUploadingAction() {
        super();
    }
    
    /**
     * Sets the DENSO User Uploading Facade Service.
     * 
     * @param densoUserUploadingFacadeService the DENSO User Uploading Facade Service.
     */
    public void setDensoUserUploadingFacadeService(
        DensoUserUploadingFacadeService densoUserUploadingFacadeService) {
        this.densoUserUploadingFacadeService = densoUserUploadingFacadeService;
    }
    
    /**
     * Do initial.
     * <p>Initial DENSO user uploading screen when load page.</p>
     * <ul>
     * <li>In case click MainMenu (DENSO UserInformation) link, Reset button</li>
     * </ul>
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Wadm007Form form = (Wadm007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        HttpSession session = request.getSession();
        String sessionCode = session.getId();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        String userDscId = userLoginDomain.getDscId();
        
        try{
            /*Call Facade Service to clear upload DENSO user information in temporary table*/
            /*Set value to argDomain.*/
            DensoUserUploadingDomain criteria = new DensoUserUploadingDomain();
            criteria.setUserDscId(userDscId);
            criteria.setSessionCode(sessionCode);
            densoUserUploadingFacadeService.transactInitial(criteria);
            
            try{
                if(!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WADM007_BRESET)){
                    this.densoUserUploadingFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WADM007_BRESET, locale);
                }
            }catch(ApplicationException ex){
                form.setCannotResetMessage(ex.getMessage());
            }
            
            /*Set value of domain to ActionForm*/
            form.resetForm();
        }catch (ApplicationException ex) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ex.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * Do upload
     * <p>validate CSV file that uploaded.</p>
     * <ul>
     * <li>In case click Upload button.</li>
     * </ul>
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward  doUpload(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm007Form form = (Wadm007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String firstPages = form.getFirstPages();
        String pages = form.getPages();
        HttpSession session = request.getSession();
        String sessionCode = session.getId();
        String message = new String();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        List<GroupUploadErrorDomain> uploadErrorDetailList = 
            new ArrayList<GroupUploadErrorDomain>();
        DensoUserUploadingDomain resultDomain = new DensoUserUploadingDomain();
        DensoUserUploadingDomain criteria = new DensoUserUploadingDomain();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        String userDscId = userLoginDomain.getDscId();
        FormFile fileData = form.getFileData();
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
            try {
                if(!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WADM007_BUPLOAD)){
                    this.densoUserUploadingFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WADM007_BUPLOAD, locale);
                }
                
                if(userLoginDomain.getUserType().equals(SupplierPortalConstant.USER_TYPE_SUPPLIER)){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E9_0003,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
                
                if(Constants.PAGE_SEARCH.equals(firstPages)){
                    criteria.setPageNumber(Constants.ONE);
                }else{
                    criteria.setPageNumber(form.getPageNo());
                }
                
                /*Validate do not choose file*/
                if(StringUtil.checkNullOrEmpty(form.getFileData().getFileName())){
                    MessageUtil.throwsApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_W6_0006, 
                        SupplierPortalConstant.LBL_UPLOAD_FILE);
                }
                
                /*Validate file size*/
                if(Constants.ZERO == form.getFileData().getFileSize()){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0014, 
                        SupplierPortalConstant.ERROR_CD_SP_90_0002);
                }
                
                int maxCSVFileSize = ContextParams.getMaxCsvFileSize();
                if(maxCSVFileSize < form.getFileData().getFileSize()){
                    MessageUtil.throwsApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0015, 
                        SupplierPortalConstant.ERROR_CD_SP_90_0002, 
                        new String[]{String.valueOf(maxCSVFileSize)});
                }
                
                String fileName = form.getFileData().getFileName();
                if(!Constants.REPORT_CSV_FORMAT.equalsIgnoreCase(
                    fileName.substring(fileName.lastIndexOf(Constants.SYMBOL_DOT)
                        + Constants.ONE, fileName.length()).toLowerCase())){
                    MessageUtil.throwsApplicationMessageWithLabel(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_E7_0013, 
                        SupplierPortalConstant.LBL_CSV);
                }
                
                /*Delete temporary table data*/
                criteria.setUserDscId(userDscId);
                criteria.setSessionCode(sessionCode);
                densoUserUploadingFacadeService.transactInitial(criteria);
                
                /*Call Facade Service to upload supplier user information to system.*/
                FileManagementDomain fileManagementDomain = new FileManagementDomain();
                fileManagementDomain.setFileData(form.getFileData().getInputStream());
                fileManagementDomain.setFileName(form.getFileData().getFileName());
                
                DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
                dataScopeControlDomain.setLocale(locale);
                dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
                dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain
                    .getUserRoleDomainList());
                dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
                /*Set value to argDomain.*/
                criteria.setFileManagementDomain(fileManagementDomain);
                criteria.setUserDscId(userDscId);
                criteria.setSessionCode(sessionCode);
                criteria.setDataScopeControlDomain(dataScopeControlDomain);
                
                resultDomain = densoUserUploadingFacadeService.transactUploadDensoUser(criteria);
                uploadErrorDetailList = resultDomain.getUploadErrorDetailDomain();
                /*Set value of domain to ActionForm*/
                if(null == resultDomain.getErrorMessageList() 
                    ||  resultDomain.getErrorMessageList().size() <= Constants.ZERO){
                    form.setTotalRecord(resultDomain.getTotalRecord());
                    form.setCorrectRecord(resultDomain.getCorrectRecord());
                    form.setWarningRecord(resultDomain.getWarningRecord());
                    form.setIncorrectRecord(resultDomain.getInCorrectRecord());
                    form.setUploadErrorDetailDomain(uploadErrorDetailList);
                    SpsPagingUtil.setFormForPaging(resultDomain, form);
                    /*set message for upload success.*/
                    message = MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_I6_0010);
                    applicationMessageList.add(new ApplicationMessageDomain(
                        Constants.MESSAGE_SUCCESS, message));
                }
            } catch (ApplicationException ex) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    ex.getMessage()));
            } catch (Exception e) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            } finally {
                form.setApplicationMessageList(applicationMessageList);
                form.getFileData().destroy();
                form.setFileName(form.getFileData().getFileName());
                form.setFileData(fileData);
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Register.
     * <p>Register data of CSV file uploaded.</p>
     * <ul>
     * <li>In case click Register button.</li>
     * </ul>
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doRegister(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Wadm007Form form = (Wadm007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        HttpSession session = request.getSession();
        String sessionCode = session.getId();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        String userDscId = userLoginDomain.getDscId();
        
        try{
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM007_BREGISTER)){
                //Search DENSO user uploading data
                form.setPages(Constants.PAGE_CLICK);
                DensoUserUploadingDomain criteria = new DensoUserUploadingDomain();
                criteria.setPageNumber(form.getPageNo());
                criteria.setUserDscId(userDscId);
                criteria.setSessionCode(sessionCode);
                DensoUserUploadingDomain resultDomain
                    = densoUserUploadingFacadeService.searchUploadErrorList(criteria);
                List<GroupUploadErrorDomain> resultList = resultDomain.getUploadErrorDetailDomain();
                if(null != resultList){
                    form.setTotalRecord(resultDomain.getTotalRecord());
                    form.setCorrectRecord(resultDomain.getCorrectRecord());
                    form.setWarningRecord(resultDomain.getWarningRecord());
                    form.setIncorrectRecord(resultDomain.getInCorrectRecord());
                    SpsPagingUtil.setFormForPaging(resultDomain, form);
                    form.setUploadErrorDetailDomain(resultList);
                }
                this.densoUserUploadingFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM007_BREGISTER, locale);
            }
            
            /*Call Facade to move upload data from temporary to actual table*/
            DensoUserUploadingDomain criteria = new DensoUserUploadingDomain();
            /*Set value to argDomain.*/
            criteria.setUserDscId(userDscId);
            criteria.setSessionCode(sessionCode);
            densoUserUploadingFacadeService.transactRegisterUploadItem(criteria);
            
            /*Delete temporary table data*/
            criteria.setUserDscId(userDscId);
            criteria.setSessionCode(sessionCode);
            densoUserUploadingFacadeService.transactInitial(criteria);
            
            /*Delete temporary table with current date -2 */
            DensoUserUploadingDomain newCriteria = new DensoUserUploadingDomain();
            densoUserUploadingFacadeService.transactInitial(newCriteria);
            
            /*Set value of domain to ActionForm*/
            form.resetForm();
            
            /*Set import success massage*/
            String message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, 
                SupplierPortalConstant.ERROR_CD_SP_I6_0004);
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_SUCCESS,
                message));
        }catch (ApplicationException ex) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ex.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Paging.
     * <p>search data when load page.</p>
     * <ul>
     * <li>In case click Paging (Link).</li>
     * </ul>
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doPaging(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Wadm007Form form = (Wadm007Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        HttpSession session = request.getSession();
        String sessionCode = session.getId();
        String firstPages = form.getFirstPages();
        String pages = form.getPages();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        List<GroupUploadErrorDomain> resultList = new ArrayList<GroupUploadErrorDomain>();
        DensoUserUploadingDomain resultDomain = new DensoUserUploadingDomain();
        DensoUserUploadingDomain criteria = new DensoUserUploadingDomain();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        String userDscId = userLoginDomain.getDscId();
        
        if (Constants.PAGE_SEARCH.equals(firstPages) || Constants.PAGE_CLICK.equals(pages)) {
            try{
                if(Constants.PAGE_SEARCH.equals(firstPages)){
                    criteria.setPageNumber(Constants.ONE);
                }else{
                    criteria.setPageNumber(form.getPageNo());
                }
                /*Call Facade Service to get summary error from upload to show on screen.*/

                /*Set value to argument Domain.*/
                criteria.setUserDscId(userDscId);
                criteria.setSessionCode(sessionCode);
                resultDomain = densoUserUploadingFacadeService.searchUploadErrorList(criteria);
                resultList = resultDomain.getUploadErrorDetailDomain();
                if(null != resultList){
                    form.setTotalRecord(resultDomain.getTotalRecord());
                    form.setCorrectRecord(resultDomain.getCorrectRecord());
                    form.setWarningRecord(resultDomain.getWarningRecord());
                    form.setIncorrectRecord(resultDomain.getInCorrectRecord());
                    SpsPagingUtil.setFormForPaging(resultDomain, form);
                    form.setUploadErrorDetailDomain(resultList);
                }
            }catch (ApplicationException applicationException) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
            } catch (Exception e) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }finally {
                form.setApplicationMessageList(applicationMessageList);
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Gets the key method map.
     * 
     * @return Map
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        map.put(SupplierPortalConstant.ACTION_UPLOAD,
            SupplierPortalConstant.METHOD_DO_UPLOAD);
        map.put(SupplierPortalConstant.ACTION_REGISTER,
            SupplierPortalConstant.METHOD_DO_REGISTER);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_PAGING,
            SupplierPortalConstant.METHOD_DO_PAGING);
        return map;
    }
    
    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return the UserLoginDomain.
     */
    private UserLoginDomain setUserLoginInformation() {
        
        UserLoginDomain userLoginDomain = (UserLoginDomain)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }
    
    /**
     * Get roles for supplier user.
     * <p>Get roles  from role screen of user login.</p>
     * 
     * @param userLoginDomain the UserLoginDomain
     * @return the RoleScreenDomain
     */
    private RoleScreenDomain setRoleScreen(UserLoginDomain userLoginDomain) {
        List<RoleScreenDomain> roleScreenList = userLoginDomain.getRoleScreenDomainList();
        for(RoleScreenDomain roleScreen : roleScreenList){
            if(SupplierPortalConstant
                .SCREEN_ID_WADM007.equals(roleScreen.getSpsMScreenDomain().getScreenCd())){
                return roleScreen;
            }
        }
        return null;
    }
 
}