/*
 * ModifyDate Development company     Describe 
 * 2014/07/04 CSI Phakaporn           Create
 * 2016/02/16 CSI Akat                [IN054]
 * 2016/03/18 CSI Akat                [FIX] avoid special character in URL
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.PurchaseOrderAcknowledgementFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.presentation.form.Word003Form;

/**
 * The Class PurchaseOrderAcknowledgementAction.
 * <p>Receive value from WORD003_PurchaseOrderAcknowledgement.jsp for initial value and send to 
 * purchaseOrderAcknowledgementFacadeService for search</p>
 * <ul>
 * <li>Method initial   : doInitial</li>
 * <li>Method search    : doSaveAndSend</li>
 * <li>Method return    : doReturn</li>
 * <li>Method search    : doPreviewPending</li>
 * <li>Method Forward   : doOpenWord004</li>
 * <li>Method search    : doDensoReply</li>
 * </ul>
 *
 * @author CSI
 */
public class PurchaseOrderAcknowledgementAction extends CoreAction {

    /** The Purchase Order Acknowledgement facade service. */
    private PurchaseOrderAcknowledgementFacadeService purchaseOrderAcknowledgementFacadeService;
    
    
    /**
     * Instantiates a new PurchaseOrderAcknowledgementAction.
     */
    public PurchaseOrderAcknowledgementAction(){
        super();
    }
    
    /**
     * Sets the Purchase Order Acknowledgement facade service.
     * 
     * @param purchaseOrderAcknowledgementFacadeService the 
     * new Purchase Order Acknowledgement facade service.
     */
    public void setPurchaseOrderAcknowledgementFacadeService(
        PurchaseOrderAcknowledgementFacadeService purchaseOrderAcknowledgementFacadeService) {
        this.purchaseOrderAcknowledgementFacadeService = purchaseOrderAcknowledgementFacadeService;
    }
    
    /**
     * Do initial.
     * <p>Initial data when load page.</p>
     * <ul>
     * <li>In case click link SPS P/O No , Initial Purchase Order Information when load page.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        // 1. Setting the pass parameter to ActionForm
        Word003Form form = (Word003Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        form.setUserLogin(super.setUserLogin());
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        PurchaseOrderAcknowledgementDomain criteria = null;
        Set<String> applicationMessageSet = new LinkedHashSet<String>();
        
        if (Constants.PAGE_SEARCH.equals(form.getFirstPages())
            || Constants.PAGE_CLICK.equals(form.getPages()))
        {
            criteria = this.setParameterCriteria(form);
            criteria.setLocale(locale);
            
            // [IN054] Set user type for check action mode
            criteria.setUserType(userLogin.getSpsMUserDomain().getUserType());
            
            try {
                /* 2. Call Façade Service PurchaseOrderAcknowledgementFacadeService
                 * searchInitial() */
                Set<String> result = this.searchInitalData(form, criteria);
                if(null == form.getPoDetailList()
                    || form.getPoDetailList().size() <= Constants.ZERO)
                {
                    applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(
                        locale, SupplierPortalConstant.ERROR_CD_SP_E6_0001));
                }
                if (Constants.ZERO < result.size()) {
                    applicationMessageSet.addAll(result);
                }
                
                try {
                    if (!userLogin.getButtonLinkNameList().contains(
                        RoleTypeConstants.WORD003_BPREVIEWPENDING)) 
                    {
                        this.purchaseOrderAcknowledgementFacadeService.searchRoleCanOperate(
                            RoleTypeConstants.WORD003_BPREVIEWPENDING, locale);
                    }
                } catch (ApplicationException applicationException) {
                    form.setCannotPreviewPendingMessage(applicationException.getMessage());
                }
            } catch (ApplicationException applicationException) {
                applicationMessageSet.add(applicationException.getMessage());
            } catch (Exception e) {
                applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(
                    locale, SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()}));
            } finally {
                for (String message : applicationMessageSet) {
                    form.getApplicationMessageList().add(
                        new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
                }
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    // Akat Remark : may be not use
    /**
     * Initial Preview&Pending Pop up screen.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doPreviewPending(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word003Form form = (Word003Form)actionForm;
        form.setPageType(Constants.PAGE_TYPE_POPUP);
        
        StringBuffer forward = new StringBuffer();
        
        DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WORD003_FORM, form);
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        try {

            /* Parameter to WORD005 popup
             * - dscId (DensoContext)
             * - poId (DensoContext)
             * - <List>sPartNo (DensoContext)
             * - <List>dPartNo (DensoContext)
             * - periodType (DensoContext)
             * - periodTypeName (DensoContext)
             * - poType (DensoContext)
             * */
            forward.append(SupplierPortalConstant.URL_WORD005_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL)
                .append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES).append(Constants.PAGE_SEARCH)
                .append(SupplierPortalConstant.URL_PARAM_POID).append(form.getPoId())
                .append(SupplierPortalConstant.URL_PARAM_SPSPONO).append(form.getSpsPoNo());
            
            return new ActionForward(forward.toString(), true);
        } catch (Exception e) {
            applicationMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(
                        locale, SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            form.setApplicationMessageList(applicationMessageList);
        }

        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Save and Send. 
     * <p>Open WORD002_PurchaseOrderInformation.jsp</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doSaveAndSend(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word003Form form = (Word003Form)actionForm;
        StringBuffer forward = new StringBuffer();
        boolean isReturn = true;
        
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        PurchaseOrderAcknowledgementDomain criteria = null;
        criteria = this.setParameterCriteria(form);
        criteria.setLastUpdateDscId(userLogin.getDscId());
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        criteria.setLocale(locale);

        if (null != form.getPoDetailList() && Constants.ZERO != form.getPoDetailList().size()) {
            PurchaseOrderDetailDomain firstPoDetail = form.getPoDetail(Constants.ZERO);
            criteria.setDCd(firstPoDetail.getDCd());
            criteria.setDPcd(firstPoDetail.getDPcd());
            criteria.setLastUpdateDatetime(firstPoDetail.getLastUpdateDatetime());
        }
        
        try {
            if (!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD003_BSAVESEND)) 
            {
                this.purchaseOrderAcknowledgementFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD003_BSAVESEND, locale);
            }
            
            /* 1. Call Façade Service PurchaseOrderAcknowledgementFacadeService
             * .transactSaveAndSendPo()
             * */
            PurchaseOrderAcknowledgementDomain result
                = purchaseOrderAcknowledgementFacadeService.transactSaveAndSendPo(criteria);
            if(result != null){
                
                // 2. Forward to screen "WORD002 : Purchase Order Information"  (via ActionForm)
                String message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_I6_0007);
                
                // /PurchaseOrderInformationAction.do?method=doSearch&isReturn='isReturn'
                forward.append(SupplierPortalConstant.URL_WORD002_ACTION)
                    .append(SupplierPortalConstant.URL_PARAM_METHOD)
                    .append(SupplierPortalConstant.METHOD_DO_INITIAL_WITH_CRITERIA)
                    .append(SupplierPortalConstant.URL_PARAM_ISRETURN).append(isReturn)
                
                    // param name
                    .append(SupplierPortalConstant.URL_PRM_APPLICATION_MESSAGE_TYPE)
                    .append(Constants.SYMBOL_EQUAL)
                    // value
                    .append(Constants.MESSAGE_SUCCESS)
                    
                    // param name
                    .append(SupplierPortalConstant.URL_PRM_APPLICATION_MESSAGE_MESSAGE)
                    .append(Constants.SYMBOL_EQUAL)
                    // value
                    .append(message)
                    ;
                return new ActionForward(forward.toString(), true);
            }else{
                applicationMessageList.add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        MessageUtil.getApplicationMessageHandledException(
                            locale, SupplierPortalConstant.ERROR_CD_SP_E6_0001)));
            }
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(
                        locale, SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            form.setApplicationMessageList(applicationMessageList);
        }

        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    // [IN054] New action DENSO Reply
    /**
     * Do DENSO Reply P/O Pending
     * <p>Set P/O status to RPL and send email to inform Supplier User then
     * open WORD002_PurchaseOrderInformation.jsp</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDensoReply(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word003Form form = (Word003Form)actionForm;
        StringBuffer forward = new StringBuffer();
        boolean isReturn = true;
        
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        PurchaseOrderAcknowledgementDomain criteria = null;
        criteria = this.setParameterCriteria(form);
        criteria.setLastUpdateDscId(userLogin.getDscId());
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        criteria.setLocale(locale);

        if (null != form.getPoDetailList() && Constants.ZERO != form.getPoDetailList().size()) {
            PurchaseOrderDetailDomain firstPoDetail = form.getPoDetail(Constants.ZERO);
            criteria.setDCd(firstPoDetail.getDCd());
            criteria.setDPcd(firstPoDetail.getDPcd());
            criteria.setLastUpdateDatetime(firstPoDetail.getLastUpdateDatetime());
        }
        
        try {
            if (!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD003_BDENSOREPLY)) 
            {
                this.purchaseOrderAcknowledgementFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD003_BDENSOREPLY, locale);
            }
            
            /* 1. Call Façade Service PurchaseOrderAcknowledgementFacadeService
             * .transactSaveAndSendPo()
             * */
            PurchaseOrderAcknowledgementDomain result
                = purchaseOrderAcknowledgementFacadeService.transactDensoReply(criteria);
            if(result != null){
                
                // 2. Forward to screen "WORD002 : Purchase Order Information"  (via ActionForm)
                String message = MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_I6_0025);
                
                // /PurchaseOrderInformationAction.do?method=doSearch&isReturn='isReturn'
                forward.append(SupplierPortalConstant.URL_WORD002_ACTION)
                    .append(SupplierPortalConstant.URL_PARAM_METHOD)
                    .append(SupplierPortalConstant.METHOD_DO_INITIAL_WITH_CRITERIA)
                    .append(SupplierPortalConstant.URL_PARAM_ISRETURN).append(isReturn)
                
                    // param name
                    .append(SupplierPortalConstant.URL_PRM_APPLICATION_MESSAGE_TYPE)
                    .append(Constants.SYMBOL_EQUAL)
                    // value
                    .append(Constants.MESSAGE_SUCCESS)
                    
                    // param name
                    .append(SupplierPortalConstant.URL_PRM_APPLICATION_MESSAGE_MESSAGE)
                    .append(Constants.SYMBOL_EQUAL)
                    // value
                    .append(message)
                    ;
                return new ActionForward(forward.toString(), true);
            }else{
                applicationMessageList.add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                        MessageUtil.getApplicationMessageHandledException(
                            locale, SupplierPortalConstant.ERROR_CD_SP_E6_0001)));
            }
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(
                        locale, SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            form.setApplicationMessageList(applicationMessageList);
        }

        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do open WADM004.
     * <p>Open WORD004_SupplierPromisedDueSubmittion.jsp</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doOpenWord004(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word003Form form = (Word003Form)actionForm;
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        StringBuffer forward = new StringBuffer();
        
        DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WORD003_FORM, form);
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        try {
            if (!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD003_LEDITVIEW)) 
            {
                this.purchaseOrderAcknowledgementFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD003_LEDITVIEW, locale);
            }
            
            /* Parameter to WORD004
             * - dscId
             * - poId
             * - sPartNo
             * - dPartNo
             * - periodType
             * - periodTypeName
             * - poType
             * - actionMode
             * */
            forward.append(SupplierPortalConstant.URL_WORD004_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL)
                .append(SupplierPortalConstant.URL_PARAM_DSCID).append(userLogin.getDscId())
                
                // Start : [FIX] avoid special character in URL
                //.append(SupplierPortalConstant.URL_PARAM_POID).append(form.getPoId())
                //.append(SupplierPortalConstant.URL_PARAM_SPARTNO)
                //.append(form.getSelectedSPn())
                //.append(SupplierPortalConstant.URL_PARAM_DPARTNO).append(form.getSelectedDPn())
                //.append(SupplierPortalConstant.URL_PARAM_PERIODTYPE).append(form.getPeriodType())
                //.append(SupplierPortalConstant.URL_PARAM_PERIODTYPENAME)
                //.append(form.getPeriodTypeName())
                //.append(SupplierPortalConstant.URL_PARAM_POTYPE).append(form.getPoType())
                //.append(SupplierPortalConstant.URL_PARAM_ACTIONMODE)
                //.append(form.getSelectedActionMode())
                //.append(SupplierPortalConstant.URL_PARAM_SPSPONO).append(form.getSpsPoNo())
                // End : [FIX] avoid special character in URL
                
                .append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES).append(Constants.PAGE_SEARCH);
                   
            return new ActionForward(forward.toString(), true);
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(
                        locale, SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            form.setApplicationMessageList(applicationMessageList);
        }

        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * Do return. 
     * <p>Open WORD003_PurchaseOrderAcknowledgement.jsp</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReturn(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word003Form form = (Word003Form)actionForm;
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        StringBuffer forward = new StringBuffer();
        boolean isReturn = true;
        
        DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WORD003_FORM, form);
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        try {
            if (!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WORD003_BRETURN)) 
            {
                this.purchaseOrderAcknowledgementFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WORD003_BRETURN, locale);
            }
            
            // forword : PurchaseOrderInformationAction.do?method=Search&isReturn='isReturn'
            forward.append(SupplierPortalConstant.URL_WORD002_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL_WITH_CRITERIA)
                .append(SupplierPortalConstant.URL_PARAM_ISRETURN).append(isReturn);
            
            return new ActionForward(forward.toString(), true);
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    applicationException.getMessage()));
        } catch (Exception e) {
            applicationMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(
                        locale, SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            form.setApplicationMessageList(applicationMessageList);
        }

        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Download PDF Change.
     * <p>Download PDF Change from File Management Service</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doClickLinkMore(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Word003Form form = (Word003Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        PurchaseOrderAcknowledgementDomain criteria = new PurchaseOrderAcknowledgementDomain();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        OutputStream output = null;
        try {
            /* 1. Call Façade Service PurchaseOrderInformationFacadeService searchFileName()
             * to get PDF file name
             * */
            criteria.setFileIdSelected(ContextParams.getLegendFileId());
            criteria.setLocale(locale);
            FileManagementDomain file
                = this.purchaseOrderAcknowledgementFacadeService.searchFileName(criteria);
            
            /* 2. Call Façade Service PurchaseOrderInformationFacadeService searchLegendInfo()
             * to get PDF file
             * */
            super.setHttpHeaderForFileManagement(response, request, file.getFileName());
            output = response.getOutputStream();
            this.purchaseOrderAcknowledgementFacadeService.searchLegendInfo(criteria, output);
            return null;
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
            criteria = setParameterCriteria(form);
            criteria.setLocale(locale);
            // [IN054] Set user type for check action mode
            criteria.setUserType(userLogin.getSpsMUserDomain().getUserType());
            
            this.searchInitalData(form, criteria);
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_80_0009);
            applicationMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
            criteria = setParameterCriteria(form);
            criteria.setLocale(locale);
            // [IN054] Set user type for check action mode
            criteria.setUserType(userLogin.getSpsMUserDomain().getUserType());
            
            this.searchInitalData(form, criteria);
        } finally {
            form.setApplicationMessageList(applicationMessageList);
            if (null != output) {
                output.close();
            }
        }

        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Gets the key method map.
     * 
     * @return Map
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_RETURN,
            SupplierPortalConstant.METHOD_DO_RETURN);  
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_LINK_ACTIONMODE,
            SupplierPortalConstant.METHOD_DO_OPEN_WORD004);
        keyMethodMap.put(SupplierPortalConstant.ACTION_SAVE_AND_SEND, 
            SupplierPortalConstant.METHOD_DO_SAVE_AND_SEND);
        keyMethodMap.put(SupplierPortalConstant.ACTION_PREVIEWPENDING ,
            SupplierPortalConstant.METHOD_DO_PREVIEWPENDING);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL
            , SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE
            , SupplierPortalConstant.METHOD_DO_CLICK_LINK_MORE);
        
        // [IN054] Add new action DENSO Reply 
        keyMethodMap.put(SupplierPortalConstant.ACTION_DENSO_REPLY
            , SupplierPortalConstant.METHOD_DO_DENSO_REPLY);
        
        return keyMethodMap;
    }
    
    /**
     * Sets the criteria to search.
     * <p>Set criteria form to domain for condition of query Purchase order Information.</p>
     * 
     * @param form the Word003Form
     * @return the Purchase Order Information Domain
     */
    private PurchaseOrderAcknowledgementDomain setParameterCriteria(Word003Form form) {
        PurchaseOrderAcknowledgementDomain criteria = new PurchaseOrderAcknowledgementDomain();

        if (Constants.PAGE_SEARCH.equals(form.getFirstPages())) {
            criteria.setPageNumber(Constants.ONE);
        } else {
            criteria.setPageNumber(form.getPageNo());
        }
        
        criteria.setPoId(form.getPoId());
        criteria.setPoStatus(form.getPoStatus());
        criteria.setPoType(form.getPoType());
        criteria.setPeriodType(form.getPeriodType());
        criteria.setSpsPoNo(form.getSpsPoNo());
        
        return criteria;
    }

    /**
     * Get User login information from DensoContext general area.
     * @return User Login Domain
     * */
    private UserLoginDomain getUserLoginFromDensoContext() {
        UserLoginDomain userLogin = null;
        userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLogin;
    }
    
    /**
     * Search Initial Data by call Purchase Order Acknowledgement Facade Service.
     * @param form ActionForm
     * @param criteria search criteria
     * @return search result and error message
     * @throws ApplicationException may throw from facade when call searchInitial
     * */
    private Set<String> searchInitalData(Word003Form form,
        PurchaseOrderAcknowledgementDomain criteria) throws ApplicationException
    {
        Set<String> applicationMessageSet = new LinkedHashSet<String>();
        
        try {
            List<PurchaseOrderDetailDomain> searchResult
                = purchaseOrderAcknowledgementFacadeService.searchInitial(criteria);
            
            if(Constants.ZERO < searchResult.size()){
                PurchaseOrderDetailDomain firstPoDetail = searchResult.get(Constants.ZERO);
                form.setPoStatus(firstPoDetail.getPoStatus());
                
                Integer countPending = this.purchaseOrderAcknowledgementFacadeService
                    .searchCountPurchaseOrderDetailPending(criteria);

                if (Constants.ZERO < countPending) {
                    form.setEnablePreviewPending(Constants.STR_ONE);
                }
                SpsPagingUtil.setFormForPaging(criteria, form);
                
                // Start : [IN054] check enable DENSO Reply button
                if (SupplierPortalConstant.PO_STATUS_PENDING.equals(firstPoDetail.getPoStatus())
                    && Constants.STR_D.equals(criteria.getUserType()))
                {
                    form.setEnableDensoReply(Constants.STR_ONE);
                }
                // End : [IN054] check enable DENSO Reply button
            }
            form.setPoDetailList(searchResult);
        } catch (ApplicationException applicationException) {
            applicationMessageSet.add(applicationException.getMessage());
        } catch (Exception e) {
            applicationMessageSet.add(MessageUtil.getApplicationMessageHandledException(
                criteria.getLocale(), SupplierPortalConstant.ERROR_CD_SP_90_0001,
                new String[] {e.getClass().toString(), e.getMessage()}));
        }
        
        return applicationMessageSet;
    }
    
}