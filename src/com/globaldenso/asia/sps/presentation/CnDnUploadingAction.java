/*
 * ModifyDate Developmentcompany Describe 
 * 2014/06/11 CSI Karnrawee      Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CnDnUploadingDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDomain;
import com.globaldenso.asia.sps.business.service.CnDnUploadingFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.presentation.form.Winv006Form;

/**
 * The Class CNDNUploadingAction.
 * 
 * @author CSI
 */
public class CnDnUploadingAction extends CoreAction {

    /** The cndnUploadingFacadeService */
    private CnDnUploadingFacadeService cndnUploadingFacadeService = null;

    /** The default constructor. */
    public CnDnUploadingAction() {
        super();
    }

    /**
     * Set the CNDN uploading facade service.
     * 
     * @param cndnUploadingFacadeService the CNDN uploading facade service to
     *            set
     */
    public void setCndnUploadingFacadeService(
        CnDnUploadingFacadeService cndnUploadingFacadeService) {
        this.cndnUploadingFacadeService = cndnUploadingFacadeService;
    }

    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping mapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Winv006Form form = (Winv006Form)actionForm;
        List<UserRoleDomain> roleUserList = null;
        form.resetForm();
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();

        for (RoleScreenDomain roleScreen : userLoginDomain.getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WINV006.equals(
                roleScreen.getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }
        try {
            form.setUserLogin(setUserLogin());
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleUserList);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            dataScopeControlDomain.setLocale(locale);
            form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
            
            plantSupplierWithScopeDomain
                .setDataScopeControlDomain(dataScopeControlDomain);
            CnDnUploadingDomain cnDnUploadingDomain = cndnUploadingFacadeService
                .searchInitial(dataScopeControlDomain);
            
            form.setComment(null);
            form.setFileData(null);
            form.setSCdList(cnDnUploadingDomain.getCompanySupplierList());
            form.setCompanyDensoList(cnDnUploadingDomain.getCompanyDensoList());
            form.setSPcdList(cnDnUploadingDomain.getPlantSupplierList());
            form.setDPcdList(cnDnUploadingDomain.getPlantDensoList());
            
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV006_BRESET)){
                    this.cndnUploadingFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV006_BRESET, locale);
                }
                form.setCannotResetMessage( Constants.EMPTY_STRING );
            } catch (ApplicationException ae) {
                form.setCannotResetMessage( ae.getMessage() );
            }
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil
                .getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{e.getClass().toString(), e.getMessage()});
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            form.setApplicationMessageList(errorMessageList);
        }
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }

    /**
     * Do select supplier code using file manager stream.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSCd(ActionMapping mapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Winv006Form form = (Winv006Form)actionForm;
        UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
        List<UserRoleDomain> roleUserList = null;
        PlantSupplierDomain plantSupplierDomain = new PlantSupplierDomain();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);

        for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WINV006.equals(roleScreen
                .getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }
        plantSupplierDomain.setVendorCd(form.getVendorCd());
        dataScopeControlDomain.setUserType(userLogin.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleUserList);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        plantSupplierWithScopeDomain.setLocale(locale);
        plantSupplierWithScopeDomain
            .setPlantSupplierDomain(plantSupplierDomain);
        plantSupplierWithScopeDomain
            .setDataScopeControlDomain(dataScopeControlDomain);

        try {
            List<PlantSupplierDomain>  plantSupplierList = this.cndnUploadingFacadeService
                .searchSelectedCompanySupplier(plantSupplierWithScopeDomain);

            printJson(response, new JsonResult((List<Object>)(Object)plantSupplierList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * Do select supplier plant code.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSPcd(ActionMapping mapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Winv006Form form = (Winv006Form)actionForm;
        UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
        List<UserRoleDomain> roleUserList = null;
        PlantSupplierDomain plantSupplierDomain = new PlantSupplierDomain();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);

        for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WINV006.equals(roleScreen
                .getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }
        plantSupplierDomain.setSPcd(form.getSPcd());
        dataScopeControlDomain.setUserType(userLogin.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleUserList);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        plantSupplierWithScopeDomain.setLocale(locale);
        plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierDomain);
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);

        try {
            List<CompanySupplierDomain> companySupplierList = this.cndnUploadingFacadeService
                .searchSelectedPlantSupplier(plantSupplierWithScopeDomain);

            printJson(response, new JsonResult((List<Object>)(Object)companySupplierList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * <p>
     * Select DCd.
     * </p>
     * 
     * @param actionMapping the actionMapping
     * @param actionForm the actionForm
     * @param request the request
     * @param response the response
     * @return ActionForward
     * @throws Exception Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        /**
         * Call private action method to get current user login information and
         * roles from DENSO-context.
         */
        Winv006Form form = (Winv006Form)actionForm;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        List<UserRoleDomain> roleUserList = new ArrayList<UserRoleDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);

        /** Get roles for denso user. */
        for (RoleScreenDomain roleScreen : userLoginDomain
            .getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WINV006.equals(roleScreen
                .getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }
        dataScopeControlDomain.setUserRoleDomainList(roleUserList);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        /**
         * Call Façade Service DeliveryOrderInformationFacadeService
         * changeSelectDENSOCompany() to get the list of DENSO plant code by
         * DENSO company code.
         */
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(form.getDCd());
        plantDensoWithScopeDomain.getDataScopeControlDomain().setUserType(
            userLoginDomain.getUserType());
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setUserRoleDomainList(roleUserList);
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(null);
        plantDensoWithScopeDomain.setLocale(locale);

        List<PlantDensoDomain> plantDensoDomain = null;

        try {
            plantDensoDomain = cndnUploadingFacadeService
                .searchSelectedCompanyDenso(plantDensoWithScopeDomain);

            printJson(response, new JsonResult((List<Object>)(Object)plantDensoDomain), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }

    /**
     * <p>
     * Select DCd.
     * </p>
     * 
     * @param actionMapping the actionMapping
     * @param actionForm the actionForm
     * @param request the request
     * @param response the response
     * @return ActionForward
     * @throws Exception Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        /**
         * Call private action method to get current user login information and
         * roles from DENSO-context.
         */
        Winv006Form form = (Winv006Form)actionForm;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        List<UserRoleDomain> roleUserList = new ArrayList<UserRoleDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);

        /** Get roles for denso user. */
        for (RoleScreenDomain roleScreen : userLoginDomain
            .getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WINV006.equals(roleScreen
                .getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }
        dataScopeControlDomain.setUserRoleDomainList(roleUserList);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

        /**
         * Call Façade Service DeliveryOrderInformationFacadeService
         * changeSelectDENSOCompany() to get the list of DENSO plant code by
         * DENSO company code.
         */
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        plantDensoWithScopeDomain.getPlantDensoDomain().setDPcd(form.getDPcd());
        plantDensoWithScopeDomain.getDataScopeControlDomain().setUserType(
            userLoginDomain.getUserType());
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setUserRoleDomainList(roleUserList);
        plantDensoWithScopeDomain.getDataScopeControlDomain()
            .setDensoSupplierRelationDomainList(null);
        plantDensoWithScopeDomain.setLocale(locale);

        List<CompanyDensoDomain> companyDensoList = null;

        try {
            companyDensoList = cndnUploadingFacadeService
                .searchSelectedPlantDenso(plantDensoWithScopeDomain);

            printJson(response, new JsonResult((List<Object>)(Object)companyDensoList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }

    /**
     * Do upload, save file using file manager stream.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doUpload(ActionMapping mapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        int fileSize;
        String message = null;
        Winv006Form form = (Winv006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(setUserLogin());
        int maxCsvFileSize = Constants.ZERO;

        CnDnUploadingDomain cnDnUploadingDomain = new CnDnUploadingDomain();
        try {
            if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                RoleTypeConstants.WINV006_BUPLOAD)){
                this.cndnUploadingFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV006_BUPLOAD, locale);
            }
            
            fileSize = form.getFileData().getFileSize();
            maxCsvFileSize = ContextParams.getMaxCsvFileSize();

            FileManagementDomain fileManagementDomain = new FileManagementDomain();
            UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
            SpsTFileUploadDomain spsTFileUploadDomain = new SpsTFileUploadDomain();

            fileManagementDomain.setFileData(form.getFileData().getInputStream());
            fileManagementDomain.setFileName(form.getFileData().getFileName());
            fileManagementDomain.setUploadUser(userLogin.getDscId());
            fileManagementDomain.setLastUpdateUser(userLogin.getDscId());

            spsTFileUploadDomain.setLocale(locale);
            spsTFileUploadDomain.setVendorCd(form.getVendorCd());
            spsTFileUploadDomain.setSPcd(form.getSPcd());
            spsTFileUploadDomain.setDCd(form.getDCd());
            spsTFileUploadDomain.setDPcd(form.getDPcd());
            spsTFileUploadDomain.setFileName(form.getFileData().getFileName());
            spsTFileUploadDomain.setFileComment(form.getComment().toUpperCase().trim());
            spsTFileUploadDomain.setCreateDscId(userLogin.getSpsMUserDomain()
                .getDscId());
            spsTFileUploadDomain.setLastUpdateDscId(userLogin
                .getSpsMUserDomain().getDscId());

            cnDnUploadingDomain = cndnUploadingFacadeService
                .transactUploadCnDn(fileManagementDomain, spsTFileUploadDomain,
                    fileSize, maxCsvFileSize);

            if (Constants.ZERO == cnDnUploadingDomain.getErrorMessageList().size()) {
                form.setMessageType(Constants.MESSAGE_SUCCESS);
                message = MessageUtil.getApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_I6_0010);
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_SUCCESS, message));
            } else {
                errorMessageList = cnDnUploadingDomain.getErrorMessageList();
            }
        } catch (ApplicationException ae) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
        } catch (Exception e) {
            message = MessageUtil.getApplicationMessageHandledException(locale,
                SupplierPortalConstant.ERROR_CD_SP_80_0010);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            List<UserRoleDomain> roleUserList = null;
            PlantSupplierDomain plantSupplierDomain = new PlantSupplierDomain();
            DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();

            for (RoleScreenDomain roleScreen : userLoginDomain.getRoleScreenDomainList()) {
                if (SupplierPortalConstant.SCREEN_ID_WINV006.equals(roleScreen
                    .getSpsMScreenDomain().getScreenCd())) {
                    roleUserList = roleScreen.getUserRoleDomainList();
                }
            }

            plantSupplierDomain.setVendorCd(form.getVendorCd());
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleUserList);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

            CnDnUploadingDomain cnDnUploadingDomainInitial = this.cndnUploadingFacadeService
                .searchInitial(dataScopeControlDomain);

            form.setCompanyDensoList(cnDnUploadingDomainInitial.getCompanyDensoList());
            form.setSCdList(cnDnUploadingDomainInitial.getCompanySupplierList());

            PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
                new PlantSupplierWithScopeDomain();
            plantSupplierWithScopeDomain
                .setPlantSupplierDomain(plantSupplierDomain);
            plantSupplierWithScopeDomain
                .setDataScopeControlDomain(dataScopeControlDomain);

            List<PlantSupplierDomain> plantSupplierList = this.cndnUploadingFacadeService
                .searchSelectedCompanySupplier(plantSupplierWithScopeDomain);
            form.setSPcdList(plantSupplierList);

            PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(form.getDCd());
            plantDensoWithScopeDomain.getDataScopeControlDomain().setUserType(
                userLoginDomain.getUserType());
            plantDensoWithScopeDomain.getDataScopeControlDomain().setUserRoleDomainList(
                roleUserList);
            plantDensoWithScopeDomain.getDataScopeControlDomain()
                .setDensoSupplierRelationDomainList(null);
            plantDensoWithScopeDomain.setLocale(locale);

            List<PlantDensoDomain> plantDensoDomain = new ArrayList<PlantDensoDomain>();
            plantDensoDomain = this.cndnUploadingFacadeService
                .searchSelectedCompanyDenso(plantDensoWithScopeDomain);
            form.setDPcdList(plantDensoDomain);
            
            form.setApplicationMessageList(errorMessageList);
            if (null != form.getFileData()) {
                form.getFileData().destroy();
                form.setFileName(form.getFileData().getFileName());
                form.setFileData(form.getFileData());
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**
     * Do search.
     * <p>Search data by criteria from screen.</p>
     * <ul>
     * <li>In case click search button, system search data and count by criteria 
     * if count result size is 0 return message to screen "data not found".</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReset(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Winv006Form form = (Winv006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);

        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();
            
        try {
            form.setUserLogin(super.setUserLogin());
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WINV006_BRESET)) 
            {
                this.cndnUploadingFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV006_BRESET, locale);
            }
            form.resetForm();
            form.setUserLogin(super.setUserLogin());
        } catch (ApplicationException ae) {
            
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
            form.setApplicationMessageList(errorMessageList);
        } finally {
            List<UserRoleDomain> roleUserList = null;
            PlantSupplierDomain plantSupplierDomain = new PlantSupplierDomain();
            DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();

            for (RoleScreenDomain roleScreen : userLoginDomain.getRoleScreenDomainList()) {
                if (SupplierPortalConstant.SCREEN_ID_WINV006.equals(roleScreen
                    .getSpsMScreenDomain().getScreenCd())) {
                    roleUserList = roleScreen.getUserRoleDomainList();
                }
            }

            plantSupplierDomain.setVendorCd(form.getVendorCd());
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleUserList);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);

            CnDnUploadingDomain cnDnUploadingDomainInitial = this.cndnUploadingFacadeService
                .searchInitial(dataScopeControlDomain);

            form.setCompanyDensoList(cnDnUploadingDomainInitial.getCompanyDensoList());
            form.setSCdList(cnDnUploadingDomainInitial.getCompanySupplierList());

            PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
                new PlantSupplierWithScopeDomain();
            plantSupplierWithScopeDomain
                .setPlantSupplierDomain(plantSupplierDomain);
            plantSupplierWithScopeDomain
                .setDataScopeControlDomain(dataScopeControlDomain);

            List<PlantSupplierDomain> plantSupplierList = this.cndnUploadingFacadeService
                .searchSelectedCompanySupplier(plantSupplierWithScopeDomain);
            form.setSPcdList(plantSupplierList);

            PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(form.getDCd());
            plantDensoWithScopeDomain.getDataScopeControlDomain().setUserType(
                userLoginDomain.getUserType());
            plantDensoWithScopeDomain.getDataScopeControlDomain().setUserRoleDomainList(
                roleUserList);
            plantDensoWithScopeDomain.getDataScopeControlDomain()
                .setDensoSupplierRelationDomainList(null);
            plantDensoWithScopeDomain.setLocale(locale);

            List<PlantDensoDomain> plantDensoDomain = new ArrayList<PlantDensoDomain>();
            plantDensoDomain = this.cndnUploadingFacadeService
                .searchSelectedCompanyDenso(plantDensoWithScopeDomain);
            form.setDPcdList(plantDensoDomain);
            
            form.setApplicationMessageList(errorMessageList);
            if (null != form.getFileData()) {
                form.getFileData().destroy();
                form.setFileName(form.getFileData().getFileName());
                form.setFileData(form.getFileData());
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap = new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_SEARCH,
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_RESET);
        keyMethodMap.put(SupplierPortalConstant.ACTION_UPLOAD,
            SupplierPortalConstant.METHOD_DO_UPLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_SCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_SPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_DPCD);
        return keyMethodMap;
    }

    /**
     * Method to get current user login information detail from DENSO context.
     * <p>
     * Set UserLoginDomain from current user login detail.
     * </p>
     * 
     * @return the user login domain
     */
    private UserLoginDomain getCurrentUserLoginInformation() {
        UserLoginDomain userLoginDomain = new UserLoginDomain();
        userLoginDomain = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }
}