/*
 * ModifyDate Developmentcompany Describe 
 * 2014/08/15 CSI Parichat      Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.AsnUploadingDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.AsnUploadingFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Wshp006Form;

/**
 * The Class AsnUploadingAction.
 * @author CSI
 */
public class AsnUploadingAction extends CoreAction {
    

    /** The asn uploading facade service */
    private AsnUploadingFacadeService asnUploadingFacadeService; 
    
    /** The default constructor. */
    public AsnUploadingAction() {
        super();
    }

    /**
     * Set the asn uploading facade service.
     * 
     * @param asnUploadingFacadeService the asn uploading facade service to set
     */
    public void setAsnUploadingFacadeService(AsnUploadingFacadeService asnUploadingFacadeService) {
        this.asnUploadingFacadeService = asnUploadingFacadeService;
    }
    
    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp006Form form = (Wshp006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLoginDomain = this.getUserLoginFromDensoContext();
        HttpSession session = request.getSession();
        form.resetForm();
        form.setSessionId(session.getId());
        form.setUploadDscId(userLoginDomain.getDscId());
        this.initialScreen(form);
        form.setUserLogin(this.setUserLogin());
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        try{
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WSHP006_BRESET)){
                this.asnUploadingFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP006_BRESET, locale);
            }
        }catch(ApplicationException ex){
            form.setCannotResetMessage(ex.getMessage());
        }
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }

    /**
     * Do upload.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doUpload(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp006Form form = (Wshp006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AsnUploadingDomain asnUploadingCriteria = new AsnUploadingDomain();
        AsnUploadingDomain asnUploadingResult = new AsnUploadingDomain();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        UserLoginDomain userLoginDomain = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        try{
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WSHP006_BUPLOAD)){
                this.asnUploadingFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP006_BUPLOAD, locale);
            }
            
            asnUploadingCriteria.setSessionId(form.getSessionId());
            asnUploadingCriteria.setUploadDscId(form.getUploadDscId());
            asnUploadingCriteria.setLocale(locale);
            String firstPage = form.getFirstPages();
            if(Constants.PAGE_SEARCH.equals(firstPage)){
                asnUploadingCriteria.setPageNumber(Constants.ONE);
            }else{
                asnUploadingCriteria.setPageNumber(form.getPageNo());
            }
            
            //1. Clear upload temporary table with session
            asnUploadingFacadeService.deleteTmpUploadAsn(asnUploadingCriteria);
            
            //2. Validate in physical file
            if(StringUtil.checkNullOrEmpty(form.getFileData())){
                throw new ApplicationException(MessageUtil.getApplicationMessageHandledException(
                    locale, SupplierPortalConstant.ERROR_CD_SP_E7_0014));
            }
            if(Constants.ZERO == form.getFileData().getFileSize()){
                throw new ApplicationException(MessageUtil.getApplicationMessageHandledException(
                    locale, SupplierPortalConstant.ERROR_CD_SP_E7_0014));
            }
            if(ContextParams.getMaxCsvFileSize() < form.getFileData().getFileSize()){
                throw new ApplicationException(MessageUtil.getApplicationMessageHandledException(
                    locale, SupplierPortalConstant.ERROR_CD_SP_E7_0015,
                    new String[] {String.valueOf(ContextParams.getMaxCsvFileSize())}));
            }
            String fileName = form.getFileData().getFileName();
            if(!Constants.REPORT_CSV_FORMAT.equalsIgnoreCase(
                fileName.substring(fileName.lastIndexOf(Constants.SYMBOL_DOT)
                    + Constants.ONE, fileName.length()).toLowerCase()))
            {
                throw new ApplicationException(MessageUtil.getApplicationMessageWithLabel(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_E7_0013, 
                    SupplierPortalConstant.LBL_CSV));
            }
            
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            List<RoleScreenDomain> roleScreenDomainList = userLoginDomain.getRoleScreenDomainList();
            for(RoleScreenDomain roleScreen : roleScreenDomainList){
                if(roleScreen.getSpsMScreenDomain().getScreenCd().
                    equals(SupplierPortalConstant.SCREEN_ID_WSHP001)){
                    dataScopeControlDomain.setUserRoleDomainList(roleScreen.
                        getUserRoleDomainList()); 
                    break;
                }
            }
            //asnUploadingCriteria.setDataScopeControlDomain(dataScopeControlDomain);
            dataScopeControlDomain.setLocale(locale);
            this.asnUploadingFacadeService.searchDensoSupplierRelation(dataScopeControlDomain);
            
            asnUploadingCriteria.setSupplierAuthenList(
                super.doCreateSupplierPlantAuthenList(
                    dataScopeControlDomain.getDensoSupplierRelationDomainList(),
                    SupplierPortalConstant.SCREEN_ID_WSHP006));
            asnUploadingCriteria.setDensoAuthenList(
                super.doCreateDensoPlantAuthenList(
                    dataScopeControlDomain.getDensoSupplierRelationDomainList(),
                    SupplierPortalConstant.SCREEN_ID_WSHP006));
            asnUploadingCriteria.setFileInputStream(form.getFileData().getInputStream());
            
            asnUploadingResult = asnUploadingFacadeService.transactUploadAsn(asnUploadingCriteria);
            if(null == asnUploadingResult){
                applicationMessageList.addAll(asnUploadingCriteria.getErrorMessageList());
            }else{
                boolean isSuccess = true;
                SpsPagingUtil.setFormForPaging(asnUploadingResult, form);
                form.setAsnNo(asnUploadingResult.getAsnNo());
                form.setTotalRecord(String.valueOf(asnUploadingResult.getTotalRecord()));
                form.setCorrectRecord(String.valueOf(asnUploadingResult.getCorrectRecord()));
                form.setWarningRecord(String.valueOf(asnUploadingResult.getWarningRecord()));
                form.setErrorRecord(String.valueOf(asnUploadingResult.getErrorRecord()));
                form.setVendorCd(asnUploadingResult.getVendorCd());
                form.setSPcd(asnUploadingResult.getSPcd());
                form.setDCd(asnUploadingResult.getDCd());
                form.setDPcd(asnUploadingResult.getDPcd());
                form.setTmpUploadAsnList(asnUploadingResult.getTmpUploadAsnResultList());
                if(null != asnUploadingResult.getErrorMessageList() 
                    && !asnUploadingResult.getErrorMessageList().isEmpty()){
                    isSuccess = false;
                    form.setMode(Constants.EMPTY_STRING);
                    applicationMessageList.addAll(asnUploadingResult.getErrorMessageList());
                }else{
                    form.setMode(Constants.STR_UPLOAD);
                    if(null != asnUploadingResult.getWarningMessageList() 
                        && !asnUploadingResult.getWarningMessageList().isEmpty()){
                        applicationMessageList.addAll(asnUploadingResult.getWarningMessageList());
                    }
                    applicationMessageList.add(new ApplicationMessageDomain(
                        Constants.MESSAGE_SUCCESS, MessageUtil.getApplicationMessage(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_I6_0010)));
                }
                if(Constants.ZERO < asnUploadingResult.getErrorRecord() || !isSuccess){
                    form.setHasErrorFlag(Constants.STR_ONE);
                }else{
                    form.setHasErrorFlag(Constants.STR_ZERO);
                }
            }
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                ex.getMessage()));
        }catch(IOException ie){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(
                    locale, SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{Constants.ERROR, ie.getMessage()})));
        }catch(Exception e){
            applicationMessageList = new ArrayList<ApplicationMessageDomain>();
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()}))); 
        }finally{
            form.setApplicationMessageList(applicationMessageList);
            form.getFileData().destroy();
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do download.
     * <p>Export upload result to CSV file see detail</p>
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownload(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp006Form form = (Wshp006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        ServletOutputStream outputStream = null;
        AsnUploadingDomain asnUploadingCriteria = new AsnUploadingDomain();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        UserLoginDomain userLoginDomain = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        try{
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WSHP006_BDOWNLOAD)){
                this.asnUploadingFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP006_BDOWNLOAD, locale);
            }
            
            asnUploadingCriteria.setLocale(locale);
            asnUploadingCriteria.setUploadDscId(form.getUploadDscId());
            asnUploadingCriteria.setSessionId(form.getSessionId());
            asnUploadingCriteria.setAsnNo(form.getAsnNo());
            asnUploadingCriteria.setVendorCd(form.getVendorCd());
            asnUploadingCriteria.setSPcd(form.getSPcd());
            asnUploadingCriteria.setDCd(form.getDCd());
            asnUploadingCriteria.setDPcd(form.getDPcd());
            
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            List<RoleScreenDomain> roleScreenDomainList = userLoginDomain.getRoleScreenDomainList();
            for(RoleScreenDomain roleScreen : roleScreenDomainList){
                if(roleScreen.getSpsMScreenDomain().getScreenCd().
                    equals(SupplierPortalConstant.SCREEN_ID_WSHP006)){
                    dataScopeControlDomain.setUserRoleDomainList(roleScreen.
                        getUserRoleDomainList()); 
                    break;
                }
            }
            asnUploadingCriteria.setDataScopeControlDomain(dataScopeControlDomain);
            asnUploadingFacadeService.searchTmpUploadAsnCsv(asnUploadingCriteria);
            if(!StringUtil.checkNullOrEmpty(asnUploadingCriteria.getAsnInformationForCsv()))
            {
                Calendar cal = Calendar.getInstance();
                String fileName = StringUtil.appendsString(
                    Constants.ASN_UPLOAD_RESULT_LIST_FILE_NAME,
                    Constants.SYMBOL_UNDER_SCORE,
                    DateUtil.format(cal.getTime(), DateUtil.PATTERN_YYYYMMDD_HHMM));
                StringBuffer asnInformationForCsv = asnUploadingCriteria.getAsnInformationForCsv();
                if(null != asnInformationForCsv){
                    byte[] output = asnInformationForCsv.toString().getBytes();
                    setHttpHeaderForCsv(response, fileName);
                    outputStream = response.getOutputStream();
                    outputStream.write(output, Constants.ZERO, output.length);
                    outputStream.flush();
                    return null;
                }
            }
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                ex.getMessage()));
        }catch(IOException io){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_80_0012)));
        }catch(Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(null != outputStream){
                outputStream.close();
            }
        }
        form.setApplicationMessageList(applicationMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do reset.
     * <p>In case of clicking Reset button, the system will reset upload summary and detail section.</p>
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReset(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp006Form form = (Wshp006Form)actionForm;
        UserLoginDomain userLoginDomain = this.getUserLoginFromDensoContext();
        HttpSession session = request.getSession();
        form.resetForm();
        form.setSessionId(session.getId());
        form.setUploadDscId(userLoginDomain.getDscId());
        this.initialScreen(form);
        form.setUserLogin(this.setUserLogin());
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do open specified page.
     * <p>In case of search uploaded asn data on screen WSHP006. Operator click or change paging.</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doOpenSpecifiedPage(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp006Form form = (Wshp006Form)actionForm;
        boolean hasError = false;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AsnUploadingDomain asnUploadingCriteria = new AsnUploadingDomain();
        AsnUploadingDomain asnUploadingResult = new AsnUploadingDomain();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        UserLoginDomain userLoginDomain = this.getUserLoginFromDensoContext();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        form.setUploadDscId(userLoginDomain.getDscId());
        if(Constants.STR_RETURN.equals(form.getMode()))
        {
            Wshp006Form sessionForm = (Wshp006Form)DensoContext.get().getGeneralArea(
                SupplierPortalConstant.SESSION_WSHP006_FORM);
            form.setFileNameUpload(sessionForm.getFileNameUpload());
            form.setCannotResetMessage(sessionForm.getCannotResetMessage());
            
            DensoContext.get().removeGeneralArea(SupplierPortalConstant.SESSION_WSHP006_FORM);
            form.setPages(null);
            form.setFirstCount(Constants.FIVE);
            form.setFirstPages(Constants.PAGE_SEARCH);
        }
        
        String firstPage = form.getFirstPages();
        if(Constants.PAGE_SEARCH.equals(firstPage)){
            asnUploadingCriteria.setPageNumber(Constants.ONE);
        }else{
            asnUploadingCriteria.setPageNumber(form.getPageNo());
        }
        try{
            asnUploadingCriteria.setSessionId(form.getSessionId());
            asnUploadingCriteria.setUploadDscId(form.getUploadDscId());
            asnUploadingCriteria.setAsnNo(form.getAsnNo());
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            List<RoleScreenDomain> roleScreenDomainList = userLoginDomain.getRoleScreenDomainList();
            for(RoleScreenDomain roleScreen : roleScreenDomainList){
                if(roleScreen.getSpsMScreenDomain().getScreenCd().
                    equals(SupplierPortalConstant.SCREEN_ID_WSHP006)){
                    dataScopeControlDomain.setUserRoleDomainList(roleScreen.
                        getUserRoleDomainList()); 
                    break;
                }
            }
            asnUploadingCriteria.setDataScopeControlDomain(dataScopeControlDomain);
            asnUploadingResult = asnUploadingFacadeService.searchByReturn(asnUploadingCriteria);
            SpsPagingUtil.setFormForPaging(asnUploadingResult, form);
            form.setTotalRecord(String.valueOf(asnUploadingResult.getTotalRecord()));
            form.setCorrectRecord(String.valueOf(asnUploadingResult.getCorrectRecord()));
            form.setWarningRecord(String.valueOf(asnUploadingResult.getWarningRecord()));
            form.setErrorRecord(String.valueOf(asnUploadingResult.getErrorRecord()));
            form.setAsnNo(asnUploadingResult.getAsnNo());
            form.setVendorCd(asnUploadingResult.getVendorCd());
            form.setSPcd(asnUploadingResult.getSPcd());
            form.setDCd(asnUploadingResult.getDCd());
            form.setDPcd(asnUploadingResult.getDPcd());
            form.setTmpUploadAsnList(asnUploadingResult.getTmpUploadAsnResultList());
            if(null != asnUploadingResult.getErrorMessageList() 
                && !asnUploadingResult.getErrorMessageList().isEmpty()){
                hasError = true;
                applicationMessageList.addAll(asnUploadingResult.getErrorMessageList());
            }else{
                form.setMode(Constants.STR_UPLOAD);
                if(null != asnUploadingResult.getWarningMessageList() 
                    && !asnUploadingResult.getWarningMessageList().isEmpty()){
                    applicationMessageList.addAll(asnUploadingResult.getWarningMessageList());
                }
                applicationMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_SUCCESS, MessageUtil.getApplicationMessage(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_I6_0010)));
            }
            if(Constants.ZERO < asnUploadingResult.getErrorRecord()){
                form.setHasErrorFlag(Constants.STR_ONE);
            }else{
                form.setHasErrorFlag(Constants.STR_ZERO);
            }
        }catch(ApplicationException ex){
            hasError = true;
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                ex.getMessage()));
        }catch (Exception e) {
            hasError = true;
            applicationMessageList = new ArrayList<ApplicationMessageDomain>();
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(
                    locale, SupplierPortalConstant.ERROR_CD_SP_90_0002)));
        }finally{
            form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
            form.setUserLogin(super.setUserLogin());
            form.setApplicationMessageList(applicationMessageList);
            if(hasError){
                form.setMode(Constants.EMPTY_STRING);
                form.setFileNameUpload(Constants.EMPTY_STRING);
                form.setTmpUploadAsnList(null);
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do register.
     * <p>In case of clicking on Register button, the system will open screen Wshp004.</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doRegister(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp006Form form = (Wshp006Form)actionForm;
        StringBuffer forward = new StringBuffer();
        
        List<ApplicationMessageDomain> appMsgList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLoginDomain = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        try{
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WSHP006_BREGISTER)){
                this.asnUploadingFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP006_BREGISTER, locale);
            }
            forward.append(SupplierPortalConstant.URL_WSHP004_ACTION);
            forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
            forward.append(SupplierPortalConstant.METHOD_DO_INITIAL);
            forward.append(SupplierPortalConstant.URL_PARAM_SESSIONID);
            forward.append(form.getSessionId());
            forward.append(SupplierPortalConstant.URL_PARAM_ASNNO);
            forward.append(form.getAsnNo());
            forward.append(SupplierPortalConstant.URL_PARAM_SCREENID);
            forward.append(SupplierPortalConstant.SCREEN_ID_WSHP006);
            forward.append(SupplierPortalConstant.URL_PARAM_TEMPORARY_MODE);
            forward.append(Constants.STR_ONE);
            DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WSHP006_FORM, form);
            return new ActionForward(forward.toString(), true);
        }catch(ApplicationException e){
            appMsgList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, e.getMessage()));
        }catch(Exception e){
            appMsgList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        form.setApplicationMessageList(appMsgList);
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap()
    {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL, 
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_UPLOAD, 
            SupplierPortalConstant.METHOD_DO_UPLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET, 
            SupplierPortalConstant.METHOD_DO_RESET);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD, 
            SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_OPEN_SPECIFIED_PAGE, 
            SupplierPortalConstant.METHOD_DO_OPEN_SPECIFIED_PAGE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_REGISTER, 
            SupplierPortalConstant.METHOD_DO_REGISTER);
        return keyMethodMap;
    }
    
    /**
     * Method for initial screen when click MainMenu - Shipping - ASN Uploading and Reset Button
     * @param form Wshp006Form
     * */
    private void initialScreen(Wshp006Form form){
        List<ApplicationMessageDomain> applicationMsgList
            = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AsnUploadingDomain asnUploadingCriteria = new AsnUploadingDomain();
        try{
            //Clear upload temporary table with session
            asnUploadingCriteria.setUploadDscId(form.getUploadDscId());
            asnUploadingCriteria.setSessionId(form.getSessionId());
            asnUploadingFacadeService.deleteTmpUploadAsn(asnUploadingCriteria);
            //Clear upload temporary table with current date - 2 days
            asnUploadingCriteria = new AsnUploadingDomain();
            asnUploadingFacadeService.deleteTmpUploadAsn(asnUploadingCriteria);
        }catch(Exception e){
            applicationMsgList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        form.getApplicationMessageList().addAll(applicationMsgList);
    }
    
    /**
     * Get User login information from DensoContext general area.
     * @return User Login Domain
     * */
    private UserLoginDomain getUserLoginFromDensoContext()
    {
        UserLoginDomain userLogin = null;
        userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLogin;
    }
}
