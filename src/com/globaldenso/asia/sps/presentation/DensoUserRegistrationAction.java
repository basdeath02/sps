/*
 * ModifyDate Development company     Describe 
 * 2014/06/19 CSI Phakaporn           Create
 * 2016/02/10 CSI Akat                [IN049]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoUserRegistrationDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserDensoDetailDomain;
import com.globaldenso.asia.sps.business.domain.UserDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.DensoUserRegistrationFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Wadm006Form;

/**
 * The Class DensoUserRegistrationAction.
 * <p>
 * Receive value from WADM006 for initial value and send to DensoUserRegistrationFacadeService
 * for insert update 
 * </p>
 * <ul>
 * <li>Method search : doInitial</li>
 * <li>Method search : doInitialWithCriteria</li>
 * <li>Method create : doRegister</li>
 * <li>Method update : doUpdate</li>
 * <li>Method search : doReturn</li>
 * <li>Method search : doSelectDCd</li>
 * <li>Method search : doSelectDPcd</li>
 * </ul>
 * 
 * @author CSI
 */
public class DensoUserRegistrationAction extends CoreAction {
    
    /** The DENSO User facade service. */
    private DensoUserRegistrationFacadeService densoUserRegistrationFacadeService;

    /**
     * Instantiates a new DensoUserRegistrationAction.
     */
    public DensoUserRegistrationAction() {
        super();
    }
    
    /**
     * Sets the DENSO User Register Facade Service.
     * 
     * @param densoUserRegistrationFacadeService the DENSO User Facade Service.
     */
    public void setDensoUserRegistrationFacadeService(
            DensoUserRegistrationFacadeService densoUserRegistrationFacadeService) {
        this.densoUserRegistrationFacadeService = densoUserRegistrationFacadeService;
    }

    /**
     * Do initial.
     * <p>
     * Initial data when load page.
     * </p>
     * <ul>
     * <li>In case click Menu from 'MainMenu (DENSO User Registration) link , Reset button</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping actionMapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        Wadm006Form form = (Wadm006Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String dscId = form.getDscId();
        DensoUserRegistrationDomain result = new DensoUserRegistrationDomain();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setUserLogin(setUserLogin());
        /*set menu code list for initial menu bar*/
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Call Facade Service for initial*/
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        try{
            result = densoUserRegistrationFacadeService.searchInitialRegister(
                dataScopeControlDomain);
            /*Set value from domain to ActionForm*/
            form.resetForm();
            List<CompanyDensoDomain> companyDensoList = result.getCompanyDensoList();
            form.setCompanyDensoList(companyDensoList);
            form.setPlantDensoList(result.getPlantDensoList());
            form.setDepartmentList(result.getDepartmentList());
            form.setMaxDepartment(result.getDepartmentList().size());
            if(Constants.MODE_EDIT.equals(form.getMode())){
                form.setDscId(dscId);
            }else{
                form.setMode(Constants.MODE_REGISTER);
            }
            
            try{
                if(!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WADM006_BRESET)){
                    this.densoUserRegistrationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WADM006_BRESET, locale);
                }
            }catch(ApplicationException ex){
                form.setCannotResetMessage(ex.getMessage());
            }
            
        }catch(ApplicationException applicationException){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                applicationException.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Initial With Criteria from WADM005 pages.
     * <p> 
     * initial data by criteria for update data. 
     * </p>
     * <ul>
     * <li>In case initial screen by criteria from WADM005 pages.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitialWithCriteria(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception
    {
        Wadm006Form form = (Wadm006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        DensoUserRegistrationDomain criteria = new DensoUserRegistrationDomain();
        DensoUserRegistrationDomain result = new DensoUserRegistrationDomain();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setUserLogin(setUserLogin());
        /*set menu code list for initial menu bar*/
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument domain*/
        UserDensoDetailDomain densoUserDetailDomain = new UserDensoDetailDomain();
        UserDomain userDomain = new UserDomain();
        userDomain.setDscId(form.getDscId());
        densoUserDetailDomain.setUserDomain(userDomain);
        criteria.setUserDensoDetailDomain(densoUserDetailDomain);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        criteria.setDataScopeControlDomain(dataScopeControlDomain);
        criteria.setLocale(locale);
        
        /*Call Facade Service for initial Edit*/
        try {
            result = densoUserRegistrationFacadeService.searchInitialEdit(criteria);
            /*Set value from domain to ActionForm*/
            UserDensoDetailDomain resultDetailDomain = result.getUserDensoDetailDomain();
            form.setDscId(resultDetailDomain.getSpsMUserDomain().getDscId());
            form.setEmployeeCode(resultDetailDomain.getSpsMUserDensoDomain().getEmployeeCd());
            form.setDCd(resultDetailDomain.getSpsMUserDensoDomain().getDCd());
            form.setDPcd(resultDetailDomain.getSpsMUserDensoDomain().getDPcd());
            form.setFirstName(resultDetailDomain.getSpsMUserDomain().getFirstName());
            form.setMiddleName(resultDetailDomain.getSpsMUserDomain().getMiddleName());
            form.setLastName(resultDetailDomain.getSpsMUserDomain().getLastName());
            form.setDepartmentName(resultDetailDomain.getSpsMUserDomain().getDepartmentName());
            form.setEmlCreateCancelAsnFlag(
                resultDetailDomain.getSpsMUserDensoDomain().getEmlCreateCancelAsnFlag());
            form.setEmlReviseAsnFlag(
                resultDetailDomain.getSpsMUserDensoDomain().getEmlReviseAsnFlag());
            form.setEmlCreateInvoiceFlag(
                resultDetailDomain.getSpsMUserDensoDomain().getEmlCreateInvoiceFlag());
            // Start : [IN049] Change Column Name
            //form.setEmlAbnormalTransferData1Flag(
            //    resultDetailDomain.getSpsMUserDensoDomain().getEmlAbnormalTransfer1Flag());
            //form.setEmlAbnormalTransferData2Flag(
            //    resultDetailDomain.getSpsMUserDensoDomain().getEmlAbnormalTransfer2Flag());
            form.setEmlPedpoDoalcasnUnmatpnFlg(
                resultDetailDomain.getSpsMUserDensoDomain().getEmlPedpoDoalcasnUnmatpnFlg());
            form.setEmlAbnormalTransferFlg(
                resultDetailDomain.getSpsMUserDensoDomain().getEmlAbnormalTransferFlg());
            // End : [IN049] Change Column Name
            form.setEmail(resultDetailDomain.getSpsMUserDomain().getEmail());
            form.setTelephone(resultDetailDomain.getSpsMUserDomain().getTelephone());
            form.setUpdateDatetime(result.getUpdateDatetime());
            form.setUpdateDatetimeDenso(result.getUpdateDatetimeDenso());
            if(null != result.getDepartmentList()){
                form.setDepartmentList(result.getDepartmentList());
                form.setMaxDepartment(result.getDepartmentList().size());
            }
            form.setPlantDensoList(result.getPlantDensoList());
            form.setCompanyDensoList(result.getCompanyDensoList());
            form.setMode(Constants.MODE_EDIT);
            
            try{
                if(!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WADM006_BRESET)){
                    this.densoUserRegistrationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WADM006_BRESET, locale);
                }
            }catch(ApplicationException ex){
                form.setCannotResetMessage(ex.getMessage());
            }
            
        }catch(ApplicationException applicationException){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                applicationException.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Search DENSO Plant Code by selected DENSO Company Code and filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm006Form form = (Wadm006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<PlantDensoDomain> plantDensoList =  new ArrayList<PlantDensoDomain>();
        DensoUserRegistrationDomain criteria = new DensoUserRegistrationDomain();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Set user login information*/
        form.setUserLogin(setUserLogin());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        try{
            if(!Constants.MISC_CODE_ALL.equals(form.getDCd())){
                criteria.setDCd(form.getDCd());
            }
            criteria.setLocale(locale);
            criteria.setDataScopeControlDomain(dataScopeControlDomain);
            plantDensoList = densoUserRegistrationFacadeService.searchSelectedCompanyDenso(
                criteria);
            printJson(response, new JsonResult((List<Object>)(Object)plantDensoList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * Search company DENSO Code by selected plant DENSO Code and filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm006Form form = (Wadm006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanyDensoDomain> companyList = null;
        
        // 1. Call private action method to get current user login and roles from DENSO-context
        UserLoginDomain userLogin = this.setUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        // 2. Get roles for supplier user
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLogin);
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLogin.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        try{
            /* 3. Call Facade Service AcknowledgedDoInformationFacadeService
             * searchSelectedPlantDenso() to get the list of supplier plant code by supplier
             * company code
             * */
            PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getDPcd())
                && !Constants.STR_NINETY_NINE.equals(form.getDPcd())) {
                plantDenso.getPlantDensoDomain().setDPcd(form.getDPcd());
            }
            plantDenso.setLocale(locale);
            plantDenso.setDataScopeControlDomain(dataScopeControlDomain);
            companyList = this.densoUserRegistrationFacadeService.searchSelectedPlantDenso(
                plantDenso);
            
            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * Do Register DENSO User.
     * <p> create data with parameter from screen.
     * </p>
     * <ul>
     * <li>In case click Register button, System will register DENSO user.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doRegister(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm006Form form = (Wadm006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        DensoUserRegistrationDomain criteria = new DensoUserRegistrationDomain();
        DensoUserRegistrationDomain result = new DensoUserRegistrationDomain();
        UserDensoDetailDomain densoUserDetailDomain = new UserDensoDetailDomain();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        boolean isRegisterSuccess = false;
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setUserLogin(setUserLogin());
        /*set menu code list for initial menu bar*/
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        String userDscId = userLoginDomain.getDscId();
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        criteria.setDataScopeControlDomain(dataScopeControlDomain);
        
        /*Set value to argument Domain.*/
        String dscId = this.emptyToNull(form.getDscId());
        String employeeCode = this.emptyToNull(form.getEmployeeCode());
        String firstName = this.emptyToNull(form.getFirstName());
        String middleName = this.emptyToNull(form.getMiddleName());
        String lastName = this.emptyToNull(form.getLastName());
        String email = this.emptyToNull(form.getEmail());
        String telephone = this.emptyToNull(form.getTelephone());
        String departmentName = this.emptyToNull(form.getDepartmentName());
        
        UserDomain userDomain = new UserDomain();
        SpsMUserDensoDomain densoDomain = new SpsMUserDensoDomain();
        userDomain.setDscId(dscId);
        userDomain.setFirstName(firstName);
        userDomain.setMiddleName(middleName);
        userDomain.setLastName(lastName);
        userDomain.setEmail(email);
        userDomain.setTelephone(telephone);
        userDomain.setDepartmentName(departmentName);
        userDomain.setIsActive(Constants.IS_ACTIVE);
        userDomain.setCreateUser(userDscId);
        userDomain.setUpdateUser(userDscId);
        
        densoDomain.setDscId(dscId);
        densoDomain.setEmployeeCd(employeeCode);
        densoDomain.setDCd(form.getDCd());
        densoDomain.setDPcd(form.getDPcd());
        densoDomain.setEmlCreateCancelAsnFlag(form.getEmlCreateCancelAsnFlag());
        densoDomain.setEmlReviseAsnFlag(form.getEmlReviseAsnFlag());
        densoDomain.setEmlCreateInvoiceFlag(form.getEmlCreateInvoiceFlag());

        // Start : [IN049] Change Column Name
        //densoDomain.setEmlAbnormalTransfer1Flag(form.getEmlAbnormalTransferData1Flag());
        //densoDomain.setEmlAbnormalTransfer2Flag(form.getEmlAbnormalTransferData2Flag());
        densoDomain.setEmlPedpoDoalcasnUnmatpnFlg(form.getEmlPedpoDoalcasnUnmatpnFlg());
        densoDomain.setEmlAbnormalTransferFlg(form.getEmlAbnormalTransferFlg());
        // End : [IN049] Change Column Name
        
        densoDomain.setCreateDscId(userDscId);
        densoDomain.setLastUpdateDscId(userDscId);
        
        densoUserDetailDomain.setUserDomain(userDomain);
        densoUserDetailDomain.setSpsMUserDensoDomain(densoDomain);
        
        criteria.setDscId(dscId);
        criteria.setUserDomain(userDomain);
        criteria.setUserDensoDetailDomain(densoUserDetailDomain);
        
        try {
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM006_BREGISTER)){
                this.densoUserRegistrationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM006_BREGISTER, locale);
            }
            
            result = densoUserRegistrationFacadeService.transactRegisterNewUserDenso(criteria);
            if(null != result.getErrorMessageList()){
                applicationMessageList = result.getErrorMessageList();
            }else if(Constants.IS_ACTIVE.equals(result.getIsActive())){
                StringBuffer forward = new StringBuffer();
                forward.append(SupplierPortalConstant.URL_WADM008_ACTION)
                    .append(SupplierPortalConstant.URL_PARAM_METHOD)
                    .append(SupplierPortalConstant.METHOD_DO_INITIAL);
                forward.append(SupplierPortalConstant.URL_PARAM_DSCID);
                forward.append(dscId);
                forward.append(SupplierPortalConstant.URL_PARAM_DCD);
                forward.append(form.getDCd());
                forward.append(SupplierPortalConstant.URL_PARAM_DPCD);
                forward.append(form.getDPcd());
                forward.append(SupplierPortalConstant.URL_PARAM_FIRSTNAME);
                forward.append(firstName);
                forward.append(SupplierPortalConstant.URL_PARAM_MIDDLENAME);
                forward.append(StringUtil.nullToEmpty(form.getMiddleName()));
                forward.append(SupplierPortalConstant.URL_PARAM_LASTNAME);
                forward.append(lastName);
                forward.append(SupplierPortalConstant.URL_PARAM_RETURN_MODE);
                forward.append(Constants.MODE_REGISTER);
                forward.append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES);
                forward.append(Constants.PAGE_SEARCH);
                isRegisterSuccess = true;
                return new ActionForward(forward.toString(), true);
            }
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ex.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(!isRegisterSuccess){
                this.setPlantCombobox(form);
            }
            form.setApplicationMessageList(applicationMessageList);
            form.setMode(Constants.MODE_REGISTER);
        }
//        /*initial screen*/
//        try {
//            criteria = densoUserRegistrationFacadeService
//                .searchInitialRegister(dataScopeControlDomain);
//            form.setPlantDensoList(criteria.getPlantDensoList());
//            form.setCompanyDensoList(criteria.getCompanyDensoList());
//            if(null != criteria.getDepartmentNameList()){
//                form.setDepartmentNameList(criteria.getDepartmentNameList());
//            }
//            if(null != result.getErrorMessageList()){
//                applicationMessageList = result.getErrorMessageList();
//                form.setApplicationMessageList(applicationMessageList);
//            }
//        }catch(ApplicationException applicationException){
//            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                applicationException.getMessage()));
//            form.setApplicationMessageList(applicationMessageList);
//        }catch (Exception e) {
//            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                MessageUtil.getApplicationMessageHandledException(locale,
//                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
//                    new String[] {e.getClass().toString(), e.getMessage()})));
//        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Register DENSO User.
     * <p> update data with parameter from screen.
     * </p>
     * <ul>
     * <li>In case click Register button, System will register DENSO user.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doUpdate(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm006Form form = (Wadm006Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        DensoUserRegistrationDomain criteria = new DensoUserRegistrationDomain();
        DensoUserRegistrationDomain result = new DensoUserRegistrationDomain();
        UserDensoDetailDomain densoUserDetailDomain = new UserDensoDetailDomain();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        boolean isUpdateSuccess = false;
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setUserLogin(setUserLogin());
        /*set menu code list for initial menu bar*/
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        String userDscId = userLoginDomain.getDscId();
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        criteria.setDataScopeControlDomain(dataScopeControlDomain);
        
        /*Set value to argument Domain.*/
        String dscId = this.emptyToNull(form.getDscId());
        String employeeCode = this.emptyToNull(form.getEmployeeCode());
        String firstName = this.emptyToNull(form.getFirstName());
        String middleName = this.emptyToNull(form.getMiddleName());
        String lastName = this.emptyToNull(form.getLastName());
        String email = this.emptyToNull(form.getEmail());
        String telephone = this.emptyToNull(form.getTelephone());
        String departmentName = this.emptyToNull(form.getDepartmentName());
        
        UserDomain userDomain = new UserDomain();
        SpsMUserDensoDomain densoDomain = new SpsMUserDensoDomain();
        userDomain.setDscId(dscId);
        userDomain.setFirstName(firstName);
        userDomain.setMiddleName(middleName);
        userDomain.setLastName(lastName);
        userDomain.setEmail(email);
        userDomain.setTelephone(telephone);
        userDomain.setDepartmentName(departmentName);
        userDomain.setIsActive(Constants.IS_ACTIVE);
        userDomain.setCreateUser(userDscId);
        userDomain.setUpdateUser(userDscId);
        
        densoDomain.setDscId(dscId);
        densoDomain.setEmployeeCd(employeeCode);
        densoDomain.setDCd(form.getDCd());
        densoDomain.setDPcd(form.getDPcd());
        densoDomain.setEmlCreateCancelAsnFlag(form.getEmlCreateCancelAsnFlag());
        densoDomain.setEmlReviseAsnFlag(form.getEmlReviseAsnFlag());
        densoDomain.setEmlCreateInvoiceFlag(form.getEmlCreateInvoiceFlag());

        // Start : [IN049] Change Column Name
        //densoDomain.setEmlAbnormalTransfer1Flag(form.getEmlAbnormalTransferData1Flag());
        //densoDomain.setEmlAbnormalTransfer2Flag(form.getEmlAbnormalTransferData2Flag());
        densoDomain.setEmlPedpoDoalcasnUnmatpnFlg(form.getEmlPedpoDoalcasnUnmatpnFlg());
        densoDomain.setEmlAbnormalTransferFlg(form.getEmlAbnormalTransferFlg());
        // End : [IN049] Change Column Name
        
        densoDomain.setCreateDscId(userDscId);
        densoDomain.setLastUpdateDscId(userDscId);
        
        densoUserDetailDomain.setUserDomain(userDomain);
        densoUserDetailDomain.setSpsMUserDensoDomain(densoDomain);
        
        criteria.setDscId(dscId);
        criteria.setUserDomain(userDomain);
        criteria.setUpdateDatetime(form.getUpdateDatetime());
        criteria.setUpdateDatetimeDenso(form.getUpdateDatetimeDenso());
        criteria.setUserDensoDetailDomain(densoUserDetailDomain);
        
        try {
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM006_BREGISTER)){
                this.densoUserRegistrationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM006_BREGISTER, locale);
            }
            
            result = densoUserRegistrationFacadeService.transactRegisterExistUserDenso(criteria);
            if(null != result.getErrorMessageList()){
                applicationMessageList = result.getErrorMessageList();
            }else if(Constants.IS_ACTIVE.equals(result.getIsActive())){
                StringBuffer forward = new StringBuffer();
                forward.append(SupplierPortalConstant.URL_WADM008_ACTION)
                    .append(SupplierPortalConstant.URL_PARAM_METHOD)
                    .append(SupplierPortalConstant.METHOD_DO_INITIAL);
                forward.append(SupplierPortalConstant.URL_PARAM_DSCID);
                forward.append(dscId);
                forward.append(SupplierPortalConstant.URL_PARAM_DCD);
                forward.append(form.getDCd());
                forward.append(SupplierPortalConstant.URL_PARAM_DPCD);
                forward.append(form.getDPcd());
                forward.append(SupplierPortalConstant.URL_PARAM_FIRSTNAME);
                forward.append(firstName);
                forward.append(SupplierPortalConstant.URL_PARAM_MIDDLENAME);
                forward.append(StringUtil.nullToEmpty(form.getMiddleName()));
                forward.append(SupplierPortalConstant.URL_PARAM_LASTNAME);
                forward.append(lastName);
                forward.append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES);
                forward.append(Constants.PAGE_SEARCH);
                forward.append(SupplierPortalConstant.URL_PARAM_RETURN_MODE);
                forward.append(Constants.MODE_EDIT);
                isUpdateSuccess = true;
                return new ActionForward(forward.toString(), true);
            }
        }catch(ApplicationException applicationException){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(!isUpdateSuccess){
                this.setPlantCombobox(form);
            }
            form.setMode(Constants.MODE_EDIT);
            form.setApplicationMessageList(applicationMessageList);
        }
//        /*initial screen*/
//        try{
//            criteria = densoUserRegistrationFacadeService.searchInitialEdit(criteria);
//            form.setPlantDensoList(result.getPlantDensoList());
//            form.setCompanyDensoList(result.getCompanyDensoList());
//            if(null != result.getErrorMessageList()){
//                applicationMessageList = result.getErrorMessageList();
//                form.setApplicationMessageList(applicationMessageList);
//            }
//        }catch(ApplicationException applicationException){
//            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                applicationException.getMessage()));
//            form.setApplicationMessageList(applicationMessageList);
//        }catch (Exception e) {
//            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                MessageUtil.getApplicationMessageHandledException(locale,
//                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
//                    new String[] {e.getClass().toString(), e.getMessage()})));
//        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do return to WADM005.
     * <p>Open WADM005_DensoUserInformation</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReturn(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm006Form form = (Wadm006Form)actionForm;
        StringBuilder forward = new StringBuilder();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setUserLogin(setUserLogin());
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        try{
            if(!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM006_BRETURN)){
                this.setPlantCombobox(form);
                this.densoUserRegistrationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM006_BRETURN, locale);
            }
            
            forward.append(SupplierPortalConstant.URL_WADM005_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_RETURN)
                .append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES)
                .append(Constants.PAGE_SEARCH);
            return new ActionForward(forward.toString(), true);
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ex.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        form.setApplicationMessageList(applicationMessageList);
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Gets the key method map.
     * 
     * @return Map
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap = new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RETURN,
            SupplierPortalConstant.METHOD_DO_RETURN);
        keyMethodMap.put(SupplierPortalConstant.ACTION_REGISTER,
            SupplierPortalConstant.METHOD_DO_REGISTER);
        keyMethodMap.put(SupplierPortalConstant.ACTION_UPDATE,
            SupplierPortalConstant.METHOD_DO_UPDATE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL_WITH_CRITERIA,
            SupplierPortalConstant.METHOD_DO_INITIAL_WITH_CRITERIA);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD,
            SupplierPortalConstant.METHOD_DO_SELECT_DPCD);
        return keyMethodMap;
    }
    
    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return the UserLoginDomain.
     */
    private UserLoginDomain setUserLoginInformation() {
        UserLoginDomain userLoginDomain = (UserLoginDomain)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }

    /**
     * Get roles for supplier user.
     * <p>Get roles  from role screen of user login.</p>
     * 
     * @param userLoginDomain the UserLoginDomain
     * @return the RoleScreenDomain
     */
    private RoleScreenDomain setRoleScreen(UserLoginDomain userLoginDomain) {
        List<RoleScreenDomain> roleScreenList = userLoginDomain.getRoleScreenDomainList();
        for(RoleScreenDomain roleScreen : roleScreenList){
            if(SupplierPortalConstant
                .SCREEN_ID_WADM006.equals(roleScreen.getSpsMScreenDomain().getScreenCd())){
                return roleScreen;
            }
        }
        return null;
    }
    
    /**
     * Set value inside Supplier Plant combo box.
     * @param form the Wadm006Form
     * */
    private void setPlantCombobox(Wadm006Form form)
    {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLoginDomain = this.setUserLoginInformation();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        DensoUserRegistrationDomain densoUserRegistrationDomain = 
            new DensoUserRegistrationDomain();
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        if (!Strings.judgeBlank(form.getDCd())){
            List<PlantDensoDomain> plantDensoList = null;
            try{
                if(!Constants.MISC_CODE_ALL.equals(form.getDCd())){
                    densoUserRegistrationDomain.setDCd(form.getDCd());
                }
                densoUserRegistrationDomain.setDataScopeControlDomain(dataScopeControlDomain);
                plantDensoList = densoUserRegistrationFacadeService
                    .searchSelectedCompanyDenso(densoUserRegistrationDomain);
                form.setPlantDensoList(plantDensoList);
            }catch(ApplicationException e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    e.getMessage()));
            }catch(Exception e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }finally{
                form.setApplicationMessageList(applicationMessageList);
            }
        }
    }
    
    /**
     * If input string is empty or has only space, return null.<br />
     * If not, return trimmed string.
     * @param input - input string
     * @return trimmed input string or null
     * @
     * */
    private String emptyToNull(String input) {
        if (null == input) {
            return null;
        }
        if (Constants.EMPTY_STRING.equals(input.trim())) {
            return null;
        }
        return input.toUpperCase().trim();
    }
}