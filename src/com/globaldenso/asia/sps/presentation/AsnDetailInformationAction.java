/*
 * ModifyDate Development company     Describe 
 * 2014/07/29 CSI Chatchai            Create
 * 2018/04/11 Netband U.Rungsiwut     Generate ASN PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.AsnDetailInformationDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.AsnDetailInformationFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Wshp008Form;

/**
 * The Class AsnDetailInformationAction.
 * 
 * @author CSI
 */
public class AsnDetailInformationAction extends CoreAction {

    /** The asn detail information facade service */
    private AsnDetailInformationFacadeService asnDetailInformationFacadeService;

    /** The default Constructor */
    public AsnDetailInformationAction() {

    }

    /**
     * Set the asn detail information facade service.
     * 
     * @param asnDetailInformationFacadeService the asn detail information facade service to set
     */
    public void setAsnDetailInformationFacadeService(
        AsnDetailInformationFacadeService asnDetailInformationFacadeService) {
        this.asnDetailInformationFacadeService = asnDetailInformationFacadeService;
    }
    
    /**
     * Do initial.
     * <p>
     * Initial data when load page.
     * </p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp008Form form = (Wshp008Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AsnDetailInformationDomain asnDetailInformationDomain = new AsnDetailInformationDomain();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        UserLoginDomain userLoginDomain = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(this.setUserLogin());
        try{
            form.resetForm();
            DataScopeControlDomain dataScopeControlDomain 
                = this.getDataScopeControlForUserLogin(userLoginDomain);
            asnDetailInformationDomain.setLocale(locale);
            asnDetailInformationDomain.setDataScopeControlDomain(dataScopeControlDomain);
            asnDetailInformationDomain 
                = asnDetailInformationFacadeService.searchInitial(asnDetailInformationDomain);
            
            this.setInitialCriteria(form);
            form.setCompanySupplierList(asnDetailInformationDomain.getCompanySupplierList());
            form.setCompanyDensoList(asnDetailInformationDomain.getCompanyDensoList());
            form.setPlantSupplierList(asnDetailInformationDomain.getPlantSupplierList());
            form.setPlantDensoList(asnDetailInformationDomain.getPlantDensoList());
            form.setAsnStatusList(asnDetailInformationDomain.getAsnStatusList());
            form.setTransList(asnDetailInformationDomain.getTransList());
            form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
                asnDetailInformationDomain.getDataScopeControlDomain()
                    .getDensoSupplierRelationDomainList(),
                    SupplierPortalConstant.SCREEN_ID_WSHP008));
            form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
                asnDetailInformationDomain.getDataScopeControlDomain()
                    .getDensoSupplierRelationDomainList(),
                    SupplierPortalConstant.SCREEN_ID_WSHP008));
            
            try{
                if(!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WSHP008_BRESET)){
                    this.asnDetailInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WSHP008_BRESET, locale);
                }
            }catch(ApplicationException ex){
                form.setCannotResetMessage(ex.getMessage());
            }
            
            try{
                if(!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WSHP008_BDOWNLOAD)){
                    this.asnDetailInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WSHP008_BDOWNLOAD, locale);
                }
            }catch(ApplicationException ex){
                form.setCannotDownloadMessage(ex.getMessage());
            }
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ex.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(Constants.ZERO < applicationMessageList.size()){
                form.setApplicationMessageList(applicationMessageList);
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do initial with criteria.
     * <p>
     * Initial data when return from WSHP003 screen.
     * </p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitialWithCriteria(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp008Form form = (Wshp008Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AsnDetailInformationDomain asnDetailInformationDomain = new AsnDetailInformationDomain();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        try{
            UserLoginDomain userLoginDomain = this.getUserLoginFromDensoContext();
            form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
            form.setUserLogin(this.setUserLogin());
            
            //Call private action method to get current user login and roles from DENSO-context
            DataScopeControlDomain dataScopeControlDomain 
                = this.getDataScopeControlForUserLogin(userLoginDomain);
            asnDetailInformationDomain.setLocale(locale);
            asnDetailInformationDomain.setDataScopeControlDomain(dataScopeControlDomain);
            
            //Call Facade Service AsnDetailInformationFacadeService initial()
            asnDetailInformationDomain = asnDetailInformationFacadeService.searchInitial(
                asnDetailInformationDomain);
            
            Wshp008Form sessionForm = (Wshp008Form)DensoContext.get().getGeneralArea(
                SupplierPortalConstant.SESSION_WSHP008_FORM);
            form.setPlanEtaFrom(sessionForm.getPlanEtaFrom());
            form.setPlanEtaTo(sessionForm.getPlanEtaTo());
            form.setVendorCd(sessionForm.getVendorCd());
            form.setSPcd(sessionForm.getSPcd());
            form.setDCd(sessionForm.getDCd());
            form.setDPcd(sessionForm.getDPcd());
            form.setAsnStatus(sessionForm.getAsnStatus());
            form.setTrans(sessionForm.getTrans());
            form.setAsnNo(sessionForm.getAsnNo());
            form.setSpsDoNo(sessionForm.getSpsDoNo());
            form.setActualEtdFrom(sessionForm.getActualEtdFrom());
            form.setActualEtdTo(sessionForm.getActualEtdTo());
            form.setCannotResetMessage(sessionForm.getCannotResetMessage());
            form.setCannotDownloadMessage(sessionForm.getCannotDownloadMessage());
            form.setSupplierAuthenList(sessionForm.getSupplierAuthenList());
            form.setDensoAuthenList(sessionForm.getDensoAuthenList());
            form.setCompanySupplierList(asnDetailInformationDomain.getCompanySupplierList());
            form.setCompanyDensoList(asnDetailInformationDomain.getCompanyDensoList());
            form.setAsnStatusList(asnDetailInformationDomain.getAsnStatusList());
            form.setTransList(asnDetailInformationDomain.getTransList());
            
            DensoContext.get().removeGeneralArea(SupplierPortalConstant.SESSION_WSHP008_FORM);
            
            //Call Private Action Method for search ASN information
            form.setFrom(sessionForm.getFrom());
            form.setTo(sessionForm.getTo());
            form.setMax(sessionForm.getMax());
            form.setPages(null);
            form.setCount(sessionForm.getCount());
            form.setPageNo(sessionForm.getPageNo());
            form.setFirstCount(Constants.FIVE);
            form.setFirstPages(Constants.PAGE_SEARCH);
            this.setParameterCriteria(form, asnDetailInformationDomain);
            this.searchAsnDetailInformation(asnDetailInformationDomain, form);
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ex.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            this.setPlantCombobox(form);
            if(Constants.ZERO < applicationMessageList.size()){
                form.setApplicationMessageList(applicationMessageList);
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * Do search.
     * <p>Search data by criteria from screen.</p>
     * <ul>
     * <li>In case click search button, system search data and count by criteria 
     * if count result size is 0 return message to screen "data not found".</li>
     * </ul> 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doSearch(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp008Form form = (Wshp008Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        AsnDetailInformationDomain asnDetailInformationCriteria = new AsnDetailInformationDomain();
        asnDetailInformationCriteria.setLocale(locale);
        
        UserLoginDomain userLoginDomain = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        if (Constants.PAGE_SEARCH.equals(form.getFirstPages()) 
            || Constants.PAGE_CLICK.equals(form.getPages()))
        {
            try {
                if(!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WSHP008_BSEARCH)){
                    this.asnDetailInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WSHP008_BSEARCH, locale);
                }
                //Call Private Action Method for search ASN Detail information.
                this.setParameterCriteria(form, asnDetailInformationCriteria);
                this.searchAsnDetailInformation(asnDetailInformationCriteria, form);
            } catch(ApplicationException ex){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    ex.getMessage()));
            } catch(Exception e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            } finally {
                this.setPlantCombobox(form);
                if(Constants.ZERO < applicationMessageList.size()){
                    form.setApplicationMessageList(applicationMessageList);
                }
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**Do select supplier company code
     * <ul>
     * <li>when supplier company code combo box change load new supplier plant code
     * for relate company selected.
     * </li>
     * </ul> 
     * 
     * @param actionMapping the actionMapping
     * @param actionForm the actionForm
     * @param request the request
     * @param response the response
     * @return the ActionForward
     * @throws Exception the Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp008Form form = (Wshp008Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        List<PlantSupplierDomain> plantList = null;
        
        try{
            // 1. Call private action method to get current user login and roles from DENSO-context
            UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            
            // 2. Get roles for supplier user
            DataScopeControlDomain dataScopeControl
                = this.getDataScopeControlForUserLogin(userLogin);
            
            /* 3. Call Facade Service PurchaseOrderInformationFacadeService
             * changeSelectSupplierCompany() to get the list of supplier plant code by
             * supplier company code
             * */
            PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getVendorCd())) {
                plantSupplier.getPlantSupplierDomain().setVendorCd(form.getVendorCd());
            }
            plantSupplier.setLocale(locale);
            plantSupplier.setDataScopeControlDomain(dataScopeControl);
            plantList 
                = asnDetailInformationFacadeService.searchSelectedCompanySupplier(plantSupplier);
            
            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)plantList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.getApplicationMessageList().addAll(applicationMessageList);
        }
        return null;
    }

    /**Do select denso company code
     * <p>Denso company code combo box change in criteria area of screen.</p>
     * <ul>
     * <li>when company combo box change load new denso plant code
     * for relate company selected.
     * </li>
     * </ul>
     * 
     * @param actionMapping the actionMapping
     * @param actionForm the actionForm
     * @param request the request
     * @param response the response
     * @return ActionForward
     * @throws Exception Exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp008Form form = (Wshp008Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        List<PlantDensoDomain> plantList = null;
        
        try{
            // 1. Call private action method to get current user login and roles from DENSO-context
            UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            
            // 2. Get roles for supplier user
            DataScopeControlDomain dataScopeControl
                = this.getDataScopeControlForUserLogin(userLogin);
            
            /* 3. Call Facade Service PurchaseOrderInformationFacadeService
             * changeSelectSupplierCompany() to get the list of supplier plant code by supplier
             * company code
             * */
            PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getDCd())) {
                plantDenso.getPlantDensoDomain().setDCd(form.getDCd());
            }
            plantDenso.setLocale(locale);
            plantDenso.setDataScopeControlDomain(dataScopeControl);
            plantList = this.asnDetailInformationFacadeService
                .searchSelectedCompanyDenso(plantDenso);
            
            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)plantList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.getApplicationMessageList().addAll(applicationMessageList);
        }
        return null;
    }
    
    /**
     * Search Supplier Company Code by selected Supplier Plant Code filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp008Form form = (Wshp008Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanySupplierDomain> companyList = null;
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        
        try{
            // 1. Call private action method to get current user login and roles from DENSO-context
            UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            
            // 2. Get roles for supplier user
            DataScopeControlDomain dataScopeControl
                = this.getDataScopeControlForUserLogin(userLogin);
            
            /* 3. Call Facade Service AsnDetailInformationFacadeService
             * searchSelectedPlantSupplier() to get the list of supplier company code by
             * supplier plant code
             * */
            PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getSPcd())) {
                plantSupplier.getPlantSupplierDomain().setSPcd(form.getSPcd());
            }
            plantSupplier.setLocale(locale);
            plantSupplier.setDataScopeControlDomain(dataScopeControl);
            companyList = this.asnDetailInformationFacadeService
                .searchSelectedPlantSupplier(plantSupplier);
            
            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.getApplicationMessageList().addAll(applicationMessageList);
        }
        return null;
    }
    
    /**
     * Search company DENSO Code by selected plant DENSO Code and filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp008Form form = (Wshp008Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanyDensoDomain> companyList = null;
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        
        try{
            // 1. Call private action method to get current user login and roles from DENSO-context
            UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
            form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
            
            // 2. Get roles for supplier user
            DataScopeControlDomain dataScopeControl
                = this.getDataScopeControlForUserLogin(userLogin);
            
            /* 3. Call Facade Service AsnDetailInformationFacadeService
             * searchSelectedPlantDenso() to get the list of supplier plant code by supplier
             * company code
             * */
            PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
            if (!Constants.MISC_CODE_ALL.equals(form.getDPcd())) {
                plantDenso.getPlantDensoDomain().setDPcd(form.getDPcd());
            }
            plantDenso.setLocale(locale);
            plantDenso.setDataScopeControlDomain(dataScopeControl);
            companyList = this.asnDetailInformationFacadeService
                .searchSelectedPlantDenso(plantDenso);
            
            // 4. Set value of domain to ActionForm
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.getApplicationMessageList().addAll(applicationMessageList);
        }
        return null;
    }
    
    /**Do download PDF
     * <p>In case of clicking 'PDF' icon, the system will download asn report from file manager.</p>
     * 
     * @param actionMapping the action Mapping
     * @param actionForm the action Form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the Exception
     */
    public ActionForward doDownload(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp008Form form = (Wshp008Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AsnDetailInformationDomain criteria = new AsnDetailInformationDomain();
        AsnDetailInformationDomain result = new AsnDetailInformationDomain();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        String fileName = null;
        ServletOutputStream outputStream = null;
        
        UserLoginDomain userLoginDomain = this.getUserLoginFromDensoContext();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        try{
            Calendar cal = Calendar.getInstance();
            this.setParameterCriteria(form, criteria);
            criteria.setLocale(locale);
            result = asnDetailInformationFacadeService.searchAsnDetailCsv(criteria);
            if(!StringUtil.checkNullOrEmpty(result.getCsvBuffer())){
                fileName = StringUtil.appendsString(
                    Constants.ASN_DETAIL_INFORMATION_LIST_FILE_NAME, Constants.SYMBOL_UNDER_SCORE,
                    DateUtil.format(cal.getTime(), DateUtil.PATTERN_YYYYMMDD_HHMM));
                byte[] output = result.getCsvBuffer().toString().getBytes();
                super.setHttpHeaderForCsv(response, fileName);
                response.setContentLength(output.length);
                outputStream = response.getOutputStream();
                outputStream.write(output, Constants.ZERO, output.length);
                outputStream.flush();
                return null;
            }
            if(null != result.getErrorMessageList() 
                && Constants.ZERO < result.getErrorMessageList().size()){
                applicationMessageList.addAll(result.getErrorMessageList());
            }
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ex.getMessage()));
        }catch(IOException ioe){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_80_0012)));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(null != outputStream){
                outputStream.close();
            }
        }
        form.setApplicationMessageList(applicationMessageList);
        this.setPlantCombobox(form);
        //this.setInitialCriteria(form);
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**Do download PDF
     * <p>In case of clicking 'PDF' icon, the system will download asn report from file manager.</p>
     * 
     * @param actionMapping the action Mapping
     * @param actionForm the action Form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the Exception
     */
    public ActionForward doDownloadPdf(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp008Form form = (Wshp008Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AsnDetailInformationDomain asnDetailInformationDomain = new AsnDetailInformationDomain();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        UserLoginDomain userLoginDomain = this.getUserLoginFromDensoContext();
        form.setUserLogin(super.setUserLogin());
        ServletOutputStream outputStream = null;
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
//        try{
//            if(!userLoginDomain.getButtonLinkNameList().contains(
//                RoleTypeConstants.WSHP008_LASNPDF)){
//                this.asnDetailInformationFacadeService.searchRoleCanOperate(
//                    RoleTypeConstants.WSHP008_LASNPDF, locale);
//            }
//            /* 1. Call Facade Service AsnDetailInformationFacadeService findFileName()
//             * to get PDF file name
//             * */
//            criteria.setFileId(form.getPdfFileId());
//            criteria.setLocale(locale);
//
//            /** to get PDF information. */
//            fileManagementDomain = asnDetailInformationFacadeService.searchGenerateAsn(kanbanOrderInformationDomain);
//            
//            super.setHttpHeaderForFileManagement(response, request, fileManagementDomain.getFileName());
//            outputStream = response.getOutputStream();
//            FileUtil.writeInputStreamToOutputStream(fileManagementDomain.getFileData(), response.getOutputStream());
//            outputStream.flush();
//            return null;
//        }catch(ApplicationException ae){
//            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                ae.getMessage()));
//        }catch(Exception e){
//            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                MessageUtil.getApplicationMessageHandledException(locale,
//                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
//                    new String[] {e.getClass().toString(), e.getMessage()})));
//        }finally{
//            if(null != outputStream){
//                outputStream.close();
//            }
//        }
        try{
            if(!userLoginDomain.getButtonLinkNameList().contains(RoleTypeConstants.WSHP008_LASNPDF)){
                this.asnDetailInformationFacadeService.searchRoleCanOperate(RoleTypeConstants.WSHP008_LASNPDF, locale);
            }
            Timestamp updateDateTime = null;
            FileManagementDomain fileManagementDomain = asnDetailInformationFacadeService.generateAndUpdatePdfFile(
                locale, 
                form.getSelectedAsnNo(), 
                form.getSelectedDCd(), 
                updateDateTime, 
                userLoginDomain.getDscId());
            super.setHttpHeaderForFileManagement(response, request, fileManagementDomain.getFileName());
            outputStream = response.getOutputStream();
            FileUtil.writeInputStreamToOutputStream(fileManagementDomain.getFileData(), response.getOutputStream());
//            this.asnMaintenanceFacadeService.searchDownloadAsnReport(criteria,
//                response.getOutputStream());
            return null;
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                ex.getMessage()));
        }catch(Exception e) {
            applicationMessageList = new ArrayList<ApplicationMessageDomain>();
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            if (null != outputStream) {
                outputStream.close();
            }
            form.setApplicationMessageList(applicationMessageList);

            Set<String> applicationMessageSet = new LinkedHashSet<String>();
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
        this.setPlantCombobox(form);
        form.setFirstPages(null);
        form.setPages(Constants.PAGE_CLICK);
        asnDetailInformationDomain.setLocale(locale);
        this.setParameterCriteria(form, asnDetailInformationDomain);
        this.searchAsnDetailInformation(asnDetailInformationDomain, form);
        form.getApplicationMessageList().addAll(applicationMessageList);
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

  
    /**Do Click Link More
     * <p>In case of clicking 'More' link, the system will download file from file manager.</p>
     * 
     * @param actionMapping actionMapping
     * @param actionForm actionForm
     * @param request request
     * @param response response
     * @return ActionForward
     * @throws Exception Exception
     */
    public ActionForward doClickLinkMore(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception
    {
        Wshp008Form form = (Wshp008Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        AsnDetailInformationDomain criteria = new AsnDetailInformationDomain();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        
        UserLoginDomain userLoginDomain = this.getUserLoginFromDensoContext();
        form.setUserLogin(super.setUserLogin());
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        OutputStream outputStream = null;
        try{
            /* 1. Call Facade Service AsnDetailInformationFacadeService findFileName()
             * to get PDF file name
             * */
            criteria.setFileId(ContextParams.getLegendFileId());
            criteria.setLocale(locale);
            FileManagementDomain fileManagement
                = this.asnDetailInformationFacadeService.searchFileName(criteria);
            /* 2. Call Facade Service AsnDetailInformationFacadeService searchLegendInfo()
             * to get PDF file
             * */
            outputStream = response.getOutputStream();
            super.setHttpHeaderForFileManagement(response, request, fileManagement.getFileName());
            this.asnDetailInformationFacadeService.searchLegendInfo(criteria, outputStream);
            outputStream.flush();
            return null;
        }catch(ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        }catch(Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(null != outputStream){
                outputStream.close();
            }
        }
        form.setApplicationMessageList(applicationMessageList);
        this.setPlantCombobox(form);
        this.setInitialCriteria(form);
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**Do Click Link SpsDoNo
     * <p>In case of clicking 'SPS D/O No.' link, the system will link to WSHP003 screen.
     * </p>
     * 
     * @param mapping actionMapping
     * @param actionForm actionForm
     * @param request request
     * @param response response
     * @return ActionForward
     * @throws Exception Exception
     */
    public ActionForward doClickLinkSpsDoNo(ActionMapping mapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp008Form form = (Wshp008Form)actionForm;
        StringBuffer forward = new StringBuffer();
        AsnDetailInformationDomain asnDetailInformationDomain = new AsnDetailInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setUserLogin(super.setUserLogin());
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        try{
            if(!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WSHP008_LSPSDONO)){
                this.asnDetailInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP008_LSPSDONO, locale);
            }
            
            forward.append(SupplierPortalConstant.URL_WSHP003_ACTION);
            forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
            forward.append(SupplierPortalConstant.METHOD_DO_INITIAL);
            forward.append(SupplierPortalConstant.URL_PARAM_DSCID);
            forward.append(userLogin.getDscId());
            forward.append(SupplierPortalConstant.URL_PARAM_DOID);
            forward.append(form.getSelectedDoId());
            forward.append(SupplierPortalConstant.URL_PARAM_SPSDONO);
            forward.append(form.getSelectedSpsDoNo());
            forward.append(SupplierPortalConstant.URL_PARAM_REVISION);
            forward.append(form.getSelectedRev());
            forward.append(SupplierPortalConstant.URL_PARAM_ROUTENO);
            forward.append(form.getSelectedRoute());
            forward.append(SupplierPortalConstant.URL_PARAM_DEL);
            forward.append(form.getSelectedDel());
            forward.append(SupplierPortalConstant.URL_PARAM_DCD);
            forward.append(form.getSelectedDCd());
            forward.append(SupplierPortalConstant.URL_PARAM_SCREENID);
            forward.append(SupplierPortalConstant.SCREEN_ID_WSHP008);
            DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WSHP008_FORM, form);
            return new ActionForward(forward.toString(), true);
        }catch(ApplicationException e){
            errorMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        this.setPlantCombobox(form);
        form.setFirstPages(null);
        form.setPages(Constants.PAGE_CLICK);
        asnDetailInformationDomain.setLocale(locale);
        this.setParameterCriteria(form, asnDetailInformationDomain);
        this.searchAsnDetailInformation(asnDetailInformationDomain, form);
        form.getApplicationMessageList().addAll(errorMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**Do Click Link ASN No.
     * <p>In case of clicking 'ASN No.' link, the system will link to WSHP004 screen.
     * </p>
     * 
     * @param mapping actionMapping
     * @param actionForm actionForm
     * @param request request
     * @param response response
     * @return ActionForward
     * @throws Exception Exception
     */
    public ActionForward doClickLinkAsnNo(ActionMapping mapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wshp008Form form = (Wshp008Form)actionForm;
        StringBuffer forward = new StringBuffer();
        AsnDetailInformationDomain asnDetailInformationDomain = new AsnDetailInformationDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        form.setUserLogin(super.setUserLogin());
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        
        try{
            if(!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WSHP008_LASNNO)){
                this.asnDetailInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP008_LASNNO, locale);
            }
            
//            Timestamp actualEtd = DateUtil.parseToTimestamp(
//                form.getSelectedActualEtd(), DateUtil.PATTERN_YYYYMMDD_HHMMSSSSS_DASH);
//            String actualEtdStr = DateUtil.format(new Date(
//                actualEtd.getTime()), DateUtil.PATTERN_YYYYMMDD_SLASH);
//            
//            forward.append(SupplierPortalConstant.URL_WSHP009_ACTION);
//            forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
//            forward.append(SupplierPortalConstant.METHOD_DO_INITIAL);
//            forward.append(SupplierPortalConstant.URL_PARAM_MODE);
//            forward.append(Constants.MODE_INITIAL);
//            forward.append(SupplierPortalConstant.URL_PARAM_DSCID);
//            forward.append(userLogin.getDscId());
//            forward.append(SupplierPortalConstant.URL_PARAM_VENDOR_CD);
//            forward.append(form.getSelectedVendorCd());
//            forward.append(SupplierPortalConstant.URL_PARAM_SPCD);
//            forward.append(form.getSelectedSPcd());
//            forward.append(SupplierPortalConstant.URL_PARAM_DCD);
//            forward.append(form.getSelectedDCd());
//            forward.append(SupplierPortalConstant.URL_PARAM_DPCD);
//            forward.append(form.getSelectedDPcd());
//            forward.append(SupplierPortalConstant.URL_PARAM_ASNNO);
//            forward.append(form.getSelectedAsnNo());
//            forward.append(SupplierPortalConstant.URL_PARAM_ACTUAL_ETD_FROM);
//            forward.append(actualEtdStr);
//            forward.append(SupplierPortalConstant.URL_PARAM_ACTUAL_ETD_TO);
//            forward.append(actualEtdStr);
            forward.append(SupplierPortalConstant.URL_WSHP004_ACTION);
            forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
            forward.append(SupplierPortalConstant.METHOD_DO_INITIAL);
            forward.append(SupplierPortalConstant.URL_PARAM_SCREENID)
                .append(SupplierPortalConstant.SCREEN_ID_WSHP008);
            forward.append(SupplierPortalConstant.URL_PARAM_ASNNO).append(form.getSelectedAsnNo());
            forward.append(SupplierPortalConstant.URL_PARAM_DCD).append(form.getSelectedDCd());
            forward.append(SupplierPortalConstant.URL_PARAM_TEMPORARY_MODE)
                .append(Constants.STR_ZERO);
            DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WSHP008_FORM, form);
            return new ActionForward(forward.toString(), true);
        }catch(ApplicationException e){
            errorMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        this.setPlantCombobox(form);
        form.setFirstPages(null);
        form.setPages(Constants.PAGE_CLICK);
        asnDetailInformationDomain.setLocale(locale);
        this.setParameterCriteria(form, asnDetailInformationDomain);
        this.searchAsnDetailInformation(asnDetailInformationDomain, form);
        form.getApplicationMessageList().addAll(errorMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.apache.struts.actions.LookupDispatchAction#getKeyMethodMap()
     */
    @Override
    protected Map<String, String> getKeyMethodMap()
    {
        Map<String, String> keyMethodMap = new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_SEARCH,
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET, 
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_SCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SPCD,
            SupplierPortalConstant.METHOD_DO_SELECT_SPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD,
            SupplierPortalConstant.METHOD_DO_SELECT_DPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE, 
            SupplierPortalConstant.METHOD_DO_CLICK_LINK_MORE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD_PDF, 
            SupplierPortalConstant.METHOD_DO_DOWNLOAD_PDF);
        keyMethodMap.put(SupplierPortalConstant.ACTION_LINK_SPSDONO, 
            SupplierPortalConstant.METHOD_DO_CLINK_LINK_SPSDONO);
        keyMethodMap.put(SupplierPortalConstant.ACTION_LINK_ASNNO,
            SupplierPortalConstant.METHOD_DO_CLINK_LINK_ASNNO);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL_WITH_CRITERIA,
            SupplierPortalConstant.METHOD_DO_INITIAL_WITH_CRITERIA);
        return keyMethodMap;
    }
    
    /**Search Asn Detail Information
     * @param asnDetailInformationCriteria the asn detail information domain
     * @param form the wshp008 form
     * @throws ApplicationException ApplicationException
     */
    private void searchAsnDetailInformation(AsnDetailInformationDomain asnDetailInformationCriteria,
        Wshp008Form form) throws ApplicationException
    {
        AsnDetailInformationDomain asnDetailInformationResult 
            = new AsnDetailInformationDomain();
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        
        asnDetailInformationResult 
            = asnDetailInformationFacadeService.searchAsnDetail(asnDetailInformationCriteria);
        SpsPagingUtil.setFormForPaging(asnDetailInformationCriteria, form);
        //Set value of domain to Action Form
        if(null != asnDetailInformationResult.getErrorMessageList()
            && Constants.ZERO < asnDetailInformationResult.getErrorMessageList().size())
        {
            applicationMessageList.addAll(asnDetailInformationResult.getErrorMessageList());
            form.setApplicationMessageList(applicationMessageList);
        }else{
            form.setAsnDetailInformationList(asnDetailInformationResult.getAsnInformationList());
        }
    }

    /**Sets Parameter criteria
     * @param form the wshp008 form
     * @param criteria the asn detail information domain
     */
    private void setParameterCriteria(Wshp008Form form, AsnDetailInformationDomain criteria)
    {
        String firstPages = form.getFirstPages();
        
        if (Constants.PAGE_SEARCH.equals(firstPages)) {
            criteria.setPageNumber(Constants.ONE);
        } else {
            criteria.setPageNumber(form.getPageNo());
        }
        if (!StringUtil.checkNullOrEmpty(form.getPlanEtaFrom().trim())) {
            criteria.setPlanEtaFrom(form.getPlanEtaFrom().trim());
        }
        if (!StringUtil.checkNullOrEmpty(form.getPlanEtaTo().trim())) {
            criteria.setPlanEtaTo(form.getPlanEtaTo().trim());
        }
        if (!StringUtil.checkNullOrEmpty(form.getVendorCd())) {
            criteria.setVendorCd(form.getVendorCd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getSPcd())) {
            criteria.setSPcd(form.getSPcd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDCd())) {
            criteria.setDCd(form.getDCd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getDPcd())) {
            criteria.setDPcd(form.getDPcd());
        }
        if (!StringUtil.checkNullOrEmpty(form.getAsnStatus())) {
            criteria.setAsnStatus(form.getAsnStatus());
        }
        if (!StringUtil.checkNullOrEmpty(form.getTrans())) {
            criteria.setTrans(form.getTrans());
        }
        if (!StringUtil.checkNullOrEmpty(form.getAsnNo().trim())) {
            criteria.setAsnNo(form.getAsnNo().toUpperCase().trim());
        }
        if (!StringUtil.checkNullOrEmpty(form.getSpsDoNo().trim())) {
            criteria.setSpsDoNo(form.getSpsDoNo().toUpperCase().trim());
        }
        if (!StringUtil.checkNullOrEmpty(form.getActualEtdFrom().trim())) {
            criteria.setActualEtdFrom(form.getActualEtdFrom().trim());
        }
        if (!StringUtil.checkNullOrEmpty(form.getActualEtdTo().trim())) {
            criteria.setActualEtdTo(form.getActualEtdTo().trim());
        }
        criteria.setSupplierAuthenList(form.getSupplierAuthenList());
        criteria.setDensoAuthenList(form.getDensoAuthenList());
    }

    /**
     * Get User login information from DensoContext general area.
     * @return User Login Domain
     * */
    private UserLoginDomain getUserLoginFromDensoContext()
    {
        UserLoginDomain userLogin = null;
        userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLogin;
    }
    
    /**
     * Get Data Scope Control for current user login.
     * @param userLogin User Login Information
     * @return Data Scope Control
     * */
    private DataScopeControlDomain getDataScopeControlForUserLogin(UserLoginDomain userLogin)
    {
        DataScopeControlDomain dataScopeControl = new DataScopeControlDomain();
        dataScopeControl.setUserType(userLogin.getUserType());
        for (RoleScreenDomain roleScreen : userLogin.getRoleScreenDomainList())
        {
            if (SupplierPortalConstant.SCREEN_ID_WSHP008.equals(
                roleScreen.getSpsMScreenDomain().getScreenCd())) {
                dataScopeControl.setUserRoleDomainList(roleScreen.getUserRoleDomainList());
            }
        }
        dataScopeControl.setDensoSupplierRelationDomainList(null);
        return dataScopeControl;
    }
    
    /**
     * Set value inside Supplier Plant and DENSO Plant combo box.
     * @param form the WSHP008Form
     * */
    private void setPlantCombobox(Wshp008Form form)
    {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginFromDensoContext();
        DataScopeControlDomain dataScopeControl
            = this.getDataScopeControlForUserLogin(userLogin);
        
        if (!Strings.judgeBlank(form.getVendorCd())) {
            List<ApplicationMessageDomain> applicationMessageList
                = new ArrayList<ApplicationMessageDomain>();
            List<PlantSupplierDomain> plantList = null;
            
            try{
                PlantSupplierWithScopeDomain plantSupplier = new PlantSupplierWithScopeDomain();
                if (!Constants.MISC_CODE_ALL.equals(form.getVendorCd())) {
                    plantSupplier.getPlantSupplierDomain().setVendorCd(form.getVendorCd());
                }
                plantSupplier.setLocale(locale);
                plantSupplier.setDataScopeControlDomain(dataScopeControl);
                plantList = this.asnDetailInformationFacadeService.searchSelectedCompanySupplier(
                    plantSupplier);
                
                form.setPlantSupplierList(plantList);
            } catch (Exception e) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001)));
            }
        }
        
        if (!Strings.judgeBlank(form.getDCd()))
        {
            List<ApplicationMessageDomain> applicationMessageList
                = new ArrayList<ApplicationMessageDomain>();
            List<PlantDensoDomain> plantList = null;
            
            try{
                PlantDensoWithScopeDomain plantDenso = new PlantDensoWithScopeDomain();
                if (!Constants.MISC_CODE_ALL.equals(form.getDCd())) {
                    plantDenso.getPlantDensoDomain().setDCd(form.getDCd());
                }
                plantDenso.setLocale(locale);
                plantDenso.setDataScopeControlDomain(dataScopeControl);
                plantList = this.asnDetailInformationFacadeService.searchSelectedCompanyDenso(
                    plantDenso);
                form.setPlantDensoList(plantList);
            } catch (Exception e) {
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001)));
            }
        }
    }
    
    /**
     * Sets Initial data for search criteria.
     * @param form the Wshp008Form
     * */
    private void setInitialCriteria(Wshp008Form form){
        String currentDateStr  = DateUtil.format(
            Calendar.getInstance().getTime(), DateUtil.PATTERN_YYYYMMDD_SLASH);
        form.setPlanEtaFrom(currentDateStr);
        form.setPlanEtaTo(currentDateStr);
        form.setVendorCd(Constants.EMPTY_STRING);
        form.setSPcd(Constants.EMPTY_STRING);
        form.setDCd(Constants.EMPTY_STRING);
        form.setDPcd(Constants.EMPTY_STRING);
        form.setAsnStatus(Constants.EMPTY_STRING);
        form.setTrans(Constants.EMPTY_STRING);
        form.setAsnNo(Constants.EMPTY_STRING);
        form.setSpsDoNo(Constants.EMPTY_STRING);
        form.setActualEtdFrom(Constants.EMPTY_STRING);
        form.setActualEtdTo(Constants.EMPTY_STRING);
        form.setMax(Constants.ZERO);
        form.setAsnDetailInformationList(null);
    }
}