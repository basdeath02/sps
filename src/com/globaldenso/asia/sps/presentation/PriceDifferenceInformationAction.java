package com.globaldenso.asia.sps.presentation;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMMiscDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain;
import com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDomain;
import com.globaldenso.asia.sps.business.service.PriceDifferenceInformationFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.SpsPagingUtil;
import com.globaldenso.asia.sps.presentation.form.Winv005Form;

/**
 * The Class PriceDifferenceInformationAction.
 * 
 * @author CSI
 */
public class PriceDifferenceInformationAction extends CoreAction {
    /** The Price Difference Information Facade Service. */
    private PriceDifferenceInformationFacadeService priceDifferenceInformationFacadeService =
        null;

    /** The default constructor. */
    public PriceDifferenceInformationAction() {
        super();
    }

    /**
     * Set the Price Difference Information Facade Service.
     * 
     * @param priceDifferenceInformationFacadeService the Price Difference
     *            Information Facade Service to set
     */
    public void setPriceDifferenceInformationFacadeService(
        PriceDifferenceInformationFacadeService priceDifferenceInformationFacadeService) {
        this.priceDifferenceInformationFacadeService = priceDifferenceInformationFacadeService;
    }

    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping mapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Winv005Form form = (Winv005Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        List<UserRoleDomain> roleUserList = null;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        PriceDifferenceInformationDomain priceDifferenceInformationDomain =
            new PriceDifferenceInformationDomain();
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain =
            new PlantSupplierWithScopeDomain();
        try {

            for (RoleScreenDomain roleScreen : userLoginDomain
                .getRoleScreenDomainList()) {
                if (SupplierPortalConstant.SCREEN_ID_WINV005.equals(roleScreen
                    .getSpsMScreenDomain().getScreenCd())) {
                    roleUserList = roleScreen.getUserRoleDomainList();
                }
            }

            dataScopeControlDomain.setLocale(locale);
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleUserList);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            plantSupplierWithScopeDomain
                .setDataScopeControlDomain(dataScopeControlDomain);
            priceDifferenceInformationDomain.setDCd(null);
            priceDifferenceInformationDomain.setVendorCd(null);

            priceDifferenceInformationDomain = priceDifferenceInformationFacadeService
                .searchInitial(plantSupplierWithScopeDomain,
                    priceDifferenceInformationDomain);
            
            Calendar firstDateOfCurrentMounth = Calendar.getInstance();
            firstDateOfCurrentMounth.set(Calendar.DAY_OF_MONTH, Constants.ONE);
            Calendar lastDateOfCurrentMounth = Calendar.getInstance();
            lastDateOfCurrentMounth.set(Calendar.DAY_OF_MONTH,
                lastDateOfCurrentMounth.getActualMaximum(Calendar.DAY_OF_MONTH));
            form.setInvoiceDateFrom(DateUtil.format(firstDateOfCurrentMounth.getTime(),
                DateUtil.PATTERN_YYYYMMDD_SLASH));
            form.setInvoiceDateTo(DateUtil.format(lastDateOfCurrentMounth.getTime(),
                DateUtil.PATTERN_YYYYMMDD_SLASH));
            form.setCnDateFrom(null);
            form.setCnDateTo(null);
            form.setVendorCd(null);
            form.setSPcd(null);
            form.setDCd(null);
            form.setDPcd(null);
            form.setInvoiceStatus(null);
            form.setSCdList(priceDifferenceInformationDomain.getCompanySupplierDomainList());
            form.setSPcdList(priceDifferenceInformationDomain.getPlantSupplierMiscList());
            form.setDCdList(priceDifferenceInformationDomain.getCompanyDensoDomainList());
            form.setDPcdList(priceDifferenceInformationDomain.getPlantDensoMiscList());
            form.setInvoiceStatusList(priceDifferenceInformationDomain.getInvoiceStatusList());
            form.setInvoiceNo(null);
            form.setCnNo(null);
            form.setAsnNo(null);
            form.setDPn(null);
            form.setSPn(null);

            form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
                priceDifferenceInformationDomain.getDensoSupplierRelationDomainList(), 
                SupplierPortalConstant.SCREEN_ID_WINV005));
            form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
                priceDifferenceInformationDomain.getDensoSupplierRelationDomainList(), 
                SupplierPortalConstant.SCREEN_ID_WINV005));
            
            form.setUserLogin(setUserLogin());
            
            try {
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV005_BDOWNLOAD)) 
                {
                    this.priceDifferenceInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV005_BDOWNLOAD, locale);
                }
                form.setCannotDownloadMessage( Constants.EMPTY_STRING );
            } catch (ApplicationException ae) {
                form.setCannotDownloadMessage( ae.getMessage() );
            }
            try {
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV005_BRESET)) 
                {
                    this.priceDifferenceInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV005_BRESET, locale);
                }
                form.setCannotResetMessage( Constants.EMPTY_STRING );
            } catch (ApplicationException ae) {
                form.setCannotResetMessage( ae.getMessage() );
            }

        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_90_0002);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            form.setApplicationMessageList(errorMessageList);
        }
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }

    
    /**
     * Do search.
     * <p>
     * Search data by criteria from screen.
     * </p>
     * <ul>
     * <li>In case click search button, system search data and count by criteria
     * if count result size is 0 return message to screen "data not found".</li>
     * </ul>
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doSearch(ActionMapping mapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        Winv005Form form = (Winv005Form)actionForm;
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        String fristPage = form.getFirstPages();
        String page = form.getPages();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        form.setUserLogin(setUserLogin());

        if (Constants.PAGE_SEARCH.equals(fristPage) || Constants.PAGE_CLICK.equals(page)) {

            try {
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV005_BSEARCH)) 
                {
                    this.priceDifferenceInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV005_BSEARCH, locale);
                }
                
                List<PriceDifferenceInformationDomain> result = null;
                PriceDifferenceInformationReturnDomain returnDomain = this.search(form);
                if (null != returnDomain.getErrorMessageList()
                    && Constants.ZERO < returnDomain.getErrorMessageList().size()) {
                    errorMessageList = returnDomain.getErrorMessageList();
                    form.setMessageType(Constants.MESSAGE_FAILURE);
                } else {
                    result = returnDomain.getPriceDifferenceInformationDomainList();
                    SpsPagingUtil.setFormForPaging(
                        returnDomain.getPriceDifferenceInformationDomain(), form);
                    form.setPriceDifferenceInformationForDisplayList(result);
                    form.setMessageType(Constants.MESSAGE_SUCCESS);
                }
            } catch (ApplicationException e) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, e.getMessage()));
            } catch (Exception e) {
                String message = MessageUtil
                    .getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[]{e.getClass().toString(), e.getMessage()});
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, message));
            } finally {
                PlantSupplierWithScopeDomain plantSupplierWithScopeDomain =
                    new PlantSupplierWithScopeDomain();
                
                PriceDifferenceInformationDomain priceDifferenceInformationDomain =
                    new PriceDifferenceInformationDomain();
                priceDifferenceInformationDomain.setDCd(null);
                priceDifferenceInformationDomain.setVendorCd(null);
                
                DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
                UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
                List<UserRoleDomain> roleUserList = null;
                for (RoleScreenDomain roleScreen : userLoginDomain.getRoleScreenDomainList()) {
                    if (SupplierPortalConstant.SCREEN_ID_WINV005.equals(
                        roleScreen.getSpsMScreenDomain().getScreenCd())) {
                        roleUserList = roleScreen.getUserRoleDomainList();
                    }
                }
                dataScopeControlDomain.setLocale(locale);
                dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
                dataScopeControlDomain.setUserRoleDomainList(roleUserList);
                dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
                
                plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
                
                priceDifferenceInformationDomain = priceDifferenceInformationFacadeService
                    .searchInitial(plantSupplierWithScopeDomain, priceDifferenceInformationDomain);
                
                form.setInvoiceStatusList(priceDifferenceInformationDomain.getInvoiceStatusList());
                
                if (!Constants.MISC_CODE_ALL.equals(form.getVendorCd())) {
                    plantSupplierWithScopeDomain.getPlantSupplierDomain().setVendorCd(
                        form.getVendorCd());
                } else {
                    plantSupplierWithScopeDomain.getPlantSupplierDomain().setVendorCd(null);
                }
                plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
                List<SpsMMiscDomain> plantSupplierList
                    = this.priceDifferenceInformationFacadeService.searchSelectedCompanySupplier(
                        plantSupplierWithScopeDomain);
                form.setSPcdList(plantSupplierList);

                PlantDensoWithScopeDomain plantDensoWithScopeDomain
                    = new PlantDensoWithScopeDomain();
                PlantDensoDomain plantDensoDomain = new PlantDensoDomain();
                if (!Constants.MISC_CODE_ALL.equals(form.getDCd())) {
                    plantDensoDomain.setDCd(form.getDCd());
                } else {
                    plantDensoDomain.setDCd(null);
                }
                plantDensoWithScopeDomain.setPlantDensoDomain(plantDensoDomain);
                plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
                
                List<SpsMMiscDomain> plantDensoList = this.priceDifferenceInformationFacadeService
                    .searchSelectedCompanyDenso(plantDensoWithScopeDomain);
                form.setDPcdList(plantDensoList);
                form.setApplicationMessageList(errorMessageList);
                
                try {
                    if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                        RoleTypeConstants.WINV005_BDOWNLOAD)) 
                    {
                        this.priceDifferenceInformationFacadeService.searchRoleCanOperate(
                            RoleTypeConstants.WINV005_BDOWNLOAD, locale);
                    }
                    form.setCannotDownloadMessage( Constants.EMPTY_STRING );
                } catch (ApplicationException ae) {
                    form.setCannotDownloadMessage( ae.getMessage() );
                }
                try {
                    if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                        RoleTypeConstants.WINV005_BRESET)) 
                    {
                        this.priceDifferenceInformationFacadeService.searchRoleCanOperate(
                            RoleTypeConstants.WINV005_BRESET, locale);
                    }
                    form.setCannotResetMessage( Constants.EMPTY_STRING );
                } catch (ApplicationException ae) {
                    form.setCannotResetMessage( ae.getMessage() );
                }
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do return.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReturn(ActionMapping mapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        Winv005Form form = (Winv005Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        Winv005Form sessionForm = (Winv005Form)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_WINV005_FORM);
        List<PriceDifferenceInformationDomain> resultPage = null;
        PriceDifferenceInformationReturnDomain result  = null;
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        try {
            form.setSessionId(request.getSession().getId());
            this.setCriteriaOnScreen(sessionForm, form);
            result = this.search(form);
            form.setPriceDifferenceInformationForDisplayList(
                result.getPriceDifferenceInformationDomainList());
            this.initialCombobox(form);
            
            resultPage = result.getPriceDifferenceInformationDomainList();
            SpsPagingUtil.setFormForPaging(result.getPriceDifferenceInformationDomain(), form);
            form.setPriceDifferenceInformationForDisplayList(resultPage);
        } catch (ApplicationException ae) {
            result = new PriceDifferenceInformationReturnDomain();
            result.getErrorMessageList().add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, ae.getMessage()));
        } catch (Exception e) {
            result = new PriceDifferenceInformationReturnDomain();
            result.setLocale(locale);
        } finally{
            form.setApplicationMessageList(errorMessageList);
        }
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }

    /**
     * Do download.
     * <p>
     * Download data by criteria from screen.
     * </p>
     * <ul>
     * <li>In case click download button, system search data and count by
     * criteria.</li>
     * </ul>
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownload(ActionMapping mapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Winv005Form form = (Winv005Form)actionForm;
        PriceDifferenceInformationDomain criteria = new PriceDifferenceInformationDomain();
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = new 
            PlantSupplierWithScopeDomain();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        PriceDifferenceInformationReturnDomain priceDifferenceInformationReturnDomain = 
            new PriceDifferenceInformationReturnDomain();
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        List<UserRoleDomain> roleUserList = null;
        form.setUserLogin(setUserLogin());
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        for (RoleScreenDomain roleScreen : userLoginDomain
            .getRoleScreenDomainList()) {
            if (SupplierPortalConstant.SCREEN_ID_WINV005.equals(roleScreen
                .getSpsMScreenDomain().getScreenCd())) {
                roleUserList = roleScreen.getUserRoleDomainList();
            }
        }
        
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setLocale(locale);
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setUserType(
            userLoginDomain.getUserType());
        plantSupplierWithScopeDomain.getDataScopeControlDomain().setUserRoleDomainList(
            roleUserList);
        plantSupplierWithScopeDomain.getDataScopeControlDomain().
            setDensoSupplierRelationDomainList(null);
        
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleUserList);
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        String firstPages = form.getFirstPages();
        String pages = form.getPages();

        criteria.setFileId(form.getFileId());
        criteria.setLocale(locale);
        criteria.setInvoiceDateFrom(form.getInvoiceDateFrom());
        criteria.setInvoiceDateTo(form.getInvoiceDateTo());
        criteria.setInvoiceStatus(form.getInvoiceStatus());
        criteria.setInvoiceNo(form.getInvoiceNo());
        criteria.setCnDateFrom(form.getCnDateFrom());
        criteria.setCnDateTo(form.getCnDateTo());
        criteria.setCnNo(form.getCnNo());
        criteria.setAsnNo(form.getAsnNo());
        criteria.setVendorCd(form.getVendorCd());
        criteria.setSPcd(form.getSPcd());
        criteria.setDCd(form.getDCd());
        criteria.setDPcd(form.getDPcd());
        criteria.setDPn(form.getDPn());
        criteria.setSPn(form.getSPn());
        criteria.setSupplierAuthenList(form.getSupplierAuthenList());
        criteria.setDensoAuthenList(form.getDensoAuthenList());
        
        form.setUserLogin(super.setUserLogin());
        if (Constants.PAGE_SEARCH.equals(firstPages)
            || Constants.PAGE_CLICK.equals(pages)) {
            ServletOutputStream servletOutputStream = null;
            try {
                if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                    RoleTypeConstants.WINV005_BDOWNLOAD)) 
                {
                    this.priceDifferenceInformationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WINV005_BDOWNLOAD, locale);
                }
                
                priceDifferenceInformationReturnDomain = priceDifferenceInformationFacadeService
                    .searchPriceDifferenceInformationCsv(criteria);
                if (null != priceDifferenceInformationReturnDomain
                    .getErrorMessageList()) {
                    errorMessageList = priceDifferenceInformationReturnDomain
                        .getErrorMessageList();
                } else {
                    form.setPriceDifferenceInformationForDisplayList(
                        priceDifferenceInformationReturnDomain
                        .getPriceDifferenceInformationDomainList());
                    
                    StringBuffer resultStr = priceDifferenceInformationReturnDomain
                        .getCsvBufffer();
                    if (null != resultStr) {
                        byte[] output = priceDifferenceInformationReturnDomain
                            .getCsvBufffer().toString().getBytes();
                        super.setHttpHeaderForCsv(response, 
                            priceDifferenceInformationReturnDomain.getFileName());
                        response.setContentLength(output.length);
                        servletOutputStream = response.getOutputStream();
                        servletOutputStream.write(output, Constants.ZERO,
                            output.length);
                        servletOutputStream.flush();
                    }
                }

                if (null != priceDifferenceInformationReturnDomain
                    .getErrorMessageList()) {

                    form.setApplicationMessageList(priceDifferenceInformationReturnDomain
                        .getErrorMessageList());
                }

                form.setMessageType(Constants.MESSAGE_SUCCESS);
            } catch (ApplicationException e) {
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, e.getMessage()));
            } catch (Exception e) {
                String message = MessageUtil
                    .getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_80_0009);
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_FAILURE, message));
            } finally {
                form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
                PriceDifferenceInformationDomain priceDifferenceInformationDomain
                    = new PriceDifferenceInformationDomain();
                for (RoleScreenDomain roleScreen : userLoginDomain
                    .getRoleScreenDomainList()) {
                    if (SupplierPortalConstant.SCREEN_ID_WINV005.equals(roleScreen
                        .getSpsMScreenDomain().getScreenCd())) {
                        roleUserList = roleScreen.getUserRoleDomainList();
                    }
                }

                dataScopeControlDomain.setLocale(locale);
                dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
                dataScopeControlDomain.setUserRoleDomainList(roleUserList);
                dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
                plantSupplierWithScopeDomain
                    .setDataScopeControlDomain(dataScopeControlDomain);
                priceDifferenceInformationDomain.setDCd(null);
                priceDifferenceInformationDomain.setVendorCd(null);
                priceDifferenceInformationDomain = priceDifferenceInformationFacadeService
                    .searchInitial(plantSupplierWithScopeDomain, priceDifferenceInformationDomain);
                form.setInvoiceStatusList(priceDifferenceInformationDomain.getInvoiceStatusList());
                
                PlantDensoWithScopeDomain plantDensoWithScopeDomain = new 
                    PlantDensoWithScopeDomain();
                PlantDensoDomain plantDensoDomain = new PlantDensoDomain();
             
                if (!Constants.MISC_CODE_ALL.equals(form.getVendorCd())) {
                    plantSupplierWithScopeDomain.getPlantSupplierDomain()
                        .setVendorCd(form.getVendorCd());
                } else {
                    plantSupplierWithScopeDomain.getPlantSupplierDomain()
                        .setVendorCd(null);
                }
                
                plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);

                List<SpsMMiscDomain> plantSupplierList = this
                    .priceDifferenceInformationFacadeService
                    .searchSelectedCompanySupplier(plantSupplierWithScopeDomain);
                form.setSPcdList(plantSupplierList);
                
                if (!Constants.MISC_CODE_ALL.equals(form.getDCd())) {
                    plantDensoDomain.setDCd(form.getDCd());
                } else {
                    plantDensoDomain.setDCd(null);
                }
                plantDensoWithScopeDomain.setPlantDensoDomain(plantDensoDomain);
                    
                plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
                
                List<SpsMMiscDomain> plantDensoList = this.priceDifferenceInformationFacadeService
                    .searchSelectedCompanyDenso(plantDensoWithScopeDomain);
                form.setDPcdList(plantDensoList);
                
                form.setApplicationMessageList(errorMessageList);
                try {
                    if (null != servletOutputStream) {
                        servletOutputStream.close();
                    }
                } catch (Exception e2) {
                    throw new ApplicationException(e2.getMessage());
                }
            }
        }
        if (null != form.getApplicationMessageList()
            && Constants.ZERO != form.getApplicationMessageList().size()) {
            return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
        } else {
            return null;
        }
    }

    /**
     * Do Select DENSO Company.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDCd(ActionMapping mapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception
    {
        Winv005Form form = (Winv005Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        List<UserRoleDomain> roleUserList = null;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        PlantDensoDomain plantDensoDomain = new PlantDensoDomain();
        List<SpsMMiscDomain> plantDensoList = null;
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        try {
            for (RoleScreenDomain roleScreen : userLoginDomain.getRoleScreenDomainList()) {
                if (SupplierPortalConstant.SCREEN_ID_WINV005.equals(
                    roleScreen.getSpsMScreenDomain().getScreenCd()))
                {
                    roleUserList = roleScreen.getUserRoleDomainList();
                }
            }
            plantDensoWithScopeDomain.setPlantDensoDomain(plantDensoDomain);
            dataScopeControlDomain.setLocale(locale);
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleUserList);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            plantDensoWithScopeDomain.getPlantDensoDomain().setDCd(form.getDCd());
            
            plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
            plantDensoList = priceDifferenceInformationFacadeService
                .searchSelectedCompanyDenso(plantDensoWithScopeDomain);
            form.setUserLogin(setUserLogin());

            printJson(response, new JsonResult((List<Object>)(Object)plantDensoList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }

    /**
     * Do Select DENSO Plant.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectDPcd(ActionMapping mapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Winv005Form form = (Winv005Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        List<UserRoleDomain> roleUserList = null;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        PlantDensoDomain plantDensoDomain = new PlantDensoDomain();
        List<CompanyDensoDomain> companyDensoList = null;
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        try {
            for (RoleScreenDomain roleScreen : userLoginDomain.getRoleScreenDomainList()) {
                if (SupplierPortalConstant.SCREEN_ID_WINV005.equals(
                    roleScreen.getSpsMScreenDomain().getScreenCd()))
                {
                    roleUserList = roleScreen.getUserRoleDomainList();
                }
            }
            plantDensoWithScopeDomain.setPlantDensoDomain(plantDensoDomain);
            dataScopeControlDomain.setLocale(locale);
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleUserList);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            plantDensoWithScopeDomain.getPlantDensoDomain().setDPcd(form.getDPcd());
            
            plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
            companyDensoList = priceDifferenceInformationFacadeService
                .searchSelectedPlantDenso(plantDensoWithScopeDomain);
            form.setUserLogin(setUserLogin());

            printJson(response, new JsonResult((List<Object>)(Object)companyDensoList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }

    /**
     * Do Select Supplier Company.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSCd(ActionMapping mapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Winv005Form form = (Winv005Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<UserRoleDomain> roleUserList = null;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();

        List<SpsMMiscDomain> plantSupplierList = null;
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        try {
            for (RoleScreenDomain roleScreen : userLoginDomain.getRoleScreenDomainList()) {
                if (SupplierPortalConstant.SCREEN_ID_WINV005.equals(
                    roleScreen.getSpsMScreenDomain().getScreenCd()))
                {
                    roleUserList = roleScreen.getUserRoleDomainList();
                }
            }
            dataScopeControlDomain.setLocale(locale);
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleUserList);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            plantSupplierWithScopeDomain.getPlantSupplierDomain().setVendorCd(form.getVendorCd());
            plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
            plantSupplierList = priceDifferenceInformationFacadeService
                .searchSelectedCompanySupplier(plantSupplierWithScopeDomain);
            form.setUserLogin(setUserLogin());

            printJson(response, new JsonResult((List<Object>)(Object)plantSupplierList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }

    /**
     * Do Select Supplier Plant.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSPcd(ActionMapping mapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {

        Winv005Form form = (Winv005Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<UserRoleDomain> roleUserList = null;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        List<CompanySupplierDomain> companySupplierList = null;
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        try {
            for (RoleScreenDomain roleScreen : userLoginDomain.getRoleScreenDomainList()) {
                if (SupplierPortalConstant.SCREEN_ID_WINV005.equals(
                    roleScreen.getSpsMScreenDomain().getScreenCd()))
                {
                    roleUserList = roleScreen.getUserRoleDomainList();
                }
            }
            dataScopeControlDomain.setLocale(locale);
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleUserList);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            plantSupplierWithScopeDomain.getPlantSupplierDomain().setSPcd(form.getSPcd());
            plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
            companySupplierList = priceDifferenceInformationFacadeService
                .searchSelectedPlantSupplier(plantSupplierWithScopeDomain);
            form.setUserLogin(setUserLogin());

            printJson(response, new JsonResult((List<Object>)(Object)companySupplierList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }

    /**
     * Do forward page.
     * <p>
     * Forwarding to target page.
     * </p>
     * <ul>
     * <li>In case click Invoice no link, system forwarding to target page and
     * send invoice no for initial screen.</li>
     * </ul>
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doForwardPage(ActionMapping mapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        
        Winv005Form form = (Winv005Form)actionForm;
        UserLoginDomain sessionUserLoginDomain = (UserLoginDomain)DensoContext.get().
            getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
        int statusIndex = Integer.parseInt(form.getStatusIndex());
        
        StringBuffer forward = null;
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        
        try {
            form.setUserLogin(super.setUserLogin());
            
            if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                RoleTypeConstants.WINV005_LINVOICENO)) 
            {
                this.priceDifferenceInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV005_LINVOICENO, locale);
            }
            
            Winv005Form sessionForm = new Winv005Form();
            this.setCriteriaOnScreen(form, sessionForm);
            DensoContext.get().putGeneralArea(SupplierPortalConstant.SESSION_WINV005_FORM,
                sessionForm);
            
            forward = new StringBuffer();
            forward.append(SupplierPortalConstant.URL_WINV004_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_INITIAL);
            forward.append(SupplierPortalConstant.URL_PARAM_DSCID)
                .append(sessionUserLoginDomain.getDscId());
            forward.append(SupplierPortalConstant.URL_PARAM_INVOICE_ID)
                .append(form.getPriceDifferenceInformationForDisplayList()
                    .get(statusIndex).getSpsTInvoiceDomain().getInvoiceId());
            forward.append(SupplierPortalConstant.URL_PARAM_SCREENID)
                .append(SupplierPortalConstant.SCREEN_ID_WINV005);
            
            return new ActionForward(forward.toString());
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil
                .getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_80_0009);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        }
        
        try {
            UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
            form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
            
            PlantSupplierWithScopeDomain plantSupplierWithScopeDomain =
                new PlantSupplierWithScopeDomain();
            PriceDifferenceInformationDomain priceDifferenceInformationDomain =
                new PriceDifferenceInformationDomain();
            DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
            List<UserRoleDomain> roleUserList = null;
            
            for (RoleScreenDomain roleScreen : userLoginDomain.getRoleScreenDomainList()) {
                if (SupplierPortalConstant.SCREEN_ID_WINV005.equals(roleScreen
                    .getSpsMScreenDomain().getScreenCd())) {
                    roleUserList = roleScreen.getUserRoleDomainList();
                }
            }
            dataScopeControlDomain.setLocale(locale);
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleUserList);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
            priceDifferenceInformationDomain.setDCd(null);
            priceDifferenceInformationDomain.setVendorCd(null);
            
            priceDifferenceInformationDomain = priceDifferenceInformationFacadeService
                .searchInitial(plantSupplierWithScopeDomain, priceDifferenceInformationDomain);
            
            form.setInvoiceStatusList(priceDifferenceInformationDomain.getInvoiceStatusList());
            
            List<UserRoleDomain> roleUserList1 = null;
            DataScopeControlDomain dataScopeControlDomain1 = new DataScopeControlDomain();
            
            PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
            PlantDensoDomain plantDensoDomain = new PlantDensoDomain();
         
            for (RoleScreenDomain roleScreen : userLoginDomain.getRoleScreenDomainList()) {
                if (SupplierPortalConstant.SCREEN_ID_WINV005
                    .equals(roleScreen.getSpsMScreenDomain().getScreenCd())) {
                    roleUserList1 = roleScreen.getUserRoleDomainList();
                }
            }
            if (!Constants.MISC_CODE_ALL.equals(form.getVendorCd())) {
                plantSupplierWithScopeDomain.getPlantSupplierDomain()
                    .setVendorCd(form.getVendorCd());
            } else {
                plantSupplierWithScopeDomain.getPlantSupplierDomain()
                    .setVendorCd(null);
            }
            dataScopeControlDomain1.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain1.setUserRoleDomainList(roleUserList1);
            dataScopeControlDomain1.setDensoSupplierRelationDomainList(null);
            
            plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain1);

            List<SpsMMiscDomain> plantSupplierList = this.priceDifferenceInformationFacadeService
                .searchSelectedCompanySupplier(plantSupplierWithScopeDomain);
            form.setSPcdList(plantSupplierList);
            
            if (!Constants.MISC_CODE_ALL.equals(form.getDCd())) {
                plantDensoDomain.setDCd(form.getDCd());
            } else {
                plantDensoDomain.setDCd(null);
            }
            plantDensoWithScopeDomain.setPlantDensoDomain(plantDensoDomain);
            dataScopeControlDomain1.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain1.setUserRoleDomainList(roleUserList1);
            dataScopeControlDomain1.setDensoSupplierRelationDomainList(null);
                
            plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain1);
            
            List<SpsMMiscDomain> plantDensoList = this
                .priceDifferenceInformationFacadeService
                .searchSelectedCompanyDenso(plantDensoWithScopeDomain);
            form.setDPcdList(plantDensoList);
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil
                .getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_80_0009);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            form.setApplicationMessageList(errorMessageList);
        }

        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
        
    }

    /**
     * Do Download PDF Change.
     * <p>Download PDF Change from File Management Service</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doClickLinkMore(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Winv005Form form = (Winv005Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        PriceDifferenceInformationDomain criteria = new PriceDifferenceInformationDomain();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();

        OutputStream output = null;
        try {
            
            criteria.setFileId(ContextParams.getLegendFileId());
            criteria.setLocale(locale);
            FileManagementDomain file
                = this.priceDifferenceInformationFacadeService.searchFileName(criteria);
            
            super.setHttpHeaderForFileManagement(response, request, file.getFileName());
            output = response.getOutputStream();
            this.priceDifferenceInformationFacadeService.searchLegendInfo(criteria, output);
            return null;
        } catch (ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
            criteria = new PriceDifferenceInformationDomain();
            this.search(form);
        } catch (Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001)));
            criteria = new PriceDifferenceInformationDomain();
            this.search(form);
        } finally {
            form.getApplicationMessageList().addAll(applicationMessageList);
            if (null != output) {
                output.close();
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap = new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_SEARCH,
            SupplierPortalConstant.METHOD_DO_SEARCH);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_FORWARD_PAGE,
            SupplierPortalConstant.METHOD_DO_FORWARD_PAGE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_RETURN, 
            SupplierPortalConstant.METHOD_DO_RETURN);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE, 
            SupplierPortalConstant.METHOD_DO_CLICK_LINK_MORE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_DCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SCD,
            SupplierPortalConstant.METHOD_DO_DO_SELECT_SCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_SPCD);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_DPCD
            , SupplierPortalConstant.METHOD_DO_SELECT_DPCD);
        return keyMethodMap;
    }
    
    /**
     * Search.
     * <p>
     * Search data by priceDifferenceInformationDomain from screen.
     * </p>
     * </ul>
     * 
     * @param form the form
     * @return list of invoice information domain
     * @throws ApplicationException the application exception
     */
    private PriceDifferenceInformationReturnDomain search(Winv005Form form)
        throws ApplicationException {
        PriceDifferenceInformationDomain Criteria = new PriceDifferenceInformationDomain();
        PriceDifferenceInformationReturnDomain priceDifferenceInformationReturnDomain = 
            new PriceDifferenceInformationReturnDomain();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        Criteria.setLocale(locale);
        form.setUserLogin(setUserLogin());
        if (!Strings.judgeBlank(form.getInvoiceDateFrom())) {
            Criteria.setInvoiceDateFrom(form.getInvoiceDateFrom());
        }
        if (!Strings.judgeBlank(form.getInvoiceDateTo())) {
            Criteria.setInvoiceDateTo(form.getInvoiceDateTo());
        }
        if (!Strings.judgeBlank(form.getInvoiceStatus())) {
            Criteria.setInvoiceStatus(form.getInvoiceStatus());
        }
        if (!Strings.judgeBlank(form.getInvoiceNo().trim())) {
            Criteria.setInvoiceNo(form.getInvoiceNo().toUpperCase().trim());
        }
        if (!Strings.judgeBlank(form.getCnDateFrom())) {
            Criteria.setCnDateFrom(form.getCnDateFrom());
        }
        if (!Strings.judgeBlank(form.getCnDateTo())) {
            Criteria.setCnDateTo(form.getCnDateTo());
        }
        if (!Strings.judgeBlank(form.getCnNo().trim())) {
            Criteria.setCnNo(form.getCnNo().toUpperCase().trim());
        }
        if (!Strings.judgeBlank(form.getAsnNo().trim())) {
            Criteria.setAsnNo(form.getAsnNo().toUpperCase().trim());
        }
        if (!Strings.judgeBlank(form.getVendorCd())) {
            Criteria.setVendorCd(form.getVendorCd());
        }
        if (!Strings.judgeBlank(form.getSPcd())) {
            Criteria.setSPcd(form.getSPcd());
        }
        if (!Strings.judgeBlank(form.getDCd())) {
            Criteria.setDCd(form.getDCd());
        } 
        if (!Strings.judgeBlank(form.getDPcd())) {
            Criteria.setDPcd(form.getDPcd());
        }
        if (!Strings.judgeBlank(form.getDPn().trim())) {
            Criteria.setDPn(form.getDPn().toUpperCase().trim());
        }
        if (!Strings.judgeBlank(form.getSPn().trim())) {
            Criteria.setSPn(form.getSPn().toUpperCase().trim());
        }
        
        Criteria.setSupplierAuthenList(form.getSupplierAuthenList());
        Criteria.setDensoAuthenList(form.getDensoAuthenList());
        
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        try {
            if(Constants.PAGE_SEARCH.equals(form.getFirstPages())){
                Criteria.setPageNumber(Constants.ONE);
            }else{
                Criteria.setPageNumber(form.getPageNo());
            }
            
            priceDifferenceInformationReturnDomain = priceDifferenceInformationFacadeService
                .searchPriceDifferenceInformation(Criteria);
            
            if (null != priceDifferenceInformationReturnDomain.getErrorMessageList()
                && Constants.ZERO < priceDifferenceInformationReturnDomain
                    .getErrorMessageList().size()) {
                errorMessageList = new ArrayList<ApplicationMessageDomain>();
                errorMessageList.addAll(priceDifferenceInformationReturnDomain
                    .getErrorMessageList());
                priceDifferenceInformationReturnDomain
                    .setErrorMessageList(errorMessageList);
            }
            return priceDifferenceInformationReturnDomain;
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            priceDifferenceInformationReturnDomain
                .setErrorMessageList(errorMessageList);
            form.setApplicationMessageList(errorMessageList);
        }
        return priceDifferenceInformationReturnDomain;
    }
    
    /**
     * setCriteriaOnScreen.
     * <p>Setting data between action form and  session form.</p>
     * 
     * @param sessionForm the Winv005Form
     * @param targetForm the Winv005Form
     */
    private void setCriteriaOnScreen(Winv005Form sessionForm, Winv005Form targetForm){
        targetForm.setInvoiceDateFrom(sessionForm.getInvoiceDateFrom());
        targetForm.setInvoiceDateTo(sessionForm.getInvoiceDateTo());
        targetForm.setInvoiceStatus(sessionForm.getInvoiceStatus());
        targetForm.setInvoiceNo(sessionForm.getInvoiceNo());
        targetForm.setCnDateFrom(sessionForm.getCnDateFrom());
        targetForm.setCnDateTo(sessionForm.getCnDateTo());
        targetForm.setCnNo(sessionForm.getCnNo());
        targetForm.setAsnNo(sessionForm.getAsnNo());
        targetForm.setVendorCd(sessionForm.getVendorCd());
        targetForm.setSPcd(sessionForm.getSPcd());
        targetForm.setDCd(sessionForm.getDCd());
        targetForm.setDPcd(sessionForm.getDPcd());
        targetForm.setDPn(sessionForm.getDPn());
        targetForm.setSPn(sessionForm.getSPn());
        targetForm.setSupplierAuthenList(sessionForm.getSupplierAuthenList());
        targetForm.setDensoAuthenList(sessionForm.getDensoAuthenList());
        
        targetForm.setFirstPages(Constants.PAGE_SEARCH);
    }
    
    /**
     * initialCombobox.
     * <p>Initial combo box from screen.</p>
     * 
     * @param form the WINV005 form
     * @throws ApplicationException the application exception
     * @throws Exception the exception
     */
    private void initialCombobox(Winv005Form form) 
        throws ApplicationException, Exception {
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        List<UserRoleDomain> roleUserList = null;
        UserLoginDomain userLoginDomain = this.getCurrentUserLoginInformation();
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        List<ApplicationMessageDomain> errorMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        PriceDifferenceInformationDomain priceDifferenceInformationDomain =
            new PriceDifferenceInformationDomain();
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain =
            new PlantSupplierWithScopeDomain();
        PlantDensoWithScopeDomain plantDensoWithScopeDomain = new PlantDensoWithScopeDomain();
        PlantSupplierDomain plantSupplierDomain =  new PlantSupplierDomain();
        PlantDensoDomain plantDensoDomain = new PlantDensoDomain();
        
        try {
            if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                RoleTypeConstants.WINV005_BDOWNLOAD)) 
            {
                this.priceDifferenceInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV005_BDOWNLOAD, locale);
            }
            form.setCannotDownloadMessage( Constants.EMPTY_STRING );
        } catch (ApplicationException ae) {
            form.setCannotDownloadMessage( ae.getMessage() );
        }
        try {
            if (!getCurrentUserLoginInformation().getButtonLinkNameList().contains(
                RoleTypeConstants.WINV005_BRESET)) 
            {
                this.priceDifferenceInformationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WINV005_BRESET, locale);
            }
            form.setCannotResetMessage( Constants.EMPTY_STRING );
        } catch (ApplicationException ae) {
            form.setCannotResetMessage( ae.getMessage() );
        }
        
        try {

            for (RoleScreenDomain roleScreen : userLoginDomain
                .getRoleScreenDomainList()) {
                if (SupplierPortalConstant.SCREEN_ID_WINV005.equals(roleScreen
                    .getSpsMScreenDomain().getScreenCd())) {
                    roleUserList = roleScreen.getUserRoleDomainList();
                }
            }

            dataScopeControlDomain.setLocale(locale);
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleUserList);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            plantSupplierWithScopeDomain
                .setDataScopeControlDomain(dataScopeControlDomain);
            
            priceDifferenceInformationDomain = this.priceDifferenceInformationFacadeService
                .searchInitial(
                    plantSupplierWithScopeDomain, priceDifferenceInformationDomain);
            
            Calendar firstDateOfCurrentMounth = Calendar.getInstance();
            firstDateOfCurrentMounth.set(Calendar.DAY_OF_MONTH, Constants.ONE);
            Calendar lastDateOfCurrentMounth = Calendar.getInstance();
            lastDateOfCurrentMounth.set(Calendar.DAY_OF_MONTH,
                lastDateOfCurrentMounth.getActualMaximum(Calendar.DAY_OF_MONTH));
            form.setSCdList(priceDifferenceInformationDomain.getCompanySupplierDomainList());
            form.setDCdList(priceDifferenceInformationDomain.getCompanyDensoDomainList());
            
            plantSupplierWithScopeDomain
                .setPlantSupplierDomain(plantSupplierDomain);
            plantSupplierWithScopeDomain
                .setDataScopeControlDomain(dataScopeControlDomain);
        
            List<SpsMMiscDomain> plantSupplierList = this
                .priceDifferenceInformationFacadeService
                .searchSelectedCompanySupplier(plantSupplierWithScopeDomain);
            form.setSPcdList(plantSupplierList);
            
            plantDensoWithScopeDomain.setPlantDensoDomain(plantDensoDomain);
            dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleUserList);
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
                
            plantDensoWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
            List<SpsMMiscDomain> plantDensoList = this
                .priceDifferenceInformationFacadeService
                .searchSelectedCompanyDenso(plantDensoWithScopeDomain);
            form.setDPcdList(plantDensoList);
            
            form.setInvoiceStatusList(priceDifferenceInformationDomain
                .getInvoiceStatusList());
            form.setUserLogin(super.setUserLogin());
            
            form.setSupplierAuthenList(super.doCreateSupplierPlantAuthenList(
                priceDifferenceInformationDomain.getDensoSupplierRelationDomainList(), 
                SupplierPortalConstant.SCREEN_ID_WINV005));
            form.setDensoAuthenList(super.doCreateDensoPlantAuthenList(
                priceDifferenceInformationDomain.getDensoSupplierRelationDomainList(), 
                SupplierPortalConstant.SCREEN_ID_WINV005));
        
        } catch (Exception e) {
            String message = MessageUtil.getApplicationMessageHandledException(
                locale, SupplierPortalConstant.ERROR_CD_SP_90_0002);
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            form.setApplicationMessageList(errorMessageList);
        }
        
    }

    /**
     * Method to get current user login information detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return the user login domain
     */
    private UserLoginDomain getCurrentUserLoginInformation(){
        
        UserLoginDomain userLoginDomain = new UserLoginDomain();
        userLoginDomain = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }
}
