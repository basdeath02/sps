/*
 * ModifyDate Developmentcompany    Describe 
 * 2014/07/28 CSI Parichat          Create 
 * 2017/08/30 Netband U.Rungsiwut   SPS Phase II
 * 2018/04/11 Netband U.Rungsiwut   Generate ASN PDF via click on screen
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.library.filemanagerstream.business.domain.FileManagementDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
//import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.AsnMaintenanceFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.DateUtil;
import com.globaldenso.asia.sps.common.utils.FileUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Wshp001Form;
import com.globaldenso.asia.sps.presentation.form.Wshp004Form;

/**
 * The Class ASNMaintenanceAction.
 * @author CSI
 */
public class AsnMaintenanceAction extends CoreAction {
    
    /** The asn maintenance facade service. */
    private AsnMaintenanceFacadeService asnMaintenanceFacadeService; 

    /** The default constructor. */
    public AsnMaintenanceAction() {
        super();
    }

    /**
     * Set the asn maintenance facade service.
     * 
     * @param asnMaintenanceFacadeService the asn maintenance facade service to set
     */
    public void setAsnMaintenanceFacadeService(
        AsnMaintenanceFacadeService asnMaintenanceFacadeService) {
        this.asnMaintenanceFacadeService = asnMaintenanceFacadeService;
    }

    /**
     * Do initial.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings({Constants.SUPPRESS_WARNINGS_RAWTYPES, Constants.SUPPRESS_WARNINGS_UNCHECKED})
    public ActionForward doInitial(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Wshp004Form form = (Wshp004Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        boolean hasError = false;
        AsnMaintenanceDomain asnMaintenanceDomain = new AsnMaintenanceDomain();
        AsnMaintenanceDomain criteria = new AsnMaintenanceDomain();
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDoInformationList = 
            new ArrayList<AcknowledgedDoInformationReturnDomain>();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>(); 
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setDscId(userLogin.getDscId());
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setMode(Constants.EMPTY_STRING);
        form.setSuccessFlag(Constants.STR_ZERO);
        form.setUserLogin(super.setUserLogin());
        try {
            if(SupplierPortalConstant.SCREEN_ID_WSHP001.equals(form.getScreenId())){
                //Gets Warning Message from WSHP001 Screen
                List<ApplicationMessageDomain> warningMsgList = null;
                Wshp001Form sessionForm = (Wshp001Form)DensoContext.get().getGeneralArea(
                    SupplierPortalConstant.SESSION_WSHP001_FORM);
                warningMsgList = sessionForm.getApplicationMessageList();
                if(null != warningMsgList && Constants.ZERO < warningMsgList.size()){
                    applicationMessageList.addAll(warningMsgList);
                }
                
                Map<String, AcknowledgedDoInformationReturnDomain> selectedDoMap 
                    = (HashMap<String, AcknowledgedDoInformationReturnDomain>)DensoContext.get().
                        getGeneralArea(Constants.MAP_KEY_SELECTED_DO);
                if(null != selectedDoMap && !selectedDoMap.isEmpty()){
                    Iterator itr = selectedDoMap.keySet().iterator();
                    while(itr.hasNext()){
                        String keyStr = (String)itr.next();
                        acknowledgedDoInformationList.add(selectedDoMap.get(keyStr));
                    }
                }
                criteria.setLocale(locale);
                criteria.setDoInfoList(acknowledgedDoInformationList);
                asnMaintenanceDomain = asnMaintenanceFacadeService.searchInitialWshp001(criteria);
                if(Constants.ZERO < asnMaintenanceDomain.getDoGroupAsnList().size()){
                    form.setDoGroupAsnList(asnMaintenanceDomain.getDoGroupAsnList());
                }
                if(Constants.ZERO < asnMaintenanceDomain.getChangeReasonList().size()){
                    form.setChgReasonList(asnMaintenanceDomain.getChangeReasonList());
                }
            }else if(SupplierPortalConstant.SCREEN_ID_WSHP008.equals(form.getScreenId())
                || SupplierPortalConstant.SCREEN_ID_WSHP009.equals(form.getScreenId())){
                criteria.setLocale(locale);
                criteria.setAsnNo(form.getAsnNo());
                criteria.setDCd(form.getDCd());
                asnMaintenanceDomain = asnMaintenanceFacadeService.searchInitialWshp008(criteria);
                this.doSetForm(asnMaintenanceDomain, form);
            }else if(SupplierPortalConstant.SCREEN_ID_WSHP006.equals(form.getScreenId())){
                criteria.setLocale(locale);
                criteria.setAsnNo(form.getAsnNo());
                criteria.setSessionId(form.getSessionId());
                asnMaintenanceDomain = asnMaintenanceFacadeService.searchInitialWshp006(criteria);
                this.doSetForm(asnMaintenanceDomain, form);
            }
//            Start to add the warning message for case create ASN before reach tag
            if(SupplierPortalConstant.SCREEN_ID_WSHP006.equals(form.getScreenId())
                || SupplierPortalConstant.SCREEN_ID_WSHP001.equals(form.getScreenId())){
                if(asnMaintenanceDomain.getDoGroupAsnList().get(Constants.ZERO).getGroupDoDomain().getOneWayKanbanTagFlag().equals(Constants.STR_ZERO)
                    && !asnMaintenanceDomain.getDoGroupAsnList().get(Constants.ZERO).getGroupDoDomain().getTagOutput().equals(Constants.STR_FOUR)){
                    applicationMessageList.add(
                        new ApplicationMessageDomain(Constants.MESSAGE_WARNING,
                            MessageUtil.getApplicationMessage(locale,
                                SupplierPortalConstant.ERROR_CD_SP_W2_0007)));
                }
            }
//            End to add the warning message for case create ASN before reach tag
            
            form.setOperateReviseQtyMsg(Constants.EMPTY_STRING);
            form.setUtc(criteria.getUtc());
            form.setAllowReviseQtyFlag(asnMaintenanceDomain.getAllowReviseQtyFlag());
            form.setSkipValidateLotSizeFlag(asnMaintenanceDomain.getSkipValidateLotSizeFlag());
            form.setUnmatchedLotSizeFlag(Constants.STR_ZERO);
            form.setContinueOperation(Constants.STR_ZERO);
            try {
                if(!userLogin.getButtonLinkNameList().contains(
                    RoleTypeConstants.WSHP004_BREVISESHIPPINGQTY)){
                    this.asnMaintenanceFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WSHP004_BREVISESHIPPINGQTY, locale);
                }
            } catch (ApplicationException ex) {
                form.setOperateReviseQtyMsg(ex.getMessage());
            }
        } catch (ApplicationException ex) {
            hasError = true;
            applicationMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, ex.getMessage()));
        } catch (Exception e) {
            hasError = true;
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            form.setApplicationMessageList(applicationMessageList);
            if(hasError){
                form.setMode(Constants.MODE_REGISTER);
                form.setAsnNo(Constants.EMPTY_STRING);
                form.setAsnRcvStatus(Constants.EMPTY_STRING);
                form.setInvStatus(Constants.EMPTY_STRING);
                form.setLastModified(Constants.EMPTY_STRING);
                form.setActualEtdDate(Constants.EMPTY_STRING);
                form.setActualEtdTime(Constants.EMPTY_STRING);
                form.setPlanEtaDate(Constants.EMPTY_STRING);
                form.setPlanEtaTime(Constants.EMPTY_STRING);
                form.setNoOfPallet(Constants.EMPTY_STRING);
                form.setTripNo(Constants.EMPTY_STRING);
                form.setDoGroupAsnList(null);
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Save and Send Asn.
     * <p>Save detail data of ASN and set ASN status to "Issued".</p>
     * <ul>
     * <li>In case click save&send button, system will save data of asn 
     * , send asn data to denso by email and send asn data to cigma system.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doSaveAndSend(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Wshp004Form form = (Wshp004Form)actionForm;
        boolean isSuccess = false;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AsnMaintenanceDomain asnMaintenanceDomain = new AsnMaintenanceDomain();
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        asnMaintenanceDomain.setLocale(locale);
        try{
            if(!userLogin.getButtonLinkNameList().contains(RoleTypeConstants.WSHP004_BSAVESEND)){
                this.asnMaintenanceFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP004_BSAVESEND, locale);
            }
            
            if(!StringUtil.checkNullOrEmpty(form.getAsnNo())){
                asnMaintenanceDomain.setAsnNo(form.getAsnNo());
            }
            if(!StringUtil.checkNullOrEmpty(form.getAsnRcvStatus())){
                asnMaintenanceDomain.setAsnRcvStatus(form.getAsnRcvStatus());
            }
            if(!StringUtil.checkNullOrEmpty(form.getInvStatus())){
                asnMaintenanceDomain.setInvoiceStatus(form.getInvStatus());
            }
            if(!StringUtil.checkNullOrEmpty(form.getLastModified())){
                asnMaintenanceDomain.setLastModified(form.getLastModified());
            }
            if(!StringUtil.checkNullOrEmpty(form.getActualEtdDate().trim())){
                asnMaintenanceDomain.setActualEtdDate(form.getActualEtdDate().trim());
            }
            if(!StringUtil.checkNullOrEmpty(form.getActualEtdTime().trim())){
                asnMaintenanceDomain.setActualEtdTime(form.getActualEtdTime().trim());
            }
            if(!StringUtil.checkNullOrEmpty(form.getPlanEtaDate().trim())){
                asnMaintenanceDomain.setPlanEtaDate(form.getPlanEtaDate().trim());
            }
            if(!StringUtil.checkNullOrEmpty(form.getPlanEtaTime().trim())){
                asnMaintenanceDomain.setPlanEtaTime(form.getPlanEtaTime().trim());
            }
            if(!StringUtil.checkNullOrEmpty(form.getNoOfPallet().trim())){
                asnMaintenanceDomain.setNoOfPallet(form.getNoOfPallet().trim());
            }
            if(!StringUtil.checkNullOrEmpty(form.getTripNo().trim())){
                asnMaintenanceDomain.setTripNo(form.getTripNo().trim());
            }
            asnMaintenanceDomain.setSkipValidateLotSizeFlag(form.getSkipValidateLotSizeFlag());
            asnMaintenanceDomain.setDscId(form.getDscId());
            asnMaintenanceDomain.setTemporaryMode(form.getTemporaryMode());
            asnMaintenanceDomain.setDoGroupAsnList(form.getDoGroupAsnList());
            boolean result 
                = asnMaintenanceFacadeService.transactSaveAndSendAsn(asnMaintenanceDomain);
            if(result){
                form.setAsnNo(asnMaintenanceDomain.getAsnNo());
                form.setAsnRcvStatus(asnMaintenanceDomain.getAsnRcvStatus());
                form.setLastModified(asnMaintenanceDomain.getLastModified());
                form.setDoGroupAsnList(asnMaintenanceDomain.getDoGroupAsnList());
                form.setAllowReviseQtyFlag(asnMaintenanceDomain.getAllowReviseQtyFlag());
                
                if(null != asnMaintenanceDomain.getWarningMessageList() 
                    && !asnMaintenanceDomain.getWarningMessageList().isEmpty()){
                    applicationMessageList.addAll(asnMaintenanceDomain.getWarningMessageList());
                }
                if(SupplierPortalConstant.SCREEN_ID_WSHP006.equals(form.getScreenId())){
                    AsnMaintenanceDomain criteria = new AsnMaintenanceDomain();
                    criteria.setSessionId(form.getSessionId());
                    criteria.setAsnNo(form.getAsnNo());
                    asnMaintenanceFacadeService.deleteTmpUploadAsn(asnMaintenanceDomain);
                }
                //Send Email
                String mode = Constants.EMPTY_STRING;
                if(Constants.STR_ONE.equals(form.getTemporaryMode())){
                    mode = Constants.STR_CREATE;
                }else{
                    mode = Constants.STR_UPDATE;
                }
                boolean isSuccessSendMail = this.asnMaintenanceFacadeService.searchSendingEmail(
                    asnMaintenanceDomain, mode);
                if(isSuccessSendMail){
                    applicationMessageList.add(new ApplicationMessageDomain(
                        Constants.MESSAGE_SUCCESS, 
                        MessageUtil.getApplicationMessageHandledException(locale, 
                            SupplierPortalConstant.ERROR_CD_SP_I6_0012)));
                }else{
                    applicationMessageList.add(new ApplicationMessageDomain(
                        Constants.MESSAGE_SUCCESS,
                        MessageUtil.getApplicationMessageWithLabel(locale,
                            SupplierPortalConstant.ERROR_CD_SP_I6_0020,
                            SupplierPortalConstant.LBL_REGISTER)));
                }
                form.setTemporaryMode(asnMaintenanceDomain.getTemporaryMode());
                form.setSkipValidateLotSizeFlag(Constants.STR_ZERO);
                form.setUnmatchedLotSizeFlag(Constants.STR_ZERO);
                form.setContinueOperation(Constants.STR_ZERO);
                form.setSuccessFlag(Constants.STR_ONE);
                isSuccess = true;
            }else{
                if(null != asnMaintenanceDomain.getErrorMessageList()
                    && Constants.ZERO < asnMaintenanceDomain.getErrorMessageList().size()){
                    applicationMessageList = asnMaintenanceDomain.getErrorMessageList();
                }else{
                    form.setSkipValidateLotSizeFlag(Constants.STR_ONE);
                }
                form.setUnmatchedLotSizeFlag(asnMaintenanceDomain.getUnmatchedLotSizeFlag());
            }
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                ex.getMessage()));
        }catch(Exception e){
            String errorMessage = e.getCause().toString();
            if(Constants.ZERO <= errorMessage.indexOf(Constants.ERROR_CD_ORA_00001)){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_E6_0050)));
            }else{
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }
        }finally{
            form.setApplicationMessageList(applicationMessageList);
            if(isSuccess){
                form.setMode(Constants.MODE_SAVE);
            }else{
                if(Constants.STR_ONE.equals(form.getTemporaryMode())){
                    form.setMode(Constants.EMPTY_STRING);
                }else{
                    form.setMode(Constants.MODE_SAVE);
                }
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Cancel and Send Asn.
     * <p>Cancel sending asn data and set ASN status to "Cancel".</p>
     * <ul>
     * <li>In case click 'Cancel ASN & Send' button, system will inform cancel asn data 
     * to denso by email and cancel asn data from CIGMA system.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doCancelAndSend(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Wshp004Form form = (Wshp004Form)actionForm;
        boolean isSuccess = true;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AsnMaintenanceDomain asnMaintenanceDomain = new AsnMaintenanceDomain();
        List<ApplicationMessageDomain> applicationMessageList 
            = new ArrayList<ApplicationMessageDomain>();
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setUserLogin(super.setUserLogin());
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        asnMaintenanceDomain.setLocale(locale);
        try{
            if(!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WSHP004_BCANCELASNSEND)){
                this.asnMaintenanceFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP004_BCANCELASNSEND, locale);
            }
            
            if(!StringUtil.checkNullOrEmpty(form.getAsnNo())){
                asnMaintenanceDomain.setAsnNo(form.getAsnNo());
            }
            if(!StringUtil.checkNullOrEmpty(form.getAsnRcvStatus())){
                asnMaintenanceDomain.setAsnRcvStatus(form.getAsnRcvStatus());
            }
            if(!StringUtil.checkNullOrEmpty(form.getInvStatus())){
                asnMaintenanceDomain.setInvoiceStatus(form.getInvStatus());
            }
            if(!StringUtil.checkNullOrEmpty(form.getLastModified())){
                asnMaintenanceDomain.setLastModified(form.getLastModified());
            }
            if(!StringUtil.checkNullOrEmpty(form.getActualEtdDate().trim())){
                asnMaintenanceDomain.setActualEtdDate(form.getActualEtdDate().trim());
            }
            if(!StringUtil.checkNullOrEmpty(form.getActualEtdTime().trim())){
                asnMaintenanceDomain.setActualEtdTime(form.getActualEtdTime().trim());
            }
            if(!StringUtil.checkNullOrEmpty(form.getPlanEtaDate().trim())){
                asnMaintenanceDomain.setPlanEtaDate(form.getPlanEtaDate().trim());
            }
            if(!StringUtil.checkNullOrEmpty(form.getPlanEtaTime().trim())){
                asnMaintenanceDomain.setPlanEtaTime(form.getPlanEtaTime().trim());
            }
            if(!StringUtil.checkNullOrEmpty(form.getNoOfPallet().trim())){
                asnMaintenanceDomain.setNoOfPallet(form.getNoOfPallet().trim());
            }
            if(!StringUtil.checkNullOrEmpty(form.getTripNo().trim())){
                asnMaintenanceDomain.setTripNo(form.getTripNo().trim());
            }
            asnMaintenanceDomain.setDscId(form.getDscId());
            asnMaintenanceDomain.setDoGroupAsnList(form.getDoGroupAsnList());
            asnMaintenanceFacadeService.transactCancelAsnAndSend(asnMaintenanceDomain);
            
            form.setDoGroupAsnList(asnMaintenanceDomain.getDoGroupAsnList());
            form.setAsnRcvStatus(asnMaintenanceDomain.getAsnRcvStatus());
            form.setLastModified(asnMaintenanceDomain.getLastModified());
            form.setAllowReviseQtyFlag(Constants.STR_ZERO);
            form.setSkipValidateLotSizeFlag(Constants.STR_ZERO);
            form.setUnmatchedLotSizeFlag(Constants.STR_ZERO);
            form.setContinueOperation(Constants.STR_ZERO);
            
            if(null != asnMaintenanceDomain.getWarningMessageList()
                && !asnMaintenanceDomain.getWarningMessageList().isEmpty()){
                applicationMessageList.addAll(asnMaintenanceDomain.getWarningMessageList());
            }
            
            //Send Email
            boolean isSuccessSendMail = this.asnMaintenanceFacadeService.searchSendingEmail(
                asnMaintenanceDomain, Constants.STR_CANCEL);
            if(isSuccessSendMail){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_SUCCESS,
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_I6_0013)));
            }else{
                applicationMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_SUCCESS,
                    MessageUtil.getApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_I6_0020,
                        SupplierPortalConstant.LBL_CANCEL)));
            }
            
        }catch(ApplicationException ex){
            isSuccess = false;
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                ex.getMessage()));
        }catch(Exception e){
            isSuccess = false;
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
            if(isSuccess){
                form.setMode(Constants.MODE_CANCEL);
            }else{
                if(Constants.STR_ONE.equals(form.getTemporaryMode())){
                    form.setMode(Constants.EMPTY_STRING);
                }else{
                    //set mode = 'save' when cancel ASN has an error. 
                    form.setMode(Constants.MODE_SAVE);
                }
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do download pdf preview.
     * <p>Download Preview ASN Report PDF file.</p>
     * <ul>
     * <li>In case click 'View PDF' button, system will download preview ASN report pdf file(Without ASN No.)</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownloadPdfPreview(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Wshp004Form form = (Wshp004Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        AsnMaintenanceDomain asnMaintenanceDomain = new AsnMaintenanceDomain();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setUserLogin(super.setUserLogin());
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        try{
            if(!userLogin.getButtonLinkNameList().contains(RoleTypeConstants.WSHP004_BVIEWPDF)){
                this.asnMaintenanceFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP004_BVIEWPDF, locale);
            }
            
            asnMaintenanceDomain.setDoGroupAsnList(form.getDoGroupAsnList());
            asnMaintenanceDomain.setLocale(locale);
            asnMaintenanceDomain.setAsnNo(form.getAsnNo());
            asnMaintenanceDomain.setTripNo(form.getTripNo());
            asnMaintenanceDomain.setActualEtdDate(form.getActualEtdDate());
            asnMaintenanceDomain.setActualEtdTime(form.getActualEtdTime());
            asnMaintenanceDomain.setPlanEtaDate(form.getPlanEtaDate());
            asnMaintenanceDomain.setPlanEtaTime(form.getPlanEtaTime());
            asnMaintenanceDomain.setNoOfPallet(form.getNoOfPallet());
            asnMaintenanceDomain.setTemporaryMode(form.getTemporaryMode());
            asnMaintenanceDomain.setSkipValidateLotSizeFlag(form.getSkipValidateLotSizeFlag());
            
            InputStream input = asnMaintenanceFacadeService.createViewAsnReport(
                asnMaintenanceDomain);
            
            if(!StringUtil.checkNullOrEmpty(input)){
                String currentDateTime = DateUtil.format(
                    Calendar.getInstance().getTime(), DateUtil.PATTERN_YYYYMMDD_HHMM_SLASH); 
                
                StringBuffer fileName = new StringBuffer();
                /* Change the filename of ASN report.
                 * fileName.append(SupplierPortalConstant.ADVANCE_SHIP_NOTICE);
                 */
                String asnTempleteFilename = asnMaintenanceFacadeService.getAsnTempleteFilename(form.getDoGroupAsnList().get(Constants.ZERO).getDCd().toString()); 
                fileName.append(asnTempleteFilename);
                /* End to change the filename of ASN report.*/
                fileName.append(currentDateTime.substring(Constants.ZERO, Constants.TEN)
                    .replaceAll(Constants.SYMBOL_SLASH, Constants.EMPTY_STRING));
                fileName.append(currentDateTime.substring(Constants.ELEVEN)
                    .replaceAll(Constants.SYMBOL_COLON, Constants.EMPTY_STRING));
                fileName.append(Constants.SYMBOL_DOT);
                fileName.append(Constants.REPORT_PDF_FORMAT);
                
                this.setHttpHeaderForFileManagement(response, request, fileName.toString());
                FileUtil.writeInputStreamToOutputStream(input, response.getOutputStream());
                return null;
            }else{
                if(null != asnMaintenanceDomain.getErrorMessageList()
                    && !asnMaintenanceDomain.getErrorMessageList().isEmpty()){
                    applicationMessageList.addAll(asnMaintenanceDomain.getErrorMessageList());
                }else{
                    form.setSkipValidateLotSizeFlag(Constants.STR_ONE);
                }
                form.setUnmatchedLotSizeFlag(asnMaintenanceDomain.getUnmatchedLotSizeFlag());
            }
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                ex.getMessage()));
        }catch(Exception e) {
            applicationMessageList = new ArrayList<ApplicationMessageDomain>();
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        form.setMode(Constants.EMPTY_STRING);
        form.setApplicationMessageList(applicationMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do download pdf.
     * <p>Download ASN Report PDF file.</p>
     * <ul>
     * <li>In case click 'Download PDF' button, system will download ASN report pdf file.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doDownloadPdf(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Wshp004Form form = (Wshp004Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
//        AsnMaintenanceDomain criteria = new AsnMaintenanceDomain();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        ServletOutputStream outputStream = null;
        try{
            if(!userLogin.getButtonLinkNameList().contains(RoleTypeConstants.WSHP004_BDOWNLOADPDF)){
                this.asnMaintenanceFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP004_BVIEWPDF, locale);
            }
            
            
//            2018-04-11
//            Don't get data from database
//            AsnMaintenanceReturnDomain asnList = form.getDoGroupAsnList().get(Constants.ZERO);
//            criteria.setFileId(asnList.getAsnDomain().getPdfFileId());
//            criteria.setLocale(locale);
//            FileManagementDomain fileMangement
//                = this.asnMaintenanceFacadeService.searchFileName(criteria);
            Timestamp updateDateTime = null;
            FileManagementDomain fileManagementDomain = asnMaintenanceFacadeService.generateAndUpdatePdfFile(
                locale, 
                form.getAsnNo(), 
                form.getDoGroupAsnList().get(Constants.ZERO).getDCd(), 
                updateDateTime, 
                form.getDscId());
            super.setHttpHeaderForFileManagement(response, request, fileManagementDomain.getFileName());
            outputStream = response.getOutputStream();
            FileUtil.writeInputStreamToOutputStream(fileManagementDomain.getFileData(), response.getOutputStream());
//            this.asnMaintenanceFacadeService.searchDownloadAsnReport(criteria,
//                response.getOutputStream());
            return null;
        }catch(ApplicationException ex){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                ex.getMessage()));
        }catch(Exception e) {
            applicationMessageList = new ArrayList<ApplicationMessageDomain>();
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        } finally {
            if (null != outputStream) {
                outputStream.close();
            }
            form.setApplicationMessageList(applicationMessageList);

            Set<String> applicationMessageSet = new LinkedHashSet<String>();
            for (String message : applicationMessageSet) {
                form.getApplicationMessageList().add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
        }
        form.setApplicationMessageList(applicationMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }

    /**
     * Do click link more.
     * <p>Download Legend Information PDF file.</p>
     * <ul>
     * <li>In case click pdf icon, system search file id and return report in PDF file format.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doClickLinkMore (ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        Wshp004Form form = (Wshp004Form)actionForm;
        OutputStream outputStream = null;
        AsnMaintenanceDomain asnMaintenanceDomain = new AsnMaintenanceDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        
        try{
            String legendFileId = ContextParams.getLegendFileId();
            asnMaintenanceDomain.setFileId(legendFileId);
            FileManagementDomain fileManagement
                = asnMaintenanceFacadeService.searchFileName(asnMaintenanceDomain);
            
            super.setHttpHeaderForFileManagement(response, request, fileManagement.getFileName());
            outputStream = response.getOutputStream();
            asnMaintenanceFacadeService.searchLegendInfo(asnMaintenanceDomain, outputStream);
            outputStream.flush();
            return null;
        }catch(ApplicationException e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, 
                e.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(null != outputStream){
                outputStream.close();
            }
        }
        form.setApplicationMessageList(errorMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Return.
     * <p>Return to previous screen.</p>
     * <ul>
     * <li>In case click 'Return' button, the system will return to parent screen.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReturn (ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception{
        Wshp004Form form = (Wshp004Form)actionForm;
        StringBuffer forward = new StringBuffer();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        try{
            if(!userLogin.getButtonLinkNameList().contains(RoleTypeConstants.WSHP004_BRETURN)){
                this.asnMaintenanceFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP004_BRETURN, locale);
            }
            
            if(SupplierPortalConstant.SCREEN_ID_WSHP001.equals(form.getScreenId())){
                forward.append(SupplierPortalConstant.URL_WSHP001_ACTION);
                forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
                forward.append(SupplierPortalConstant.METHOD_DO_INITIAL_RETURN);
                forward.append(SupplierPortalConstant.URL_PARAM_MODE);
                forward.append(Constants.STR_RETURN);
            }else if(SupplierPortalConstant.SCREEN_ID_WSHP008.equals(form.getScreenId())){
                forward.append(SupplierPortalConstant.URL_WSHP008_ACTION);
                forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
                forward.append(SupplierPortalConstant.METHOD_DO_INITIAL_WITH_CRITERIA);
//                forward.append(SupplierPortalConstant.URL_PARAM_DSCID).append(form.getDscId());
//                forward.append(SupplierPortalConstant.URL_PARAM_DOID).append(form.getDoId());
//                forward.append(SupplierPortalConstant.URL_PARAM_DCD).append(form.getDCd());
//                forward.append(SupplierPortalConstant.URL_PARAM_SPSDONO).append(form.getSpsDoNo());
//                forward.append(SupplierPortalConstant.URL_PARAM_REVISION).append(form.getRevision());
//                forward.append(SupplierPortalConstant.URL_PARAM_ROUTENO).append(form.getRouteNo());
//                forward.append(SupplierPortalConstant.URL_PARAM_DEL).append(form.getDel());
//                forward.append(SupplierPortalConstant.URL_PARAM_SCREENID)
//                    .append(form.getPrevScreenId());
            }else if(SupplierPortalConstant.SCREEN_ID_WSHP006.equals(form.getScreenId())){
                forward.append(SupplierPortalConstant.URL_WSHP006_ACTION);
                forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
                forward.append(SupplierPortalConstant.METHOD_DO_OPEN_SPECIFIED_PAGE);
                forward.append(SupplierPortalConstant.URL_PARAM_MODE);
                forward.append(Constants.STR_RETURN);
                forward.append(SupplierPortalConstant.URL_PARAM_SESSIONID);
                forward.append(form.getSessionId());
                forward.append(SupplierPortalConstant.URL_PARAM_ASNNO);
                forward.append(form.getAsnNo());
            }else if(SupplierPortalConstant.SCREEN_ID_WSHP009.equals(form.getScreenId())){
                forward.append(SupplierPortalConstant.URL_WSHP009_ACTION);
                forward.append(SupplierPortalConstant.URL_PARAM_METHOD);
                forward.append(SupplierPortalConstant.METHOD_DO_INITIAL);
                forward.append(SupplierPortalConstant.URL_PARAM_MODE);
                forward.append(Constants.MODE_INITIAL);
            }
            return new ActionForward(forward.toString(), true);
        }catch(ApplicationException ex){
            errorMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, ex.getMessage()));
        }catch(Exception e){
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }
        form.setContinueOperation(Constants.STR_ONE);
        form.setApplicationMessageList(errorMessageList);
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Allow Revise Qty.
     * <p>Allow supplier user to revise shipping qty by DENSO user.</p>
     * <ul>
     * <li>In case click 'Allow Revise' button,
     * the system will set allow revise flag to ASN detail data.</li>
     * </ul>
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doAllowReviseQty (ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception{
        Wshp004Form form = (Wshp004Form)actionForm;
        boolean isSuccess = true;
        AsnMaintenanceDomain asnMaintenanceDomain = new AsnMaintenanceDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLogin = this.getUserLoginInformation();
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        try{
            if(!userLogin.getButtonLinkNameList().contains(
                RoleTypeConstants.WSHP004_BALLOWREVISEQTY)){
                this.asnMaintenanceFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WSHP004_BALLOWREVISEQTY, locale);
            }
            
            asnMaintenanceDomain.setLocale(locale);
            asnMaintenanceDomain.setDscId(userLogin.getDscId());
            asnMaintenanceDomain.setAsnNo(form.getAsnNo());
            asnMaintenanceDomain.setTripNo(form.getTripNo());
            asnMaintenanceDomain.setActualEtdDate(form.getActualEtdDate());
            asnMaintenanceDomain.setActualEtdTime(form.getActualEtdTime());
            asnMaintenanceDomain.setPlanEtaDate(form.getPlanEtaDate());
            asnMaintenanceDomain.setPlanEtaTime(form.getPlanEtaTime());
            asnMaintenanceDomain.setNoOfPallet(form.getNoOfPallet());
            asnMaintenanceDomain.setTemporaryMode(form.getTemporaryMode());
            asnMaintenanceDomain.setDoGroupAsnList(form.getDoGroupAsnList());
            boolean resultAllowRevise =
                this.asnMaintenanceFacadeService.transactAllowReviseQty(asnMaintenanceDomain);
            form.setDoGroupAsnList(asnMaintenanceDomain.getDoGroupAsnList());
            form.setAllowReviseQtyFlag(asnMaintenanceDomain.getAllowReviseQtyFlag());
            form.setSkipValidateLotSizeFlag(Constants.STR_ZERO);
            form.setUnmatchedLotSizeFlag(Constants.STR_ZERO);
            form.setContinueOperation(Constants.STR_ZERO);
            
            if(resultAllowRevise){
                errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_SUCCESS, 
                    MessageUtil.getApplicationMessageHandledException(locale, 
                        SupplierPortalConstant.ERROR_CD_SP_I6_0021)));
            }else{
                errorMessageList.add(new ApplicationMessageDomain(
                    Constants.MESSAGE_SUCCESS,
                    MessageUtil.getApplicationMessageWithLabel(locale,
                        SupplierPortalConstant.ERROR_CD_SP_I6_0020,
                        SupplierPortalConstant.LBL_ALLOW_REVISE)));
            }
        }catch(ApplicationException ex){
            isSuccess = false;
            errorMessageList.add(
                new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, ex.getMessage()));
        }catch(Exception e){
            isSuccess = false;
            errorMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale, 
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(errorMessageList);
            if(!isSuccess){
                if(Constants.STR_ONE.equals(form.getTemporaryMode())){
                    form.setMode(Constants.EMPTY_STRING);
                }else{
                    form.setMode(Constants.MODE_SAVE);
                }
            }
        }
        return mapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * @return Map<String, String>
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap =  new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_SAVE_AND_SEND,
            SupplierPortalConstant.METHOD_DO_SAVE_AND_SEND);
        keyMethodMap.put(SupplierPortalConstant.ACTION_CANCEL_AND_SEND,
            SupplierPortalConstant.METHOD_DO_CANCEL_AND_SEND);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD_PDF_PREVIEW,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD_PDF_PREVIEW);
        keyMethodMap.put(SupplierPortalConstant.ACTION_DOWNLOAD_PDF,
            SupplierPortalConstant.METHOD_DO_DOWNLOAD_PDF);
        keyMethodMap.put(SupplierPortalConstant.ACTION_MORE,
            SupplierPortalConstant.METHOD_DO_CLICK_LINK_MORE);
        keyMethodMap.put(SupplierPortalConstant.ACTION_RETURN,
            SupplierPortalConstant.METHOD_DO_RETURN);
        keyMethodMap.put(SupplierPortalConstant.ACTION_ALLOW_REVISE,
            SupplierPortalConstant.METHOD_DO_ALLOW_REVISE);
        return keyMethodMap;
    }

    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return userLoginDomain the user login domain
     */
    private UserLoginDomain getUserLoginInformation(){
        UserLoginDomain userLogin = null;
        userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLogin;
    }
    
    /**
     * Do set form.
     * <p>Set AsnMaintenance domain to Wshp004 form.</p>
     * 
     * @param asnMaintenanceDomain the asn maintenance domain
     * @param form the wshp004 form
     */
    private void doSetForm(AsnMaintenanceDomain asnMaintenanceDomain, Wshp004Form form){
        form.setAsnRcvStatus(asnMaintenanceDomain.getAsnRcvStatus());
        form.setInvStatus(asnMaintenanceDomain.getInvoiceStatus());
        form.setLastModified(asnMaintenanceDomain.getLastModified());
        form.setActualEtdDate(asnMaintenanceDomain.getActualEtdDate());
        form.setActualEtdTime(asnMaintenanceDomain.getActualEtdTime());
        form.setPlanEtaDate(asnMaintenanceDomain.getPlanEtaDate());
        form.setPlanEtaTime(asnMaintenanceDomain.getPlanEtaTime());
        form.setNoOfPallet(asnMaintenanceDomain.getNoOfPallet());
        form.setTripNo(asnMaintenanceDomain.getTripNo());
        if(Constants.ZERO < asnMaintenanceDomain.getDoGroupAsnList().size()){
            form.setDoGroupAsnList(asnMaintenanceDomain.getDoGroupAsnList());
        }
        if(Constants.ZERO < asnMaintenanceDomain.getChangeReasonList().size()){
            form.setChgReasonList(asnMaintenanceDomain.getChangeReasonList());
        }
    }
}