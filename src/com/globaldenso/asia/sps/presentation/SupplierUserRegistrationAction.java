/*
 * ModifyDate Development company     Describe 
 * 2014/05/19 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.RoleTypeConstants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.JsonResult;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Wadm002Form;
import com.globaldenso.asia.sps.auto.business.domain.SpsMAs400VendorDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserSupplierDomain;
import com.globaldenso.asia.sps.business.service.SupplierUserRegistrationFacadeService;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserRegistrationDomain;
import com.globaldenso.asia.sps.business.domain.UserDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;

/**
 * The Class SupplierUserRegistrationAction.
 * <p>
 * Receive value from WADM002_SupplierUserRegistration.jsp for initial value and send to 
 * supplierUserRegistrationFacadeServiceImpl.
 * for search
 * </p>
 * <ul>
 * <li>Method initial : doInitial</li>
 * <li>Method initial : doInitialWithCriteria</li>
 * <li>Method create : doRegisterSupplier</li>
 * <li>Method update : doUpdateSupplier</li>
 * <li>Method return : doReturn</li>
 * <li>Method search : doSupplierCompanyChange</li>
 * </ul>
 * 
 * @author CSI
 */
public class SupplierUserRegistrationAction extends CoreAction {
    
    /** The supplier User facade service. */
    private SupplierUserRegistrationFacadeService supplierUserRegistrationFacadeService;
    
    /**
     * Instantiates a new SupplierUserRegistrationAction.
     */
    public SupplierUserRegistrationAction() {
        super();
    }
    
    /**
     * Sets the supplier User Register Facade Service.
     * 
     * @param supplierUserRegistrationFacadeService the supplier User Facade Service.
     */
    public void setSupplierUserRegistrationFacadeService(
            SupplierUserRegistrationFacadeService supplierUserRegistrationFacadeService) {
        this.supplierUserRegistrationFacadeService = supplierUserRegistrationFacadeService;
    }

    /**
     * Do initial.
     * <p>
     * Initial data when load page.
     * </p>
     * <ul>
     * <li>In case click Menu from MainMenu (Supplier User Registration) or Reset button</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitial(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception
    {
        Wadm002Form form = (Wadm002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        SupplierUserRegistrationDomain supplierUserRegistrationDomain = 
            new SupplierUserRegistrationDomain();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setUserLogin(setUserLogin());
        /*set menu code list for initial menu bar*/
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Call Facade Service for initial*/
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setLocale(locale);
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        try {
            supplierUserRegistrationDomain
                = supplierUserRegistrationFacadeService.searchInitialRegister(
                    dataScopeControlDomain);
            /*Set value from domain to ActionForm*/
            form.resetForm();
            form.setVendorCdList(null);
            form.setMaxVendorCd(Constants.ZERO);
            form.setCompanySupplierList(supplierUserRegistrationDomain.getCompanySupplierList());
            form.setPlantSupplierList(supplierUserRegistrationDomain.getPlantSupplierList());
            form.setDCdList(supplierUserRegistrationDomain.getCompanyDensoList());
            form.setDepartmentList(supplierUserRegistrationDomain.getDepartmentList());
            form.setMaxDepartment(supplierUserRegistrationDomain.getDepartmentList().size());
            form.setIsDCompany(true);
            if(Constants.MODE_EDIT.equals(form.getMode())){
                form.setDscId(form.getDscId());
            }else{
                form.setMode(Constants.MODE_REGISTER);
            }
            
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WADM002_BRESET)) 
                {
                    this.supplierUserRegistrationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WADM002_BRESET, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotResetMessage(applicationException.getMessage());
            }
            
        }catch(ApplicationException applicationException){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Initial With Criteria from WADM001 pages.
     * <p> 
     * initial data by criteria for update data. 
     * </p>
     * <ul>
     * <li>In case initial screen by criteria from WADM001 pages.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitialWithCriteria(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception
    {
        Wadm002Form form = (Wadm002Form)actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String message = new String();
        SupplierUserRegistrationDomain supplierUserRegistrationDomain = 
            new SupplierUserRegistrationDomain();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setUserLogin(setUserLogin());
        /*set menu code list for initial menu bar*/
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument domain*/
        UserSupplierDetailDomain supplierUserDetailDomain = new UserSupplierDetailDomain();
        UserDomain userDomain = new UserDomain();
        userDomain.setDscId(form.getDscId());
        supplierUserDetailDomain.setUserDomain(userDomain);
        supplierUserRegistrationDomain.setUserSupplierDetailDomain(supplierUserDetailDomain);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        supplierUserRegistrationDomain.setDataScopeControlDomain(dataScopeControlDomain);
        supplierUserRegistrationDomain.setLocale(locale);
        
        /*Call Facade Service for initial Edit*/
        try {
            supplierUserRegistrationDomain = supplierUserRegistrationFacadeService
                .searchInitialEdit(supplierUserRegistrationDomain);
            /*Set value from domain to ActionForm*/
            UserSupplierDetailDomain resultDetailDomain = supplierUserRegistrationDomain
                .getUserSupplierDetailDomain();
            form.setDscId(resultDetailDomain.getSpsMUserDomain().getDscId());
            form.setDensoOwner(resultDetailDomain.getSpsMUserSupplierDomain().getDOwner());
            form.setSCd(resultDetailDomain.getSpsMUserSupplierDomain().getSCd());
            form.setSPcd(resultDetailDomain.getSpsMUserSupplierDomain().getSPcd());
            form.setFirstName(resultDetailDomain.getSpsMUserDomain().getFirstName());
            form.setMiddleName(resultDetailDomain.getSpsMUserDomain().getMiddleName());
            form.setLastName(resultDetailDomain.getSpsMUserDomain().getLastName());
            form.setDepartmentName(resultDetailDomain.getSpsMUserDomain().getDepartmentName());
            form.setEmlInfoFlag(resultDetailDomain.getSpsMUserSupplierDomain()
                .getEmlSInfoNotfoundFlag());
            form.setEmlUrgentFlag(resultDetailDomain.getSpsMUserSupplierDomain()
                .getEmlUrgentOrderFlag());
            form.setEmlAllowReviseFlag(resultDetailDomain.getSpsMUserSupplierDomain()
                .getEmlAllowReviseFlag());
            form.setEmlCancelInvoiceFlag(resultDetailDomain.getSpsMUserSupplierDomain()
                .getEmlCancelInvoiceFlag());
            form.setEmail(resultDetailDomain.getSpsMUserDomain().getEmail());
            form.setTelephone(resultDetailDomain.getSpsMUserDomain().getTelephone());
            form.setLastUpdateDatetimeUser(supplierUserRegistrationDomain.getUpdateDatetime());
            form.setLastUpdateDatetimeSupplier(
                supplierUserRegistrationDomain.getUpdateDatetimeSupplier());
            form.setMode(Constants.MODE_EDIT);
            if(null != supplierUserRegistrationDomain.getDepartmentList()){
                form.setDepartmentList(supplierUserRegistrationDomain.getDepartmentList());
                form.setMaxDepartment(supplierUserRegistrationDomain.getDepartmentList().size());
            }
            if(null != supplierUserRegistrationDomain.getCompanyDensoList()){
                form.setDCdList(supplierUserRegistrationDomain.getCompanyDensoList());
            }
            if(null != supplierUserRegistrationDomain.getCompanySupplierList()){
                form.setCompanySupplierList(
                    supplierUserRegistrationDomain.getCompanySupplierList());
            }
            form.setPlantSupplierList(supplierUserRegistrationDomain.getPlantSupplierList());
            form.setVendorCdList(supplierUserRegistrationDomain.getVendorCdList());
            form.setMaxVendorCd(supplierUserRegistrationDomain.getVendorCdList().size());
            form.setIsDCompany(supplierUserRegistrationDomain.getIsDCompany());
            if(!supplierUserRegistrationDomain.getIsDCompany()){
                message = MessageUtil.getApplicationMessage(locale,
                    SupplierPortalConstant.ERROR_CD_SP_E7_0036);
                applicationMessageList.add(
                    new ApplicationMessageDomain(Constants.MESSAGE_FAILURE, message));
            }
            try {
                if (!userLoginDomain.getButtonLinkNameList().contains(
                    RoleTypeConstants.WADM002_BRESET)) 
                {
                    this.supplierUserRegistrationFacadeService.searchRoleCanOperate(
                        RoleTypeConstants.WADM002_BRESET, locale);
                }
            } catch (ApplicationException applicationException) {
                form.setCannotResetMessage(applicationException.getMessage());
            }
        }catch(ApplicationException applicationException){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            form.setApplicationMessageList(applicationMessageList);
//            if(supplierUserRegistrationDomain.getIsDCompany()){
//                this.setPlantCombobox(form);
//            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Supplier Company Changed.
     * <p>
     * set data from the supplier company code combo box change supplier company code value.
     * </p>
     * <ul>
     * <li>In case change supplier company code value.</li>
     * </ul>
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSCd(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception
    {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        Wadm002Form form = (Wadm002Form) actionForm;
        SupplierUserRegistrationDomain result = new SupplierUserRegistrationDomain();
        List<PlantSupplierDomain> plantSupplierList = new ArrayList<PlantSupplierDomain>();
        List<SpsMAs400VendorDomain> vendorCdList = new ArrayList<SpsMAs400VendorDomain>();
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        form.setUserLogin(setUserLogin());
        
        /*set menu code list for initial menu bar*/
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        PlantSupplierDomain plantSupplierDomain = 
            plantSupplierWithScopeDomain.getPlantSupplierDomain();
        DataScopeControlDomain dataScopeControlDomain = 
            plantSupplierWithScopeDomain.getDataScopeControlDomain();
        plantSupplierDomain.setSCd(form.getSCd());
        plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierDomain);
        
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierWithScopeDomain.setLocale(locale);
        try{
            
            plantSupplierDomain.setSCd(form.getSCdCriteria());
            plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierDomain);
            
            /*Call Facade Service to get the list of supplier plant code by supplier company code*/
            result = supplierUserRegistrationFacadeService.searchSelectedCompanySupplier(
                plantSupplierWithScopeDomain);
            plantSupplierList = result.getPlantSupplierList();
            if(Constants.ONE == plantSupplierList.size()){
                PlantSupplierDomain plantSupplier = plantSupplierList.get(Constants.ZERO);
                if(Constants.UNDEFINED_FOR_SUPPLIER.equals(plantSupplier.getSPcd())){
                    plantSupplierList = new  ArrayList<PlantSupplierDomain>();
                }
            }
            vendorCdList = result.getVendorCdList();
            form.setPlantSupplierList(plantSupplierList);
            form.setVendorCdList(vendorCdList);
            
            printJson(response, new JsonResult((List<Object>)(Object)plantSupplierList,
                (List<Object>)(Object)vendorCdList), null);
        } catch (ApplicationException e) {
            printJson(response, null, e);
        } catch (Exception e) {
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * Search Supplier Company Code by selected Supplier Plant Code filtered by user's role.
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public ActionForward doSelectSPcd(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm002Form form = (Wadm002Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<CompanySupplierDomain> companyList = null;
        
        try{
            UserLoginDomain userLogin = this.setUserLoginInformation();
            RoleScreenDomain roleScreenDomain = setRoleScreen(userLogin);
            
            CompanySupplierWithScopeDomain companySupplierWithScope
                = new CompanySupplierWithScopeDomain();
            DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
            dataScopeControlDomain.setLocale(locale);
            dataScopeControlDomain.setUserType(userLogin.getUserType());
            dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
            dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
            
            companySupplierWithScope.setLocale(locale);
            companySupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
            
            if (!Constants.MISC_CODE_ALL.equals(form.getSPcd())
                && !Constants.UNDEFINED_FOR_SUPPLIER.equals(form.getSPcd())) {
                companySupplierWithScope.getCompanySupplierDomain().setSPcd(form.getSPcd());
            }
            
            companyList = this.supplierUserRegistrationFacadeService
                .searchSelectedPlantSupplier(companySupplierWithScope);
            
            printJson(response, new JsonResult((List<Object>)(Object)companyList), null);
        }catch(ApplicationException e){
            printJson(response, null, e);
        }catch(Exception e){
            printJson(response, null, e);
        }
        return null;
    }
    
    /**
     * Do Register Supplier User.
     * <p> create data with parameter from screen.
     * </p>
     * <ul>
     * <li>In case click Register button, System will register supplier user.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doRegister(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm002Form form = (Wadm002Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        SupplierUserRegistrationDomain criteria = new SupplierUserRegistrationDomain();
        SupplierUserRegistrationDomain result = new SupplierUserRegistrationDomain();
        UserSupplierDetailDomain supplierUserDetailDomain = new UserSupplierDetailDomain();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        boolean isRegisterSuccess = false;
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        String userDscId = userLoginDomain.getDscId();
        form.setUserLogin(setUserLogin());
        /*set menu code list for initial menu bar*/
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        criteria.setDataScopeControlDomain(dataScopeControlDomain);
        
        String dscId = this.emptyToNull(form.getDscId());
        String firstName = this.emptyToNull(form.getFirstName());
        String middleName = this.emptyToNull(form.getMiddleName());
        String lastName = this.emptyToNull(form.getLastName());
        String email = this.emptyToNull(form.getEmail());
        String telephone = this.emptyToNull(form.getTelephone());
        String departmentName = this.emptyToNull(form.getDepartmentName());
        
        /*Set value to argument Domain.*/
        UserDomain userDomain = new UserDomain();
        SpsMUserSupplierDomain supplierDomain = new SpsMUserSupplierDomain();
        userDomain.setDscId(dscId);
        userDomain.setFirstName(firstName);
        userDomain.setMiddleName(middleName);
        userDomain.setLastName(lastName);
        userDomain.setEmail(email);
        userDomain.setTelephone(telephone);
        userDomain.setDepartmentName(departmentName);
        userDomain.setIsActive(Constants.IS_ACTIVE);
        userDomain.setCreateUser(userDscId);
        userDomain.setUpdateUser(userDscId);
        
        supplierDomain.setDscId(dscId);
        supplierDomain.setDOwner(form.getDensoOwner());
        supplierDomain.setSCd(form.getSCd());
        supplierDomain.setSPcd(form.getSPcd());
        supplierDomain.setEmlSInfoNotfoundFlag(form.getEmlInfoFlag());
        supplierDomain.setEmlUrgentOrderFlag(form.getEmlUrgentFlag());
        supplierDomain.setEmlAllowReviseFlag(form.getEmlAllowReviseFlag());
        supplierDomain.setEmlCancelInvoiceFlag(form.getEmlCancelInvoiceFlag());
        supplierDomain.setCreateDscId(userDscId);
        supplierDomain.setLastUpdateDscId(userDscId);
        
        supplierUserDetailDomain.setUserDomain(userDomain);
        supplierUserDetailDomain.setSpsMUserSupplierDomain(supplierDomain);
        
        criteria.setDscId(dscId);
        criteria.setUserDomain(userDomain);
        criteria.setUserSupplierDetailDomain(supplierUserDetailDomain);
        try {
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM002_BREGISTER)) 
            {
                this.supplierUserRegistrationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM002_BREGISTER, locale);
            }
            
            result = supplierUserRegistrationFacadeService.transactCreateSupplierUser(criteria);
            if(null != result.getErrorMessageList()){
                applicationMessageList = result.getErrorMessageList();
            }else if(Constants.IS_ACTIVE.equals(result.getIsActive())){
                StringBuffer forward = new StringBuffer();
                forward.append(SupplierPortalConstant.URL_WADM004_ACTION)
                    .append(SupplierPortalConstant.URL_PARAM_METHOD)
                    .append(SupplierPortalConstant.METHOD_DO_INITIAL);
                forward.append(SupplierPortalConstant.URL_PARAM_DSCID);
                forward.append(dscId);
                forward.append(SupplierPortalConstant.URL_PARAM_SCD);
                forward.append(form.getSCd());
                forward.append(SupplierPortalConstant.URL_PARAM_SPCD);
                forward.append(form.getSPcd());
                forward.append(SupplierPortalConstant.URL_PARAM_FIRSTNAME);
                forward.append(firstName);
                forward.append(SupplierPortalConstant.URL_PARAM_MIDDLENAME);
                forward.append(StringUtil.nullToEmpty(form.getMiddleName()).toUpperCase());
                forward.append(SupplierPortalConstant.URL_PARAM_LASTNAME);
                forward.append(lastName);
                forward.append(SupplierPortalConstant.URL_PARAM_RETURN_MODE);
                forward.append(Constants.MODE_REGISTER);
                forward.append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES);
                forward.append(Constants.PAGE_SEARCH);
                forward.append(SupplierPortalConstant.URL_PARAM_COMPANY_NAME);
                forward.append(result.getUserSupplierDetailDomain().getSpsMCompanySupplierDomain()
                    .getCompanyName());
                isRegisterSuccess = true;
                return new ActionForward(forward.toString(), true);
            }
        }catch(ApplicationException applicationException){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(!isRegisterSuccess){
                this.setPlantCombobox(form);
            }
            form.setMode(Constants.MODE_REGISTER);
            form.setIsDCompany(true);
            form.setApplicationMessageList(applicationMessageList);
        }
//        try{
//            criteria = supplierUserRegistrationFacadeService.searchInitialRegister(
//                dataScopeControlDomain);
//            form.setCompanySupplierList(criteria.getCompanySupplierList());
//            form.setPlantSupplierList(criteria.getPlantSupplierList());
//            form.setDCdList(criteria.getCompanyDensoList());
//            form.setDepartmentNameList(criteria.getDepartmentNameList());
//        } catch (ApplicationException applicationException) {
//            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                applicationException.getMessage()));
//            form.setApplicationMessageList(applicationMessageList);
//        } catch (Exception e) {
//            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                MessageUtil.getApplicationMessageHandledException(locale,
//                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
//                    new String[] {e.getClass().toString(), e.getMessage()})));
//        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Register Supplier User.
     * <p> create data with parameter from screen.
     * </p>
     * <ul>
     * <li>In case click Register button, System will register supplier user.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doUpdate(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm002Form form = (Wadm002Form) actionForm;
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        SupplierUserRegistrationDomain criteria = new SupplierUserRegistrationDomain();
        UserSupplierDetailDomain supplierUserDetailDomain = new UserSupplierDetailDomain();
        SupplierUserRegistrationDomain result = new SupplierUserRegistrationDomain();
        boolean isUpdateSuccess = false;
        
        /*Get current user login detail from DENSO context*/
        UserLoginDomain userLoginDomain = setUserLoginInformation();
        String userDscId = userLoginDomain.getDscId();
        form.setUserLogin(setUserLogin());
        /*set menu code list for initial menu bar*/
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        DataScopeControlDomain dataScopeControlDomain = new DataScopeControlDomain();
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        criteria.setDataScopeControlDomain(dataScopeControlDomain);
        
        /*Set value to argument Domain.*/
        String dscId = this.emptyToNull(form.getDscId());
        String firstName = this.emptyToNull(form.getFirstName());
        String middleName = this.emptyToNull(form.getMiddleName());
        String lastName = this.emptyToNull(form.getLastName());
        String email = this.emptyToNull(form.getEmail());
        String telephone = this.emptyToNull(form.getTelephone());
        String departmentName = this.emptyToNull(form.getDepartmentName());
        
        UserDomain userDomain = new UserDomain();
        SpsMUserSupplierDomain supplierDomain = new SpsMUserSupplierDomain();
        userDomain.setDscId(dscId);
        userDomain.setFirstName(firstName);
        userDomain.setMiddleName(middleName);
        userDomain.setLastName(lastName);
        userDomain.setEmail(email);
        userDomain.setTelephone(telephone);
        userDomain.setDepartmentName(departmentName);
        userDomain.setIsActive(Constants.IS_ACTIVE);
        userDomain.setUpdateUser(userDscId);
        
        supplierDomain.setDscId(dscId);
        supplierDomain.setDOwner(form.getDensoOwner());
        supplierDomain.setSCd(form.getSCd());
        supplierDomain.setSPcd(form.getSPcd());
        supplierDomain.setEmlSInfoNotfoundFlag(form.getEmlInfoFlag());
        supplierDomain.setEmlUrgentOrderFlag(form.getEmlUrgentFlag());
        supplierDomain.setEmlAllowReviseFlag(form.getEmlAllowReviseFlag());
        supplierDomain.setEmlCancelInvoiceFlag(form.getEmlCancelInvoiceFlag());
        supplierDomain.setLastUpdateDscId(userDscId);
        
        supplierUserDetailDomain.setUserDomain(userDomain);
        supplierUserDetailDomain.setSpsMUserSupplierDomain(supplierDomain);
        
        criteria.setDscId(dscId);
        criteria.setUserDomain(userDomain);
        criteria.setUpdateDatetime(form.getLastUpdateDatetimeUser());
        criteria.setUpdateDatetimeSupplier(form.getLastUpdateDatetimeSupplier());
        criteria.setUserSupplierDetailDomain(supplierUserDetailDomain);
        
        try {
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM002_BREGISTER)) 
            {
                this.supplierUserRegistrationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM002_BREGISTER, locale);
            }
            
            result = supplierUserRegistrationFacadeService.transactUpdateSupplierUser(criteria);
            if(null != result.getErrorMessageList()){
                applicationMessageList = result.getErrorMessageList();
            }else if(Constants.IS_ACTIVE.equals(result.getIsActive())){
                StringBuffer forward = new StringBuffer();
                forward.append(SupplierPortalConstant.URL_WADM004_ACTION)
                    .append(SupplierPortalConstant.URL_PARAM_METHOD)
                    .append(SupplierPortalConstant.METHOD_DO_INITIAL);
                forward.append(SupplierPortalConstant.URL_PARAM_DSCID);
                forward.append(dscId);
                forward.append(SupplierPortalConstant.URL_PARAM_SCD);
                forward.append(form.getSCd());
                forward.append(SupplierPortalConstant.URL_PARAM_SPCD);
                forward.append(form.getSPcd());
                forward.append(SupplierPortalConstant.URL_PARAM_FIRSTNAME);
                forward.append(firstName);
                forward.append(SupplierPortalConstant.URL_PARAM_MIDDLENAME);
                forward.append(StringUtil.nullToEmpty(form.getMiddleName()).toUpperCase());
                forward.append(SupplierPortalConstant.URL_PARAM_LASTNAME);
                forward.append(lastName);
                forward.append(SupplierPortalConstant.URL_PARAM_RETURN_MODE);
                forward.append(Constants.MODE_EDIT);
                forward.append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES);
                forward.append(Constants.PAGE_SEARCH);
                forward.append(SupplierPortalConstant.URL_PARAM_COMPANY_NAME);
                forward.append(result.getUserSupplierDetailDomain().getSpsMCompanySupplierDomain()
                    .getCompanyName());
                isUpdateSuccess = true;
                return new ActionForward(forward.toString(), true);
            }
        }catch(ApplicationException applicationException){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        }catch(Exception e){
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(!isUpdateSuccess){
                this.setPlantCombobox(form);
            }
            form.setMode(Constants.MODE_EDIT);
            form.setIsDCompany(true);
            form.setApplicationMessageList(applicationMessageList);
        }
//        try{
//            result = supplierUserRegistrationFacadeService.searchInitialEdit(criteria);
//            this.setMasterForm(criteria, form);
//        }catch(ApplicationException applicationException){
//            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                applicationException.getMessage()));
//        }catch (Exception e) {
//            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
//                MessageUtil.getApplicationMessageHandledException(locale,
//                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
//                    new String[] {e.getClass().toString(), e.getMessage()})));
//        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do return to WADM001.
     * <p>Open WADM001_SupplierUserInformation</p>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doReturn(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wadm002Form form = (Wadm002Form) actionForm;
        StringBuilder forward = new StringBuilder();
        List<ApplicationMessageDomain> applicationMessageList = 
            new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        boolean isSuccess = false;
        try{
            UserLoginDomain userLoginDomain = setUserLoginInformation();
            if (!userLoginDomain.getButtonLinkNameList().contains(
                RoleTypeConstants.WADM002_BRETURN)) 
            {
                this.supplierUserRegistrationFacadeService.searchRoleCanOperate(
                    RoleTypeConstants.WADM002_BRETURN, locale);
            }
            
            forward.append(SupplierPortalConstant.URL_WADM001_ACTION)
                .append(SupplierPortalConstant.URL_PARAM_METHOD)
                .append(SupplierPortalConstant.METHOD_DO_RETURN);
            forward.append(SupplierPortalConstant.URL_PARAM_FIRSTPAGES);
            forward.append(Constants.PAGE_SEARCH);
            isSuccess = true;
            return new ActionForward(forward.toString(), true);
        } catch(ApplicationException applicationException) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                applicationException.getMessage()));
        } catch(Exception e) {
            applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                MessageUtil.getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0001,
                    new String[] {e.getClass().toString(), e.getMessage()})));
        }finally{
            if(!isSuccess){
                this.setPlantCombobox(form);
                form.setApplicationMessageList(applicationMessageList);
            }
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Gets the key method map.
     * 
     * @return Map
     */
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        map.put(SupplierPortalConstant.ACTION_RETURN,
            SupplierPortalConstant.METHOD_DO_RETURN);
        map.put(SupplierPortalConstant.ACTION_REGISTER,
            SupplierPortalConstant.METHOD_DO_REGISTER);
        map.put(SupplierPortalConstant.ACTION_UPDATE,
            SupplierPortalConstant.METHOD_DO_UPDATE);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL, 
            SupplierPortalConstant.METHOD_DO_INITIAL);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL_WITH_CRITERIA, 
            SupplierPortalConstant.METHOD_DO_INITIAL_WITH_CRITERIA);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SCD, 
            SupplierPortalConstant.METHOD_DO_DO_SELECT_SCD);
        map.put(SupplierPortalConstant.ACTION_GLOBAL_DO_SELECT_SPCD,
            SupplierPortalConstant.METHOD_DO_SELECT_SPCD);
        return map;
    }
    
    /**
     * Method to get current user login detail from DENSO context.
     * <p>Set UserLoginDomain from current user login detail.</p>
     * 
     * @return the UserLoginDomain.
     */
    private UserLoginDomain setUserLoginInformation() {
        
        UserLoginDomain userLoginDomain = (UserLoginDomain)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
        return userLoginDomain;
    }
    
    /**
     * Get roles for supplier user.
     * <p>Get roles  from role screen of user login.</p>
     * 
     * @param userLoginDomain the UserLoginDomain
     * @return the RoleScreenDomain
     */
    private RoleScreenDomain setRoleScreen(UserLoginDomain userLoginDomain) {
        List<RoleScreenDomain> roleScreenList = userLoginDomain.getRoleScreenDomainList();
        for(RoleScreenDomain roleScreen : roleScreenList){
            if(SupplierPortalConstant.SCREEN_ID_WADM002.equals(
                roleScreen.getSpsMScreenDomain().getScreenCd())){
                return roleScreen;
            }
        }
        return null;
    }
    
    /**
     * Set value inside Supplier Plant combo box.
     * @param form the Wadm002Form
     * */
    private void setPlantCombobox(Wadm002Form form)
    {
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        UserLoginDomain userLoginDomain = this.setUserLoginInformation();
        List<ApplicationMessageDomain> applicationMessageList
            = new ArrayList<ApplicationMessageDomain>();
        SupplierUserRegistrationDomain result = new SupplierUserRegistrationDomain();
        /*Get roles for supplier user*/
        RoleScreenDomain roleScreenDomain = setRoleScreen(userLoginDomain);
        
        /*Set value to argument Domain.*/
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain = 
            new PlantSupplierWithScopeDomain();
        PlantSupplierDomain plantSupplierDomain = 
            plantSupplierWithScopeDomain.getPlantSupplierDomain();
        DataScopeControlDomain dataScopeControlDomain = 
            plantSupplierWithScopeDomain.getDataScopeControlDomain();
        plantSupplierWithScopeDomain.setPlantSupplierDomain(plantSupplierDomain);
        
        dataScopeControlDomain.setUserType(userLoginDomain.getUserType());
        dataScopeControlDomain.setUserRoleDomainList(roleScreenDomain.getUserRoleDomainList());
        dataScopeControlDomain.setDensoSupplierRelationDomainList(null);
        
        plantSupplierWithScopeDomain.setDataScopeControlDomain(dataScopeControlDomain);
        plantSupplierWithScopeDomain.setLocale(locale);
        
        if (!Strings.judgeBlank(form.getSCd())){
            List<PlantSupplierDomain> plantSupplierList = null;
            List<SpsMAs400VendorDomain> vendorCdList = null;
            try{
                PlantSupplierWithScopeDomain plantSupplierWithScope
                    = new PlantSupplierWithScopeDomain();
                
                if (!Constants.MISC_CODE_ALL.equals(form.getSCd())) {
                    plantSupplierWithScope.getPlantSupplierDomain().setSCd(form.getSCd());
                }
                plantSupplierWithScope.setLocale(locale);
                plantSupplierWithScope.setDataScopeControlDomain(dataScopeControlDomain);
                result = supplierUserRegistrationFacadeService
                    .searchSelectedCompanySupplier(plantSupplierWithScope);
                plantSupplierList = result.getPlantSupplierList();
                vendorCdList = result.getVendorCdList();
                form.setPlantSupplierList(plantSupplierList);
                form.setVendorCdList(vendorCdList);
                form.setMaxVendorCd(vendorCdList.size());
            }catch(ApplicationException e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    e.getMessage()));
            }catch(Exception e){
                applicationMessageList.add(new ApplicationMessageDomain(Constants.MESSAGE_FAILURE,
                    MessageUtil.getApplicationMessageHandledException(locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0001,
                        new String[] {e.getClass().toString(), e.getMessage()})));
            }finally{
                form.getApplicationMessageList().addAll(applicationMessageList);
            }
        }
    }
    
    /**
     * If input string is empty or has only space, return null.<br />
     * If not, return trimmed string.
     * @param input - input string
     * @return trimmed input string or null
     * @
     * */
    private String emptyToNull(String input) {
        if (null == input) {
            return null;
        }
        if (Constants.EMPTY_STRING.equals(input.trim())) {
            return null;
        }
        return input.toUpperCase().trim();
    }

}