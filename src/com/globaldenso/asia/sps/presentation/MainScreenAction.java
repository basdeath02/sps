/*
 * ModifyDate Development company     Describe 
 * 2014/07/02 CSI Akat                Create
 * 2015/09/17 CSI Akat                FIX wrong dateformat
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.presentation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.ai.common.core.util.Strings;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMPlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.ApplicationMessageDomain;
import com.globaldenso.asia.sps.business.domain.MainScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserLoginDomain;
import com.globaldenso.asia.sps.business.service.MainScreenFacadeService;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SupplierPortalConstant;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.presentation.form.Wcom002Form;

/**
 * The Class MainScreenAction.
 * <p>
 * Action for Welcome screen.
 * </p>
 * <ul>
 * <li>Method initial : doInitial</li>
 * <li>Method initial : doInitialContactUs</li>
 * <li>Method initial : doInitialHelp</li>
 * <li>Method logout  : doLogout</li>
 * </ul>
 * 
 * @author CSI
 */
public class MainScreenAction extends CoreAction {

    /** The facade Main Screen Facade. */
    private MainScreenFacadeService mainScreenFacadeService;

    /** The default constructor. */
    public MainScreenAction() {

    }

    /**
     * <p>
     * Setter method for mainScreenFacadeService.
     * </p>
     * 
     * @param mainScreenFacadeService Set for mainScreenFacadeService
     */
    public void setMainScreenFacadeService(
        MainScreenFacadeService mainScreenFacadeService) {
        this.mainScreenFacadeService = mainScreenFacadeService;
    }

    /**
     * Do initial.
     * <p>Initial data when load page.</p>
     * <ul>
     * <li>Show user information.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    
    public ActionForward doInitial(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        Wcom002Form form = (Wcom002Form)actionForm;
        
        MainScreenDomain mainScreenDomain = new MainScreenDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        
        UserLoginDomain userLoginDomain = (UserLoginDomain)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
        
        if(null != locale){
            if(Constants.TH.equals(locale.getLanguage())){
                form.setLang(Constants.LANGUAGE_TH);
            }else if(Constants.JA.equals(locale.getLanguage())){
                form.setLang(Constants.LANGUAGE_JA);
            }else{
                form.setLang(Constants.LANGUAGE_EN);
            }
        }
        
        try {
            String dscId = null;
            String uidHttpHeader = request.getHeader(Constants.HTTP_HEADER_KEY_UID);
            String uidHttpParam = request.getParameter(Constants.HTTP_HEADER_KEY_UID);
            
            if(null != uidHttpHeader){
                dscId = uidHttpHeader;
            }else if (null != uidHttpParam){
                dscId = uidHttpParam;
            }else{
                dscId = form.getDscId();
            }
            
            if(StringUtil.checkNullOrEmpty(dscId) && null == userLoginDomain){
                DensoContext.get().clearGeneralArea();
                response.setHeader(Constants.HTTP_HEADER_KEY_UID, null);
                StringBuffer forward = new StringBuffer();
                forward.append(SupplierPortalConstant.URL_LOGIN);
                return new ActionForward(forward.toString());
            }
            
            if (null == userLoginDomain) {
                /** Call Façade Service MainScreenFacadeService searchInitial(). */
                userLoginDomain = mainScreenFacadeService.searchInitial(dscId, locale);
                /** Load return value from call façade service MainScreenFacadeService initial() to DENSO-Context. */
                form.setSpsMMenuDomainlist(userLoginDomain.getSpsMMenuDomainlist());
                form.setMenuListSize(userLoginDomain.getSpsMMenuDomainlist().size());
                form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
                DensoContext.get().putGeneralArea(
                    SupplierPortalConstant.SESSION_USER_LOGIN, userLoginDomain);
                form.setUserLogin(setUserLogin());
            } else {
                if(null != dscId && !userLoginDomain.getDscId().equals(dscId)){
                    /*Clear DENSO-Context*/
                    DensoContext.get().clearGeneralArea();
                    /*Remove Http header "UID"*/
                    response.setHeader(Constants.HTTP_HEADER_KEY_UID, null);
                }else{
                    dscId = userLoginDomain.getDscId();
                }
                
                userLoginDomain = mainScreenFacadeService.searchInitial(dscId, locale);
                
                form.setSpsMMenuDomainlist(userLoginDomain.getSpsMMenuDomainlist());
                form.setMenuListSize(userLoginDomain.getSpsMMenuDomainlist().size());
                form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
                DensoContext.get().putGeneralArea(
                    SupplierPortalConstant.SESSION_USER_LOGIN, userLoginDomain);
                form.setUserLogin(setUserLogin());
            }

            if (null == locale) {
                String strDefaultLocale = ContextParams.getDefaultLocale();
                locale = LocaleUtil.getLocaleFromString(strDefaultLocale);
                if (!StringUtil.checkNullOrEmpty(locale)) {
                    DensoContext.get().putGeneralArea(Globals.LOCALE_KEY, locale);
                }
            }
            
            //create authen List
            List<SpsMPlantSupplierDomain> supplierAuthenList
                = super.doCreateSupplierPlantAuthenList(
                    userLoginDomain.getDataScopeControlDomain().getDensoSupplierRelationDomainList()
                    , SupplierPortalConstant.SCREEN_ID_WCOM002);

            List<SpsMPlantDensoDomain> densoAuthenList = super.doCreateDensoPlantAuthenList(
                userLoginDomain.getDataScopeControlDomain().getDensoSupplierRelationDomainList()
                , SupplierPortalConstant.SCREEN_ID_WCOM002);
            
            /** Call Façade Service MainScreenFacadeService searchWelcomMessage(). */
            mainScreenDomain = mainScreenFacadeService.searchWelcomMessage(
                userLoginDomain, locale, supplierAuthenList, densoAuthenList);

            /** Set value of domain to ActionForm. */
            form.setUserType(userLoginDomain.getUserType());
            form.setSpsMAnnounceMessageDomainList(
                mainScreenDomain.getSpsMAnnounceMessageDomainList());
            // FIX : wrong dataformat
            form.setAnnounceMessageDomainList(mainScreenDomain.getAnnounceMessageDomainList());
            
            form.setMainScreenResultDomain(mainScreenDomain.getMainScreenResultDomain());
            
            //Check existing user role.
            mainScreenFacadeService.searchUserRole(dscId, locale);

        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            String message = MessageUtil
                .getApplicationMessageHandledException(locale,
                    SupplierPortalConstant.ERROR_CD_SP_90_0002,
                    new String[]{e.getClass().toString(), e.getMessage()});
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, message));
        } finally {
            form.getApplicationMessageList().addAll(errorMessageList);
        }
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS);
    }
    
    /**
     * Do Logout.
     * <p>
     * Action from main menu to logout the system.
     * </p>
     * <ul>
     * <li>Logout click link (main menu screen) from main menu to logout the system.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doLogout(ActionMapping actionMapping,
        ActionForm actionForm, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        
        /* IT Environment
         * 
        // Clear DENSO-Context
        DensoContext.get().clearGeneralArea();
        
        // Remove Http header "UID"
        response.setHeader(Constants.HTTP_HEADER_KEY_UID, null);
        
        // Forward to logout page in siteminder.
        //return actionMapping.findForward(Constants.ACTION_FWD_LOGOUT);
        response.sendRedirect(Constants.SPS_LOGOUT_URL);
        
        return null;
        */
        
        // Session delete, and .Cookie delete logic / セッションの破棄・Cookieの削除
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }

        // Forwarding
        return actionMapping.findForward(Constants.ACTION_FWD_LOGOUT);
    }

    /**
     * Do initial Contact Us.
     * <p>Initial data when load page.</p>
     * <ul>
     * <li>In case click menu Contact Us, Initial data when load page.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitialContactUs(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wcom002Form form = (Wcom002Form)actionForm;
        UserLoginDomain userLogin = (UserLoginDomain)DensoContext.get().getGeneralArea(
            SupplierPortalConstant.SESSION_USER_LOGIN);
        form.setMenuCodelist(userLogin.getSpsMMenuDomainlist());
        form.setUserLogin(super.setUserLogin());
        form.setUserType(userLogin.getUserType());
        return actionMapping.findForward(Constants.ACTION_FWD_SUCCESS_CONTACT_US);
    }
    
    /**
     * Do initial Help.
     * <p>Initial data when load page.</p>
     * <ul>
     * <li>In case click menu Help, Initial data when load page.</li>
     * </ul>
     * 
     * @param actionMapping the action mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doInitialHelp(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        Wcom002Form form = (Wcom002Form)actionForm;
        UserLoginDomain userLoginDomain = (UserLoginDomain)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);
        form.setUserType(userLoginDomain.getUserType());
        form.setUserLogin(super.setUserLogin());
        form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
        return actionMapping.findForward(Constants.ACTION_FWD_INITIAL_HELP);
    }
    
    /**
     * Do change locale.
     * 
     * @param mapping the mapping
     * @param actionForm the action form
     * @param request the request
     * @param response the response
     * @return the action forward
     * @throws Exception the exception
     */
    public ActionForward doChangeLocale(ActionMapping mapping, ActionForm actionForm, 
        HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String targetLocale = getParameter(request, "targetLocale");
        //HttpSession session = request.getSession();
        if(!Strings.judgeBlank(targetLocale)){
            //session.setAttribute(Globals.LOCALE_KEY, LocaleUtil.getLocaleFromString(targetLocale));
            DensoContext.get().putGeneralArea(
                Globals.LOCALE_KEY, LocaleUtil.getLocaleFromString(targetLocale));
        }else{
            //session.setAttribute(Globals.LOCALE_KEY, LocaleUtil.getLocaleFromString(
            //    ContextParams.getDefaultLocale()));
            DensoContext.get().putGeneralArea(Globals.LOCALE_KEY, LocaleUtil.getLocaleFromString(
                ContextParams.getDefaultLocale()));
        }
        
        // Set Main Screen.
        
        Wcom002Form form = (Wcom002Form)actionForm;
        
        MainScreenDomain mainScreenDomain = new MainScreenDomain();
        List<ApplicationMessageDomain> errorMessageList = new ArrayList<ApplicationMessageDomain>();
        Locale locale = (Locale)DensoContext.get().getGeneralArea(
            Globals.LOCALE_KEY);
        
        UserLoginDomain userLoginDomain = (UserLoginDomain)DensoContext.get()
            .getGeneralArea(SupplierPortalConstant.SESSION_USER_LOGIN);

        try {
            
            String dscId = null;
            String uidHttpHeader = request.getHeader(Constants.HTTP_HEADER_KEY_UID);
            String uidHttpParam = request.getParameter(Constants.HTTP_HEADER_KEY_UID);
            
            if(null != uidHttpHeader){
                dscId = uidHttpHeader;
            } else if (null != uidHttpParam) {
                dscId = uidHttpParam;
            } else {
                dscId = form.getDscId();
            }
            
            if(StringUtil.checkNullOrEmpty(dscId) && null == userLoginDomain){
                DensoContext.get().clearGeneralArea();
                response.setHeader(Constants.HTTP_HEADER_KEY_UID, null);
                StringBuffer forward = new StringBuffer();
                forward.append(SupplierPortalConstant.URL_LOGIN);
                return new ActionForward(forward.toString());
            }
            
            if (null == userLoginDomain) {
                
                /** Call Façade Service MainScreenFacadeService searchInitial(). */
                userLoginDomain = mainScreenFacadeService.searchInitial(dscId, locale);
                /** Load return value from call façade service MainScreenFacadeService initial() to DENSO-Context. */
                form.setSpsMMenuDomainlist(userLoginDomain.getSpsMMenuDomainlist());
                form.setMenuListSize(userLoginDomain.getSpsMMenuDomainlist().size());
                form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
                DensoContext.get().putGeneralArea(
                    SupplierPortalConstant.SESSION_USER_LOGIN, userLoginDomain);
                form.setUserLogin(setUserLogin());
            } else {
                if(null != dscId && !userLoginDomain.getDscId().equals(dscId)){
                    /*Clear DENSO-Context*/
                    DensoContext.get().clearGeneralArea();
                    /*Remove Http header "UID"*/
                    response.setHeader(Constants.HTTP_HEADER_KEY_UID, null);
                }else{
                    dscId = userLoginDomain.getDscId();
                }
                userLoginDomain = mainScreenFacadeService.searchInitial(dscId, locale);
                form.setSpsMMenuDomainlist(userLoginDomain.getSpsMMenuDomainlist());
                form.setMenuListSize(userLoginDomain.getSpsMMenuDomainlist().size());
                form.setMenuCodelist(userLoginDomain.getSpsMMenuDomainlist());
                DensoContext.get().putGeneralArea(
                    SupplierPortalConstant.SESSION_USER_LOGIN, userLoginDomain);
                form.setUserLogin(setUserLogin());
            }
            
            if (null == locale) {
                String strDefaultLocale = ContextParams.getDefaultLocale();
                locale = LocaleUtil.getLocaleFromString(strDefaultLocale);
                if (!StringUtil.checkNullOrEmpty(locale)) {
                    DensoContext.get().putGeneralArea(Globals.LOCALE_KEY, locale);
                }
            }
            
            //create authen List
            List<SpsMPlantSupplierDomain> supplierAuthenList
                = super.doCreateSupplierPlantAuthenList(
                    userLoginDomain.getDataScopeControlDomain().getDensoSupplierRelationDomainList()
                    , SupplierPortalConstant.SCREEN_ID_WCOM002);

            List<SpsMPlantDensoDomain> densoAuthenList = super.doCreateDensoPlantAuthenList(
                userLoginDomain.getDataScopeControlDomain().getDensoSupplierRelationDomainList()
                , SupplierPortalConstant.SCREEN_ID_WCOM002);
                
            /** Call Façade Service MainScreenFacadeService searchWelcomMessage(). */
            mainScreenDomain = mainScreenFacadeService.searchWelcomMessage(
                userLoginDomain, locale, supplierAuthenList, densoAuthenList);
            
            /** Set value of domain to ActionForm. */
            form.setUserType(userLoginDomain.getUserType());
            form.setSpsMAnnounceMessageDomainList(
                mainScreenDomain.getSpsMAnnounceMessageDomainList());
            form.setMainScreenResultDomain(mainScreenDomain.getMainScreenResultDomain());
            
        } catch (ApplicationException e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, e.getMessage()));
        } catch (Exception e) {
            errorMessageList.add(new ApplicationMessageDomain(
                Constants.MESSAGE_FAILURE, MessageUtil
                    .getApplicationMessageHandledException(
                        locale,
                        SupplierPortalConstant.ERROR_CD_SP_90_0002,
                        new String[] {SupplierPortalConstant.LBL_ERROR, e.getMessage()})));
        } finally {
            form.getApplicationMessageList().addAll(errorMessageList);
        }
        
        return mapping.findForward(Constants.ACTION_FWD_INITIAL);
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.apache.struts.actions.LookupDispatchAction#getKeyMethodMap()
     */
    @Override
    protected Map<String, String> getKeyMethodMap() {
        Map<String, String> keyMethodMap = new HashMap<String, String>();
        keyMethodMap.put(SupplierPortalConstant.ACTION_RESET,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL,
            SupplierPortalConstant.METHOD_DO_INITIAL);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_LINK_DO_LOGOUT,
            SupplierPortalConstant.METHOD_DO_LOGOUT);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_LINK_INITIAL_CONTACT_US,
            SupplierPortalConstant.METHOD_DO_INITIAL_CONTACT_US);
        keyMethodMap.put(SupplierPortalConstant.ACTION_GLOBAL_DO_INITIAL_HELP,
            SupplierPortalConstant.METHOD_DO_INITIAL_HELP);
        keyMethodMap.put(SupplierPortalConstant.ACTION_CHANGE_LOCALE,
            SupplierPortalConstant.METHOD_DO_CHANGE_LOCALE);
        return keyMethodMap;
    }
}
