/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.tag;

import java.util.Locale;

import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;

/**
 * The Class CoreTag.
 * This class for provide the get locale method, page context and tag for inherit class.
 * 
 * @author CSI
 * @version 1.00
 */
public abstract class CoreTag implements Tag {

    /** The log. */
    private static final Log LOG = LogFactory.getLog(CoreTag.class);
    
    /** The page context. */
    protected PageContext pageContext;
    
    /** The parent. */
    protected Tag parent;
    
    /**
     * Gets the page context.
     * 
     * @return the page context
     */
    public PageContext getPageContext(){
        return pageContext;
    }
    
    /**
     * Sets the page context.
     * 
     * @param pc the new page context
     */
    public void setPageContext(PageContext pc){
        pageContext = pc;
    }
    
    /**
     * getParent.
     * 
     * @return the tag
     */
    public Tag getParent() {
        return this.parent;
    }

    /**
     * setParent.
     * @param tag the tag
     */
    public void setParent(Tag tag) {
        parent = tag;
    }
    
    /**
     * Get Locale from user information in session, if user locale is not found then get from cookie.
     * @return Locale.
     */
    protected Locale getLocale() {
        //Locale locale = (Locale)pageContext.getSession().getAttribute(Globals.LOCALE_KEY);
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        if(null == locale){
            String strDefaultLocale = ContextParams.getDefaultLocale();
            locale = LocaleUtil.getLocaleFromString(strDefaultLocale);
            if (!StringUtil.checkNullOrEmpty(locale)) {
                LOG.debug("System default locale is " + locale);
                // Set default locale to org.apache.struts.Globals.LOCALE_KEY
                pageContext.getSession().setAttribute(Globals.LOCALE_KEY, locale);
            }
        }
        return locale;
    }
}