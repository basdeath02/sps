/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.tag;

import java.util.Locale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.taglib.TagUtils;

import com.globaldenso.ai.common.core.context.DensoContext;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.MessageUtil;

/**
 * MsgTag is used for display message from a specified KEY and ARGs. For
 * example: &lt;eps:msg key="MyKey" arg0="ABC"&gt; is called. It will perform
 * getting template message from perperties file with key "MyKey" then placing
 * parameter at index = 0 with value "ABC".
 * 
 * @author CSI.
 * @version 1.00
 */
public class MsgTag extends TagSupport {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4120418602915789754L;

    /** The log. */
    private static final Log LOG = LogFactory.getLog(MsgTag.class);

    /** The arg0. */
    private String arg0;
    
    /** The arg1. */
    private String arg1;
    
    /** The arg2. */
    private String arg2;
    
    /** The arg3. */
    private String arg3;
    
    /** The arg4. */
    private String arg4;
    
    /** The name. */
    private String name;

    /**
     * Instantiates a new msg tag.
     */
    public MsgTag() {
        arg0 = null;
        arg1 = null;
        arg2 = null;
        arg3 = null;
        arg4 = null;
        name = null;

    }

    /**
     * Gets the arg0.
     * 
     * @return the arg0
     */
    public String getArg0() {
        return arg0;
    }

    /**
     * Sets the arg0.
     * 
     * @param arg0 the new arg0
     */
    public void setArg0(String arg0) {
        this.arg0 = arg0;
    }

    /**
     * Gets the arg1.
     * 
     * @return the arg1
     */
    public String getArg1() {
        return arg1;
    }

    /**
     * Sets the arg1.
     * 
     * @param arg1 the new arg1
     */
    public void setArg1(String arg1) {
        this.arg1 = arg1;
    }

    /**
     * Gets the arg2.
     * 
     * @return the arg2
     */
    public String getArg2() {
        return arg2;
    }

    /**
     * Sets the arg2.
     * 
     * @param arg2 the new arg2
     */
    public void setArg2(String arg2) {
        this.arg2 = arg2;
    }

    /**
     * Gets the arg3.
     * 
     * @return the arg3
     */
    public String getArg3() {
        return arg3;
    }

    /**
     * Sets the arg3.
     * 
     * @param arg3 the new arg3
     */
    public void setArg3(String arg3) {
        this.arg3 = arg3;
    }

    /**
     * Gets the arg4.
     * 
     * @return the arg4
     */
    public String getArg4() {
        return arg4;
    }

    /**
     * Sets the arg4.
     * 
     * @param arg4 the new arg4
     */
    public void setArg4(String arg4) {
        this.arg4 = arg4;
    }

    /**
     * Gets the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * 
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Do start tag.
     * 
     * @return the int
     * @throws JspException the jsp exception
     */
    public int doStartTag() throws JspException {
        String bundle = ContextParams.getBaseDirMsg();
        if (bundle == null) {
            JspException e = 
                new JspException("Not found base directory of message properties file.");
            TagUtils.getInstance().saveException(pageContext, e);
            throw e;
        }

        String[] args = {arg0, arg1, arg2, arg3, arg4};
        //Locale locale = (Locale) pageContext.getSession().getAttribute(Globals.LOCALE_KEY);
        Locale locale = (Locale)DensoContext.get().getGeneralArea(Globals.LOCALE_KEY);
        String message = null;
        try {
            message = MessageUtil.getMessage(ContextParams.getBaseDirMsg(), locale, name, args);
            LOG.debug("Message: " + message);
        } catch (Exception e1) {
            JspException e = new JspException("Cannot get message from " 
                + ContextParams.getBaseDirMsg());
            TagUtils.getInstance().saveException(pageContext, e);
            throw e;
        }
        if (message == null) {
            JspException e = 
                new JspException("Not found base directory of message properties file.");
            TagUtils.getInstance().saveException(pageContext, e);
            throw e;
        } else {
            TagUtils.getInstance().write(pageContext, message);
            return SKIP_BODY;
        }
    }

    /**
     * release
     */
    public void release() {
        super.release();
        arg0 = null;
        arg1 = null;
        arg2 = null;
        arg3 = null;
        arg4 = null;
        name = null;
    }
}
