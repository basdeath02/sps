/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.tag;

import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.Tag;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.globaldenso.asia.sps.presentation.form.CoreActionForm;

/**
 * This class is a custom tag for displaying table to show the result of
 * searching process. It will check the display from attribute named
 * "totalCount" (clicking search button trigger), if totalCount in ActionForm
 * has null value, the table will not be displayed, if the result of table is 0,
 * the table shows "No item found" otherwise result of searching will be displayed.
 * 
 * @author CSI
 * @version 1.00
 */
public class DisplayTableTag extends BodyTagSupport implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6685985111492843302L;

    /** The log. */
    private static final Log LOG = LogFactory.getLog(DisplayTableTag.class);
    
    /** The pc. */
    private transient PageContext pc = null;
    
    /** The parent. */
    private Tag parent = null;
    
    /** The id. */
    private String id;
    
    /** The name. */
    private String name;
    
    /** The width. */
    private Integer width;
    
    /** The height. */
    private Integer height;

    /**
     * Instantiates a new display table tag.
     */
    public DisplayTableTag(){
        super();
    }
    
    /**
     * Sets the page context.
     * 
     * @param p the new page context
     */
    @Override
    public void setPageContext(PageContext p) {
        pc = p;
    }

    /**
     * getParent.
     * @param t the tag
     */
    public void setParent(Tag t) {
        parent = t;
    }

    /**
     * getParent.
     * 
     * @return the tag
     */
    public Tag getParent() {
        return parent;
    }

    /**
     * getStyleId.
     * 
     * @return the styleId
     */
    public String getId() {
        return id;
    }

    /**
     * Set ID of table.
     * 
     * @param id the new id
     */
    @Override
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Set name of bean to find "totalCount".
     * 
     * @param name
     *            - name of bean in request or session scope.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the width.
     * 
     * @return the width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * Set width of table.
     * 
     * @param width
     *            - width of table.
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * Gets the height.
     * 
     * @return the height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * Set height of table.
     * 
     * @param height
     *            - height of table.
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * doStartTag performs checking "totalCount" in Struts ActionForm then
     * render the result table based on "totalCount" value.
     * 
     * @return the int
     * @throws JspException the jsp exception
     */
    @Override
    public int doStartTag() throws JspException {
        try {
            // Get totalCount from Struts ActionForm
            HttpServletRequest req = (HttpServletRequest) pc.getRequest();
            CoreActionForm form = (CoreActionForm) req.getAttribute(getName());
            StringBuffer strTag = new StringBuffer();
            JspWriter writer = pc.getOut();

            if (null == form) {
                // Not found bean in request scope then search bean in scope
                // session.
                form = (CoreActionForm) req.getSession().getAttribute(getName());
                if (form == null) {
                    // Not found bean in scope request and session then return.
                    return SKIP_BODY;
                }
            }
            Integer totalCount = form.getTotalCount();
            if (null == totalCount) {
                // User clicked search button and no data found.
                // Display "No data found." table.
                strTag.append("<div style=\"width:")
                      .append(width).append("px;height:").append(height)
                      .append("px;\">")
                      .append("</div>");
                writer.write(strTag.toString());
                return SKIP_BODY;
            } else {
                return EVAL_BODY_BUFFERED;
            }
        } catch (Exception e) {
            LOG.error(e.getClass() + ": " + e.getMessage());
        }
        return SKIP_BODY;
    }

    /**
     * doAfterBody.
     * @return int
     * @throws JspException The JspException
     */
    public int doAfterBody() throws JspException {
        try {
            BodyContent bodycontent = getBodyContent();
            String body = bodycontent.getString();
            JspWriter out = bodycontent.getEnclosingWriter();
            out.write(body);
        } catch (Exception e) {
            LOG.error(e.getClass() + ": " + e.getMessage());
        }
        return EVAL_PAGE;
    }

    /**
     * doEndTag.
     * @return int
     * @throws JspException The JspException
     */
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    /**
     * release.
     */
    public void release() {
        pc = null;
        parent = null;
        id = null;
        name = null;
        width = null;
        height = null;
    }
}