/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.tag;

import java.io.Serializable;
import java.util.Locale;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.globaldenso.asia.sps.common.utils.MessageUtil;

/**
 * We use this tag to display label message.
 * LabelTag performs getting message from properties file that start with "label." 
 * <br/>For example: in JSP it has &lt;eps:label name="COMPANY"&gt; 
 * This class will search for value of key "label.COMPANY" then print out found value.
 * 
 * @author CSI
 * @version 1.00
 */
public class LabelTag extends CoreTag implements Tag, Serializable {
    
    /** serialVersionUID. */
    private static final long serialVersionUID = 4109076835520540562L;

    /** The log. */
    private static final Log LOG = LogFactory.getLog(LabelTag.class);

    /** The name. */
    private String name = null;

    /**
     * Instantiates a new label tag.
     */
    public LabelTag(){
        super();
    }

    /**
     * Gets the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * 
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Do start tag.
     * 
     * @return the int
     * @throws JspException the jsp exception
     */
    public int doStartTag() throws JspException {
        try {
            Locale locale = getLocale();
            String output = MessageUtil.getLabel(locale, getName());
            pageContext.getOut().write(output);
        } catch (Exception e) {
            LOG.error(e.getClass() + ": " + e.getMessage());
        }
        return SKIP_BODY;
    }

    /**
     * doEndTag.
     * 
     * @return the eval page
     * @throws JspException the jsp exception
     */
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    /**
     * release
     */
    public void release() {
        pageContext = null;
        parent = null;
        name = null;
    }
}