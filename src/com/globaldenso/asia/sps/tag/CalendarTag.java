/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.taglib.html.BaseFieldTag;

import com.globaldenso.asia.sps.common.constant.Constants;

/**
 * When <eps:calendar> is called, it will perform generating text box with calendar and bin (to delete) image.
 * Developer must specify text box properties such as ID , name and value. 
 * 
 * @author CSI.
 * @version 1.00
 */
public class CalendarTag extends BaseFieldTag {
    
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1227552930779989629L;
    
    /** The Constant LOG. */
    private static final Log LOG = LogFactory.getLog(CalendarTag.class);
    
    
    /** The parent. */
    private Tag parent;
    
    /** The id. */
    private String id;
    
    /** The name. */
    private String name;
    
    /** The value. */
    private String value;
    
    /** The onchange. */
    private String onchange;
    
    /** The show image. */
    private String showImage;
    
    /** The get script. */
    private String notGetScript;

    /**
    * Instantiates a new calendar tag.
    */
    public CalendarTag(){
        name = "org.apache.struts.taglib.html.BEAN";
        parent = null;
        id = null;
        value = null;
    }

    /**
     * Do start tag.
     * 
     * @return the int
     * @throws JspException the jsp exception
     */
    public int doStartTag() throws JspException {
        try {

            String dateFormat = "%Y/%m/%d";
            String calendarId = "calendar_" + id;
            String recycleId = "recycle_" + id;
            StringBuffer strTag = new StringBuffer();
            strTag.append("<input type=\"text\" id=\"")
                  .append(id)
                  .append("\" name=\"")
                  .append(name)
                  //.append("\" class=\"readonly\" readonly=\"readonly\" onchange=\"" + onchange 
                  //+ "\" maxlength=\"10\" size=\"8\" value=\"");
                  .append("\" onchange=\"" + onchange + "\" maxlength=\"10\" size=\"8\" value=\"");
            if(null == value){
                strTag.append(Constants.EMPTY_STRING);
            } else {
                strTag.append(value);
            }
            strTag.append("\"").append(prepareEvent())
                .append("\" ")
                  
                // If user input 8 character, system will insert '/' for correct format.
                .append(" onblur=\"insertSlashToDate('").append(id).append("');return false;\" ")

                // If user click to edit, remove '/'.
                .append(" onfocus=\"removeSlashFromDate('").append(id).append("');return false;\" ")

                .append(" style=\"font-size:13px\">")
                .append("&nbsp;");
            
            if(null == showImage || Constants.EMPTY_STRING.equals(showImage)){
                strTag.append("<img src=\"images/icons/calendar.gif\"")
                      .append("alt=\"Calendar\" id=\"")
                      .append(calendarId)
                      .append("\" width=\"13\" height=\"13\" border=\"0\" align=\"bottom\" ")
                      .append("onmouseover=\"style.cursor='hand'\"/>");
                
                strTag.append("&nbsp;<img id=\"" + recycleId + "\" ")
                      .append("src=\"images/icons/delete.gif\" alt=\"Delete\"")
                      .append("width=\"13\" height=\"13\" border=\"0\" align=\"bottom\"")
                      .append("onmouseover=\"style.cursor='hand'\" ")
                      .append("onClick=\"document.getElementById('")
                      .append(id).append("').value = '';\">");
                if(null == notGetScript){
                    strTag.append("<script type=\"text/javascript\">")
                          .append("Calendar.setup({inputField:'")
                          .append(id)
                          .append("',weekNumbers:false,button:'")
                          .append(calendarId)
                          .append("',ifFormat:'").append(dateFormat).append("'});")
                          .append("</script>");
                }
            }
            
            pageContext.getOut().write(strTag.toString());
            strTag = null;

        } catch (Exception e) {
            LOG.error(e.getClass() + ": " + e.getMessage());
        }
        return SKIP_BODY;
    }

    /**
    * Prepare event.
    * 
    * @return the string
    */
    public String prepareEvent(){
        StringBuffer results = new StringBuffer();
        results.append(prepareEventHandlers());
        return results.toString();
    }

    /**
     * doEndTag.
     * 
     * @return the eval page
     * @throws JspException the jsp exception
     */
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    /**
     * getParent.
     * 
     * @return the tag
     */
    public Tag getParent() {
        return parent;
    }

    /**
     * setParent.
     * @param tag the tag
     */
    public void setParent(Tag tag) {
        this.parent = tag;
    }

    /**
     * release.
     */
    public void release() {
        super.release();
        pageContext = null;
        parent = null;
        id = null;
        name = null;
        value = null;
    }

    /**
     * getStyleId.
     * 
     * @return the styleId
     */
    public String getId() {
        return this.id;
    }

    /**
     * Set ID of table.
     * 
     * @param id the new id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the name.
     * 
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the name.
     * 
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the value.
     * 
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     * 
     * @param value the value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the onchange.
     * 
     * @return the onchange
     */
    public String getOnchange() {
        return onchange;
    }

    /**
     * Sets the onchange.
     * 
     * @param onchange the onchange
     */
    public void setOnchange(String onchange) {
        this.onchange = onchange;
    }

    /**
     * Gets the notGetScript.
     * 
     * @return the notGetScript
     */
    public String getNotGetScript() {
        return notGetScript;
    }

    /**
     * Sets the notGetScript.
     * 
     * @param notGetScript the notGetScript
     */
    public void setNotGetScript(String notGetScript) {
        this.notGetScript = notGetScript;
    }

    /**
     * Gets the show image.
     * 
     * @return the show image
     */
    public String getShowImage() {
        return showImage;
    }

    /**
     * Sets the show image.
     * 
     * @param showImage the new show image
     */
    public void setShowImage(String showImage) {
        this.showImage = showImage;
    }
}