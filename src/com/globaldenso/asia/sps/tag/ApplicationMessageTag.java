/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.tag;

import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.fw.ApplicationMessage;
import com.globaldenso.asia.sps.common.fw.ApplicationMessages;
import com.globaldenso.asia.sps.common.utils.MessageUtil;

/**
 * ApplicationMessageTag is used for displaying the message that has been set
 * into request scope from server side. Once this tag is called, it performs get
 * message from request scope by using the same set key from server side
 * (Constants.RESULT_MESSAGE). It will check if obtained value of the constant
 * key is instance of String or ApplicationMesasges. If message from server is
 * set as String, this tag will print out the message. If message from server is
 * set as ApplicationMessages, this tag will get list of ApplicationMessage then
 * get key and parameter index value to generate complete message. In case
 * inside ApplicationMessages contain more than 1 ApplicationMessage, this tag
 * generating message from all items in list and complete message will be
 * append.
 * 
 * @author CSI.
 * @version 1.00
 */
public class ApplicationMessageTag extends CoreTag implements Tag {
    
    /** The log. */
    private static final Log LOG = LogFactory.getLog(ApplicationMessageTag.class);
    
    /** The width. */
    private String width;

    /**
     * Instantiates a new application message tag.
     */
    public ApplicationMessageTag(){
        super();
        width = null;
    }
    
    /**
     * doEndTag.
     * 
     * @return integer integer
     * @throws JspException the jsp exception
     * @exception JspException JspException
     */
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    /**
     * doStartTag.
     * 
     * @return integer integer
     * @throws JspException the jsp exception
     * @exception JspException JspException
     */
    public int doStartTag() throws JspException {
        try {

            Object msgs = pageContext.getRequest().getAttribute(Constants.RESULT_MESSAGE);
            StringBuffer out = new StringBuffer();
            if (null == msgs) {
                LOG.debug("Application result message is null or empty.");
                return EVAL_PAGE;
            } else {
                Locale locale = getLocale();
                if (msgs instanceof String) {
                    out.append("<tr id=\"errorMessage\" height=\"20px\">")
                        .append("<td>")
                        .append("<div class=\"ui-state-error\">")
                        .append("<ul>")
                        .append((String) msgs)
                        .append("</ul>")
                        .append("</div>")
                        .append("</td>")
                        .append("</tr>");
                    pageContext.getOut().write(out.toString());
                } else if (msgs instanceof ApplicationMessages) {
                    ApplicationMessages appMsgs = (ApplicationMessages) msgs;
                    List<ApplicationMessage> listMsg = appMsgs.getApplicationMessages();
                    if(Constants.ZERO < listMsg.size()){
                        String typeOfMsg = Constants.EMPTY_STRING;
                        if(appMsgs.getMessageType().equals(ApplicationMessages.MESSAGE_SUCCESS)){
                            typeOfMsg = "success";
                        }else{
                            typeOfMsg = "error";
                        }
                        out.append("<div id=\"errorMessage\"");
                        if(null != width){
                            out.append(" style=\"width: " + width + "px;\">")
                                .append("<tr width=\"" + width + "\" height=\"20px\">");
                        }else{
                            out.append(" style=\"width: 788px;\">")
                                .append("<tr width=\"788\" height=\"20px\">");
                        }
                        out.append("<td>")
                            .append("<div class=\"ui-state-" + typeOfMsg + "\">")
                            .append("<ul>");
                        for (ApplicationMessage msg : listMsg) {
                            /** Append message to display. */
                            try {
                                out.append(MessageUtil.getApplicationMessage(locale, msg.getKey(),
                                    (String[])msg.getParams()));
                                if(1 < listMsg.size()){
                                    out.append("<br/>");
                                }
                            } catch (MissingResourceException e) {
                                out.append(msg.getKey());
                            }
                        }
                        out.append("</ul>")
                            .append("</div>")
                            .append("</td>")
                            .append("</tr>")
                            .append("</div>");
                    }
                    pageContext.getOut().write(out.toString());
                }
            }
        } catch (Exception e) {
            LOG.error(e.getClass() + ": " + e.getMessage());
        }
        return SKIP_BODY;
    }

    /**
     * release.
     */
    public void release() {
        pageContext = null;
        parent = null;
        width = null;
    }

    /**
     * Gets the width.
     * 
     * @return the width
     */
    public String getWidth() {
        return width;
    }

    /**
     * Sets the width.
     * 
     * @param width the new width
     */
    public void setWidth(String width) {
        this.width = width;
    }
}
