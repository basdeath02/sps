/*
 * ModifyDate Developmentcompany Describe 
 * 2014/03/03 CSI Akat           Create
 * 2016/03/31 CSI Akat           [IN063]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.listener;
 
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.fw.ContextParams;
import com.globaldenso.asia.sps.common.utils.LocaleUtil;
import com.globaldenso.asia.sps.common.utils.MessageUtil;
import com.globaldenso.asia.sps.common.utils.StringUtil;
import com.globaldenso.asia.sps.conf.ApplicationConfig;

/**
 * This class performs loading all context parameters from web.xml and set key
 * and value to ContextParam.class in core package.
 * @author CSI  
 */
public class LoadingContextListener implements ServletContextListener {

    /** The log. */
    private static final Log LOG = LogFactory.getLog(LoadingContextListener.class);
    
    /**
     * Instantiates a new loading context listener.
     */
    public LoadingContextListener(){
        super();
    }
    
    /**
     * contextInitialized is used when application is loaded. It performs
     * loading context parameter named "DEFAULT_LOCALE" then set to
     * org.apache.struts.Globals.LOCALE_KEY.
     * 
     * @param event the event
     */
    public void contextInitialized(ServletContextEvent event) {
        try {
            LOG.info("Get configuration from web.xml");
            ServletContext servletContext = event.getServletContext();
            // This locale is for creating time stamp object in system cannot be changed.  
            LOG.info("Set default locale to en_US that will be used throught create date/update "
                + "date conversion process.");
            Locale.setDefault(new Locale("en", "US"));
            LOG.debug("Initial parameters...");
            String baseDirAppConf = servletContext.getInitParameter("BASE_DIR_APP_CONF");
            ContextParams.setBaseDirApplicationConf(baseDirAppConf);
            LOG.debug("Base directory for ApplicationConf.properties file: " + baseDirAppConf);
   
            String strDefaultLocale = servletContext.getInitParameter(
                ApplicationConfig.KEY.DEFAULT_LOCALE.name());
            ContextParams.setDefaultLocale(strDefaultLocale);
            LOG.debug("Default locale for web application: " + strDefaultLocale);
            
            String baseDirGui = MessageUtil.getMessage(baseDirAppConf, 
                ApplicationConfig.KEY.BASE_DIR_GUI.name());
            ContextParams.setBaseDirGui(baseDirGui);
            LOG.debug("Base directory for label message: " + baseDirGui);
            
            String baseDirReportGui = MessageUtil.getMessage(baseDirAppConf, 
                ApplicationConfig.KEY.BASE_DIR_REPORT_GUI.name());
            ContextParams.setBaseDirReportGui(baseDirReportGui);
            LOG.debug("Base directory Report for label message: " + baseDirReportGui);
            
            String baseEmail = MessageUtil.getMessage(baseDirAppConf, 
                ApplicationConfig.KEY.BASE_DIR_EMAIL.name());
            ContextParams.setBaseEmail(baseEmail);
            LOG.debug("Base directory email for label message: " + baseEmail);

            String baseDirMsg = MessageUtil.getMessage(baseDirAppConf, 
                ApplicationConfig.KEY.BASE_DIR_MSG.name());
            ContextParams.setBaseDirMsg(baseDirMsg);
            LOG.debug("Base directory for message to display transaction execution result: "
                + baseDirMsg);

            LOG.debug("Loading fonts from GUI files...");
            String webRootContext = servletContext.getRealPath(Constants.EMPTY_STRING);
            LOG.debug("Web Context: " + webRootContext);
            String srcPath = servletContext.getRealPath("/WEB-INF/classes/");
            LOG.debug("Classes path: " + srcPath);
            String guiMessageFullPath = getBaseDirRealPath(srcPath, ContextParams.getBaseDirGui());
            LOG.debug("Gui file full path: " + guiMessageFullPath);
            String guiDir = guiMessageFullPath.substring(0, guiMessageFullPath.lastIndexOf("\\"));
            LOG.debug("GUI direcotry: " + guiDir);
            String guiMsgFileName = getFileNameFromBaseDirConf(ContextParams.getBaseDirGui());
            LOG.debug("GUI file name: " + guiMsgFileName);
            
            // ========== Manage Default Locale Process =========
            Locale defaultLocale = LocaleUtil.getLocaleFromString(strDefaultLocale);
            if (!StringUtil.checkNullOrEmpty(defaultLocale)) {
                LOG.debug("System default locale is " + defaultLocale);
                // Set default locale to org.apache.struts.Globals.LOCALE_KEY
                servletContext.setAttribute(org.apache.struts.Globals.LOCALE_KEY, defaultLocale);
            }
            // ============= COMMON STYLE SHEET PROCESS ==============
            LOG.debug("Replacing font in style sheet with font list from "
                + "ApplicationConf.properties process...");
            String baseDirStyleSheet = MessageUtil.getMessage(
                ContextParams.getBaseDirApplicationConf(), 
                ApplicationConfig.KEY.BASE_DIR_DEFAULT_THEME.name());
            LOG.debug("- Base directory for default-theme.css: " + baseDirStyleSheet);

            String styleSheetRealPath = getBaseDirRealPath(webRootContext, baseDirStyleSheet);
            String styleSheetName = getFileNameFromBaseDirConf(baseDirStyleSheet);
            LOG.debug("- Style sheet real path: " + styleSheetRealPath + ", file name: "
                + styleSheetName);
            
            String legendFileId = MessageUtil.getMessage(baseDirAppConf, 
                ApplicationConfig.KEY.LEGEND_FILE_ID.name());
            ContextParams.setLegendFileId(legendFileId);
            LOG.debug("Legend file id for downloading file: " + legendFileId);
            
            String maxCsvFileSize = MessageUtil.getMessage(baseDirAppConf,
                ApplicationConfig.KEY.MAX_CSV_FILE_SIZE.name());
            ContextParams.setMaxCsvFileSize(Integer.valueOf(maxCsvFileSize));
            LOG.debug("Default maxCsvFileSize: " + maxCsvFileSize);
            
            String jasperFilePath = null;
            if (Constants.MINUS_ONE != srcPath.indexOf(Constants.SYMBOL_SLASH)) {
                jasperFilePath = StringUtil.appendsString(srcPath, Constants.SYMBOL_SLASH);
            } else if (Constants.MINUS_ONE != srcPath.indexOf(Constants.SYMBOL_BACK_SLASH)) {
                jasperFilePath = StringUtil.appendsString(srcPath, Constants.SYMBOL_BACK_SLASH);
            } else {
                jasperFilePath = MessageUtil.getMessage(baseDirAppConf,
                    ApplicationConfig.KEY.JASPER_FILE_PATH.name());
            }
            
            ContextParams.setJasperFilePath(jasperFilePath);
            
            LOG.debug("Default jasperFilePath: " + jasperFilePath);
            
            String emailSmtp = MessageUtil.getMessage(baseDirAppConf,
                ApplicationConfig.KEY.EMAIL_SMTP.name());
            ContextParams.setEmailSmtp(emailSmtp);
            LOG.debug("Default emailSmtp: " + emailSmtp);
            
            String defaultEmailFrom = MessageUtil.getMessage(baseDirAppConf,
                ApplicationConfig.KEY.DEFAULT_EMAIL_FROM.name());
            ContextParams.setDefaultEmailFrom(defaultEmailFrom);
            LOG.debug("Default e-mail from: " + defaultEmailFrom);
            
            String defaultCigmaPoNo = MessageUtil.getMessage(baseDirAppConf,
                ApplicationConfig.KEY.DEFAULT_CIGMA_PO_NO.name());
            ContextParams.setDefaultCigmaPoNo(defaultCigmaPoNo);
            LOG.debug("Default Cigma P/O No: " + defaultCigmaPoNo);
            
            // [IN063] for upload digital signature
            String maxSignatureFileSize = MessageUtil.getMessage(baseDirAppConf,
                ApplicationConfig.KEY.MAX_SIGNATURE_FILE_SIZE.name());
            ContextParams.setMaxSignatureFileSize(Integer.valueOf(maxSignatureFileSize));
            LOG.debug("Maximum image for digital signature file size : " + maxSignatureFileSize);
            
            // Purge process send e-mail to person in charge
            String purgeEmailSendTo = MessageUtil.getMessage(baseDirAppConf,
                ApplicationConfig.KEY.PURGE_EMAIL_SEND_TO.name());
            ContextParams.setPurgeEmailSendTo(purgeEmailSendTo);
            LOG.debug("Purge process send e-mail to person in charge : " + purgeEmailSendTo);
            
        } catch (Exception e) {
            LOG.error(e);
            LOG.error(e.getClass() + ": " + e.getMessage());
        }
    }

    /**
     * getBaseDirRealPath performs transform base directory in web.xml to real
     * path.
     * 
     * @param webRootContext the web root context
     * @param baseDir the base dir
     * @return String - real path in form of "aaa/def/XXXX".
     * @sample input: com.globaldenso.asia.sps.conf.ApplicationConfig output:
     * com/globaldenso/asia/sps/conf/ApplicationConfig
     */
    private String getBaseDirRealPath(String webRootContext, String baseDir) {
        if ((null != baseDir) && !(Constants.EMPTY_STRING.equals(baseDir))) {
            StringBuffer fullPath = new StringBuffer(webRootContext);
            fullPath.append(Constants.SYMBOL_BACK_SLASH);
            return fullPath.append(baseDir.replace(Constants.SYMBOL_DOT,
                Constants.SYMBOL_BACK_SLASH)).toString();
        } else {
            return null;
        }
    }

    /**
     * Gets the file name from base dir conf.
     * 
     * @param baseDir the base dir
     * @return the file name from base dir conf
     */
    private String getFileNameFromBaseDirConf(String baseDir) {
        if ((null != baseDir) && !(Constants.EMPTY_STRING.equals(baseDir))
                && (-1 < baseDir.lastIndexOf("."))) {
            int lastDotInx = baseDir.lastIndexOf(".");
            if (-1 < lastDotInx) {
                // Remove the word after last "." sign. (It is prefix of file
                // name not path.)
                String screenMsgFileName = baseDir.substring(lastDotInx + 1,
                        baseDir.length());
                LOG.info("SCREEN LABEL MESSAGE FILE NAME: " + screenMsgFileName);
                return screenMsgFileName;

            }
        }
        // base directory is null or empty or not found "." sign.
        return baseDir;
    }

    /**
     * @param arg0 ServletContextEvent
     */
    public void contextDestroyed(ServletContextEvent arg0) {
    }
}