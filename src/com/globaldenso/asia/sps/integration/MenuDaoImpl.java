/*
 * ModifyDate Development company     Describe 
 * 2014/07/15 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain;
import com.globaldenso.asia.sps.business.dao.MenuDao;
import com.globaldenso.asia.sps.business.domain.UserMenuDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>The Class MenuDaoImpl implements MenuDao.</p>
 * <p>Provide operator about Menu.</p>
 * <ul>
 * <li>Method search : searchMenuByUserPermission</li>
 * </ul>
 *
 * @author CSI
 */
public class MenuDaoImpl extends SqlMapClientDaoSupport implements MenuDao {

    /**
     * The default constructor.
     * */
    public MenuDaoImpl() {
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.MenuDao#searchMenuByUserPermission(com.globaldenso.asia.sps.auto.business.domain.SpsMMenuDomain)
     * */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsMMenuDomain> searchMenuByUserPermission(UserMenuDomain userMenuDomain) {
        List<SpsMMenuDomain> menuList = (List<SpsMMenuDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.MENU_SEARCH_MENU_BY_USER_PERMISSION, userMenuDomain);
        return menuList;
    }

}
