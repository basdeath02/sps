/*
 * ModifyDate Development company     Describe 
 * 2014/08/20 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.dao.TmpUploadInvoiceDao;
import com.globaldenso.asia.sps.business.domain.TmpUploadInvoiceDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface TmpUploadInvoiceDaoImpl.</p>
 * <p>Service for temporary upload invoice about search temporary upload invoice data.</p>
 * <ul>
 * <li>Method search  : searchCountInvoiceInformation</li>
 * </ul>
 *
 * @author CSI
 */
public class TmpUploadInvoiceDaoImpl extends SqlMapClientDaoSupport implements TmpUploadInvoiceDao {
    
    /**
     * Instantiates a new Invoice Dao Implement.
     */
    public TmpUploadInvoiceDaoImpl(){
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.TmpUploadInvoiceDao#searchTmpUploadInvoice(com.globaldenso.asia.sps.business.domain.TmpUploadInvoiceDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<TmpUploadInvoiceDomain> searchTmpUploadInvoice(
        TmpUploadInvoiceDomain tmpUploadInvoiceDomain) {
        
        List<TmpUploadInvoiceDomain> result = null;
        result = (List<TmpUploadInvoiceDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.TMP_UPLOAD_INVOICE_SEARCH_TMP_UPLOAD_INVOICE,
                tmpUploadInvoiceDomain);
        return result;
    }
    
}
