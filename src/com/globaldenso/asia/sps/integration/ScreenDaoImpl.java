/*
 * ModifyDate Development company     Describe 
 * 2014/07/15 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.dao.ScreenDao;
import com.globaldenso.asia.sps.business.domain.RoleScreenDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleScreenDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>The Interface ScreenDao.</p>
 * <p>Provide operator about Menu.</p>
 * <ul>
 * <li>Method search : searchScreenByUserPermission</li>
 * </ul>
 *
 * @author CSI
 */
public class ScreenDaoImpl extends SqlMapClientDaoSupport implements ScreenDao {

    /** The default constructor. */
    public ScreenDaoImpl() {
        super();
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.ScreenDao#searchScreenByUserPermission(com.globaldenso.asia.sps.auto.business.domain.UserRoleScreenDomain)
     * */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<RoleScreenDomain> searchScreenByUserPermission(
        UserRoleScreenDomain roleUserScreenDomain)
    {
        List<RoleScreenDomain> screenList = (List<RoleScreenDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.SCREEN_SEARCH_SCREEN_BY_USER_PERMISSION,
                roleUserScreenDomain);
        return screenList;
    }

}
