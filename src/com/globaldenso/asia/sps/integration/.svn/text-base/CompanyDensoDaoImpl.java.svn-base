/*
 * ModifyDate Development company    Describe 
 * 2014/04/24 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import java.util.List;

import com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain;
import com.globaldenso.asia.sps.business.dao.CompanyDensoDao;
import com.globaldenso.asia.sps.business.domain.As400ServerConnectionInformationDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoDomain;
import com.globaldenso.asia.sps.business.domain.CompanyDensoWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>The CompanyDensoDaoImpl.</p>
 * <p>Provide operator about Company Denso.</p>
 * <li>Method search  : searchCompanyDensoDetail</li>
 * <li>Method search  : searchDensoCompanyByRole</li>
 * <li>Method search  : searchDensoCompanyByRelation</li>
 * <li>Method search  : searchAs400ServerConnection</li>
 * <li>Method search  : searchDensoCompanyByCodeList</li>
 * @author CSI
 * 
 */
public class CompanyDensoDaoImpl extends SqlMapClientDaoSupport implements CompanyDensoDao {
    
    /**
     * Instantiates a new Company Denso dao impl.
     */
    public CompanyDensoDaoImpl(){
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanyDensoDao#searchCompanyDensoDetail
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<CompanyDensoDomain> searchCompanyDensoDetail() {
        List<CompanyDensoDomain> companyDensoList = 
            (List<CompanyDensoDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.COMPANY_DENSO_SEARCH_COMPANY_DENSO_DETAIL);
        return companyDensoList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanyDensoDao#searchCompanyDensoByRole(
     * com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<CompanyDensoDomain> searchCompanyDensoByRole(
        PlantDensoWithScopeDomain plantDensoWithScope)
    {
        List<CompanyDensoDomain> companyDensoList = 
            (List<CompanyDensoDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.COMPANY_DENSO_SEARCH_COMPANY_DENSO_BY_ROLE, 
                    plantDensoWithScope);
        return companyDensoList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanyDensoDao#searchCompanyDensoByRelation(
     * com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<CompanyDensoDomain> searchCompanyDensoByRelation(
        PlantDensoWithScopeDomain plantDensoWithScope)
    {
        List<CompanyDensoDomain> companyDensoList = 
            (List<CompanyDensoDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.COMPANY_DENSO_SEARCH_COMPANY_DENSO_BY_RELATION,
                    plantDensoWithScope);
        return companyDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanyDensoDao#searchAs400ServerConnection(
     * com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<As400ServerConnectionInformationDomain> searchAs400ServerConnection(
        SpsMCompanyDensoDomain companyDensoDomain)
    {
        List<As400ServerConnectionInformationDomain> as400ServerInformationList = null;
        as400ServerInformationList = (List<As400ServerConnectionInformationDomain>)
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.COMPANY_DENSO_SEARCH_AS400_SERVER_CONNECTION, companyDensoDomain);
        return as400ServerInformationList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanyDensoDao#searchCompanyDensoByCodeList(
     * com.globaldenso.asia.sps.auto.business.domain.SpsMCompanyDensoDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsMCompanyDensoDomain> searchCompanyDensoByCodeList(
        SpsMCompanyDensoDomain companyDensoDomain){
        
        List<SpsMCompanyDensoDomain> spsMCompanyDensoList = null;
        spsMCompanyDensoList = (List<SpsMCompanyDensoDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.COMPANY_DENSO_SEARCH_COMPANY_DENSO_BY_CODE_LIST, 
                companyDensoDomain);
        return spsMCompanyDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanySupplierDao#searchExistCompanyDensoByRelation
     * (com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain)
     */
    public Integer searchExistCompanyDensoByRelation(
        CompanyDensoWithScopeDomain companyDensoWithScopeDomain)
    {
        Integer recordCount = 
            (Integer)getSqlMapClientTemplate().queryForObject(
                SqlMapConstants.COMPANY_DENSO_SEARCH_EXIST_COMPANY_DENSO_BY_RELATION,
                companyDensoWithScopeDomain);
        return recordCount;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanySupplierDao#searchExistCompanyDensoByRole
     * (com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain)
     */
    public Integer searchExistCompanyDensoByRole(
        CompanyDensoWithScopeDomain companyDensoWithScopeDomain)
    {
        Integer recordCount = 
            (Integer)getSqlMapClientTemplate().queryForObject(
                SqlMapConstants.COMPANY_DENSO_SEARCH_EXIST_COMPANY_DENSO_BY_ROLE,
                companyDensoWithScopeDomain);
        return recordCount;
    }
}
