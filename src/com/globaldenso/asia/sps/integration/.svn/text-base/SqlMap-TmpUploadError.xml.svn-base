<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE sqlMap
    PUBLIC "-//ibatis.apache.org//DTD SQL Map 2.0//EN" 
    "http://ibatis.apache.org/dtd/sql-map-2.dtd">
    
<sqlMap namespace="TmpUploadError">
    
    <sql id="PropertySearchGroupOfValidationError"> 
        select
            /* TmpUploadError.PropertyGroupOfValidationError */
            UPERR.USER_DSC_ID,
            UPERR.SESSION_CD,
            UPERR.MISC_TYPE,
            UPERR.MISC_CD,
            MAX(MISC.MISC_VALUE) messageType,
            MAX(MISC.MISC_DESCRIPTION) description,
            
            <!-- [IN068] Add new field for keep message parameter -->
            UPERR.MSG_PARAM,
            
            LINE_NO_TBL.LINE_NO
        from 
            SPS_TMP_UPLOAD_ERROR   UPERR,
            SPS_M_MISC  MISC,
            (
                select
                    UPERR.MISC_CD,
                    <!-- [IN068] Add new field for keep message parameter -->
                    UPERR.MSG_PARAM,
                    
                        listagg(UPERR.LINE_NO, ',') within group (order by UPERR.LINE_NO) LINE_NO
                    from
                        SPS_TMP_UPLOAD_ERROR   UPERR
                    where
                        UPERR.USER_DSC_ID      =   #userDscId#  and
                        UPERR.SESSION_CD       =   #sessionId#
                    group by
                        UPERR.MISC_CD
                        <!-- [IN068] Add new field for keep message parameter -->
                        , UPERR.MSG_PARAM
                        
            ) LINE_NO_TBL
        where 
            UPERR.USER_DSC_ID    =       #userDscId#            and
            UPERR.SESSION_CD     =       #sessionId#            and
            UPERR.MISC_CD        =       LINE_NO_TBL.MISC_CD    and
            
            <!-- [IN068] Add new field for keep message parameter -->
            ( UPERR.MSG_PARAM is null 
                or UPERR.MSG_PARAM = LINE_NO_TBL.MSG_PARAM ) and
            
            UPERR.MISC_TYPE      =       MISC.MISC_TYPE         and
            UPERR.MISC_CD        =       MISC.MISC_CD
        group by
            UPERR.USER_DSC_ID,
            UPERR.SESSION_CD,
            UPERR.MISC_TYPE,
            UPERR.MISC_CD,
            <!-- [IN068] Add new field for keep message parameter -->
            UPERR.MSG_PARAM,
            
            LINE_NO_TBL.LINE_NO
        order by
            UPERR.MISC_CD
            
            <!-- [IN068] Add new field for keep message parameter -->
            , UPERR.MSG_PARAM
    </sql>
    
    <sql id="PropertySearchCountWarning"> 
        select
        /* TmpUploadError.PropertySearchCountWarning */
            UPERR.USER_DSC_ID,
            UPERR.LINE_NO,
            MISC.MISC_VALUE
        from    
            SPS_TMP_UPLOAD_ERROR   UPERR,
            SPS_M_MISC             MISC
        where   
            UPERR.LINE_NO not in (
                select 
                    LINE_NO
                from   
                    SPS_TMP_UPLOAD_ERROR   UPERR_SUP,
                    SPS_M_MISC             MISC_SUP
                where  
                    MISC_SUP.MISC_VALUE     =   #messageType#         and
                    UPERR_SUP.USER_DSC_ID   =   UPERR.USER_DSC_ID     and
                    UPERR_SUP.SESSION_CD    =   UPERR.SESSION_CD      and
                    UPERR_SUP.MISC_TYPE     =   MISC_SUP.MISC_TYPE    and
                    UPERR_SUP.MISC_CD       =   MISC_SUP.MISC_CD
            ) and
            UPERR.USER_DSC_ID       =       #userDscId#            and
            UPERR.SESSION_CD        =       #sessionId#            and
            UPERR.MISC_TYPE         =       MISC.MISC_TYPE         and
            UPERR.MISC_CD           =       MISC.MISC_CD
        group by 
            UPERR.USER_DSC_ID, 
            UPERR.LINE_NO,  
            MISC.MISC_VALUE
    </sql>
    
    <sql id="PropertySearchCountIncorrect"> 
        select
        /* TmpUploadError.PropertySearchCountIncorrect */
            UPERR.USER_DSC_ID, 
            UPERR.LINE_NO
        from
            SPS_TMP_UPLOAD_ERROR   UPERR,
            SPS_M_MISC             MISC
        where  
            MISC.MISC_VALUE     =   #messageType#           and
            UPERR.USER_DSC_ID   =   #userDscId#             and
            UPERR.SESSION_CD    =   #sessionId#             and
            UPERR.MISC_TYPE     =   MISC.MISC_TYPE          and
            UPERR.MISC_CD       =   MISC.MISC_CD
        group by 
            UPERR.USER_DSC_ID, 
            UPERR.LINE_NO
    </sql>
    
    <select id="SearchCountWarning" parameterClass="GroupUploadErrorDomain" resultClass="Integer">
        select 
            /* TmpUploadError.SearchCountWarning */
            count(uploadWarning_detail.USER_DSC_ID)
        from(
            <include refid="PropertySearchCountWarning" />
            )uploadWarning_detail
    </select>
    
    <select id="SearchCountIncorrect" parameterClass="GroupUploadErrorDomain" resultClass="Integer">
        select 
            /* TmpUploadError.SearchCountIncorrect */
            count(uploadIncorrect_detail.USER_DSC_ID)
        from(
            <include refid="PropertySearchCountIncorrect" />
            )uploadIncorrect_detail
    </select>
    
    <resultMap class="GroupUploadErrorDomain" id="GroupOfValidationErrorMap">
        <result property="userDscId"            column="USER_DSC_ID"/>
        <result property="sessionId"            column="SESSION_CD"/>
        <result property="messageType"          column="messageType"/>
        <result property="description"          column="description"/>
        <result property="lineNo"               column="LINE_NO"/>
        
        <!-- [IN068] Add new field for keep message parameter -->
        <result property="messageParam"         column="MSG_PARAM"/>
        
    </resultMap>
    <select id="SearchGroupOfValidationError" parameterClass="GroupUploadErrorDomain" resultMap="GroupOfValidationErrorMap">
        select
            /* TmpUploadError.SearchGroupOfValidationError */
            t.*
        from (
            select 
                rownum r,
                uploadError_detail.*
            from ( 
                <include refid="PropertySearchGroupOfValidationError" /> 
            )uploadError_detail
            where rownum <![CDATA[<=]]> #rowNumTo#
        ) t
        where 
            r <![CDATA[>=]]> #rowNumFrom#
    </select>
    
    <select id="SearchCountGroupOfValidationError" parameterClass="GroupUploadErrorDomain" resultClass="Integer">
        select 
            /* TmpUploadError.SearchCountGroupOfValidationError */
            count(uploadError_detail.USER_DSC_ID)
        from(
            <include refid="PropertySearchGroupOfValidationError" />
            )uploadError_detail
    </select>
    
</sqlMap>