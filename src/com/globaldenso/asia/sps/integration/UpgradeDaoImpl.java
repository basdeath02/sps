/*
 * ModifyDate Development company     Describe 
 * 2017/09/26 Netband U.Rungsiwut     Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;


import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.globaldenso.asia.sps.business.dao.UpgradeDao;
import com.globaldenso.asia.sps.business.domain.BhtUpgradeDomain;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface UResourceServices.</p>
 * <p>Service for Upgrade Resource about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchBhtUpgrade</li>
 * </ul>
 *
 * @author Netband
 */
public class UpgradeDaoImpl extends SqlMapClientDaoSupport implements UpgradeDao {
    
    /**
     * Instantiates a new Upgrade Dao Implement.
     */
    public UpgradeDaoImpl(){
        super();
    }
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UpgradeDao#searchBhtUpgrade(BhtUpgradeDomain)
     */
    public BhtUpgradeDomain searchBhtUpgrade(BhtUpgradeDomain bhtUpgradeDomain) {
        BhtUpgradeDomain bhtUpgradeDomainResult = null;
        bhtUpgradeDomainResult = (BhtUpgradeDomain)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.UPGRADE_BHT_SEARCH_BHT_UPGRADE, bhtUpgradeDomain);
        
        if(bhtUpgradeDomain.getVerNo().equals(bhtUpgradeDomainResult.getVerNo())){
            bhtUpgradeDomainResult = null;
        }
        return bhtUpgradeDomainResult;
    }
}
