/*
 * ModifyDate Development company    Describe 
 * 2014/04/24 CSI Parichat           Create
 * 2014/09/22 CSI Phakaporn          Add searchSupplierCompanyNameByRelation method.
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.dao.CompanySupplierDao;
import com.globaldenso.asia.sps.business.domain.CompanySupplierDomain;
import com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>The class CompanySupplierDaoImpl implements CompanySupplierDao.</p>
 * <p>Provide operator about Company Supplier.</p>
 * <li>Method search  : searchSupplierCompanyByRole</li>
 * <li>Method search  : searchSupplierCompanyByRelation</li>
 * <li>Method search  : searchSupplierCompanyNameByRelation</li>
 * <li>Method search  : searchCompanySupplierByCode</li>
 * <li>Method search  : searchExistCompanySupplierInformation</li>
 * @author CSI
 * 
 */
public class CompanySupplierDaoImpl extends SqlMapClientDaoSupport implements CompanySupplierDao {

    /** The default constructor. */
    public CompanySupplierDaoImpl() {
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanySupplierDao#searchCompanySupplierByRole(
     * com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<CompanySupplierDomain> searchCompanySupplierByRole(
        PlantSupplierWithScopeDomain plantSupplierWithScope)
    {
        List<CompanySupplierDomain> companySupplierList = 
            (List<CompanySupplierDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.COMPANY_SUPPLIER_SEARCH_COMPANY_SUPPLIER_BY_ROLE,
                    plantSupplierWithScope);
        return companySupplierList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanySupplierDao#searchCompanySupplierByRelation
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<CompanySupplierDomain> searchCompanySupplierByRelation(
        PlantSupplierWithScopeDomain plantSupplierWithScope)
    {
        List<CompanySupplierDomain> companySupplierList = 
            (List<CompanySupplierDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.COMPANY_SUPPLIER_SEARCH_COMPANY_SUPPLIER_BY_RELATION,
                    plantSupplierWithScope);
        return companySupplierList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanySupplierDao#searchCompanySupplierNameByRelation
     * (com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<CompanySupplierDomain> searchCompanySupplierNameByRelation(
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain)
    {
        List<CompanySupplierDomain> companySupplierList = 
            (List<CompanySupplierDomain>)getSqlMapClientTemplate().queryForList(
                SqlMapConstants.COMPANY_SUPPLIER_SEARCH_COMPANY_SUPPLIER_NAME_BY_RELATION,
                    companySupplierWithScopeDomain);
        return companySupplierList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanySupplierDao#searchCompanySupplierByCode
     * (com.globaldenso.asia.sps.business.domain.CompanySupplierDomain)
     */
    public CompanySupplierDomain searchCompanySupplierByCode(
        CompanySupplierDomain companySupplierCriteria){
        CompanySupplierDomain companySupplier = (CompanySupplierDomain)getSqlMapClientTemplate()
                .queryForObject(SqlMapConstants.COMPANY_SUPPLIER_SEARCH_COMPANY_SUPPLIER_BY_CODE,
                    companySupplierCriteria);
        return companySupplier;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanySupplierDao#searchExistCompanySupplierInformation
     * (com.globaldenso.asia.sps.business.domain.CompanySupplierDomain)
     */
    public Integer searchExistCompanySupplierInformation(
        CompanySupplierDomain companySupplierDomain)
    {
        Integer recordCount = (Integer)getSqlMapClientTemplate()
            .queryForObject(
                SqlMapConstants.COMPANY_SUPPLIER_SEARCH_EXIST_COMPANY_SUPPLIER_INFORMATION, 
                companySupplierDomain);
        return recordCount;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanySupplierDao#searchExistCompanySupplierNameByRelation
     * (com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain)
     */
    public Integer searchExistCompanySupplierByRelation(
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain)
    {
        Integer recordCount = 
            (Integer)getSqlMapClientTemplate().queryForObject(
                SqlMapConstants.COMPANY_SUPPLIER_SEARCH_EXIST_COMPANY_SUPPLIER_BY_RELATION,
                    companySupplierWithScopeDomain);
        return recordCount;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CompanySupplierDao#searchExistCompanySupplierByRole
     * (com.globaldenso.asia.sps.business.domain.CompanySupplierWithScopeDomain)
     */
    public Integer searchExistCompanySupplierByRole(
        CompanySupplierWithScopeDomain companySupplierWithScopeDomain)
    {
        Integer recordCount = 
            (Integer)getSqlMapClientTemplate().queryForObject(
                SqlMapConstants.COMPANY_SUPPLIER_SEARCH_EXIST_COMPANY_SUPPLIER_BY_ROLE,
                    companySupplierWithScopeDomain);
        return recordCount;
    }
}
