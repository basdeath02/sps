/*
 * ModifyDate Development company     Describe 
 * 2014/06/06 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.business.dao.UserDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>The Interface UserDaoImpl.</p>
 * <p>Call SQL statement from sqlMap-User.xml to run and return result to UserDao</p>
 * <li>Method update  : searchDepartmentName</li>
 * <li>Method search  : searchEmailUserDenso</li>
 * @author CSI
 * 
 */
public class UserDaoImpl extends SqlMapClientDaoSupport implements UserDao {
    
    /**
     * Instantiates a new User Dao Implement.
     */
    public UserDaoImpl(){
        super();
    }
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserDao#searchDepartmentName
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsMUserDomain> searchDepartmentName() {
        List<SpsMUserDomain> departmentList = null;
        departmentList = (List<SpsMUserDomain>)getSqlMapClientTemplate().queryForList(
            SqlMapConstants.USER_SEARCH_DEPARTMENT_NAME);
        return departmentList;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserDao#searchEmailUserDenso
     * (com.globaldenso.asia.sps.auto.business.domain.SpsMUserDensoDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsMUserDomain> searchEmailUserDenso(SpsMUserDensoDomain userDensoDomain) {
        List<SpsMUserDomain> userResultList = null;
        userResultList = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.USER_SEARCH_EMAIL_USER_DENSO, userDensoDomain);
        return userResultList;
    }
    
}
