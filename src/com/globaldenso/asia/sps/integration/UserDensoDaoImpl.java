/*
 * ModifyDate Development company     Describe 
 * 2014/06/03 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain;
import com.globaldenso.asia.sps.business.dao.UserDensoDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface UserDensoDaoImpl.</p>
 * <p>Call SQL statement from sqlMap-UserDenso.xml to run and return result to UserDensoDao</p>
 * <li>Method search  : searchCountUserDenso</li>
 * <li>Method search  : searchUserDenso</li>
 * <li>Method search  : searchCountUserDensoIncludeRole</li>
 * <li>Method search  : searchDensoUserIncludeRole</li>
 * @author CSI
 * 
 */
public class UserDensoDaoImpl extends SqlMapClientDaoSupport implements UserDensoDao {
    
    /**
     * Instantiates a new User DENSO Dao Implement.
     */
    public UserDensoDaoImpl(){
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserDensoDao#searchCountUserDenso
     * (com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain)
     */
    public Integer searchCountUserDenso(DensoUserInformationDomain densoUserInformationDomain){
        Integer count = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.USER_DENSO_SEARCH_COUNT_USER_DENSO, 
                densoUserInformationDomain);
        return count;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserDensoDao#searchUserDenso
     * (com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DensoUserInformationDomain> searchUserDenso(DensoUserInformationDomain 
        densoUserInformationDomain){
        List<DensoUserInformationDomain> result = 
            (List<DensoUserInformationDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.USER_DENSO_SEARCH_USER_DENSO,
                    densoUserInformationDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserDensoDao#searchCountUserDensoIncludeRole
     * (com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain)
     */
    public Integer searchCountUserDensoIncludeRole(
        DensoUserInformationDomain densoUserInformationDomain){
        Integer count = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.USER_DENSO_SEARCH_COUNT_USER_DENSO_INCLUDE_ROLE,
                densoUserInformationDomain);
        return count;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserDensoDao#searchUserDensoIncludeRole
     * (com.globaldenso.asia.sps.business.domain.DensoUserInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DensoUserInformationDomain> searchUserDensoIncludeRole(
        DensoUserInformationDomain densoUserInformationDomain){
        List<DensoUserInformationDomain> result = 
            (List<DensoUserInformationDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.USER_DENSO_SEARCH_USER_DENSO_INCLUDE_ROLE,
                    densoUserInformationDomain);
        return result;
    }
}
