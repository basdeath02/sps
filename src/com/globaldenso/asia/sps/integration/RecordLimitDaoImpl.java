/*
 * ModifyDate Development company     Describe 
 * 2014/06/25 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.dao.RecordLimitDao;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface RecordLimitDaoImpl.</p>
 * <p>Service about miscellaneous for search miscellaneous data.</p>
 * <ul>
 * <li>Method search  : searchRecordLimit</li>
 * <li>Method search  : searchRecordLimitPerPage</li>
 * </ul>
 *
 * @author CSI
 */
public class RecordLimitDaoImpl extends SqlMapClientDaoSupport implements RecordLimitDao {
    
    /**
     * Instantiates a new Record Limit Dao Implement.
     */
    public RecordLimitDaoImpl(){
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.RecordLimitDao#searchRecordLimit(com.globaldenso.asia.sps.business.domain.MiscDomain)
     */
    public MiscellaneousDomain searchRecordLimit(MiscellaneousDomain miscDomain){
        MiscellaneousDomain recordLimit =  (MiscellaneousDomain)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.RECORD_LIMIT_SEARCH_RECORD_LIMIT, miscDomain);
        return recordLimit;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.RecordLimitDao#searchRecordLimitPerPage(com.globaldenso.asia.sps.business.domain.MiscDomain)
     */
    public MiscellaneousDomain searchRecordLimitPerPage(MiscellaneousDomain miscDomain){
        MiscellaneousDomain recordLimitPerPage = new MiscellaneousDomain();
        recordLimitPerPage = (MiscellaneousDomain)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.RECORD_LIMIT_SEARCH_RECORD_LIMIT_PER_PAGE, miscDomain);
        return recordLimitPerPage;
    }
}
