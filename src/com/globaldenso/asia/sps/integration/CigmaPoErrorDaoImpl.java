/*
 * ModifyDate Development company     Describe 
 * 2015/02/27 CSI Akat                Create
 * 2016/03/16 CSI Akat                [IN068]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.sql.Date;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain;
import com.globaldenso.asia.sps.business.dao.CigmaPoErrorDao;
import com.globaldenso.asia.sps.business.domain.TransferPoErrorEmailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>The Class CigmaPoErrorDao.</p>
 * <p>For SPS_CIGMA_PO_ERROR.</p>
 * <ul>
 * <li>Method search  : searchTransferPoError</li>
 * <li>Method search  : searchMinIssueDate</li>
 * </ul>
 *
 * @author CSI
 */
public class CigmaPoErrorDaoImpl extends SqlMapClientDaoSupport implements CigmaPoErrorDao {
    
    /** The default constructor. */
    public CigmaPoErrorDaoImpl() {
        super();
    }
    
    /** {@inheritDoc}
     * @see com.com.globaldenso.asia.sps.business.dao.CigmaPoErrorDao#searchTransferPoError(
     * com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<TransferPoErrorEmailDomain> searchTransferPoError(
        SpsCigmaPoErrorDomain spsCigmaPoErrorDomain)
    {
        List<TransferPoErrorEmailDomain> result = (List<TransferPoErrorEmailDomain>)
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.CIGMA_PO_ERROR_SEARCH_TRANSFER_PO_ERROR, spsCigmaPoErrorDomain);
        return result;
    }

    // [IN068] add method for search minimum issue date
    /** {@inheritDoc}
     * @see com.com.globaldenso.asia.sps.business.dao.CigmaPoErrorDao#searchMinIssueDate(
     * com.globaldenso.asia.sps.auto.business.domain.SpsCigmaPoErrorDomain)
     */
    public Date searchMinIssueDate(SpsCigmaPoErrorDomain spsCigmaPoErrorDomain) {
        Date result = (Date)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.CIGMA_PO_ERROR_SEARCH_MIN_ISSUE_DATE, spsCigmaPoErrorDomain);
        return result;
    }

}
