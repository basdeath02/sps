/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.auto.business.domain.SpsTInvoiceDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTInvoiceCriteriaDomain;
import com.globaldenso.asia.sps.business.dao.InvoiceDao;
import com.globaldenso.asia.sps.business.domain.InvoiceCoverPageDomain;
import com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurgingAsnDomain;
import com.globaldenso.asia.sps.business.domain.PurgingBhtTransmitLogDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface InvoiceDaoImpl.</p>
 * <p>Service for invoice about search and update invoice status.</p>
 * <ul>
 * <li>Method search  : searchInvoiceInformation</li>
 * <li>Method create  : createInvoice</li>
 * <li>Method search  : searchInvoiceCoverPage</li>
 * <li>Method search  : searchInvoiceByAsn</li>
 * <li>Method search  : searchPurgingInvoice</li>
 * </ul>
 *
 * @author CSI
 */
public class InvoiceDaoImpl extends SqlMapClientDaoSupport implements InvoiceDao {
    
    /**
     * Instantiates a new Invoice Dao Implement.
     */
    public InvoiceDaoImpl(){
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceDao#searchInvoiceInformationDetail(com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<InvoiceInformationDomain> searchInvoiceInformation(
        InvoiceInformationDomain invoiceInformationDomain)
    {
        List<InvoiceInformationDomain> result = null;
        result = (List<InvoiceInformationDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.INVOICE_SEARCH_INVOICE_INFORMATION,
                invoiceInformationDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceDao#searchCountInvoiceInformation(com.globaldenso.asia.sps.business.domain.SpsTInvoiceDomain)
     */
    public Integer searchCountInvoiceInformation(InvoiceInformationDomain invoiceInformationDomain)
    {
        Integer recordCount = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.INVOICE_SEARCH_COUNT_INVOICE_INFORMATION, 
                invoiceInformationDomain);
        return recordCount;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceDao#createInvoice(com.globaldenso.asia.sps.business.domain.SpsTInvoiceDomain)
     */
    public Integer createInvoice(SpsTInvoiceDomain spsTInvoiceDomain)
    {
        Integer invoiceId = null;
        invoiceId = (Integer)getSqlMapClientTemplate()
            .insert(SqlMapConstants.INVOICE_CREATE_INVOICE, spsTInvoiceDomain);
        return invoiceId;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceDao#searchInvoiceCoverPage(com.globaldenso.asia.sps.business.domain.InvoiceInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<InvoiceCoverPageDomain> searchInvoiceCoverPage(
        InvoiceCoverPageDomain invoiceCoverPageDomain)
    {
        List<InvoiceCoverPageDomain> result = null;
        result = (List<InvoiceCoverPageDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.INVOICE_SEARCH_INVOICE_COVER_PAGE, 
                invoiceCoverPageDomain);
        return result;
    }

    // [IN012] ASN can create by many Invoice (in case cancel invoice)
    ///** {@inheritDoc}
    // * @see com.globaldenso.asia.sps.business.dao.InvoiceDao#searchInvoiceByAsn(com.globaldenso.asia.sps.auto.business.domain.PurgingAsnDomain)
    // */
    //public SpsTInvoiceDomain searchInvoiceByAsn(PurgingAsnDomain purgingAsnDomain)
    //{
    //    SpsTInvoiceDomain spsTInvoiceDomain = null;
    //    spsTInvoiceDomain = (SpsTInvoiceDomain)getSqlMapClientTemplate()
    //        .queryForObject(SqlMapConstants.INVOICE_SEARCH_INVOICE_BY_ASN, purgingAsnDomain);
    //    return spsTInvoiceDomain;
    //}
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceDao#searchInvoiceByAsn(com.globaldenso.asia.sps.auto.business.domain.PurgingAsnDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsTInvoiceDomain> searchInvoiceByAsn(PurgingAsnDomain purgingAsnDomain)
    {
        List<SpsTInvoiceDomain> invoiceList = null;
        invoiceList = (List<SpsTInvoiceDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.INVOICE_SEARCH_INVOICE_BY_ASN, purgingAsnDomain);
        return invoiceList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceDao#searchPurgingInvoice(com.globaldenso.asia.sps.business.domain.PurgingPODomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsTInvoiceDomain> searchPurgingInvoice(PurgingPoDomain purgingPoDomain)
    {
        List<SpsTInvoiceDomain> spsTInvoiceList = null;
        spsTInvoiceList = (List<SpsTInvoiceDomain> )getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.INVOICE_SEARCH_PURGING_INVOICE, purgingPoDomain);
        return spsTInvoiceList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceDao#searchInvoiceForTransferToJde(com.globaldenso.asia.sps.business.domain.SpsTInvoiceCriteriaDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<InvoiceInformationDomain> searchInvoiceForTransferToJde(
        SpsTInvoiceCriteriaDomain spsTInvoiceCriteriaDomain)
    {
        List<InvoiceInformationDomain> invoiceInfoList = null;
        invoiceInfoList = (List<InvoiceInformationDomain> )getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.INVOICE_SEARCH_INVOICE_FOR_TRANSFER_TO_JDE, 
                spsTInvoiceCriteriaDomain);
        return invoiceInfoList;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceDao#deletePurgingInvoice(String)
     */
    public int deletePurgingInvoice(String invoiceId)
    {
        int result = Constants.ZERO;
        result += getSqlMapClientTemplate().delete(SqlMapConstants.INVOICE_DELETE_SPS_T_CN_DETAIL, invoiceId);
        result += getSqlMapClientTemplate().delete(SqlMapConstants.INVOICE_DELETE_SPS_T_CN, invoiceId);
        result += getSqlMapClientTemplate().delete(SqlMapConstants.INVOICE_DELETE_SPS_T_INVOICE_DETAIL, invoiceId);
        result += getSqlMapClientTemplate().delete(SqlMapConstants.INVOICE_SPS_T_INV_COVERPAGE_RUNNO, invoiceId);
        result += getSqlMapClientTemplate().delete(SqlMapConstants.INVOICE_DELETE_SPS_T_INVOICE, invoiceId);
        return result;
    }

	public Integer deleteInvoice(
			PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria) {
		int result = Constants.ZERO;
		result += getSqlMapClientTemplate().delete(
				SqlMapConstants.INVOICE_DELETE_SPS_T_INVOICE_BY_PURGE,
				purgingBhtTransmitLogCriteria);
		return result;
	}
}
