/*
 * ModifyDate Development company     Describe 
 * 2014/04/24 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.auto.business.domain.SpsMUserDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain;
import com.globaldenso.asia.sps.business.domain.UserSupplierDomain;
import com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain;
import com.globaldenso.asia.sps.business.dao.UserSupplierDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface UserSupplierDaoImpl.</p>
 * <p>Call SQL statement from sqlMap-UserSupplier.xml to run and return result to UserSupplierDao</p>
 * <li>Method search  : searchCountUserSupplier</li>
 * <li>Method search  : searchSupplierUser</li>
 * <li>Method search  : searchUserEmailBySupplierCode</li>
 * <li>Method search  : searchUserSupplierIncludeRole</li>
 * <li>Method search  : searchUserSupplierEmailByNotFoundFlag</li>
 * <li>Method search  : searchUserSupplierEmailByUrgentFlag</li>
 * <li>Method search  : searchEmailUserSupplier</li>
 * @author CSI
 * 
 */
public class UserSupplierDaoImpl extends SqlMapClientDaoSupport implements UserSupplierDao {
    
    /**
     * Instantiates a new User Supplier Dao Implement.
     */
    public UserSupplierDaoImpl(){
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserSupplierDao#searchCountUserSupplier
     * (com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain)
     */
    public Integer searchCountUserSupplier(
        SupplierUserInformationDomain supplierUserInformationDomain){
        Integer count = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.USER_SUPPLIER_SEARCH_COUNT_USER_SUPPLIER, 
                supplierUserInformationDomain);
        return count;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserSupplierDao#searchUserSupplier
     * (com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SupplierUserInformationDomain> searchUserSupplier(
        SupplierUserInformationDomain supplierUserInformationDomain){
        List<SupplierUserInformationDomain> result = 
            (List<SupplierUserInformationDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.USER_SUPPLIER_SEARCH_USER_SUPPLIER, 
                    supplierUserInformationDomain);
        return result;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserSupplierDao#searchUserEmailBySupplierCode
     * (com.globaldenso.asia.sps.business.domain.UserSupplierDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<String> searchUserEmailBySupplierCode(UserSupplierDomain userSupplierDomain) {
        List<String> userEmailList = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.USER_SUPPLIER_SEARCH_USER_EMAIL_BY_SUPPLIER_CODE,
                userSupplierDomain);
        return userEmailList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserSupplierDao#searchUserSupplierEmailByNotFoundFlag
     * (com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SupplierUserInformationDomain> searchUserSupplierEmailByNotFoundFlag(
        UserSupplierDetailDomain userSupplierDetailDomain){
        List<SupplierUserInformationDomain> result = 
            (List<SupplierUserInformationDomain>)getSqlMapClientTemplate().queryForList(
                SqlMapConstants.USER_SUPPLIER_SEARCH_USER_SUPPLIER_EMAIL_BY_NOT_FOUND_FLAG,
                userSupplierDetailDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserSupplierDao#searchUserSupplierEmailByUrgentFlag
     * (com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SupplierUserInformationDomain> searchUserSupplierEmailByUrgentFlag(
        UserSupplierDetailDomain userSupplierDetailDomain){
        List<SupplierUserInformationDomain> result = 
            (List<SupplierUserInformationDomain>)getSqlMapClientTemplate().queryForList(
                SqlMapConstants.USER_SUPPLIER_SEARCH_USER_SUPPLIER_EMAIL_BY_URGENT_FLAG,
                userSupplierDetailDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserSupplierDao#searchUserSupplierIncludeRole
     * (com.globaldenso.asia.sps.business.domain.SupplierUserInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SupplierUserInformationDomain> searchUserSupplierIncludeRole(
        SupplierUserInformationDomain supplierUserInformationDomain){
        List<SupplierUserInformationDomain> result = 
            (List<SupplierUserInformationDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.USER_SUPPLIER_SEARCH_USER_SUPPLIER_INCLUDE_ROLE,
                    supplierUserInformationDomain);
        return result;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserSupplierDao#searchEmailUserSupplier(com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsMUserDomain> searchEmailUserSupplier(
        UserSupplierDetailDomain userSupplierDetailDomain) {
        List<SpsMUserDomain> userSupplierList = null;
        userSupplierList = getSqlMapClientTemplate().queryForList(
            SqlMapConstants.USER_SUPPLIER_SEARCH_EMAIL_USER_SUPPLIER, userSupplierDetailDomain);
        return userSupplierList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserSupplierDao#searchUserSupplierEmailByNotFoundFlag
     * (com.globaldenso.asia.sps.business.domain.UserSupplierDetailDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SupplierUserInformationDomain> searchUserSupplierEmailByUrgentFlagForBackorder(
        UserSupplierDetailDomain userSupplierDetailDomain){
        List<SupplierUserInformationDomain> result = 
            (List<SupplierUserInformationDomain>)getSqlMapClientTemplate().queryForList(
                SqlMapConstants.USER_SUPPLIER_SEARCH_USER_SUPPLIER_EMAIL_BY_URGENT_FLAG_FOR_BACKORDER,
                userSupplierDetailDomain);
        return result;
    }
}
