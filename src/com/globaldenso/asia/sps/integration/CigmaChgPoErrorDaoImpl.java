/*
 * ModifyDate Development company     Describe 
 * 2015/02/27 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain;
import com.globaldenso.asia.sps.business.dao.CigmaChgPoErrorDao;
import com.globaldenso.asia.sps.business.domain.TransferChgPoErrorEmailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>The Class CigmaChgPoErrorDaoImpl.</p>
 * <p>For SPS_CIGMA_CHG_PO_ERROR.</p>
 * <ul>
 * <li>Method search  : searchTransferPoError</li>
 * </ul>
 *
 * @author CSI
 */
public class CigmaChgPoErrorDaoImpl extends SqlMapClientDaoSupport implements CigmaChgPoErrorDao {
    
    /** The default constructor. */
    public CigmaChgPoErrorDaoImpl() {
        super();
    }
    
    /** {@inheritDoc}
     * @see com.com.globaldenso.asia.sps.business.dao.CigmaChgPoErrorDao#searchTransferChgPoError(
     * com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgPoErrorDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<TransferChgPoErrorEmailDomain> searchTransferChgPoError(
        SpsCigmaChgPoErrorDomain spsCigmaChgPoErrorDomain)
    {
        List<TransferChgPoErrorEmailDomain> result = (List<TransferChgPoErrorEmailDomain>)
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.CIGMA_CHG_PO_ERROR_SEARCH_TRANSFER_CHG_PO_ERROR, 
                spsCigmaChgPoErrorDomain);
        return result;
    }

}
