/*
 * ModifyDate Development company     Describe 
 * 2014/06/23 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.dao.MiscellaneousDao;
import com.globaldenso.asia.sps.business.domain.MiscellaneousDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface MiscDaoImpl.</p>
 * <p>Service for miscellaneous about search miscellaneous data.</p>
 * <ul>
 * <li>Method search  : searchMiscValue</li>
 * <li>Method search  : searchMisc</li>
 * </ul>
 *
 * @author CSI
 */
public class MiscellaneousDaoImpl extends SqlMapClientDaoSupport implements MiscellaneousDao {
    
    /**
     * Instantiates a new Misc Dao Implement.
     */
    public MiscellaneousDaoImpl(){
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.MiscDaoImpl#searchMiscValue(com.globaldenso.asia.sps.business.domain.MiscDomain)
     */
    public Integer searchMiscValue(MiscellaneousDomain miscellaneousDomain)
    {
        Integer miscValue = null;
        miscValue = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.MISCELLANEOUS_SEARCH_MISC_VALUE, miscellaneousDomain);
        return miscValue;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.MiscDaoImpl#searchMisc(com.globaldenso.asia.sps.business.domain.MiscDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<MiscellaneousDomain> searchMisc(MiscellaneousDomain miscellaneousDomain)
    {
        List<MiscellaneousDomain> miscResultList = null;
        miscResultList = (List<MiscellaneousDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.MISCELLANEOUS_SEARCH_MISC, miscellaneousDomain);
        return miscResultList;
    }
}
