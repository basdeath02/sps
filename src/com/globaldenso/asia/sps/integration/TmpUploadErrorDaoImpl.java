/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;


import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain;
import com.globaldenso.asia.sps.business.dao.TmpUploadErrorDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface TempUploadErrorDaoImpl.</p>
 * <p>Call SQL statement from sqlMap-TempUploadError.xml to run and return result to TempUploadErrorDao</p>
 * <li>Method search  : searchCountWarning</li>
 * <li>Method search  : searchCountIncorrect</li>
 * <li>Method search  : searchGroupOfValidationError</li>
 * <li>Method search  : searchCountGroupOfValidationError</li>
 * @author CSI
 * 
 */
public class TmpUploadErrorDaoImpl extends SqlMapClientDaoSupport implements TmpUploadErrorDao {
    
    /**
     * Instantiates a new Temp Upload Error Dao Implement.
     */
    public TmpUploadErrorDaoImpl(){
        super();
    }
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.TmpUploadErrorDao#searchCountWarning(com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain)
     */
    public int searchCountWarning(GroupUploadErrorDomain groupUploadErrorDomain) {
        int countRecord = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.TMP_UPLOAD_ERROR_SEARCH_COUNT_WARNING, 
                groupUploadErrorDomain);
        return countRecord;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.TmpUploadErrorDao#searchCountIncorrect(com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain)
     */
    public int searchCountIncorrect(GroupUploadErrorDomain groupUploadErrorDomain) {
        int countRecord = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.TMP_UPLOAD_ERROR_SEARCH_COUNT_IN_CORRECT, 
                groupUploadErrorDomain);
        return countRecord;
    }
    
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.TmpUploadErrorDao#searchGroupOfValidationError(com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<GroupUploadErrorDomain> searchGroupOfValidationError(
        GroupUploadErrorDomain groupUploadErrorDomain)
    {
        List<GroupUploadErrorDomain> result = (List<GroupUploadErrorDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.TMP_UPLOAD_ERROR_SEARCH_GROUP_OF_VALIDATION_ERROR, 
                groupUploadErrorDomain);
        return result;
    }
       
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.TmpUploadErrorDao#searchCountGroupOfValidationError(com.globaldenso.asia.sps.business.domain.GroupUploadErrorDomain)
     */
    public int searchCountGroupOfValidationError(GroupUploadErrorDomain groupUploadErrorDomain) {
        int countRecord = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.TMP_UPLOAD_ERROR_SEARCH_COUNT_GROUP_OF_VALIDATION_ERROR, 
                groupUploadErrorDomain);
        return countRecord;
    }

}
