/*
 * ModifyDate Development company     Describe 
 * 2014/06/26 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;


import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.domain.TmpUserDensoDomain;
import com.globaldenso.asia.sps.business.dao.TmpUploadUserDensoDao;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface TempUploadUserDensoDaoImpl.</p>
 * <p>Call SQL statement from sqlMap-TempUploadUserDenso.xml to run and 
 * return result to TempUploadUserDensoDao</p>
 * <li>Method search  : searchTempUploadDensoOneRecord</li>
 * @author CSI
 * 
 */
public class TmpUploadUserDensoDaoImpl extends SqlMapClientDaoSupport 
                implements TmpUploadUserDensoDao {
    
    /**
     * Instantiates a new Temp User Supplier Dao Implement.
     */
    public TmpUploadUserDensoDaoImpl(){
        super();
    }
       
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.TmpUploadUserDensoDao#searchTempUploadDensoOneRecord(com.globaldenso.asia.sps.business.domain.TmpUserDensoDomain)
     */
    public TmpUserDensoDomain searchTmpUploadDensoOneRecord(TmpUserDensoDomain 
        tmpUserDensoDomain) {
        TmpUserDensoDomain result = (TmpUserDensoDomain)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.
                TMP_UPLOAD_USER_DENSO_SERACH_TMP_UPLOAD_DENSO_ONE_RECORD, tmpUserDensoDomain);
        return result;
    }

}
