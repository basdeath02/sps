/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.auto.business.domain.SpsMRoleTypeDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsMUserRoleCriteriaDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleDetailDomain;
import com.globaldenso.asia.sps.business.domain.UserRoleScreenDomain;
import com.globaldenso.asia.sps.business.dao.UserRoleDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface UserRoleDaoImpl.</p>
 * <p>Call SQL statement from sqlMap-UserRole.xml to run and return result to UserRoleDao</p>
 * <li>Method search  : searchUserRoleByDscId</li>
 * <li>Method delete  : deleteUserRoleByDscId</li>
 * @author CSI
 * 
 */
public class UserRoleDaoImpl extends SqlMapClientDaoSupport implements UserRoleDao {
    
    /**
     * Instantiates a new User Role Dao Implement.
     */
    public UserRoleDaoImpl(){
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserRoleDao#searchUserRoleByDscId
     * (com.globaldenso.asia.sps.business.domain.UserRoleDetailDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<UserRoleDetailDomain> searchUserRoleByDscId(UserRoleDetailDomain 
        userRoleDetailDomain){
        List<UserRoleDetailDomain> result 
            = (List<UserRoleDetailDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.USER_ROLE_SEARCH_USER_ROLE_BY_DSCID, 
                    userRoleDetailDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserRoleDao#deleteUserRoleByDscId
     * (com.globaldenso.asia.sps.business.domain.SpsMUserRoleCriteriaDomain)
     */
    public int deleteUserRoleByDscId(SpsMUserRoleCriteriaDomain
        userRoleCriteriaDomain){
        int count = (int)getSqlMapClientTemplate()
                .delete(SqlMapConstants.USER_ROLE_DELETE_USER_ROLE_BY_DSCID, 
                    userRoleCriteriaDomain);
        return count;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UserRoleDao#searchUserRoleType
     * (com.globaldenso.asia.sps.business.domain.UserRoleScreenDomain)
     * */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsMRoleTypeDomain> searchUserRoleType(UserRoleScreenDomain userRoleScreen) {
        List<SpsMRoleTypeDomain> result = (List<SpsMRoleTypeDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.USER_ROLE_SEARCH_USER_ROLE_TYPE, 
                userRoleScreen);
        return result;
    }
}
