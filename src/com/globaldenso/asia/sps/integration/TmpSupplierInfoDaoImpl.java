/*
 * ModifyDate Development company     Describe 
 * 2014/08/24 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;


import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.TmpSupplierInfoDomain;
import com.globaldenso.asia.sps.business.dao.TmpSupplierInfoDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface TempUserSupplierDaoImpl.</p>
 * <p>Call SQL statement from sqlMap-TempUserSupplier.xml to run and return result to TempUserSupplierDao</p>
 * <li>Method search  : searchTempUploadSupplierOneRecord</li>
 * @author CSI
 * 
 */
public class TmpSupplierInfoDaoImpl extends SqlMapClientDaoSupport implements TmpSupplierInfoDao {
    
    /**
     * Instantiates a new Temp Supplier Info Dao Implement.
     */
    public TmpSupplierInfoDaoImpl(){
        super();
    }
       
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.TmpSupplierInfoDao#searchTempUploadSupplierInfoOneRecord(com.globaldenso.asia.sps.business.domain.TmpSupplierInfoDomain)
     */
    public TmpSupplierInfoDomain searchTmpUploadSupplierInfoOneRecord(TmpSupplierInfoDomain 
        tmpSupplierInfoDomain) {
        TmpSupplierInfoDomain result = (TmpSupplierInfoDomain)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.
                TMP_SUPPLIER_INFO_SEARCH_TMP_UPLOAD_SUPPLIER_INFO_ONE_RECORD,
                tmpSupplierInfoDomain);
        return result;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.TmpSupplierInfoDao#searchTmpUploadSupplierInfoCompanyName(com.globaldenso.asia.sps.business.domain.TmpSupplierInfoDomain)
     */
    public TmpSupplierInfoDomain searchTmpUploadSupplierInfoCompanyName(TmpSupplierInfoDomain 
        tmpSupplierInfoDomain) {
        TmpSupplierInfoDomain result = (TmpSupplierInfoDomain)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.
                TMP_SUPPLIER_INFO_SEARCH_TMP_UPLOAD_SUPPLIER_INFO_COMPANY_NAME,
                tmpSupplierInfoDomain);
        return result;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.TmpSupplierInfoDao#searchTmpUploadSupplierInfoPlant(com.globaldenso.asia.sps.business.domain.TmpSupplierInfoDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PlantSupplierDomain> searchTmpUploadSupplierInfoPlant(TmpSupplierInfoDomain 
        tmpSupplierInfoDomain) {
        List<PlantSupplierDomain> result = (List<PlantSupplierDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.
                TMP_SUPPLIER_INFO_SEARCH_TMP_UPLOAD_SUPPLIER_INFO_PLANT,
                tmpSupplierInfoDomain);
        return result;
    }

}
