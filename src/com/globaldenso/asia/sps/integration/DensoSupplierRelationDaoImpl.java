/*
 * ModifyDate Development company     Describe 
 * 2014/07/16 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.dao.DensoSupplierRelationDao;
import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierRelationDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>The Interface DensoSupplierRelationDao.</p>
 * <p>Provide operator about Menu.</p>
 * <ul>
 * <li>Method search : searchRelationByRoleDenso</li>
 * <li>Method search : searchRelationByRoleSupplier</li>
 * </ul>
 *
 * @author CSI
 */
public class DensoSupplierRelationDaoImpl extends SqlMapClientDaoSupport
    implements DensoSupplierRelationDao
{
    
    /** The default constructor. */
    public DensoSupplierRelationDaoImpl() {
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DensoSupplierRelationDao#searchRelationByRoleDenso
     * (com.globaldenso.asia.sps.auto.business.domain.DataScopeControlDomain)
     * */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DensoSupplierRelationDomain> searchRelationByRoleDenso(
        DataScopeControlDomain dataScopeControlDemain)
    {
        List<DensoSupplierRelationDomain> densoSupplierRelationList
            = (List<DensoSupplierRelationDomain>)getSqlMapClientTemplate().queryForList(
                SqlMapConstants.DENSO_SUPPLIER_RELATION_SEARCH_DENSO_SUPPLIER_RELATION_BY_ROLE_DENSO
                , dataScopeControlDemain);
        return densoSupplierRelationList;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DensoSupplierRelationDao#searchRelationByRoleSupplier
     * (com.globaldenso.asia.sps.auto.business.domain.DataScopeControlDomain)
     * */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DensoSupplierRelationDomain> searchRelationByRoleSupplier(
        DataScopeControlDomain dataScopeControlDemain)
    {
        List<DensoSupplierRelationDomain> densoSupplierRelationList
            = (List<DensoSupplierRelationDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants
                    .DENSO_SUPPLIER_RELATION_SEARCH_DENSO_SUPPLIER_RELATION_BY_ROLE_SUPPLIER, 
                    dataScopeControlDemain);
        return densoSupplierRelationList;
    }

}
