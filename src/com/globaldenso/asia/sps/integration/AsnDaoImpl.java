/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Parichat            Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain;
import com.globaldenso.asia.sps.business.dao.AsnDao;
import com.globaldenso.asia.sps.business.domain.AsnDomain;
import com.globaldenso.asia.sps.business.domain.AsnInfoDomain;
import com.globaldenso.asia.sps.business.domain.AsnInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain;
import com.globaldenso.asia.sps.business.domain.AsnProgressInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.PurgingAsnDomain;
import com.globaldenso.asia.sps.business.domain.PurgingDoDomain;
import com.globaldenso.asia.sps.business.domain.SendingAsnDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface RecordLimitDaoImpl.</p>
 * <p>Service for ASN about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchSendingAsn</li>
 * <li>Method search  : searchCountASNInformation</li>
 * <li>Method search  : searchAsnDetail</li>
 * <li>Method search  : searchAsnForGroupInvoice</li>
 * <li>Method search  : searchPurgingAsnOrder</li>
 * <li>Method search  : searchSumShippingQty</li>
 * <li>Method search  : searchSumShippingQtyByPart</li>
 * </ul>
 *
 * @author CSI
 */
public class AsnDaoImpl extends SqlMapClientDaoSupport implements AsnDao {
    
    /**
     * Instantiates a new ASN Dao Implement.
     */
    public AsnDaoImpl(){
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.ASNDao#searchSendingAsn(com.globaldenso.asia.sps.business.domain.SendingAsnDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<AsnInfoDomain> searchSendingAsn(SendingAsnDomain sendingAsnDomain){
        List<AsnInfoDomain> asnInfoList = null;
        asnInfoList = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.ASN_SEARCH_SENDING_ASN, sendingAsnDomain);
        return asnInfoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.ASNDao#searchAsnDetail(com.globaldenso.asia.sps.business.domain.ASNInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<AsnInformationDomain> searchAsnDetail(AsnInformationDomain asnInformationDomain) {
    	List<AsnInformationDomain> asnInformationList = null;
    	    asnInformationList = getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.ASN_SEARCH_ASN_DETAIL, asnInformationDomain);
        return asnInformationList;
    }
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.ASNDao#searchAsnInformation(com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<AsnProgressInformationReturnDomain> searchAsnInformation(
        AsnProgressInformationDomain asnProgressInformationDomain){
        List<AsnProgressInformationReturnDomain> asnInformationResultList = null;
        asnInformationResultList = (List<AsnProgressInformationReturnDomain>)
            getSqlMapClientTemplate().queryForList(SqlMapConstants.ASN_SEARCH_ASN_INFORMATION,
                asnProgressInformationDomain);
        return asnInformationResultList;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.ASNDao#searchCountAsnInformation(com.globaldenso.asia.sps.business.domain.AsnProgressInformationDomain)
     */
    public int searchCountAsnInformation(AsnProgressInformationDomain asnProgressInformationDomain){
        int recordCount = Constants.ZERO;
        recordCount = (Integer)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.ASN_SEARCH_COUNT_ASN_INFORMATION, asnProgressInformationDomain);
        return recordCount;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.ASNDao#searchAsnDetail(com.globaldenso.asia.sps.business.domain.ASNDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<AsnDomain> searchExistAsn(AsnDomain asnDomain){
    	List<AsnDomain> asnList = null;
    	asnList = getSqlMapClientTemplate().queryForList(
    	            SqlMapConstants.ASN_SEARCH_EXIST_ASN, asnDomain);
    	return asnList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.ASNDao#searchAsnReport(com.globaldenso.asia.sps.auto.business.domain.SpsTAsnDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<AsnInfoDomain> searchAsnReport(SpsTAsnDomain spsTAsnDomain){
        List<AsnInfoDomain> result = null;
        result = getSqlMapClientTemplate().queryForList(
            SqlMapConstants.ASN_SEARCH_ASN_REPORT, spsTAsnDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.ASNDao#searchAsnForGroupInvoice(com.globaldenso.asia.sps.business.domain.ASNDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<AsnDomain> searchAsnForGroupInvoice(AsnDomain asnDomain){
        List<AsnDomain> asnList = null;
        asnList = getSqlMapClientTemplate().queryForList(
            SqlMapConstants.ASN_SEARCH_ASN_FOR_GROUP_INVOICE, asnDomain);
        return asnList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.ASNDao#searchPurgingAsnOrder(com.globaldenso.asia.sps.business.domain.PurgingDODomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurgingAsnDomain> searchPurgingAsnOrder(PurgingDoDomain purgingDoDomain){
        List<PurgingAsnDomain> purgingAsnList = null;
        purgingAsnList = (List<PurgingAsnDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.ASN_SEARCH_PURGING_ASN_ORDER, purgingDoDomain);
        return purgingAsnList;
    }

    // [IN012] For check delete P/O, D/O
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.ASNDao#searchCountNotPurgingAsn(com.globaldenso.asia.sps.business.domain.PurgingDODomain)
     */
    public Integer searchCountNotPurgingAsn(PurgingDoDomain purgingDoDomain){
        int recordCount = Constants.ZERO;
        recordCount = (Integer)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.ASN_SEARCH_COUNT_NOT_PURGING_ASN_ORDER, purgingDoDomain);
        return recordCount;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.ASNDao#searchSumShippingQty(com.globaldenso.asia.sps.business.domain.SpsTAsnDetailDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsTAsnDetailDomain> searchSumShippingQty(SpsTAsnDetailDomain spsTAsnDetailDomain){
        List<SpsTAsnDetailDomain> SpsTAsnDetailList = null;
        SpsTAsnDetailList = (List<SpsTAsnDetailDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.ASN_SEARCH_SUM_SHIPPING_QTY, spsTAsnDetailDomain);
        return SpsTAsnDetailList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.AsnDao#searchSumShippingQtyByPart(com.globaldenso.asia.sps.business.domain.SpsTAsnDetailDomain)
     */
    public SpsTAsnDetailDomain searchSumShippingQtyByPart(
        SpsTAsnDetailDomain spsTAsnDetailDomain) {
        SpsTAsnDetailDomain asnDetailResult = null;
        asnDetailResult = (SpsTAsnDetailDomain)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.ASN_SEARCH_SUM_SHIPPING_QTY_BY_PART, spsTAsnDetailDomain);
        return asnDetailResult;
    }
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceDao#deletePurgingInvoice(String)
     */
    public int deletePurgingAsn(String asnNo)
    {
        int result = Constants.ZERO;
        result += getSqlMapClientTemplate().delete(SqlMapConstants.ASN_DELETE_SPS_T_ASN_DETAIL, asnNo);
        result += getSqlMapClientTemplate().delete(SqlMapConstants.ASN_DELETE_SPS_T_ASN, asnNo);
        return result;
    }
}
