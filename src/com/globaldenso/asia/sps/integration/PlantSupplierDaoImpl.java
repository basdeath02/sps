/*
 * ModifyDate Development company     Describe 
 * 2014/07/23 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.dao.PlantSupplierDao;
import com.globaldenso.asia.sps.business.domain.PlantSupplierDomain;
import com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>The class PlantSupplierDaoImpl implement PlantSupplierDao.</p>
 * <p>DAO for Company Supplier.</p>
 * <ul>
 * <li>Method search  : searchPlantSupplierByRole</li>
 * <li>Method search  : searchPlantSupplierByRelation</li>
 * </ul>
 *
 * @author CSI
 */
public class PlantSupplierDaoImpl extends SqlMapClientDaoSupport implements PlantSupplierDao {

    /** The default constructor. */
    public PlantSupplierDaoImpl() {
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PlantSupplierDao#searchPlantSupplierByRole
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PlantSupplierDomain> searchPlantSupplierByRole(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain){
        List<PlantSupplierDomain> plantSupplierList = null;
        plantSupplierList = (List<PlantSupplierDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PLANT_SUPPLIER_SEARCH_PLANT_SUPPLIER_BY_ROLE, 
                plantSupplierWithScopeDomain);
        return plantSupplierList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PlantSupplierDao#searchPlantSupplierByRelation
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PlantSupplierDomain> searchPlantSupplierByRelation(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain){
        List<PlantSupplierDomain> plantSupplierList = null;
        plantSupplierList = (List<PlantSupplierDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PLANT_SUPPLIER_SEARCH_PLANT_SUPPLIER_BY_RELATION,
                plantSupplierWithScopeDomain);
        return plantSupplierList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PlantSupplierDao#searchPlantSupplierByRole
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PlantSupplierDomain> searchPlantSupplierInformationByRole(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain){
        List<PlantSupplierDomain> plantSupplierList = null;
        plantSupplierList = (List<PlantSupplierDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PLANT_SUPPLIER_SEARCH_PLANT_SUPPLIER_INFORMATION_BY_ROLE, 
                plantSupplierWithScopeDomain);
        return plantSupplierList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PlantSupplierDao#searchPlantSupplierByRelation
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PlantSupplierDomain> searchPlantSupplierInformationByRelation(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain){
        List<PlantSupplierDomain> plantSupplierList = null;
        plantSupplierList = (List<PlantSupplierDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants
                .PLANT_SUPPLIER_SEARCH_PLANT_SUPPLIER_INFORMATION_BY_RELATION,
                plantSupplierWithScopeDomain);
        return plantSupplierList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PlantSupplierDao#searchExistPlantSupplierByRelation
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public Integer searchExistPlantSupplierByRelation(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain){
        Integer recordCount = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.PLANT_SUPPLIER_SEARCH_EXIST_PLANT_SUPPLIER_BY_RELATION,
                plantSupplierWithScopeDomain);
        return recordCount;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PlantSupplierDao#searchExistPlantSupplierByRole
     * (com.globaldenso.asia.sps.business.domain.PlantSupplierWithScopeDomain)
     */
    public Integer searchExistPlantSupplierByRole(
        PlantSupplierWithScopeDomain plantSupplierWithScopeDomain){
        Integer recordCount = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.PLANT_SUPPLIER_SEARCH_EXIST_PLANT_SUPPLIER_BY_ROLE,
                plantSupplierWithScopeDomain);
        return recordCount;
    }

}
