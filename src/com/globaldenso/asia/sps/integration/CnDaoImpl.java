/*
 * ModifyDate Development company     Describe 
 * 2014/07/07 CSI Karnrawee           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.auto.business.domain.SpsTCnDomain;
import com.globaldenso.asia.sps.auto.business.domain.criteria.SpsTCnCriteriaDomain;
import com.globaldenso.asia.sps.business.dao.CnDao;
import com.globaldenso.asia.sps.business.domain.CnInformationDomain;
import com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface CNDaoImpl.</p>
 * <p>Service for CN about search CN information.</p>
 * <ul>
 * <li>Method search  : searchPriceDifferenceInformationDetail</li>
 * <li>Method search  : searchCountCn</li>
 * <li>Method search  : searchCn</li>
 * <li>Method search  : searchCnCoverPage</li>
 * <li>Method search  : searchCnForTransferToJde</li>
 * </ul>
 *
 * @author CSI
 */
public class CnDaoImpl extends SqlMapClientDaoSupport implements CnDao {
    
    /**
     * Instantiates a new File Upload Dao Implement.
     */
    public CnDaoImpl(){
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CNDao#searchPriceDifferenceInformationDetail(
     * com.globaldenso.asia.sps.business.domain.PriceDifferenceInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PriceDifferenceInformationDomain> searchCn(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain) {
        List<PriceDifferenceInformationDomain> result = (
            List<PriceDifferenceInformationDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.CN_SEARCH_CN, priceDifferenceInformationDomain);
        return result;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CNDao#searchCountCN(
     * com.globaldenso.asia.sps.business.domain.searchCountCN)
     */
    public Integer searchCountCn(
        PriceDifferenceInformationDomain priceDifferenceInformationDomain) {
        Integer recordCount = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.CN_SEARCH_COUNT_CN,
                priceDifferenceInformationDomain);
        return recordCount;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CNDao#createCN(
     * com.globaldenso.asia.sps.business.domain.SpsTCnDomain)
     */
    public Integer createCn(SpsTCnDomain spsTCnDomain){
        Integer cnId = null;
        cnId = (Integer)getSqlMapClientTemplate().insert(
            SqlMapConstants.CN_CREATE_CN, spsTCnDomain);
        return cnId;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CNDao#searchCnCoverPage(
     * com.globaldenso.asia.sps.business.domain.CNInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<CnInformationDomain> searchCnCoverPage(CnInformationDomain cnInformationDomain) {
        List<CnInformationDomain> result = null;
        result = (List<CnInformationDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.CN_SEARCH_CN_COVER_PAGE, cnInformationDomain);
        return result;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CNDao#searchCnForTransferToJde(
     * com.globaldenso.asia.sps.auto.business.domain.SpsTCnCriteriaDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<CnInformationDomain> searchCnForTransferToJde(
        SpsTCnCriteriaDomain spsTCnCriteriaDomain) {
        List<CnInformationDomain> result = null;
        result = (List<CnInformationDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.CN_SEARCH_CN_FOR_TRANSFER_TO_JDE, spsTCnCriteriaDomain);
        return result;
    }
}
