package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.auto.business.domain.SpsTFileUploadDomain;
import com.globaldenso.asia.sps.business.dao.FileUploadDao;
import com.globaldenso.asia.sps.business.domain.FileUploadDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>Type in the functional overview of the class.</p>
 *
 * @author CSI
 * @version 1.00
 */
public class FileUploadDaoImpl extends SqlMapClientDaoSupport implements FileUploadDao {

    /**
     * Instantiates a new File Upload Dao Implement.
     */
    public FileUploadDaoImpl(){
        super();
    }
    
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.FileUploadDaoImpl
     * #searchFileUploadInformation(com.globaldenso.asia.sps.business.domain.FileUploadDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsTFileUploadDomain> searchFileUploadInformation(FileUploadDomain fileUploadDomain)
    {
        List<SpsTFileUploadDomain> FileUploadList = null;
        FileUploadList = (List<SpsTFileUploadDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.FILE_UPLOAD_SEARCH_BY_CONDITION_FOR_PAGING, 
                fileUploadDomain);
        return FileUploadList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.FileUploadDaoImpl
     * #searchCount(com.globaldenso.asia.sps.business.domain.FileUploadDomain)
     */
    public Integer searchCount (FileUploadDomain fileUploadDomain)
    {
        Integer rowCount = Constants.ZERO;
        rowCount = (Integer)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.FILE_UPLOAD_SEARCH_COUNT, fileUploadDomain);
        return rowCount;
    }
}


