/*
 * ModifyDate Development company     Describe 
 * 2015/03/11 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain;
import com.globaldenso.asia.sps.business.dao.CigmaDoErrorDao;
import com.globaldenso.asia.sps.business.domain.TransferDoErrorEmailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>The Class CigmaDoErrorDao.</p>
 * <p>For SPS_CIGMA_DO_ERROR.</p>
 * <ul>
 * <li>Method search  : searchTransferDoError</li>
 * </ul>
 *
 * @author CSI
 */
public class CigmaDoErrorDaoImpl extends SqlMapClientDaoSupport implements CigmaDoErrorDao {
    
    /** The default constructor. */
    public CigmaDoErrorDaoImpl() {
        super();
    }
    
    /** {@inheritDoc}
     * @see com.com.globaldenso.asia.sps.business.dao.CigmaDoErrorDao#searchTransferDoError(
     * com.globaldenso.asia.sps.auto.business.domain.SpsCigmaDoErrorDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<TransferDoErrorEmailDomain> searchTransferDoError(
        SpsCigmaDoErrorDomain spsCigmaDoErrorDomain)
    {
        List<TransferDoErrorEmailDomain> result = (List<TransferDoErrorEmailDomain>)
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.CIGMA_DO_ERROR_SEARCH_TRANSFER_DO_ERROR, spsCigmaDoErrorDomain);
        return result;
    }

}
