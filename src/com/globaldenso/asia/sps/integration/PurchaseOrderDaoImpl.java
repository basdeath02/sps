/*
 * ModifyDate Development company     Describe 
 * 2014/07/01 CSI Phakaporn           Create
 * 2016/01/19 CSI Akat                [IN055]
 * 2016/02/24 CSI Akat                [IN054]
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.business.domain.ChangeMaterialReleaseReportDomain;
import com.globaldenso.asia.sps.business.domain.MainScreenResultDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderAcknowledgementDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderNotificationDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderDueDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderReportDomain;
import com.globaldenso.asia.sps.business.domain.PurgingBhtTransmitLogDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;
import com.globaldenso.asia.sps.business.domain.TaskListDomain;
import com.globaldenso.asia.sps.business.dao.PurchaseOrderDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface purchase Order Dao Implement.</p>
 * <p>Service for purchase Order Information about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchPurchaseOrderReport</li>
 * <li>Method search  : searchChangeMaterialReleaseReport</li>
 * <li>Method search  : searchPurchaseOrder</li>
 * <li>Method search  : searchCountPurchaseOrder</li>
 * <li>Method search  : searchPurchaseOrderDetail</li>
 * <li>Method search  : searchPurchaseOrderNotification</li>
 * <li>Method search  : searchPurchaseOrderDue</li>
 * <li>Method search  : searchCountPurchaseOrderDetail</li>
 * <li>Method search  : searchPoFirmWaitToAckForSUser</li>
 * <li>Method search  : searchPoForcastWaitToAckForSUser</li>
 * <li>Method search  : searchPoFirmWaitToAckForDUser</li>
 * <li>Method search  : searchPoForcastWaitToAckForDUser</li>
 * <li>Method search  : searchCountPurchaseOrderHeader</li>
 * <li>Method search  : searchPurchaseOrderHeader</li>
 * <li>Method search  : searchPurgingPurchaseOrder</li>
 * <li>Method search  : searchPurchaseOrderByDo</li>
 * <li>Method create  : createPurchaseOrder</li>
 * <li>Method search  : searchExistPurchaseOrder</li>
 * <li>Method search  : searchChangePurchaseOrder</li>
 * <li>Method search  : searchCountChangePurchaseOrder</li>
 * <li>Method search : searchCountEffectParts</li>
 * <li>Method update : updateRejectNotReplyDue</li>
 * </ul>
 * @author CSI
 * 
 */
public class PurchaseOrderDaoImpl extends SqlMapClientDaoSupport implements PurchaseOrderDao {
    
    /**
     * Instantiates a new Purchase Order Information Dao Implement.
     */
    public PurchaseOrderDaoImpl(){
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderInformationDao#searchPurchaseOrderReport(com.globaldenso.asia.sps.business.domain.PurchaseOrderDetailDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurchaseOrderReportDomain> searchPurchaseOrderReport(
        SpsTPoDomain input) {
        return (List<PurchaseOrderReportDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_REPORT , 
                input);
    }
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderInformationDao#searchChangeMaterialReleaseReport(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<ChangeMaterialReleaseReportDomain> searchChangeMaterialReleaseReport ( 
        PurchaseOrderInformationDomain input) {
        return (List<ChangeMaterialReleaseReportDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PURCHASE_ORDER_SEARCH_CHANGE_MATERIAL_RELEASE_REPORT,
                input);
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderInformationDao#searchPurchaseOrder(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurchaseOrderInformationDomain> searchPurchaseOrder(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
    {
        List<PurchaseOrderInformationDomain> result = 
            (List<PurchaseOrderInformationDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.PURCHASE_ORDER_SEARCH_PURCHASE_ORDER, 
                    purchaseOrderInformationDomain);
        return result;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderInformationDao#searchCountPurchaseOrder(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public Integer searchCountPurchaseOrder(PurchaseOrderInformationDomain 
        purchaseOrderInformationDomain)
    {
        Integer count = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.PURCHASE_ORDER_SEARCH_COUNT_PURCHASE_ORDER, 
                purchaseOrderInformationDomain);
        return count;
    } 
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderInformationDao#searchPurchaseOrderDetail(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurchaseOrderDetailDomain> searchPurchaseOrderDetail(
        PurchaseOrderAcknowledgementDomain purchaseOrderAcknowledgementDomain)
    {
        List<PurchaseOrderDetailDomain> result = 
            (List<PurchaseOrderDetailDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_DETAIL, 
                    purchaseOrderAcknowledgementDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderInformationDao#searchPurchaseOrderNotification(com.globaldenso.asia.sps.business.domain.PurchaseOrderNotificationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurchaseOrderNotificationDomain> searchPurchaseOrderNotification(
        PurchaseOrderNotificationDomain purchaseOrderNotificationDomain)
    {
        List<PurchaseOrderNotificationDomain> result = 
            (List<PurchaseOrderNotificationDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_NOTIFICATION, 
                    purchaseOrderNotificationDomain);
        return result;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderInformationDao#searchPurchaseOrderDue(com.globaldenso.asia.sps.business.domain.PurchaseOrderDueDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurchaseOrderDueDomain> searchPurchaseOrderDue(
        PurchaseOrderDueDomain purchaseOrderDueDomain)
    {
        List<PurchaseOrderDueDomain> result = 
            (List<PurchaseOrderDueDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_DUE, 
                    purchaseOrderDueDomain);
        return result;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderInformationDao#searchCountPurchaseOrderDetail(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public Integer searchCountPurchaseOrderDetail(PurchaseOrderAcknowledgementDomain 
        purchaseOrderAcknowledgementDomain)
    {
        Integer count = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.PURCHASE_ORDER_SEARCH_COUNT_PURCHASE_ORDER_DETAIL, 
                purchaseOrderAcknowledgementDomain);
        return count;
    } 

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderInformationDao#searchCountPurchaseOrderDetailPending(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public Integer searchCountPurchaseOrderDetailPending(PurchaseOrderAcknowledgementDomain 
        purchaseOrderAcknowledgementDomain)
    {
        Integer count = (Integer)getSqlMapClientTemplate()
            .queryForObject(
                SqlMapConstants.PURCHASE_ORDER_SEARCH_COUNT_PURCHASE_ORDER_DETAIL_PENDING, 
                purchaseOrderAcknowledgementDomain);
        return count;
    } 

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#searchPoFirmWaitToAckForSUser(com.globaldenso.asia.sps.business.domain.TaskListDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<MainScreenResultDomain> searchPoFirmWaitToAckForSUser(
        TaskListDomain taskListCriteriaDomain)
    {
        List<MainScreenResultDomain> mainScreenResultDomain = (List<MainScreenResultDomain>) 
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.PURCHASE_ORDER_SEARCH_PO_FIRM_WAIT_TO_ACK_FOR_S_USER, 
                taskListCriteriaDomain);
        
        return mainScreenResultDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#searchPoForcastWaitToAckForSUser(com.globaldenso.asia.sps.business.domain.TaskListDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<MainScreenResultDomain> searchPoForcastWaitToAckForSUser(
        TaskListDomain taskListCriteriaDomain)
    {
        List<MainScreenResultDomain> mainScreenResultDomain = (List<MainScreenResultDomain>) 
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.PURCHASE_ORDER_SEARCH_PO_FORCAST_WAIT_TO_ACK_FOR_S_USER, 
                taskListCriteriaDomain);
        
        return mainScreenResultDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#searchPoFirmWaitToAckForDUser(com.globaldenso.asia.sps.business.domain.TaskListDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<MainScreenResultDomain> searchPoFirmWaitToAckForDUser(
        TaskListDomain taskListCriteriaDomain)
    {
        List<MainScreenResultDomain> mainScreenResultDomain = (List<MainScreenResultDomain>) 
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.PURCHASE_ORDER_SEARCH_PO_FIRM_WAIT_TO_ACK_FOR_D_USER, 
                taskListCriteriaDomain);
        
        return mainScreenResultDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#searchPoForcastWaitToAckForDUser(com.globaldenso.asia.sps.business.domain.TaskListDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<MainScreenResultDomain> searchPoForcastWaitToAckForDUser(
        TaskListDomain taskListCriteriaDomain) {
        List<MainScreenResultDomain> mainScreenResultDomain = (List<MainScreenResultDomain>) 
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.PURCHASE_ORDER_SEARCH_PO_FORCAST_WAIT_TO_ACK_FOR_D_USER, 
                taskListCriteriaDomain);
        
        return mainScreenResultDomain;
    }
    
    //[IN072]
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#searchPoForcastDensoAcceptForSUser(com.globaldenso.asia.sps.business.domain.TaskListDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<MainScreenResultDomain> searchPoForcastDensoAcceptForSUser(
        TaskListDomain taskListCriteriaDomain)
    {
        List<MainScreenResultDomain> mainScreenResultDomain = (List<MainScreenResultDomain>) 
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.PURCHASE_ORDER_SEARCH_PO_FORCAST_ACCEPT_FOR_S_USER, 
                taskListCriteriaDomain);
        
        return mainScreenResultDomain;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#searchPoForcastDensoRejectForSUser(com.globaldenso.asia.sps.business.domain.TaskListDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<MainScreenResultDomain> searchPoForcastDensoRejectForSUser(
        TaskListDomain taskListCriteriaDomain)
    {
        List<MainScreenResultDomain> mainScreenResultDomain = (List<MainScreenResultDomain>) 
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.PURCHASE_ORDER_SEARCH_PO_FORCAST_REJECT_FOR_S_USER, 
                taskListCriteriaDomain);
        
        return mainScreenResultDomain;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#searchPoForcastDensoPendingForDUser(com.globaldenso.asia.sps.business.domain.TaskListDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<MainScreenResultDomain> searchPoForcastDensoPendingForDUser(
        TaskListDomain taskListCriteriaDomain)
    {
        List<MainScreenResultDomain> mainScreenResultDomain = (List<MainScreenResultDomain>) 
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.PURCHASE_ORDER_SEARCH_PO_FORCAST_PENDING_WAIT_D_USER, 
                taskListCriteriaDomain);
        
        return mainScreenResultDomain;
    }
    
    

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#searchCountPurchaseOrderHeader(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public Integer searchCountPurchaseOrderHeader(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
    {
        Integer count = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.PURCHASE_ORDER_SEARCH_COUNT_PURCHASE_ORDER_HEADER, 
                purchaseOrderInformationDomain);
        return count;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#searchPurchaseOrderHeader(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurchaseOrderInformationDomain> searchPurchaseOrderHeader(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
    {
        List<PurchaseOrderInformationDomain> result = 
            (List<PurchaseOrderInformationDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_HEADER, 
                    purchaseOrderInformationDomain);
        return result;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#searchPurgingPurchaseOrder(com.globaldenso.asia.sps.business.domain.PurgingPODomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurgingPoDomain> searchPurgingPurchaseOrder(PurgingPoDomain purgeDataProcess)
    {
        List<PurgingPoDomain> purgingPOList = null;
        purgingPOList = (List<PurgingPoDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PURCHASE_ORDER_SEARCH_PURGING_PURCHASE_ORDER,
                purgeDataProcess);
        return purgingPOList;
    } 
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#searchPurchaseOrderByDo(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurchaseOrderInformationDomain> searchPurchaseOrderByDo(
        PurchaseOrderInformationDomain criteria) {
        List<PurchaseOrderInformationDomain> poInfoList = null;
        poInfoList = (List<PurchaseOrderInformationDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_BY_DO, criteria);
        return poInfoList;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#createPurchaseOrder(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain)
     */
    public BigDecimal createPurchaseOrder(SpsTPoDomain tpoDomain) {
        return (BigDecimal)getSqlMapClientTemplate()
            .insert(SqlMapConstants.PURCHASE_ORDER_CREATE_PURCHASE_ORDER, tpoDomain );
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#searchExistPurchaseOrder(com.globaldenso.asia.sps.business.domain.PurchaseOrderDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsTPoDomain> searchExistPurchaseOrder(PurchaseOrderDomain poDomain) {
        return (List<SpsTPoDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PURCHASE_ORDER_SEARCH_EXIST_PURCHASE_ORDER, poDomain );
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderInformationDao#searchChangePurchaseOrder(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurchaseOrderInformationDomain> searchChangePurchaseOrder(
        PurchaseOrderInformationDomain purchaseOrderInformationDomain)
    {
        List<PurchaseOrderInformationDomain> result = 
            (List<PurchaseOrderInformationDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.PURCHASE_ORDER_SEARCH_CHANGE_PURCHASE_ORDER, 
                    purchaseOrderInformationDomain);
        return result;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderInformationDao#searchCountChangePurchaseOrder(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    public Integer searchCountChangePurchaseOrder(PurchaseOrderInformationDomain 
        purchaseOrderInformationDomain)
    {
        Integer count = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.PURCHASE_ORDER_SEARCH_CHANGE_COUNT_PURCHASE_ORDER, 
                purchaseOrderInformationDomain);
        return count;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderInformationDao#searchPurchaseOrderKanbanReport(com.globaldenso.asia.sps.business.domain.PurchaseOrderDetailDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurchaseOrderReportDomain> searchPurchaseOrderKanbanReport(
        SpsTPoDomain input) {
        return (List<PurchaseOrderReportDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PURCHASE_ORDER_SEARCH_PURCHASE_ORDER_KANBAN_REPORT , 
                input);
    }
    
    // [IN055] Validate Supplier Information before delete
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.service.PurchaseOrderService#searchCountEffectParts(com.globaldenso.asia.sps.business.domain.PurchaseOrderDomain)
     */
    public Integer searchCountEffectParts(PurchaseOrderDomain poDomain) {
        Integer count = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.PURCHASE_ORDER_SEARCH_COUNT_EFFECT_PARTS, 
                poDomain);
        return count;
    }
    
    // [IN054] Update all Supplier Promised Due that not reply to Reject
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderDao#updateRejectNotReplyDue(com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain)
     */
    public Integer updateRejectNotReplyDue( SpsTPoDomain spsTPoDomain ) {
        Integer count = getSqlMapClientTemplate().update(
            SqlMapConstants.PURCHASE_ORDER_UPDATE_REJECT_NOT_REPLY_DUE, spsTPoDomain);
        return count;
    }
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceDao#deletePurgingInvoice(String)
     */
    public int deletePurgingPo(String poId)
    {
        int result = Constants.ZERO;
        result += getSqlMapClientTemplate().delete(SqlMapConstants.PURCHASE_ORDER_DELETE_SPS_T_PO_COVER_PAGE_DETAIL, poId);
        result += getSqlMapClientTemplate().delete(SqlMapConstants.PURCHASE_ORDER_DELETE_SPS_T_PO_COVER_PAGE, poId);
        result += getSqlMapClientTemplate().delete(SqlMapConstants.PURCHASE_ORDER_DELETE_SPS_T_PO_DUE, poId);
        result += getSqlMapClientTemplate().delete(SqlMapConstants.PURCHASE_ORDER_DELETE_SPS_T_PO_DETAIL, poId);
        result += getSqlMapClientTemplate().delete(SqlMapConstants.PURCHASE_ORDER_DELETE_SPS_T_PO, poId);
        return result;
    }

	public Integer deletePurchaseOrder(
			PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria) {
		int result = Constants.ZERO;
		result += getSqlMapClientTemplate().delete(
				SqlMapConstants.PURCHASE_ORDER_DELETE_SPS_T_PO_BY_PURGE,
				purgingBhtTransmitLogCriteria);
		return result;
	}
    
}
