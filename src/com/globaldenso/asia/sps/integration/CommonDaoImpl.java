/*
 * ModifyDate Development company     Describe 
 * 2014/06/12 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;


import java.sql.Timestamp;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.dao.CommonDao;
import com.globaldenso.asia.sps.business.domain.CommonDomain;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface CommonDaoImpl.</p>
 * <p>Manage data of common function.</p>
 * <ul>
 * <li>Method search  : searchSysDate</li>
 * @author CSI
 * 
 */
public class CommonDaoImpl extends SqlMapClientDaoSupport implements CommonDao {
    
    /**
     * Instantiates a new Common Dao Implement.
     */
    public CommonDaoImpl(){
        super();
    }
    
    /**{@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CommonDao#searchSysDate()
     */
    public Timestamp searchSysDate(){
        Timestamp currentDateTime = (Timestamp) getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.COMMON_SEARCH_SYSDATE);
        return currentDateTime;
    }
    
    /**{@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.CommonDao#searchSysDate(com.globaldenso.eps.business.domain.CommonDomain)
     */
    public Timestamp searchSysDate(CommonDomain commonDomain){
        Timestamp currentDateTime = (Timestamp) getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.COMMON_SEARCH_SYSDATE_BY_CONDITION, commonDomain);
        return currentDateTime;
    }
}
