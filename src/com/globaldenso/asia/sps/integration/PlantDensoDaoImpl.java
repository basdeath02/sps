/*
 * ModifyDate Development company     Describe 
 * 2014/07/23 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.dao.PlantDensoDao;
import com.globaldenso.asia.sps.business.domain.PlantDensoDomain;
import com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>The class PlantDensoDaoImpl implement PlantDensoDao.</p>
 * <p>DAO for Plant DENSO.</p>
 * <ul>
 * <li>Method search  : searchPlantDensoByRole</li>
 * <li>Method search  : searchPlantDensoByRelation</li>
 * </ul>
 *
 * @author CSI
 */
public class PlantDensoDaoImpl extends SqlMapClientDaoSupport implements PlantDensoDao {

    /** The default constructor. */
    public PlantDensoDaoImpl(){
        super();
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PlantDensoDao#searchPlantDensoByRole
     * (com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PlantDensoDomain> searchPlantDensoByRole(
            PlantDensoWithScopeDomain plantDensoWithScopeDomain){
        List<PlantDensoDomain> plantDensoList = null;
        plantDensoList = (List<PlantDensoDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PLANT_DENSO_SEARCH_PLANT_DENSO_BY_ROLE, 
                plantDensoWithScopeDomain);
        return plantDensoList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PlantDensoDao#searchPlantDensoByRelation
     * (com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PlantDensoDomain> searchPlantDensoByRelation(
            PlantDensoWithScopeDomain plantDensoWithScopeDomain){
        List<PlantDensoDomain> plantDensoList = null;
        plantDensoList = (List<PlantDensoDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PLANT_DENSO_SEARCH_PLANT_DENSO_BY_RELATION, 
                plantDensoWithScopeDomain);
        return plantDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PlantDensoDao#searchPlantDensoInformationByRole
     * (com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PlantDensoDomain> searchPlantDensoInformationByRole(
            PlantDensoWithScopeDomain plantDensoWithScopeDomain){
        List<PlantDensoDomain> plantDensoList = null;
        plantDensoList = (List<PlantDensoDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PLANT_DENSO_SEARCH_PLANT_DENSO_INFORMATION_BY_ROLE,
                plantDensoWithScopeDomain);
        return plantDensoList;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PlantDensoDao#searchPlantDensoInformationByRelation
     * (com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PlantDensoDomain> searchPlantDensoInformationByRelation(
            PlantDensoWithScopeDomain plantDensoWithScopeDomain){
        List<PlantDensoDomain> plantDensoList = null;
        plantDensoList = (List<PlantDensoDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PLANT_DENSO_SEARCH_PLANT_DENSO_INFORMATION_BY_RELATION, 
                plantDensoWithScopeDomain);
        return plantDensoList;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PlantSupplierDao#searchExistPlantDensoByRelation
     * (com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public Integer searchExistPlantDensoByRelation(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain){
        Integer recordCount = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.PLANT_DENSO_SEARCH_EXIST_PLANT_DENSO_BY_RELATION,
                plantDensoWithScopeDomain);
        return recordCount;
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PlantSupplierDao#searchExistPlantDensoByRole
     * (com.globaldenso.asia.sps.business.domain.PlantDensoWithScopeDomain)
     */
    public Integer searchExistPlantDensoByRole(
        PlantDensoWithScopeDomain plantDensoWithScopeDomain){
        Integer recordCount = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.PLANT_DENSO_SEARCH_EXIST_PLANT_DENSO_BY_ROLE,
                plantDensoWithScopeDomain);
        return recordCount;
    }

}
