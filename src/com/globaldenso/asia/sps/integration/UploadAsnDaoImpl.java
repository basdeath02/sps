/*
 * ModifyDate Development company    Describe 
 * 2014/08/19 CSI Parichat           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.dao.UploadAsnDao;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface UploadAsnDaoImpl.</p>
 * <p>DAO for ASN Uploading about search data from criteria.</p>
 * <ul>
 * <li>Method search  : searchTmpUploadAsn</li>
 * </ul>
 *
 * @author CSI
 */
public class UploadAsnDaoImpl extends SqlMapClientDaoSupport implements UploadAsnDao {
    
    /**
     * Instantiates a new Upload Asn Dao Implement.
     */
    public UploadAsnDaoImpl(){
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.UploadAsnDao#searchTmpUploadAsn(com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<AsnMaintenanceReturnDomain> searchTmpUploadAsn(
        AsnMaintenanceDomain asnMaintenanceDomain){
        List<AsnMaintenanceReturnDomain> asnInformationResultList = null;
        asnInformationResultList = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.UPLOAD_ASN_SEARCH_TMP_UPLOAD_ASN, asnMaintenanceDomain);
        return asnInformationResultList;
    }
}
