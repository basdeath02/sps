/*
 * ModifyDate Development company     Describe 
 * 2014/08/24 CSI Phakaporn           Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;


import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.domain.DataScopeControlDomain;
import com.globaldenso.asia.sps.business.domain.DensoSupplierPartDomain;
import com.globaldenso.asia.sps.business.dao.DensoSupplierPartDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;


/**
 * <p>The Interface DensoSupplierPartDaoImpl.</p>
 * <p>Call SQL statement from sqlMap-DensoSupplierPart.xml to run and return result to 
 * DensoSupplierPartDao</p>
 * <ul>
 * <li>Method search  : searchDensoSupplierPart</li>
 * <li>Method search  : searchDensoSupplierPartByRole</li>
 * <li>Method search  : searchDensoSupplierPartByRelation</li>
 * </ul>
 * @author CSI
 * 
 */
public class DensoSupplierPartDaoImpl extends SqlMapClientDaoSupport
    implements DensoSupplierPartDao {
    
    /**
     * Instantiates a new DENSO Supplier Part Dao Implement.
     */
    public DensoSupplierPartDaoImpl(){
        super();
    }
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DensoSupplierPartDao#searchDensoSupplierPart
     * (com.globaldenso.asia.sps.business.domain.DensoSupplierPartDomain)
     */
    public DensoSupplierPartDomain searchDensoSupplierPart(DensoSupplierPartDomain 
        densoSupplierPartDomain){
        DensoSupplierPartDomain result = (DensoSupplierPartDomain)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.DENSO_SUPPLIER_PART_SEARCH_DENSO_SUPPLIER_PART,
                densoSupplierPartDomain);
        return result;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DensoSupplierPartDao#searchDensoSupplierPartByRole
     * (com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DensoSupplierPartDomain> searchDensoSupplierPartByRole(DataScopeControlDomain 
        dataScopeControlDomain){
        List<DensoSupplierPartDomain> result  = 
            (List<DensoSupplierPartDomain>)getSqlMapClientTemplate().queryForList(
                SqlMapConstants.DENSO_SUPPLIER_PART_SEARCH_DENSO_SUPPLIER_PART_BY_ROLE,
                    dataScopeControlDomain);
        return result;
    }
    
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DensoSupplierPartDao#searchDensoSupplierPartByRelation
     * (com.globaldenso.asia.sps.business.domain.DataScopeControlDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DensoSupplierPartDomain> searchDensoSupplierPartByRelation(DataScopeControlDomain 
        dataScopeControlDomain ){
        List<DensoSupplierPartDomain> result = (List<DensoSupplierPartDomain>)
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.DENSO_SUPPLIER_PART_SEARCH_DENSO_SUPPLIER_PART_BY_RELATION,
                    dataScopeControlDomain);
        return result;
    }

}
