package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.business.dao.PurchaseOrderCoverPageDao;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderCoverPageReportDomain;
import com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>The Interface purchase Order Cover Page Dao Implement.</p>
 * <p>Dao for Purchase Order Cover Page about search data from criteria.</p>
 *
 * <ul>
 * <li>Method search : searchPoCoverPageReport</li>
 * </ul>
 * @author CSI
 */
public class PurchaseOrderCoverPageDaoImpl extends SqlMapClientDaoSupport  implements PurchaseOrderCoverPageDao {

    /**
     * Instantiates a new Purchase Order Cover Page Dao Implement.
     */
    public PurchaseOrderCoverPageDaoImpl() {
        super();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.PurchaseOrderCoverPageDao#searchPoCoverPageReport(com.globaldenso.asia.sps.business.domain.PurchaseOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurchaseOrderCoverPageReportDomain> searchPoCoverPageReport(
        PurchaseOrderInformationDomain input)
    {
        List<PurchaseOrderCoverPageReportDomain> poCoverPageReportList = null;
        poCoverPageReportList = (List<PurchaseOrderCoverPageReportDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.PURCHASE_ORDER_COVER_PAGE_SEARCH_PURCHASE_ORDER_REPORT,
                input);
        return poCoverPageReportList;
    }

}
