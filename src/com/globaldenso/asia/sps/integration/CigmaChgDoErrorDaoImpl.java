/*
 * ModifyDate Development company     Describe 
 * 2015/02/27 CSI Akat                Create
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain;
import com.globaldenso.asia.sps.business.dao.CigmaChgDoErrorDao;
import com.globaldenso.asia.sps.business.domain.TransferChgDoErrorEmailDomain;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;

/**
 * <p>The Class CigmaChgDoErrorDaoImpl.</p>
 * <p>For SPS_CIGMA_CHG_DO_ERROR.</p>
 * <ul>
 * <li>Method search  : searchTransferDoError</li>
 * </ul>
 *
 * @author CSI
 */
public class CigmaChgDoErrorDaoImpl extends SqlMapClientDaoSupport implements CigmaChgDoErrorDao {
    
    /** The default constructor. */
    public CigmaChgDoErrorDaoImpl() {
        super();
    }
    
    /** {@inheritDoc}
     * @see com.com.globaldenso.asia.sps.business.dao.CigmaChgDoErrorDao#searchTransferChgDoError(
     * com.globaldenso.asia.sps.auto.business.domain.SpsCigmaChgDoErrorDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<TransferChgDoErrorEmailDomain> searchTransferChgDoError(
        SpsCigmaChgDoErrorDomain spsCigmaChgDoErrorDomain)
    {
        List<TransferChgDoErrorEmailDomain> result = (List<TransferChgDoErrorEmailDomain>)
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.CIGMA_CHG_DO_ERROR_SEARCH_TRANSFER_CHG_DO_ERROR, 
                spsCigmaChgDoErrorDomain);
        return result;
    }

}
