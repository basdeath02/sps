/*
 * ModifyDate Development company     Describe 
 * 2014/03/03 CSI Phakaporn           Create
 * 2015/08/24 CSI Akat                [IN012]
 * 2015/08/22 Netband U.Rungsiwut     SPS phase II
 * 
 * (c) Copyright Denso All rights reserved.
 */
package com.globaldenso.asia.sps.integration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.globaldenso.ai.common.core.exception.ApplicationException;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDetailDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain;
import com.globaldenso.asia.sps.auto.business.domain.SpsTPoDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationDomain;
import com.globaldenso.asia.sps.business.domain.AcknowledgedDoInformationReturnDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceDomain;
import com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain;
import com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDetailDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderReportDomain;
import com.globaldenso.asia.sps.business.domain.DeliveryOrderSubDetailDomain;
import com.globaldenso.asia.sps.business.domain.DoCreatedAsnDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailDomain;
import com.globaldenso.asia.sps.business.domain.InquiryDoDetailReturnDomain;
import com.globaldenso.asia.sps.business.domain.KanbanDeliveryOrderReportDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDoDetailReturnDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain;
import com.globaldenso.asia.sps.business.domain.KanbanOrderInformationResultDomain;
import com.globaldenso.asia.sps.business.domain.KanbanTagDomain;
import com.globaldenso.asia.sps.business.domain.MainScreenResultDomain;
import com.globaldenso.asia.sps.business.domain.OneWayKanbanTagReportDomain;
import com.globaldenso.asia.sps.business.domain.PurgingBhtTransmitLogDomain;
import com.globaldenso.asia.sps.business.domain.PurgingDoDomain;
import com.globaldenso.asia.sps.business.domain.PurgingPoDomain;
import com.globaldenso.asia.sps.business.domain.ReceiveByScanDomain;
import com.globaldenso.asia.sps.business.domain.TaskListDomain;
import com.globaldenso.asia.sps.business.domain.UrgentOrderDomain;
import com.globaldenso.asia.sps.business.dao.DeliveryOrderDao;
import com.globaldenso.asia.sps.common.constant.Constants;
import com.globaldenso.asia.sps.common.constant.SqlMapConstants;



/**
 * <p>The Interface PODaoImpl.</p>
 * <p>Call SQL statement from sqlMap-DeliveryOrder.xml to run and return result to DeliveryOrderDao</p>
 * <ul>
 * <li>Method search  : searchCountAcknowledgedDo</li>
 * <li>Method search  : searchAcknowledgedDo</li>
 * <li>Method search  : searchCountDeliveryOrder</li>
 * <li>Method search  : searchDeliveryOrder</li>
 * <li>Method search  : searchCountDoDetail</li>
 * <li>Method search  : searchDoDetail</li>
 * <li>Method search  : searchCountKanbanOrder</li>
 * <li>Method search  : searchKanbanOrder</li>
 * <li>Method search  : searchDoGroupAsn</li>
 * <li>Method search  : searchCompleteShipDo</li>
 * <li>Method search  : searchCountKanbanOrderHeader</li>
 * <li>Method search  : searchKanbanOrderHeader</li>
 * <li>Method search  : searchCountDeliveryOrderHeader</li>
 * <li>Method search  : searchDeliveryOrderHeader</li>
 * <li>Method search  : searchUrgentOrderForSupplierUser</li>
 * <li>Method search  : searchExistDo</li>
 * <li>Method search  : searchDeliveryOrderReport</li>
 * <li>Method search  : searchKanbanDeliveryOrderReport</li>
 * <li>Method search  : searchCountBackOrder</li>
 * <li>Method search  : searchBackOrder</li>
 * <li>Method create  : createDeliveryOrder</li>
 * <li>Method search  : searchExistDeliveryOrder</li>
 * <li>Method search  : searchUrgentOrder</li>
 * <li>Method search  : searchDoCreatedAsn</li>
 * <li>Method search  : searchPurgingDeliveryOrder</li>
 * <li>Method search  : searchPurgingUrgentDeliveryOrder</li>
 * </ul>
 * @author CSI
 * 
 */
public class DeliveryOrderDaoImpl extends SqlMapClientDaoSupport implements DeliveryOrderDao {
    
    /**
     * Instantiates a new delivery order dao impl.
     */
    public DeliveryOrderDaoImpl(){
        super();
    }
    
    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchCountAcknowledgedDO(com.globaldenso.AcknowledgedDOInformationDomain.business.domain.AcknowledgedDOInformationDomain)
     */
    public Integer searchCountAcknowledgedDo(
        AcknowledgedDoInformationDomain acknowledgedDoInformationDomain)
    {
        Integer rowCount = Constants.ZERO;
        rowCount = (Integer)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.DELIVERY_ORDER_SEARCH_COUNT_ACKNOWLEDGED_DO,
            acknowledgedDoInformationDomain);
        return rowCount;
    }

    /** {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchAcknowledgedDO(com.globaldenso.AcknowledgedDOInformationDomain.business.domain.AcknowledgedDOInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<AcknowledgedDoInformationReturnDomain> searchAcknowledgedDo(
        AcknowledgedDoInformationDomain acknowledgedDoInformationDomain)
    {
        List<AcknowledgedDoInformationReturnDomain> acknowledgedDOInfoResultList = 
            new ArrayList<AcknowledgedDoInformationReturnDomain>();
        acknowledgedDOInfoResultList = (List<AcknowledgedDoInformationReturnDomain>)
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.DELIVERY_ORDER_SEARCH_ACKNOWLEDGED_DO,
                acknowledgedDoInformationDomain);
        return acknowledgedDOInfoResultList;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchCountDeliveryOrder(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public Integer searchCountDeliveryOrder(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
    {
        Integer rowCount = Constants.ZERO;
        rowCount = (Integer)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.DELIVERY_ORDER_SEARCH_COUNT_DELIVERY_ORDER, 
            deliveryOrderInformationDomain);
        return rowCount;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchDeliveryOrder(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DeliveryOrderDetailDomain> searchDeliveryOrder(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain) 
    {
        List<DeliveryOrderDetailDomain> deliveryOrderDetailDomain = 
            new ArrayList<DeliveryOrderDetailDomain>();
        deliveryOrderDetailDomain = (List<DeliveryOrderDetailDomain>) 
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.DELIVERY_ORDER_SEARCH_DELIVERY_ORDER, 
                deliveryOrderInformationDomain);
        return deliveryOrderDetailDomain;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchCountDoDetail(com.globaldenso.asia.sps.business.domain.InquiryDODetailDomain)
     */
    public Integer searchCountDoDetail(
        InquiryDoDetailDomain inquiryDoDetailDomain) {
        Integer rowCountDo = Constants.ZERO;
        rowCountDo = (Integer)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.DELIVERY_ORDER_SEARCH_COUNT_DO_DETAIL, inquiryDoDetailDomain);
        return rowCountDo;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchDODetail(com.globaldenso.asia.sps.business.domain.InquiryDODetailDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<InquiryDoDetailReturnDomain> searchDoDetail(
        InquiryDoDetailDomain inquiryDoDetailDomain)
    {
        List<InquiryDoDetailReturnDomain> inquiryDoDetailResultList = null;
        inquiryDoDetailResultList = getSqlMapClientTemplate().queryForList(
            SqlMapConstants.DELIVERY_ORDER_SEARCH_DO_DETAIL, inquiryDoDetailDomain);
        return inquiryDoDetailResultList;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchCountKANBANOrder(com.globaldenso.asia.sps.business.domain.KANBANOrderInformationDomain)
     */
    public Integer searchCountKanbanOrder(
        KanbanOrderInformationDomain kanbanOrderInformationDomain) 
    {
        Integer rowCount = Constants.ZERO;
        rowCount = (Integer)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.DELIVERY_ORDER_SEARCH_COUNT_KANBAN_ORDER,
            kanbanOrderInformationDomain);
        return rowCount;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchKanbanOrder(com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<KanbanOrderInformationResultDomain> searchKanbanOrder(
        KanbanOrderInformationDomain kanbanOrderInformationDomain) 
    {
        List<KanbanOrderInformationResultDomain> kanbanOrderInformationResultDomain = null;
        kanbanOrderInformationResultDomain 
            = (List<KanbanOrderInformationResultDomain>)getSqlMapClientTemplate()
                .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_KANBAN_ORDER, 
                    kanbanOrderInformationDomain);
        return kanbanOrderInformationResultDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchDoGroupAsn(com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<AsnMaintenanceReturnDomain> searchDoGroupAsn(
        AsnMaintenanceDomain asnMaintenanceDomain) {
        List<AsnMaintenanceReturnDomain> doGroupAsnResultList = null;
        doGroupAsnResultList = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_DO_GROUP_ASN,
                asnMaintenanceDomain);
        return doGroupAsnResultList;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchCompleteShipDo(com.globaldenso.asia.sps.business.domain.AsnMaintenanceReturnDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<AsnMaintenanceReturnDomain> searchCompleteShipDo(
        AsnMaintenanceDomain asnMaintenanceDomain)
    {
        List<AsnMaintenanceReturnDomain> deliveryOrderList = null;
        deliveryOrderList = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_COMPLETE_SHIP_DO,
                asnMaintenanceDomain);
        return deliveryOrderList;
    }

    // [IN012] : search for number of P/No. in D/O group by P/No. Ship Status
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchDistinctPnStatus(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<String> searchDistinctPnStatus(
        SpsTDoDomain doDomain)
    {
        List<String> result = null;
        result = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_DISTINCT_PN_STATUS,
                doDomain);
        return result;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchCountKanbanOrderHeader(com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain)
     */
    public Integer searchCountKanbanOrderHeader(
        KanbanOrderInformationDomain kanbanOrderInformationDomain)
    {
        Integer rowCount = Constants.ZERO;
        rowCount = (Integer)getSqlMapClientTemplate()
            .queryForObject(SqlMapConstants.DELIVERY_ORDER_SEARCH_COUNT_KANBAN_ORDER_HEADER,
                kanbanOrderInformationDomain);
        return rowCount;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchKanbanOrderHeader(com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<KanbanOrderInformationResultDomain> searchKanbanOrderHeader(
        KanbanOrderInformationDomain kanbanOrderInformationDomain) 
    {
        List<KanbanOrderInformationResultDomain> kanbanOrderInformationResultDomain = null;
        kanbanOrderInformationResultDomain = (List<KanbanOrderInformationResultDomain>)
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.DELIVERY_ORDER_SEARCH_KANBAN_ORDER_HEADER,
                kanbanOrderInformationDomain);
    
        return kanbanOrderInformationResultDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchCountDeliveryOrderHeader(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public Integer searchCountDeliveryOrderHeader(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain) 
    {
        Integer rowCount = Constants.ZERO;
        rowCount = (Integer)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.DELIVERY_ORDER_SEARCH_COUNT_DELIVERY_ORDER_HEADER,
            deliveryOrderInformationDomain);
        return rowCount;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchDeliveryOrderHeader(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DeliveryOrderDetailDomain> searchDeliveryOrderHeader(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain) 
    {
        List<DeliveryOrderDetailDomain> deliveryOrderDetailDomain 
            = new ArrayList<DeliveryOrderDetailDomain>();
        deliveryOrderDetailDomain = (List<DeliveryOrderDetailDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_HEADER,
                deliveryOrderInformationDomain);
        return deliveryOrderDetailDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchUrgentOrderForSupplierUser(com.globaldenso.asia.sps.business.domain.TaskListDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<MainScreenResultDomain> searchUrgentOrderForSupplierUser(
        TaskListDomain taskListCriteriaDomain)
    {
        List<MainScreenResultDomain> mainScreenResultDomain = null; 
        mainScreenResultDomain = (List<MainScreenResultDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_URGENT_ORDER_FOR_SUPPLIER_USER,
                taskListCriteriaDomain);
        return mainScreenResultDomain;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchExistDo(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DeliveryOrderDetailDomain> searchExistDo(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain)
    {
        List<DeliveryOrderDetailDomain> deliveryOrderDetailResultList = null;
        deliveryOrderDetailResultList = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_EXIST_DO,
                deliveryOrderInformationDomain);
        return deliveryOrderDetailResultList;
    }
  
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchDeliveryOrderReport(com.globaldenso.asia.sps.business.domain.DeliveryOrderSubDetailDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DeliveryOrderReportDomain> searchDeliveryOrderReport(
        DeliveryOrderSubDetailDomain deliveryOrderSubDetailDomain)
    {
        List<DeliveryOrderReportDomain> deliveryOrderReportDomain = 
            new ArrayList<DeliveryOrderReportDomain>();
        deliveryOrderReportDomain = (List<DeliveryOrderReportDomain>)
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_REPORT,
                deliveryOrderSubDetailDomain);
        
        return deliveryOrderReportDomain;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchKanbanDeliveryOrderReport(com.globaldenso.asia.sps.business.domain.KanbanOrderInformationDODetailReturnDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<KanbanDeliveryOrderReportDomain> searchKanbanDeliveryOrderReport(
        KanbanOrderInformationDoDetailReturnDomain kanbanOrderInformationDODetailReturnDomain)
    {
        List<KanbanDeliveryOrderReportDomain> kanbanDeliveryOrderReportDomain =
            new ArrayList<KanbanDeliveryOrderReportDomain>();
        kanbanDeliveryOrderReportDomain = (List<KanbanDeliveryOrderReportDomain>)
            getSqlMapClientTemplate().queryForList(
                SqlMapConstants.DELIVERY_ORDER_SEARCH_KANBAN_DELOVERY_ORDER_REPORT,
                kanbanOrderInformationDODetailReturnDomain);
        return kanbanDeliveryOrderReportDomain;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchCountBackOrder(com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain)
     */
    public Integer searchCountBackOrder(BackOrderInformationDomain backOrderInformationDomain)
    {
        Integer rowCount = Constants.ZERO;
        rowCount = (Integer)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.DELIVERY_ORDER_SEARCH_COUNT_BACK_ORDER, backOrderInformationDomain);
        return rowCount;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchBackOrder(com.globaldenso.asia.sps.business.domain.BackOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<BackOrderInformationDomain> searchBackOrder(
        BackOrderInformationDomain backOrderInformationDomain)
    {
        List<BackOrderInformationDomain> backOrderInformationList = null;
        backOrderInformationList = (List<BackOrderInformationDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_BACK_ORDER,
                backOrderInformationDomain);
        return backOrderInformationList;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#createDeliveryOrder(com.globaldenso.asia.sps.auto.business.domain.SpsTDoDomain)
     */
    public BigDecimal createDeliveryOrder(SpsTDoDomain domain)
    {
        BigDecimal recordCount = new BigDecimal(Constants.ZERO);
        recordCount = (BigDecimal)getSqlMapClientTemplate()
            .insert(SqlMapConstants.DELIVERY_ORDER_CREATE_DELIVERY_ORDER, domain);
        return recordCount;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchExistDeliveryOrder(java.util.Map)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsTDoDomain> searchExistDeliveryOrder(DeliveryOrderDomain parameter){
        List<SpsTDoDomain> doResultList = null;
        doResultList = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_EXIST_DELIVERY_ORDER, parameter);
        return doResultList;
    }
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchUrgentOrder(com.globaldenso.asia.sps.business.domain.UrgentOrderDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DeliveryOrderDomain> searchUrgentOrder(UrgentOrderDomain criteria){
        List<DeliveryOrderDomain> deliveryOrderList = null;
        deliveryOrderList = (List<DeliveryOrderDomain>)getSqlMapClientTemplate().queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_URGENT_ORDER, criteria);
        return deliveryOrderList;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchDoCreatedAsn(com.globaldenso.asia.sps.business.domain.DeliveryOrderDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DoCreatedAsnDomain> searchDoCreatedAsn(DeliveryOrderDomain criteria){
        List<DoCreatedAsnDomain> asnResultList = null;
        asnResultList = (List<DoCreatedAsnDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_DO_CREATED_ASN, criteria);
        return asnResultList;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchPurgingDeliveryOrder(com.globaldenso.asia.sps.business.domain.PurgingPoDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurgingDoDomain> searchPurgingDeliveryOrder(PurgingPoDomain purgingPoDomain){
        List<PurgingDoDomain> deliveryOrderList = null;
        deliveryOrderList = (List<PurgingDoDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_PURGING_DO, purgingPoDomain);
        return deliveryOrderList;
    }

    // [IN012] for check delete P/O
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchCountNotPurgingDeliveryOrder(com.globaldenso.asia.sps.business.domain.PurgingPoDomain)
     */
    public Integer searchCountNotPurgingDeliveryOrder(PurgingPoDomain purgingPoDomain){
        Integer rowCount = Constants.ZERO;
        rowCount = (Integer)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.DELIVERY_ORDER_SEARCH_COUNT_NOT_PURGING_DO, purgingPoDomain);
        return rowCount;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchPurgingUrgentDeliveryOrder(com.globaldenso.asia.sps.business.domain.PurgingPoDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<PurgingDoDomain> searchPurgingUrgentDeliveryOrder(
        PurgingPoDomain purgingPoDomain) {
        List<PurgingDoDomain> purgingUrgentDoList = null;
        purgingUrgentDoList = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_PURGING_URGENT_DO, purgingPoDomain);
        return purgingUrgentDoList;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchDeliveryOrderAcknowledgement(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public SpsTPoDomain searchDeliveryOrderAcknowledgement(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain) {
        SpsTPoDomain poDomain = null;
        poDomain = (SpsTPoDomain)getSqlMapClientTemplate().queryForObject(
            SqlMapConstants.DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_ACKNOWLEDGEMENT,
            deliveryOrderInformationDomain);
        return poDomain;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#updateDateTimeKanbanPdf(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public Integer updateDateTimeKanbanPdf(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain) {
        Integer resultCode = getSqlMapClientTemplate().update(
            SqlMapConstants.UPDATE_DATE_TIME_KANBAN_PDF, deliveryOrderInformationDomain);
        return resultCode;
    }
    

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchOneWayKanbanTagReportInformation(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<OneWayKanbanTagReportDomain> searchOneWayKanbanTagReportInformation(
        DeliveryOrderInformationDomain deliveryOrderInformationDomain) {
        List<OneWayKanbanTagReportDomain> OneWayKanbanTagReportResult = null;
        OneWayKanbanTagReportResult = (List<OneWayKanbanTagReportDomain>)getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_ONE_WAY_KANBAN_TAG_REPORT, deliveryOrderInformationDomain);
        return OneWayKanbanTagReportResult;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchNotifyDeliveryOrderToSupplier(com.globaldenso.asia.sps.business.domain.SpsTDoDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsTDoDomain> searchNotifyDeliveryOrderToSupplier(SpsTDoDomain spsTDoDomain) {
        List<SpsTDoDomain> spsTDoDomainList = null;
        spsTDoDomainList = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_NOTIFY_DELIVERY_ORDER_TO_SUPPLIER, spsTDoDomain);
        return spsTDoDomainList;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#createDeliveryOrder(com.globaldenso.asia.sps.auto.business.domain.KanbanTagDomain)
     */
    public BigDecimal createKanbanTag(KanbanTagDomain domain)
    {
    	BigDecimal recordCount = new BigDecimal(Constants.ZERO);
    		recordCount = (BigDecimal)getSqlMapClientTemplate()
    	            .insert(SqlMapConstants.DELIVERY_ORDER_CREATE_KANBAN_TAG, domain);
        return recordCount;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#updateDateTimeKanbanPdf(com.globaldenso.asia.sps.business.domain.DeliveryOrderInformationDomain)
     */
    public Integer updateOneWayKanbanTagFlag(
        KanbanTagDomain domain) {
        Integer resultCode = getSqlMapClientTemplate().update(
            SqlMapConstants.DELIVERY_ORDER_UPDATE_ONE_WAY_KANBAN_TAG_FLAG, domain);
        return resultCode;
    }
    
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchBatchNumber(void)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public Integer searchBatchNumber() {
        List<Integer> resultCode = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_BATCH_NO);
        return resultCode.get(Constants.ZERO).intValue();
    }
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#createTransmitLog(com.globaldenso.asia.sps.auto.business.domain.ReceiveByScanDomain)
     */
    public BigDecimal createTransmitLog(ReceiveByScanDomain receiveByScanDomain)
    {
        BigDecimal recordCount = new BigDecimal(Constants.ZERO);
        recordCount = (BigDecimal)getSqlMapClientTemplate()
            .insert(SqlMapConstants.DELIVERY_ORDER_CREATE_TRANSMIT_LOG, receiveByScanDomain);
        return recordCount;
    }

    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#isExistTransmitLog(com.globaldenso.asia.sps.business.domain.ReceiveByScanDomain)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<ReceiveByScanDomain> isExistTransmitLog(ReceiveByScanDomain receiveByScanDomain)
        throws ApplicationException {
//        Integer rowCount = Constants.ZERO;
//        rowCount = (List<ReceiveByScanDomain>)getSqlMapClientTemplate().queryForObject(
//            SqlMapConstants.DELIVERY_ORDER_SEARCH_IS_EXIST_TRANSMIT_LOG, receiveByScanDomain);
        List<ReceiveByScanDomain> receiveByScanDomainList = null;
        receiveByScanDomainList = getSqlMapClientTemplate().queryForList(
            SqlMapConstants.DELIVERY_ORDER_SEARCH_IS_EXIST_TRANSMIT_LOG, receiveByScanDomain);
        return receiveByScanDomainList;
    }
    /**
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.InvoiceDao#deletePurgingInvoice(String)
     */
    public int deletePurgingDo(String doId)
    {
        int result = Constants.ZERO;
        result += getSqlMapClientTemplate().delete(SqlMapConstants.DELIVERY_ORDER_DELETE_SPS_T_DO_KANBAN_SEQ, doId);
        result += getSqlMapClientTemplate().delete(SqlMapConstants.DELIVERY_ORDER_DELETE_SPS_T_SPS_T_KANBAN_TAG, doId);
        result += getSqlMapClientTemplate().delete(SqlMapConstants.DELIVERY_ORDER_DELETE_SPS_T_DO_DETAIL, doId);
        result += getSqlMapClientTemplate().delete(SqlMapConstants.DELIVERY_ORDER_DELETE_SPS_T_DO, doId);
        return result;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchExistDeliveryOrderBackOrder(java.util.Map)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsTDoDomain> searchExistDeliveryOrderBackOrder(DeliveryOrderDomain parameter){
        List<SpsTDoDomain> doResultList = null;
        doResultList = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_BACK_ORDER_HEADER, parameter);
        return doResultList;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#searchExistDeliveryOrderDetailBackOrder(java.util.Map)
     */
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<SpsTDoDetailDomain> searchExistDeliveryOrderBackOrderDetail(DeliveryOrderDomain parameter){
        List<SpsTDoDetailDomain> doResultList = null;
        doResultList = getSqlMapClientTemplate()
            .queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_DELIVERY_ORDER_BACK_ORDER_DETAIL, parameter);
        return doResultList;
    }
    
    public Integer updateBackOrderDetail(SpsTDoDetailDomain parameters){
        Integer result = getSqlMapClientTemplate().update(SqlMapConstants.DELIVERY_ORDER_UPDATE_BACK_ORDER_DETAIL, parameters); 
        return result;
    }
    
    public Integer updateBackOrderHeader(SpsTDoDomain parameters){
        Integer result = getSqlMapClientTemplate().update(SqlMapConstants.DELIVERY_ORDER_UPDATE_BACK_ORDER_HEADER, parameters); 
        return result;
    }
    
    @SuppressWarnings(Constants.SUPPRESS_WARNINGS_UNCHECKED)
    public List<DeliveryOrderDomain> searchMailBackOrder(SpsTDoDetailDomain criteria){
        List<DeliveryOrderDomain> deliveryOrderList = null;
        deliveryOrderList = (List<DeliveryOrderDomain>)getSqlMapClientTemplate().queryForList(SqlMapConstants.DELIVERY_ORDER_SEARCH_MAIL_BACKORDER, criteria);
        return deliveryOrderList;
    }

	public Integer deleteBhtTransmitLog(
			PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria) {
		int result = Constants.ZERO;
		result += getSqlMapClientTemplate().delete(
				SqlMapConstants.DELIVERY_ORDER_DELETE_SPS_T_BHT_TRANSMIT_LOG_BY_PURGE,
				purgingBhtTransmitLogCriteria);
		return result;
	}

	public Integer deleteDeliveryOrder(
			PurgingBhtTransmitLogDomain purgingBhtTransmitLogCriteria) {
		int result = Constants.ZERO;
		result += getSqlMapClientTemplate().delete(
				SqlMapConstants.DELIVERY_ORDER_DELETE_SPS_T_DO_BY_PURGE,
				purgingBhtTransmitLogCriteria);
		return result;
	}
	
	/**
     * 
     * {@inheritDoc}
     * @see com.globaldenso.asia.sps.business.dao.DeliveryOrderDao#createDeliveryOrder(com.globaldenso.asia.sps.auto.business.domain.KanbanTagDomain)
     */
	/*
    public boolean createKanbanTagList(List<KanbanTagDomain> kanbanList) throws ApplicationException
    {
    	boolean result = false;
    	int check = 0;
      	int rowInsert= 0;
      	SqlMapClient client = getSqlMapClientTemplate().getSqlMapClient();
      	try{
      		client.startTransaction();
      		client.getCurrentConnection().setAutoCommit(false);
      		for (int k=0;k<kanbanList.size();k++){
      			System.out.println("Kanban row : "+k);
      			System.out.println("Kanban DO_ID : "+kanbanList.get(k).getDoId());
      			//rowInsert =client.update("SpsTAsnFromCIGMA.InsertASN", criteria);
      			rowInsert = client.update(SqlMapConstants.DELIVERY_ORDER_CREATE_KANBAN_TAG, kanbanList.get(k));
        		//rowInsert = client.insert(SqlMapConstants.DELIVERY_ORDER_CREATE_KANBAN_TAG, kanbanList.get(k));
    			if(rowInsert<0){
    				check++;
    			}
        	}
        	
        	if(check==0){
    			rowInsert = 1;
    			result = true;
    			client.getCurrentConnection().commit();
    		}else{
    			rowInsert = 0;
    			result = false;
    			client.getCurrentConnection().rollback();
    		}
      	}catch (Exception ex){
 		   try{
			   client.getCurrentConnection().rollback();
	            }catch(Exception e){
	            	e.printStackTrace();
	            }
		   ex.printStackTrace();
		   throw new ApplicationException(ex.getMessage());
	   }
      	return result;
    }
    */
	
	public Integer insertKanbanTag(KanbanTagDomain domain)
    {
		Integer recordCount = 0;
    		recordCount = getSqlMapClientTemplate().update(SqlMapConstants.DELIVERY_ORDER_CREATE_KANBAN_TAG, domain);
        return recordCount;
    }
}
