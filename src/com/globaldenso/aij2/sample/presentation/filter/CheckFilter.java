/*
 * PROJECT：xxxxxx
 * 
 * [JP] 不正なリクエストをチェックするためのフィルタクラスです。
 * [EN] Filter class for checking invalid request.
 * 
 * Version.   更新日      更新者        更新内容
 * 1.0.0      2014/05/06  DNITS         新規作成
 * 
 */
package com.globaldenso.aij2.sample.presentation.filter;

import java.io.IOException;
import java.util.Enumeration;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * [JP] 不正なリクエストをチェックするためのフィルタクラスです。<br>
 * [JP] 不正なリクエストを検知した場合、HTTPステータスコード400を返します。<br>
 * [EN] Filter class for checking invalid request.<br>
 * [EN] Return HTTP status code 400 if request is invalid.
 */
public class CheckFilter implements Filter {

    /**
     * [JP] 不正なリクエストとして検知するパターンの定義です。((.*\.|^|.*|\[('|"))(c|C)lass(\.|('|")]|\[).*)<br>
     * [EN] Definition of the pattern to be checked as invalid request.((.*\.|^|.*|\[('|"))(c|C)lass(\.|('|")]|\[).*)
     */
    private static final Pattern EXCLUDE_PARAMS = Pattern
        .compile("(.*\\.|^|.*|\\[(\'|\"))(c|C)lass(\\.|(\'|\")]|\\[).*");

    /**
     * [JP] デフォルトコンストラクタです。<br>
     * [EN] Default constructor.
     */
    public CheckFilter() {
    }

    /**
     * {@inheritDoc}
     * 
     * @see javax.servlet.Filter#destroy()
     */
    public void destroy() {
    }

    /**
     * {@inheritDoc}
     * 
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
     *      javax.servlet.ServletResponse, javax.servlet.FilterChain)
     * @see {@link #doFilter(HttpServletRequest, HttpServletResponse, FilterChain)}
     */
    public void doFilter(ServletRequest request, ServletResponse response,
        FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpreq = (HttpServletRequest)request;
        HttpServletResponse httpres = (HttpServletResponse)response;
        Enumeration<?> params = httpreq.getParameterNames();
        while (params.hasMoreElements()) {
            String paramName = (String)params.nextElement();
            if (isAttack(paramName)) {
                httpres.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
        }
        Cookie[] cookies = httpreq.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                String cookieName = c.getName();
                if (isAttack(cookieName)) {
                    httpres.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
            }
        }
        chain.doFilter(request, response);
    }

    /**
     * [JP] 不正なリクエストが含まれているかチェックします。<br>
     * [EN] Check whether or not target string matches the pattern of invalid request.
     * 
     * @param target [JP] チェック対象の文字列です。
     *               [EN] target string for checking.
     * @return boolean [JP] 不正なリクエストの場合、trueを返します。
     *                 [EN] return true if target string is invalid request.
     */
    private static boolean isAttack(String target) {
        return EXCLUDE_PARAMS.matcher(target).find();
    }

    /**
     * {@inheritDoc}
     * 
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    public void init(FilterConfig config) throws ServletException {
    }

}
