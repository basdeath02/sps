/*
 * PROJECT：xxxxxx
 * 
 * [JP] マルチパートリクエストに不正なリクエストが含まれていないかチェックを追加したクラスです。
 * [EN] Multipart request handler class that is added a function to check invalid request.
 * 
 * Version.   更新日      更新者        更新内容
 * 1.0.0      2014/05/07  DNITS         新規作成
 * 
 */
package com.globaldenso.aij2.sample.presentation.struts.upload;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.struts.upload.CommonsMultipartRequestHandler;

/**
 * [JP] マルチパートリクエストに不正なリクエストが含まれていないかチェックを追加したクラスです。<br>
 * [JP] 不正なリクエストをチェックするためのフィルタクラスのみではマルチパートリクエストの場合にチェックできないため、本クラスでチェックを追加しています。<br>
 * [EN] Multipart request handler class that is added a function to check invalid request.<br>
 * [EN] Filter class is not enough for checking invalid request if the request is multipart request. So this class check the request.
 */
public class MultipartRequestCheckHandler extends CommonsMultipartRequestHandler {

    /**
     * [JP] 不正なリクエストとして検知するパターンの定義です。((.*\.|^|.*|\[('|"))(c|C)lass(\.|('|")]|\[).*)<br>
     * [EN] Definition of the pattern to be checked as invalid request.((.*\.|^|.*|\[('|"))(c|C)lass(\.|('|")]|\[).*)
     */
    private static final Pattern EXCLUDE_PARAMS = Pattern
        .compile("(.*\\.|^|.*|\\[(\'|\"))(c|C)lass(\\.|(\'|\")]|\\[).*");

    /**
     * [JP] デフォルトコンストラクタです。<br>
     * [EN] Default constructor.
     */
    public MultipartRequestCheckHandler() {
    }

    /**
     * {@inheritDoc}
     */
    protected void addTextParameter(HttpServletRequest request, FileItem item) {
        String fieldName = item.getFieldName();
        if (isAttack(fieldName)) {
            throw new IllegalArgumentException();
        }
        super.addTextParameter(request, item);
    }

    /**
     * {@inheritDoc}
     */
    protected void addFileParameter(FileItem item) {
        String fieldName = item.getFieldName();
        if (isAttack(fieldName)) {
            throw new IllegalArgumentException();
        }
        super.addFileParameter(item);
    }

    /**
     * [JP] 不正なリクエストが含まれているかチェックします。<br>
     * [EN] Check whether or not target string matches the pattern of invalid request.
     * 
     * @param target [JP] チェック対象の文字列です。
     *               [EN] target string for checking.
     * @return boolean [JP] 不正なリクエストの場合、trueを返します。
     *                 [EN] return true if target string is invalid request.
     */
    private static boolean isAttack(String target) {
        return EXCLUDE_PARAMS.matcher(target).find();
    }

}
