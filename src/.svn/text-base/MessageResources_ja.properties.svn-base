################################################################################
#
#   MessageResources.properties
#
# [JP] \u30e1\u30c3\u30bb\u30fc\u30b8\u3092\u6271\u3046\u30d7\u30ed\u30d1\u30c6\u30a3\u3067\u3059\u3002(\u82f1\u8a9e\u7248)
# [JP] \u30e1\u30c3\u30bb\u30fc\u30b8\u30a8\u30ea\u30a2\u304a\u3088\u3073\u30ed\u30b0\u306b\u51fa\u529b\u3059\u308b\u30e1\u30c3\u30bb\u30fc\u30b8\u3092\u5b9a\u7fa9\u3057\u3066\u3044\u307e\u3059\u3002
#
# [EN] It is a property that handles the message. (English)
# [EN] I have to define the message to be output to the log and the message area.
#
#   $ MessageResources.properties 4311 2013-05-16 07:26:40Z hiroko_nagata@dnitsol.com $
#
################################################################################

# [JP] \u30a8\u30e9\u30fc\u30e1\u30c3\u30bb\u30fc\u30b8\u51fa\u529b\u5f62\u5f0f
# [EN] Output format error message
errors.header=<font color="#FF0000"><ul>
errors.footer=</ul></font>
errors.prefix=<li>
errors.suffix=</li>

# [JP] AI-J2\u62e1\u5f35 Validator\u30c1\u30a7\u30c3\u30af\u30eb\u30fc\u30eb\u7528\u30e1\u30c3\u30bb\u30fc\u30b8
# [EN] Check messages for Validator extension rule AI-J2
errors.alphaNumerics=Please input "{0}" in single-byte alphanumeric characters.\u65e5\u672c\u8a9e
errors.prohibited="{0}" contains the entry-suppression character.\u65e5\u672c\u8a9e
errors.zenkakuKatakana=Please input "{0}" in double-byte katakana.\u65e5\u672c\u8a9e
errors.zenkaku=Please input "{0}" in double-byte characters.\u65e5\u672c\u8a9e
errors.hankakuKatakana=Please input "{0}" in single-byte katakana.\u65e5\u672c\u8a9e
errors.hankaku=Please input "{0}" in single-byte characters.\u65e5\u672c\u8a9e
errors.includedHankakuKatakana="{0}" contains the single-byte katakana character.\u65e5\u672c\u8a9e
errors.byteLength=Please input "{0}" in {1} bytes.\u65e5\u672c\u8a9e
errors.byteRange=Please input "{0}" within a range(byte count) of {2} from {1}.\u65e5\u672c\u8a9e
errors.byteMaxLimit="{0}" exceeds the capacity for input value.\u65e5\u672c\u8a9e
errors.surrogatePair="{0}" contains the surrogate pair character.\u65e5\u672c\u8a9e

# [JP] Struts\u63d0\u4f9b Validator\u30c1\u30a7\u30c3\u30af\u30eb\u30fc\u30eb\u7528\u30c7\u30d5\u30a9\u30eb\u30c8\u30e1\u30c3\u30bb\u30fc\u30b8
# [EN] The default message for the Struts Validator provides validation rules
errors.invalid=The input format of {0} is wrong. \u65e5\u672c\u8a9e
errors.maxlength=Please make the {0} lower than {1} characters.\u65e5\u672c\u8a9e
errors.minlength=Please make the {0} more than {1} characters.\u65e5\u672c\u8a9e
errors.range=Please input "{0}" within a range of {2} from {1}.\u65e5\u672c\u8a9e
errors.required=Please input "{0}".\u65e5\u672c\u8a9e
errors.byte=Please input "{0}" in byte type.\u65e5\u672c\u8a9e
errors.date=Please input the right date in "{0}".\u65e5\u672c\u8a9e
errors.double=Please input "{0}" in decimal fraction(double type).\u65e5\u672c\u8a9e
errors.float=Please input "{0}" in decimal fraction(float type).\u65e5\u672c\u8a9e
errors.integer=Please input "{0}" in one-byte integer number.\u65e5\u672c\u8a9e
errors.long=Please input "{0}" in integer number(long type).\u65e5\u672c\u8a9e
errors.short=Please input "{0}" in integer number(short type).\u65e5\u672c\u8a9e
errors.creditcard=Please input the right credit card number in "{0}" .\u65e5\u672c\u8a9e
errors.email=Please input email address format in "{0}".\u65e5\u672c\u8a9e
errors.url=Please input appropriate URL format in "{0}".\u65e5\u672c\u8a9e
errors.validwhen=The input of {0} is wrong.\u65e5\u672c\u8a9e

# [JP] \u30a2\u30d7\u30ea\u57fa\u76e4\u5171\u901a(\u6c4e\u7528)
# [EN] Common Application Development Infrastructure (generic)
com.globaldenso.ai.common.LOG_MESSAGE_TEMPLATE=ID:{0} CODE:{1} MESSAGE:{2} DATE & TIME:{3}\u65e5\u672c\u8a9e
com.globaldenso.ai.common.UNDEFINED_ERROR_CODE=The error-code is undefined. Please define a new error-code. Type:{0} ErrorMessage:{1}\u65e5\u672c\u8a9e
com.globaldenso.ai.common.UNKNOWN_ERROR_CODE=The exception occurs out of a framework. \u65e5\u672c\u8a9e

# [JP] \u30a2\u30d7\u30ea\u57fa\u76e4\u5171\u901a(\u30b7\u30b9\u30c6\u30e0\u30a8\u30e9\u30fc)
# [EN] Common Application Development Infrastructure (system error)
AI-90-0000=[AI-90-0000]Unexpected exception occurred. Exception Type: {0}, Message: {1}\u65e5\u672c\u8a9e
AI-90-0001=[AI-90-0001]Unexpected exception occurred in the presentation layer. Exception Type: {0}, Message: {1}\u65e5\u672c\u8a9e
AI-90-0002=[AI-90-0002]Unexpected exception occurred in the business layer. Exception Type: {0}, Message: {1}\u65e5\u672c\u8a9e
AI-90-0003=[AI-90-0003]Unexpected exception occurred in the integration layer. Exception Type: {0}, Message: {1}\u65e5\u672c\u8a9e
AI-90-0004=[AI-90-0004]Unexpected exception occurred in the DB connection. Exception Type: {0}, Message: {1}\u65e5\u672c\u8a9e
AI-90-0005=[AI-90-0005]When viewing the error.jsp, exceptions unexpected occurred.\u65e5\u672c\u8a9e
AI-80-0000=[AI-80-0000]Unexpected exception occurred in JDBC operation.\u65e5\u672c\u8a9e
AI-80-0001=[AI-80-0001]Invalid SQL issued. Please check the issued SQL statement.\u65e5\u672c\u8a9e
AI-80-0002=[AI-80-0002]Invalid operation manipulated. Please check the operation.\u65e5\u672c\u8a9e
AI-80-0003=[AI-80-0003]Failed in establishment of the database connection. Please check the database connection. \u65e5\u672c\u8a9e
AI-80-0004=[AI-80-0004]There is not data access authority. Please check your data-access authority.\u65e5\u672c\u8a9e
AI-80-0005=[AI-80-0005]There is a data mismatch. Please check the data.\u65e5\u672c\u8a9e
AI-80-0006=[AI-80-0006]Cannot get lock. Please execute again.\u65e5\u672c\u8a9e
AI-80-0007=[AI-80-0007]The transaction cannot be serialized. Please check the transaction.\u65e5\u672c\u8a9e
AI-80-0008=[AI-80-0008]The deadlock was detected.\u65e5\u672c\u8a9e
AI-80-0009=[AI-80-0009]There is a unique constraint violation. Please check the data.\u65e5\u672c\u8a9e
AIJ2-E0-0000=[AIJ2-E0-0000]Common application exception message\u65e5\u672c\u8a9e
AIJ2-90-0000=[AIJ2-90-0000]Common system exception message\u65e5\u672c\u8a9e

# [JP] \u30e1\u30fc\u30eb\u9001\u4fe1\u6a5f\u80fd(mailsend)
# [EN] Mail sending function (mailsend)
AI-E0-ML00=[AI-E0-ML00]Properties file for mail transmission cannot be found. File Name:{0}\u65e5\u672c\u8a9e
AI-E0-ML01=[AI-E0-ML01]The value is not set up to properties file for mail transmission.File Name:{0} Key{1}\u65e5\u672c\u8a9e
AI-E0-ML02=[AI-E0-ML02]The mail address is not set.\u65e5\u672c\u8a9e
AI-E0-ML03=[AI-E0-ML03]Host name of SMTP server is not set.\u65e5\u672c\u8a9e
AI-E0-ML04=[AI-E0-ML04]Delivery address is not set.\u65e5\u672c\u8a9e
AI-E0-ML05=[AI-E0-ML05]Mail text is not set.\u65e5\u672c\u8a9e

# [JP] OS\u30b3\u30de\u30f3\u30c9\u5b9f\u884c(executecommand)
# [EN] OS command execution (executecommand)
AI-E0-EC00=[AI-E0-EC00]Executable file is not specified.\u65e5\u672c\u8a9e
AI-E0-EC01=[AI-E0-EC01]Specified executable file cannot be found.\u65e5\u672c\u8a9e
AI-E0-EC02=[AI-E0-EC02]Null was specified as parameter of executable file.\u65e5\u672c\u8a9e

# ------------------------------------------------------------------------------
# [JP] \u30a2\u30d7\u30ea\u3067\u30e1\u30c3\u30bb\u30fc\u30b8\u3092\u8ffd\u52a0\u3059\u308b\u5834\u5408\u306f\u4ee5\u4e0b\u306b\u8ffd\u52a0\u3057\u3066\u304f\u3060\u3055\u3044
# [EN] Please add the following if you want to add a message in the application
# ------------------------------------------------------------------------------

###################################################################################################
# SPS message resource
###################################################################################################
SP-90-0001=An unexpected exception occurred in the presentation layer. Exception type:{0}, Message:{1}\u65e5\u672c\u8a9e
SP-90-0002=An unexpected exception occurred in the business layer. Exception type:{0}, Message:{1}\u65e5\u672c\u8a9e
SP-90-0003=An unexpected exception occurred in the integration layer. Exception type:{0}, Message:{1}\u65e5\u672c\u8a9e
SP-90-0004=An unexpected exception occurred in the batch layer. Exception type:{0}, Message:{1}\u65e5\u672c\u8a9e

# Purchase ----------------------------------------------------------------------------------------
XX-XX-0001={0} please input in format yyyy/MM/dd\u65e5\u672c\u8a9e

# Invoice -----------------------------------------------------------------------------------------
01-80-0001=WebService not available.\u65e5\u672c\u8a9e
SP-80-0003=It failed to establish database connection. Please check the access to the database.\u65e5\u672c\u8a9e
SP-80-0004=You have no authority to access to the data. Please check your authority for the data access.\u65e5\u672c\u8a9e
SP-80-0009=It is unable to get file from server. Please try again.\u65e5\u672c\u8a9e
SP-80-0010=It is unable to upload file to server. Please try again.\u65e5\u672c\u8a9e
SP-80-0012=It is unable to generate CSV file. Please try again.\u65e5\u672c\u8a9e
SP-80-0013=It is unable to generate PDF file for CIGMA P/O {0}. Please try again.\u65e5\u672c\u8a9e
SP-80-0014=It is unable to generate PDF file for ASN Report.\u65e5\u672c\u8a9e
SP-80-0015=It is unable to generate PDF file for Invoice Cover Page.\u65e5\u672c\u8a9e
SP-80-0016=It is unable to generate PDF file for CIGMA D/O No {0}. Please try again.\u65e5\u672c\u8a9e
SP-80-0017=It is unable to generate PDF file for One-way Kanban Report. Please try again.\u65e5\u672c\u8a9e

#(E7)Input information error (At the input value check and etc.)  
#Information message. -----------------------------------------------------------------------------
SP-I6-0001=Register process finished.\u65e5\u672c\u8a9e
SP-I6-0002=Update process finished.\u65e5\u672c\u8a9e
SP-I6-0003=Delete process finished.\u65e5\u672c\u8a9e
SP-I6-0004=Import process finished.\u65e5\u672c\u8a9e
SP-I6-0005=Export process finished.\u65e5\u672c\u8a9e
SP-I6-0006=System not found data for export.\u65e5\u672c\u8a9e
SP-I6-0007=Acknowledge P/O process finished.\u65e5\u672c\u8a9e
SP-I6-0008=Submittion Promised Due for P/O finished.\u65e5\u672c\u8a9e
SP-I6-0010=Upload CSV file process finished.\u65e5\u672c\u8a9e
SP-I6-0011=System not found data for import.\u65e5\u672c\u8a9e
SP-I6-0012=Sending ASN process finished.\u65e5\u672c\u8a9e
SP-I6-0013=Cancel ASN process finished.\u65e5\u672c\u8a9e
SP-I6-0014=Start process {0}\u65e5\u672c\u8a9e
SP-I6-0015=End process {0}\u65e5\u672c\u8a9e
SP-I6-0016=No P/O to process 'Force Acknowledge'\u65e5\u672c\u8a9e
SP-I6-0017=Encoding : {0}\u65e5\u672c\u8a9e
SP-I6-0018=Send redirect to error screen.\u65e5\u672c\u8a9e
SP-I6-0019={0} user role process finished.\u65e5\u672c\u8a9e
SP-I6-0020={0} ASN process finished, but fail to send notification mail.\u65e5\u672c\u8a9e
SP-I6-0021=Allow Revise ASN process finished.\u65e5\u672c\u8a9e
SP-I6-0022={0} process finished.\u65e5\u672c\u8a9e
SP-I6-0023={0} process finished, but fail to send notification mail.\u65e5\u672c\u8a9e
SP-I6-0024={0} process finished, but fail to send notification mail of {1}.
# [IN054] Message for DENSO Reply Process Finish
SP-I6-0025=DENSO Reply process finished.\u65e5\u672c\u8a9e

#Database information -----------------------------------------------------------------------------
SP-I5-0001=Job Process Start.\u65e5\u672c\u8a9e
SP-I5-0002=Initial Process. \u65e5\u672c\u8a9e
SP-I5-0003=Start Read File {0}\u65e5\u672c\u8a9e
SP-I5-0005=All job operations finished.\u65e5\u672c\u8a9e
SP-I5-0006=Terminate job process.\u65e5\u672c\u8a9e

SP-E5-0001=Cannot send mail to inform urgent order ({0}).\u65e5\u672c\u8a9e
SP-E5-0002=Cannot send mail to inform abnormal order ({0}).\u65e5\u672c\u8a9e
SP-E5-0003=Message file not found.\u65e5\u672c\u8a9e
SP-E5-0004=CSV file data is not complete.\u65e5\u672c\u8a9e
SP-E5-0005=Effective start date is not found in CSV file.\u65e5\u672c\u8a9e
SP-E5-0006=Effective end date is not found in CSV file.\u65e5\u672c\u8a9e
SP-E5-0007=Announcement message is not found in CSV file.\u65e5\u672c\u8a9e
SP-E5-0008=Effective start date is not in format.\u65e5\u672c\u8a9e
SP-E5-0009=Effective end date is not in format.\u65e5\u672c\u8a9e
SP-E5-0010=Announcement message length is over limit.\u65e5\u672c\u8a9e
SP-E5-0011=Effective start date cannot more than Effective end date.\u65e5\u672c\u8a9e
SP-E5-0012=Effective end date cannot less than current date.\u65e5\u672c\u8a9e
SP-E5-0013=Save announce message data fail.\u65e5\u672c\u8a9e
SP-E5-0018=File temporary location not found.\u65e5\u672c\u8a9e
SP-E5-0021=Selected file is not the CSV file.\u65e5\u672c\u8a9e
SP-E5-0022=Message file backup not found.\u65e5\u672c\u8a9e
SP-E5-0023=Cannot sending email from {0}\u65e5\u672c\u8a9e
SP-E5-0025=There are no AS400 configuration for some DENSO company\u65e5\u672c\u8a9e
SP-E5-0026=There are no AS400 configuration for all DENSO company\u65e5\u672c\u8a9e
SP-E5-0027=Cannot complete {0} some error occurred, please check in log file\u65e5\u672c\u8a9e
SP-E5-0028=Class: {0} {1}\u65e5\u672c\u8a9e
SP-E5-0029=Filter ERROR: {0} {1}\u65e5\u672c\u8a9e
SP-E5-0030=JSON String: {0}\u65e5\u672c\u8a9e
SP-E5-0031=Cannot send mail to inform backorder ({0}).

SP-E6-0001=Data not found.\u65e5\u672c\u8a9e
SP-E6-0002=Cannot connect to CIGMA database, all transfer data from SPS is pending.\u65e5\u672c\u8a9e
SP-E6-0003=Search result more than {0} records, please input more criteria.\u65e5\u672c\u8a9e
SP-E6-0004=Cannot {0} data, data already used by another user.\u65e5\u672c\u8a9e
SP-E6-0006=File name is duplicate, please change file name.\u65e5\u672c\u8a9e
SP-E6-0007=Cannot update Mail Send Flag of D/O Detail to ''Send Mail Complete''\u65e5\u672c\u8a9e
SP-E6-0008=No urgent order in this batch running period\u65e5\u672c\u8a9e
SP-E6-0009=Cannot update ASN data at CIGMA DB ASN_HED and ASN_DTL for ASN No. {0}\u65e5\u672c\u8a9e
SP-E6-0010=Cannot update Transfer Flag of ASN Header to 'Completed'. ASN No. {0}\u65e5\u672c\u8a9e
SP-E6-0011=Part No does not match with Supplier Plant\u65e5\u672c\u8a9e
SP-E6-0012=No ASN Data to send to CIGMA Server\u65e5\u672c\u8a9e
SP-E6-0013={0} master data does not exist\u65e5\u672c\u8a9e
SP-E6-0014=No abnormal data in this batch running period\u65e5\u672c\u8a9e
SP-E6-0015=Cannot get Supplier Information data for CIGMA P/O No. {0}, Part No {1}\u65e5\u672c\u8a9e
SP-E6-0016=Cannot create P/O data at SPS system for CIGMA P/O No {0}, Part No {1}, Due Date {2}, from step {3}\u65e5\u672c\u8a9e
SP-E6-0017=Cannot delete record CIGMA P/O No. {0}, Part No {1}, Due Date {2} from {3}\u65e5\u672c\u8a9e
SP-E6-0018=Cannot update SPS Flag to {0} on CIGMA table {1}\u65e5\u672c\u8a9e
SP-E6-0019=Cannot save record CIGMA table {0} for CIGMA P/O No. {1}, Part No {2}, Due Date(ETD) {3} to {4}\u65e5\u672c\u8a9e
SP-E6-0020=Cannot create CHG P/O data at SPS system for CIGMA P/O No. {0}, Part No {1}, Due Date {2}, from step {3}\u65e5\u672c\u8a9e
SP-E6-0021=Cannot get Supplier Information data for CIGMA D/O No{0}, Part No {1}\u65e5\u672c\u8a9e
SP-E6-0022=Cannot create D/O data at SPS system for CIGMA D/O No. {0}, Part No. {1}, from step {2}\u65e5\u672c\u8a9e
SP-E6-0023=Cannot delete record CIGMA D/O No. {0}, Part No. {1}, from {2}\u65e5\u672c\u8a9e
SP-E6-0024=Cannot update SPS Flag to {0} on CIGMA table {1}\u65e5\u672c\u8a9e
SP-E6-0025=Cannot save record CIGMA table {0} for CIGMA D/O No. {1}, Part No {2}, Current Order QTY {3} to {4}\u65e5\u672c\u8a9e
SP-E6-0026=Cannot delete record CIGMA D/O No {0}, Part No. {1}, Order QTY {2} from {3}\u65e5\u672c\u8a9e
SP-E6-0027=Cannot merge D/O data at SPS system from CIGMA D/O No. {0}, Part No. {1}, from step {2}\u65e5\u672c\u8a9e
SP-E6-0028=Cannot upload ASN data because shipment status of SPS D/O No. is "Complete-Ship"\u65e5\u672c\u8a9e
SP-E6-0029=Cannot update P/O Status to "Acknowledge".\u65e5\u672c\u8a9e
SP-E6-0030=Cannot update P/O Status to "Acknowledge" because timestamp was changed.\u65e5\u672c\u8a9e
SP-E6-0031=Cannot get data for combo box {0}\u65e5\u672c\u8a9e
SP-E6-0032=Cannot get maximum limit record\u65e5\u672c\u8a9e
SP-E6-0033=Cannot create CHG P/O data at SPS system for CIGMA P/O No. {0}. Because of changing in "Firm" period\u65e5\u672c\u8a9e
SP-E6-0034=Cannot {0} , please try again.\u65e5\u672c\u8a9e
SP-E6-0035=Cannot submit promised due for P/O\u65e5\u672c\u8a9e
SP-E6-0036=Cannot submit promised due for P/O because timestamp was changed\u65e5\u672c\u8a9e
SP-E6-0037=Register {0} to the system is not success.\u65e5\u672c\u8a9e
SP-E6-0038=Cannot create or update ASN data\u65e5\u672c\u8a9e
SP-E6-0039=Cannot cancel ASN data\u65e5\u672c\u8a9e
SP-E6-0040={0} temparary data does not exist\u65e5\u672c\u8a9e
SP-E6-0041=Invalid email address index : {0}\u65e5\u672c\u8a9e
SP-E6-0042=Cannot save data to database : {0}\u65e5\u672c\u8a9e
SP-E6-0043={0} purging data does not exist\u65e5\u672c\u8a9e
SP-E6-0044=Cannot delete {0}\u65e5\u672c\u8a9e
SP-E6-0045=Cannot transfer Invoice Id. {0} to JDE. Because of {1}\u65e5\u672c\u8a9e
SP-E6-0046=Cannot find SPS P/O No. from CIGMA D/O No. {0}\u65e5\u672c\u8a9e
SP-E6-0047={0} Invoice data for transfering to JDE does not exist\u65e5\u672c\u8a9e
SP-E6-0048={0} is not completed.\u65e5\u672c\u8a9e
SP-E6-0049=Data for create ASN should not more than {0} D/O.\u65e5\u672c\u8a9e
SP-E6-0050=Duplicate ASN No., this ASN is used by another user, please try again.\u65e5\u672c\u8a9e
SP-E6-0051=Duplicate Delivery Order CIGMA D/O No. {0}, D.PCD {1}, Data Type {2}.\u65e5\u672c\u8a9e
SP-E6-0054=Cannot find receiver to send email Notification Pending P/O.\u65e5\u672c\u8a9e
SP-E6-0055=Supplier Company, Currency or TAX master data does not exist.\u65e5\u672c\u8a9e
SP-E6-0056=Related ASN No. over than maximum limit to get receiving information, please input more criteria.\u65e5\u672c\u8a9e
# [IN054] Error message cannot update P/O Status
SP-E6-0057=Cannot update P/O Status to "Reply".\u65e5\u672c\u8a9e
SP-E6-0058=Cannot find receiver to send email Reply Pending P/O.\u65e5\u672c\u8a9e
# [IN063] Digital Signature duplicate
SP-E6-0059=Digital Signature already exist, please change D.CD, D.PCD, Signature Type or Effective Date.\u65e5\u672c\u8a9e
SP-E6-0060=No D/O notification in this batch running period.\u65e5\u672c\u8a9e
SP-E6-0061=No Kanban tag notification in this batch running period.\u65e5\u672c\u8a9e
SP-E6-0062=Cannot create One-Way Kanban data at SPS system for CIGMA D/O No. {0}, Tag Seq. {1}, from step {2}\u65e5\u672c\u8a9e
SP-E6-0063=Cannot update D/O at SPS system for CIGMA D/O No. {0}\u65e5\u672c\u8a9e
SP-E6-0064=Cannot create PO at CIGMA system for SPS PO No. : {0}
SP-E6-0065=Cannot update PO transfer flag at SPS system for SPS PO No. : {0}
SP-E6-0066=Cannot create DO at CIGMA system for SPS DO No. : {0}
SP-E6-0067=Cannot update DO transfer flag at SPS system for SPS DO No. : {0}
SP-E6-0068=Cannot create ASN at SPS system for ASN No. : {0}
SP-E6-0069=Cannot create ASN at CIGMA system for ASN No. : {0}
SP-E6-0070=Cannot update ASN transfer flag at SPS system for ASN No. : {0}
SP-E6-0071=Cannot update ASN transfer flag at CIGMA system for ASN No. : {0}
SP-E6-0072=Validate failed : Cannot find original ASN for cancel ASN No. : {0}
SP-E6-0073=Validate failed : Cannot find related D/O or D/O status is CCL for ASN No. : {0}
SP-E6-0074=Validate failed : Related D/O has difference D.CD, or D.PCD, or S.CD, or S.PCD. for ASN No. : {0}
SP-E6-0075=Validate failed : Related D/O has difference scan flag for ASN No. : {0}
SP-E6-0076=Validate failed : Related D/O has difference W/H for ASN No. : {0}
SP-E6-0077=Validate failed : Total record is more than MAX_RECORD_FOR_RECEIVE_BY_SCAN for ASN No. : {0}
SP-E6-0078=Validate failed : Total shipping qty is over than order qty for ASN No : {0}
SP-E6-0079=D/O is not exist (Back order)
SP-E6_0080=D/O QTY not match as ASN (Back order)
SP-E6-0081=CHG D/O is not exist (Back order)
SP-E6-0082=CHG D/O QTY not match as ASN (Back order)
SP-E6-0083=Cannot update CIGMA DO detail (Back order)
SP-E6-0084=Cannot update CIGMA DO header (Back order)
SP-E6-0085=No back order in this batch running period

SP-E7-0001=Please select data for export to CSV file.\u65e5\u672c\u8a9e
SP-E7-0002=Number of columns of CSV file does not match\u65e5\u672c\u8a9e
SP-E7-0003=To press [Acknowledge], please select data for status "Issued" only\u65e5\u672c\u8a9e
SP-E7-0004=To Group Invoice, selected ASN must have same {0}.\u65e5\u672c\u8a9e
SP-E7-0005=No data of column {0} in CSV file\u65e5\u672c\u8a9e
SP-E7-0006=Cannot select for grouping ASN, all {0}\u65e5\u672c\u8a9e
SP-E7-0007=Please input {0}\u65e5\u672c\u8a9e
SP-E7-0008=Cannot upload for grouping Invoice, all {0}\u65e5\u672c\u8a9e
SP-E7-0009=Cannot create invoice, {0} have to equal with {1}\u65e5\u672c\u8a9e
SP-E7-0010=Please input New Due, New Qty and Reason\u65e5\u672c\u8a9e
SP-E7-0011=Please input any search criteria.\u65e5\u672c\u8a9e
SP-E7-0012=Please select any record for {0}.\u65e5\u672c\u8a9e
SP-E7-0013=Invalid File Type, allow to upload only {0} file\u65e5\u672c\u8a9e
SP-E7-0014=Cannot upload empty file, please select new file\u65e5\u672c\u8a9e
SP-E7-0015=File size is more than "{0}", please select new file.\u65e5\u672c\u8a9e
SP-E7-0016=Please input Shipping QTY > 0\u65e5\u672c\u8a9e
SP-E7-0017=Please input Shipping QTY equal to {0}\u65e5\u672c\u8a9e
SP-E7-0018=Please input Shipping QTY between {0} and {1}\u65e5\u672c\u8a9e
SP-E7-0019=Please choose file before {0}.\u65e5\u672c\u8a9e
SP-E7-0020=The CSV file size is over than {0}.\u65e5\u672c\u8a9e
SP-E7-0021=System not found the upload file.\u65e5\u672c\u8a9e
SP-E7-0022={0} already existing in system.\u65e5\u672c\u8a9e
SP-E7-0023=Input {0} is wrong format\u65e5\u672c\u8a9e
SP-E7-0024=Duplicate user role.\u65e5\u672c\u8a9e
SP-E7-0025=Cannot save data. Because of does not have the operations on this screen.\u65e5\u672c\u8a9e
SP-E7-0026=Do you want to cancel all operations on this screen.\u65e5\u672c\u8a9e
SP-E7-0027=DSC ID is not register in the system.\u65e5\u672c\u8a9e
SP-E7-0028=User right is out of the effective date range.\u65e5\u672c\u8a9e
SP-E7-0029=Input New Due and New QTY with the same value of Order Due and Order QTY, please input another value again.\u65e5\u672c\u8a9e
SP-E7-0030=Input {0} over limit. Please input not over {1} digit\u65e5\u672c\u8a9e
SP-E7-0031=Cannot input {0} > {1}\u65e5\u672c\u8a9e
SP-E7-0032=This DSC-ID number is already use\u65e5\u672c\u8a9e
SP-E7-0033=Cannot input quantity with decimal digit for {0}\u65e5\u672c\u8a9e
SP-E7-0034=For grouping invoice, any ASN have same {0}.\u65e5\u672c\u8a9e
SP-E7-0035={0} is complete.\u65e5\u672c\u8a9e
SP-E7-0036=Selected item DENSO owner value is not match with current user DENSO role.\u65e5\u672c\u8a9e
SP-E7-0037=Cannot {0}\u65e5\u672c\u8a9e
SP-E7-0038=Cannot submit {0} less than {1}.\u65e5\u672c\u8a9e
SP-E7-0039=Shipping Qty does not match with lot size. Do you want to continue?\u65e5\u672c\u8a9e
# [IN039] allow user to input Shipping Box Qty
SP-E7-0040=If Shipping QTY is not zero, Shipping Box QTY must not zero.\u65e5\u672c\u8a9e
SP-E7-0041=If Shipping QTY is zero, Shipping Box QTY must be zero.\u65e5\u672c\u8a9e
#SP-E7-0042=Cannot select for grouping ASN, Because of one-way Kanban Tag has not been printed.\u65e5\u672c\u8a9e
SP-E7-0042=Cannot select for grouping ASN, Because of one-way Kanban Tag does not reach lead-time.\u65e5\u672c\u8a9e
SP-E7-0043=ASN cannot contains more than {0} parts for 1 Rcv Lane.\u65e5\u672c\u8a9e
SP-E7-0044=The transmit information is exist.\u65e5\u672c\u8a9e

# Security error (Such as authorization error or session information error) : E9
SP-E9-0001=Cannot connect to the authentication server.\u65e5\u672c\u8a9e
SP-E9-0002=Login fail.\u65e5\u672c\u8a9e
SP-E9-0003=You do not have a permission to operate requested screen.\u65e5\u672c\u8a9e
#SP-E9-0004=You don\u2019t have authority, this button allow for Role ( {0} ) .\u65e5\u672c\u8a9e
SP-E9-0004=You don\u2019t have authority.\u65e5\u672c\u8a9e

#Warning message.
SP-W2-0001=Selected data not same {0}, Are you sure to group to same invoice?\u65e5\u672c\u8a9e
SP-W2-0002=User information is difference with siteminder system. Do you want to save to the system ?\u65e5\u672c\u8a9e
SP-W2-0003=System will clear checkbox in current page, cannot group ASN across page. Do you want to continue?\u65e5\u672c\u8a9e
SP-W2-0004={0} is not suitable, for {1} should specify in the same {2}\u65e5\u672c\u8a9e
SP-W2-0005=This input value is invalid. The value must be page No. which is exist in results of search.\u65e5\u672c\u8a9e
SP-W2-0006=One-way Kanban Tag has been printed.\\nPlease re-check when attach One-way Kanban Tag.\u65e5\u672c\u8a9e
SP-W2-0007=The DO information which you choose are not passed Tag Lead Time yet.\u65e5\u672c\u8a9e

SP-W6-0001=Are you sure to {0}?\u65e5\u672c\u8a9e
SP-W6-0002=Please define all mandatory input before {0}.\u65e5\u672c\u8a9e
SP-W6-0003=Cannot define effective start date more than or equal effective end date.\u65e5\u672c\u8a9e
SP-W6-0004=Cannot define effective end date less than or equal current date.\u65e5\u672c\u8a9e
SP-W6-0006=Please choose file before {0}.\u65e5\u672c\u8a9e
SP-W6-0007=System not found the upload file.\u65e5\u672c\u8a9e
SP-W6-0008=Are you sure to return to previous screen? inputted data will be lost.\u65e5\u672c\u8a9e
SP-W6-0009=Are you sure to Save&Send? Pending List will send and other Part No status will be Acknowledge.\u65e5\u672c\u8a9e
SP-W6-0010=Some user information not match with Siteminder system. Are you sure to save not match data to the system ?\u65e5\u672c\u8a9e
SP-W6-0011=Warning for grouping ASN all {0}\u65e5\u672c\u8a9e
