$(document).ready(function() {

    var errorMessage = $("span[id='errorTotal']:last").text();
    
    validateMenuCode();
    
    $('#btnBrowse').focus();
    $('#btnUpload').click(function() {
        $('#method').val('Upload');
        $('#methodResult').val('Upload');
        disableBeforeSubmit();
        $('#Word001FormCriteria').submit();
    });
    
    if(!checkNullOrEmpty($("#maxPlantSupplierList").val())){
        var strPlantCd = "  ";
        for(var i=0; i < $('#maxPlantSupplierList').val(); i++){
            strPlantCd += $('#plantSupplierCode' + i).val();
            strPlantCd += "\r\n";
        }
        $("#plantSupplier").text(strPlantCd);
    }
     
    $('#btnReset').click(function() {
        if (!checkNullOrEmpty($('#cannotResetMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
        } else {
            $('#method').val('Reset');
            $('#methodResult').val('Reset');
            disableBeforeSubmit();
            $('#Word001FormCriteria').submit();
        }
    });
    
    $('#btnRegister').click(function() {
        if(confirm(msgConfirmRegister.replace("{0}", labelRegisterProcess))){
            $('#method').val('Register');
            $('#methodResult').val('Register');
            disableBeforeSubmit();
            $('#Word001FormCriteria').submit();
        }
    });
    
    $('#btnExport').click(function() {
        if (!checkNullOrEmpty($('#cannotExportMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotExportMessage').val() + "</span>");
        } else {
            $('#method').val('Export');
            $('#methodResult').val('Export');
            $('#Word001FormCriteria').submit();
        }
    });
});

function supplierInfoSetPathFile(){
    $('#txtUpload').val($('#fileData').val().split('\\').pop());
}
