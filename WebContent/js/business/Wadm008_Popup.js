$(document).ready(function() {
    $('#effectStart').addClass("mandatory");
    $('#effectEnd').addClass("mandatory");
    
    var boxDCd = $("select#DCd");
    var boxDPcd = $("select#DPcd");
    
    boxDCd.unbind('change');
    boxDCd.change(function(){
        var errorTotal = '';
        var dCd = boxDCd.val();
        var boxDensoPlantVal = boxDPcd.val();
        var params = {
            "method" : "doSelectDCD",
            "DCd" : dCd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "DensoUserRoleAssignmentAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                if(!checkNullOrEmpty(data.jsonResult)){
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>"
                        + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    // [IN052] Undefined must not set as default
                    boxDPcd.append($("<option></option>").val("").html(labelPleaseSelect));
                    boxDPcd.append($("<option></option>").val("99").html(labelUndefined));
                }else{
                    var plantList = data.jsonList1;
                    boxDPcd.empty();
                    //[IN052] Undefined must not set as default
                    //boxDPcd.append($("<option></option>").val("99").html(labelUndefined));
                    boxDPcd.append($("<option></option>").val("").html(labelPleaseSelect));
                    
                    if(plantList && plantList.length > 0){
                        $.each(plantList,function(index,val){
                            if(boxDensoPlantVal == val.DPcd){
                                boxDPcd.append($("<option selected></option>").val(val.DPcd).text(val.DPcd));
                            }else{
                                boxDPcd.append($("<option></option>").val(val.DPcd).text(val.DPcd));
                            }
                        });
                    }
                    //[IN052] Undefined must not set as default
                    boxDPcd.append($("<option></option>").val("99").html(labelUndefined));
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }
                boxDensoPlantVal = "";
                enableAfterSubmit();
            },
            error: function() {
                enableAfterSubmit();
            }
        });
    });
    
    boxDPcd.unbind('change');
    boxDPcd.change(function(){
        var errorTotal = '';
        var boxDensoCdValue = boxDCd.val();
        var dPcd = boxDPcd.val();
        var params = {
            "method" : "doSelectDPCD",
            "DPcd"   : dPcd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "DensoUserRoleAssignmentAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data){
                var companyList = data.jsonList1;
                boxDCd.empty();
                if(companyList && companyList.length > 0){
                    $.each(companyList,function(index,val){
                        if(boxDensoCdValue == val.DCd){
                            boxDCd.append($("<option selected></option>").val(val.DCd).text(val.DCd));
                        }else{
                            boxDCd.append($("<option></option>").val(val.DCd).text(val.DCd));
                        }
                    });
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }else{
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                    $("#divError").show();
                }
                boxDensoCdValue = "";
                enableAfterSubmit();
            },
            error: function(){
                enableAfterSubmit();
            }
        });
    });
    
    $('#btnOK').click(function() {
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotOkMessage').val())){
            var jsonStr = '';
            var mode          = $('#mode').val();
            var dscId         = $('#dscId').val();
            var companyDensoCode = $('#DCd').val();
            var plantDensoCode   = $('#DPcd').val();
            var roleCode      = $('#roleCode').val();
            var effectEnd     = $('#effectEnd').val();
            var effectStart   = $('#effectStart').val();
            
            jsonStr += '{"mode" : "' + mode + '",';
            jsonStr += '"roleCode" : "' + roleCode + '",';
            jsonStr += '"companyDensoCode" : "' + companyDensoCode + '",';
            jsonStr += '"plantDensoCode" : "' + plantDensoCode + '",';
            jsonStr += '"effectEnd" : "' + effectEnd + '",';
            jsonStr += '"effectStart" : "' + effectStart + '",';
            jsonStr += '"dscId" : "' + dscId + '"}';
                
            var data = jQuery.parseJSON('[' + jsonStr + ']');
            window.returnValue = data;
            window.close();
            self.close();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotOkMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    $('#btnCancel').click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotCancelMessage').val())){
            window.close();
            self.close();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotCancelMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });

});
