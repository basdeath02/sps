$(document).ready(function() {
    validateMenuCode();
    
    var boxDCd = $("select#DCd");
    var boxDPcd = $("select#DPcd");
    
    boxDCd.unbind('change');
    boxDCd.change(function(){
        var errorTotal = '';
        var dCd = boxDCd.val();
        var boxDensoPlantVal = boxDPcd.val();
        var params = {
            "method" : "doSelectDCD",
            "DCd"    : dCd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "DensoUserRegistrationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                if(!checkNullOrEmpty(data.jsonResult)){
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>"
                        + data.jsonResult + "</span>");
                    $("#divError").show();

                    // [IN052] Do not set Undefined as default DENSO Plant.
                    boxDPcd.append($("<option></option>").val("").html(labelPleaseSelect));
                    boxDPcd.append($("<option></option>").val("99").html(labelUndefined));
                }else{
                    var plantList = data.jsonList1;
                    boxDPcd.empty();
                    // [IN052] Do not set Undefined as default DENSO Plant.
                    //boxDPcd.append($("<option></option>").val("99").html(labelUndefined));
                    boxDPcd.append($("<option></option>").val("").html(labelPleaseSelect));
                    
                    if(plantList && plantList.length > 0){
                        $.each(plantList,function(index,val){
                            if(boxDensoPlantVal == val.DPcd){
                                boxDPcd.append($("<option selected></option>").val(val.DPcd).text(val.DPcd));
                            }else{
                                boxDPcd.append($("<option></option>").val(val.DPcd).text(val.DPcd));
                            }
                        });
                    }
                    // [IN052] Do not set Undefined as default DENSO Plant.
                    boxDPcd.append($("<option></option>").val("99").html(labelUndefined));
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }
                boxDensoPlantVal = "";
                enableAfterSubmit();
                wadm006CheckEnableButton();
            },
            error: function() {
                enableAfterSubmit();
                wadm006CheckEnableButton();
            }
        });
    });
    
    boxDPcd.unbind('change');
    boxDPcd.change(function(){
        var errorTotal = '';
        var boxDensoCdValue = boxDCd.val();
        var dPcd = boxDPcd.val();
        var params = {
            "method" : "doSelectDPCD",
            "DPcd"   : dPcd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "DensoUserRegistrationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data){
                var companyList = data.jsonList1;
                boxDCd.empty();
                if(companyList && companyList.length > 0){
                    // [IN052] Set --Please Select-- as default
                	boxDCd.append($("<option></option>").val("").html(labelPleaseSelect));
                    
                    $.each(companyList,function(index,val){
                        if(boxDensoCdValue == val.DCd){
                            boxDCd.append($("<option selected></option>").val(val.DCd).text(val.DCd));
                        }else{
                            boxDCd.append($("<option></option>").val(val.DCd).text(val.DCd));
                        }
                    });
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }else{
                    // [IN052] Set --Please Select-- as default
                	boxDCd.append($("<option></option>").val("").html(labelPleaseSelect));
                    
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                    $("#divError").show();
                }
                boxDensoCdValue = "";
                enableAfterSubmit();
                wadm006CheckEnableButton();
            },
            error: function(){
                enableAfterSubmit();
                wadm006CheckEnableButton();
            }
        });
    });
    
    $('#btnReset').click(function() {
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotResetMessage').val())){
            $('#method').val('Reset');
            $('#isReset').val(true);
            disableBeforeSubmit();
            $('#Wadm006Form').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
            
    $('#btnReturn').click(function() {
        if(confirm(msgConfirmReturn.replace("{0}", labelReturn))){
            $('#method').val('Return');
            disableBeforeSubmit();
            $('#Wadm006Form').submit();
        }
    });
   
    $('#btnRegister').click(function(){
        var mode = $('#mode').val();
        if(mode == 'Register'){
            $('#method').val('Register');
        }else{
            $('#method').val('Update');
        }
        setEmailDensoFlag();
        disableBeforeSubmit();
        $('#Wadm006Form').submit();
    });
    
    var departmentTags = [];
    for(var i=0; i < $('#maxDepartment').val(); i++){
        departmentTags.push($('#dept' + i).val());
    }
    
     $('#departmentName').autocomplete({
         source: departmentTags
     });
});

function setEmailDensoFlag(){
    if($('#emlCreateCancelAsnChk').attr('checked')){
        $('#emlCreateCancelAsnFlag').val('1');
    }else{
        $('#emlCreateCancelAsnFlag').val('0');
    }
    
    if($('#emlReviseAsnChk').attr('checked')){
        $('#emlReviseAsnFlag').val('1');
    }else{
        $('#emlReviseAsnFlag').val('0');
    }
    
    if($('#emlCreateInvoiceChk').attr('checked')){
        $('#emlCreateInvoiceFlag').val('1');
    }else{
        $('#emlCreateInvoiceFlag').val('0');
    }
    
    // Start : [IN049] Change Column Name
    //if($('#emlAbnormalTransferData1Chk').attr('checked')){
    //    $('#emlAbnormalTransferData1Flag').val('1');
    //}else{
    //    $('#emlAbnormalTransferData1Flag').val('0');
    //}
    //if($('#emlAbnormalTransferData2Chk').attr('checked')){
    //    $('#emlAbnormalTransferData2Flag').val('1');
    //}else{
    //    $('#emlAbnormalTransferData2Flag').val('0');
    //}
    

    if($('#emlPedpoDoalcasnUnmatpnChk').attr('checked')){
        $('#emlPedpoDoalcasnUnmatpnFlg').val('1');
    }else{
        $('#emlPedpoDoalcasnUnmatpnFlg').val('0');
    }
    
    if($('#emlAbnormalTransferChk').attr('checked')){
        $('#emlAbnormalTransferFlg').val('1');
    }else{
        $('#emlAbnormalTransferFlg').val('0');
    }
    // End : [IN049] Change Column Name
}

function wadm006CheckEnableButton(){
    if('Register' == $('#mode').val()){
        $('#btnReturn').attr('disabled',true);
    }else{
        $('#btnReturn').removeAttr('disabled');
    }
}