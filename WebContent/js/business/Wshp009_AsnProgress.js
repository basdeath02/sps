$(document).ready(function() {
    validateMenuCode();
    $("#actualEtdFrom").addClass('mandatory', true);
    $("#actualEtdTo").addClass('mandatory', true);
    $("#actualEtaFrom").addClass('vAlignDate', true);
    $("#actualEtaTo").addClass('vAlignDate', true);
    $("#invoiceDateFrom").addClass('vAlignDate', true);
    $("#invoiceDateTo").addClass('vAlignDate', true);
    
    var boxDensoCode = $("select#DCd");
    var boxDensoPlantCode = $("select#DPcd");
    var boxSupplierCode = $("select#vendorCd");
    var boxSupplierPlantCode = $("select#SPcd");
    var errorMessage = $("span[id='errorTotal']:last").text();
    
    $('#btnSearch').click(function(){
        $('#method').val('Search');
        $('#mode').val('search');
        $('#methodResult').val('Search');
        disableBeforeSubmit();
        $('#Wshp009FormCriteria').submit();
    });
    
    $('#btnReset').click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotResetMessage').val())){
            $('#method').val('Reset');
            $('#mode').val('');
            disableBeforeSubmit();
            $('#Wshp009FormCriteria').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    $('#btnDownload').click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotDownloadMessage').val())){
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
            
            $('#method').val('Download');
            $('#Wshp009FormCriteria').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotDownloadMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    boxDensoCode.unbind('change');
    boxDensoCode.change(function(){
        var dCd = boxDensoCode.val();
        var boxDensoPlantValue = boxDensoPlantCode.val();
        var errorTotal = '';
        var params = {
            "method" : "doSelectDCD",
            "DCd"    : dCd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "AsnDetailProgressAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                var plantList = data.jsonList1;
                boxDensoPlantCode.empty();
                if(plantList && 0 < plantList.length && '' != data.jsonResult ){
                    boxDensoPlantCode.append($("<option></option>").val("ALL").html(labelAll));
                    $.each(plantList,function(index,val){
                        if(boxDensoPlantValue == val.DPcd){
                            boxDensoPlantCode.append($("<option selected></option>").val(val.DPcd).text(val.DPcd));
                        }else{
                            boxDensoPlantCode.append($("<option></option>").val(val.DPcd).text(val.DPcd));
                        }
                    });
                    
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }else{
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>"
                        + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    boxDensoPlantCode.append($("<option></option>").val("ALL").html(labelAll));
                }
                boxDensoPlantValue = "";
                enableAfterSubmit();
            },
            error: function() {
                enableAfterSubmit();
            }
        });
    });
    
    boxSupplierCode.unbind('change');
    boxSupplierCode.change(function(){
        var errorTotal = '';
        var vendorCd = boxSupplierCode.val();
        var boxSupplierPlantValue = boxSupplierPlantCode.val();
        var params = {
            "method"    : "doSelectSCD",
            "vendorCd"  : vendorCd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "AsnDetailProgressAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                var plantList = data.jsonList1;
                boxSupplierPlantCode.empty();
                if(plantList && plantList.length > 0){
                    boxSupplierPlantCode.append($("<option></option>").val("ALL").html(labelAll));
                    $.each(plantList,function(index,val){
                        if(boxSupplierPlantValue == val.SPcd){
                            boxSupplierPlantCode.append($("<option selected></option>").val(val.SPcd).text(val.SPcd));
                        }else{
                            boxSupplierPlantCode.append($("<option></option>").val(val.SPcd).text(val.SPcd));
                        }
                        
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    });
                }else{
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>"
                        + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    boxSupplierPlantCode.append($("<option></option>").val("ALL").html(labelAll));
                }
                boxSupplierPlantValue = "";
                enableAfterSubmit();
            },
            error: function() {
                enableAfterSubmit();
            }
        });
    });
    
    boxDensoPlantCode.unbind('change');
    boxDensoPlantCode.change(function(){
        var errorTotal = '';
        var boxDensoCdValue = boxDensoCode.val();
        var dPcd = boxDensoPlantCode.val();
        var params = {
            "method" : "doSelectDPCD",
            "DPcd"   : dPcd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "AsnDetailProgressAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data){
                var companyList = data.jsonList1;
                boxDensoCode.empty();
                if(companyList && companyList.length > 0){
                    boxDensoCode.append($("<option></option>").val("ALL").html(labelAll));
                    $.each(companyList,function(index,val){
                        if(boxDensoCdValue == val.DCd){
                            boxDensoCode.append($("<option selected></option>").val(val.DCd).text(val.DCd));
                        }else{
                            boxDensoCode.append($("<option></option>").val(val.DCd).text(val.DCd));
                        }
                    });
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }else{
                   errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    boxDensoCode.append($("<option></option>").val("ALL").html(labelAll));
                }
                boxDensoCdValue = "";
                enableAfterSubmit();
            },
            error: function(){
                enableAfterSubmit();
            }
        });
    });
    
    boxSupplierPlantCode.unbind('change');
    boxSupplierPlantCode.change(function(){
        var errorTotal = '';
        var boxVendorCdValue = boxSupplierCode.val();
        var sPcd = boxSupplierPlantCode.val();
        var params = {
            "method" : "doSelectSPCD",
            "SPcd"   : sPcd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "AsnDetailProgressAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                var companyList = data.jsonList1;
                boxSupplierCode.empty();
                if(companyList && companyList.length > 0){
                    boxSupplierCode.append($("<option></option>").val("ALL").html(labelAll));
                    $.each(companyList,function(index,val){
                        if(boxVendorCdValue == val.vendorCd){
                            boxSupplierCode.append($("<option selected></option>").val(val.vendorCd).text(val.vendorCd));
                        }else{
                            boxSupplierCode.append($("<option></option>").val(val.vendorCd).text(val.vendorCd));
                        }
                    });
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                } else {
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    boxSupplierCode.append($("<option></option>").val("ALL").html(labelAll));
                }
                boxVendorCdValue = "";
                enableAfterSubmit();
            },
            error: function() {
                enableAfterSubmit();
            }
        });
    });
    
    $("#moreLegend").click(function(){
        $('#method').val('More');
        $('#Wshp009FormCriteria').submit();
    });
});
function wshp009ViewPdf(fileId){
    $('#method').val('ViewPdf');
    $('#pdfFileId').val(fileId);
    $('#Wshp009FormCriteria').submit();
}
function wshp009SetDateFromTo(inputItemFromId, inputItemToId){
    var dateFrom = $('#' + inputItemFromId).val();
    if( $('#' + inputItemToId).val() == '' ) {
        $('#' + inputItemToId).val(dateFrom);
    }
}
function wshp009ViewInvoiceDetail(invId, invDate, cnDate, invRcvSta, invNo, cnNo, dc, dpcd, sc, spcd){
    $('#method').val('ForwardPage');
    $('#methodResult').val('ForwardPage');
    $('#invoiceIdCriteria').val(invId);
    $('#selectInvoiceDate').val(invDate);
    $('#selectCnDate').val(cnDate);
    $('#rcvStatusCriteria').val(invRcvSta);
    $('#invoiceNoCriteria').val(invNo);
    $('#cnNoCriteria').val(cnNo);
    $('#dCdCriteria').val(dc);
    $('#dPcdCriteria').val(dpcd);
    $('#sCdCriteria').val(sc);
    $('#sPcdCriteria').val(spcd);
    
    disableBeforeSubmit();
    $('#Wshp009FormResult').submit();
}
function wshp009ViewAsnMaintenance(asnNo, dCd){
    $('#method').val('LinkAsnNo');
    $('#methodResult').val('LinkAsnNo');
    $('#selectedAsnNo').val(asnNo);
    $('#selectedDCd').val(dCd);
    disableBeforeSubmit();
    $('#Wshp009FormResult').submit();
}