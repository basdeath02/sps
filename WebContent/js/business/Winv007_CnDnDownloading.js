$(document).ready(function() {
    validateMenuCode();
    $("#updateDateFrom").addClass('vAlignDate', true);
    $("#updateDateTo").addClass('vAlignDate', true);
    
    $("#btnSearch").click(function() {
        $('#method').val('Search');
        disableBeforeSubmit();
        $('#Winv007FormCriteria').submit();
    });
    $('#btnReset').click(function() {
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotResetMessage').val())){
            $('#method').val('Reset');
            $('#mode').val('');
            disableBeforeSubmit();
            $('#Winv007FormCriteria').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    var boxSupplierCode = $("select#supplierCompanyCode");
    var boxSupplierPlantCode = $("select#supplierPlantCode");
    var boxDensoCode = $("select#dCd");
    var boxDensoPlantCode = $("select#dPcd");
    
    // Change Supplier Company Code ----------------------------------------------------------------
    boxSupplierCode.unbind('change');
    boxSupplierCode.change(function(){
        var errorTotal = '';
        var supplierCode = boxSupplierCode.val();
        if("" != supplierCode){
            if("ALL" == boxSupplierCode.val()){
                var params = {
                    "method"     : "doSelectSCD"
                    ,"vendorCd"  : "" };
            }else{
                var params = {
                    "method"     : "doSelectSCD"
                    ,"vendorCd"  : boxSupplierCode.val() };
            }
	        disableBeforeSubmit();
	        $.ajax({
	            url : "CnDnDownloadingAction.do",
	            type : 'GET',
	            async : false,
	            data: params,
	            dataType : 'json',
	            success : function(data) {
	                var plantList = data.jsonList1;
	                var boxSupplierPlantValue = boxSupplierPlantCode.val();
	                boxSupplierPlantCode.empty();
	                if(plantList && plantList.length > 0){
	                    boxSupplierPlantCode.append( $("<option></option>").val("ALL").html(labelAll) );
	                    $.each(plantList,function(index,val){
	                        if(boxSupplierPlantValue == val.miscCd){
	                            boxSupplierPlantCode.append( $("<option selected></option>").val(val.miscCd).text(val.miscValue) );
	                        }else{
	                            boxSupplierPlantCode.append( $("<option></option>").val(val.miscCd).text(val.miscValue) );
	                        }
	                    });
	                    errorTotal = $('#errorTotal');
	                    errorTotal.empty();
	                    $("#divError").hide();
	                }else{
	                    errorTotal = $('#errorTotal');
	                    errorTotal.empty();
	                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
	                    $("#divError").show();
	                    
	                    boxSupplierPlantCode.append( $("<option></option>").val("ALL").html(labelAll) );
	                }
	                enableAfterSubmit();
	            },
	            error: function() {
	                enableAfterSubmit();
	            }
	        });
	    }
	});

    // Change Supplier Company Code ----------------------------------------------------------------
    boxSupplierPlantCode.unbind('change');
    boxSupplierPlantCode.change(function(){
        var errorTotal = '';
        var sPcd = boxSupplierPlantCode.val();
        if("" != sPcd){
            if("ALL" == sPcd){
                var params = {
                    "method"     : "doSelectSPCD"
                    ,"SPcd"  : "" };
            }else{
                var params = {
                    "method"     : "doSelectSPCD"
                    ,"SPcd"  : sPcd };
            }
	        disableBeforeSubmit();
	        $.ajax({
	            url : "CnDnDownloadingAction.do",
	            type : 'GET',
	            async : false,
	            data: params,
	            dataType : 'json',
	            success : function(data) {
	                var companyList = data.jsonList1;
	                var boxSupplierCompanyValue = boxSupplierCode.val();
	                boxSupplierCode.empty();
	                if(companyList && companyList.length > 0){
	                	boxSupplierCode.append( $("<option></option>").val("ALL").html(labelAll) );
	                    $.each(companyList,function(index,val){
	                        if(boxSupplierCompanyValue == val.vendorCd){
	                        	boxSupplierCode.append( $("<option selected></option>").val(val.vendorCd).text(val.vendorCd) );
	                        }else{
	                        	boxSupplierCode.append( $("<option></option>").val(val.vendorCd).text(val.vendorCd) );
	                        }
	                    });
	                    errorTotal = $('#errorTotal');
	                    errorTotal.empty();
	                    $("#divError").hide();
	                }else{
	                    errorTotal = $('#errorTotal');
	                    errorTotal.empty();
	                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
	                    $("#divError").show();
	                    
	                    boxSupplierPlantCode.append( $("<option></option>").val("ALL").html(labelAll) );
	                }
	                enableAfterSubmit();
	            },
	            error: function() {
	                enableAfterSubmit();
	            }
	        });
	    }
	});
    
    // Change DENSO Company Code -------------------------------------------------------------------
    boxDensoCode.unbind('change');
    boxDensoCode.change(function(){
        var errorTotal = '';
        var densoCode = boxDensoCode.val();
        if("" != densoCode){
            if("ALL" == densoCode){
                var params = {
                    "method"   : "doSelectDCD",
                    "DCd"      : "" };
            }else{
                var params = {
                    "method"   : "doSelectDCD",
                    "DCd"      : densoCode };
            }
            disableBeforeSubmit();
            $.ajax({
                url : "CnDnDownloadingAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var plantList = data.jsonList1;
                    var boxDensoPlantValue = boxDensoPlantCode.val();
                    boxDensoPlantCode.empty();
                    if(plantList && plantList.length > 0){
                        boxDensoPlantCode.append($("<option></option>").val("ALL").html(labelAll));
                        $.each(plantList,function(index,val){
                            if(boxDensoPlantValue == val.DPcd){
                                boxDensoPlantCode.append($("<option selected></option>").val(val.DPcd).text(val.DPcd));
                            }else{
                                boxDensoPlantCode.append($("<option></option>").val(val.DPcd).text(val.DPcd));
                            }
                        });
                        
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    }else{
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                        $("#divError").show();
                        
                        boxDensoPlantCode.append( $("<option></option>").val("ALL").html(labelAll));
                    }
                    enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        }
    });

    // Change DENSO Company Code -------------------------------------------------------------------
    boxDensoPlantCode.unbind('change');
    boxDensoPlantCode.change(function(){
        var errorTotal = '';
        var dPcd = boxDensoPlantCode.val();
        if("" != dPcd){
            if("ALL" == dPcd){
                var params = {
                    "method"   : "doSelectDPCD",
                    "DPcd"      : "" };
            }else{
                var params = {
                    "method"   : "doSelectDPCD",
                    "DPcd"      : dPcd };
            }
            disableBeforeSubmit();
            $.ajax({
                url : "CnDnDownloadingAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var companyList = data.jsonList1;
                    var boxDensoCompanyValue = boxDensoCode.val();
                    boxDensoCode.empty();
                    if(companyList && companyList.length > 0){
                    	boxDensoCode.append($("<option></option>").val("ALL").html(labelAll));
                        $.each(companyList,function(index,val){
                            if(boxDensoCompanyValue == val.DCd){
                            	boxDensoCode.append($("<option selected></option>").val(val.DCd).text(val.DCd));
                            }else{
                            	boxDensoCode.append($("<option></option>").val(val.DCd).text(val.DCd));
                            }
                        });
                        
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    }else{
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                        $("#divError").show();
                        
                        boxDensoCode.append( $("<option></option>").val("ALL").html(labelAll));
                    }
                    enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        }
    });

});

function Winv007DowloadFile(fileId){
    var errorTotal = '';
    if(checkNullOrEmpty($('#cannotDownloadMessage').val())){
        errorTotal = $('#errorTotal');
        errorTotal.empty();
        $("#divError").hide();
        
        $('#method').val('Download');
        $('#fileId').val(fileId);
        $('#Winv007FormCriteria').submit();
    }else{
        errorTotal = $('#errorTotal');
        errorTotal.empty();
        errorTotal.html("<span class='ui-messageError'>"
            + $('#cannotDownloadMessage').val() + "</span>");
        $("#divError").show();
        return false;
    }
}
