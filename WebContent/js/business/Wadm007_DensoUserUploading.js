$(document).ready(function() {
    validateMenuCode();
    if(!checkNullOrEmpty($('#fileName').val())){
        $('#fileData').prop('disabled',true);
        $('#btnBrowse').prop('disabled',true);
        $('#btnUpload').prop('disabled',true);
    }else{
        $('#fileData').prop('disabled',false);
        $('#btnBrowse').prop('disabled',false);
        $('#btnUpload').prop('disabled',false);
    }
    
    $('#btnUpload').click(function() {
        $('#method').val('Upload');
        $('#methodResult').val('Upload');
        disableBeforeSubmit();
        $('#Wadm007FormCriteria').submit();
    });
     
    $('#btnReset').click(function() {
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotResetMessage').val())){
            $('#method').val('Reset');
            $('#methodResult').val('Reset');
            disableBeforeSubmit();
            $('#Wadm007FormCriteria').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    $('#btnRegister').click(function() {
        if(confirm(msgConfirmRegister.replace("{0}", labelRegisterProcess))){
            $('#method').val('Register');
            $('#methodResult').val('Register');
            disableBeforeSubmit();
            $('#Wadm007FormResult').submit();
        }
    });
});

function densoSetPathFile(){
    $('#txtUpload').val($('#fileData').val().split('\\').pop());
}
