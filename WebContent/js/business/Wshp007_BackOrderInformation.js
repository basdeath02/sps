$(document).ready(function() {
    validateMenuCode();
    var boxDensoCode = $("select#DCd");
    var boxDensoPlantCode = $("select#DPcd");
    var boxSupplierCode = $("select#SCd");
    var boxSupplierPlantCode = $("select#SPcd");
    
    $('#deliveryDateFrom').addClass('mandatory');
    $('#deliveryDateFrom').val($('#deliveryDateFrom').val());
    $('#deliveryDateTo').addClass("mandatory");
    $('#deliveryDateTo').val($('#deliveryDateTo').val());
    
    $('#btnSearch').click(function(){
        $('#method').val('Search');
        $('#methodResult').val('Search');
        $('#mode').val('search');
        disableBeforeSubmit();
        $('#Wshp007FormCriteria').submit();
    });
    
    $('#btnReset').click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotResetMessage').val())){
            $('#method').val('Reset');
            $('#mode').val('');
            disableBeforeSubmit();
            $('#Wshp007FormCriteria').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    $('#btnDownload').click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotDownloadMessage').val())){
            $('#method').val('Download');
            $('#Wshp007FormCriteria').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotDownloadMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    $("#moreLegend").click(function(){
        $('#methodDownload').val('More');
        $('#Wshp007FormDownload').submit();
    });
    
    boxDensoCode.unbind('change');
    boxDensoCode.change(function(){
        var errorTotal = '';
        var dCd = boxDensoCode.val();
        var boxDensoPlantValue = boxDensoPlantCode.val();
        
        $("#DPcd").removeAttr('disabled');
        var params = {
            "method"    : "doSelectDCD"
            ,"DCd"      : dCd
        };
        disableBeforeSubmit();
        
        $.ajax({
            url : "BackOrderInformationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                var plantList = data.jsonList1;
                boxDensoPlantCode.empty();
                if(plantList && plantList.length > 0){
                    boxDensoPlantCode.append($("<option></option>").val("ALL").html(labelAll));
                    $.each(plantList,function(index,val){
                        if(boxDensoPlantValue == val.DPcd){
                            boxDensoPlantCode.append( $("<option selected></option>").val(val.DPcd).text(val.DPcd));
                        }else{
                            boxDensoPlantCode.append( $("<option></option>").val(val.DPcd).text(val.DPcd));
                        }
                    });
                    
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }else{
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>"
                        + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    boxDensoPlantCode.append($("<option></option>").val("ALL").html(labelAll));
                }
                boxDensoPlantValue = "";
                enableAfterSubmit();
            },
            error: function() {
                enableAfterSubmit();
            }
        });
    });
    
    boxDensoPlantCode.unbind('change');
    boxDensoPlantCode.change(function(){
        var errorTotal = '';
        var boxDensoCdValue = boxDensoCode.val();
        var dPcd = boxDensoPlantCode.val();
        var params = {
             "method" : "doSelectDPCD"
             ,"DPcd"  : dPcd
        };
        disableBeforeSubmit();
         
        $.ajax({
            url : "BackOrderInformationAction.do",
            type : 'GET',
            async : false,
            data : params,
            dataType : 'json',
            success : function(data){
                var companyList = data.jsonList1;
                boxDensoCode.empty();
                if(companyList && companyList.length > 0){
                    boxDensoCode.append($("<option></option>").val("ALL").html(labelAll));
                    $.each(companyList,function(index,val){
                        if(boxDensoCdValue == val.DCd){
                            boxDensoCode.append($("<option selected></option>").val(val.DCd).text(val.DCd));
                        }else{
                            boxDensoCode.append($("<option></option>").val(val.DCd).text(val.DCd));
                        }
                    });
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }else{
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                    $("#divError").show();
             
                    boxDensoCode.append($("<option></option>").val("ALL").html(labelAll));
                }
                boxDensoCdValue = "";
                enableAfterSubmit();
            },
            error: function(){
                enableAfterSubmit();
            }
        });
    });
    
    boxSupplierCode.unbind('change');
    boxSupplierCode.change(function(){
        var errorTotal = '';
        var supplierCode = boxSupplierCode.val();
        var boxSupplierPlantValue = boxSupplierPlantCode.val();
        var params = {
            "method"     : "doSelectSCD"
            ,"vendorCd"  : supplierCode
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "BackOrderInformationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                var plantList = data.jsonList1;
                boxSupplierPlantCode.empty();
                if(plantList && plantList.length > 0){
                    boxSupplierPlantCode.append($("<option></option>").val("ALL").html(labelAll));
                    $.each(plantList,function(index,val){
                        if(boxSupplierPlantValue == val.SPcd){
                            boxSupplierPlantCode.append($("<option selected></option>").val(val.SPcd).text(val.SPcd));
                        }else{
                            boxSupplierPlantCode.append($("<option></option>").val(val.SPcd).text(val.SPcd));
                        }
                    });
                    
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }else{
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>"
                        + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    boxSupplierPlantCode.append($("<option></option>").val("ALL").html(labelAll));
                }
                boxSupplierPlantValue = "";
                enableAfterSubmit();
            },
            error: function() {
                enableAfterSubmit();
            }
        });
    });
    
    boxSupplierPlantCode.unbind('change');
    boxSupplierPlantCode.change(function(){
        var errorTotal = '';
        var boxVendorCdValue = boxSupplierCode.val();
        var sPcd = boxSupplierPlantCode.val();
        var params = {
            "method" : "doSelectSPCD"
            ,"SPcd"  : sPcd
        };
        disableBeforeSubmit();
        
        $.ajax({
            url : "BackOrderInformationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                var companyList = data.jsonList1;
                boxSupplierCode.empty();
                if(companyList && companyList.length > 0){
                    boxSupplierCode.append($("<option></option>").val("ALL").html(labelAll));
                    $.each(companyList,function(index,val){
                        if(boxVendorCdValue == val.vendorCd){
                            boxSupplierCode.append($("<option selected></option>").val(val.vendorCd).text(val.vendorCd));
                        }else{
                            boxSupplierCode.append($("<option></option>").val(val.vendorCd).text(val.vendorCd));
                        }
                    });
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                } else {
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    boxSupplierCode.append($("<option></option>").val("ALL").html(labelAll));
                }
                boxVendorCdValue = "";
                enableAfterSubmit();
            },
            error: function() {
                enableAfterSubmit();
            }
        });
    });
    
});

function wshp007ForwardPage(statusIndex){
    $('#methodResult').val('ForwardPage');
    $('#statusIndex').val(statusIndex);
    disableBeforeSubmit();
    $('#Wshp007FormResult').submit();
}
