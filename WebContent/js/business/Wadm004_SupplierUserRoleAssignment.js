$(document).ready(function() {
    
    validateMenuCode();
    $('#btnAdd').click(function() {
        var mode = 'Register';
        var companySupplierCode = $('#SCd').val();
        wadm004UpdateLink(mode, '', '', '', '', companySupplierCode, '','');
    });

    $('#btnDelete').click(function() {
        if(confirm(msgConfirm.replace("{0}", labelDelete))){
            $('#method').val('Delete');
            $('#methodResult').val('Delete');
            disableBeforeSubmit();
            $('#Wadm004FormResult').submit();
        }     
    });

    $('#btnReturn').click(function() {
        if(confirm(msgConfirm.replace("{0}", labelReturn))){
            $('#method').val('Return');
            $('#methodResult').val('Return');
            disableBeforeSubmit();
            $('#Wadm004FormResult').submit();
        }
    });
    
    var countRecord = parseInt($('#countDelete').val());
    wadm004ActivateDeleteBtn(countRecord);
    
});

function wadm004UpdateLink(mode, roleCode, effectStart, effectEnd, plantSupplierCode, companySupplierCode, lastUpdateDatetime, seqNo){
    if ('Edit' == mode && !checkNullOrEmpty($('#cannotUpdateMessage').val())) {
        var divError = $('#divError').show();
        var errorTotal = $('#errorTotal');
        errorTotal.empty();
        errorTotal.html("<span class='ui-messageError'>"
            + $('#cannotUpdateMessage').val() + "</span>");
        return false;
    }
    
    if ('Register' == mode && !checkNullOrEmpty($('#cannotAddMessage').val())) {
        var divError = $('#divError').show();
        var errorTotal = $('#errorTotal');
        errorTotal.empty();
        errorTotal.html("<span class='ui-messageError'>"
            + $('#cannotAddMessage').val() + "</span>");
        return false;
    }
    
    var dscId = $('#dscId').val();
    var url = './SupplierUserRoleAssignmentAction.do?method=doInitialPopup&dscId=' + dscId
    + "&mode=" + mode + "&roleCode=" + roleCode + "&effectStart=" + effectStart 
    + "&effectEnd=" + effectEnd + "&SPcd=" + plantSupplierCode + "&SCd=" 
    + companySupplierCode + "&isPopup=true";

var popupWindow = window.open(url, "Popup", "height=255,width=410");
// Wait for the popup window to close and handle the returned data
var intervalId = setInterval(function () {
    if (popupWindow.closed) {
        clearInterval(intervalId);

        // Access the data returned from the popup window
        var data = popupWindow.returnValue;
    if(data){
        var strDscId = "";
        var strMode = "";
        var strRoleCode = "";
        var strPlantSupplier = "";
        var strSupplierCode = "";
        var strEffectEnd = "";
        var strEffectStart = "";
            
        $.each(data, function(index, obj) {
            if(0 < index){
                strDscId         += ',';
                strMode          += ',';
                strRoleCode      += ',';
                strPlantSupplier += ',';
                strSupplierCode  += ',';
                strEffectEnd     += ',';
                strEffectStart   += ',';
            }
            strDscId += obj.dscId;
            strMode += obj.mode;
            strRoleCode += obj.roleCode;
            strPlantSupplier += obj.plantSupplierCode;
            strSupplierCode += obj.companySupplierCode;
            strEffectEnd  += obj.effectEnd;
            strEffectStart += obj.effectStart;
        });
            $("#dscId").val(strDscId);
            $("#roleCode").val(strRoleCode);
            $("#SCd").val(strSupplierCode);
            $("#SPcd").val(strPlantSupplier);
            $("#effectEnd").val(strEffectEnd);
            $("#effectStart").val(strEffectStart);
            $("#mode").val(strMode);
            $("#lastUpdateDatetimeScreen").val(lastUpdateDatetime);
            $("#seqNo").val(seqNo);
            $("#roleNameSelected").val(roleCode);
            
        if($("#mode").val() == "Edit"){
            $('#method').val('UpdateUser');
            $('#methodResult').val('UpdateUser');
        }else{
            $('#method').val('AddUser');
            $('#methodResult').val('AddUser');
        }
        disableBeforeSubmit();
        $('#Wadm004FormCriteria').submit();
    }
}
}, 500);
}

/*This function set all check box to checked or unchecked*/
function wadm004CheckAll(source){
    var rowChk = $("input[name='userRoleChkbox']").val();
    var countRecord = parseInt($('#countDelete').val());
    var selectedRec = 0;
    var row = 0;
    if(undefined != rowChk){
        checkboxes = document.getElementsByName('userRoleChkbox');
        for(var i = 0, n = checkboxes.length; i < n; i++){
            if(checkboxes[i].checked){
                selectedRec = selectedRec + 1;
            }
            checkboxes[i].checked = source.checked;
            if(source.checked){
                $('#roleCdSelected' + i).val(i);
                row = row + 1;
            }else{
                $('#roleCdSelected' + i).val('');
            }
        }
        countRecord = countRecord - selectedRec + row;
        wadm004ActivateDeleteBtn(countRecord);
    }
}
/* This function prepare data for delete supplier user role.*/
function wadm004setUserRoleList(row){
    var countRecord = parseInt($('#countDelete').val());
    if($('#checkUserRole' + row).attr('checked')){
        $('#roleCdSelected' + row).val(row);
        countRecord = countRecord + parseInt(1);
    }
    else{
        $('#roleCdSelected' + row).val('');
        countRecord = countRecord - parseInt(1);
    }
    wadm004ActivateDeleteBtn(countRecord);
}

function wadm004ActivateDeleteBtn(countRecord){
    if(0 < countRecord){
        $("#btnDelete").prop('disabled',false);
    }else{
        $("#btnDelete").prop('disabled',true);
    }
    $('#countDelete').val(countRecord);
}
