$(document).ready(function() {
     
     validateMenuCode();
    $('#btnUpload').click(function() {
       $('#method').val('Upload');
       disableBeforeSubmit();
       $('#Winv003Form').submit();
    });
   
    $('#btnReset').click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotResetMessage').val())){
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
            
            $('#method').val('Reset');
            $('#mode').val('');
            disableBeforeSubmit();
            $('#Winv003Form').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    $('#btnDownload').click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotDownloadMessage').val())){
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
            
            $('#methodDownload').val('Download');
            $('#Winv003FormDownload').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotDownloadMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    $('#btnGroup').click(function(){
        $('#methodResult').val('Group');
        disableBeforeSubmit();
        $('#Winv003FormResult').submit();
    });
});
function setPathFile(){
    $('#txtUpload').val($('#fileData').val().split('\\').pop());
}