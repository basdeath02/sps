$(document).ready(function() {
    validateMenuCode();
    var rowChk;
    var countRecord = parseInt($('#countDelete').val());
    wadm001ActivateDeleteBtn(countRecord);

    $('#btnReset').click(function() {
        if (!checkNullOrEmpty($('#cannotResetMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
        } else {
            $('#method').val('Reset');
            $('#methodResult').val('Reset');
            disableBeforeSubmit();
            $('#Wadm001FormCriteria').submit();
        }
    });

    $('#btnSearch').click(function() {
        $('#method').val('Search');
        $('#methodResult').val('Search');
        disableBeforeSubmit();
        $('#Wadm001FormCriteria').submit();
    });

    $('#btnDelete').click(function() {
        if(confirm(msgConfirmDelete.replace("{0}", labelDelete))){
            $('#method').val('Delete');
            $('#methodResult').val('Delete');
            disableBeforeSubmit();
            $('#Wadm001FormResult').submit();
        }
    });

    $('#btnDownload').click(function() {
        if (!checkNullOrEmpty($('#cannotDownloadCsvMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotDownloadCsvMessage').val() + "</span>");
        } else {
            $('#method').val('Download');
            $('#methodResult').val('Download');
            $('#Wadm001FormCriteria').submit();
        }
    });
    
    var boxSCd = $("select#SCd");
    var boxSPcd = $("select#SPcd");
    boxSCd.change(function(){
        var errorTotal = '';
        var boxSPcdValue = boxSPcd.val();
        var sCd = boxSCd.val();
        
        $("#SPcd").prop('disabled', false);
        var params = {
            "method"   : "doSelectSCD",
            "SCd"      : sCd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "SupplierUserInformationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data){
                if(!checkNullOrEmpty(data.jsonResult)){
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>"
                        + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    boxSPcd.append($("<option></option>").val("ALL").html(labelAll));
                }else{
                    var plantList = data.jsonList1;
                    boxSPcd.empty();
                    boxSPcd.append($("<option></option>").val("ALL").html(labelAll));
                    boxSPcd.append($("<option></option>").val(undefinedValue).html(labelUndefined));
                    $.each(plantList,function(index,val){
                        if(boxSPcdValue == val.SPcd){
                            boxSPcd.append($("<option selected></option>").val(val.SPcd).text(val.SPcd));
                        }else{
                            boxSPcd.append($("<option></option>").val(val.SPcd).text(val.SPcd));
                        }
                    });
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }
                boxSPcdValue = "";
                enableAfterSubmit();
                wadm001CheckEnableDeleteBtn();
            },
            error: function() {
                enableAfterSubmit();
                wadm001CheckEnableDeleteBtn();
            }
        });
    });
    
    boxSPcd.unbind('change');
    boxSPcd.change(function(){
        var errorTotal = '';
        var boxSCdValue = boxSCd.val();
        var sPcd = boxSPcd.val();
        var params = {
            "method" : "doSelectSPCD",
            "SPcd"   : sPcd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "SupplierUserInformationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                var companyList = data.jsonList1;
                boxSCd.empty();
                if(companyList && companyList.length > 0){
                    boxSCd.append($("<option></option>").val("ALL").html(labelAll));
                    $.each(companyList,function(index,val){
                        var sCd = doTrim(val.SCd);
                        if(boxSCdValue == val.SCd){
                            boxSCd.append($("<option selected></option>")
                                .val(val.SCd).text(sCd + " - " + val.companyName));
                        }else{
                            boxSCd.append($("<option></option>")
                                .val(val.SCd).text(sCd + " - " + val.companyName));
                        }
                    });
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }else{
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    boxSCd.append($("<option></option>").val("ALL").html(labelAll));
                }
                boxSCdValue = "";
                enableAfterSubmit();
                wadm001CheckEnableDeleteBtn();
            },
            error: function() {
                enableAfterSubmit();
                wadm001CheckEnableDeleteBtn();
            }
        });
    });
});

/*This function set all check box to checked or unchecked*/
function wadm001CheckAll(source){
    var rowChk = $("input[name='supplierChkbox']").val();
    var countRecord = parseInt($('#countDelete').val());
    var selectedRec = 0;
    var row = 0;
    if(undefined != rowChk){
        checkboxes = document.getElementsByName('supplierChkbox');
        for(var i = 0, n = checkboxes.length; i < n; i++){
             if(checkboxes[i].checked){
                 selectedRec = selectedRec + 1;
             }
            checkboxes[i].checked = source.checked;
            if(source.checked){
                $('#dscIdSelected' + i).val(i);
                row = row + 1;
            }else{
                $('#dscIdSelected' + i).val('');
            }
        }
        countRecord = countRecord - selectedRec + row;
        wadm001ActivateDeleteBtn(countRecord);
    }
}
/* This function prepare data for delete supplier user.*/
function wadm001setSupplierList(row){
    var countRecord = parseInt($('#countDelete').val());
    if($('#checkSupplier' + row).attr('checked')){
        $('#dscIdSelected' + row).val(row);
        countRecord = countRecord + parseInt(1);
    }
    else{
        $('#dscIdSelected' + row).val('');
        countRecord = countRecord - parseInt(1);
    }
    wadm001ActivateDeleteBtn(countRecord);
}

function wadm001ActivateDeleteBtn(countRecord){
    if(0 < countRecord){
        $("#btnDelete").prop('disabled',false);
    }else{
        $("#btnDelete").prop('disabled',true);
    }
    $('#countDelete').val(countRecord);
}

/* This function prepare data for open WADM002 screen(Edit mode).*/
function wadm001DscIdLink(dscId){
    $('#method').val('linkDscId');
    $('#methodResult').val('linkDscId');
    $('#dscIdSelected').val(dscId);
    disableBeforeSubmit();
    $('#Wadm001FormResult').submit();
} 

/* This function prepare data for open WADM004 screen.*/
function wadm001RoleLink(dscId, companySupplierCode, plantSupplierCode, firstName, middleName, lastName, companyName){
    $('#method').val('linkRole');
    $('#methodResult').val('linkRole');
    $('#dscIdSelected').val(dscId);
    $('#SCdSelected').val(companySupplierCode);
    $('#SPcdSelected').val(plantSupplierCode);
    $('#firstNameSelected').val(firstName);
    $('#middleNameSelected').val(middleName);
    $('#lastNameSelected').val(lastName);
    $('#companyName').val(companyName);
    disableBeforeSubmit();
    $('#Wadm001FormResult').submit();
} 

function wadm001SetDateFromTo(inputItemFromId, inputItemToId){
    var dateFrom = $('#' + inputItemFromId).val();
    $('#' + inputItemToId).val(dateFrom);
}

function wadm001CheckEnableDeleteBtn(){
    var countRecord = parseInt($('#countDelete').val());
    wadm001ActivateDeleteBtn(countRecord);
    return true;
}
