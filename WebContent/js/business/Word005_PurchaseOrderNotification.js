$(document).ready(function() {

    var errorMessage = $("span[id='errorTotal']:last").text();
    
    $('#btnReturn').click(function() {
        if (!checkNullOrEmpty($('#cannotReturnMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotReturnMessage').val() + "</span>");
        } else {
        	window.close();
        }
    });
    
});