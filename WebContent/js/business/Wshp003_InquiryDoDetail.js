$(document).ready(function() {
    validateMenuCode();
    $('#spsDoNo').addClass('readonly', true);
    $('#revision').addClass('readonly', true);
    $('#routeNo').addClass('readonly', true);
    $('#del').addClass('readonly', true);
    $("#spsDoNo").attr('readonly',true);
    $("#revision").attr('readonly',true);
    $("#routeNo").attr('readonly',true);
    $("#del").attr('readonly',true);
    
    $("#btnReturn").click(function(){
        $('#method').val('Return');
        $('#methodResult').val('Return');
        disableBeforeSubmit();
        $('#Wshp003FormResult').submit();
    });
    
    $("#moreLegend").click(function(){
        $('#methodResult').val('More');
        $('#Wshp003FormResult').submit();
    });
    
    if(0 < $('a:contains("Next")').length){
        $('a:contains("Next")').mouseover(function(){
            wshp003SetMethodDoInitial();
        });
    }
    if(0 < $('a:contains("Last")').length){
        $('a:contains("Last")').mouseover(function(){
            wshp003SetMethodDoInitial();
        });
    }
    if(0 < $('a:contains("First")').length){
        $('a:contains("First")').mouseover(function(){
            wshp003SetMethodDoInitial();
        });
    }
    if(0 < $('a:contains("Previous")').length){
        $('a:contains("Previous")').mouseover(function(){
            wshp003SetMethodDoInitial();
        });
    }
    $('[name=pageNum]').keypress(function(){
        wshp003SetMethodDoInitial();
    });
});
function wshp003SetMethodDoInitial(){
    $('#method').val('doInitial');
    $('#methodResult').val('doInitial');
}
