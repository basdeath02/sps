$(document).ready(function() {
    validateMenuCode();
    
    var countRecord = parseInt($('#countDelete').val());
    checkActivateDeleteBtn(countRecord);
    
    var boxDCd = $("select#DCd");
    var boxDPcd = $("select#DPcd");
    
    boxDCd.unbind('change');
    boxDCd.change(function(){
        var errorTotal = '';
        var boxDensoPlantVal = boxDPcd.val();
        var dCd = boxDCd.val();
        var params = {
            "method" : "doSelectDCD",
            "DCd"    : dCd
        };
        disableBeforeSubmit();
        $.ajax({
            url : "DensoUserInformationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                if(!checkNullOrEmpty(data.jsonResult)){
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>"
                        + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    boxDPcd.append( $("<option></option>").val("ALL").html(labelAll) );
                }else{
                    var plantList = data.jsonList1;
                    boxDPcd.empty();
                    
                    boxDPcd.append($("<option></option>").val("ALL").html(labelAll));
                    boxDPcd.append($("<option></option>").val("99").html(labelUndefined));
                    if(plantList && plantList.length > 0){
                        $.each(plantList,function(index,val){
                            if(boxDensoPlantVal == val.DPcd){
                                boxDPcd.append($("<option selected></option>").val(val.DPcd).text(val.DPcd));
                            }else{
                                boxDPcd.append($("<option></option>").val(val.DPcd).text(val.DPcd));
                            }
                        });
                    }
                    
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }
                boxDensoPlantVal = "";
                enableAfterSubmit();
                wadm005CheckEnableDeleteBtn();
            },
            error: function() {
                enableAfterSubmit();
                wadm005CheckEnableDeleteBtn();
            }
        });
    });
    
    boxDPcd.unbind('change');
    boxDPcd.change(function(){
        var errorTotal = '';
        var boxDCdValue = boxDCd.val();
        var dPcd = boxDPcd.val();
        var params = {
            "method" : "doSelectDPCD",
            "DPcd"   : dPcd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "DensoUserInformationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data){
                var companyList = data.jsonList1;
                boxDCd.empty();
                if(companyList && companyList.length > 0){
                    boxDCd.append($("<option></option>").val("ALL").html(labelAll));
                    $.each(companyList,function(index,val){
                        if(boxDCdValue == val.DCd){
                            boxDCd.append($("<option selected></option>").val(val.DCd).text(val.DCd));
                        }else{
                            boxDCd.append($("<option></option>").val(val.DCd).text(val.DCd));
                        }
                    });
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }else{
                   errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                   boxDCd.append($("<option></option>").val("ALL").html(labelAll));
                }
                boxDCdValue = "";
                enableAfterSubmit();
                wadm005CheckEnableDeleteBtn();
            },
            error: function(){
                enableAfterSubmit();
                wadm005CheckEnableDeleteBtn();
            }
        });
    });
    
    var rowChk; 
    $('#btnReset').click(function() {
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotResetMessage').val())){
            $('#method').val('Reset');
            disableBeforeSubmit();
            $('#Wadm005FormCriteria').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });

    $('#btnSearch').click(function() {
        $('#method').val('Search');
        $('#methodResult').val('Search');
        disableBeforeSubmit();
        $('#Wadm005FormCriteria').submit();
    });

    $('#btnDelete').click(function() {
        if(confirm(msgConfirmDelete.replace("{0}", labelDelete))){
            $('#method').val('Delete');
            $('#methodResult').val('Delete');
            disableBeforeSubmit();
            $('#Wadm005FormResult').submit();
        }
    });

    $('#btnDownload').click(function() {
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotDownloadMessage').val())){
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
            
            $('#method').val('Download');
            $('#Wadm005FormCriteria').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotDownloadMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });

});

/*This function set all check box to checked or unchecked*/
function densoCheckAll(source){
    var rowChk = $("input[name='densoChkbox']").val();
    var countRecord = parseInt($('#countDelete').val());
    var selectedRec = 0;
    var row = 0;
    if(undefined != rowChk){
        checkboxes = document.getElementsByName('densoChkbox');
        for(var i = 0, n = checkboxes.length; i < n; i++){
            if(checkboxes[i].checked){
                selectedRec = selectedRec + 1;
            }
            checkboxes[i].checked = source.checked;
            if(source.checked){
                $('#dscIdSelected' + i).val(i);
                row = row + 1;
            }else{
                $('#dscIdSelected' + i).val('');
            }
        }
        countRecord = countRecord - selectedRec + row;
        checkActivateDeleteBtn(countRecord);
    }
}
/* This function prepare data for delete DENSO user.*/
function setDensoList(row){
    var countRecord = parseInt($('#countDelete').val());
    if($('#checkDenso' + row).attr('checked')){
        $('#dscIdSelected' + row).val(row);
        countRecord = countRecord + parseInt(1);
    }
    else{
        $('#dscIdSelected' + row).val('');
        countRecord = countRecord - parseInt(1);
    }
    checkActivateDeleteBtn(countRecord);
}

function checkActivateDeleteBtn(countRecord){
    if(0 < countRecord){
        $("#btnDelete").prop('disabled',false);
    }else{
        $("#btnDelete").prop('disabled',true);
    }
    $('#countDelete').val(countRecord);
}

/* This function prepare data for open WADM006 screen(Edit mode).*/
function densoDscIdLink(dscId){
    $('#method').val('linkDscId');
    $('#methodResult').val('linkDscId');
    $('#dscIdSelected').val(dscId);
    disableBeforeSubmit();
    $('#Wadm005FormResult').submit();
} 

/* This function prepare data for open WADM008 screen.*/
function densoRoleLink(dscId, companyDensoCode, plantDensoCode, firstName, middleName, lastName){
    $('#method').val('linkRole');
    $('#methodResult').val('linkRole');
    $('#dscIdSelected').val(dscId);
    $('#DCdSelected').val(companyDensoCode);
    $('#DPcdSelected').val(plantDensoCode);
    $('#firstNameSelected').val(firstName);
    $('#middleNameSelected').val(middleName);
    $('#lastNameSelected').val(lastName);
    disableBeforeSubmit();
    $('#Wadm005FormResult').submit();
}

function wadm005SetDateFromTo(inputItemFromId, inputItemToId){
    var dateFrom = $('#' + inputItemFromId).val();
    $('#' + inputItemToId).val(dateFrom);
}
function wadm005CheckEnableDeleteBtn(){
    var countRecord = parseInt($('#countDelete').val());
    checkActivateDeleteBtn(countRecord);
    return true;
}