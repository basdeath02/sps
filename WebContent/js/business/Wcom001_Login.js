// From common.js
(function($) {
    $.fn.disable = function() {return this.attr('disabled', true);}
    $.fn.disableTxt = function() {
        $(this).css('background-color', '#e0e0e0');
        return this.attr('disabled', true);
    }
    $.fn.enable = function() {return this.attr('disabled', false).addClass('sbttn');}
    $.fn.enableTxt = function() {
        $(this).css('background-color', '');
        return this.attr('disabled', false);
    }
    // This function is used for enable button by checking its auth before
    // enable.
    $.fn.enableButtonWithAuth = function(idAuth){ 
        // alert(idButton+'/'+auth);
        var objAuth = document.getElementById(idAuth);
        if((null != objAuth) && (undefined != objAuth)){
        var hasAuth = objAuth.value;
         if('1' == hasAuth){
             // If user has authority for update data then the button
            // can be enabled.
             return this.attr('disabled', false).addClass('sbttn');
         }
        }
    }// End of checking enable button function.
})(jQuery);

function disableBeforeSubmit(){
    // Get all input type = button.
    var allButtons =  $(":button");
    if(allButtons != undefined){
    $.each(allButtons,function(idx,button){
        $(button).disable();
    });
    }
   
    var allLinks = $("a");
    if(allLinks != undefined){
    $.each(allLinks, function(idx,link){
        $(link).disable();
    });
    } 
  
}

/*
 * checkNullOrEmpty is used for checking value of input object. If input value
 * is NaN, undefined, empty or null, the result is "true" otherwise return
 * false. @param varInput @return true Or false
 */
function checkNullOrEmpty(varInput) {
    return ((varInput == NaN) || (varInput == undefined) || (varInput == null) || (varInput == ""));
}

// END from common.js


$(document).ready(function() {
	
    $('#userName').focus();
    /** Focus event is depend on error message. */
    
    var currText = $('.ui-state-error').text();
    if(!checkNullOrEmpty(currText)){
        if(msgPasswordIncorrect == currText){
            $('#password').focus();
        }else if(jQuery.trim(msgPasswordMandatory.replace('{0}', labelPassword)) 
            == jQuery.trim(currText)){
            $('#password').focus();
        }
    }

    $('#btnLogin').click(function() {
        $('#method').val('Login');
        disableBeforeSubmit();
        $('#Wcom001Form').submit();
    });

    /** Use on Permission Screen */
    $('#btnInitial').click(function() {
        $('#method').val('doInitial');
        disableBeforeSubmit();
        $('#Wcom001Form').submit();
    });

    // Test File Manager Stream
    $('#btnFileManager').click(function() {
        $('#method').val('Logout');
        disableBeforeSubmit();
        $('#Wcom001Form').submit();
    });
    
    /*this.reloadChildFrames = function(){alert("reloadChildFrames");
        var allFrames = document.getElementsByTagName("frame");
        for (var i = 0; i < allFrames.length; i++){
            var f = allFrames[i];
            f.contentDocument.location = f.src;
        }
    }*/
});

/** Support enter key */
$(document).keypress(function(event){

    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        $('#method').val('Login');
        disableBeforeSubmit();
        $('#Wcom001Form').submit();
    }
});

function reloadChildFrames(){alert("reloadChildFrames");
    var allFrames = document.getElementsByTagName("titleFrame");alert(allFrames.length);
    var frame = top.document.getElementById("frame");alert(frame);
    for (var i = 0; i < allFrames.length; i++){
        var f = allFrames[i];
        f.contentDocument.location = f.src;
    }
}

/** If session timeout program will redirect to login page. */
function breakoutOfFrame(){//alert(window.top.document.frames[0]);
    window.opener = window.dialogArguments;
    if(null != window.dialogArguments){
        window.close();
    }
    //parent.menuFrame.location.reload();
    
    //var frame = parent.document.getElementById("frame");
    //alert(frame);
    
    //alert(top.frames[0].document.body.clientHeight);
    window.top.location.href = 'Wcom001_Login_rev01.jsp';
    //top.location.reload();
    //window.top.location.href = window.top.location.href;
    //window.location.reload();
    //window.top.document.frames[0].location.reload();
    //window.top.document.frames[1].location.reload();
    //window.top.document.frames[2].location.reload();
    
    
    //window.parent.location.href = 'Wcom001_Login_rev01.jsp';
    //top.reload(true);
    //top.frames.document.location.href = top.frames.document.location.href;
    //alert(window.parent.location);
    //alert(window.self.location);
    //window.self.location.href = 'Wcom001_Login_rev01.jsp';
    //window.top.location.href = 'Wcom001_Login_rev01.jsp';
    //window.top.location.href = 'Wcom001_Login_rev01.jsp';
    //window.top.location.href = 'Wcom001_Login_rev01.jsp';
    //window.parent.location.href = 'Wcom001_Login_rev01.jsp';
    //window.parent.location.href = 'Wcom001_Login_rev01.jsp';
    //window.parent.location.href = 'Wcom001_Login_rev01.jsp';
    //top.frames.document.location.href=top.frames.document.location.href
    //top.location.href = 'Wcom001_Login_rev01.jsp';
    //top.frames["titleFrame"].location.reload();
    //top.frames["titleFrame"].location.href = 'Wcom001_Login_rev01.jsp';
    //window.parent.menuFrame.location.reload();
    //top.location.reload();
    //window.parent.frames[0].location.href = 'Wcom001_Login_rev01.jsp';
    //window.parent.frames[1].location.href = 'Wcom001_Login_rev01.jsp';
    //window.parent.frames[2].location.href = 'Wcom001_Login_rev01.jsp';
    //top.reloadChildFrames();
    
    /*window.close();
    window.parent.location.href = 'Wcom001_Login_rev01.jsp';
    window.top.location.href = 'Wcom001_Login_rev01.jsp';*/
    
    /*window.close();
    window.parent.location.href = 'Wcom001_Login_rev01.jsp';
    window.top.location.href = 'Wcom001_Login_rev01.jsp';*/
    //alert(window.parent.location.href);
    //window.close();
    //window.top.location.href = window.top.location.href;
    //parent.frames['titleFrame'].location.reload();
    
    //top.reload(true);
    //window.top.location.href = 'Wcom001_Login_rev01.jsp';
    //var allFrames = document.getElementsByTagName("titleFrame");
    //alert(allFrames.length);
    /*for (var i = 0; i < allFrames.length; i++){
        var f = allFrames[i];
        f.contentDocument.location = f.src;
    }*/
    //window.parent.location.href = 'Wcom001_Login_rev01.jsp';
}

/** Support multiple locale */
function changeLang(input){  
    $('#targetLocale').val(input);
    $('#method').val('doChangeLocale');
    $('#Wcom001Form').submit();
}

function favorite(){
    window.external.AddFavorite(location.href, document.title);
}