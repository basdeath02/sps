$(document).ready(function() {
    validateMenuCode();
    wshp004EnableReviseQtyBtn();
    
    $('#asnNo').addClass('readonly', true);
    $('#asnRcvStatus').addClass('readonly', true);
    $('#invStatus').addClass('readonly', true);
    $('#lastModified').addClass('readonly', true);
    
    $("#asnNo").prop('readonly',true);
    $("#asnRcvStatus").prop('readonly',true);
    $("#invStatus").prop('readonly',true);
    $("#lastModified").prop('readonly',true);
    
    $("#actualEtdDate").addClass('vAlignDate', true);
    $("#planEtaDate").addClass('vAlignDate', true);
    
    $('#btnSaveAndSend').prop('disabled',true);
    $('#btnViewPdf').prop('disabled',true);
    $('#btnDownloadPdf').prop('disabled',true);
    $('#btnCancelAsnAndSend').prop('disabled',true);
    
    var errorMessage = $("span[id='errorTotal']:last").text();
    
    var screenId = $('#screenId').val();
    if('WSHP001' == screenId && checkNullOrEmpty($('#mode').val())){
        $("#actualEtdDate").addClass('mandatory', true);
        $("#actualEtdTime").addClass('mandatory', true);
        $("#planEtaDate").addClass('mandatory', true);
        $("#planEtaTime").addClass('mandatory', true);
        $("#noOfPallet").addClass('mandatory', true);
        $("#tripNo").addClass('mandatory', true);
        
        $('#btnSaveAndSend').prop('disabled',false);
        $('#btnViewPdf').prop('disabled',false);
        $('#btnDownloadPdf').prop('disabled',true);
        $('#btnCancelAsnAndSend').prop('disabled',true);
        $("[id^='chgReasonTxt']").prop('disabled',true);
        
        wshp004SetChgReasonCbControl();
        wshp004EnableCalendar();
    }else if(('WSHP008' == screenId || 'WSHP009' == screenId)
        && checkNullOrEmpty($('#mode').val())){
        $('#btnDownloadPdf').removeAttr('disabled');
        $('#actualEtdTime').addClass('readonly', true);
        $('#actualEtdDate').addClass('readonly', true);
        $('#planEtaDate').addClass('readonly', true);
        $('#planEtaTime').addClass('readonly', true);
        $('#noOfPallet').addClass('readonly', true);
        $('#tripNo').addClass('readonly', true);
        
        $('#actualEtdTime').prop('readonly',true);
        $('#actualEtdDate').prop('readonly',true);
        $('#planEtaDate').prop('readonly',true);
        $('#planEtaTime').prop('readonly',true);
        $('#noOfPallet').prop('readonly',true);
        $('#tripNo').prop('readonly',true);
        
        $("[id^='shippingQtyByPartStr']").prop('readonly',true);
        $("[id^='chgReasonTxt']").prop('disabled',true);
        $('#btnSaveAndSend').prop('disabled',true);
        $('#btnViewPdf').prop('disabled',true);
        $('#btnDownloadPdf').prop('disabled',false);
        
        // [IN039] : allow user input Shipping Box Qty
        $("[id^='shippingBoxQtyByPartInput']").prop('readonly',true);
        
        wshp004DisableCalendar();
        wshp004EnableCancelAndSendBtn();
    }else if('WSHP006' == screenId && checkNullOrEmpty($('#mode').val())){
        $("#actualEtdDate").addClass('mandatory', true);
        $("#actualEtdTime").addClass('mandatory', true);
        $("#planEtaDate").addClass('mandatory', true);
        $("#planEtaTime").addClass('mandatory', true);
        $("#noOfPallet").addClass('mandatory', true);
        $("#tripNo").addClass('mandatory', true);
        
        $('#btnSaveAndSend').prop('disabled',false);
        $('#btnViewPdf').prop('disabled',false);
        $('#btnDownloadPdf').prop('disabled',true);
        $('#btnCancelAsnAndSend').prop('disabled',true);
        $("[id^='chgReasonTxt']").prop('disabled',true);
        
        wshp004SetChgReasonCbControl();
        wshp004EnableCalendar();
    }
    
    if('save' == $('#mode').val()){
        $('#actualEtdTime').addClass('readonly', true);
        $('#actualEtdDate').addClass('readonly', true);
        $('#planEtaDate').addClass('readonly', true);
        $('#planEtaTime').addClass('readonly', true);
        $('#noOfPallet').addClass('readonly', true);
        $('#tripNo').addClass('readonly', true);
        $('#actualEtdTime').prop('readonly',true);
        $('#actualEtdDate').prop('readonly',true);
        $('#planEtaDate').prop('readonly',true);
        $('#planEtaTime').prop('readonly',true);
        $('#noOfPallet').prop('readonly',true);
        $('#tripNo').prop('readonly',true);
        wshp004DisableCalendar();
        
        $('#btnSaveAndSend').prop('disabled',true);
        $('#btnViewPdf').prop('disabled',true);
        
        $("[id^='shippingQtyByPartStr']").prop('readonly',true);
        $("[id^='chgReasonTxt']").prop('disabled',true);
        $('#btnDownloadPdf').removeAttr('disabled');

        // [IN039] : allow user input Shipping Box Qty
        $("[id^='shippingBoxQtyByPartInput']").prop('readonly',true);
        
        wshp004EnableCancelAndSendBtn();
        
    }else if('cancel' == $('#mode').val()){
        $('#actualEtdTime').addClass('readonly', true);
        $('#actualEtdDate').addClass('readonly', true);
        $('#planEtaDate').addClass('readonly', true);
        $('#planEtaTime').addClass('readonly', true);
        $('#noOfPallet').addClass('readonly', true);
        $('#tripNo').addClass('readonly', true);
        $('#actualEtdTime').prop('readonly',true);
        $('#actualEtdDate').prop('readonly',true);
        $('#planEtaDate').prop('readonly',true);
        $('#planEtaTime').prop('readonly',true);
        $('#noOfPallet').prop('readonly',true);
        $('#tripNo').prop('readonly',true);
        
        $('#btnSaveAndSend').prop('disabled',true);
        $('#btnViewPdf').prop('disabled',true);
        $('#btnDownloadPdf').prop('disabled',false);
        $('#btnCancelAsnAndSend').prop('disabled',true);
        
        $("[id^='shippingQtyByPartStr']").prop('readonly',true);
        $("[id^='chgReasonTxt']").prop('disabled',true);

        // [IN039] : allow user input Shipping Box Qty
        $("[id^='shippingBoxQtyByPartInput']").prop('readonly',true);
        
        wshp004DisableCalendar();
        
    }
    
    wshp004EnableAllowReviseQtyBtn();
    wshp004SetActiveChkAllowRevise();
    wshp004CheckInitialError();
    wshp004CheckValidateLotSize();
    
    $('#btnSaveAndSend').click(function(){
        $('#skipValidateLotSizeFlag').val("0");
        $('#continueOperation').val("0");
        wshp004CallMethodSaveAndSend();
    });
    
    $('#btnCancelAsnAndSend').click(function(){
        var msgCancelAsn = msgConfirmCancel.replace('{0}', labelCancelAsn);
        if(confirm(msgCancelAsn)){
            $('#method').val('CancelAndSend');
            $('#methodResult').val('CancelAndSend');
//            $('#modeResult').val('cancel');
            $('#utcResult').val($('#utc').val());
            $('#asnNoResult').val($('#asnNo').val());
            $('#asnRcvStatusResult').val($('#asnRcvStatus').val());
            $('#invStatusResult').val($('#invStatus').val());
            $('#lastModifiedResult').val($('#lastModified').val());
            $('#actualEtdDateResult').val($('#actualEtdDate').val());
            $('#actualEtdTimeResult').val($('#actualEtdTime').val());
            $('#planEtaDateResult').val($('#planEtaDate').val());
            $('#planEtaTimeResult').val($('#planEtaTime').val());
            $('#noOfPalletResult').val($('#noOfPallet').val());
            $('#tripNoResult').val($('#tripNo').val());
            wshp004CopyToBackup();
            disableBeforeSubmit();
            $('#Wshp004FormResult').submit();
        }
        return false;
    });
    
    $('#btnReviseShippingQty').click(function(){
        var errorTotal = '';
        if(!checkNullOrEmpty($('#operateReviseQtyMsg').val())){
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#operateReviseQtyMsg').val() + "</span>");
            $("#divError").show();
            return false;
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
            
            var tabelObj = $("[id^='chgReasonTxt']").length;
            var reviseShippingQtyFlag = false;
            for(var i = 0; i < tabelObj; i++){
                if("1" == $('#revisingFlag' + i).val()){
                    $('#shippingQtyByPartStr' + i).prop('readonly', false);
                    $('#chgReasonTxt' + i).prop('disabled',false);
                    // [IN039] : allow user input Shipping Box Qty
                    $("#shippingBoxQtyByPartInput" + i).prop('readonly',false);
                    
                    reviseShippingQtyFlag = true;
                }else{
                    $('#shippingQtyByPartStr' + i).prop('readonly',true);
                    $('#chgReasonTxt' + i).prop('disabled',true);
                    // [IN039] : allow user input Shipping Box Qty
                    $("#shippingBoxQtyByPartInput" + i).prop('readonly',true);
                }
            }
            if(reviseShippingQtyFlag){
                $('#noOfPallet').prop('readonly',false);
                $("#noOfPallet").addClass('mandatory', true);
                $('#noOfPallet').removeClass('readonly');
                
                $('#btnViewPdf').prop('disabled', false);
                $('#btnDownloadPdf').prop('disabled',true);
                $('#btnSaveAndSend').prop('disabled',false);
            }
        }
    });
    
    $("#btnReturn").click(function(){
        if(checkNullOrEmpty($('#asnRcvStatus').val()) ||
            ("PTR" == $('#asnRcvStatus').val()
                && (checkNullOrEmpty($('#invStatus').val()) || "DCL" == $('#invStatus').val()))){
            if(confirm(msgConfirmReturn)){
                wshp004Return();
            }
        }else{
            wshp004Return();
        }
    });
    
    $("#moreLegend").click(function(){
        var errorTotal = '';
        errorTotal = $('#errorTotal');
        errorTotal.empty();
        $("#divError").hide();
        
        $('#method').val('More');
        $('#methodResult').val('More');
        $('#continueOperation').val("1");
        wshp004CopyToBackup();
        $('#Wshp004FormResult').submit();
    });
    
    $('#btnViewPdf').click(function(){
        if(0 != $('#divError').length){
            $('#divError').html('');
        }
        $('#continueOperation').val("0");
        $('#skipValidateLotSizeFlag').val("0");
        //Set Textbox color to default
        var objSelect = $("[id^='shippingQtyByPartStr']");
        for(var i = 0; i < objSelect.length; i++){
            $('#shippingQtyByPartStr' + i).removeClass('numberWarning').addClass('number', true);
        }
        wshp004CallMethodPreviewPdf();
    });
    
    $('#btnDownloadPdf').click(function(){
        var errorTotal = '';
        errorTotal = $('#errorTotal');
        errorTotal.empty();
        $("#divError").hide();
        
        $('#method').val('Download PDF');
        $('#methodResult').val('Download PDF');
        
        $('#continueOperation').val("1");
        $('#asnNoResult').val($('#asnNo').val());
        $('#asnRcvStatusResult').val($('#asnRcvStatus').val());
        $('#invStatusResult').val($('#invStatus').val());
        $('#lastModifiedResult').val($('#lastModified').val());
        $('#actualEtdDateResult').val($('#actualEtdDate').val());
        $('#actualEtdTimeResult').val($('#actualEtdTime').val());
        $('#planEtaDateResult').val($('#planEtaDate').val());
        $('#planEtaTimeResult').val($('#planEtaTime').val());
        $('#noOfPalletResult').val($('#noOfPallet').val());
        $('#tripNoResult').val($('#tripNo').val());
        wshp004CopyToBackup();
        $('#Wshp004FormResult').submit();
    });
    
    $('#btnAllowReviseQty').click(function(){
        $('#method').val('Allow Revise');
        $('#methodResult').val('Allow Revise');
        
        $('#asnNoResult').val($('#asnNo').val());
        $('#asnRcvStatusResult').val($('#asnRcvStatus').val());
        $('#invStatusResult').val($('#invStatus').val());
        $('#lastModifiedResult').val($('#lastModified').val());
        $('#actualEtdDateResult').val($('#actualEtdDate').val());
        $('#actualEtdTimeResult').val($('#actualEtdTime').val());
        $('#planEtaDateResult').val($('#planEtaDate').val());
        $('#planEtaTimeResult').val($('#planEtaTime').val());
        $('#noOfPalletResult').val($('#noOfPallet').val());
        $('#tripNoResult').val($('#tripNo').val());
        wshp004CopyToBackup();
        disableBeforeSubmit();
        $('#Wshp004FormResult').submit();
    });
});

function wshp004CallMethodSaveAndSend(){
    $('#method').val('SaveAndSend');
    $('#methodResult').val('SaveAndSend');
    
    $('#utcResult').val($('#utc').val());
    $('#asnNoResult').val($('#asnNo').val());
    $('#asnRcvStatusResult').val($('#asnRcvStatus').val());
    $('#invStatusResult').val($('#invStatus').val());
    $('#lastModifiedResult').val($('#lastModified').val());
    $('#actualEtdDateResult').val($('#actualEtdDate').val());
    $('#actualEtdTimeResult').val($('#actualEtdTime').val());
    $('#planEtaDateResult').val($('#planEtaDate').val());
    $('#planEtaTimeResult').val($('#planEtaTime').val());
    $('#noOfPalletResult').val($('#noOfPallet').val());
    $('#tripNoResult').val($('#tripNo').val());
    $('#temporaryModeResult').val($('#temporaryMode').val());
    $('#receiveByScanResult').val($('#receiveByScan').val());
    wshp004CopyToBackup();
    disableBeforeSubmit();
    $('#Wshp004FormResult').submit();
}

function wshp004CallMethodPreviewPdf(){
    $('#method').val('Download PDF Preview');
    $('#methodResult').val('Download PDF Preview');
    
    $('#utcResult').val($('#utc').val());
    $('#asnNoResult').val($('#asnNo').val());
    $('#asnRcvStatusResult').val($('#asnRcvStatus').val());
    $('#invStatusResult').val($('#invStatus').val());
    $('#lastModifiedResult').val($('#lastModified').val());
    $('#actualEtdDateResult').val($('#actualEtdDate').val());
    $('#actualEtdTimeResult').val($('#actualEtdTime').val());
    $('#planEtaDateResult').val($('#planEtaDate').val());
    $('#planEtaTimeResult').val($('#planEtaTime').val());
    $('#noOfPalletResult').val($('#noOfPallet').val());
    $('#tripNoResult').val($('#tripNo').val());
    $('#temporaryModeResult').val($('#temporaryMode').val());
    wshp004CopyToBackup();
    $('#Wshp004FormResult').submit();
}

function wshp004Return(){
     $('#method').val('Return');
     $('#methodResult').val('Return');
     
     $('#asnNoResult').val($('#asnNo').val());
     $('#asnRcvStatusResult').val($('#asnRcvStatus').val());
     $('#invStatusResult').val($('#invStatus').val());
     $('#lastModifiedResult').val($('#lastModified').val());
     $('#actualEtdDateResult').val($('#actualEtdDate').val());
     $('#actualEtdTimeResult').val($('#actualEtdTime').val());
     $('#planEtaDateResult').val($('#planEtaDate').val());
     $('#planEtaTimeResult').val($('#planEtaTime').val());
     $('#noOfPalletResult').val($('#noOfPallet').val());
     $('#tripNoResult').val($('#tripNo').val());
     wshp004CopyToBackup();
     disableBeforeSubmit();
     $('#Wshp004FormResult').submit();
}

function wshp004ReCalculateShipBoxQty(densoPartNo, index, indexByPart){
    if(checkNullOrEmpty($('#shippingQtyByPartStr' + indexByPart).val())){
        $('#shippingQtyByPartStr' + indexByPart).val('0');
    }
    
    //Check Scale
    var newShippingQty = 0.0;
    var qtyBox = parseFloat(removeCommaFromString($('#qtyBoxDiv' + densoPartNo).text()));
    var shippingQtyStr = removeCommaFromString($('#shippingQtyByPartStr' + indexByPart).val());
    var parts = shippingQtyStr.toString().split(".");
    if(undefined != parts[1]){
        shippingQtyStr = parseFloat(shippingQtyStr).toFixed(2).toString(); 
    }
    newShippingQty = parseFloat(shippingQtyStr);
    
    //var shippingQtyByPartOld = parseFloat($('#shippingQtyByPart' + densoPartNo).val());
    //var shippingQtyByPartNew = shippingQtyByPartOld - oldShippingQty + newShippingQty;
    var shippingQtyByPartNew = newShippingQty;
    var shippingBoxQty = Math.ceil(shippingQtyByPartNew / qtyBox);
    var shippingBoxQtyStr = addCommaStr((shippingBoxQty).toString());
    
    $('#shipBoxQtyDiv' + densoPartNo).html(shippingBoxQtyStr);
    $('#shippingBoxQtyStr' + densoPartNo).val(shippingBoxQtyStr);

    // [IN039] allow user input Shipping Box Qty when unit is KG or MT
    if (!$('#shippingBoxQtyByPartInput' + indexByPart).prop('readonly')) {
        $('#shippingBoxQtyByPartInput' + indexByPart).val(shippingBoxQtyStr);
    }
    
    $('#shippingQtyByPart' + indexByPart).val(newShippingQty);
    $('#shippingQtyByPartStr' + indexByPart).val(addCommaStr((newShippingQty).toString()));
    
    //$('#shippingQty' + index).val(newShippingQty);
    //$('#shippingQtyTxt' + index).val(newShippingQty);

    $("[id^='shippingBoxQty" + densoPartNo + "']").val(shippingBoxQty);
    
    if("0" == $('#allowReviseQtyFlag').val()){
        var totalRemainingQty = parseFloat($('#remainingQtyByPart' + indexByPart).val());
        if(newShippingQty < totalRemainingQty){// && 0.0 < newShippingQty
            wshp004EnableChgReasonCb(indexByPart);
        }else{
            wshp004DisableChgReasonCb(indexByPart);
        }
    }
}

function wshp004DisableChgReasonCb(index){
    $('#chgReasonTxt' + index).val('');
    $('#chgReasonTxt' + index).prop('disabled',true);
}

function wshp004EnableChgReasonCb(index){
    $('#chgReasonTxt' + index).prop('disabled',false);
}

function wshp004SetChangeReasonCbItem(value, index){
    $('#chgReason' + index).val(value);
    return false;
}

function wshp004SetSelectedValue(selectObj, valueToSet) {
  for (var i = 0; i < selectObj.options.length; i++) {
      if (selectObj.options[i].value == valueToSet) {
          selectObj.options[i].selected = true;
          return;
      }
  }
}
function wshp004EnableReviseQtyBtn(){
    var asnStatus = $('#asnRcvStatus').val();
    if((checkNullOrEmpty($('#invStatus').val()) || "DCL" == $('#invStatus').val())
        && !checkNullOrEmpty(asnStatus) && "CCL" != asnStatus && "RCP" != asnStatus
        && "1" == $('#allowReviseQtyFlag').val()){
        $('#btnReviseShippingQty').prop('disabled',false);
    }else{
        $('#btnReviseShippingQty').prop('disabled',true);
    }
}

function wshp004EnableCancelAndSendBtn(){
    if("ISS" == $('#asnRcvStatus').val()
        && (checkNullOrEmpty($('#invStatus').val()) || "DCL" == $('#invStatus').val())){
        $('#btnCancelAsnAndSend').prop('disabled',false);
    }else{
        $('#btnCancelAsnAndSend').prop('disabled',true);
    }
}

function wshp004SetChgReasonCbControl(){
    var objSelect = $("[id^='chgReasonTxt']");
    for(var i = 0; i < objSelect.length; i++){
        var totalRemainingQty = parseFloat($('#remainingQtyByPart' + i).val());
        var shippingQty = parseFloat(removeCommaFromString($('#shippingQtyByPartStr' + i).val()));
        if(shippingQty < totalRemainingQty){
            $('#chgReasonTxt' + i).prop('disabled',false);
        }else{
            wshp004DisableChgReasonCb(i);
        }
    }
}

function wshp004SetChgReasonCbBeforeSubmit(){
    var objSelect = $("[id^='chgReasonTxt']");
    for(var i = 0; i < objSelect.length; i++){
        if("1" == $('#revisingFlag' + i).val()){
            $('#chgReasonTxt' + i).prop('disabled',false);
            $('#shippingQtyByPartStr' + i).prop('readonly', false);
            // [IN039] : allow user input Shipping Box Qty
            $("#shippingBoxQtyByPartInput" + i).prop('readonly',false);
        }
    }
}

function wshp004DisableCalendar(){
    $("#calendar_actualEtdDate").prop('disabled',true);
    $("#recycle_actualEtdDate").prop('disabled',true);
    $("#calendar_planEtaDate").prop('disabled',true);
    $("#recycle_planEtaDate").prop('disabled',true);
}

function wshp004EnableCalendar(){
    $("#calendar_actualEtdDate").prop('disabled',false);
    $("#recycle_actualEtdDate").prop('disabled',false);
    $("#calendar_planEtaDate").prop('disabled',false);
    $("#recycle_planEtaDate").prop('disabled',false);
}

function wshp004CheckInitialError(){
    var tabelObj = $('#tblBody tr').length;
    if(0 == (tabelObj - 1)){
        $('#actualEtdTime').addClass('readonly', true);
        $('#actualEtdDate').addClass('readonly', true);
        $('#planEtaDate').addClass('readonly', true);
        $('#planEtaTime').addClass('readonly', true);
        $('#noOfPallet').addClass('readonly', true);
        $('#tripNo').addClass('readonly', true);
        
        $('#actualEtdTime').prop('readonly',true);
        $('#actualEtdDate').prop('readonly',true);
        $('#planEtaDate').prop('readonly',true);
        $('#planEtaTime').prop('readonly',true);
        $('#noOfPallet').prop('readonly',true);
        $('#tripNo').prop('readonly',true);
        wshp004DisableCalendar();
    }
}

function wshp004SetAllowReviseFlag(row){
    if($('#chkAllowReviseQty' + row).attr('checked')){
        $('#allowReviseQtyFlag' + row).val("1");
    }else{
        $('#allowReviseQtyFlag' + row).val("0");
    }
}

function wshp004EnableAllowReviseQtyBtn(){
    var asnStatus = $('#asnRcvStatus').val();
    if("PTR" == $('#asnRcvStatus').val()
        && (checkNullOrEmpty($('#invStatus').val()) || "DCL" == $('#invStatus').val())){
        $("[name='chkAllowReviseQty']").prop('disabled',false);
        $('#btnAllowReviseQty').prop('disabled',false);
    }else{
        $("[name='chkAllowReviseQty']").prop('disabled',true);
        $('#btnAllowReviseQty').prop('disabled',true);
    }
}

function wshp004SetActiveChkAllowRevise(){
    var objSelect = $("[name='chkAllowReviseQty']");
    var reviseCompleteAllRcvLane = true;
    for(var i = 0; i < objSelect.length; i++){
        var allowReviseFlag = $('#allowReviseQtyFlag' + i).val();
        if("1" == allowReviseFlag){
            $('#chkAllowReviseQty' + i).prop('checked', true);
        }else{
            $('#chkAllowReviseQty' + i).prop('checked', false);
        }
        if("1" == $('#reviseCompleteFlag' + i).val()){
            $('#chkAllowReviseQty' + i).prop('disabled',true);
        }else{
            reviseCompleteAllRcvLane = false;
        }
    }
    if(reviseCompleteAllRcvLane){
        $('#btnAllowReviseQty').prop('disabled',true);
        $('#btnReviseShippingQty').prop('disabled',true);
    }
}

function wshp004CheckValidateLotSize(){
    if("1" == $('#unmatchedLotSizeFlag').val()){
        if("0" == $('#continueOperation').val()){
            if(confirm(msgConfirmLotSize)){
                // call method transact save and send
                wshp004SetChgReasonCbBeforeSubmit();
                $('#continueOperation').val("1");
                $('#unmatchedLotSizeFlag').val("0")
                if("SaveAndSend" == $('#method').val()){
                    wshp004CallMethodSaveAndSend();
                }else if("Download PDF Preview" == $('#method').val()){
                    wshp004CallMethodPreviewPdf();
                }
            }else{
                setWarningLotSizeUnmatched();
            }
        }else{
            setWarningLotSizeUnmatched();
        }
    }
    return false;
}

function setWarningLotSizeUnmatched() {
    //call private method : set color text box
    var objSelect = $("[id^='shippingQtyByPartStr']");
    for(var i = 0; i < objSelect.length; i++){
        if("1" == $('#lotSizeNotMatchFlag' + i).val()){
            $('#shippingQtyByPartStr' + i).removeClass('number').addClass('numberWarning');
        }else{
            $('#shippingQtyByPartStr' + i).removeClass('numberWarning').addClass('number');
        }
    }
}

function wshp004CopyToBackup() {
    var objSelect = $("[id^='chgReasonTxt']");
    for(var i = 0; i < objSelect.length; i++){
        $('#chgReasonBackUp' + i).val($('#chgReasonTxt' + i).val());
        $('#chgReasonBackUp' + i).attr('disabled',false);
    }
}