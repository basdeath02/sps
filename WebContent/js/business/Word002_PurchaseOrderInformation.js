$(document).ready(function() {

    validateMenuCode();

    $('#issuedDateFrom').addClass("mandatory");
    $('#issuedDateTo').addClass("mandatory");
    if ("" !=  $('#issuedDateFromResult').val()) {
        $('#issuedDateFrom').val($('#issuedDateFromResult').val());
    }
    if ("" !=  $('#issuedDateToResult').val()) {
        $('#issuedDateTo').val($('#issuedDateToResult').val());
    }
    
    if('1' == $('#viewPdfCriteria').val()){
        $('#viewPdfChk').prop('checked',true);
    }else{
        $('#viewPdfChk').prop('checked',false);
    }
    
    //if (miscCodeAll ==  $('#viewPdfCombo').val()) {
    //    $("#btnDownload").prop('disabled',true);
    //} else {
    //    $("#btnDownload").prop('disabled',false);
    //}
    
    // Button -----------------------------------------------------------------------
    
    $('#btnSearch').click(function() {
        $('#method').val('Search');
        $('#methodResult').val('Search');
        disableBeforeSubmit();
        $('#Word002FormCriteria').submit();
    });
    
    $('#btnDownload').click(function() {
        if (!checkNullOrEmpty($('#cannotDownloadCsvMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotDownloadCsvMessage').val() + "</span>");
        } else {
            $('#method').val('Export');
            $('#methodResult').val('Export');
            //disableBeforeSubmit();
            $('#Word002FormCriteria').submit();
        }
    });
    
    $('#btnReset').click(function() {
        if (!checkNullOrEmpty($('#cannotResetMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
        } else {
            $('#method').val('Reset');
            disableBeforeSubmit();
            $('#Word002FormCriteria').submit();
        }
    });
    
    $('#btnAck').click(function() {
        $('#method').val('Acknowledge');
        $('#methodResult').val('Acknowledge');
        disableBeforeSubmit();
        $('#Word002FormResult').submit();
    });
    // ---------------------------------------------------------------------------------------------
    
    // Check Box -----------------------------------------------------------------------------------
    $("#checkAll").click(function(){
        if ($('#checkAll').prop('checked')) {
            $("[id^='chkPurchaseOrder']").prop('checked', true);
            word002SetActiveScreen();
        } else {
            $("[id^='chkPurchaseOrder']").prop('checked', false);
            word002SetActiveScreen();
        }
    });
    
    $('#btnAck').prop('disabled',true);
    
    $("#viewPdfChk").click(function(){
        if ($('#viewPdfChk').prop('checked')) {
            $('#viewPdfCriteria').val('1');
        }else{
            $('#viewPdfCriteria').val('');
        }
    });
    // ---------------------------------------------------------------------------------------------
    
    // DO Select Vendor Code -----------------------------------------------------------------------
    var boxVendorCd = $("select#vendorCd");
    var boxSPcd = $("select#SPcd");
    boxVendorCd.change(function(){
        var boxSupplierPlantValue = boxSPcd.val();
        var vendorCd = boxVendorCd.val();
        if("" != vendorCd){
            boxSPcd.removeAttr('disabled');
            var params = {
                "method" : "doSelectSCD"
                ,"vendorCd" : vendorCd
            };
            
            disableBeforeSubmit();
            var errorTotalSpan = '';
            $.ajax({
                url : "PurchaseOrderInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var plantList = data.jsonList1;
                    boxSPcd.empty();
                    if(plantList && plantList.length > 0){
                        boxSPcd.append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(plantList,function(index,val){
                            if(boxSupplierPlantValue == val.SPcd){
                                boxSPcd.append(
                                    $("<option selected></option>").val(val.SPcd).text(val.SPcd));
                            }else{
                                boxSPcd.append(
                                    $("<option></option>").val(val.SPcd).text(val.SPcd));
                            }
                        });
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    } else {
                        var divError = $('#divError').show();
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>"
                            + data.jsonResult + "</span>");
                        boxSPcd.append( $("<option></option>").val("ALL").html(labelAll) );
                    }
                    boxSupplierPlantValue = "";
                    enableAfterSubmit();
                    word002CheckEnableDownloadAndAcknowledge();
                },
                error: function() {
                    enableAfterSubmit();
                    word002CheckEnableDownloadAndAcknowledge();
                }
            });
        } else {
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
        }
    });
    // ---------------------------------------------------------------------------------------------

    // DO Select Supplier Plant Code ---------------------------------------------------------------
    boxSPcd.change(function(){
        var boxVendorCdValue = boxVendorCd.val();
        var SPcd = boxSPcd.val();
        if("" != SPcd){
            var params = {
                "method" : "doSelectSPCD"
                ,"SPcd"  : SPcd
            };
            
            disableBeforeSubmit();
            var errorTotalSpan = '';
            $.ajax({
                url : "PurchaseOrderInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var companyList = data.jsonList1;
                    boxVendorCd.empty();
                    if(companyList && companyList.length > 0){
                        boxVendorCd.append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(companyList,function(index,val){
                            if(boxVendorCdValue == val.vendorCd){
                                boxVendorCd.append($("<option selected></option>")
                                    .val(val.vendorCd).text(val.vendorCd));
                            }else{
                                boxVendorCd.append($("<option></option>")
                                    .val(val.vendorCd).text(val.vendorCd));
                            }
                        });
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    } else {
                        var divError = $('#divError').show();
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>"
                            + data.jsonResult + "</span>");
                        boxVendorCd.append( $("<option></option>").val("ALL").html(labelAll) );
                    }
                    boxSupplierPlantValue = "";
                    enableAfterSubmit();
                    word002CheckEnableDownloadAndAcknowledge();
                },
                error: function() {
                    enableAfterSubmit();
                    word002CheckEnableDownloadAndAcknowledge();
                }
            });
        }else{
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
        }
    });
    // ---------------------------------------------------------------------------------------------

    // DO Select DENSO Company Code ----------------------------------------------------------------
    var boxDCd = $("select#DCd");
    var boxDPcd = $("select#DPcd");
    boxDCd.change(function(){
        var boxDensoPlantVal = boxDPcd.val();
        var DCd = boxDCd.val();
        if("" != DCd){
            boxDPcd.removeAttr('disabled');
            var params = {
                "method" : "doSelectDCD"
                ,"DCd" : DCd
            };

            disableBeforeSubmit();
            var errorTotalSpan = '';
            $.ajax({
                url : "PurchaseOrderInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var plantList = data.jsonList1;
                    boxDPcd.empty();
                    if(plantList && plantList.length > 0){
                        boxDPcd.append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(plantList,function(index,val){
                            if(boxDensoPlantVal == val.DPcd){
                                boxDPcd.append(
                                    $("<option selected></option>").val(val.DPcd).text(val.DPcd));
                            }else{
                                boxDPcd.append(
                                    $("<option></option>").val(val.DPcd).text(val.DPcd));
                            }
                        });
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    } else {
                        var divError = $('#divError').show();
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>"
                            + data.jsonResult + "</span>");
                        boxDPcd.append( $("<option></option>").val("ALL").html(labelAll) );
                    }
                    boxDensoPlantVal = "";
                    enableAfterSubmit();
                    word002CheckEnableDownloadAndAcknowledge();
                },
                error: function() {
                    enableAfterSubmit();
                    word002CheckEnableDownloadAndAcknowledge();
                }
            });
        }else{
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
        }
    });
    // ---------------------------------------------------------------------------------------------

    // DO Select DENSO Plant Code ------------------------------------------------------------------
    boxDPcd.change(function(){
        var boxDensoCodeVal = boxDCd.val();
        var DPcd = boxDPcd.val();
        if("" != DPcd){
            var params = {
                "method" : "doSelectDPCD"
                ,"DPcd" : DPcd
            };

            disableBeforeSubmit();
            var errorTotalSpan = '';
            $.ajax({
                url : "PurchaseOrderInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var companyList = data.jsonList1;
                    boxDCd.empty();
                    if(companyList && companyList.length > 0){
                        boxDCd.append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(companyList,function(index,val){
                            if(boxDensoCodeVal == val.DCd){
                                boxDCd.append(
                                    $("<option selected></option>").val(val.DCd).text(val.DCd));
                            }else{
                                boxDCd.append(
                                    $("<option></option>").val(val.DCd).text(val.DCd));
                            }
                        });
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    } else {
                        var divError = $('#divError').show();
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>"
                            + data.jsonResult + "</span>");
                        boxDCd.append( $("<option></option>").val("ALL").html(labelAll) );
                    }
                    boxDensoPlantVal = "";
                    enableAfterSubmit();
                    word002CheckEnableDownloadAndAcknowledge();
                },
                error: function() {
                    enableAfterSubmit();
                    word002CheckEnableDownloadAndAcknowledge();
                }
            });
        }else{
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
        }
    });
    // ---------------------------------------------------------------------------------------------
    
    //$('#viewPdfCombo').change(function(){
    //    if (miscCodeAll ==  $('#viewPdfCombo').val()) {
    //        $("#btnDownload").prop('disabled',true);
    //    } else {
    //        $("#btnDownload").prop('disabled',false);
    //    }
    //});
    
    // jQuery cannot access onclick attribute in IE8 Campatibility mode
    //if(0 < $('[onclick^="pageClick("]').length){
    //    $('[onclick^="pageClick("]').mouseover(function(){
    //        word002SetMethodDoInitial();
    //    });
    //}
    if(0 < $('a:contains("Next")').length){
        $('a:contains("Next")').mouseover(function(){
            word002SetMethodDoInitial();
        });
    }
    if(0 < $('a:contains("Last")').length){
        $('a:contains("Last")').mouseover(function(){
            word002SetMethodDoInitial();
        });
    }
    if(0 < $('a:contains("First")').length){
        $('a:contains("First")').mouseover(function(){
            word002SetMethodDoInitial();
        });
    }
    if(0 < $('a:contains("Previous")').length){
        $('a:contains("Previous")').mouseover(function(){
            word002SetMethodDoInitial();
        });
    }
    $('[name=pageNum]').keypress(function(){
        word002SetMethodDoInitial();
    });
});

/* This function prepare data for open WORD003.*/
function word002OpenWORD003(poId, spsPoNo, periodType, periodTypeName, poType){
    $('#methodResult').val('doLinkSpsPoNo');
    $('#clickedPoId').val(poId);
    $('#clickedSpsPoNo').val(spsPoNo);
    $('#clickedPeriodType').val(periodType);
    $('#periodTypeName').val(periodTypeName);
    $('#clickedPoType').val(poType);
    disableBeforeSubmit();
    $('#Word002FormResult').submit();
} 

// Check BOX ---------------------------------------------------------------------------------
function word002SetActiveScreen() {
    var objSelect = $("[name='chkSelectPo']");
    for(var i = 0; i < objSelect.length; i++) {
        word002SetAckPOList(i);
    }
}

function word002CheckEnableAcknowledge() {
    if (0 == $("[id^='poIdSelected'][value!='']").length) {
        $('#btnAck').prop('disabled',true);
        $('#checkAll').prop('checked',false);
    } else {
        $('#btnAck').prop('disabled',false);
    }
    
    if ($("[name='chkSelectPo']").length == $("[id^='poIdSelected'][value!='']").length) {
        $('#checkAll').prop('checked' ,true);
    }
    return true;
}

function word002SetAckPOList(row){
    if ($('#chkPurchaseOrder'+ row).prop('checked')) {
        $('#poIdSelected'+ row).val($('#poId' + row).val());
        $('#poStatusSelected'+ row).val($('#poStatus' + row).val());
        word002CheckEnableAcknowledge();
    } else {
        $('#poIdSelected'+ row).val('');
        $('#poStatusSelected'+ row).val('');
        word002CheckEnableAcknowledge();
    }
}

function word002CheckEnableDownloadAndAcknowledge() {
    //if (miscCodeAll ==  $('#viewPdfCombo').val()) {
    //    $("#btnDownload").prop('disabled',true);
    //} else {
    //    $("#btnDownload").prop('disabled',false);
    //}
    if (0 == $("[id^='poIdSelected'][value!='']").length) {
        $('#btnAck').prop('disabled',true);
    } else {
        $('#btnAck').prop('disabled',false);
    }
    return true;
}

// DOWNLOAD SECTION ------------------------------------------------------------------------

function word002SetMethodDoInitial(){
    $('#method').val('Search');
    $('#methodResult').val('Search');
}

function word002DownloadPdfOriginal(originalFileId) {
    $('#methodResult').val('Download PDF Original');
    $('#fileIdSelected').val(originalFileId);
    //disableBeforeSubmit();
    $('#Word002FormResult').submit();
}

function word002DownloadPdfChange(changeFileId) {
    $('#methodResult').val('Download PDF Change');
    $('#fileIdSelected').val(changeFileId);
    //disableBeforeSubmit();
    $('#Word002FormResult').submit();
}

function word002DownloadLegendFile() {
    $('#methodResult').val('More');
    //disableBeforeSubmit();
    $('#Word002FormResult').submit();
}
