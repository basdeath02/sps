$(document).ready(function() {
    validateMenuCode();
    
    $('#btnRegister').attr('disabled',true);

    // Start : [IN054] Check screen mode when disabled Save Button and Accept/Reject combobox
    if (modeReply == $("#actionMode").val()) {
        $('#btnSave').attr('disabled', false);
        
        var objSelect = $("[name='chkPoDue']");
        for(var i = 0; i < objSelect.length; i++){
            if ($('#markPendingFlg' + i).val() == "1") {
                $('#densoReplyFlg' + i).attr('disabled',false);
            } else {
                $('#densoReplyFlg' + i).attr('disabled',true);
            }
        }
    } else {
        $('#btnSave').attr('disabled', true);
        $("[id^='densoReplyFlg']").attr('disabled', true);
    }
    // End : [IN054] Check screen mode when disabled Save Button and Accept/Reject combobox

    // [IN054] Add new button
    $('#btnSave').click(function() {
        $('#method').val('DoSave');
        word004CopyToBackup();
        disableBeforeSubmit();
        $('#Word004Form').submit();
    });

    $('#btnRegister').click(function() {
        $('#method').val('Register');
        word004CopyToBackup();
        disableBeforeSubmit();
        $('#Word004Form').submit();
    });

    $('#btnReturn').click(function() {
        word004CopyToBackup();
        if (0 == $("[id^='changePoDueFlag'][value='1']").length) {
            $('#method').val('Return');
            disableBeforeSubmit();
            $('#Word004Form').submit();
        } else {
            if (confirm(msgConfirmReturn)) {
                $('#method').val('Return');
                disableBeforeSubmit();
                $('#Word004Form').submit();
            }
        }
    });
    
    $("#chkall").click(function(){
        if ($('#chkall').attr('checked')) {
            $("[id^='chkPoDue']").attr('checked', true);
            $("[id^='changePoDueFlag']").val("1");
            $("[id^='stringProposedDate']").attr('disabled',false);
            $("[id^='calendar_stringProposedDate']").attr('disabled',false);
            $("[id^='recycle_stringProposedDate']").attr('disabled',false);
            $("[id^='stringProposedQty']").attr('disabled',false);
            $("[id^='spsPendingReasonCd']").attr('disabled',false);
            $('#btnRegister').attr('disabled',false);
        } else {
            $("[id^='chkPoDue']").attr('checked', false);
            $("[id^='changePoDueFlag']").val("0");
            $("[id^='stringProposedDate']").attr('disabled',true);
            $("[id^='calendar_stringProposedDate']").attr('disabled',true);
            $("[id^='recycle_stringProposedDate']").attr('disabled',true);
            $("[id^='stringProposedQty']").attr('disabled',true);
            $("[id^='spsPendingReasonCd']").attr('disabled',true);
            $('#btnRegister').attr('disabled',true);
        }
    });
    
    if (modeEdit == $("#actionMode").val()) {
        $(":checkbox").attr('disabled',false);
    } else {
        $(":checkbox").attr('disabled',true);
    }

    if (0 != $(varEnableCheckPoDueId).length) {
        $(varEnableCheckPoDueId).attr('checked', true);
        
        word004SetActiveScreen();
    } else {
        $("[id^='changePoDueFlag']").val("0");
        $("[id^='stringProposedDate']").attr('disabled',true);
        $("[id^='calendar_stringProposedDate']").attr('disabled',true);
        $("[id^='recycle_stringProposedDate']").attr('disabled',true);
        $("[id^='stringProposedQty']").attr('disabled',true);
        $("[id^='spsPendingReasonCd']").attr('disabled',true);
    }

});

function word004SetActivePoDue(row){
    if ($('#chkPoDue' + row).attr('checked')) {
        $('#changePoDueFlag' + row).val("1");
        $('#stringProposedDate' + row).attr('disabled',false);
        $("#calendar_stringProposedDate" + row).attr('disabled',false);
        $("#recycle_stringProposedDate" + row).attr('disabled',false);
        $('#stringProposedQty' + row).attr('disabled',false);
        $('#spsPendingReasonCd' + row).attr('disabled',false);
        word004CheckEnableRegister();
    } else {
        $('#changePoDueFlag' + row).val("0");
        $('#stringProposedDate' + row).attr('disabled',true);
        $("#calendar_stringProposedDate" + row).attr('disabled',true);
        $("#recycle_stringProposedDate" + row).attr('disabled',true);
        $('#stringProposedQty' + row).attr('disabled',true);
        $('#spsPendingReasonCd' + row).attr('disabled',true);
        word004CheckEnableRegister();
    }
}

function word004DownloadLegendFile() {
    $('#method').val('More');
    $('#Word004Form').submit();
}

function word004CheckEnableRegister() {
    if (0 == $("[id^='changePoDueFlag'][value='1']").length) {
        $('#btnRegister').attr('disabled' ,true);
        $('#chkall').attr('checked' ,false);
    } else {
        $('#btnRegister').attr('disabled',false);
    }

    if ($("[name='chkPoDue']").length == $("[id^='changePoDueFlag'][value='1']").length) {
        $('#chkall').attr('checked' ,true)
    }
}

function word004CopyToBackup() {
    var objSelect = $("[name='chkPoDue']");
    for(var i = 0; i < objSelect.length; i++){
        $('#stringProposedDateBackup' + i).val($('#stringProposedDate' + i).val());
        $('#stringProposedQtyBackup' + i).val($('#stringProposedQty' + i).val());
        $('#pendingReasonBackup' + i).val($('#spsPendingReasonCd' + i).val());

        // [IN054] Backup DENSO Reply Flag
        $('#acceptRejectBackup' + i).val($('#densoReplyFlg' + i).val());
        
        $('#stringProposedDateBackup' + i).attr('disabled',false);
        $('#stringProposedQtyBackup' + i).attr('disabled',false);
        $('#pendingReasonBackup' + i).attr('disabled',false);
    }
}

function word004SetActiveScreen() {
    var objSelect = $("[name='chkPoDue']");
    for(var i = 0; i < objSelect.length; i++) {
        word004SetActivePoDue(i);
    }
}
