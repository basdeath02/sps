var errorMessage = "";

$(document).ready(function() {
    
    // Initial
    var errorMessageTmp = $("#errorTmp").html();
    errorMessage = $("span[id='errorTotal']:last").text();
    
    validateMenuCode();
    $("#deliveryDateFrom").addClass('vAlignDate', true);
    $("#deliveryDateTo").addClass('vAlignDate', true);
    $("#shipDateFrom").addClass('vAlignDate', true);
    $("#shipDateTo").addClass('vAlignDate', true);
    $('#issueDateFrom').addClass("mandatory");
    $('#issueDateTo').addClass("mandatory");

    // Change Supplier Code -----------------------------------------------------------------------
    
    $("#select#sCd").unbind('change');
    $("#sCd").change(function() {
        var sCd = $('#sCd').val();
        if ("" != sCd) {
            $("#sPcd").removeAttr('disabled');
            var params = null;
            
            if($("#sCd").val() != "ALL"){
                params = {
                    "method"    : "doSelectSCD"
                    ,"vendorCd" : $("#sCd").val()
                };
            } else {
                params = {
                    "method"    : "doSelectSCD"
                    ,"vendorCd" : ""
                };
            }
            
            var printObj = typeof JSON !== "undefined" ? JSON.stringify : function( obj ) {
                var arr = [];
                $.each( obj, function( key, val ) {
                    var next = key + ": ";
                    next += $.isPlainObject( val ) ? printObj( val ) : val;
                    arr.push( next );
                });
                return "{ " +  arr.join( ", " ) + " }";
            };
            
            var errorTotalSpan = '';

            disableBeforeSubmit();
            $.ajax({
                 url : "KanbanOrderInformationAction.do",
                 type : 'GET',
                 async : false,
                 data: params,
                 dataType : 'json',
                 success : function(data) {
                     var plantList = data.jsonList1;
                     var selectedPlantSup = $("#sPcd").val();
                     $("#sPcd").empty();
                     if(data.jsonResult != '' && plantList && plantList.length > 0){
                        $("#sPcd").append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(plantList,function(index,val){
                            if(selectedPlantSup == val.SPcd){
                                $("#sPcd").append( $("<option selected></option>").val(val.SPcd).text(val.SPcd) );
                            }else{
                                $("#sPcd").append( $("<option></option>").val(val.SPcd).text(val.SPcd) );
                            }
                        });
                       
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    } else {
                        var divError = $('#divError').show();
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                         
                        $("#sPcd").append( $("<option></option>").val("ALL").html(labelAll) );
                     }
                     enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        } else {
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
        }
    });
    
    // Change Plant Supplier Code -----------------------------------------------------------------------
    $("#sPcd").change(function() {
        var sPcd = $('#sPcd').val();
        if ("" != sPcd) {
            $("#sCd").removeAttr('disabled');
            var params = null;
            
            if($("#sPcd").val() != "ALL"){
                params = {
                    "method" : "doSelectSPCD"
                    ,"SPcd"  : $("#sPcd").val()
                };
            } else {
                params = {
                    "method" : "doSelectSPCD"
                    ,"SPcd"  : ""
                };
            }
            
            var printObj = typeof JSON !== "undefined" ? JSON.stringify : function( obj ) {
                var arr = [];
                $.each( obj, function( key, val ) {
                    var next = key + ": ";
                    next += $.isPlainObject( val ) ? printObj( val ) : val;
                    arr.push( next );
                });
                return "{ " +  arr.join( ", " ) + " }";
            };
            
            var errorTotalSpan = '';

            disableBeforeSubmit();
            $.ajax({
                 url : "KanbanOrderInformationAction.do",
                 type : 'GET',
                 async : false,
                 data: params,
                 dataType : 'json',
                 success : function(data) {
                     var plantList = data.jsonList1;
                     var selectedSupplier = $("#sCd").val();
                     $("#sCd").empty();
                     if(data.jsonResult != '' && plantList && plantList.length > 0){
                        $("#sCd").append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(plantList,function(index,val){
                            if(selectedSupplier == val.vendorCd){
                                $("#sCd").append( $("<option selected></option>").val(val.vendorCd).text(val.vendorCd) );
                            }else{
                                $("#sCd").append( $("<option></option>").val(val.vendorCd).text(val.vendorCd) );
                            }
                        });
                        
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    } else {
                        var divError = $('#divError').show();
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                         
                        $("#sCd").append( $("<option></option>").val("ALL").html(labelAll) );
                     }
                     enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        } else {
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
        }
    });
    
    // Change Denso Code --------------------------------------------------------------------------
    $("#dCd").change(function() {
        var DENSOCode = $('#dCd').val();
        if ("" != DENSOCode) {
            $("#dPcd").removeAttr('disabled');
            
            var params = null;
            if($("#dCd").val() != "ALL"){
                params = {
                    "method" : "doSelectDCD"
                    ,"DCd"   : $("#dCd").val()
                };
            } else {
                params = {
                    "method" : "doSelectDCD"
                    ,"DCd"   : ""
                };
            }
            
            var printObj = typeof JSON !== "undefined" ? JSON.stringify : function( obj ) {
                var arr = [];
                $.each( obj, function( key, val ) {
                    var next = key + ": ";
                    next += $.isPlainObject( val ) ? printObj( val ) : val;
                    arr.push( next );
                });
                return "{ " +  arr.join( ", " ) + " }";
            };
                
            var errorTotalSpan = '';

            disableBeforeSubmit();
            $.ajax({
                 url : "KanbanOrderInformationAction.do",
                 type : 'GET',
                 async : false,
                 data: params,
                 dataType : 'json',
                 success : function(data) {
                     var plantList = data.jsonList1;
                     var selectedPlantDen = $("#dPcd").val();
                     $("#dPcd").empty();
                     if(data.jsonResult != '' && plantList && plantList.length > 0){
                        $("#dPcd").append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(plantList,function(index,val){
                            if(selectedPlantDen == val.DPcd){
                                $("#dPcd").append( $("<option selected></option>").val(val.DPcd).text(val.DPcd) );
                            }else{
                                $("#dPcd").append( $("<option></option>").val(val.DPcd).text(val.DPcd) );
                            }
                        });
                        
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    } else {
                        var divError = $('#divError').show();
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");

                        $("#sPcd").append( $("<option></option>").val("ALL").html(labelAll) );
                     }
                     enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
            
        }else{
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
        }
    });

    // Change Plant Denso Code --------------------------------------------------------------------------
    $("#dPcd").change(function() {
        var dPcd = $('#dPcd').val();
        if ("" != dPcd) {
            $("#dCd").removeAttr('disabled');
            
            var params = null;
            if($("#dPcd").val() != "ALL"){
                params = {
                    "method"  : "doSelectDPCD"
                    ,"DPcd"   : $("#dPcd").val()
                };
            } else {
                params = {
                    "method"  : "doSelectDPCD"
                    ,"DPcd"   : ""
                };
            }
            
            var printObj = typeof JSON !== "undefined" ? JSON.stringify : function( obj ) {
                var arr = [];
                $.each( obj, function( key, val ) {
                    var next = key + ": ";
                    next += $.isPlainObject( val ) ? printObj( val ) : val;
                    arr.push( next );
                });
                return "{ " +  arr.join( ", " ) + " }";
            };
                
            var errorTotalSpan = '';

            disableBeforeSubmit();
            $.ajax({
                 url : "KanbanOrderInformationAction.do",
                 type : 'GET',
                 async : false,
                 data: params,
                 dataType : 'json',
                 success : function(data) {
                     var plantList = data.jsonList1;
                     var selectedDensoCode = $("#dCd").val();
                     $("#dCd").empty();
                     if(data.jsonResult != '' && plantList && plantList.length > 0){
                        $("#dCd").append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(plantList,function(index,val){
                            if(selectedDensoCode == val.DCd){
                                $("#dCd").append( $("<option selected></option>").val(val.DCd).text(val.DCd) );
                            }else{
                                $("#dCd").append( $("<option></option>").val(val.DCd).text(val.DCd) );
                            }
                        });
                        
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    } else {
                        var divError = $('#divError').show();
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");

                        $("#dCd").append( $("<option></option>").val("ALL").html(labelAll) );
                     }
                     enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
            
        }else{
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
        }
    });
    
    $("#btnSearch").click(function() {
        $('#method').val('Search');
        $('#methodResult').val('Search');
        $('#mode').val('doSearch');
        disableBeforeSubmit();
        $('#Word007FormCriteria').submit();    
    });

    $('#btnReset').click(function() {
        if (!checkNullOrEmpty($('#cannotResetMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
        } else {
            $('#method').val('Reset');
            $('#mode').val('');
            disableBeforeSubmit();
            $('#Word007FormCriteria').submit();
        }
    });

    $('#btnDownload').click(function() {
        if (!checkNullOrEmpty($('#cannotDownloadCsvMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotDownloadCsvMessage').val() + "</span>");
        } else {
            $('#method').val('Download');
            $('#mode').val('doSearch');
            $('#Word007FormCriteria').submit();
        }
    });
    
    $("#moreLegend").click(function(){
            $('#method').val('More');
            $('#Word007FormCriteria').submit();
    });
     
});

function word007ViewPdf(fileId){
    $('#method').val('Download PDF');
    $('#pdfFileId').val(fileId);
    $('#Word007FormCriteria').submit();
}

function word007ViewKanbanPdf(doId,type,pdfFileKbTagPrintDate){
	if(pdfFileKbTagPrintDate != '' || $('#pdfFileKbTagPrintDateFlag' + doId).val() == 'Y'){
		alert(msgPrinted);
	}
	$('#method').val('Download One Way Kanban PDF');
    $('#doId').val(doId);
    $('#pdfType').val(type);
    $('#pdfFileKbTagPrintDate').val(pdfFileKbTagPrintDate == '' ? 'N' : 'Y');
    $('#pdfFileKbTagPrintDateFlag' + doId).val('Y');
    $('#Word007FormCriteria').submit();
}