$(document).ready(function() {
    validateMenuCode();
    
    var boxSCd = $("select#SCd");
    var boxSPcd = $("select#SPcd");
    var boxDensoOwner = $("select#densoOwner");
    var boxSupplierPlantValue = boxSPcd.val();
    
    //Write vendor code list into text area.
    if(!checkNullOrEmpty($("#maxVendorCd").val())){
        var strVendorCd = "";
        for(var i=0; i < $('#maxVendorCd').val(); i++){
            var dCd = $('#companyDensoCode' + i).val();
            strVendorCd += $('#vendorCode' + i).val() + " (" + doTrim(dCd) + ")";
            strVendorCd += "\n";
        }
        $("#vendorCd").html(strVendorCd);
    }
    
    $('#btnReset').click(function() {
        if (!checkNullOrEmpty($('#cannotResetMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
        } else {
            $('#method').val('Reset');
            $('#isReset').val(true);
            disableBeforeSubmit();
            $('#Wadm002Form').submit();
        }
    });
            
    $('#btnReturn').click(function() {
        if(confirm(msgConfirmReturn.replace("{0}", labelReturn))){
            wadm002SetEmailFlag();
            $('#method').val('Return');
            disableBeforeSubmit();
            $('#Wadm002Form').submit();
        }
    });
   
   $('#btnRegister').click(function() { 
        var mode = $('#mode').val();
        if(mode == 'Register'){
            $('#method').val('Register');
        }else{
            $('#method').val('Update');
        }
        wadm002SetEmailFlag();
        disableBeforeSubmit();
        $('#Wadm002Form').submit();
    });
   
    boxSCd.unbind('change');
    boxSCd.change(function(){
        var sCd = boxSCd.val();
        var params = {
            "method"       : "doSelectSCD",
            "SCdCriteria"  : sCd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "SupplierUserRegistrationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                if(!checkNullOrEmpty(data.jsonResult)){
                    var errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                    $('#divError').show();
                    // [IN052] Do not set Undefined as default Supplier Plant.
                    boxSPcd.append($("<option></option>").val("").html(labelPleaseSelect));
                    boxSPcd.append($("<option></option>").val(undefinedValue).html(labelUndefined));
                }else{
                    var plantList = data.jsonList1;
                    boxSPcd.empty();
                    // [IN052] Do not set Undefined as default Supplier Plant.
                    //boxSPcd.append($("<option></option>").val(undefinedValue).html(labelUndefined));
                    boxSPcd.append($("<option></option>").val("").html(labelPleaseSelect));
                    
                    if(plantList && plantList.length > 0){
                        $.each(plantList,function(index,val){
                            if(boxSupplierPlantValue == val.SPcd){
                                boxSPcd.append($("<option selected></option>").val(val.SPcd).text(val.SPcd));
                            }else{
                                boxSPcd.append($("<option></option>").val(val.SPcd).text(val.SPcd));
                            }
                        });
                    }
                    // [IN052] Do not set Undefined as default Supplier Plant.
                    boxSPcd.append($("<option></option>").val(undefinedValue).html(labelUndefined));
                    
                    var errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }
                boxSupplierPlantValue = "";
                enableAfterSubmit();
                wadm002CheckEnableButton();
                
                //Set list of Vendor Code.
                var vendorList = data.jsonList2;
                var strVendorCd = "";
                $.each(vendorList,function(index,val){
                    strVendorCd += val.vendorCd + " (" + doTrim(val.DCd) + ")";
                    strVendorCd += "\n";
                });
                $("#vendorCd").html(strVendorCd);
            },
            error: function() {
                enableAfterSubmit();
                wadm002CheckEnableButton();
            }
        });
    });
    
    boxSPcd.unbind('change');
    boxSPcd.change(function(){
        var errorTotal = '';
        var boxSCdValue = boxSCd.val();
        var sPcd = boxSPcd.val();
        var params = {
            "method" : "doSelectSPCD",
            "SPcd"   : sPcd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "SupplierUserRegistrationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                var companyList = data.jsonList1;
                boxSCd.empty();
                if(companyList && companyList.length > 0){
                    // [IN052] Do not set Undefined as default Supplier Plant.
                    boxSCd.append($("<option></option>").val("").html(labelPleaseSelect));
                    
                    $.each(companyList,function(index,val){
                        var sCd = doTrim(val.SCd);
                        if(boxSCdValue == val.SCd){
                            boxSCd.append($("<option selected></option>")
                                .val(val.SCd).text(sCd + " - " + val.companyName));
                        }else{
                            boxSCd.append($("<option></option>")
                                .val(val.SCd).text(sCd + " - " + val.companyName));
                        }
                    });
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }else{
                    // [IN052] Do not set Undefined as default Supplier Plant.
                    boxSCd.append($("<option></option>").val("").html(labelPleaseSelect));
                    
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                    $("#divError").show();
                }
                boxSCdValue = "";
                enableAfterSubmit();
                wadm002CheckEnableButton();
            },
            error: function() {
                enableAfterSubmit();
                wadm002CheckEnableButton();
            }
        });
    });
    
   var departmentTags = [];
   for(var i=0; i < $('#maxDepartment').val(); i++){
       departmentTags.push($('#dept' + i).val());
   }
   
    $('#departmentName').autocomplete({
        source: departmentTags
    });

});

function wadm002CheckEnableButton(){
    if('Register' == $('#mode').val()){
        $('#btnReturn').attr('disabled',true);
    }else{
        $('#btnReturn').removeAttr('disabled');
    }
    
    if('true' == $('#isDCompany').val()){
        $('#btnRegister').removeAttr('disabled');
        $('#btnReset').removeAttr('disabled');
    }else{
        $('#btnRegister').attr('disabled',true);
        $('#btnReset').attr('disabled',true);
    }
}

function wadm002SetEmailFlag(){
    var emlUrgentchk = document.getElementById('emlUrgentchk');
    var emlInfochk = document.getElementById('emlInfochk');
    
    if(null != emlUrgentchk){
        if(emlUrgentchk.checked){
            $('#emlUrgentFlag').val('1');
        }else{
            $('#emlUrgentFlag').val('0');
        }
    }
    
    if(null != emlInfochk){
        if(emlInfochk.checked){
            $('#emlInfoFlag').val('1');
        }else{
            $('#emlInfoFlag').val('0');
        }
    }
    
    if($('#emlAllowReviseChg').attr('checked')){
        $('#emlAllowReviseFlag').val('1');
    }else{
        $('#emlAllowReviseFlag').val('0');
    }
    
    if($('#emlCancelInvoiceChg').attr('checked')){
        $('#emlCancelInvoiceFlag').val('1');
    }else{
        $('#emlCancelInvoiceFlag').val('0');
    }
}