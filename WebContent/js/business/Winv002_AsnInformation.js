$(document).ready(function() {
    validateMenuCode();
    
    var boxDensoCode = $("select#DCd");
    var boxDensoPlantCode = $("select#DPcd");
    var boxDensoPlantValue = boxDensoPlantCode.val();
    var boxSupplierCode = $("select#SCd");
    var boxSupplierPlantCode = $("select#SPcd");
    var boxSupplierPlantValue = boxSupplierPlantCode.val();
    
    $("#actualEtdFromCriteria").addClass('vAlignDate', true);
    $("#actualEtdToCriteria").addClass('vAlignDate', true);
    $('#planEtaFromCriteria').addClass('mandatory');
    $('#planEtaFromCriteria').val($('#planEtaFrom').val());
    $('#planEtaToCriteria').addClass("mandatory");
    $('#planEtaToCriteria').val($('#planEtaTo').val());
    $('#actualEtdFromCriteria').val($('#actualEtdFrom').val());
    $('#actualEtdToCriteria').val($('#actualEtdTo').val());
    
    var countRecord = parseInt($('#countGroupAsn').val());
    winv002ActivateGroupAsnBtn(countRecord);
    
    winv002ActivateCheckAll();
    
    $('#btnSearch').click(function(){
        $('#method').val('Search');
        $('#methodResult').val('Search');
        $('#mode').val('search');
        disableBeforeSubmit();
        $('#Winv002FormCriteria').submit();
    });
    
    $('#btnReset').click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotResetMessage').val())){
            $('#method').val('Reset');
            $('#mode').val('');
            disableBeforeSubmit();
            $('#Winv002FormCriteria').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    $('#btnGroup').click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotGroupMessage').val())){
            $('#methodResult').val('Group');
            disableBeforeSubmit();
            $('#Winv002FormResult').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotGroupMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    $("#moreLegend").click(function(){
        $('#methodDownload').val('More');
        $('#Winv002FormDownload').submit();
    });
    
    // Change Company Denso -----------------------------------------------------------------------
    boxDensoCode.unbind('change');
    boxDensoCode.change(function(){
        var densoCode = boxDensoCode.val();
        if("" != densoCode){
            var params = {
                "method"             : "doSelectDCD"
                ,"DCd"                : boxDensoCode.val()
            };
            
            var errorTotal = '';
            disableBeforeSubmit();
            $.ajax({
                url : "AsnInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    boxDensoPlantValue = boxDensoPlantCode.val();
                    var plantList = data.jsonList1;
                    boxDensoPlantCode.empty();
                    if(plantList && plantList.length > 0){
                        boxDensoPlantCode.append($("<option></option>").val("ALL").html(labelAll) );
                        $.each(plantList,function(index,val){
                            if(boxDensoPlantValue == val.DPcd){
                                boxDensoPlantCode.append( $("<option selected></option>").val(val.DPcd).text(val.DPcd) );
                            }else{
                                boxDensoPlantCode.append( $("<option></option>").val(val.DPcd).text(val.DPcd) );
                            }
                        });
                        
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    }else{
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>"
                            + data.jsonResult + "</span>");
                        $("#divError").show();

                        boxDensoPlantCode.append($("<option></option>").val("ALL").html(labelAll) );
                    }
                    boxDensoPlantValue = "";
                    enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        }else{
            boxDensoPlantValue = "";
        }
    });

    // Change Plant Denso -----------------------------------------------------------------------
    boxDensoPlantCode.unbind('change');
    boxDensoPlantCode.change(function(){
        var DPcd = boxDensoPlantCode.val();
        if("" != DPcd){
            var params = {
                "method"             : "doSelectDPCD"
                ,"DPcd"              : boxDensoPlantCode.val()
            };
            
            var errorTotal = '';
            disableBeforeSubmit();
            $.ajax({
                url : "AsnInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var boxDensoCompanyValue = boxDensoCode.val();
                    var companyList = data.jsonList1;
                    boxDensoCode.empty();
                    if(companyList && companyList.length > 0){
                    	boxDensoCode.append($("<option></option>").val("ALL").html(labelAll) );
                        $.each(companyList,function(index,val){
                            if(boxDensoCompanyValue == val.DCd){
                            	boxDensoCode.append( $("<option selected></option>").val(val.DCd).text(val.DCd) );
                            }else{
                            	boxDensoCode.append( $("<option></option>").val(val.DCd).text(val.DCd) );
                            }
                        });
                        
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    }else{
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>"
                            + data.jsonResult + "</span>");
                        $("#divError").show();

                        boxDensoCode.append($("<option></option>").val("ALL").html(labelAll) );
                    }
                    enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        }
    });
    
    // Change Company Supplier --------------------------------------------------------------------
    boxSupplierCode.unbind('change');
    boxSupplierCode.change(function(){
        var supplierCode = boxSupplierCode.val();
        if("" != supplierCode){
            var params = {
                "method"                 : "doSelectSCD"
                ,"vendorCd"              : boxSupplierCode.val()
            };
            
            var errorTotal = '';
            $.ajax({
                url : "AsnInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var plantList = data.jsonList1;
                    boxSupplierPlantValue = boxSupplierPlantCode.val();
                    boxSupplierPlantCode.empty();
                    if(plantList && plantList.length > 0){
                        boxSupplierPlantCode.append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(plantList,function(index,val){
                            if(boxSupplierPlantValue == val.SPcd){
                                boxSupplierPlantCode.append( $("<option selected></option>").val(val.SPcd).text(val.SPcd) );
                            }else{
                                boxSupplierPlantCode.append( $("<option></option>").val(val.SPcd).text(val.SPcd) );
                            }
                        });
                        
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    }else{
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>"
                            + data.jsonResult + "</span>");
                        $("#divError").show();

                        boxSupplierPlantCode.append( $("<option></option>").val("ALL").html(labelAll) );
                    }
                    boxSupplierPlantValue = "";
                }
            });
        }else{
            boxSupplierPlantValue = "";
        }
    });

    // Change Company Supplier --------------------------------------------------------------------
    boxSupplierPlantCode.unbind('change');
    boxSupplierPlantCode.change(function(){
        var SPcd = boxSupplierPlantCode.val();
        if("" != SPcd){
            var params = {
                "method"                 : "doSelectSPCD"
                ,"SPcd"                  : boxSupplierPlantCode.val()
            };
            
            var errorTotal = '';
            $.ajax({
                url : "AsnInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var companyList = data.jsonList1;
                    var boxSupplierCodeValue = boxSupplierCode.val();
                    boxSupplierCode.empty();
                    if(companyList && companyList.length > 0){
                    	boxSupplierCode.append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(companyList,function(index,val){
                            if(boxSupplierCodeValue == val.vendorCd){
                            	boxSupplierCode.append( $("<option selected></option>").val(val.vendorCd).text(val.vendorCd) );
                            }else{
                            	boxSupplierCode.append( $("<option></option>").val(val.vendorCd).text(val.vendorCd) );
                            }
                        });
                        
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    }else{
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>"
                            + data.jsonResult + "</span>");
                        $("#divError").show();

                        boxSupplierCode.append( $("<option></option>").val("ALL").html(labelAll) );
                    }
                    boxSupplierPlantValue = "";
                }
            });
        }else{
            boxSupplierPlantValue = "";
        }
    });
    
});
function winv002SetSelected(rowNo){
    var countRecord = parseInt($('#countGroupAsn').val());
    if($('#checkASN' + rowNo).attr('checked')){
        $('#asnSelected' + rowNo).val(rowNo);
        countRecord = countRecord + parseInt(1);
    }
    else{
        $('#asnSelected' + rowNo).val('');
        countRecord = countRecord - parseInt(1);
    }
    winv002ActivateGroupAsnBtn(countRecord);
}
function winv002AllChecked(source){
    var rowChk = $("input[name='doChkbox']").val();
    var totalRecord = parseInt($('#countGroupAsn').val());
    if(undefined != rowChk){
        checkboxes = document.getElementsByName('doChkbox');
        for(var i = 0, n = checkboxes.length; i < n; i++){
            checkboxes[i].checked = source.checked;
            if(source.checked){
                $('#asnSelected' + i).val(i);
                totalRecord = totalRecord + 1;
            }else{
                $('#asnSelected' + i).val('');
                totalRecord = totalRecord - 1;
            }
        }
        winv002ActivateGroupAsnBtn(totalRecord);
    }
}
function winv002ActivateGroupAsnBtn(countRecord){
    if(0 < countRecord){
        $("#btnGroup").prop('disabled',false);
    }else{
        $("#btnGroup").prop('disabled',true);
    }
    $('#countGroupAsn').val(countRecord);
}
function winv002ActivateCheckAll(){
    var rowChk = $("input[name='doChkbox']").val();
    var checkboxes = document.getElementsByName('doChkbox');
    var checkAll = true;
    if(undefined != rowChk){
        for(var i = 0, n = checkboxes.length; i < n; i++){
            if(!checkboxes[i].checked){
                checkAll = false;
            }
        }
        if(checkAll){
            $("#checkboxAll").prop("checked", true)
        }
    }
}
