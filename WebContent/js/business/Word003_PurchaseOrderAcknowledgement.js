$(document).ready(function() {
    validateMenuCode();
    
    $('#btnPreviewPending').click(function() {
        if (!checkNullOrEmpty($('#cannotPreviewPendingMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotPreviewPendingMessage').val() + "</span>");
        } else {
            word003OpenPopup();
        }
    });
    
    $('#btnSaveSend').click(function() {
        if (confirm(msgConfirmSaveAndSend)) {
            $('#method').val('SaveAndSend');
            disableBeforeSubmit();
            $('#Word003Form').submit();
        }
    });
    
    $('#btnReturn').click(function() {
        $('#method').val('Return');
        disableBeforeSubmit();
        $('#Word003Form').submit();
    });
    
    // [IN054] Add new button
    $('#btnDensoReply').click(function() {
        $('#method').val('DENSO Reply');
        disableBeforeSubmit();
        $('#Word003Form').submit();
    });
    
});

/* This function prepare data for open WORD004.*/
function word003OpenWORD004(supplierPartNo, densoPartNo, actionMode){
    $('#method').val('doLinkActionMode');
    $('#selectedSPn').val(supplierPartNo);
    $('#selectedDPn').val(densoPartNo);
    $('#selectedActionMode').val(actionMode);
    disableBeforeSubmit();
    $('#Word003Form').submit();
}

function word003OpenPopup(){
    window.showModalDialog('./PurchaseOrderAcknowledgementAction.do?method=PreviewPending&poId='
        + $('#poId').val() + '&spsPoNo=' + $('#spsPoNo').val()
        , window,"dialogHeight: 450px; dialogWidth:930px;");
}

function word003DownloadLegendFile() {
    $('#methodDownload').val('More');
    //disableBeforeSubmit();
    $('#Word003FormDownload').submit();
}
//function setAckPOList(row){
//    if ($('#chkPurchaseOrder'+ row).attr('checked')) {
//        $('#poIdSelected'+ row).val($('#poId' + row).val());
//        $('#poStatusSelected'+ row).val($('#poStatus' + row).val());
//        $('#lastUpdateDatetimeSelected'+ row).val($('#lastUpdateDatetime' + row).val());
//    } else {
//        $('#poIdSelected'+ row).val('');
//        $('#poStatusSelected'+ row).val('');
//        $('#lastUpdateDatetimeSelected'+ row).val('');
//    }
//}

