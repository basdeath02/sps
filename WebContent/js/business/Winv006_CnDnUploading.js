$(document).ready(function() {
    validateMenuCode();
    
     $('#btnUpload').click(function() {
        $('#method').val('Upload');
        disableBeforeSubmit();
        $('#Winv006Form').submit();
    });
    
    $('#btnReset').click(function() {
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotResetMessage').val())){
            $('#method').val('Reset');
            $('#mode').val('');
            disableBeforeSubmit();
            $('#Winv006Form').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    var boxSupplierCode = $("select#supplierCompanyCode");
    var boxSupplierPlantCode = $("select#supplierPlantCode");
    var boxDensoCode = $("select#dCd");
    var boxDensoPlantCode = $("select#dPcd");
    
    var printObj = typeof JSON !== "undefined" ? JSON.stringify : function( obj ) {
        var arr = [];
        $.each( obj, function( key, val ) {
          var next = key + ": ";
          next += $.isPlainObject( val ) ? printObj( val ) : val;
          arr.push( next );
        });
        return "{ " +  arr.join( ", " ) + " }";
      };
    
    // Change Supplier Company Code ----------------------------------------------------------------
    boxSupplierCode.unbind('change');
    boxSupplierCode.change(function(){
        var errorTotal = '';
        var supplierCode = boxSupplierCode.val();
        if("" != supplierCode){
            var params = {
                "method"     : "doSelectSCD"
                ,"vendorCd"  : boxSupplierCode.val() };
	        disableBeforeSubmit();
	        $.ajax({
	            url : "CnDnUploadingAction.do",
	            type : 'GET',
	            async : false,
	            data: params,
	            dataType : 'json',
	            success : function(data) {
	                var plantList = data.jsonList1;
	                var boxSupplierPlantValue = boxSupplierPlantCode.val();
	                boxSupplierPlantCode.empty();
	                if(plantList && plantList.length > 0){
	                    $.each(plantList,function(index,val){
	                        if(boxSupplierPlantValue == val.SPcd){
	                            boxSupplierPlantCode.append( $("<option selected></option>").val(val.SPcd).text(val.SPcd) );
	                        }else{
	                            boxSupplierPlantCode.append( $("<option></option>").val(val.SPcd).text(val.SPcd) );
	                        }
	                    });
	                    
	                    errorTotal = $('#errorTotal');
	                    errorTotal.empty();
	                    $("#divError").hide();
	                    
	                }else{
	                    errorTotal = $('#errorTotal');
	                    errorTotal.empty();
	                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
	                    $("#divError").show();
	                }
	                boxSupplierPlantValue = "";
	                enableAfterSubmit();
	            },
	            error: function() {
	                enableAfterSubmit();
	            }
	        });
	    }
	});

    // Change Supplier Plant Code ----------------------------------------------------------------
    boxSupplierPlantCode.unbind('change');
    boxSupplierPlantCode.change(function(){
        var errorTotal = '';
        var sPcd = boxSupplierPlantCode.val();
        if("" != sPcd){
            var params = {
                "method" : "doSelectSPCD"
                ,"SPcd"  : boxSupplierPlantCode.val() };
	        disableBeforeSubmit();
	        $.ajax({
	            url : "CnDnUploadingAction.do",
	            type : 'GET',
	            async : false,
	            data: params,
	            dataType : 'json',
	            success : function(data) {
	                var companyList = data.jsonList1;
	                var boxSupplierCodeValue = boxSupplierCode.val();
	                boxSupplierCode.empty();
	                if(companyList && companyList.length > 0){
	                    $.each(companyList,function(index,val){
	                        if(boxSupplierCodeValue == val.vendorCd){
	                        	boxSupplierCode.append( $("<option selected></option>").val(val.vendorCd).text(val.vendorCd) );
	                        }else{
	                        	boxSupplierCode.append( $("<option></option>").val(val.vendorCd).text(val.vendorCd) );
	                        }
	                    });
	                    
	                    errorTotal = $('#errorTotal');
	                    errorTotal.empty();
	                    $("#divError").hide();
	                    
	                }else{
	                    errorTotal = $('#errorTotal');
	                    errorTotal.empty();
	                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
	                    $("#divError").show();
	                }
	                enableAfterSubmit();
	            },
	            error: function() {
	                enableAfterSubmit();
	            }
	        });
	    }
	});
    
    // Change DENSO Company Code -------------------------------------------------------------------
    boxDensoCode.unbind('change');
    boxDensoCode.change(function(){
        var errorTotal = '';
        var densoCode = boxDensoCode.val();
        if("" != densoCode){
            var params = {
                "method"   : "doSelectDCD",
                "DCd"      : densoCode
            };
            disableBeforeSubmit();
            $.ajax({
                url : "CnDnUploadingAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var plantList = data.jsonList1;
                    var boxDensoPlantValue = boxDensoPlantCode.val();
                    boxDensoPlantCode.empty();
                    if(plantList && plantList.length > 0){
                        $.each(plantList,function(index,val){
                            if(boxDensoPlantValue == val.DPcd){
                                boxDensoPlantCode.append($("<option selected></option>").val(val.DPcd).text(val.DPcd));
                            }else{
                                boxDensoPlantCode.append($("<option></option>").val(val.DPcd).text(val.DPcd));
                            }
                        });
                        
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    }else{
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>"
                            + data.jsonResult + "</span>");
                        $("#divError").show();
                    }
                    enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        }
    });

    // Change DENSO Plant Code -------------------------------------------------------------------
    boxDensoPlantCode.unbind('change');
    boxDensoPlantCode.change(function(){
        var errorTotal = '';
        var dPcd = boxDensoPlantCode.val();
        if("" != dPcd){
            var params = {
                "method"    : "doSelectDPCD",
                "DPcd"      : dPcd
            };
            disableBeforeSubmit();
            $.ajax({
                url : "CnDnUploadingAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var plantList = data.jsonList1;
                    var boxDensoCodeValue = boxDensoCode.val();
                    boxDensoCode.empty();
                    if(plantList && plantList.length > 0){
                        $.each(plantList,function(index,val){
                            if(boxDensoCodeValue == val.DCd){
                            	boxDensoCode.append($("<option selected></option>").val(val.DCd).text(val.DCd));
                            }else{
                            	boxDensoCode.append($("<option></option>").val(val.DCd).text(val.DCd));
                            }
                        });
                        
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    }else{
                        errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>"
                            + data.jsonResult + "</span>");
                        $("#divError").show();
                    }
                    enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        }
    });

});
function Winv006SetPathFile(){
    $('#txtUpload').val($('#fileData').val().split('\\').pop());
}