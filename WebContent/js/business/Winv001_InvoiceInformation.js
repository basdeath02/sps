$(document).ready(function() {
    validateMenuCode();
    
    var boxDensoCode = $("select#DCd");
    var boxDensoPlantCode = $("select#DPcd");
    var boxDensoPlantValue = boxDensoPlantCode.val();
    
    $("#cnDateFromCriteria").addClass('vAlignDate', true);
    $("#cnDateToCriteria").addClass('vAlignDate', true);
    $('#invoiceDateFromCriteria').addClass('mandatory');
    $('#invoiceDateFromCriteria').val($('#invoiceDateFrom').val());
    $('#invoiceDateToCriteria').addClass("mandatory");
    $('#invoiceDateToCriteria').val($('#invoiceDateTo').val());
    $('#cnDateFromCriteria').val($('#cnDateFrom').val());
    $('#cnDateToCriteria').val($('#cnDateTo').val());
    
    var countRecord = parseInt($('#countCancelInvoice').val());
    winv001CheckEnableCancelBtn(countRecord);
    
    $('#btnSearch').click(function(){
        $('#method').val('Search');
        $('#methodResult').val('Search');
        $('#mode').val('search');
        disableBeforeSubmit();
        $('#Winv001FormCriteria').submit();
    });
    
    $('#btnReset').click(function(){
        if (!checkNullOrEmpty($('#cannotResetMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
        } else {
            $('#method').val('Reset');
            $('#mode').val('');
            disableBeforeSubmit();
            $('#Winv001FormCriteria').submit();
        }
    });
    
    $('#btnDownload').click(function(){
        if (!checkNullOrEmpty($('#cannotDownloadMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotDownloadMessage').val() + "</span>");
        } else {
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
            
            $('#method').val('Download');
            $('#Winv001FormCriteria').submit();
        }
    });
    
    $('#btnCancelInvoice').click(function(){
        if(confirm($('#strConfirm').val())){
            disableBeforeSubmit();
            $('#methodResult').val('Cancel');
            $('#Winv001FormResult').submit();
        }
    });
    
    $("#moreLegend").click(function(){
        $('#methodDownload').val('More');
        $('#Winv001FormDownload').submit();
    });
    
    if(0 < $('a:contains("Next")').length){
        $('a:contains("Next")').mouseover(function(){
            winv001SetMethodOpenSpecifiedPage();
        });
    }
    if(0 < $('a:contains("Last")').length){
        $('a:contains("Last")').mouseover(function(){
            winv001SetMethodOpenSpecifiedPage();
        });
    }
    if(0 < $('a:contains("First")').length){
        $('a:contains("First")').mouseover(function(){
            winv001SetMethodOpenSpecifiedPage();
        });
    }
    if(0 < $('a:contains("Previous")').length){
        $('a:contains("Previous")').mouseover(function(){
            winv001SetMethodOpenSpecifiedPage();
        });
    }
    $('[name=pageNum]').keypress(function(){
        winv001SetMethodOpenSpecifiedPage();
    });
    
    // Change Company DENSO -----------------------------------------------------------------------
    boxDensoCode.unbind('change');
    boxDensoCode.change(function(){
        var densoCode = boxDensoCode.val();
        if("" != densoCode){
            var params = {
                "method"             : "doSelectDCD"
                ,"DCd"                : boxDensoCode.val()
                };
            
            var errorTotalSpan = '';
            disableBeforeSubmit();
            $.ajax({
                url : "InvoiceInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var plantList = data.jsonList1;
                    boxDensoPlantValue = boxDensoPlantCode.val();
                    boxDensoPlantCode.empty();
                    if(plantList && plantList.length > 0){
                        boxDensoPlantCode.append($("<option></option>").val("ALL").html(labelAll) );
                        $.each(plantList,function(index,val){
                            if(boxDensoPlantValue == val.DPcd){
                                boxDensoPlantCode.append( $("<option selected></option>").val(val.DPcd).text(val.DPcd) );
                            }else{
                                boxDensoPlantCode.append( $("<option></option>").val(val.DPcd).text(val.DPcd) );
                            }
                        });

                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    } else {
                        var divError = $('#divError').show();
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>"
                            + data.jsonResult + "</span>");
                        
                        boxDensoPlantCode.append($("<option></option>").val("ALL").html(labelAll) );
                    }
                    boxDensoPlantValue = "";
                    enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        }else{
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
            
            boxDensoPlantValue = "";
        }
    });

    // Change Plant DENSO -------------------------------------------------------------------------
    boxDensoPlantCode.unbind('change');
    boxDensoPlantCode.change(function(){
        var DPcd = boxDensoPlantCode.val();
        if("" != DPcd){
            var params = {
                "method"             : "doSelectDPCD"
                ,"DPcd"              : boxDensoPlantCode.val()
                };
            
            var errorTotalSpan = '';
            disableBeforeSubmit();
            $.ajax({
                url : "InvoiceInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var plantList = data.jsonList1;
                    var boxDensoCompanyValue = boxDensoCode.val();
                    boxDensoCode.empty();
                    if(plantList && plantList.length > 0){
                    	boxDensoCode.append($("<option></option>").val("ALL").html(labelAll) );
                        $.each(plantList,function(index,val){
                            if(boxDensoCompanyValue == val.DCd){
                            	boxDensoCode.append( $("<option selected></option>").val(val.DCd).text(val.DCd) );
                            }else{
                            	boxDensoCode.append( $("<option></option>").val(val.DCd).text(val.DCd) );
                            }
                        });

                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        $("#divError").hide();
                    } else {
                        var divError = $('#divError').show();
                        var errorTotal = $('#errorTotal');
                        errorTotal.empty();
                        errorTotal.html("<span class='ui-messageError'>"
                            + data.jsonResult + "</span>");
                        
                        boxDensoCode.append($("<option></option>").val("ALL").html(labelAll) );
                    }
                    boxDensoPlantValue = "";
                    enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        }else{
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
            
            boxDensoPlantValue = "";
        }
    });
    
});
function winv001SetMethodOpenSpecifiedPage(){
    $('#method').val('Search');
    $('#methodResult').val('Search');
}
function confirmCancel(statusIndex){
    if(confirm($('#strConfirm').val())){
        disableBeforeSubmit();
        $('#statusIndex').val(statusIndex);
        changeInvoiceStatus();
    }
}
function changeInvoiceStatus(){
    $('#methodResult').val('Cancel');
    document.getElementById('Winv001FormResult').pages.value = 'click';
    $('#Winv001FormResult').submit();
}
function forwardPage(statusIndex){
    $('#methodResult').val('ForwardPage');
    $('#statusIndex').val(statusIndex);
    disableBeforeSubmit();
    $('#Winv001FormResult').submit();
}
function downloadFileManagement(coverPageFileId){
    var errorTotal = $('#errorTotal');
    errorTotal.empty();
    $("#divError").hide();
    
    $('#methodResult').val('Download PDF');
    $('#coverPageFileId').val(coverPageFileId);
    $('#Winv001FormResult').submit();
}
function winv002SetCancelInvoiceList(rowNo){
    var countRecord =  parseInt($('#countCancelInvoice').val());
    if ($('#chkInvoice' + rowNo).attr('checked')) {
        $('#invoiceIdSelected'+ rowNo).val($('#invoiceId' + rowNo).val());
        countRecord = countRecord + parseInt(1);
    } else {
        $('#invoiceIdSelected'+ rowNo).val('');
        countRecord = countRecord - parseInt(1);
    }
    winv001CheckEnableCancelBtn(countRecord);
}
function winv001CheckAll(source){
    var rowChk = $("input[name='chkSelectedInv']").val();
    var countRecord = parseInt($('#countCancelInvoice').val());
    var selectedRec = 0;
    var row = 0;
    if(undefined != rowChk){
        checkboxes = document.getElementsByName('chkSelectedInv');
        for(var i = 0, n = checkboxes.length; i < n; i++){
            if(checkboxes[i].checked){
                selectedRec = selectedRec + 1;
            }
            checkboxes[i].checked = source.checked;
            if(source.checked){
                $('#invoiceIdSelected' + i).val(i);
                row = row + 1;
            }else{
                $('#invoiceIdSelected' + i).val('');
            }
        }
        countRecord = countRecord - selectedRec + row;
        winv001CheckEnableCancelBtn(countRecord);
    }
}
function winv001CheckEnableCancelBtn(countRecord) {
    if(0 < countRecord){
        $("#btnCancelInvoice").prop('disabled',false);
    }else{
        var rowChk = $("input[name='chkSelectedInv']").val();
        if(undefined != rowChk){
            checkboxes = document.getElementsByName('chkSelectedInv');
            for(var i = 0, n = checkboxes.length; i < n; i++){
                checkboxes[i].checked = false;
                $('#invoiceIdSelected' + i).val('');
            }
        }
        $("#btnCancelInvoice").prop('disabled',true);
    }
    $('#countCancelInvoice').val(countRecord);
    
    if ($("[name='chkSelectedInv']").length == $("[id^='invoiceIdSelected'][value!='']").length){
        $('#checkAll').prop('checked' ,true);
    }else{
        $('#checkAll').prop('checked' ,false);
    }
    return false;
}