$(document).ready(function() {

    $('#effectStart').addClass("mandatory");
    $('#effectEnd').addClass("mandatory");
    $('#btnOK').click(function() {
        if (!checkNullOrEmpty($('#cannotOkMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotOkMessage').val() + "</span>");
        } else {
            var jsonStr = '';
            var mode          = $('#mode').val();
            var dscId        = $('#dscId').val();
            var companySupplierCode  = $('#SCd').val();
            var plantSupplierCode = $('#SPcd').val();
            var roleCode      = $('#roleCode').val();
            var effectEnd     = $('#effectEnd').val();
            var effectStart   = $('#effectStart').val();

            jsonStr += '{"mode" : "' + mode + '",';
            jsonStr += '"roleCode" : "' + roleCode + '",';
            jsonStr += '"plantSupplierCode" : "' + plantSupplierCode + '",';
            jsonStr += '"companySupplierCode" : "' + companySupplierCode + '",';
            jsonStr += '"effectEnd" : "' + effectEnd + '",';
            jsonStr += '"effectStart" : "' + effectStart + '",';
            jsonStr += '"dscId" : "' + dscId + '"}';
                
            var data = jQuery.parseJSON('[' + jsonStr + ']');
            window.returnValue = data;
            window.close();
            self.close();   
        }
    });
    
    $('#btnCancel').click(function() {
        if (!checkNullOrEmpty($('#cannotCancelMessage').val())) {
            var divError = $('#divError').show();
            var errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotCancelMessage').val() + "</span>");
        } else {
            window.close();
            self.close();
        }
    });
});

