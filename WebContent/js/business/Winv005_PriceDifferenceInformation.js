$(document).ready(function() {
    
    errorMessage = $("td[id='errorTotal']").text();
    validateMenuCode();
    
    $('#invoiceDateFrom').addClass("mandatory");
    $('#invoiceDateTo').addClass("mandatory");
    $("#cnDateFrom").addClass('vAlignDate', true);
    $("#cnDateTo").addClass('vAlignDate', true);
    
    $('#btnSearch').click(function(){
        $('#method').val('Search');
        $('#methodResult').val('Search');
        disableBeforeSubmit();
        $('#Winv005FormCriteria').submit();
    });
    
    $('#btnReset').click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotResetMessage').val())){
            $('#method').val('Reset');
            disableBeforeSubmit();
            $('#Winv005FormCriteria').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    $('#btnDownload').click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotDownloadMessage').val())){
            $('#method').val('Download');
            //disableBeforeSubmit();
            $('#Winv005FormCriteria').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotDownloadMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    $("#moreLegend").click(function(){
        $('#methodDownload').val('More');
        $('#Winv005FormDownload').submit();
    });
    
    var boxDensoCode = $("select#densoCode");
    var boxDensoPlantCode = $("select#densoPlantCode");
    var boxSupplierCode = $("select#supplierCode");
    var boxSupplierPlantCode = $("select#supplierPlantCode");
    
    // Select Company DENSO -----------------------------------------------------------------------
    boxDensoCode.unbind('change');
    boxDensoCode.change(function(){
        var densoCode = boxDensoCode.val();
        if("" != densoCode){
            if("ALL" == boxDensoCode.val()){
                var params = {
                    "method" : "doSelectDCD"
                    ,"DCd"    : ""
                };
            }else{
                var params = {
                    "method" : "doSelectDCD"
                    ,"DCd"    : boxDensoCode.val()
                };
            }
            var errorTotalSpan = '';
            disableBeforeSubmit();
            $.ajax({
                url : "PriceDifferenceInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var plantList = data.jsonList1;
                    var boxDensoPlantValue = boxDensoPlantCode.val();
                    boxDensoPlantCode.empty();
                    if(plantList && plantList.length > 0){
                        boxDensoPlantCode.append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(plantList,function(index,val){
                            if(boxDensoPlantValue == val.miscCd){
                                boxDensoPlantCode.append( $("<option selected></option>").val(val.miscCd).text(val.miscValue) );
                            }else{
                                boxDensoPlantCode.append( $("<option></option>").val(val.miscCd).text(val.miscValue) );
                            }
                        });
                        errorTotalSpan = $("td[id='errorTotal']");
                        if(errorTotalSpan.text() != '') {
                            errorTotalSpan.empty();
                            $("#divErrorTotal").hide();
                        }
                        $("#divError").hide();
                    }else{
                    	errorTotalSpan = $("td[id='errorTotal']");
                        if(errorTotalSpan.text() != '') {
                            errorTotalSpan.empty();
                            errorTotalSpan.html(errorMessage + "<br/><span class='ui-messageError'>" + data.jsonResult + "</span>");
                        } else {
                            $("#divError").show();
                            $("#spanError").html(data.jsonResult);
                        }
                        
                        boxDensoPlantCode.append( $("<option></option>").val("ALL").html(labelAll) );
                    }
                    boxDensoPlantValue = "";
                    enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        }
    });
    
    // Select Company DENSO -----------------------------------------------------------------------
    boxDensoPlantCode.unbind('change');
    boxDensoPlantCode.change(function(){
        var dPcd = boxDensoPlantCode.val();
        if("" != dPcd){
            if("ALL" == boxDensoPlantCode.val()){
                var params = {
                    "method" : "doSelectDPCD"
                    ,"DPcd"  : ""
                };
            }else{
                var params = {
                    "method" : "doSelectDPCD"
                    ,"DPcd"  : boxDensoPlantCode.val()
                };
            }
            var errorTotalSpan = '';
            disableBeforeSubmit();
            $.ajax({
                url : "PriceDifferenceInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var companyList = data.jsonList1;
                    var boxDensoCompanyValue = boxDensoCode.val();
                    boxDensoCode.empty();
                    if(companyList && companyList.length > 0){
                    	boxDensoCode.append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(companyList,function(index,val){
                            if(boxDensoCompanyValue == val.DCd){
                            	boxDensoCode.append( $("<option selected></option>").val(val.DCd).text(val.DCd) );
                            }else{
                            	boxDensoCode.append( $("<option></option>").val(val.DCd).text(val.DCd) );
                            }
                        });
                        errorTotalSpan = $("td[id='errorTotal']");
                        if(errorTotalSpan.text() != '') {
                            errorTotalSpan.empty();
                            $("#divErrorTotal").hide();
                        }
                        $("#divError").hide();
                    }else{
                    	errorTotalSpan = $("td[id='errorTotal']");
                        if(errorTotalSpan.text() != '') {
                            errorTotalSpan.empty();
                            errorTotalSpan.html(errorMessage + "<br/><span class='ui-messageError'>" + data.jsonResult + "</span>");
                        } else {
                            $("#divError").show();
                            $("#spanError").html(data.jsonResult);
                        }
                        
                        boxDensoCode.append( $("<option></option>").val("ALL").html(labelAll) );
                    }
                    boxDensoCompanyValue = "";
                    enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        }
    });
    
    // Select Company Supplier ---------------------------------------------------------------------
    boxSupplierCode.unbind('change');
    boxSupplierCode.change(function(){
        var supplierCode = boxSupplierCode.val();
        if("" != supplierCode){
            if("ALL" == boxSupplierCode.val()){
                var params = {
                    "method"    : "doSelectSCD"
                    ,"vendorCd" : ""
                };
            }else{
                var params = {
                    "method"    : "doSelectSCD"
                    ,"vendorCd" : boxSupplierCode.val()
                };
            }
            var errorTotalSpan = '';
            disableBeforeSubmit();
            $.ajax({
                url : "PriceDifferenceInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var plantList = data.jsonList1;
                    var boxSupplierPlantValue = boxSupplierPlantCode.val();
                    boxSupplierPlantCode.empty();
                    if(plantList && plantList.length > 0){
                        boxSupplierPlantCode.append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(plantList,function(index,val){
                            if(boxSupplierPlantValue == val.miscCd){
                                boxSupplierPlantCode.append( $("<option selected></option>").val(val.miscCd).text(val.miscValue) );
                            }else{
                                boxSupplierPlantCode.append( $("<option></option>").val(val.miscCd).text(val.miscValue) );
                            }
                        });
                        errorTotalSpan = $("td[id='errorTotal']");
                        if(errorTotalSpan.text() != '') {
                            errorTotalSpan.empty();
                            $("#divErrorTotal").hide();
                        }
                        $("#divError").hide();
                    }else{
                    	errorTotalSpan = $("td[id='errorTotal']");
                        if(errorTotalSpan.text() != '') {
                            errorTotalSpan.empty();
                            errorTotalSpan.html(errorMessage + "<br/><span class='ui-messageError'>" + data.jsonResult + "</span>");
                        } else {
                            $("#divError").show();
                            $("#spanError").html(data.jsonResult);
                        }
                        boxSupplierPlantCode.append( $("<option></option>").val("ALL").html(labelAll) );
                    }
                    boxSupplierPlantValue = "";
                    enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        }
    });

    // Select Plant Supplier ---------------------------------------------------------------------
    boxSupplierPlantCode.unbind('change');
    boxSupplierPlantCode.change(function(){
        var sPcd = boxSupplierPlantCode.val();
        if("" != sPcd){
            if("ALL" == boxSupplierPlantCode.val()){
                var params = {
                    "method"    : "doSelectSPCD"
                    ,"SPcd" : ""
                };
            }else{
                var params = {
                    "method"    : "doSelectSPCD"
                    ,"SPcd" : boxSupplierPlantCode.val()
                };
            }
            var errorTotalSpan = '';
            disableBeforeSubmit();
            $.ajax({
                url : "PriceDifferenceInformationAction.do",
                type : 'GET',
                async : false,
                data: params,
                dataType : 'json',
                success : function(data) {
                    var plantList = data.jsonList1;
                    var boxSupplierCompanyValue = boxSupplierCode.val();
                    boxSupplierCode.empty();
                    if(plantList && plantList.length > 0){
                    	boxSupplierCode.append( $("<option></option>").val("ALL").html(labelAll) );
                        $.each(plantList,function(index,val){
                            if(boxSupplierCompanyValue == val.vendorCd){
                            	boxSupplierCode.append( $("<option selected></option>").val(val.vendorCd).text(val.vendorCd) );
                            }else{
                            	boxSupplierCode.append( $("<option></option>").val(val.vendorCd).text(val.vendorCd) );
                            }
                        });
                        errorTotalSpan = $("td[id='errorTotal']");
                        if(errorTotalSpan.text() != '') {
                            errorTotalSpan.empty();
                            $("#divErrorTotal").hide();
                        }
                        $("#divError").hide();
                    }else{
                    	errorTotalSpan = $("td[id='errorTotal']");
                        if(errorTotalSpan.text() != '') {
                            errorTotalSpan.empty();
                            errorTotalSpan.html(errorMessage + "<br/><span class='ui-messageError'>" + data.jsonResult + "</span>");
                        } else {
                            $("#divError").show();
                            $("#spanError").html(data.jsonResult);
                        }
                        boxSupplierCode.append( $("<option></option>").val("ALL").html(labelAll) );
                    }
                    boxSupplierCompanyValue = "";
                    enableAfterSubmit();
                },
                error: function() {
                    enableAfterSubmit();
                }
            });
        }
    });
    
});


function Winv005ForwardPage(statusIndex){
    $('#methodResult').val('ForwardPage');
    $('#statusIndex').val(statusIndex);
    disableBeforeSubmit();
    $('#Winv005FormResult').submit();
}
