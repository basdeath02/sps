$(document).ready(function() {
    validateMenuCode();
    $("#deliveryDateFrom").addClass('mandatory', true);
    $("#deliveryDateTo").addClass('mandatory', true);
    $("#shipDateFrom").addClass('vAlignDate', true);
    $("#shipDateTo").addClass('vAlignDate', true);
    $("#issueDateFrom").addClass('vAlignDate', true);
    $("#issueDateTo").addClass('vAlignDate', true);
    
    var boxDensoCode = $("select#DCd");
    var boxDensoPlantCode = $("select#DPcd");
    var boxSupplierCode = $("select#SCd");
    var boxSupplierPlantCode = $("select#SPcd");
    
    var countRecord = parseInt($('#countGroupAsn').val());
    wshp001ActivateGroupAsnBtn(countRecord);
    
    $('#btnSearch').click(function(){
        $('#method').val('Search');
        $('#methodResult').val('Search');
        $('#mode').val('search');
        disableBeforeSubmit();
        $('#Wshp001FormCriteria').submit();
    });
    
    $('#btnReset').click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotResetMessage').val())){
            $('#method').val('Reset');
            $('#mode').val('');
            disableBeforeSubmit();
            $('#Wshp001FormCriteria').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotResetMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    $('#btnGroupAsn').click(function(){
    	//alert($('totalCount').val());
        $('#methodResult').val('GroupAsn');
        disableBeforeSubmit();
        $('#Wshp001FormResult').submit();
    });
    
    // DO Select DENSO Company Code ----------------------------------------------------------------
    boxDensoCode.unbind('change');
    boxDensoCode.change(function(){
        var errorTotal = '';
        var dCd = boxDensoCode.val();
        var boxDensoPlantValue = boxDensoPlantCode.val();
        
        var params = {
            "method"   : "doSelectDCD",
            "DCd"      : dCd
        };
        disableBeforeSubmit();
        
        $.ajax({
            url : "AcknowledgedDoInformationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                var plantList = data.jsonList1;
                boxDensoPlantCode.empty();
                if(plantList && plantList.length > 0){
                    boxDensoPlantCode.append($("<option></option>").val("ALL").html(labelAll));
                    $.each(plantList,function(index,val){
                        if(boxDensoPlantValue == val.DPcd){
                            boxDensoPlantCode.append($("<option selected></option>").val(val.DPcd).text(val.DPcd));
                        }else{
                            boxDensoPlantCode.append($("<option></option>").val(val.DPcd).text(val.DPcd));
                        }
                    });
                    
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }else{
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>"
                        + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    boxDensoPlantCode.append( $("<option></option>").val("ALL").html(labelAll));
                }
                boxDensoPlantValue = "";
                enableAfterSubmit();
                wshp001CheckEnableGroupAsnBtn();
            },
            error: function() {
                enableAfterSubmit();
                wshp001CheckEnableGroupAsnBtn();
            }
        });
    });
    // ---------------------------------------------------------------------------------------------
    
    // DO Select Supplier Plant Code ---------------------------------------------------------------
    boxDensoPlantCode.unbind('change');
    boxDensoPlantCode.change(function(){
        var errorTotal = '';
        var boxDensoCdValue = boxDensoCode.val();
        var dPcd = boxDensoPlantCode.val();
        var params = {
            "method" : "doSelectDPCD",
            "DPcd"   : dPcd
        };
         
         disableBeforeSubmit();
         $.ajax({
             url : "AcknowledgedDoInformationAction.do",
             type : 'GET',
             async : false,
             data: params,
             dataType : 'json',
             success : function(data){
                 var companyList = data.jsonList1;
                 boxDensoCode.empty();
                 if(companyList && companyList.length > 0){
                     boxDensoCode.append($("<option></option>").val("ALL").html(labelAll));
                     $.each(companyList,function(index,val){
                         if(boxDensoCdValue == val.DCd){
                             boxDensoCode.append($("<option selected></option>").val(val.DCd).text(val.DCd));
                         }else{
                             boxDensoCode.append($("<option></option>").val(val.DCd).text(val.DCd));
                         }
                     });
                     errorTotal = $('#errorTotal');
                     errorTotal.empty();
                     $("#divError").hide();
                 }else{
                    errorTotal = $('#errorTotal');
                     errorTotal.empty();
                     errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                     $("#divError").show();
                     
                    boxDensoCode.append($("<option></option>").val("ALL").html(labelAll));
                 }
                 boxDensoCdValue = "";
                 enableAfterSubmit();
                 wshp001CheckEnableGroupAsnBtn();
             },
             error: function(){
                 enableAfterSubmit();
                 wshp001CheckEnableGroupAsnBtn();
             }
         });
    });
    // ---------------------------------------------------------------------------------------------
    
    // DO Select Vendor Code -----------------------------------------------------------------------
    boxSupplierCode.unbind('change');
    boxSupplierCode.change(function(){
        var errorTotal = '';
        var supplierCode = boxSupplierCode.val();
        var boxSupplierPlantValue = boxSupplierPlantCode.val();
        var params = {
            "method"    : "doSelectSCD",
            "vendorCd"  : supplierCode
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "AcknowledgedDoInformationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                var plantList = data.jsonList1;
                boxSupplierPlantCode.empty();
                if(plantList && plantList.length > 0){
                    boxSupplierPlantCode.append($("<option></option>").val("ALL").html(labelAll));
                    $.each(plantList,function(index,val){
                        if(boxSupplierPlantValue == val.SPcd){
                            boxSupplierPlantCode.append($("<option selected></option>").val(val.SPcd).text(val.SPcd));
                        }else{
                            boxSupplierPlantCode.append($("<option></option>").val(val.SPcd).text(val.SPcd));
                        }
                    });
                    
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                }else{
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    boxSupplierPlantCode.append($("<option></option>").val("ALL").html(labelAll));
                }
                boxSupplierPlantValue = "";
                enableAfterSubmit();
                wshp001CheckEnableGroupAsnBtn();
            },
            error: function() {
                enableAfterSubmit();
                wshp001CheckEnableGroupAsnBtn();
            }
        });
    });
    // ---------------------------------------------------------------------------------------------
    
    // DO Select Supplier Plant Code ---------------------------------------------------------------
    boxSupplierPlantCode.unbind('change');
    boxSupplierPlantCode.change(function(){
        var errorTotal = '';
        var boxVendorCdValue = boxSupplierCode.val();
        var sPcd = boxSupplierPlantCode.val();
        var params = {
            "method" : "doSelectSPCD",
            "SPcd"   : sPcd
        };
        
        disableBeforeSubmit();
        $.ajax({
            url : "AcknowledgedDoInformationAction.do",
            type : 'GET',
            async : false,
            data: params,
            dataType : 'json',
            success : function(data) {
                var companyList = data.jsonList1;
                boxSupplierCode.empty();
                if(companyList && companyList.length > 0){
                    boxSupplierCode.append($("<option></option>").val("ALL").html(labelAll));
                    $.each(companyList,function(index,val){
                        if(boxVendorCdValue == val.vendorCd){
                            boxSupplierCode.append($("<option selected></option>").val(val.vendorCd).text(val.vendorCd));
                        }else{
                            boxSupplierCode.append($("<option></option>").val(val.vendorCd).text(val.vendorCd));
                        }
                    });
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    $("#divError").hide();
                } else {
                    errorTotal = $('#errorTotal');
                    errorTotal.empty();
                    errorTotal.html("<span class='ui-messageError'>" + data.jsonResult + "</span>");
                    $("#divError").show();
                    
                    boxSupplierCode.append($("<option></option>").val("ALL").html(labelAll));
                }
                boxVendorCdValue = "";
                enableAfterSubmit();
                wshp001CheckEnableGroupAsnBtn();
            },
            error: function() {
                enableAfterSubmit();
                wshp001CheckEnableGroupAsnBtn();
            }
        });
    });
    // ---------------------------------------------------------------------------------------------
    
    
    $("#moreLegend").click(function(){
        $('#method').val('More');
        $('#Wshp001FormCriteria').submit();
    });
    
    if(0 < $('a:contains("Next")').length){
        $('a:contains("Next")').mouseover(function(){
            wshp001SetMethodDoInitial();
        });
    }
    
    if(0 < $('a:contains("Last")').length){
        $('a:contains("Last")').mouseover(function(){
            wshp001SetMethodDoInitial();
        });
    }
    
    if(0 < $('a:contains("First")').length){
        $('a:contains("First")').mouseover(function(){
            wshp001SetMethodDoInitial();
        });
    }
    
    if(0 < $('a:contains("Previous")').length){
         $('a:contains("Previous")').mouseover(function(){
             wshp001SetMethodDoInitial();
         });
    }
    
    $('[name=pageNum]').keypress(function(){
        wshp001SetMethodDoInitial();
    });
});

function wshp001SetMethodDoInitial(){
    $('#method').val('Search');
    $('#methodResult').val('Search');
}
function wshp001PageClick(formId, num){
    document.getElementById(formId).pages.value = 'click';
    document.getElementById(formId).pageNo.value = num;
    document.getElementById(formId).method.value = 'Search';
    document.getElementById(formId).action = "/SPS/AcknowledgedDoInformationAction.do"
    document.getElementById(formId).submit();
}
function wshp001ViewPdf(fileId){
    $('#methodResult').val('Download PDF');
    $('#pdfFileId').val(fileId);
    $('#Wshp001FormResult').submit();
}
function wshp001ViewKanbanPdf(doId,type,pdfFileKbTagPrintDate,index){
	if(pdfFileKbTagPrintDate != '' || $('#pdfFileKbTagPrintDateFlag' + doId).val() == 'Y'){
		alert(msgPrinted);
	}
	$('#methodResult').val('Download One Way Kanban PDF');
    $('#doId').val(doId);
    $('#pdfType').val(type);
    $('#pdfFileKbTagPrintDate').val(pdfFileKbTagPrintDate == '' ? 'N' : 'Y');
    $('#pdfFileKbTagPrintDateFlag' + doId).val('Y');
    document.getElementsByName('acknowledgedDoInformationReturn[' + index + '].deliveryOrderDomain.isPrintOneWayKanbanTag')[0].value = 'Y';
    document.getElementsByName('acknowledgedDoInformationReturn[' + index + '].isPrintOneWayKanbanTag')[0].value = 'Y';
    $('#Wshp001FormResult').submit();
}
function wshp001SetDateFromTo(inputItemFromId, inputItemToId){
    var dateFrom = $('#' + inputItemFromId).val();
    $('#' + inputItemToId).val(dateFrom);
}
function wshp001ViewShipNotice(doId, spsDoNo, revision, routeNo, del, dCd){
    $('#methodResult').val('Link');
    $('#selectedDoId').val(doId);
    $('#selectedDCd').val(dCd);
    $('#selectedSpsDoNo').val(spsDoNo);
    $('#selectedRev').val(revision);
    $('#selectedRoute').val(routeNo);
    $('#selectedDel').val(del);
    disableBeforeSubmit();
    $('#Wshp001FormResult').submit();
}
function wshp001SetAcknowlegdedDo(rowNo){
    var countRecord = parseInt($('#countGroupAsn').val());
    if($('#chkAcknowledgedDo' + rowNo).attr('checked')){
        $('#selectedDoId' + rowNo).val(rowNo);
        countRecord = countRecord + parseInt(1);
    }else{
        $('#selectedDoId' + rowNo).val('');
        countRecord = countRecord - parseInt(1);
    }
    wshp001ActivateGroupAsnBtn(countRecord);
    
}
function wshp001AllChecked(source){
    var rowChk = $("input[name='doChkbox']").val();
    var countRecord = parseInt($('#countGroupAsn').val());
    var selectedRec = 0;
    var row = 0;
    if(undefined != rowChk){
        checkboxes = document.getElementsByName('doChkbox');
        for(var i = 0, n = checkboxes.length; i < n; i++){
            if(checkboxes[i].checked){
                selectedRec = selectedRec + 1;
            }
            checkboxes[i].checked = source.checked;
            if(source.checked){
                $('#selectedDoId' + i).val(i);
                row = row + 1;
            }else{
                $('#selectedDoId' + i).val('');
            }
        }
        countRecord = countRecord - selectedRec + row;
        wshp001ActivateGroupAsnBtn(countRecord);
    }
}
function wshp001ActivateGroupAsnBtn(countRecord){
    if(0 < countRecord){
        $("#btnGroupAsn").prop('disabled',false);
    }else{
        var rowChk = $("input[name='doChkbox']").val();
        if(undefined != rowChk){
            checkboxes = document.getElementsByName('doChkbox');
            for(var i = 0, n = checkboxes.length; i < n; i++){
                checkboxes[i].checked = false;
                $('#selectedDoId' + i).val('');
            }
        }
        $("#btnGroupAsn").prop('disabled',true);
    }
    $('#countGroupAsn').val(countRecord);
}
function wshp001CheckEnableGroupAsnBtn(){
    var countRecord = parseInt($('#countGroupAsn').val());
    wshp001ActivateGroupAsnBtn(countRecord);
    return true;
}