$(document).ready(function() {
    validateMenuCode();
    checkActiveDownloadPdfBtn();
    
    $('#btnPreview').click(function(){
        if(0 != $('#divError').length){
            $('#divError').html('');
        }
        
        $('#method').val('Preview');
        $('#Winv004Form').submit();
    });
    
    $('#btnDownloadPdf').click(function(){
         $('#method').val('Download PDF');
         $('#Winv004Form').submit();
    });
    
    $('#moreLegend').click(function(){
        $('#method').val('More');
        $('#Winv004Form').submit();
   });
    
    $('#btnDownload').click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotDownloadMessage').val())){
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();
            
            $('#method').val('Download');
            $('#Winv004Form').submit();
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotDownloadMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    $('#btnRegister').click(function(){
        if(confirm($('#strConfirm').val())){
            $('#method').val('Register');
            disableBeforeSubmit();
            $('#Winv004Form').submit();
        }
    });
    
    $("#btnReturn").click(function(){
        if('Register' == $('#mode').val() || 'Upload' == $('#mode').val()){
            if(confirm($('#strConfirmReturn').val())){
                $('#method').val('Return');
                disableBeforeSubmit();
                $('#Winv004Form').submit();
            }
        }else{
            $('#method').val('Return');
            disableBeforeSubmit();
            $('#Winv004Form').submit();
        }
        
    });
    
    $("#btnCalculate").click(function(){
        var errorTotal = '';
        if(checkNullOrEmpty($('#cannotCalculateCnMessage').val())){
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            $("#divError").hide();

            var errorTotal = '';
            var errorCal = '';

            if ('' == $('#supplierBaseAmount').val()) {
                errorCal += "<span class='ui-messageError'>" + $('#errorSupplierBaseNull').val()
                    + "</span><br />";
            }
            
            // [FIX] None VAT will not input all VAT amount
            //if ('' == $('#supplierVatAmount').val()) {
            if ('' == $('#supplierVatAmount').val() && !$('#nonVAT').prop('checked')) {
                errorCal += "<span class='ui-messageError'>" + $('#errorSupplierVatNull').val()
                    + "</span><br />";
            }
            
            if ('' == $('#supplierTotalAmount').val()) {
                errorCal += "<span class='ui-messageError'>" + $('#errorSupplierTotalNull').val()
                    + "</span><br />";
            }
            
            if(checkNullOrEmpty(errorCal)){
                errorTotal = $('#errorTotal');
                errorTotal.empty();
                $("#divError").hide();
            } else {
                errorTotal = $('#errorTotal');
                errorTotal.empty();
                errorTotal.html(errorCal);
                $("#divError").show();
                return false;
            }
            
            var totalBaseAmount = parseFloat(removeCommaFromString($('#totalBaseAmount').val()));
            var totalAmount = parseFloat(removeCommaFromString($('#totalAmount').val()));
            var supplierBaseAmount
                = parseFloat(removeCommaFromString($('#supplierBaseAmount').val()));
            var supplierTotalAmount
                = parseFloat(removeCommaFromString($('#supplierTotalAmount').val()));
        
            // Start : [FIX] None VAT will not input all VAT amount
            //var totalVatAmount = parseFloat(removeCommaFromString($('#totalVatAmount').val()));
            //var supplierVatAmount = parseFloat(removeCommaFromString($('#supplierVatAmount').val()));
            var totalVatAmount = parseFloat('0');
            var supplierVatAmount = parseFloat('0');
            if (!$('#nonVAT').prop('checked')) {
                totalVatAmount = parseFloat(removeCommaFromString($('#totalVatAmount').val()));
                supplierVatAmount = parseFloat(removeCommaFromString($('#supplierVatAmount').val()));
            }
            // End : [FIX] None VAT will not input all VAT amount
            
            if (supplierBaseAmount < totalBaseAmount || supplierVatAmount < totalVatAmount
                || supplierTotalAmount < totalAmount)
            {
                errorTotal = $('#errorTotal');
                errorTotal.empty();
                errorTotal.html("<span class='ui-messageError'>"
                    + $('#errorSupplierInvoiceAmount').val() + "</span>");
                $("#divError").show();
                return false;
            } else {
                errorTotal = $('#errorTotal');
                errorTotal.empty();
                $("#divError").hide();
            }
            
            if ('' != $('#cnBaseAmount').val() || '' != $('#cnVatAmount').val()
                || '' != $('#cnTotalAmount').val())
            {
                if(confirm($('#strConfirmCalculate').val())) {
                    winv004AutoCalculateCnAmount(
                        supplierBaseAmount, supplierVatAmount, supplierTotalAmount
                        , totalBaseAmount, totalVatAmount, totalAmount);
                }
            } else {
                winv004AutoCalculateCnAmount(supplierBaseAmount, supplierVatAmount, supplierTotalAmount
                    , totalBaseAmount, totalVatAmount, totalAmount);
            }
        }else{
            errorTotal = $('#errorTotal');
            errorTotal.empty();
            errorTotal.html("<span class='ui-messageError'>"
                + $('#cannotCalculateCnMessage').val() + "</span>");
            $("#divError").show();
            return false;
        }
    });
    
    if('Register' == $('#mode').val() || 'Upload' == $('#mode').val()){
        $('#invoiceDateCriteria').addClass('mandatory');
    }else{
        $('#invoiceDateCriteria').addClass('readonly', true);
        $('#invoiceDateCriteria').attr('readonly', true);
        $("#calendar_invoiceDateCriteria").attr('disabled',true);
        $("#recycle_invoiceDateCriteria").attr('disabled',true);
        $('#cnDateCriteria').addClass('readonly', true);
        $('#cnDateCriteria').attr('readonly', true);
        $("#calendar_cnDateCriteria").attr('disabled',true);
        $("#recycle_cnDateCriteria").attr('disabled',true);
    }
    
    // [IN059] If VAT type is None VAT, calculate none VAT
    if ($('#nonVAT').prop('checked')) {
        calNonVat();
    }
    
});
function checkActiveDownloadPdfBtn(){
    if(checkNullOrEmpty($('#mode').val()) || "Register" == $('#mode').val()
        || "Upload" == $('#mode').val() || "initial" == $('#mode').val()){
        $("#btnDownloadPdf").attr('disabled',true);
    }else{
        $("#btnDownloadPdf").removeAttr('disabled');
    }
}
function calNewVat(){
    
    var vat = $('#vatRate').val();
    var decimalDisp = $('#decimalDisp').val();
    var totalBaseAmount = parseFloat(removeCommaFromString($('#totalBaseAmount').val()));
    var totalVatAmount = parseFloat(Math.round(totalBaseAmount * vat) / 100);
    var totalDiffBaseAmount = parseFloat(removeCommaFromString($('#totalDiffBaseAmount').val()));
    var totalDiffVatAmount = parseFloat(Math.round(totalDiffBaseAmount * vat) / 100);
    var totalAmount = parseFloat(totalBaseAmount + totalVatAmount);
    var totalDiffAmount = parseFloat(totalDiffBaseAmount + totalDiffVatAmount);

    // [FIX] None VAT will not input all VAT amount
    //$('#totalDiffVatAmount').val(addCommaStr(totalDiffVatAmount.toFixed(decimalDisp)));
    //$('#totalVatAmount').val(addCommaStr(totalVatAmount.toFixed(decimalDisp)));
    if (!$('#nonVAT').prop('checked')) {
        $('#totalDiffVatAmount').val(addCommaStr(totalDiffVatAmount.toFixed(decimalDisp)));
        $('#totalVatAmount').val(addCommaStr(totalVatAmount.toFixed(decimalDisp)));
    }

    $('#totalAmount').val(addCommaStr(totalAmount.toFixed(decimalDisp)));
    $('#totalDiffAmount').val(addCommaStr(totalDiffAmount.toFixed(decimalDisp)));
}
function calNonVat(){
    var vat = $('#vatRate').val();
    var decimalDisp = $('#decimalDisp').val();
    var zero = parseFloat(0).toFixed(decimalDisp);

    // Start : [FIX] None VAT will not input all VAT amount
    //$('#totalDiffVatAmount').val(zero);
    //$('#totalVatAmount').val(zero);
    $('#totalDiffVatAmount').val('');
    $('#totalVatAmount').val('');
    $('#cnVatAmount').prop('readonly', true);
    $('#cnVatAmount').val('');
    $('#supplierVatAmount').addClass('readonly');
    $('#supplierVatAmount').prop('readonly', true);
    $('#supplierVatAmount').val('');
    // End : [FIX] None VAT will not input all VAT amount
    
    $('#totalAmount').val($('#totalBaseAmount').val());
    $('#totalDiffAmount').val($('#totalDiffBaseAmount').val());
    
    $('#vatRate').prop('readonly', true);
    $('#vatRate').val(' ');
}
function checkEnter(e) {
    if(PositiveIntegerFilter(e) || e.keyCode == 13){
        if (e.keyCode == 13) {
            calNewVat();
        }
    }else{
        return false;
    }
}
function addDot(obj) {
    if(!isNaN(obj) || obj.value != ""){
        var decimalDisp = $('#decimalDisp').val();
        var amount = removeCommaFromString(obj.value);
        obj.value =  addCommaStr(parseFloat(amount).toFixed(decimalDisp));
    }
    return obj;
}
function checkAmountNegative(e,obj) {
    if(DoubleFilter(e) || e.keyCode == 13){
        if (e.keyCode == 13) {
            addDot(obj);
        }
    }else{
        return false;
    }
}
function checkAmount(e,obj) {
    if(PositiveDoubleFilter(e) || e.keyCode == 13){
        if (e.keyCode == 13) {
            addDot(obj);
        }
    }else{
        return false;
    }
}

function setMandatory(){
    $("#vatRate").prop('readonly', false);

    // Start : [FIX] None VAT will not input all VAT amount
    $('#cnVatAmount').prop('readonly', false);
    $('#supplierVatAmount').removeClass('readonly');
    $('#supplierVatAmount').prop('readonly', false);
    // End : [FIX] None VAT will not input all VAT amount
    
    // Start : [IN059] When select Vat, set default Vat Rate and calculate Vat.
    $('#vatRate').val(defaultVatRate);
    calNewVat();
    // End : [IN059] When select Vat, set default Vat Rate and calculate Vat.
}

function winv004AutoCalculateCnAmount(supplierBaseAmount, supplierVatAmount, supplierTotalAmount
    , totalBaseAmount, totalVatAmount, totalAmount)
{
    var decimalDisp = $('#decimalDisp').val();
    var cnBaseAmount = totalBaseAmount - supplierBaseAmount;
    var cnVatAmount = totalVatAmount - supplierVatAmount;
    var cnTotalAmount = totalAmount - supplierTotalAmount;
    
    if (0.0 != cnBaseAmount) {
        $('#cnBaseAmount').val(addCommaStr(cnBaseAmount.toFixed(decimalDisp)));
    } else {
        $('#cnBaseAmount').val('');
    }

    // [FIX] None VAT will not input all VAT amount
    //if (0.0 != cnVatAmount) {
    if (0.0 != cnVatAmount && !$('#nonVAT').prop('checked')) {
        $('#cnVatAmount').val(addCommaStr(cnVatAmount.toFixed(decimalDisp)));
    } else {
        $('#cnVatAmount').val('');
    }
    
    if (0.0 != cnTotalAmount) {
        $('#cnTotalAmount').val(addCommaStr(cnTotalAmount.toFixed(decimalDisp)));
    } else {
        $('#cnTotalAmount').val('');
    }
}

function winv004CalculateDiffAmount(index) {
    var errorTotal = '';
    if ('' == $('#supplierUnitPriceTxt' + index)) {
        return;
    }
    
    var supplierUnitPrice = 0.0;
    if (!checkNullOrEmpty($('#supplierUnitPriceTxt' + index).val())) {
        supplierUnitPrice = parseFloat(
            removeCommaFromString($('#supplierUnitPriceTxt' + index).val()));
    }
    var densoUnitPrice = parseFloat(removeCommaFromString($('#densoUnitPrice' + index).val()));
    
    if (supplierUnitPrice < densoUnitPrice) {
        errorTotal = $('#errorTotal');
        errorTotal.empty();
        errorTotal.html("<span class='ui-messageError'>"
            + $('#errorSupplierUnitPrice').val() + "</span>");
        $("#divError").show();
        return false;
    } else {
        errorTotal = $('#errorTotal');
        errorTotal.empty();
        $("#divError").hide();
    }
    
    var vat = $('#vatRate').val();
    var decimalDisp = $('#decimalDisp').val();
    var priceDecimal = 4;

    $('#supplierUnitPrice' + index).val(supplierUnitPrice.toFixed(priceDecimal));
    $('#supplierUnitPriceTxt' + index).val(addCommaStr(supplierUnitPrice.toFixed(priceDecimal)));
    var shippingQty = parseFloat(removeCommaFromString($('#shippingQty' + index).val()));
    var diffBaseAmount = (densoUnitPrice - supplierUnitPrice) * shippingQty;
    $('#diffBaseAmount' + index).val(diffBaseAmount.toFixed(decimalDisp));
    // [IN010] : Also rounding hidden value
    //$('#bdDiffBaseAmount' + index).val(diffBaseAmount);
    $('#bdDiffBaseAmount' + index).val(diffBaseAmount.toFixed(decimalDisp));

    $('#spanDiffBaseAmount' + index).html(addCommaStr(diffBaseAmount.toFixed(decimalDisp)));
    if (0.0 == diffBaseAmount) {
        $('#spanDiffBaseAmount' + index).removeClass("textred");
    } else {
        $('#spanDiffBaseAmount' + index).addClass("textred");
    }

    var sumDiffBase = 0.0;
    var hiddenDiffBaseAmount = $("[id^='bdDiffBaseAmount']");
    for(var i = 0; i < hiddenDiffBaseAmount.length; i++) {
        sumDiffBase += parseFloat(removeCommaFromString($('#bdDiffBaseAmount' + i).val()));
    }
    
    var totalDiffBaseAmount = parseFloat(sumDiffBase);
    var totalDiffVatAmount = parseFloat(Math.round(sumDiffBase * vat) / 100);
    var totalDiffAmount = parseFloat(sumDiffBase + totalDiffVatAmount);

    $('#totalDiffBaseAmount').val(addCommaStr(totalDiffBaseAmount.toFixed(decimalDisp)));

    // [FIX] None VAT will not input all VAT amount
    //$('#totalDiffVatAmount').val(addCommaStr(totalDiffVatAmount.toFixed(decimalDisp)));
    if (!$('#nonVAT').prop('checked')) {
        $('#totalDiffVatAmount').val(addCommaStr(totalDiffVatAmount.toFixed(decimalDisp)));
    }
    
    $('#totalDiffAmount').val(addCommaStr(totalDiffAmount.toFixed(decimalDisp)));
    
    return;
}