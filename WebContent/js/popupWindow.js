// JavaScript Document
/*******************************************/
//- Description 	: Set popup window show align center.  
//- Arguments 		: URL [String], Window's name [String], Window's width [Integer], Window's height [Integer]
//- Return 			: [Integer]
//- Create by		: Nakhorn C.(20081104)
//- Modify			: 

//- Ex. Request  	: return xf_setMaxlength(this,255);
//- Ex. Response  	: Popup window show align center.

var newWin;
function xf_popupCenter(url,name,windowWidth,windowHeight){
	windowWidth = (screen.width*90)/100;
	windowHeight = (screen.height*80)/100;
	//alert(windowWidth+" x "+windowHeight);
	myleft=(screen.width)?(screen.width-windowWidth)/2:100;
	mytop=(screen.height)?(screen.height-windowHeight)/2:100;		
	properties = "width="+windowWidth+"px,height="+windowHeight+"px,scrollbars=yes, top="+mytop+",left="+myleft + ",resizable=yes";   
	newWin = window.open(url,name,properties)
	if (window.focus) {newWin.focus()}
	
}
function xf_popupDialogCenter(url,name,dialogWidth,dialogHeight){   
	windowWidth = (screen.width*90)/100;
	windowHeight = (screen.height*80)/100;
	//alert(windowWidth+" x "+windowHeight);
	myleft=(screen.width)?(screen.width-dialogWidth)/2:100;
	mytop=(screen.height)?(screen.height-dialogHeight)/2:100;	
	properties = "dialogWidth:"+dialogWidth+"px"+",dialogHeight:"+dialogHeight+"px"+",scrollbars=yes, top="+mytop+",left="+myleft + ",resizable=yes";   
	newWin = window.showModalDialog(url,name,properties);

}

function xf_popupDialogCenterReturnValue(url,name,dialogWidth,dialogHeight){   
	windowWidth = (screen.width*90)/100;
	windowHeight = (screen.height*80)/100;
	//alert(windowWidth+" x "+windowHeight);
	myleft=(screen.width)?(screen.width-dialogWidth)/2:100;
	mytop=(screen.height)?(screen.height-dialogHeight)/2:100;	
	properties = "dialogWidth:"+dialogWidth+"px"+",dialogHeight:"+dialogHeight+"px"+",scrollbars=yes, top="+mytop+",left="+myleft + ",resizable=yes";   
	return window.showModalDialog(url,name,properties);
}