
// Start History Back ##############################################################################

setTimeout('window.history.forward();', 1);

//End History Back #################################################################################

/*
 * function for support enable and disable object in jquery.
 */
(function($) {
    $.fn.disable = function() {return this.attr('disabled', true);}
    $.fn.disableTxt = function() {
        $(this).css('background-color', '#e0e0e0');
        return this.attr('disabled', true);
    }
    $.fn.enable = function() {return this.attr('disabled', false).addClass('sbttn');}
    $.fn.enableTxt = function() {
        $(this).css('background-color', '');
        return this.attr('disabled', false);
    }
    // This function is used for enable button by checking its auth before
    // enable.
    $.fn.enableButtonWithAuth = function(idAuth){ 
        // alert(idButton+'/'+auth);
        var objAuth = document.getElementById(idAuth);
        if((null != objAuth) && (undefined != objAuth)){
        var hasAuth = objAuth.value;
         if('1' == hasAuth){
             // If user has authority for update data then the button
            // can be enabled.
             return this.attr('disabled', false).addClass('sbttn');
         }
        }
    }// End of checking enable button function.
})(jQuery);

var changeFlag = false;
$(document).ready(function(){
    autoBindingFlag();
});

// Mark flag when have event of tag on screen.
function autoBindingFlag(){
    if (1 == $("#mainFrame", parent.document).length) {
        $("input[type=text],input[type=file],input[type=checkbox],select,textarea").change(function(){
            setChangeFlag();
        });
        $("input[type=radio],img[src$='icons/delete.gif'],img[src$='icons/page_find.gif']").click(function(){
            setChangeFlag();
        });
    }
}

// Set flag to frame menu to check on change menu.
function setChangeFlag() {
    changeFlag = true;
    top.frames['menuFrame'].Wcom001Form.elements[0].value = changeFlag;
}
/**
 * disableLinkAndButton: This function is used for disable all link and buttons
 * before performing executing transaction (CRUD).
 */
function disableBeforeSubmit(){
    // Get all input type = button.
    var allButtons =  $(":button");
    if(allButtons != undefined){
    $.each(allButtons,function(idx,button){
        $(button).disable();
    });
    }
   
    var allLinks = $("a");
    if(allLinks != undefined){
    $.each(allLinks, function(idx,link){
        $(link).disable();
    });
    } 
  
}
function enableAfterSubmit(){
    // Get all input type = button.
    var allButtons =  $(":button");
    if(allButtons != undefined){
        $.each(allButtons,function(idx,button){
            $(button).enable();
        });
    }
    
    var allLinks = $("a");
    if(allLinks != undefined){
        $.each(allLinks, function(idx,link){
            $(link).enable();
        });
    } 
    
}
/**
 * toUpperCase is used for reset value of input type="text" to UPPER CASE
 * (CAPITAL LETTER)
 * 
 * @param textBoxObj -
 *            an object of input type = "text".
 * @sample toUpperCase(this);
 */
function toUpperCase(textBoxObj){
    $(textBoxObj).val($(textBoxObj).val().toUpperCase());
}


/*
 * resetWithTrim is used for reset value of input object by trimming space on
 * the left and right side then re-assign result from trimming to input object.
 */
function resetWithTrim(obj){
    if(undefined != obj){
    obj.value = Trim(obj.value);
    }
}

/*
 * checkNullOrEmpty is used for checking value of input object. If input value
 * is NaN, undefined, empty or null, the result is "true" otherwise return
 * false. @param varInput @return true Or false
 */
function checkNullOrEmpty(varInput) {
    return ((varInput == NaN) || (varInput == undefined) || (varInput == null) || (varInput == ""));
}

function Trim(data){
  
    if(checkNullOrEmpty(data) || (data.length < 1)){
      return "";
    }
    data = RTrim(data);
    data = LTrim(data);
    if(data == ""){
      return "";
    }else{
      return data;
    }
}

function RTrim(VALUE){
    var v_length = VALUE.length;
    if(v_length < 0){
      return "";
    }
    var strTemp = "";
    var iTemp = v_length -1;
    var w_space = String.fromCharCode(32);
    while(iTemp > -1){
      if(!(VALUE.charAt(iTemp) == w_space)){
        strTemp = VALUE.substring(0,iTemp +1);
        break;
      }
      iTemp = iTemp-1;
    }
    return strTemp;
}

function LTrim(VALUE){
    var v_length = VALUE.length;
    if(v_length < 1){
    return "";
    }
    var strTemp = "";
    var iTemp = 0;
    var w_space = String.fromCharCode(32);
    while(iTemp < v_length){
      if(!(VALUE.charAt(iTemp) == w_space)){
        strTemp = VALUE.substring(iTemp,v_length);
        break;
      }
      iTemp = iTemp + 1;
    }
    return strTemp;
 }

function isNumbericValue(value){
    var result = true;
    for(var i = 0; i < value.length; i++){
        if(parseInt("0123456789".indexOf(value.charAt(i))) == -1){
            result = false;
            break;
        }
    }
    return result;
}

function isNumbericWithMinusValue(value){
    var result = true;
    for(var i = 0; i < value.length; i++){
        if(parseInt("-0123456789".indexOf(value.charAt(i))) == -1){
            result = false;
            break;
        }
    }
    return result;
}

function isDecimalValue(value){
    var result = true;
    for(var i = 0; i < value.length; i++){
        if(parseInt("-0123456789.".indexOf(value.charAt(i))) == -1){
            result = false;
            break;
        }
    }
    return result;
}

function IntegerFilter( e ) {
    var code = (e.which || e.keyCode);
    var char = String.fromCharCode(code);
    var elmt = $(e.target ? e.target : e.srcElement).val();

    if( code === 8 ) // 'Backspace' key press.
        return true;
    if( code === 9 ) // 'TAB' key press.
        return true;
    if( code !== 8 && ((char === '-' && (elmt.length !== 0)) || "-0123456789".indexOf(char) < 0) )
        return false;
    return true;
}

function PositiveIntegerFilter( e ) {
    var code = (e.which || e.keyCode);

    if( code === 8 ) // 'Backspace' key press.
        return true;
    if( code === 9 ) // 'TAB' key press.
        return true;
    if( code !== 8 && "0123456789".indexOf( String.fromCharCode(code) ) < 0 )
        return false;
    return true;
}

function DoubleFilter( e ) {
    var cd = (e.which || e.keyCode);
    var ch = String.fromCharCode(cd);
    var vl = $(e.target ? e.target : e.srcElement).val();

    if( cd === 8 ) // 'Backspace' key press.
        return true;
    if( cd === 9 ) // 'TAB' key press.
        return true;
    if( cd !== 8 && ((ch === '-' && vl.indexOf('-') > -1) || (ch === '.' && (vl.length === 0 || vl.indexOf('.') > -1))) || "-.0123456789".indexOf(ch) < 0 )
        return false;
    return true;
}
    
function PositiveDoubleFilter( e ){
    var cd = (e.which || e.keyCode);
    var ch = String.fromCharCode(cd);
    var vl = $(e.target ? e.target : e.srcElement).val();

    if( cd === 8 ) // 'Backspace' key press.
        return true;
    if( cd === 9 ) // 'TAB' key press.
        return true;
    if(cd !== 8 && (ch === '.' && (vl.length === 0 || vl.indexOf('.') > -1)) || ".0123456789".indexOf(ch) < 0)
        return false;
    return true;
}

function adjustMaxLength(elementId){
    
    if(($('#'+elementId).val().indexOf(".") != -1) && ($('#'+elementId).val().indexOf("-") != -1)){$('#'+elementId).attr("maxlength",parseInt($('#'+elementId+'MaxLength').attr("maxlength"),10)+2);}else if(($('#'+elementId).val().indexOf(".") != -1) || ($('#'+elementId).val().indexOf("-") != -1)){$('#'+elementId).attr("maxlength",parseInt($('#'+elementId+'MaxLength').attr("maxlength"),10)+1);}else if(($('#'+elementId).val().indexOf(".") == -1) && ($('#'+elementId).val().indexOf("-") == -1)){$('#'+elementId).attr("maxlength",parseInt($('#'+elementId+'MaxLength').attr("maxlength"),10));}
}

function isDecimal(event,elementId) {
    var key;
    if (window.event) {key = event.keyCode;} // IE
    else if (event.which) {key = event.which;}     // Netscape/Firefox/Opera
    var ctrl = event.ctrlKey;
    var parameter = document.getElementById(elementId).value;
    var isDecimal = parameter.split(".").length;
    var isMinus = parameter.split("-").length;
    
    // Increase max lenght
    if((((key==110) || (key==190)) && (isDecimal == 1))){$('#'+elementId).attr("maxlength",parseInt($('#'+elementId).attr("maxlength"),10)+1);}
    if(((key==109) || (key==189)) && (isMinus == 1)){$('#'+elementId).attr("maxlength",parseInt($('#'+elementId).attr("maxlength"),10)+1);}
    //alert($('#'+elementId).attr("maxlength"));
    //alert(key);
    if (((key >= 48) && (key <= 57)) || ((key>=96) && (key<=105)) // 0-9
        || (key==8)      // Backspace
        || (key==9)     // Tab
        || (key==13)    // Enter
        || ((key>=35) && (key<=40))    // End,Home,Left,Top,Right,Down
        || (key==45)    // Insert
        || (key==46)    // Delete
        //|| (key==22)     // Control+V
        || (((key==110) || (key==190)) && (isDecimal == 1)) // . and has one "."
        || (((key==109) || (key==189)) && (isMinus == 1))     // -
        || (ctrl && (key==65))    // Ctrl+A
        || (ctrl && (key==67))    // Ctrl+C
        //|| (ctrl && (key==86))    // Ctrl+V
        || (ctrl && (key==88))    // Ctrl+X
    ) {
        return true;
    } else {
        return false;
    }
}

function isNumbericAndEngChar(event) {
    var key = event.keyCode;
    if (((key >= 48) && (key <= 57)) || ((key>=96) && (key<=105)) // 0-9
        || (key==8)      // Backspace
        || (key==9)     // Tab
        || (key==13)    // Enter
        || ((key>=35) && (key<=40))    // End,Home,Left,Top,Right,Down
        || (key==45)    // Insert
        || (key==46)    // Delete
        || ((key >= 97) && (key <= 122)) || ((key>=65) && (key<=90))    // a-z
                                        // ,
                                        // A-Z
    ) {
        return true;
    } else {
        return false;
    }
}

function isNumberic(event) {
    var key = event.keyCode;
    if (((key >= 48) && (key <= 57)) || ((key>=96) && (key<=105)) // 0-9
        || (key == 8)     // Backspace
        || (key == 9)     // Tab
        || (key == 13)    // Enter
        || ((key >= 35) && (key <= 40)) // End,Home,Left,Top,Right,Down
        || (key == 45)    // Insert
        || (key == 46)    // Delete
    ) {
        return true;
    } else {
        return false;
    }
}

function isNumbericWithMinus(event) {
    var ctrl = event.ctrlKey;
    var key = event.keyCode;
    if (((key >= 48) && (key <= 57)) || ((key>=96) && (key<=105)) // 0-9
        || (key == 45)    // -
        || (key == 8)     // Backspace
        || (key == 9)     // Tab
        || (key == 13)    // Enter
        || ((key >= 35) && (key <= 40)) // End,Home,Left,Top,Right,Down
        || (key == 45)    // Insert
        || (key == 46)    // Delete
        || (ctrl && (key==65))    // Ctrl+A
        || (ctrl && (key==67))    // Ctrl+C
        || (ctrl && (key==88))    // Ctrl+X
    ) {
        return true;
    } else {
        return false;
    }
}

/**
 * paste comma in string value str = 1234 --> 1,234
 * 
 * @param str
 * @returns {String}
 */
function addCommas(str) {   
    if(checkNullOrEmpty(str)) {
        return "";
    }
    var output = ""; 
    var value = "";
    var integer = "";
    var decimal = "";
    var hasDot = false;
    var hasHypen = false;
    
    if(checkNullOrEmpty(str)) {
        return "";
    }
    if(!checkNullOrEmpty(str) && Trim(str) === "-") {
        return "";
    }
    
    if(!checkNullOrEmpty(str)){
        if(str.indexOf(".") != -1){
            value = str.split(".");
            if(!isNumbericWithMinusValue(value[0])) {
                return "";
            }
            if(!isNumbericWithMinusValue(value[1])) {
                return "";
            }

            if(checkNullOrEmpty(value[0])){value[0] = "0";}
            if(value[0].length === 1 && value[0] === "-") {value[0] = "-0";}
            integer = value[0];
            decimal = value[1];
            // <!- Parse value in float format -->
            decimal = parseFloat("."+decimal,10);
            // <!- After parse decimal value become to 0.12 -->
            // <!- Sub string start at index 0 for get 12 -->
            decimal = (decimal+'').substring(2);
            if(isNumbericValue(decimal)){hasDot = true;}
        }else{
            if(!isNumbericWithMinusValue(str)) {
                return "";
            }
            integer = str; 
        }
        var amount = new String(parseFloat(integer,10));
        if(integer === "-0"){amount = "-0";}
        if(amount.indexOf("-") != -1){
            amount = amount.replace(/-/g,'');
            hasHypen = true;
        }
        amount = amount.split("").reverse();      
        for ( var i = 0; i <= amount.length-1; i++ ){         
            output = amount[i] + output;         
            if (((i+1) % 3 == 0) && (amount.length-1) !== i){output = ',' + output;}     
        }   
        if(hasHypen){output = "-"+output;}
    }
    if(hasDot && !checkNullOrEmpty(decimal)){
        output += '.' + decimal;
    }
    
    return output; 
} 

function setFlagUpdate(activeEle,dbHiddenEle,updateEle){
    if(undefined != activeEle){
        // Get object by ID.
        var activeObj = document.getElementById(activeEle.id);
        if(undefined != activeObj){
            var dbObj = document.getElementById(dbHiddenEle.id);
            // //alert('DB Value: '+dbObj.value+'/Input Value:
            // '+activeObj.value);
            var updateObj = document.getElementById(updateEle.id);
            if(activeObj.value == dbObj.value){
                // Data is not update.
                updateObj.value = '0';
            }else{
                // Data is updated.
                updateObj.value = '1';
            }
        }
        // alert(updateObj.value);
    }
}

/**
 * addHyphen is used for add hyphen to a specified element according to index
 * that user input, function perform getting value from element, add hyphen
 * according to index and setting value with hyphen to the element.
 * 
 * @param indexArray
 * @param elementId
 */

function addHyphen(indexArray, elementId){
    var value = document.getElementById(elementId).value;

    if (!checkNullOrEmpty(value)){
        // the split() method is used to split a string into an array of
        // substrings,
        // and
        // returns the new array
        var valueArray = value.split("");

        if (value.length > indexArray[indexArray.length-1]){
            for (var i = indexArray.length-1; i >= 0; i--){
                // the splice() method is used to add hyphen to
                // an array at index.
                valueArray.splice(indexArray[i], 0, "-");
            }
        }

        document.getElementById(elementId).value = valueArray.join("");
    }
}

/**
 * removeHyphen is used for remove all hyphen from a specified element, function
 * perform getting value from element, remove all hyphen and setting value
 * without hyphen to the element.
 * 
 * @param elementId
 */

function removeHyphen(elementId){
    var value = document.getElementById(elementId).value;

    /*
     * if (!checkNullOrEmpty(value)){ var hyphenIndex = value.indexOf("-"); //
     * hyphenIndex return -1 if hyphen never occurs. while (hyphenIndex != -1){
     * value = value.replace("-", ""); hyphenIndex = value.indexOf("-"); }
     * 
     * document.getElementById(elementId).value = value; }
     */
    if (!checkNullOrEmpty(value)){document.getElementById(elementId).value = value.replace(/-/g,'');}
}

/**
 * removeComma is used for remove all comma from a specified element, function
 * perform getting value from element, remove all comma and setting value
 * without comma to the element.
 * 
 * @param elementId
 */

function removeComma(elementId){
    var value = document.getElementById(elementId).value;

    /*
     * if (!checkNullOrEmpty(value)){ var hyphenIndex = value.indexOf(","); //
     * hyphenIndex return -1 if hyphen never occurs. while (hyphenIndex != -1){
     * value = value.replace(",", ""); hyphenIndex = value.indexOf(","); }
     * 
     * document.getElementById(elementId).value = value; }
     */
    if (!checkNullOrEmpty(value)){document.getElementById(elementId).value = value.replace(/,/g,'');}
}

/**
 * removeCommaFromString is used for remove all comma from string value,
 * function perform remove all comma from string value that user input and
 * return string value without comma
 * 
 * @param value
 * @returns value without comma
 */

function removeCommaFromString(value){
    /*
     * if (!checkNullOrEmpty(value)){ var hyphenIndex = value.indexOf("-"); //
     * hyphenIndex return -1 if hyphen never occurs. while (hyphenIndex != -1){
     * value = value.replace("-", ""); hyphenIndex = value.indexOf("-"); } }
     */
    if (!checkNullOrEmpty(value)){value = value.replace(/,/g,'');}
    return value;
}

/**
 * removeDotFromString is used for remove all dot from string value, function
 * perform remove all dot from string value that user input and return string
 * value without dot
 * 
 * @param value
 * @returns value without dot
 */

function removeDotFromString(value){
    if (!checkNullOrEmpty(value)){value = value.replace(/\./g,'');}
    return value;
}

/**
 * removeHyphenFromString is used for remove all hyphen from string value,
 * function perform remove all hyphen from string value that user input and
 * return string value without hyphen
 * 
 * @param value
 * @returns value without hyphen
 */

function removeHyphenFromString(value){
    /*
     * if (!checkNullOrEmpty(value)){ var hyphenIndex = value.indexOf("-"); //
     * hyphenIndex return -1 if hyphen never occurs. while (hyphenIndex != -1){
     * value = value.replace("-", ""); hyphenIndex = value.indexOf("-"); } }
     */
    if (!checkNullOrEmpty(value)){value = value.replace(/-/g,'');}
    return value;
}

/**
 * addHyphenFromString is used for add hyphen, function perform add hyphen that
 * user input and return string value with hyphen
 * 
 * @param value
 * @returns value with hyphen
 */
function addHyphenFromString(indexArray, value){

    if (!checkNullOrEmpty(value)){
        // the split() method is used to split a string into an array of
        // substrings, and returns the new array
        var valueArray = value.split("");

        if (value.length > indexArray[indexArray.length-1]){
            for (var i = indexArray.length-1; i >= 0; i--){
                // the splice() method is used to add hyphen to
                // an array at index.
                valueArray.splice(indexArray[i], 0, "-");
            }
        }

        value = valueArray.join("");
    }
    return value;
}

// Error Code
var varErrorCode = new Array("'","\"","&","<",">");
// Jsp Href: used in disable link process.
var varHrefClick = new Array();
// Jsp Button: used in disable link process.
var varButtonClick = new Array();

// Handle function key on every pages
document.onkeydown = handleFunctionKey;
document.onhelp=function(){return false};
window.onhelp=function(){return false};

function handleFunctionKey(e){
        if (!e) {
            var e = window.event;
        }
        var key = e.keyCode;
        if ((key > 111) && (key < 123)){
            if (!e.stopPropagation){ // IE
                e.keyCode = 0;
                e.cancelBubble = true;
            } else {e.stopPropagation(); // FireFox
}
            return false;
        }
}

/*
 * Validate if data type is numeric or not. @param : sField (value of element,
 * not object). @return : true or false @example validateNumeric("25000") return
 * true
 */
function validateNumeric(sField){
    var objRegExp  =  /(^-?\d\d*\.\d*$)|(^-?\d\d*$)|(^-?\.\d\d*$)/;
    return objRegExp.test(sField);

}


/*
 * To format input number by adding decimal part as specify with decnum. @param
 * num - input number to be formatted. @param decnum - amount of decimal
 * position @sample formatCurrency(199,2) @return 199.00
 */
function formatCurrency(num, decnum) {
 if(checkNullOrEmpty(num) || checkNullOrEmpty(decnum)){
     return false;
 }
 // remove extra decimal point
 var decPos = num.toString().indexOf(".");

 var decpart = "";
 if ( decPos != -1){
     // Create integer part and decimal part.
     var arr  = num.split(".");
     intpart = arr[0];
     decpart = arr[1];
     num = intpart + "."  + decpart;
 }

    num = num.toString().replace(/\$|\,/g,'');
    if(isNaN(num)){num = "0";}
    sign = (num == (num = Math.abs(num)));
    divideby = Math.pow(10, decnum);
    num = Math.floor(num*100+0.50000000001);

    var cents = "";

    var tempNum = (num/100).toString();
    if (tempNum.indexOf(".") != -1){
        var tempNumArr = tempNum.split(".");
        cents = tempNumArr[1];
    }

     num = Math.floor(num/100).toString();

    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++) {
        num = num.substring(0,num.length-(4*i+3))+','+ num.substring(num.length-(4*i+3));
    }

    return (decnum == 0) ? num : AppendDecimal(num + '.'+ cents, decnum);
}

/*
 * Add decimal digit @param : tmpVal @param : decnum @return : data @example
 * AppendDecimal("25000",3) return 25,000.000
 */
function AppendDecimal(tmpVal, decNum){
    decPos = tmpVal.indexOf(".");
    if ( decPos != -1){
        var arr  = tmpVal.split(".");
        intpart = arr[0];
        decpart = arr[1];
        tmpVal = intpart + "." + CompleteDecimal(decpart,decNum);
 }else{
        tmpVal += ".";
        tmpVal = AddZero(tmpVal,decNum);
    }
 return tmpVal;
}

/*
 * CompleteDecimal is used as sub-function of formatCurrency function.
 */
function CompleteDecimal(decpart,decnum){
 if (decpart.length == 0){decpart = AddZero(decpart,decnum);}else
    if (decpart.length < decnum){
         decpart = AddZero(decpart, decnum - decpart.length);
       }else{
         if(decpart.length > decnum){decpart =  decpart.substr(0, decnum);}
       }
 return decpart;
}

/*
 * Add zero digit @param : DecNumStr @param : num @return : DecNumStr @example
 * AddZero("25,000.",3) return 25,000.000 Also used as sub-function of
 * formatCurrency.
 */
function AddZero(DecNumStr,num){
    for (i=0;i<num ; i++){
        DecNumStr = DecNumStr+'0';
    }
    return DecNumStr;
}

/*
 * Validate type data is currency or not. @param : sField @return : true or
 * false @example1 validateCurrency("200,701.01") return true @example2
 * validateCurrency("200,701.01a") return false
 */

function validateCurrency(sField) {
 sField = Trim(sField);

 if ( sField.indexOf(",") != -1 ) {
     var startnode = 0 ;
     var sFieldtemp ;
     while ((sField.indexOf(",",startnode) != -1) || (startnode < sField.length) ) {
         if ( sField.indexOf(",",startnode) != -1 ) {
             sFieldtemp = sField.substring(startnode,sField.indexOf(",",startnode));

             if ( sField.indexOf(",",startnode) == sField.length ){
                 startnode = sField.length;
             }else{
                 startnode = sField.indexOf(",",startnode)+1;
             }
         }else{
             sFieldtemp = sField.substring(startnode,sField.length);
             startnode = sField.length;
         }
         sFieldtemp = Trim(sFieldtemp);

         if ( !validateNumeric(sFieldtemp) ) {
            return false;
        }
     }
     return true ;
 } else {
     if ( !validateNumeric(sField) ) {
        return false;
    }
 }
 return true ;
}


/*
 * ==================================== FORMATTING INPUT
 * ==============================
 */
/*
 * For compare start date and end date. If at least 1 parameter is null or
 * empty, it returns true. If start date in greater than end date then return
 * "false". @param : varStartDate @param : varEndDate @return : true or false
 * @example compareDate("20070101","20060101") return false
 */
function compareDate(varStartDate,varEndDate){
    try {
        if (checkNullOrEmpty(varStartDate) || checkNullOrEmpty(varEndDate)) {
            return true;
        }
        if (Number(varStartDate) > Number(varEndDate)) {
            return false;
        }
        return true;
    } catch (e) {
        return false;
    }
}

/*
 * Change date format from "dd/MM/yyyy" to "yyyyMMdd" @param : varParam
 * (dd/MM/yyyy) @return : yyyyMMdd
 */
function returnYyyyMmDdForAllFormat(varParam,format) {
    if ((varParam == null) || (varParam == "")) {
        return "";
    }
    varArray = varParam.split("/");
    if(format == 'dd/MM/yyyy'){
        if(varArray[1].length == 1){varArray[1] = "0"+varArray[1];}
        if(varArray[0].length == 1){varArray[0] = "0"+varArray[0];}
        varReturn = varArray[2] + varArray[1] + varArray[0];
    }
    else if(format == 'MM/dd/yyyy'){
        if(varArray[1].length == 1){varArray[1] = "0"+varArray[1];}
        if(varArray[0].length == 1){varArray[0] = "0"+varArray[0];}
        varReturn = varArray[2] + varArray[0] + varArray[1];
    }
    else if(format == 'yyyy/MM/dd'){
        if(varArray[1].length == 1){varArray[1] = "0"+varArray[1];}
        if(varArray[2].length == 1){varArray[2] = "0"+varArray[2];}
        varReturn = varArray[0] + varArray[1] + varArray[2];
    }
    return varReturn;
}

/*
 * Change date format value depend on dateFormat 'dd/MM/yyyy' or 'MM/dd/yyyy'
 * (yyyy-MM-dd HH:mm:ss) @return : depend on dateFormat
 */
function returnObjectDateToDateFormat(varParam,dateFormat) {
    if ((varParam == null) || (varParam == "")) {
        return "";
    }
    varArray = varParam.split(" ")[0].split("-");
    if(dateFormat == 'dd/MM/yyyy'){
        varReturn = varArray[2] + "/" + varArray[1] + "/" + varArray[0];
    }
    if(dateFormat == 'MM/dd/yyyy'){
        varReturn = varArray[1] + "/" + varArray[2] + "/" + varArray[0];
    }
    if(dateFormat == 'yyyy/MM/dd'){
        varReturn = varArray[0] + "/" + varArray[1] + "/" + varArray[2];
    }

    return varReturn;
}

function returnYyyyMmDd(varParam) {
    if ((varParam == null) || (varParam == "")) {
        return "";
    }
    varArray = varParam.split("/");
    varReturn = varArray[2] + varArray[1] + varArray[0];
    return varReturn;
}

function delComma (varInput) {
    if (checkNullOrEmpty(varInput)) {
        return varInput;
    }
    return varInput.replace(/,/g, "");
}

function delHypen (varInput) {
    if (checkNullOrEmpty(varInput)) {
        return varInput;
    }
    return varInput.replace(/-/g, "");
}

/*
 * trimAllObject is used for trim space of all input textbox and textarea on
 * screen. Related functions are Trim(), LTrim() and RTrim().
 */
function trimAllObject() {
    varButton = document.getElementsByTagName("input");
    for (_i = 0;_i < varButton.length;_i++) {
        if ((varButton[_i].type == 'text')
                || (varButton[_i].type == 'hidden')) {
            varButton[_i].value = Trim(varButton[_i].value);
        }
    }

    varButton = document.getElementsByTagName("textarea");
    for (_i = 0;_i < varButton.length;_i++) {
            varButton[_i].value = Trim(varButton[_i].value);
    }
}
/*
 * To trim all input type "text" and "hidden" inside a specified <div> area.
 */
function trimAllObjectInArea(divarea) {
    varButton = eval("document.getElementById('"+divarea+"').getElementsByTagName('input')");
    for (_i = 0;_i < varButton.length;_i++) {
        if ((varButton[_i].type == 'text')
                || (varButton[_i].type == 'hidden')) {
            varButton[_i].value = Trim(varButton[_i].value);
        }
    }

    varButton = eval("document.getElementById('"+divarea+"').getElementsByTagName('textarea')");
    for (_i = 0;_i < varButton.length;_i++) {
            varButton[_i].value = Trim(varButton[_i].value);
    }
}

/* ============================= INPUT CHECKING ================================ */
/*
 * check length @param varInput @param varMaxLength @return true or false
 * @sample varInput = "ABC" varMaxLength = 4 then return true;
 */
function checkInputLength(varInput,varMaxLength) {
    if (checkNullOrEmpty(varInput)) {
        return true;
    }
    varLength = 0;
    for (_i = 0;_i < varInput.length;_i++) {
        if ((0 <= varInput.charCodeAt(_i)) && (varInput.charCodeAt(_i) <= 255)) {
            varLength = varLength + 1;
        } else {
            varLength = varLength + 2;
        }
    }
    if (varLength > varMaxLength) {
        return false;
    }
    return true;
}

/*
 * Check input value if it is integer or not.
 */
function checkIntegerNumber(varInput) {
    if (varInput.charAt(0) == "-") {
        varInput = varInput.substring(1);
    }
    varCheck = /^(\d)+$/;
    if (!varCheck.test(varInput)) {
        return false;
    }
    return true;
}

/*
 * Check input value if it is integer or not (0-9).
 */
function checkInputNumber(varInput) {
    if (checkNullOrEmpty(varInput)) {
        return true;
    }
    if (varInput.charAt(0) == "-") {
        varInput = varInput.substring(1);
        if (varInput.length == 0) {
            return false;
        }
    }
 if (varInput.match(/[^0-9]/) ) {
     return false;
 }
    return true;
}

/*
 * Check input value if it is decimal or not (0-9,'.').
 */
function checkDecimalNumber(varInput) {
    if (checkIntegerNumber(varInput) == true) {
        return true;
    }
    if (varInput.charAt(0) == "-") {
        varInput = varInput.substring(1);
    }
    varCheck = /^(\d)+\.(\d)+$/;
    if (!varCheck.test(varInput)) {
        return false;
    }
    return true;
}

/*
 * ==================================== CHECKBOX ACTION
 * =============================
 */

/*
 * Force to input at least 1 data @param : frm - name of form. @param :
 * radio_name - name of checkbox. @return : true, if there is at least 1
 * checkbox is checked otherwise return false.
 */
function hasItems(frm,checkbox_name){
    var counter = 0;
    for (var index = 0; index < frm.elements.length; ++index){
        var el = frm.elements[index];
        if (("checkbox" == el.type) && (checkbox_name == el.name) && el.checked){++counter;}
    }
    if (0 == counter){
        return false;
    }else{
        return true;
    }
}

/*
 * To count checked items for a specified checkbox name. @param : frm - form
 * name. @param : checkbox_name @return : int - amount of checked item.
 */
function countItems(frm,checkbox_name){
    var counter = 0;
    for (var index = 0; index < frm.elements.length; ++index){
        var el = frm.elements[index];
        if (("checkbox" == el.type) && (checkbox_name == el.name) && el.checked){
            ++counter;
        }
    }
    return counter;
}

/*
 * Tick all of checkbox @param : selAll @param : name
 */
function tickAll(selAll, formname, name) {

    var form = eval("document."+formname);

    var count = form.elements.length;

    for (i = 0; i<count; i++) {
        var ele = form.elements[i];
        if (ele.name == name) {
              if (!ele.disabled) {
                if (selAll.checked){ele.checked = 1;} else {ele.checked = 0;}
            }
        }
    }
}

/*
 * Limit amount of data for clicking on checkbox. @param : objName @param :
 * limit @example checkboxLimit(document.forms.world.countries, 2) user can
 * click 2 data
 */
function checkboxLimit(objName, limit){
    var checkgroup=name;
    var limit=limit;
    for (var i=0; i<checkgroup.length; i++){
        checkgroup[i].onclick=function(){
            var checkedcount=0;
            for (var i=0; i<checkgroup.length; i++) {
                checkedcount+=(checkgroup[i].checked)? 1 : 0;
                if (checkedcount>limit){
                    alert("You can only select a maximum of "+limit+" checkboxes");
                    this.checked=false;
                }// End if
            }// End for 1
        }// End function.
    }// End for 2
}// end fuction.


// =============================================================================
// format empty string to null
// Parameter: formatString
// Return: objReturn
// =============================================================================
function formartEmptyStringToNull(formatString){
 if (isNull(formatString) || isUndefined(formatString) || isBlank(formatString)){
     return null;
 }else{
     return formatString;
 }
}

/* ====================== START DATE SCRIPT ================ */
/*
 * Change input string from "dd/MM/yyyy" format to Date.
 */
function fromStringToDate(strDate) {

 if(strDate.length==0){
        return null;
 }
    var dateSeperator = "/";
    var intYear;
    var intMonth;
    var intDay;

 if(strDate.length!=10){
   return null;
 }

 if((strDate.charAt(2) != dateSeperator)
     || (strDate.charAt(5) != dateSeperator)){
   return null;
 }else{
   intDay = parseFloat(strDate.substring(0,2));
   intMonth = parseFloat(strDate.substring(3,5));
   intYear = parseFloat(strDate.substring(6));
 }

    var date = new Date();
    date.setFullYear(intYear,intMonth-1,intDay);
    return date;
}

/*
 * Change input from Date() to string in format "dd/MM/yyyy".
 */
function fromDateToString(dDate) {
 if (isNull(dDate)){
     return null;
 }else{
     var bYear = dDate.getYear();
        var bMonth = dDate.getMonth();
        var bDate = dDate.getDate();
     return bDate+'/'+bMonth+'/'+bYear;
 }
}

/*
 * Change input string, if it contains html text &,",<,>,',\, these character
 * will be replaced with text.
 */
function htmlDecode(strValue){
 var ret = strValue;
 if (!isUndefined(strValue) && !isBlank(strValue) && isNaN(strValue)){
     ret  = strValue.replace(/&/g, "&amp;").replace (/\"/g, "&quot;").replace (/</g, "&lt;").replace (/>/g, "&gt;").replace(/\'/g, "&#39;").replace(/\\/g, "&frasl;");
 }
 return ret;

}

/*
 * Change input string, if it contains html text, &,",<,>,',\, these character
 * will be replaced with text.
 */
function htmlEncode(strValue){
 var ret = strValue;
 if (!isUndefined(strValue) && !isBlank(strValue) && isNaN(strValue)){
        ret  = strValue.replace(/&amp;/g, "&").replace (/&quot;/g, "\"").replace (/&lt;/g, "<").replace (/&gt;/g, ">").replace(/&#39;/g, "\'").replace(/&frasl;/g, "\\");
 }
 return ret;
}

// =============================================================================
// get First Day Of Next Month By Given Time
// [Input Parameters] given_time : given time formatted in MMYYYY
// [Return Parameters] strFirstDayOfNextMonth : First Day Of Next Month By Given
// Time and returned date format is DDMMYYYY hh24:mi:ss
// =============================================================================
function getFirstDayOfNextMonthByGivenTime(given_time){
    var strFirstDayOfNextMonth = "";

    if (!isUndefined(given_time) && !isNull(given_time) && (given_time.length==6)){
        var intMonth = parseFloat(given_time.substring(0,2));
     var intYear = parseFloat(given_time.substring(2,6));
     var datGivenDate = Module.date.get(intYear,intMonth,1,0,0,0);// given
                                                                    // date
     var datNextMonthDate = Module.date.addMonth(datGivenDate,1);
     if (datNextMonthDate.getMonth()<9){
         strFirstDayOfNextMonth = "010"+(datNextMonthDate.getMonth()+1)+Module.date.getYear(datNextMonthDate)+" 00:00:00";
     }else{
         strFirstDayOfNextMonth = "01"+(datNextMonthDate.getMonth()+1)+Module.date.getYear(datNextMonthDate)+" 00:00:00";
     }
    }
    return strFirstDayOfNextMonth;

}

/*
 * Check amount of day is correct or not @param : varInputDay @param :
 * varInputMonth @param : varInputYear @return : True or False @example
 * checkDay('32','01','2008') return false
 */
function checkDay(varInputDay, varInputMonth, varInputYear){

    var nDateMax = 31;
    if( parseFloat(varInputMonth) == 4){ nDateMax = 30; }
    if( parseFloat(varInputMonth) == 6){ nDateMax = 30; }
    if( parseFloat(varInputMonth) == 9){ nDateMax = 30; }
    if( parseFloat(varInputMonth) == 11){ nDateMax = 30; }
    if( parseFloat(varInputMonth) == 2){
        nDateMax = 29;
        if((parseFloat(varInputYear)) != 0){
            var year = parseFloat(varInputYear);
            if(! (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))){nDateMax = 28;}
        }
    }

    if ( parseFloat(varInputDay) > nDateMax ) {
        return false;
    } else {
        return true;
    }

}

/*
 * Check Input Date @param dd/MM/yyyy
 */
function checkDate(varInput,format) {
    if (checkNullOrEmpty(varInput)) {
        return true;
    }
    try {
        if(format == 'dd/MM/yyyy'){
            var regType = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{1,4})/;
            // Check Date/Month/Year
            varInput = varInput.match(regType);
            if (varInput == null) {
                return false;
            }
            // new Date
            var varDateTemp = new Date(varInput[5],varInput[3]-1,varInput[1]);
            if ((varDateTemp.getFullYear() != varInput[5])
                    || ((varDateTemp.getMonth() + 1) != varInput[3])
                    || (varDateTemp.getDate() != varInput[1])) {
                return false;
            }
        }else if(format == 'MM/dd/yyyy'){
            var regType = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{1,4})/;
            // Check Year/Month/Date
            varInput = varInput.match(regType);
            if (varInput == null) {
                return false;
            }
            // new Date
            var varDateTemp = new Date(varInput[5],varInput[1]-1,varInput[3]);
            if ((varDateTemp.getFullYear() != varInput[5])
                    || ((varDateTemp.getMonth() + 1) != varInput[1])
                    || (varDateTemp.getDate() != varInput[3])) {
                return false;
            }
        }else if(format == 'yyyy/MM/dd'){
            var regType = /^(\d{1,4})(\/)(\d{1,2})(\/)(\d{1,2})/;
            // Check Year/Month/Date
            varInput = varInput.match(regType);
            if (varInput == null) {
                return false;
            }
            // new Date
            var varDateTemp = new Date(varInput[1],varInput[3]-1,varInput[5]);
            if ((varDateTemp.getFullYear() != varInput[1])
                    || ((varDateTemp.getMonth() + 1) != varInput[3])
                    || (varDateTemp.getDate() != varInput[5])) {
                return false;
            }
        }

        varDateTemp.getMinutes();
    } catch (e) {
        return false;
    }
    return true;
}


/*
 * Check Time @param HH:mm:ss
 */
function checkTime(varInput) {
    try {
        if (checkNullOrEmpty(varInput)) {
            return true;
        }
        var regType = /^(\d{1,2}):(\d{1,2}):(\d{1,2})/;
        varInput = varInput.match(regType);
        if (varInput == null) {
            return false;
        }
        // Check Hour
        var HH = varInput[1];
        if ((HH > 23) || (HH < 0)) {
            return false;
        }
        // Check Minute
        var mm = varInput[2];
        if ((mm > 59) || (mm < 0)) {
            return false;
        }
        // Check Second
        var ss = varInput[3];
        if ((ss > 59) || (ss < 0)) {
            return false;
        }
    } catch (e) {
        return false;
    }
    return true;
}

/*
 * Change format date from 'yyyy/MM/dd' to dd/Month/yyyy @param : strDate format
 * 'yyyy/MM/dd' @return : strData format 'dd/Month/yyyy' @example
 * getPdfDate('2008/03/11') return 11/Mar/2008
 */
function getPdfDate(strDate){

 if (isUndefined(strDate) || isBlank(strDate) || isNull(strDate)){
     return "";
 }
    var aMonthName = new Array();
 aMonthName["01"] = "Jan";
 aMonthName["02"] = "Feb";
 aMonthName["03"] = "Mar";
 aMonthName["04"] = "Apr";
 aMonthName["05"] = "May";
 aMonthName["06"] = "Jun";
 aMonthName["07"] = "Jul";
 aMonthName["08"] = "Aug";
 aMonthName["09"] = "Sep";
 aMonthName["10"] = "Oct";
 aMonthName["11"] = "Nov";
 aMonthName["12"] = "Dec";

 var strYear = strDate.split("|")[0].split("/")[0];
 var strMonth = strDate.split("|")[0].split("/")[1];
 var strDay = strDate.split("|")[0].split("/")[2];


 return strDay+"/"+ aMonthName[strMonth] +"/" + strYear;
}


/*
 * Default current date
 */
function getCurrentData(){
    var mydate=new Date();
    var theyear=mydate.getYear();
    if (theyear < 1000){
            theyear+=1900;

            var theday=mydate.getDay();
            var themonth=mydate.getMonth()+1;
            if (themonth<10){
                themonth="0"+themonth;
            }
            var theday=mydate.getDate();
            if (theday<10){
                theday="0"+theday;
            }

    }
}

function toattach(pathname, filename) {
    window.location = "hmsb/download.jsp?path_name="+pathname+"&file_name="+filename;
}

/*
 * clear Datas To Empty
 */
function clearData(varObjectName) {
    var varObject = document.getElementsByName(varObjectName);
    varObject[0].value = "";
    varObject[0].style.backgroundColor = "";
}



// ################################################# END DATE SCRIPT
// ############################################

function checkLength(nStr,pos,digit){
    nStr += '';
    x = nStr.split('.');
    if (pos == 'before'){x1 = x[0];} else {x1 = x.length > 1 ? x[1] : '';}

    // over max length
    if (x1.length > digit) {
        return false;
    }
    return true;
}

// START CHECK INPUT FORM
function isTimeString(t) {
    // Check a given string t whether or not in "hh:mm:ss"

    var a = /^(\d{1,2}):(\d{2}):(\d{2})$/.exec(t);
    if (!a) {
        return false;
    }
    var h = parseInt(RegExp.$1, 10);
    var m = parseInt(RegExp.$2, 10);
    return ((h < 24) && (m < 60));
} // isTimeString

function isDateString(s) {
    // Check string of date whether in "dd/mm/yyyy"
    // and also valid day in month
 // /^\d{4}(\-|\/|\.)\d{1,2}\1\d{1,2}$/
    var r = /^(\d{2})\/(\d{2})\/(\d{4})$/;
    var d, m, y;
    var a = r.exec(s);
    if (!a) {
        return false;
    }
    d = parseInt(RegExp.$1, 10);
    m = parseInt(RegExp.$2, 10);
    y = parseInt(RegExp.$3, 10);
    if ((y < 1900) || (d < 1)) {
        return false;
    }
    if ((m < 1) || (m > 12)) {
        return false;
    }
    var t = new Date(y,m,0);
    return (d <= t.getDate());
} // isDateString

function isCheck(c){
    var flag = false;
    if(c.length > 0){
        for (var counter = 0; counter < c.length; counter++){
            if (noRecord[counter].checked){
                flag = true;
                break;
            }
        }
    }else{
        if(c.checked){
            flag = true;
        }
    }
    return flag;
}

var regex__ = {
    EMPTY: /^\s*$/,
    NUMBER: /^[\+\-]{0,20}\d+$/,
    POSITIVE: /^\+?[\d]{0,20}$/
}
function checkInputForm(input){

    for(i=0;i<=input[0].length-2;i=i+2){
        for(j=0;j<input.length;j++){
            var response = null;
            try{
                var case__=(input[j])[0+i];
                var to__=null;
                 if(case__.indexOf('MAX_')>-1){
                    to__ = case__.split("_")[1];
                     case__='MAX_';
                 }
                 else if(case__.indexOf('MAXHD_')>-1){
                     to__ = case__.split("_")[1];
                     case__='MAXHD_';
                 }
                switch (case__){
                    case 'EMPTY':
                        if(regex__.EMPTY.test((input[j])[(input[j]).length-1].value)){
                            response = 'FOCUS';
                        }
                    break;
                    case 'NUMBER':
                        if(!regex__.NUMBER.test((input[j])[(input[j]).length-1].value)){
                            response = 'FOCUS';
                        }
                    break;
                    case 'POSITIVE':
                        if(!regex__.POSITIVE.test((input[j])[(input[j]).length-1].value)){
                            response = 'FOCUS';
                        }
                    break;
                    case 'POSITIVE0':
                        if(!regex__.POSITIVE.test((input[j])[(input[j]).length-1].value)||((input[j])[(input[j]).length-1].value==0)){
                            response = 'FOCUS';
                        }
                    break;
                    case 'DATE':
                        if(!isDateString((input[j])[(input[j]).length-1].value)){
                            response = 'FOCUS';
                        }
                    break;
                    case 'TIME':
                        if(!isTimeString((input[j])[(input[j]).length-1].value)){
                            response = 'FOCUS';
                        }
                    break;
                    case 'CHECK':
                        if(!isCheck((input[j])[(input[j]).length-1])){
                            response = 'FOCUS';
                        }
                    break;
                    case 'NOT_EMPTY_NUMBER':
                        if(!regex__.EMPTY.test((input[j])[(input[j]).length-1].value)&&!regex__.NUMBER.test((input[j])[(input[j]).length-1].value)){
                            response = 'FOCUS';
                        }
                    break;
                    case 'NOT_EMPTY_POSITIVE':
                        if(!regex__.EMPTY.test((input[j])[(input[j]).length-1].value)
                        &&(!regex__.POSITIVE.test((input[j])[(input[j]).length-1].value)
                        ||((input[j])[(input[j]).length-1].value==0))){
                            response = 'FOCUS';
                        }
                    break;
                    case 'NOT_EMPTY_DATE':
                        if(!regex__.EMPTY.test((input[j])[(input[j]).length-1].value)&&!isDateString((input[j])[(input[j]).length-1].value)){
                            response = 'FOCUS';
                        }
                    break;
                    case 'NOT_EMPTY_TIME':
                        if(!regex__.EMPTY.test((input[j])[(input[j]).length-1].value)&&!isTimeString((input[j])[(input[j]).length-1].value)){
                            response = 'FOCUS';
                        }
                    break;
                    case 'FUNCTION':
                        if((input[j])[(input[j]).length-1] != null){
                            response = 'FOCUS';
                        }
                    break;
                    case 'MAX_':
                        if(((input[j])[(input[j]).length-1]).value.length > to__){
                            response = 'FOCUS';
                        }
                    break;
                    case 'MAXHD_':
                        if((((input[j])[(input[j]).length-1]).value.length > 0) && (((input[j])[(input[j]).length-1]).value.length > to__)){
                            response = 'FOCUS';
                        }
                    break;
                    default:
                    break;

                }
                switch (response){
                    case 'FOCUS':
                            alert((input[j])[1+i]);
                            ((input[j])[(input[j]).length-1]).focus();
                            return false;
                    break;
                    case 'NOT-FOCUS':
                            return false;
                    break;
                    default:
                    break;

                }
            }catch(e){
                alert(e);
                return false
            }
        }
    }
    return true;
}
// END CHECK INPUT FORM

// fit maxlength of textarea
function imposeMaxLengthTextarea(Object, MaxLen)
{
    return (Object.value.length <= MaxLen);
}

function _parseDate__(str, fmt) {
    _MN___ = new Array
    ("January",
     "February",
     "March",
     "April",
     "May",
     "June",
     "July",
     "August",
     "September",
     "October",
     "November",
     "December");
    var y = 0;
    var m = -1;
    var d = 0;
    var a = str.split(/\W+/);
    if (!fmt) {
        throw 'ivalid format';
    }
    var b = fmt.match(/%./g);
    var i = 0, j = 0;
    var hr = 0;
    var min = 0;
    for (i = 0; i < a.length; ++i) {
        if (!a[i]) {
            continue;
        }
        switch (b[i]) {
            case "%d":
            case "%e":
            d = parseInt(a[i], 10);
            break;
            case "%m":
            m = parseInt(a[i], 10) - 1;
            break;
            case "%Y":
            case "%y":
            y = parseInt(a[i], 10);
            (y < 100) && (y += (y > 29) ? 1900 : 2000);
            break;
            case "%b":
            case "%B":
            for (j = 0; j < 12; ++j) {
                if (_MN___[j].substr(0, a[i].length).toLowerCase() == a[i].toLowerCase()) { m = j; break; }
            }
            break;
            case "%H":
            case "%I":
            case "%k":
            case "%l":
            hr = parseInt(a[i], 10);
            break;
            case "%P":
            case "%p":
            if (/pm/i.test(a[i]) && (hr < 12)){hr += 12;}
            break;
            case "%M":
            min = parseInt(a[i], 10);
            break;
        }
    }

    if(fmt == "%d/%m/%Y"){
        if(!regex_.DDMMYYYY_.test(str)) {
            throw 'invalid format';
        }
    }
    else if(fmt == "%Y/%m/%d"){
        if(!regex_.YYYYMMDD_.test(str)) {
            throw 'invalid format';
        }
    }

    if ((y != 0) && (m != -1) && (d != 0)) {
        return new Date(y, m, d, hr, min, 0);
    } else {
        throw 'invalid format';
    }
}

// addComma
function addComma(obj)
{   
    if(!isNaN(obj) || obj.value != "")
    {
        obj.value = obj.value.replace(/[,]/g,"");
        nStr = obj.value;
        nStr += '';
        x = nStr.split('.');
        x1 = x[0]+"";
        /** remove leading zero */
        x1 = x1.replace(/(^|[^.\d])0+(\d)/g, '$1$2')
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        obj.value = x1 + x2;
    }
    return obj;
}

function addCommaStr(str)
{
    x = str.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    str = x1 + x2;
    return str;
}
   
function PositiveDoubleFilter( e ){
    var cd = (e.which || e.keyCode);
    var ch = String.fromCharCode(cd);
    var vl = $(e.target ? e.target : e.srcElement).val();

    if( cd === 8 ) // 'Backspace' key press.
        return true;
    if( cd === 9 ) // 'TAB' key press.
        return true;
    if(cd !== 8 && (ch === '.' && (vl.length === 0 || vl.indexOf('.') > -1)) || ".0123456789".indexOf(ch) < 0)
        return false;
    return true;
}

function setMaxLengthDecimalDigit(obj, digit) {
    var objvalue = delComma(obj.value);
    if(checkNullOrEmpty(objvalue)){
        obj.value == "";
    }else{
        if(!isDecimalValue(objvalue)){
            obj.value = "";
        }else{
            obj.value = BigNumber(objvalue+"","0").toFixed(digit);
        }
    }
    return obj.value;
}

/*
 * Check input value if it is lower case (0-9) , (a-z) and (_).
 */
function checkInputLowerCase(varInput) {
    if (checkNullOrEmpty(varInput)) {
        return false;
    }
    if (!varInput.match(/[^0-9,a-z,_]/) ) {
        return false;
    }
    return true;
}

/** Validate max length in textarea */
function textLimit(field,maxlen) {
    if(field.value.length > maxlen){
        while(field.value.length > maxlen){
            field.value=field.value.replace(/.$/,'');
        }
    }
}

/** ###############################################
 *  Check max length of data.
 *  1st param is object of element.
 *  2nd param is length of digit type.
 *  3th param is length of decimal type.
 *  4th param is notification message.
*/
function checkNumuricMaxlength(obj, integerLength, decimalLength, labelLimitAmount){
    if (!checkDecimalNumber(delComma(obj.val()))) {
        return;
    }
    if(isValidMaxLengthIntegerPart(obj, labelLimitAmount, integerLength)){
        if(isValidMaxLengthDecimalPart(obj, labelLimitAmount, decimalLength)){
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

function isValidMaxLengthIntegerPart(oObj, sMessage, length) {
    
    var objvalue = delComma(oObj.val());
    objvalue = delHypen(objvalue);
    if ( (objvalue.indexOf(".") == -1) && //Input is integer
         (Number(objvalue).toString().length > length) ) {
        alert(msgNumuricLength.replace("{0}", sMessage).replace("{1}", length));
        return false;
    }else if (Number(objvalue).toString().split(".")[0].length > length) { //Subtracting zero such as 015.30 -> 15.30 before validate length
        alert(msgNumuricLength.replace("{0}", sMessage).replace("{1}", length));
        return false;
    }else {
        return true;
    }
}

function isValidMaxLengthDecimalPart(oObj, sMessage, length) {
    var objvalue = delComma(oObj.val());
    objvalue = delHypen(objvalue);
    if (objvalue.indexOf(".") == -1) { //Input is integer
        return true;
    }else if (objvalue.split(".")[1].length > length) { //Subtracting zero such as 015.30 -> 15.30 before validate length
        alert(msgDigitLength.replace("{0}", sMessage).replace("{1}", length));
        return false;
    }else {
        return true;
    }
}
/** ##############################################*/

/* 
* Start BigNumber
*/
function BigNumber(v1,v2){
    var real;
    v1=v1 != null && v1 != undefined && v1 != '' ? (''+v1):0;
    v2=v2 != null && v2 != undefined && v2 != '' ? (''+v2):0;

    function toFixed(digi,cell){
        v = real;
        cell = cell == undefined  || cell == true ? true:false;
        var a=v.split("."),z='';

        if(a.length > 1){// && digi <= (''+a[1]).length
            var zero = ''; for(var i=0;i<digi;i++){ zero += '0'; }
            var b=(a[1]+zero).substr(0,digi);
            var c=(a[1]+zero).substr(digi,1);

            if(!cell || (c.length && c < 5) ){ z = "0."+b; }
            else{
                var f1DigiB = parseFloat("0."+a[1]);
                var f2DigiB = parseFloat("0."+zero+1);
                z = (f1DigiB + f2DigiB).toFixed(digi);
            }
        }
        else{
            z='0.';for(var i=0;i<digi;i++){ z+='0'; }
        }
        return addEm(a[0]+"",z+"").real;
    }

    function addseps(x) {
        //make x a new variable
        var x=x;
        //make x a string
        x+="";
        //or x=String(x);
        //iLen is the number of digits before any decimal point
        // for 45.123, iLen is 2
        //iLen is the length of the number, if no decimals
        iLen=x.length;
        pos=x.indexOf(".");
        if (pos>-1) //there are decimals
        {
        iLen=pos;
        }
        //add the decimal point
        temp="";
        //add the decimal part to begin
        // with 45.123, we add the .123
        temp=x.substring(iLen,x.length);
        //iLen-1 is the rightmost non-decimal digit (5 in 98745.123)
        for (var i=iLen-1;i>=0;i--)
        //we add a separator when the expression (iLen-i-1)%3==0 is true...
        //except when i is (iLen-1), or the first digit
        //eg (98745.12). i is iLen-1, and the digit pos is next the decimal, 
        //it is 5. From here, we decrement i...iLen-2, iLen-3, iLen-4 ... when i is a multiple of
        //3, (i=iLen-iLen+4-1). This point is just before the number 7
        if ((iLen-i-1)%3==0&&i!=iLen-1)
        temp=x.charAt(i)+","+temp;
        else
        temp=x.charAt(i)+temp;
        return temp;
    }//end of addseps(x)


    function insertDec(num,decPlace) {
        var num=num;
        var decPlace=decPlace;
        ans="";
        if (decPlace>0)
        {
        for (var i=0;i<num.length;i++)
        {
        //when we get to the position we want to 
        //insert the decimal point, we add the point to
        //our string
        //decPlace is the number of decimal digits in the original, from getDec(x)
        //remember the return value of this function was return x.length-pos;
        if (i==num.length-decPlace)
        ans=ans+"."+num.charAt(i);
        else
        ans=ans+num.charAt(i);
        }
        return ans;
        }
        else
        return num;
    }//end of insertDec(x,decPlace)


    function getDec(x) {
        x=x+"";
        //position of first decimal digit
        pos=x.indexOf('.')+1;
        //if 0, then no decimals (-1+1)
        if (pos>0)
        return x.length-pos;
        else
        return 0;
        }//end of getDec(x)


        function removeCommas(aNum) {
        //remove any commas
        aNum=aNum.replace(/,/g,"");
        //remove any spaces
        aNum=aNum.replace(/\s/g,"");
        return aNum;
    }//end of removeCommas(aNum)

    function checkNum(aNum) {
        var isOK=0;
        var aNum=aNum+"";
        //if the number has one or none decimal points, lastIndexOf and indexOf
        //will give the same answer
        if (aNum.lastIndexOf(".")!=aNum.indexOf("."))
        isOK=1;
        else
        //here a regular expression is used to check for numbers and decimals
        if (aNum.match(/[^0-9.]/))
        isOK=2;
        return isOK;
    }//end of checkNum(aNum)

    function addEm(x,y) {
        var x=x;
        var y=y;
        //sum used for javascript
        x=removeCommas(x);
        y=removeCommas(y);
        if (!((checkNum(x)==0)&&(checkNum(y)==0)))
        {
            if (checkNum(x)>0){
                switch (checkNum(x)) {
                case 1:
                alert("Too many decimal points in number 1.")
                break    
                case 2:  
                alert("Some characters aren't numbers in number 1");  
                break       
                }//end of switch 
            }//end of check number 1

            if (checkNum(y)>0)
            {
                switch (checkNum(y)) {
                case 1:
                alert("Too many decimal points in number 2.")
                break    
                case 2:  
                alert("Some characters aren't numbers in number 2");  
                break       
                }//end of switch 
            }//end of check number 1
        }//end number problem
        else //number checks out
        {
        jssum=Number(x)+Number(y);
        decXo=getDec(x);
        decYo=getDec(y);
        decX=decXo;
        decY=decYo;

        str="";
        //remove decimal from x
        x=x.replace(".","");
        //remove decimal from y
        y=y.replace(".","");

        //initialise decPlace, which is the number of decimals in 
        //the number with the most decimals
        //First just assume x is the number with most decimal places
        //We are just declaring the variable, so any number could be used.
        var decPlace=decX;
        //Then check which is the correct number
        if (decX>decY)
        {
        decPlace=decX;
        //add zeros to y
        //the number of zeros is just the difference between the number of
        //decimal places in each number.
        //decYo is the number of decimals in the original y number.
        for (var i=0;i<(decPlace-decYo);i++)
        {
        //add a zero at the end
        y+="0";
        //increment decY, so we keep a record of the decimal position
        //in the new number. For instance
        // if decYo=2, as in 112, then the number is really 112/10^2
        //When we add zeros, the position becomes one more
        //for each zero we add .. 1120, decimal is now 3 from the end, or
        //the number is really 1120/10^3
        decY+=1;
        }

        }
        //add zeros to x, if y has more decimals
        //repeat of the above
        else
        if (decY>decX)
        {
        decPlace=decY;
        //add zeros to x
        for (var i=0;i<(decPlace-decXo);i++)
        {
        x+="0";
        decX+=1;
        }
        }


        //swap
        //find the length of the numbers, so we can put them in the right
        //order for the calculation
        xlen=x.length;
        ylen=y.length;
        //swap the numbers, if xlen isn't the longest
        if (xlen<ylen)
        {
        //swap values
        var temp=y;
        y=x;
        x=temp;
        temp=decX;
        decX=decY;
        decY=temp;
        //new lengths, if x is shorter
        //could have just swapped them as above!
        xlen=x.length;
        ylen=y.length;
        }
        //end of swapping

        //begin adding
        //set carry to zero.
        var carry=0;
        //set the string, s, which will contain the answer
        var s="";
        //now add the two numbers:
        //work down from the length of the longest string:
        //which is always x
        var numx,numy;
        for (var i=xlen-1;i>=0;i--)
        {
        //only add the y numbers, if there are any left, else we add zero
        numy=0;
        if ((ylen-xlen+i)>-1)
        numy=parseInt(y.charAt(ylen-xlen+i));
        //In the above, when ylen-xlen+i=0, we are at the end of the y numbers
        //and i=xlen-ylen. For the next i, (i-1), we will be beyond the length of y, and 
        //so just add a possible carry, and thereafter, just zeros

        //x is always found in the longest string
        numx=parseInt(x.charAt(i));
        //add the sum of the numbers and any carry from previously
        //we add only the units bit of the number
        //10 is the normal base for decimals
        s=(numy+numx+carry)%10+s;
        //we carry the tens bit of the new number
        carry=Math.floor((numy+numx+carry)/10);

        }//end of adding

        if (carry>0)
        s=carry+s;
        /*
        at the end of the first number, x, we might still have something to carry
        For instance, 9+9 gives 8, plus a FINAL carry of 1 (in a new place)

          9+
          9=
        18
        But:
         18+
           9
         27
        ... has a normal carry one place from the end, but no carry at the end
        so we add any carry on the front of the string
        */

        //put the decimals back in the numbers
        s=insertDec(s,decPlace);
        //now write out the answer:
        x=insertDec(x,decX);
        y=insertDec(y,decY);
        x=addseps(x);
        y=addseps(y);
        //once again check the lengths of the two numbers. 
        //swap numbers
        //find the length of the numbers, so we can put them in the right
        //order for the calculation
        xlen=x.length;
        ylen=y.length;
        //swap the numbers, if xlen isn't the longest
        if (xlen<ylen)
        {
        //swap values
        var temp=y;
        y=x;
        x=temp;
        temp=decX;
        decX=decY;
        decY=temp;
        //new lengths, if x is shorter
        //could have just swapped them as above!
        xlen=x.length;
        ylen=y.length;
        }
        //end of swapping

        //align the numbers by putting a space infront of the shortest number
        for (var i=0;i<(xlen-ylen);i++)
        y="&nbsp;"+y;

        //the answer string s, can only be one more longer than the x string
        //but we will write general code anyway
        //note, we cannot use y or ylen anymore because y contains "&nbsp;
        //which is 6 characters, creating one space!
        s=addseps(s);
        var slen=s.length;
        for (var i=0;i<(slen-xlen);i++){
            x="&nbsp;"+x;
            y="&nbsp;"+y;
        }
            real=(''+s).replace(/,/g,'');
            return {"jssum":jssum,"real":real,"float":s,"toFixed":toFixed};
        }//end of else (numCheck OK)
    }//end of  addEm(x,y)


    return addEm(v1,v2);
}

function validateMenuCode(){
    // [IN063] add new menu
    //for(var i=0;i<32;i++){
    for(var i=0;i<33;i++){
        var menu =  $('#menuCode'+ i ).val();
        if(!checkNullOrEmpty(menu)){
            document.getElementById(menu).style.display="";
        }
    }
}


function insertSlashToDate(calendarId) {
    var dateWithoutSlash = $("#" + calendarId).val();
    if (8 == dateWithoutSlash.length) {
        var result = dateWithoutSlash.substring(0, 4) + "/"
            + dateWithoutSlash.substring(4, 6)
            + "/" + dateWithoutSlash.substring(6, 8);
        $("#" + calendarId).val(result);
    }
}

function removeSlashFromDate(calendarId) {
    var dateWithSlash = $("#" + calendarId).val();
    var result = dateWithSlash.replace(/\//g, "");
    $("#" + calendarId).val(result);
}

function removeColonFromTime(inputId) {
    var timeWithColon = $("#" + inputId).val();
    if(!checkNullOrEmpty(timeWithColon)){
        var result = timeWithColon.replace(/:/g, "");
        $("#" + inputId).val(result);
    }
}

function addColonToTime(inputId) {
    var timeWithoutColon = $("#" + inputId).val();
    if(3 <= timeWithoutColon.length){
        if(3 == timeWithoutColon.length){
            timeWithoutColon = "0" + timeWithoutColon;
        }
        var len = timeWithoutColon.length;
        var result = timeWithoutColon.substring(0, len - 2);
        result = result + ":" + timeWithoutColon.substring(len - 2);
        $("#" + inputId).val(result);
    }
}

function doTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}