<%@ include file="./WEB-INF/jsp/includes/jsp_header.jspf"%>
<html>
<head>
<%@ include file="./WEB-INF/jsp/includes/html_header.jspf"%>
<script type="text/javascript" src="./js/business/Wcom001_Login.js" ></script>
</head>
<body>
<html:form styleId="Wcom001Form" action="/LoginAction.do" target="_parent" >
    <html:hidden styleId="method" property="method" value="doInitial" />
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="220">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <div align="center">
                    <table cellpadding="2" cellspacing="0" border="0" width="400" height="180" align="center">
                        <tr>
                            <td align="center">
                                <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="2">
                                    <tr>
                                        <td colspan="4">
                                            <table width="100%" height="142" border="0" cellpadding="0" 
                                                cellspacing="2" class="grid-table">
                                                <tr>
                                                    <td width="100%" align="right">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <div align="center">
                                                            You have no authority to access to the data. 
                                                            Please check your authority for the data access.<br>
                                                            Permission Denied<br>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="criteria-td">
                                                    <td align="center">
                                                        <input name="btnInitial" id="btnInitial" type="button" style="width: 120" 
                                                        value='  <bean:message bundle="gui" key="button.LOGIN" />  ' /> &nbsp;</td>
                                                </tr>
                                                <tr class="criteria-td">
                                                    <td align="center">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</html:form>
</body>
</html>
