<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isErrorPage="true" %>
<%@include file="/WEB-INF/jsp/includes/jsp_header.jspf" %>
<c:set var="STATUS_CODE" value="${requestScope['com.globaldenso.aij2.STATUS_CODE']}"></c:set>
<c:if test="${empty STATUS_CODE}">
    <c:set var="STATUS_CODE" value="${requestScope['javax.servlet.error.status_code']}"></c:set>    
</c:if>
<c:set var="SERVLET_NAME" value="${requestScope['com.globaldenso.aij2.SERVLET_NAME']}"></c:set>
<c:if test="${empty SERVLET_NAME}">
    <c:set var="SERVLET_NAME" value="${requestScope['javax.servlet.error.servlet_name']}"></c:set>
</c:if>
<c:set var="REQUEST_URI" value="${requestScope['com.globaldenso.aij2.REQUEST_URI']}"></c:set>
<c:if test="${empty REQUEST_URI}">
    <c:set var="REQUEST_URI" value="${requestScope['javax.servlet.error.request_uri']}"></c:set>
</c:if>
<c:set var="EXCEPTION_TYPE" value="${requestScope['com.globaldenso.aij2.EXCEPTION_TYPE']}"></c:set>
<c:if test="${empty EXCEPTION_TYPE}">
    <c:set var="EXCEPTION_TYPE" value="${requestScope['javax.servlet.error.exception_type']}"></c:set>
</c:if>
<c:set var="ERROR_MESSAGE" value="${requestScope['com.globaldenso.aij2.ERROR_MESSAGE']}"></c:set>
<c:if test="${empty ERROR_MESSAGE}">
    <c:set var="ERROR_MESSAGE" value="${requestScope['javax.servlet.error.message']}"></c:set>
</c:if>
<c:set var="EXCEPTION" value="${requestScope['com.globaldenso.aij2.EXCEPTION']}"></c:set>
<c:if test="${empty EXCEPTION}">
    <c:set var="EXCEPTION" value="${requestScope['javax.servlet.error.exception']}"></c:set>
</c:if>
<%

    pageContext.setAttribute("ID", "N/A");
    pageContext.setAttribute("CODE", "N/A");
    pageContext.setAttribute("DATE", new java.util.Date());

%>
<html>
<head>
<%@ include file="/WEB-INF/jsp/includes/html_header.jspf" %>

<title>Error Page.</title>
</head>
<body>
<p>ステータスコード : ${STATUS_CODE}</p>
<p>サーブレット名 : ${SERVLET_NAME}</p>
<p>リクエストURI : ${REQUEST_URI}</p>
<p>例外タイプ : ${EXCEPTION_TYPE}</p>
<p>例外ID : ${ID}</p>
<p>例外コード : ${CODE}</p>
<p>例外メッセージ : ${ERROR_MESSAGE}</p>
<p>例外発生日時 : <fmt:formatDate value="${DATE}" pattern="yyyy-MM-dd HH:mm:ss.SSS"/></p>
<p>スタックトレース :<br />
<c:forEach var="stackTraceLine" items="${EXCEPTION.stackTrace}">
    <c:out value="${stackTraceLine}" /><br />
</c:forEach>
</p>
</body>
</html>
