<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%--
 * error.jsp
 *
 * [JP] システムエラー画面
 * [EN] System error screen
 *
 * $ error.jsp 4294 2013-05-15 12:30:57Z HIDETOSHI_NAKATANI@denso.co.jp $
 *
 * Copyright (c) 2011 DENSO IT SOLUTIONS. All rights reserved.
--%>

<%@page isErrorPage="true" %>
<%-- [JP] アプリ基盤専用のカスタムタグ --%>
<%-- [EN] Custom tag dedicated Application Development Infrastructure --%>
<%-- [JP] このaij2:aij2ErrorTagをアプリ側で勝手に利用しないで下さい。 --%>
<%-- [EN] Do not use this aij2:aij2ErrorTag for your application without permission. --%>
<%@ taglib prefix="aij2" uri="http://aij2_core.globaldenso.com/customTag" %>
<aij2:aij2ErrorTag />

<html>
<head>
    <meta http-equiv="Content-Type"  content="text/html; charset=${pageContext.request.characterEncoding}" />
    <title>Error Page</title>
</head>
<body>
    <p>Status code : ${STATUS_CODE}</p>
    <p>Request URI : ${REQUEST_URI}</p>
    <p>Exception type : ${EXCEPTION_TYPE}</p>
    <p>Exception ID : ${ID}</p>
    <p>Exception code : ${CODE}</p>
    <p>Exception message : ${ERROR_MESSAGE}</p>
    <p>Date and time the exception occurred : <fmt:formatDate value="${DATE}" pattern="yyyy-MM-dd HH:mm:ss.SSS"/></p>
    <p>Stack trace :<br />
    <c:forEach var="stackTraceLine" items="${EXCEPTION.stackTrace}">
        <c:out value="${stackTraceLine}" /><br />
    </c:forEach>
    </p>
</body>
</html>
