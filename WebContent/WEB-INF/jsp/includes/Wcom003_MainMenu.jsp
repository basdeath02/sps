<jsp:useBean id="mainMenuCons" class="com.globaldenso.asia.sps.common.constant.MainMenuConstants" />


<html:hidden property="menuCode[0].menuCd" styleId="menuCode0" />
<html:hidden property="menuCode[1].menuCd" styleId="menuCode1" />
<html:hidden property="menuCode[2].menuCd" styleId="menuCode2" />
<html:hidden property="menuCode[3].menuCd" styleId="menuCode3" />
<html:hidden property="menuCode[4].menuCd" styleId="menuCode4" />
<html:hidden property="menuCode[5].menuCd" styleId="menuCode5" />
<html:hidden property="menuCode[6].menuCd" styleId="menuCode6" />
<html:hidden property="menuCode[7].menuCd" styleId="menuCode7" />
<html:hidden property="menuCode[8].menuCd" styleId="menuCode8" />
<html:hidden property="menuCode[9].menuCd" styleId="menuCode9" />
<html:hidden property="menuCode[10].menuCd" styleId="menuCode10" />
<html:hidden property="menuCode[11].menuCd" styleId="menuCode11" />
<html:hidden property="menuCode[12].menuCd" styleId="menuCode12" />
<html:hidden property="menuCode[13].menuCd" styleId="menuCode13" />
<html:hidden property="menuCode[14].menuCd" styleId="menuCode14" />
<html:hidden property="menuCode[15].menuCd" styleId="menuCode15" />
<html:hidden property="menuCode[16].menuCd" styleId="menuCode16" />
<html:hidden property="menuCode[17].menuCd" styleId="menuCode17" />
<html:hidden property="menuCode[18].menuCd" styleId="menuCode18" />
<html:hidden property="menuCode[19].menuCd" styleId="menuCode19" />
<html:hidden property="menuCode[20].menuCd" styleId="menuCode20" />
<html:hidden property="menuCode[21].menuCd" styleId="menuCode21" />
<html:hidden property="menuCode[22].menuCd" styleId="menuCode22" />
<html:hidden property="menuCode[23].menuCd" styleId="menuCode23" />
<html:hidden property="menuCode[24].menuCd" styleId="menuCode24" />
<html:hidden property="menuCode[25].menuCd" styleId="menuCode25" />
<html:hidden property="menuCode[26].menuCd" styleId="menuCode26" />
<html:hidden property="menuCode[27].menuCd" styleId="menuCode27" />
<html:hidden property="menuCode[28].menuCd" styleId="menuCode28" />
<html:hidden property="menuCode[29].menuCd" styleId="menuCode29" />
<html:hidden property="menuCode[30].menuCd" styleId="menuCode30" />
<html:hidden property="menuCode[31].menuCd" styleId="menuCode31" />
<%-- [IN063] Add new menu --%>
<html:hidden property="menuCode[32].menuCd" styleId="menuCode32" />
<html:hidden property="menuCode[33].menuCd" styleId="menuCode33" />

<table background="css/images/bgHeader3.jpg" width="1000px" style="margin-left:2px" border="0" cellspacing="0">
    <tr>
        <td>
            <img src="css/images/denso.png" width="143" height="20" />
        </td>
        <td class="align-right" style="font-weight:bold;">
            <sps:label name="SUPPLIER_PORTAL" />
            &nbsp;&nbsp;&nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="2" >
            <ul id="nav" class="dropdown dropdown-horizontal">
                <li id="n-sps">
                    <div id="${mainMenuCons.MENU_ID_HM}" style="display:none">
                        <sps:label name="HOME" />
                        <img src="images/icons/arrow4.png" align="middle"/>
                    </div>
                        <ul>
                            <li id="${mainMenuCons.MENU_ID_HM_MN}" class="first" style="display:none">
                                <a href="./MainScreenAction.do?method=doInitial" class="dir">
                                    <sps:label name="MAIN_SCREEN" />
                                </a>
                            </li>
                        </ul>
                </li>
                <li id="n-sps">
                    <div id="${mainMenuCons.MENU_ID_OR}"  style="display:none">
                        <sps:label name="ORDERING" />
                        <img src="images/icons/arrow4.png" align="middle"/>
                    </div>
                        <ul>
                            <li id="${mainMenuCons.MENU_ID_OR_SU}" class="first" style="display:none">
                                <a href="./SupplierInformationUploadingAction.do?method=doInitial" class="dir">
                                    <sps:label name="SUPPLIER_INFO_UPLOADING" />
                                </a>
                            </li>
                            <li id="${mainMenuCons.MENU_ID_OR_PO}" class="first" style="display:none">
                                <a href="./PurchaseOrderInformationAction.do?method=doInitial" class="dir">
                                    <sps:label name="PURCHASE_ORDER_INFO" />
                                </a>
                            </li>
                            <li id="${mainMenuCons.MENU_ID_OR_DO}" class="first" style="display:none">
                                <a href="./DeliveryOrderInformationAction.do?method=doInitial" class="dir">
                                    <sps:label name="DELIVERY_ORDER_INFO" />
                                </a>
                            </li>
                            <li id="${mainMenuCons.MENU_ID_OR_KO}" class="first" style="display:none">
                                <a href="./KanbanOrderInformationAction.do?method=doInitial" class="dir">
                                    <sps:label name="KANBAN_ORDER_INFO" />
                                </a>
                            </li>
                        </ul>
                </li>
                <li id="n-sps">
                    <div id="${mainMenuCons.MENU_ID_SH}"  style="display:none">
                        <sps:label name="SHIPPING" />
                        <img src="images/icons/arrow4.png" align="middle"/>
                    </div>
                        <ul>
                            <li id="${mainMenuCons.MENU_ID_SH_AC}" class="first" style="display:none">
                                <a href="./AcknowledgedDoInformationAction.do?method=doInitial" class="dir">
                                    <sps:label name="CREATE_ASN_BY_MANUAL" />
                                </a>
                            </li>
                            <li id="${mainMenuCons.MENU_ID_SH_AU}" class="first" style="display:none">
                                <a href="./AsnUploadingAction.do?method=doInitial" class="dir">
                                    <sps:label name="ASN_UPLOADING" />
                                </a>
                            </li>
                            <li id="${mainMenuCons.MENU_ID_SH_AD}" class="first" style="display:none">
                                <a href="./AsnDetailInformationAction.do?method=doInitial" class="dir">
                                    <sps:label name="ASN_DETAIL_INFO" />
                                </a>
                            </li>
                            <li id="${mainMenuCons.MENU_ID_SH_AP}" class="first" style="display:none">
                                <a href="./AsnDetailProgressAction.do?method=doInitial" class="dir">
                                    <sps:label name="ASN_PROGRESS_INFO" />
                                </a>
                            </li>
                             <li id="${mainMenuCons.MENU_ID_SH_BO}" class="first" style="display:none">
                                <a href="./BackOrderInformationAction.do?method=doInitial" class="dir">
                                    <sps:label name="BACK_ORDER_INFO" />
                                </a>
                            </li>
                        </ul>
                </li>
                <li id="n-sps">
                    <div id="${mainMenuCons.MENU_ID_IV}" style="display:none">
                        <sps:label name="INVOICE" />
                        <img src="images/icons/arrow4.png" align="middle"/>
                    </div>
                        <ul>
                            <li id="${mainMenuCons.MENU_ID_IV_AS}" style="display:none">
                                <a href="./AsnInformationAction.do?method=doInitial" class="dir">
                                    <sps:label name="CREATE_INV_BY_MANUAL" />
                                </a>
                            </li>
                            <li id="${mainMenuCons.MENU_ID_IV_IU}" style="display:none">
                                <a href="./InvoiceInformationUploadingAction.do?method=doInitial" class="dir">
                                    <sps:label name="INVOICE_INFO_UPLOADING" />
                                </a>
                            </li>
                            <li id="${mainMenuCons.MENU_ID_IV_II}" style="display:none">
                                <a href="./InvoiceInformationAction.do?method=doInitial" class="dir">
                                    <sps:label name="INVOICE_INFO" />
                                </a>
                            </li>
                            <li id="${mainMenuCons.MENU_ID_IV_DP}" style="display:none">
                                <a href="./PriceDifferenceInformationAction.do?method=doInitial" class="dir">
                                    <sps:label name="PRICE_DIFFERENCE_INFO" />
                                </a>
                            </li>
                            <li id="${mainMenuCons.MENU_ID_IV_UF}" style="display:none">
                                <a href="./CnDnUploadingAction.do?method=doInitial" class="dir">
                                    <sps:label name="CNDN_UPLOADING" />
                                </a>
                            </li>
                            <li id="${mainMenuCons.MENU_ID_IV_DF}" style="display:none">
                                <a href="./CnDnDownloadingAction.do?method=doInitial" class="dir">
                                    <sps:label name="CNDN_DOWNLOADING" />
                                </a>
                            </li>
                        </ul>
                </li>
                <li id="n-sps">
                    <div id="${mainMenuCons.MENU_ID_AD}" style="display:none">
                        <sps:label name="ADMIN_CONFIG" />
                        <img src="images/icons/arrow4.png" align="middle"/>
                    </div>
                        <ul>
                            <li id="${mainMenuCons.MENU_ID_AD_SU}" style="display:none">
                                <a href="#" class="dir">
                                    <sps:label name="SUPPLIER_USER_INFO" />
                                    <img src="images/icons/arrow-right.png" align="right"/>
                                </a>
                                <ul>
                                    <li id="${mainMenuCons.MENU_ID_AD_SU_SI}" style="display:none">
                                        <a href="./SupplierUserInformationAction.do?method=doInitial">
                                            <sps:label name="SUPPLIER_USER_INFO" />
                                        </a>
                                    </li>
                                    <li id="${mainMenuCons.MENU_ID_AD_SU_SR}" style="display:none">
                                        <a href="./SupplierUserRegistrationAction.do?method=doInitial&mode=Register">
                                            <sps:label name="SUPPLIER_USER_REGISTRATION" />
                                        </a>
                                    </li>
                                    <li id="${mainMenuCons.MENU_ID_AD_SU_SU}" style="display:none">
                                        <a href="./SupplierUserUploadingAction.do?method=doInitial">
                                            <sps:label name="SUPPLIER_USER_UPLOADING" />
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li id="${mainMenuCons.MENU_ID_AD_DU}" style="display:none">
                                <a href="#" class="dir">
                                    <sps:label name="DENSO_USER_INFO" />
                                    <img src="images/icons/arrow-right.png" align="right"/>
                                </a>
                                <ul>
                                    <li id="${mainMenuCons.MENU_ID_AD_DU_DI}" class="first" style="display:none">
                                        <a href="./DensoUserInformationAction.do?method=doInitial">
                                            <sps:label name="DENSO_USER_INFO" />
                                        </a>
                                    </li>
                                    <li id="${mainMenuCons.MENU_ID_AD_DU_DR}" class="first" style="display:none">
                                        <a href="./DensoUserRegistrationAction.do?method=doInitial&mode=Register">
                                            <sps:label name="DENSO_USER_REGISTRATION" />
                                        </a>
                                    </li>
                                    <li id="${mainMenuCons.MENU_ID_AD_DU_DU}" class="first" style="display:none">
                                        <a href="./DensoUserUploadingAction.do?method=doInitial">
                                            <sps:label name="DENSO_USER_UPLOADING" />
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            
                            <%-- [IN063] add new menu --%>
                            <li id="${mainMenuCons.MENU_ID_AD_DG}" style="display:none">
                                <a href="#" class="dir">
                                    <sps:label name="DIGITAL_SIGNATURE" />
                                    <img src="images/icons/arrow-right.png" align="right"/>
                                </a>
                                <ul>
                                    <li id="${mainMenuCons.MENU_ID_AD_DG_DU}" class="first" style="display:none">
                                        <a href="./DigitalSignatureUploadingAction.do?method=doInitial">
                                            <sps:label name="DIGITAL_SIGNATURE_UPLOADING" />
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            
                        </ul>
                </li>
                <li id="n-sps">
                    <div id="${mainMenuCons.MENU_ID_CT}" style="display:none" onclick="wcom003OpenContactUs();">
                        <sps:label name="CONTACT_US" />
                    </div>
                </li>
                <li id="n-sps">
                    <div id="${mainMenuCons.MENU_ID_HP}" style="display:none" onclick="wcom003OpenHelp();">
                        <sps:label name="HELP" />
                    </div>
                </li>
                <li id="n-sps" style="width:132px">
                    <div id="${mainMenuCons.MENU_ID_LO}" onclick="wcom003DoLogout();">
                            <sps:label name="LOGOUT" />
                    </div>
                </li>
            </ul>
        </td>
    </tr>
</table>
<script type="text/javascript">
    function wcom003DoLogout(){
        window.open('./MainScreenAction.do?method=doLogout','_self');
    }
    function wcom003OpenContactUs(){
        window.open('./MainScreenAction.do?method=doInitialContactUs','_self');
    }
    function wcom003OpenHelp(){
        window.open('./MainScreenAction.do?method=doInitialHelp','_self');
    }
</script>