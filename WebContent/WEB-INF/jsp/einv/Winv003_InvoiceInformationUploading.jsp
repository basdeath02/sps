<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Winv003_InvoiceInformationUploading.js">
        </script>
        <title>
            <sps:label name="INVOICE_INFORMATION_UPLOADING" />
        </title>
        <script type="text/javascript">
            var labelAll = '<sps:label name="ALL_CRITERIA" />';
            var labelPleaseSelect = '<sps:label name="PLEASE_SELECT_CRITERIA" />';
        </script>
    </head>
    
    <body class="hideHorizontal">
        <html:form styleId="Winv003Form" enctype="multipart/form-data" 
            action="/InvoiceInformationUploadingAction.do">
            
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            
            <html:hidden styleId="method" property="method" />
            <html:hidden styleId="cannotDownloadMessage" property="cannotDownloadMessage"/>
            <html:hidden styleId="cannotResetMessage" property="cannotResetMessage"/>
            <table style="height:30px; border:0; cellpadding:0; cellspacing:0; width:990px;" 
                class="bottom">
                <tr>
                    <td>
                        <sps:label name="INVOICE" />
                        &gt; 
                        <sps:label name="INVOICE_INFORMATION_UPLOADING" />
                    </td>
                    <td colspan="2" class="align-right">
                        <sps:label name="USER" />
                        &nbsp;:
                        <bean:write name="Winv003Form" property="userLogin" />
                    </td>
                </tr>
            </table>
            
            <c:choose>
                <c:when test="${not empty Winv003Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width: 992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg"
                                items="${Winv003Form.applicationMessageList}"
                                begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message} 
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            
            <fieldset class="fixwidth">
                <c:set var="lbPleaseSelect">
                    <sps:label name="PLEASE_SELECT_CRITERIA" />
                </c:set>
                <c:set var="lbAll">
                    <sps:label name="ALL_CRITERIA" />
                </c:set>
                <table style="width:975px;margin-top:10px;border:0; cellpadding:3; cellspacing:1;" 
                    class="display_criteria" >
                    <c:set var="labelBrowse">
                        <sps:label name="BROWSE"/>
                    </c:set>
                    <c:set var="labelUpload">
                        <sps:label name="UPLOAD"/>
                    </c:set>
                    <c:set var="labelReset">
                        <sps:label name="RESET"/>
                    </c:set>
                    <tr>
                        <th width="150px" class="align-right">
                            <sps:label name="FILE_NAME" />
                            :
                        </th>
                        <td class="align-center" width="530px">
                            <html:text name="Winv003Form" property="fileName" 
                                styleId="txtUpload" styleClass="txtUpload2" readonly="true"/>
                        </td>
                        <td>
                        <c:choose>
                            <c:when test="${0 != Winv003Form.max}">
                                <c:set var="browseDisplayFlag">
                                    none
                                </c:set>
                                <c:set var="displayFlag">
                                    disabled
                                </c:set>
                            </c:when>
                        </c:choose>
                        <div class="align-left" style="position: absolute;filter: alpha(opacity=0);
                        opacity:0;"
                             display: ${browseDisplayFlag};">
                                &nbsp;
                            <html:file property="fileData" styleId="fileData" 
                                maxlength="300" onchange="setPathFile();" style="width: 90px; height: 25px; margin-left: -7px;" />
                        </div>
                        <input type="button" name="Input" id="btnBrowse" value="${labelBrowse}"
                            class="select_button" ${displayFlag} />
                        <input type="button" name="Input" id="btnUpload" value="${labelUpload}" 
                            class="select_button" ${displayFlag} />
                        <input type="button" name="Input" id="btnReset" value="${labelReset}" 
                            class="select_button" />
                        </td>
                     </tr>
                </table>
                
                <c:choose>
                    <c:when test="${0 == Winv003Form.max}">
                        <c:set var="displayFlag">
                            none
                        </c:set>
                    </c:when>
                </c:choose>
            
                <div id="uploadResult" style="display: ${displayFlag};">
                    <table style="width:975px; border:0; cellpadding:3; cellspacing:1;" 
                        class="display_criteria">
                        <tr>
                            <td colspan="9" class="align-left">
                                <b>
                                    <span style="text-decoration:underline;">
                                        <sps:label name="UPLOAD_RESULT" />
                                    </span>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <th width="12%" class="align-right">
                                <sps:label name="TOTAL_RECORD" />
                                &nbsp;:
                            </th>
                            <td width="3%" class="align-right">
                                ${Winv003Form.totalRecord}
                                &nbsp;
                            </td>
                            <td width="10%" class="align-left">
                                <sps:label name="RECORD" />
                            </td>
                            <th class="align-right" width="10%">
                                <sps:label name="CORRECT" />
                                &nbsp;:
                            </th>
                            <td class="align-right" width="3%">
                                ${Winv003Form.correctRecord}
                                &nbsp;
                            </td>
                            <td width="8%" class="align-left">
                                <sps:label name="RECORD" />
                            </td>
                            <th class="align-right" width="10%">
                                <sps:label name="WARNING" />
                                &nbsp;:
                            </th>
                            <td class="align-right" width="3%">
                                ${Winv003Form.warningRecord}
                                &nbsp;
                            </td>
                            <td width="7%" class="align-left">
                               <sps:label name="RECORD" />
                            </td>
                            <th class="align-right" width="10%">
                                <sps:label name="ERROR" />
                                &nbsp;:
                            </th>
                            <td class="align-right" width="3%">
                                ${Winv003Form.errorRecord}
                                &nbsp;
                            </td>
                            <td width="7%" class="align-left">
                                <sps:label name="RECORD" />
                            </td>
                            <th width="10%" class="align-right">
                                <sps:label name="S_CD" />
                                &nbsp;:
                            </th>
                            <td width="8%" class="align-left">
                                <input type="text" value="${Winv003Form.vendorCd}" 
                                    style="width:100px" readonly />
                            </td>
                        </tr>
                        <tr>
                            <th class="align-right">
                                <sps:label name="INVOICE_NO" />
                                &nbsp;:
                            </th>
                            <td width="13%" class="align-left" colspan="5">
                                 <input type="text" value="${Winv003Form.invoiceNo}" 
                                     style="width:200px" readonly />
                            </td>
                           <th class="align-right">
                                <sps:label name="S_TAX_ID" />
                                &nbsp;:
                            </th>
                            <td colspan="2">
                                <input type="text" value="${Winv003Form.supplierTaxId}" 
                                    style="width:100px" readonly />
                            </td>
                            <th class="align-right">
                                <sps:label name="D_CD" />
                                &nbsp;:
                            </th>
                            <td colspan="2">
                                 <input type="text" value="${Winv003Form.DCd}" 
                                     style="width:100px" readonly />
                            </td>
                            <th class="align-right">
                                <sps:label name="D_P" />
                                &nbsp;:
                            </th>
                            <td>
                                 <input type="text" value="${Winv003Form.DPcd}" 
                                     style="width:100px" readonly />
                            </td>
                        </tr>
                    </table>
                </div>
            </fieldset>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        
        <html:form styleId="Winv003FormResult" action="/InvoiceInformationUploadingAction.do">
        
            <html:hidden property="method" styleId="methodResult"/>
            <html:hidden property="DCd"/>
            <html:hidden property="fileName"/>
            <html:hidden property="cannotDownloadMessage"/>
            <html:hidden property="cannotResetMessage"/>
            <div id="result" style="display: ${displayFlag};">
                <div style="width: 994px;">
                    <table class="display_data" id="tblBody" 
                        style="table-layout:fixed;width:974px;word-wrap:break-word;">
                        <tr>
                            <th width="40px" scope="col" >
                                <sps:label name="UPLOAD_RESULT" />
                            </th>
                            <th width="25px" scope="col" >
                                <sps:label name="CSV_LINE_NO" />
                            </th>
                            <th width="147px" scope="col">
                                <sps:label name="ERROR_CODE" />
                            </th>
                            <th width="105px" scope="col">
                                <sps:label name="ASN_NO" />
                            </th>
                            <th width="105px" scope="col">
                                <sps:label name="D_PART_NO" />
                            </th>
                            <th width="125px" scope="col">
                                <sps:label name="S_PART_NO" />
                            </th>
                            
                            <%-- [IN036] Add SPS_DO_NO to show in screen --%>
                            <th style="width:70px;" scope="col">
                                <sps:label name="D/O_NO" />
                            </th>
                            
                            <th width="72px" scope="col" >
                                <sps:label name="SHIPPING_QTY" />
                            </th>
                            <th width="23px" scope="col" >
                                <sps:label name="UM" />
                            </th>
                            <th width="85px" scope="col" >
                                <sps:label name="ACTUAL_ETD" />
                            </th>
                            <th width="85px" scope="col" >
                                <sps:label name="PLAN_ETA" />
                            </th>
                        </tr>
                    </table>
                </div>
                <div style="width:994px;height:270px;overflow-y:auto">
                    <table class="display_data" id="tblBody"
                        style="table-layout:fixed;width:974px;word-wrap:break-word;border-width:0px">
                        <c:forEach var="invoiceInformationUploading" 
                            items="${Winv003Form.tmpUploadInvoiceResultList}" begin="0" 
                            step="1" varStatus="status">
                            <tr>
                                <td class="align-left" width="40px">
                                    <c:choose>
                                        <c:when test="${'0' == 
                                            invoiceInformationUploading.spsTmpUploadInvoice.uploadResult}">
                                            <sps:label name="OK" />
                                        </c:when>
                                        <c:otherwise>
                                            <span class="ui-messageError2">
                                                <sps:label name="NG" />
                                            </span>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <td class="align-right" width="25px">
                                    ${invoiceInformationUploading.spsTmpUploadInvoice.csvLineNo}
                                </td>
                                <td class="align-left" width="147px">
                                    ${invoiceInformationUploading.spsTmpUploadInvoice.errorCode}
                                </td>
                                <td class="align-left" width="105px">
                                    ${invoiceInformationUploading.spsTmpUploadInvoice.asnNo}
                                </td>
                                <td class="align-right" width="105px">
                                    ${invoiceInformationUploading.spsTmpUploadInvoice.DPn}
                                </td>
                                <td class="align-right" width="125px">
                                    ${invoiceInformationUploading.spsTmpUploadInvoice.SPn}
                                </td>
                                
                                <%-- [IN036] Add SPS_DO_NO to show in screen --%>
                                <td class="align-left" width="70px">
                                    ${invoiceInformationUploading.spsTmpUploadInvoice.spsDoNo}
                                </td>
                                
                                <td class="align-right" width="72px">
                                    ${invoiceInformationUploading.shippingQty}
                                </td>
                                <td class="align-left" width="23px">
                                    ${invoiceInformationUploading.spsTmpUploadInvoice.SUnitOfMeasure}
                                </td>
                                <td class="align-left"  width="85px">
                                    ${invoiceInformationUploading.actualEtd}
                                </td>
                                <td class="align-left" width="85px">
                                    ${invoiceInformationUploading.planEta}
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <table style="width:994px;border-width:0px">
                    <c:set var="labelDownload">
                        <sps:label name="DOWNLOAD"/>
                    </c:set>
                    <c:set var="labelGroupInvoice">
                        <sps:label name="GROUP_INVOICE"/>
                    </c:set>
                    <c:if test="${'0' != Winv003Form.errorRecord}">
                        <c:set var="groupDisplayFlag">
                            disabled
                        </c:set>
                    </c:if>
                    <tr>
                        <td align="left">
                            <ai:paging action="/SPS/InvoiceInformationUploadingAction.do" 
                                formId="Winv003FormResult" jump="true" />
                        </td>
                        <td align="right">
                            <input type="button" name="Input" id="btnDownload" 
                                value="${labelDownload}" class="select_button" />
                            <input type="button" name="Input" id="btnGroup" 
                                value="${labelGroupInvoice}" class="select_button" 
                                ${groupDisplayFlag} />
                        </td>
                    </tr>
                </table>
            </div>
        </html:form>
        <html:form styleId="Winv003FormDownload" action="/InvoiceInformationUploadingAction.do" >
            <html:hidden property="method" styleId="methodDownload"/>
            <html:hidden styleId="cannotDownloadMessage3" property="cannotDownloadMessage"/>
            <html:hidden styleId="cannotResetMessage3" property="cannotResetMessage"/>
        </html:form>
    </body>
</html>
