<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Winv002_AsnInformation.js">
        </script>
        <title>
            <sps:label name="CREATE_INV_BY_MANUAL" />
        </title>
        <script type="text/javascript">
            var labelAll = '<sps:label name="ALL_CRITERIA" />';
            var labelPleaseSelect = '<sps:label name="PLEASE_SELECT_CRITERIA" />';
        </script>
    </head>
    
    <body class="hideHorizontal">
        <html:form styleId="Winv002FormCriteria" action="/AsnInformationAction.do">
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden property="method" styleId="method"/>
            <html:hidden property="mode" styleId="mode"/>
            <html:hidden property="countGroupAsn"/>
            
            <c:forEach var="supplierAuthen" items="${Winv002Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
            </c:forEach>
            <c:forEach var="densoAuthen" items="${Winv002Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
            </c:forEach>
            
            <html:hidden property="cannotResetMessage" styleId="cannotResetMessage"/>
            <html:hidden property="cannotGroupMessage" styleId="cannotGroupMessage"/>
            <html:hidden property="asnSelected" styleId="asnSelected"/>
            
            <!-- Key for paging. -->
            <html:hidden styleId="currentPage" property="currentPage"/>
            <html:hidden styleId="row" property="row"/>
            <html:hidden styleId="totalCount" property="totalCount"/>
            
            <logic:notEmpty name="Winv002Form" property="companyDensoList">
                <c:forEach var="companyDensoCode" items="${Winv002Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoCode" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            
            <logic:notEmpty name="Winv002Form" property="companySupplierList">
                <c:forEach var="companySupplierCode" items="${Winv002Form.companySupplierList}" begin="0" step="1" varStatus="status">
                   <html:hidden name="companySupplierCode" property="vendorCd" indexed="true"/>
               </c:forEach>
            </logic:notEmpty>
            
            <table id="ui-displayHeader" class="bottom">
                <tr>
                    <td>
                        <sps:label name="INVOICE" />
                        &gt; 
                        <sps:label name="CREATE_INV_BY_MANUAL" />
                    </td>
                    <td colspan="2" class="align-right">
                        <sps:label name="USER" />
                        &nbsp;:
                        <bean:write name="Winv002Form" property="userLogin" />
                    </td>
                </tr>
            </table>
            
            <c:choose>
                <c:when test="${not empty Winv002Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width: 992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg" items="${Winv002Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message} 
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            
            <fieldset class="fixwidth">
                <c:set var="lbPleaseSelect">
                    <sps:label name="PLEASE_SELECT_CRITERIA" />
                </c:set>
                <c:set var="lbAll">
                    <sps:label name="ALL_CRITERIA" />
                </c:set>
                
                <table class="display_criteria" id="ui-displayCriteria" >
                    <col style="width:45px;"/>
                    <col style="width:117px;"/>
                    <col style="width:57px;"/>
                    <col style="width:116px;"/>
                    <col style="width:57px;"/>
                    <col style="width:120px;"/>
                    <col style="width:57px;"/>
                    <col style="width:118px;"/>
                    <tr>
                        <th>
                            <sps:label name="S_CD" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Winv002Form" property="vendorCd" styleId="SCd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">${lbAll}</html:option>
                                <c:forEach var="companySupplierCode" items="${Winv002Form.companySupplierList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${companySupplierCode.vendorCd}">
                                        ${companySupplierCode.vendorCd}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="S_P" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Winv002Form" property="SPcd" styleId="SPcd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">${lbAll}</html:option>
                                <c:forEach var="plantSupplier" items="${Winv002Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${plantSupplier.SPcd}">
                                        ${plantSupplier.SPcd}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="D_CD" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Winv002Form" property="DCd" styleId="DCd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">${lbAll}</html:option>
                                <c:forEach var="companyDensoCode" items="${Winv002Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${companyDensoCode.DCd}">
                                        ${companyDensoCode.DCd}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="D_P" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Winv002Form" property="DPcd" styleId="DPcd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">${lbAll}</html:option>
                                <c:forEach var="plantDenso" items="${Winv002Form.plantDensoList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${plantDenso.DPcd}">
                                        ${plantDenso.DPcd}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                    </tr>
                </table>
                
                <table class="display_criteria" id="ui-displayCriteria" >
                    <col style="width:70px;"/>
                    <col style="width:238px;"/>
                    <col style="width:87px;"/>
                    <col style="width:235px;"/>
                    <tr>
                        <th>
                            <sps:label name="PLAN_ETA" />
                            &nbsp;:
                        </th>
                        <td>
                            <sps:calendar name="planEtaFrom" id="planEtaFromCriteria" value="${Winv002Form.planEtaFrom}"/>
                            -
                            <sps:calendar name="planEtaTo" id="planEtaToCriteria" value="${Winv002Form.planEtaTo}"/>
                        </td>
	                    <th class="align-right">
	                        <sps:label name="PLAN_ETA_TIME" />
	                        :
	                    </th>
	                    <td>
	                        <html:text name="Winv002Form" property="planEtaTimeFrom" style="width:50px"
	                            onfocus="removeColonFromTime('planEtaTimeFrom');return false;"
	                            onblur="addColonToTime('planEtaTimeFrom');return false;" styleId="planEtaTimeFrom"/>
	                            -
	                        <html:text name="Winv002Form" property="planEtaTimeTo" style="width:50px"
	                            onfocus="removeColonFromTime('planEtaTimeTo');return false;"
	                            onblur="addColonToTime('planEtaTimeTo');return false;" styleId="planEtaTimeTo"/>
	                    </td>
                    </tr>
                </table>
                
                <table class="display_criteria" id="ui-displayCriteria">
                    <col style="width:90px;"/>
                    <col style="width:120px;"/>
                    <col style="width:70px;"/>
                    <col style="width:125px;"/>
                    <col style="width:80px;"/>
                    <col style="width:240px;"/>
                    <col style="width:115px;"/>
                    <col style="width:130px;"/>
                    <tr>
                        <th>
                            <sps:label name="ASN_STATUS" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Winv002Form" property="asnStatus" styleId="asnStatus" style="width:110px">
                                <html:option value="ALL">${lbAll}</html:option>
                                <c:forEach var="asnStatusListForEach" items="${Winv002Form.asnStatusList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${asnStatusListForEach.miscCode}">
                                        ${asnStatusListForEach.miscCode} : ${asnStatusListForEach.miscValue}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="ASN_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Winv002Form" property="asnNo" style="width:120px" />
                        </td>
                        <th>
                            <sps:label name="ACTUAL_ETD" />
                            &nbsp;:
                        </th>
                        <td>
                            <sps:calendar name="actualEtdFrom" id="actualEtdFromCriteria" value="${Winv002From.actualEtdFrom}"/>
                            - 
                            <sps:calendar name="actualEtdTo" id="actualEtdToCriteria" value="${Winv002From.actualEtdTo}"/>
                        </td>
                        <th>
                            <sps:label name="SUPPLIER_TAX_ID" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Winv002Form" property="supplierPlantTaxId" style="width:120px" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <table style="width:994px">
                <c:set var="labelSearch">
                    <sps:label name="SEARCH"/>
                </c:set>
                <c:set var="labelReset">
                    <sps:label name="RESET"/>
                </c:set>
                <tr>
                    <td class="align-left" style="vertical-align:bottom;font-size:10px;">
                        <c:if test="${not empty Winv002Form.asnInformationForDisplayList}">
                            <sps:label name="SYSTEM_GET_SELECTED_REC_EVERY_PAGE"/>
                        </c:if>
                    </td>
                    <td class="align-right">
                        <input type="button" name="Input" id="btnSearch" value="${labelSearch}" class="select_button" />
                         <input type="button" name="Input" id="btnReset" value="${labelReset}" class="select_button" />
                    </td>
                </tr>
            </table>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        <html:form styleId="Winv002FormResult" action="/AsnInformationAction.do">
            <html:hidden property="method" styleId="methodResult" value="Search"/>
            <html:hidden property="vendorCd" styleId="SCd"/>
            <html:hidden property="SPcd"/>
            <html:hidden property="DCd" styleId="DCd"/>
            <html:hidden property="DPcd"/>
            <html:hidden property="planEtaFrom" styleId="planEtaFrom"/>
            <html:hidden property="planEtaTo" styleId="planEtaTo"/>
            <html:hidden property="planEtaTimeFrom"/>
            <html:hidden property="planEtaTimeTo"/>
            <html:hidden property="asnStatus"/>
            <html:hidden property="asnNo"/>
            <html:hidden property="actualEtdFrom" styleId="actualEtdFrom"/>
            <html:hidden property="actualEtdTo" styleId="actualEtdTo"/>
            <html:hidden property="supplierPlantTaxId"/>
            <html:hidden property="countGroupAsn" styleId="countGroupAsn"/>
            
            <c:forEach var="supplierAuthen" items="${Winv002Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
            </c:forEach>
            <c:forEach var="densoAuthen" items="${Winv002Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
            </c:forEach>
            
            <html:hidden property="cannotResetMessage"/>
            <html:hidden property="cannotGroupMessage"/>
            
            <logic:notEmpty name="Winv002Form" property="companySupplierList">
                <c:forEach var="companySupplierCode" items="${Winv002Form.companySupplierList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplierCode" property="vendorCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Winv002Form" property="companyDensoList">
                <c:forEach var="companyDensoCode" items="${Winv002Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoCode" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            
            <c:choose>
                <c:when test="${0 == Winv002Form.max}">
                    <c:set var="visibilityFlag">
                        hidden
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="visibilityFlag">
                        visible
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="result" style="visibility: ${visibilityFlag};">
                <div style="width:994px;">
                    <table class="display_data" id="tblBody" style="table-layout:fixed;width:974px;word-wrap:break-word;">
                        <col style="width:20px;"/>
                        <col style="width:20px;"/>
                        <col style="width:50px;"/>
                        <col style="width:100px;"/>
                        <col style="width:35px;"/>
                        <col style="width:30px;"/>
                        <col style="width:30px;"/>
                        <col style="width:150px;"/>
                        <col style="width:30px;"/>
                        <col style="width:30px;"/>
                        <col style="width:65px;"/>
                        <col style="width:65px;"/>
                        <tr>
                            <th>
                                <sps:label name="ALL" />
                                <br />
                                <input type="checkbox" name="checkboxAll" id="checkboxAll" value="checkbox"
                                    onclick="winv002AllChecked(this);"/>
                            </th>
                            <th scope="col" >
                                <sps:label name="NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ASN_STATUS" />
                            </th>
                            <th scope="col">
                                <sps:label name="ASN_NO" />
                            </th>
                            <th scope="col">
                                <sps:label name="TRIP_NO" />
                            </th>
                            <th scope="col">
                                <sps:label name="S_CD" />
                            </th>
                            <th scope="col" >
                                <sps:label name="S_P" />
                            </th>
                            <th scope="col" >
                                <sps:label name="SUPPLIER_TAX_ID" />
                            </th>
                            <th scope="col" >
                                <sps:label name="D_CD" />
                            </th>
                            <th scope="col" >
                                <sps:label name="D_P" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ACTUAL_ETD" />
                            </th>
                            <th scope="col" >
                                <sps:label name="PLAN_ETA" />
                            </th>
                        </tr>
                    </table>
                </div>
                <div style="width:990px;height:290px;overflow-y:auto">
                    <table class="display_data" id="tblBody"
                        style="table-layout: fixed;width: 974px;word-wrap: break-word;border-width: 0px">
                        <col style="width:20px;"/>
                         <col style="width:20px;"/>
                         <col style="width:50px;"/>
                         <col style="width:100px;"/>
                         <col style="width:35px;"/>
                         <col style="width:30px;"/>
                         <col style="width:30px;"/>
                         <col style="width:150px;"/>
                         <col style="width:30px;"/>
                         <col style="width:30px;"/>
                         <col style="width:65px;"/>
                         <col style="width:65px;"/>
                         
                         <c:set var="startRow">
                            ${(Winv002Form.pageNo * Winv002Form.count) - Winv002Form.count}
                        </c:set>
                        <c:forEach var="asnInformation" items="${Winv002Form.asnInformationForDisplayList}" begin="0" step="1" varStatus="status">
                            <tr class="${(status.index + 1) % 2 == 0 ? 'blueLight' : ''}">
                                <td class="align-center">
                                    <input type="checkbox" name="doChkbox" id="checkASN${status.index}" 
                                        onclick="winv002SetSelected('${status.index}')" 
                                        <c:if test="${not empty asnInformation.asnSelected}">
                                            checked
                                        </c:if>
                                    />
                                    <html:hidden name="asnInformation" property="asnSelected" indexed="true"
                                        styleId="asnSelected${status.index}" value="${asnInformation.asnSelected}"/>
                                    <html:hidden name="asnInformation" property="spsTAsnDomain.asnNo" indexed="true"/>
                                    <html:hidden name="asnInformation" property="spsTAsnDomain.vendorCd" indexed="true"/>
                                    <html:hidden name="asnInformation" property="spsTAsnDomain.SPcd" indexed="true"/>
                                    <html:hidden name="asnInformation" property="spsTAsnDomain.DCd" indexed="true"/>
                                    <html:hidden name="asnInformation" property="spsTAsnDomain.DPcd" indexed="true"/>
                                    <html:hidden name="asnInformation" property="STaxId" indexed="true"/>
                                    <html:hidden name="asnInformation" property="spsTAsnDomain.planEta" indexed="true"/>
                                </td>
                                <td class="align-right">
                                    ${startRow + (status.index+1)}
                                </td>
                                <td class="align-left">
                                    ${asnInformation.spsTAsnDomain.asnStatus}
                                </td>
                                <td class="align-left">
                                    ${asnInformation.spsTAsnDomain.asnNo}
                                </td>
                                <td class="align-left">
                                    ${asnInformation.spsTAsnDomain.tripNo}
                                </td>
                                <td class="align-left">
                                    ${asnInformation.spsTAsnDomain.vendorCd}
                                </td>
                                <td class="align-left">
                                    ${asnInformation.spsTAsnDomain.SPcd}
                                </td>
                                <td class="align-left">
                                    ${asnInformation.STaxId}
                                </td>
                                <td class="align-left">
                                    ${asnInformation.spsTAsnDomain.DCd}
                                </td>
                                <td class="align-left">
                                    ${asnInformation.spsTAsnDomain.DPcd}
                                </td>
                                <td class="align-left">
                                    ${asnInformation.actualEtd}
                                </td>
                                <td class="align-left">
                                    ${asnInformation.planEta}
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <table style="width:994px;border-width:0px">
                    <c:set var="labelGroupInvoice">
                        <sps:label name="GROUP_INVOICE"/>
                    </c:set>
                    <tr>
                        <td align="left">
                            <ai:paging action="/SPS/AsnInformationAction.do" 
                                formId="Winv002FormResult" jump="true" />
                        </td>
                        <td align="right">
                            <input type="button" name="Input" id="btnGroup" 
                                value="${labelGroupInvoice}" class="select_button" />
                        </td>
                    </tr>
                </table>
                <div id="displayLegend" style="margin:0;padding-top:2px;">
                    <table bgcolor="#EEEEEE" style="width:994px;border:0;">
                        <tr>
                            <td align="left" style="font-size:10px;">
                                <span style="font-weight: bold;">
                                    <sps:label name="LEGEND" />
                                    <span style="text-decoration: underline;">
                                        <sps:label name="ASN_STATUS" />
                                    </span>
                                </span>
                                <sps:label name="ISS_CCL_RCP_RPT" />
                            </td>
                            <td class="align-right">
                                <a href="#" style="font-size:10px;" id="moreLegend">
                                    &gt;&gt; 
                                    <sps:label name="MORE" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </html:form>
        <html:form styleId="Winv002FormDownload" action="/AsnInformationAction.do">
            <html:hidden property="method" styleId="methodDownload"/>
            <html:hidden property="vendorCd" styleId="SCd"/>
            <html:hidden property="DCd" styleId="DCd"/>
            <logic:notEmpty name="Winv002Form" property="companySupplierList">
                <c:forEach var="companySupplierCode" items="${Winv002Form.companySupplierList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplierCode" property="vendorCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Winv002Form" property="companyDensoList">
                <c:forEach var="companyDensoCode" items="${Winv002Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoCode" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
        </html:form>
    </body>
</html>
