<%@ include file="../includes/jsp_header.jspf"%>
<html>
<head>
<%@ include file="../includes/html_header.jspf"%>
<jsp:useBean id="cons" 
    class="com.globaldenso.asia.sps.common.constant.Constants" />
<script type="text/javascript" src="./js/business/Winv006_CnDnUploading.js">
</script>
<script type="text/javascript">
    var labelAll = '<sps:label name="ALL_CRITERIA" />';
</script>
<title>
    <sps:label name="CNDN_UPLOADING" />
</title>
</head>
<body class="hideHorizontal">
    <html:form styleId="Winv006Form" enctype="multipart/form-data"
        action="/CnDnUploadingAction.do">
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden styleId="method" property="method" />
            <html:hidden styleId="cannotResetMessage" property="cannotResetMessage"/>
            <table id="ui-displayHeader" class="bottom">
                <tr>
                    <td>
                        <sps:label name="INVOICE" />
                            &gt;
                        <sps:label 
                            name="CN_DN_UPLOADING" />
                    </td>
                    <td colspan="2" class="align-right">
                        <sps:label name="USER" />
                            &nbsp; : <bean:write name="Winv006Form" property="userLogin" />
                    </td>
                    </td>
                </tr>
            </table>
            <c:choose>
                <c:when test="${not empty Winv006Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width:992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg" items="${Winv006Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message}
                                </span>
                                <br/>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            
            <fieldset class="fixwidth">
                <c:set var="lbUpload">
                    <sps:label name="UPLOAD" />
                </c:set>
                <c:set var="lbBrowse">
                    <sps:label name="BROWSE" />
                </c:set>
                <c:set var="lbReset">
                    <sps:label name="RESET" />
                </c:set>
                <table class="display_criteria" id="ui-displayCriteria" style="padding-left:1px;border-spacing:0;">
                    <tr>
                        <th width="127px;" class="align-right">
                            <sps:label name="S_CD" />
                            &nbsp;:&nbsp;
                        </th>
                        <td width="123px;">
                        <html:select name="Winv006Form" property="vendorCd"
                                    style="width:120px" styleClass="mandatory"
                                    styleId="supplierCompanyCode">
                                <c:forEach var="sCompany"
                                    items="${Winv006Form.SCdList}"
                                    begin="0" step="1" varStatus="status">
                                <html:option value="${sCompany.miscCode}">
                                    ${sCompany.miscValue}
                                </html:option>
                            </c:forEach>
                        </html:select>
                        <c:forEach var="companySupplierItem"
                                    items="${Winv006Form.SCdList}"
                                    begin="0" step="1" varStatus="status">
                                <html:hidden name="companySupplierItem" property="miscCode" indexed="true"/>
                        </c:forEach>
                    </td>
                    <th width="112px;" class="align-right">
                        <sps:label name="S_P" />
                            &nbsp;:&nbsp;
                    </th>
                        <td width="121px;">
                            <html:select name="Winv006Form" property="SPcd"
                                style="width:120px" styleClass="mandatory"
                                styleId="supplierPlantCode">
                                <c:forEach var="sCompany"
                                    items="${Winv006Form.SPcdList}"
                                    begin="0" step="1" varStatus="status">
                                <html:option value="${sCompany.SPcd}">
                                    ${sCompany.SPcd}
                                </html:option>
                                </c:forEach>
                            </html:select>
                            <c:forEach var="plantSupplierItem"
                                    items="${Winv006Form.SPcdList}"
                                    begin="0" step="1" varStatus="status">
                                <html:hidden name="plantSupplierItem" property="SPcd" indexed="true"/>
                            </c:forEach>
                        </td>
                        <th width="122px;" class="align-right">
                            <sps:label name="D_CD" />
                            &nbsp;:
                        </th>
                        <td style="width: 120px;">
                            <html:select styleId="dCd" style="width:120px" styleClass="mandatory" property="DCd">
                                <c:forEach var="dCd"
                                    items="${Winv006Form.companyDensoList}"
                                    begin="0" step="1" varStatus="status">
                                    <html:option value="${dCd.miscCode}">
                                        ${dCd.miscValue}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                            <logic:notEmpty name="Winv006Form" property="companyDensoList">
                                    <c:forEach var="dCdResult" items="${Winv006Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                        <html:hidden name="dCdResult" property="miscCode" indexed="true"/>
                                        <html:hidden name="dCdResult" property="miscValue" indexed="true"/>
                                    </c:forEach>
                            </logic:notEmpty>
                        </td>
                        <th width="123px;" class="align-right">
                            <sps:label name="D_P" />
                            &nbsp;:
                        </th>
                        <td style="width: 120px;">
                            <html:select styleId="dPcd"
                                style="width: 120px" styleClass="mandatory"
                                property="DPcd">
                                <c:forEach var="sCompany"
                                        items="${Winv006Form.DPcdList}"
                                        begin="0" step="1" varStatus="status">
                                    <html:option value="${sCompany.DPcd}">
                                        ${sCompany.DPcd}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                            <c:forEach var="plantDenso"
                                items="${Winv006Form.DPcdList}"
                                begin="0" step="1" varStatus="status">
                                <html:hidden name="plantDenso" property="DPcd" indexed="true"/>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
                <table class="display_criteria" id="ui-displayCriteria">
                    <tr>
                        <th style="width:13%;" class="align-right" >
                            <sps:label name="FILE_NAME" />
                            &nbsp;:
                        </th>
                        <td style="width:31%;" class="align-left">
                            <div style="position: absolute; left: 10; filter: alpha(opacity=0);
                            opacity:0;">
                                <html:file property="fileData" styleId="fileData" style="width:395px;" size="58"
                                    maxlength="300" onchange="Winv006SetPathFile();" />
                            </div>
                             <html:text name="Winv006Form" property="fileName" 
                                styleId="txtUpload" styleClass="txtUpload" readonly="true"/>
                        </td>
                        <td style="width:56.5%;">
                            <input type="button" name="Input" id="btnBrowse" value="${lbBrowse}"
                                class="select_button"/>
                            <input type="button" name="Input" id="btnUpload" value="${lbUpload}"
                                class="select_button" />
                            <input type="button" name="Input" id="btnReset" value="${lbReset}"
                                class="select_button" />
                        </td>
                    </tr>
                    <tr>
                        <th class="align-right" style="vertical-align: top;">
                            <sps:label name="COMMENT" />
                            &nbsp;:
                        </th>
                        <td colspan="2">
                             <html:textarea property="comment" styleId="comment" rows="4" cols="150"
                                 style="width:500px" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </html:form>
    </body>
</html>
