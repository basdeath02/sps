<%@ include file="../includes/jsp_header.jspf"%>
<html>
<head>
<%@ include file="../includes/html_header.jspf"%>
<jsp:useBean id="cons"
    class="com.globaldenso.asia.sps.common.constant.Constants" />
<script type="text/javascript"
    src="./js/business/Winv005_PriceDifferenceInformation.js">
</script>
<title>
    <sps:label name="PRICE_DIFFERENCE_INFO" />
</title>
<script type="text/javascript">
    var labelAll = '<sps:label name="ALL_CRITERIA" />';
</script>
</head>
<body class="hideHorizontal" style="width: 98%;">
    <html:form styleId="Winv005FormCriteria" enctype="multipart/form-data"
        action="/PriceDifferenceInformationAction.do">
        <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
        <html:hidden styleId="method" property="method" />
        
        <c:forEach var="supplierAuthen" items="${Winv005Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
            <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
            <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="densoAuthen" items="${Winv005Form.densoAuthenList}" begin="0" step="1" varStatus="status">
            <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
            <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
        </c:forEach>
        
        <html:hidden styleId="cannotDownloadMessage" property="cannotDownloadMessage"/>
        <html:hidden styleId="cannotResetMessage" property="cannotResetMessage"/>
        
        <table id="ui-displayHeader" class="bottom">
            <tr>
                <td>
                    <sps:label name="INVOICE" />
                        &gt;
                    <sps:label
                        name="PRICE_DIFFERENCE_INFORMATION" />
                </td>
                <td colspan="2" class="align-right">
                    <sps:label name="USER" />
                        &nbsp;
                    : <bean:write name="Winv005Form" property="userLogin" />
                </td>
                </td>
            </tr>
        </table>
        <c:choose>
               <c:when test="${not empty Winv005Form.applicationMessageList}">
                   <c:set var="displayErrorMessage">
                       
                   </c:set>
               </c:when>
               <c:otherwise>
                   <c:set var="displayErrorMessage">
                       display: none;
                   </c:set>
               </c:otherwise>
           </c:choose>
           <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
               <table style="width: 992px" class="tableMessage">
                   <tr>
                       <td id="errorTotal">
                           <c:forEach var="appMsg"
                               items="${Winv005Form.applicationMessageList}"
                               begin="0" step="1" varStatus="status">
                               <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                   <c:set var="messageClass">
                                       ui-messageSuccess
                                   </c:set>
                               </c:if>
                               <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                   <c:set var="messageClass">
                                       ui-messageError
                               </c:set>
                               </c:if>
                               <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                   <c:set var="messageClass">
                                       ui-messageWarning
                               </c:set>
                               </c:if>
                               <span class="${messageClass}">
                                   ${appMsg.message} 
                               </span>
                               <br>
                           </c:forEach>
                       </td>
                   </tr>
               </table>
           </div>
            
        <fieldset class="fixwidth">
            <c:set var="lbPleaseSelect">
                <sps:label name="PLEASE_SELECT_CRITERIA" />
            </c:set>
            <c:set var="lbAll">
                <sps:label name="ALL_CRITERIA" />
            </c:set>
            <table class="display_criteria" id="ui-displayCriteria">
                <col style="width:102px;"/>
                <col style="width:228px;"/>
                <col style="width:90px;"/>
                <col style="width:152px;"/>
                <col style="width:60px;"/>
                <col style="width:140px;"/>
                <col style="width:70px;"/>
                <col style="width:125px;"/>
                <tr>
                    <th class="align-right">
                        <sps:label name="INVOICE_DATE" />
                            &nbsp;:
                    </th>
                    <td colspan="1">
                        <sps:calendar name="invoiceDateFrom" id="invoiceDateFrom"
                            value="${Winv005Form.invoiceDateFrom}" />
                            -
                        <sps:calendar name="invoiceDateTo" id="invoiceDateTo"
                            value="${Winv005Form.invoiceDateTo}" />
                    </td>
                    <th class="align-right">
                        <sps:label name="CN_DATE" />
                        &nbsp;:
                    </th>
                    <td colspan="3">
                        <sps:calendar name="cnDateFrom"
                            id="cnDateFrom" value="${Winv005Form.cnDateFrom}" />
                            -
                        <sps:calendar name="cnDateTo"
                            id="cnDateTo" value="${Winv005Form.cnDateTo}" />
                    </td>
                </tr>
                <tr>
                    <th class="align-right">
                        <sps:label name="S_CD" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:select name="Winv005Form" property="vendorCd" style="width:120px" styleClass="mandatory" styleId="supplierCode">
                            <html:option value="ALL">${lbAll}</html:option>
                            <c:forEach var="companySupplierItem" items="${Winv005Form.SCdList}" begin="0" step="1" varStatus="status">
                                <html:option value="${companySupplierItem.vendorCd}">
                                    ${companySupplierItem.vendorCd}
                                </html:option>
                            </c:forEach>
                        </html:select>
                        <c:forEach var="companySupplierItem" items="${Winv005Form.SCdList}" begin="0" step="1" varStatus="status">
                            <html:hidden name="companySupplierItem" property="vendorCd" indexed="true" />
                            <html:hidden name="companySupplierItem" property="companyName" indexed="true" />
                        </c:forEach>
                    </td>
                    <th class="align-right">
                        <sps:label name="S_P" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:select name="Winv005Form" property="SPcd" style="width:120px" styleClass="mandatory" styleId="supplierPlantCode">
                            <html:option value="ALL">${lbAll}</html:option>
                            <c:forEach var="companySupplierPlant" items="${Winv005Form.SPcdList}" begin="0" step="1" varStatus="status">
                                <html:option value="${companySupplierPlant.miscCd}">
                                    ${companySupplierPlant.miscValue}
                                </html:option>
                            </c:forEach>
                        </html:select>
                    </td>
                    <th class="align-right">
                        <sps:label name="D_CD" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:select name="Winv005Form" property="DCd" style="width:120px" styleClass="mandatory" styleId="densoCode">
                            <html:option value="ALL">${lbAll}</html:option>
                            <c:forEach var="companyDensoItem" items="${Winv005Form.DCdList}" begin="0" step="1" varStatus="status">
                                <html:option value="${companyDensoItem.DCd}">
                                        ${companyDensoItem.DCd}
                                </html:option>
                            </c:forEach>
                        </html:select>
                        <c:forEach var="companyDensoItem" items="${Winv005Form.DCdList}" begin="0" step="1" varStatus="status">
                            <html:hidden name="companyDensoItem" property="DCd" indexed="true" />
                            <html:hidden name="companyDensoItem" property="companyName" indexed="true" />
                        </c:forEach>
                    </td>
                    <th class="align-right">
                        <sps:label name="D_P" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:select name="Winv005Form" property="DPcd" style="width:120px" styleClass="mandatory" styleId="densoPlantCode">
                            <html:option value="ALL">${lbAll}</html:option>
                            <c:forEach var="densoPlantCode" items="${Winv005Form.DPcdList}" begin="0" step="1" varStatus="status">
                                <html:option value="${densoPlantCode.miscCd}">
                                    ${densoPlantCode.miscValue}
                                </html:option>
                            </c:forEach>
                        </html:select>
                    </td>
                </tr>
            </table>
            <table class="display_criteria" id="ui-displayCriteria">
                <col style="width:11%;"/>
                <col style="width:23.78%;"/>
                <col style="width:9%;"/>
                <col style="width:10%;"/>
                <col style="width:9%;"/>
                <col style="width:30%;"/>
                <tr>
                    <th class="align-right">
                        <sps:label name="ASN_NO" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:text name="Winv005Form" property="asnNo" style="width: 118px" />
                    </td>
                    <th class="align-right">
                        <sps:label name="D_PART_NO" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:text name="Winv005Form" property="DPn" style="width: 120px" />
                    </td>
                    <th class="align-right">
                        <sps:label name="S_PART_NO" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:text name="Winv005Form" property="SPn" style="width: 145px" />
                    </td>
                </tr>
                <tr>
                    <th class="align-right" style="width: 120px">
                        <sps:label name="INVOICE_STATUS" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:select name="Winv005Form" property="invoiceStatus" style="width:215px">
                            <html:option value="ALL">${lbAll}</html:option>
                            <c:forEach var="invoiceStatus" items="${Winv005Form.invoiceStatusList}" begin="0" step="1" varStatus="status">
                                <html:option value="${invoiceStatus.miscCode}">
                                    ${invoiceStatus.miscCode} : ${invoiceStatus.miscValue}
                                </html:option>
                            </c:forEach>
                        </html:select>
                    </td>
                    <th class="align-right" width="90px">
                        <sps:label name="INVOICE_NO" />
                        &nbsp;:
                        </th>
                    <td>
                      <html:text name="Winv005Form" property="invoiceNo" style="width: 190px" maxlength="25"/>
                    </td>
                    <th class="align-right">
                        <sps:label name="CN_NO" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:text name="Winv005Form" property="cnNo" style="width: 135px" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <table style="width: 994px; cellspacing: 0; padding-top: 4px;">
            <tr>
                <td align="right">
                    <input type="button" name="Input"
                    id="btnSearch" value="Search" class="select_button" />
                    <input type="button" name="Input" id="btnReset" value="Reset"
                    class="select_button" />
                    <input type="button" name="Input"
                    id="btnDownload" value="Download" class="select_button" />
                </td>
            </tr>
        </table>
        <ai:pagingoption maxRow="5" />
    </html:form>
    <html:form styleId="Winv005FormResult"
        action="/PriceDifferenceInformationAction.do">
        <html:hidden styleId="methodResult" property="method" value="Search" />
        <html:hidden property="invoiceDateFrom" styleId="invoiceDateFromResult"/>
        <html:hidden property="invoiceDateTo" styleId="invoiceDateToResult"/>
        <html:hidden property="cnDateFrom" styleId="cnDateFromResult"/>
        <html:hidden property="cnDateTo" styleId="cnDateToResult"/>
        <html:hidden property="vendorCd" styleId="vendorCdResult"/>
        <html:hidden property="SPcd" styleId="SPcdResult"/>
        <html:hidden property="DCd" styleId="DCdResult"/>
        <html:hidden property="DPcd" styleId="DPcdResult"/>
        <html:hidden property="invoiceStatus" styleId="invoiceStatusResult"/>
        <html:hidden property="invoiceNo" styleId="invoiceNoResult"/>
        <html:hidden property="statusIndex" styleId="statusIndex" />
        <html:hidden property="cnNo" styleId="cnNoResult"/>
        <html:hidden property="asnNo" styleId="asnNoResult"/>
        <html:hidden property="DPn" styleId="DPnResult"/>
        <html:hidden property="SPn" styleId="SPnResult"/>
        <html:hidden property="invoiceNoForward" styleId="invoiceNoForward"/>
        <c:forEach var="supplierAuthen" items="${Winv005Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
            <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
            <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="densoAuthen" items="${Winv005Form.densoAuthenList}" begin="0" step="1" varStatus="status">
            <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
            <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
        </c:forEach>
        <html:hidden styleId="cannotDownloadMessage2" property="cannotDownloadMessage"/>
        <html:hidden styleId="cannotResetMessage2" property="cannotResetMessage"/>
        <logic:notEmpty name="Winv005Form" property="SCdList">
            <c:forEach var="companySupplierItem" items="${Winv005Form.SCdList}" begin="0" step="1" varStatus="status">
                <html:hidden name="companySupplierItem" property="vendorCd" indexed="true" />
                <html:hidden name="companySupplierItem" property="companyName" indexed="true" />
            </c:forEach>
        </logic:notEmpty>
        <logic:notEmpty name="Winv005Form" property="DCdList">
            <c:forEach var="companyDensoItem" items="${Winv005Form.DCdList}" begin="0" step="1" varStatus="status">
                <html:hidden name="companyDensoItem" property="DCd" indexed="true" />
                <html:hidden name="companyDensoItem" property="companyName" indexed="true" />
            </c:forEach>
        </logic:notEmpty>
        <logic:notEmpty name="Winv005Form" property="invoiceStatusList">
        <c:forEach var="invoiceSt" items="${Winv005Form.invoiceStatusList}" begin="0" step="1" varStatus="status">
            <html:hidden name="invoiceSt" property="miscCode" indexed="true"/>
            <html:hidden name="invoiceSt" property="miscValue" indexed="true"/>
        </c:forEach>
        </logic:notEmpty>
        <c:choose>
            <c:when test="${null != Winv005Form.priceDifferenceInformationForDisplayList}">
                <c:set var="visibilityFlag">
                        visible
                </c:set>
            </c:when>
            <c:otherwise>
                <c:set var="visibilityFlag">
                        hidden
                </c:set>
            </c:otherwise>
        </c:choose>
        <div id="result" style="visibility: ${visibilityFlag};">
            <div style="width:98%;height: 285px;position: absolute; overflow-x:auto; overflow-y:auto;">
                    <table class="display_data" id="tblBody" style="table-layout: fixed;width: 1193px;word-wrap: break-word;">
                    <tr>
                        <th style="width: 30px;">
                            <sps:label name="NO" />
                        </th>
                        <th style="width: 40px;">
                            <sps:label name="S_CD" />
                        </th>
                        <th style="width: 40px;" >
                            <sps:label name="S_P" />
                        </th>
                        <th style="width: 45px;">
                            <sps:label name="D_CD" />
                        </th>
                        <th style="width: 40px;" >
                            <sps:label name="D_P" />
                        </th>
                        <th style="width: 120px;">
                            <sps:label name="D_PART_NO" />
                        </th>
                        <th style="width: 120px;">
                            <sps:label name="S_PART_NO" />
                        </th>
                        <th style="width: 100px;">
                            <sps:label name="D_PRICE_UNIT" />
                        </th>
                        <th style="width: 100px;">
                            <sps:label name="S_PRICE_UNIT" />
                        </th>
                        <th style="width: 110px;">
                            <sps:label name="DIFF_PRICE_UNIT" />
                        </th>
                        <th style="width: 110px;">
                            <sps:label name="ASN_NO" />
                        </th>
                        <th style="width: 45px;">
                            <sps:label name="INVOICE_STATUS" />
                        </th>
                        <th style="width: 190px;">
                            <sps:label name="INVOICE_NO" />
                        </th>
                        <th style="width: 70px;">
                            <sps:label name="INVOICE_DATE" />
                        </th>
                        <th style="width: 170px;">
                            <sps:label name="CN_NO" />
                        </th>
                        <th style="width: 70px;">
                            <sps:label name="CN_DATE" />
                        </th>
                    </tr>
                    <c:set var="startRow">
                        ${(Winv005Form.pageNo * Winv005Form.count) - Winv005Form.count}
                    </c:set>
                    <c:forEach var="priceDifferenceInformation"
                        items="${Winv005Form.priceDifferenceInformationForDisplayList}"
                        begin="0" step="1" varStatus="status">
                        <tr>
                            <td style="width: 30px;" class="align-left" >
                                ${startRow + (status.index+1)}
                            </td>
                            <td style="width: 40px;" class="align-left">
                                ${priceDifferenceInformation.vendorCd}
                                <html:hidden name="priceDifferenceInformation" property="vendorCd" indexed="true"/>
                            </td>
                            <td style="width: 40px;" class="align-left">
                                ${priceDifferenceInformation.SPcd}
                                <html:hidden name="priceDifferenceInformation" property="SPcd" indexed="true"/>
                            </td>
                            <td style="width: 45px;" class="align-left">
                                ${priceDifferenceInformation.DCd}
                                <html:hidden name="priceDifferenceInformation" property="DCd" indexed="true"/>
                            </td>
                            <td style="width: 40px;" class="align-left">
                                ${priceDifferenceInformation.DPcd}
                                <html:hidden name="priceDifferenceInformation" property="DPcd" indexed="true"/>
                            </td>
                            <td style="width: 120px;" class="align-left">
                                ${priceDifferenceInformation.DPn}
                                <html:hidden name="priceDifferenceInformation" property="DPn" indexed="true"/>
                            </td>
                            <td style="width: 145px;" class="align-left">
                                ${priceDifferenceInformation.SPn}
                                <html:hidden name="priceDifferenceInformation" property="SPn" indexed="true"/>
                            </td>
                            <td style="width: 100px;" class="align-right">
                                ${priceDifferenceInformation.densoPriceUnit}
                                <html:hidden name="priceDifferenceInformation" property="densoPriceUnit" indexed="true"/>
                            </td>
                             <td style="width: 100px;" class="align-right">
                                ${priceDifferenceInformation.supplierPriceUnit}
                                <html:hidden name="priceDifferenceInformation" property="supplierPriceUnit" indexed="true"/>
                            </td>
                            <td style="width: 110px;" class="align-right" >
                                ${priceDifferenceInformation.differencePriceUnit}
                                <html:hidden name="priceDifferenceInformation" property="differencePriceUnit" indexed="true"/>
                            </td>
                            <td style="width: 110px;" class="align-left" >
                                ${priceDifferenceInformation.spsTCnDetailDomain.asnNo}
                                <html:hidden name="priceDifferenceInformation" property="spsTCnDetailDomain.asnNo" indexed="true"/>
                            </td>
                            <td style="width: 45px;" class="align-left" >
                                ${priceDifferenceInformation.spsTInvoiceDomain.invoiceStatus}
                                <html:hidden name="priceDifferenceInformation" property="spsTInvoiceDomain.invoiceStatus" indexed="true"/>
                            </td>
                            <td style="width: 160px;" class="align-left">
                                <a href="javascript:void(0)" onclick="Winv005ForwardPage(
                                    '${status.index}');return false;" target="_self">
                                    <bean:write name="priceDifferenceInformation" property="spsTInvoiceDomain.invoiceNo" />
                                </a>
                                <html:hidden name="priceDifferenceInformation" property="spsTInvoiceDomain.invoiceId" indexed="true" />
                                <html:hidden name="priceDifferenceInformation" property="spsTInvoiceDomain.invoiceNo" indexed="true"/>
                            </td>
                            <td style="width: 70px;" class="align-left">
                                ${priceDifferenceInformation.invoiceDate}
                                <logic:notEmpty name="priceDifferenceInformation" property="invoiceDate">
                                    <html:hidden name="priceDifferenceInformation" property="invoiceDate" indexed="true"/>
                                </logic:notEmpty>
                            </td>
                            <td style="width: 145px;" class="align-left">
                                <bean:write name="priceDifferenceInformation" property="spsTCnDomain.cnNo" />
                                <html:hidden name="priceDifferenceInformation" property="spsTCnDomain.cnNo" indexed="true"/>
                            </td>
                            <td style="width: 70px;" class="align-left">
                                ${priceDifferenceInformation.cnDate}
                                <logic:notEmpty name="priceDifferenceInformation" property="cnDate">
                                    <html:hidden name="priceDifferenceInformation" property="cnDate" indexed="true"/>
                                </logic:notEmpty>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
            <div style="width:98%;height:300px;">
                    &nbsp;
                </div>
            <table style="width: 994px; border-width: 0px">
                <tr>
                    <td align="left">
                    <ai:paging
                            action="/SPS/PriceDifferenceInformationAction.do"
                            formId="Winv005FormResult" jump="true" />
                    </td>
                </tr>
            </table>
            <div id="displayLegend" style="margin:0;padding-top:2px;">
                    <table bgcolor="#EEEEEE" style="width:994px;border:0;">
                        <tr>
                            <td align="left" style="font-size:10px;">
                                <span style="font-weight: bold;">
                                    <sps:label name="LEGEND" />
                                    <span style="text-decoration: underline;">
                                        <sps:label name="INVOICE_STATUS"  />
                                    </span>
                                </span>
                                <sps:label name="ISS_ISC_DCL_RAP_DHP_PAY"  />
                            </td>
                            <td class="align-right">
                                <a href="#" style="font-size:10px;" id="moreLegend">
                                    &gt;&gt; 
                                    <sps:label name="MORE" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </html:form>
        <html:form styleId="Winv005FormDownload" action="/PriceDifferenceInformationAction.do" >
            <html:hidden property="method" styleId="methodDownload"/>
            <html:hidden property="vendorCd" styleId="SCd"/>
            <html:hidden property="DCd" styleId="DCd"/>
            <c:forEach var="supplierAuthen" items="${Winv005Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
	            <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
	            <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
	        </c:forEach>
	        <c:forEach var="densoAuthen" items="${Winv005Form.densoAuthenList}" begin="0" step="1" varStatus="status">
	            <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
	            <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
	        </c:forEach>
            <logic:notEmpty name="Winv005Form" property="SCdList">
                <c:forEach var="companySupplierItem" items="${Winv005Form.SCdList}" 
                    begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplierItem" property="vendorCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Winv005Form" property="DCdList">
                <c:forEach var="companyDensoItem" items="${Winv005Form.DCdList}"
                    begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoItem" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
        </html:form>
    </body>
</html>