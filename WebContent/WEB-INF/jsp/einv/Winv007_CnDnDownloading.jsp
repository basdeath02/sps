<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
		<%@ include file="../includes/html_header.jspf"%>
		<jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
		<script type="text/javascript" src="./js/business/Winv007_CnDnDownloading.js">
		</script>
		<script type="text/javascript">
		    var labelAll = '<sps:label name="ALL_CRITERIA" />';
		    var labelPleaseSelect = '<sps:label name="PLEASE_SELECT_CRITERIA" />';
		</script>
		<title>
		    <sps:label name="CNDN_DOWNLOADING" />
		</title>
    </head>
    
    <body class="hideHorizontal">
        <html:form styleId="Winv007FormCriteria" enctype="multipart/form-data" action="/CnDnDownloadingAction.do">
	        <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
	        <html:hidden styleId="method" property="method" />
	        <html:hidden styleId="fileId" property="fileId"/>
	        <html:hidden styleId="currentPage" property="currentPage"/>
	        <html:hidden styleId="row"  property="row"/>
	        <html:hidden styleId="totalCount" property="totalCount"/>
	        <html:hidden styleId="cannotResetMessage" property="cannotResetMessage"/>
	        <html:hidden styleId="cannotDownloadMessage" property="cannotDownloadMessage"/>
	        
            <c:forEach var="supplierAuthen" items="${Winv002Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
            </c:forEach>
            <c:forEach var="densoAuthen" items="${Winv002Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
            </c:forEach>
            
            <table id="ui-displayHeader" class="bottom">
               <tr>
                   <td>
                       <sps:label name="INVOICE" />
                        &gt; 
                       <sps:label name="CN_DN_DOWNLOADING" />
                    </td>
                    <td colspan="2" class="align-right">
                        <sps:label name="USER" />
                             &nbsp; : <bean:write name="Winv007Form" property="userLogin" />
                    </td>
                </tr>
            </table>
            
            <c:choose>
                <c:when test="${not empty Winv007Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width:992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg" items="${Winv007Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message}
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            
            <fieldset class="fixwidth">
                <c:set var="lbSearch">
                    <sps:label name="SEARCH" />
                </c:set>
                <c:set var="lbReset">
                    <sps:label name="RESET" />
                </c:set>
                <c:set var="lbDownload">
                    <sps:label name="DOWNLOAD" />
                </c:set>
                <table class="display_criteria" id="ui-displayCriteria">
                    <tr>
                        <th style="width:95px;" class="align-right">
                            <sps:label name="UPLOAD_DATE" />
                            &nbsp;:&nbsp;
                        </th>
                        <td style="width:236px;">
                            <sps:calendar name="updateDateFrom" id="updateDateFrom"
                            value="${Winv007Form.updateDateFrom}"/>
                            -
                            <sps:calendar name="updateDateTo" id="updateDateTo"
                            value="${Winv007Form.updateDateTo}"/>
                        </td>
                        <th style="width:95px;" class="align-right">
                            <sps:label name="S_CD" />
                            &nbsp;:&nbsp;
                        </th>
                        <td style="width:120px;">
                            <html:select name="Winv007Form" property="vendorCd" 
                                style="width:120px" styleId="supplierCompanyCode" >
                            <html:option value="ALL">
                                <sps:label name="ALL_CRITERIA" />
                            </html:option>
                                <c:forEach var="sCompany"
                                items="${Winv007Form.SCdList}"
                                begin="0" step="1" varStatus="status">
                                <html:option value="${sCompany.miscCode}">
                                    ${sCompany.miscValue}
                                </html:option>
                            </c:forEach>
                            </html:select>
                            <c:forEach var="companySupplierItem" 
                                items="${Winv007Form.SCdList}" 
                                begin="0" step="1" varStatus="status">
                                <html:hidden name="companySupplierItem" property="miscCode"
                                    indexed="true"/>
                                <html:hidden name="companySupplierItem" property="miscValue"
                                    indexed="true"/>
                            </c:forEach>
                        </td>
                        <th style="width:95px;" class="align-right">
                            <sps:label name="S_P" />
                            &nbsp;:&nbsp;
                        </th>
                        <td style="width:120px;">
                            <html:select name="Winv007Form" property="SPcd" 
                                style="width:120px" styleId="supplierPlantCode">
                            <html:option value="ALL">
                                <sps:label name="ALL_CRITERIA" />
                            </html:option>
                                <c:forEach var="sCompany"
                                items="${Winv007Form.SPcdList}"
                                begin="0" step="1" varStatus="status">
                                <html:option value="${sCompany.miscCd}">
                                    ${sCompany.miscValue}
                                </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th width="90px;" class="align-right">
                            <sps:label name="D_CD" />
                            &nbsp;:
                        </th>
                        <td style="width: 124px;">
                        <html:select styleId="dCd"
                            style="width:120px" property="DCd">
                            <html:option value="ALL">
                                <sps:label name="ALL_CRITERIA" />
                            </html:option>
                            <c:forEach var="dCd"
                                items="${Winv007Form.companyDensoList}"
                                begin="0" step="1" varStatus="status">
                                <html:option value="${dCd.miscCode}">
                                    ${dCd.miscValue}
                                </html:option>
                            </c:forEach>
                        </html:select>
                        <logic:notEmpty name="Winv007Form" property="companyDensoList">
                                <c:forEach var="dCdResult" items="${Winv007Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                    <html:hidden name="dCdResult" property="miscCode" indexed="true"/>
                                    <html:hidden name="dCdResult" property="miscValue" indexed="true"/>
                                </c:forEach>
                        </logic:notEmpty>
                        </td>
                    </tr>
                    </table>
                    <table class="display_criteria" id="ui-displayCriteria">
                    <tr>
                        <th width="11%" class="align-right">
                            <sps:label name="D_P" />
                            &nbsp;:
                        </th>
                        <td style="width: 140px;">
                        <html:select styleId="dPcd"
                            style="width: 120px"
                            property="DPcd">
                            <html:option value="ALL">
                                <sps:label name="ALL_CRITERIA" />
                            </html:option>
                            <c:forEach var="sCompany"
                                    items="${Winv007Form.DPcdList}"
                                    begin="0" step="1" varStatus="status">
                                <html:option value="${sCompany.DPcd}">
                                    ${sCompany.DPcd}
                                </html:option>
                            </c:forEach>
                        </html:select>
                        <c:forEach var="plantDenso"
                            items="${Winv007Form.DPcdList}"
                            begin="0" step="1" varStatus="status">
                            <html:hidden name="plantDenso" property="DPcd" indexed="true"/>
                        </c:forEach>
                        </td>
                        <th style="width:11%;" class="align-right">
                            <sps:label name="FILE_NAME" />
                            &nbsp;:&nbsp;
                        </th>
                        <td style="width:82%;">
                            <html:text property="fileNameCriteria" styleClass="txtbox"
                                style="width:655px;"/>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <table style="width:994px; cellspacing: 0; padding-top: 4px; padding-bottom: 2px;">
                <tr>
                    <td align="right">
                        <input type="button" name="Input"  id="btnSearch" value="${lbSearch}"
                            class="select_button"/>
                        <input type="button" name="Input" id="btnReset" value="${lbReset}"
                            class="select_button" />
                    </td>
                </tr>
            </table>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        <html:form styleId="Winv007FormResult" action="/CnDnDownloadingAction.do">
            <html:hidden styleId="methodResult" property="method" />
            <html:hidden property="updateDateFrom"/>
            <html:hidden property="updateDateTo"/>
            <html:hidden property="vendorCd"/>
            <html:hidden property="SPcd"/>
            <html:hidden property="DCd"/>
            <html:hidden property="DPcd"/>
            <html:hidden property="fileNameCriteria"/>
            <html:hidden property="cannotResetMessage"/>
            <html:hidden property="cannotDownloadMessage"/>
                        
            <c:forEach var="supplierAuthen" items="${Winv002Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
            </c:forEach>
            <c:forEach var="densoAuthen" items="${Winv002Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
            </c:forEach>
            
            <logic:notEmpty name="Winv007Form" property="SCdList">
                <c:forEach var="companySupplierItem" items="${Winv007Form.SCdList}"
                    begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplierItem" property="miscCode" indexed="true"/>
                    <html:hidden name="companySupplierItem" property="miscValue" indexed="true"/>
            </c:forEach>
            </logic:notEmpty>
            
            <c:choose>
                <c:when test="${0 == Winv007Form.max}">
                    <c:set var="visibilityFlag">
                        hidden
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="visibilityFlag">
                        visible
                    </c:set>
                </c:otherwise>
            </c:choose>
            
            <div id="result" style="visibility: ${visibilityFlag};">
                <div style="width: 994px;">
                    <table class="display_data" id="tblBody" 
                        style="table-layout:fixed; width:974px; word-wrap:break-word;">
                        <tr>
                            <th style="width:35px;" scope="col">
                                <sps:label name="NO" />
                            </th>
                            <th style="width:60px;" scope="col">
                                <sps:label name="S_CD" />
                            </th>
                            <th style="width:50px;" scope="col">
                                <sps:label name="S_P" />
                            </th>
                            <th style="width: 41%;" scope="col">
                                <sps:label name="D_CD" />
                            </th>
                            <th style="width: 39%;" scope="col">
                                <sps:label name="D_P" />
                            </th>
                             <th style="width:300px;" scope="col">
                                <sps:label name="FILE_NAME" />
                            </th>
                            <th style="width:94px;" scope="col">
                                <sps:label name="UPLOAD_DATE" />
                            </th>
                            <th style="width:190px;" scope="col">
                                <sps:label name="COMMENT" />
                            </th>
                            <th style="width:70px;" scope="col">
                                <sps:label name="DOWNLOAD" />
                            </th>
                        </tr>
                    </table>
                </div>
                <div style="width:994px; height:325px; overflow-y:auto">
                    <table class="display_data" id="tblBody"
                        style="table-layout:fixed; width:974px; word-wrap:break-word;
                        border-width:0px">
                        <c:set var="startRow">
                            ${(Winv007Form.pageNo * Winv007Form.count) - Winv007Form.count}
                        </c:set>
                        <c:forEach var="fileUpload" items="${Winv007Form.fileUploadForDisplayList}"
                            begin="0" step="1" varStatus="status">
                            <tr>
                                <td align="center" style="width:35px;">
                                    ${startRow + (status.index+1)}
                                </td>
                                <td style="width:60px;">
                                    ${fileUpload.fileUploadDomain.vendorCd}
                                </td>
                                <td style="width:50px;">
                                    ${fileUpload.fileUploadDomain.SPcd}
                                </td>
                                <td style="width:52px;">
                                    ${fileUpload.fileUploadDomain.DCd}
                                </td>
                                <td style="width:49px;">
                                    ${fileUpload.fileUploadDomain.DPcd}
                                </td>
                                <td style="width:300px;">
                                    ${fileUpload.fileUploadDomain.fileName}
                                </td>
                                <td style="width:94px;">
                                    ${fileUpload.fileUploadDomain.lastUpdateDate}
                                </td>
                                <td style="width:190px;">
                                    <bean:write name="fileUpload" property="fileUploadDomain.comment" />
                                </td>
                                <td style="width:70px;" align="center">
                                    <a href="#">
                                        <img src="images/icons/document.png" style="width:19px; height: 20px;"
                                            alt="${lbDownload }"
                                            onclick="Winv007DowloadFile(
                                            '${fileUpload.fileUploadDomain.fileId}');" />
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <table style="width:994px;border-width:0px">
                    <tr>
                        <td align="left">
                            <ai:paging action="/SPS/CnDnDownloadingAction.do" formId="Winv007FormResult"
                                jump="true" />
                        </td>
                    </tr>
                </table>
            </div>
        </html:form>
    </body>
</html>
