<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Winv004_InvoiceMaintenance.js">
        </script>
        
        <%-- [IN059] Keep default Vat Rate --%>
        <script type="text/javascript">
            var defaultVatRate = '${Winv004Form.defaultVatRate}';
        </script>                            
        
        <title>
            <sps:label name="INVOICE_MAINTENANCE" />
        </title>
    </head>
    
    <body class="hideHorizontal">
        <html:form styleId="Winv004Form" enctype="multipart/form-data" 
            action="/InvoiceMaintenanceAction.do">
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            
            <html:hidden styleId="method" property="method" />
            <html:hidden property="dscId" />
            <html:hidden name="Winv004Form" property="invoiceId" />
            <html:hidden property="screenId" styleId="screenId" />
            <html:hidden property="decimalDisp" styleId="decimalDisp" />
            <html:hidden property="mode" styleId="mode" />
            <html:hidden property="strConfirm" styleId="strConfirm" />
            <html:hidden property="strConfirmReturn" styleId="strConfirmReturn" />
            <html:hidden property="strConfirmCalculate" styleId="strConfirmCalculate" />
            <html:hidden property="errorSupplierInvoiceAmount" styleId="errorSupplierInvoiceAmount" />
            <html:hidden property="errorSupplierUnitPrice" styleId="errorSupplierUnitPrice" />
            <html:hidden property="errorSupplierBaseNull" styleId="errorSupplierBaseNull" />
            <html:hidden property="errorSupplierVatNull" styleId="errorSupplierVatNull" />
            <html:hidden property="errorSupplierTotalNull" styleId="errorSupplierTotalNull" />
            <html:hidden property="SCd" />
            <html:hidden property="supplierAddress1" />
            <html:hidden property="supplierAddress2" />
            <html:hidden property="supplierAddress3" />
            <html:hidden property="coverPageFileId" />
            <html:hidden property="diffDPcdFlag" />
            <html:hidden property="cannotDownloadMessage" styleId="cannotDownloadMessage" />
            <html:hidden property="cannotCalculateCnMessage" styleId="cannotCalculateCnMessage" />
            
            <table style="height: 30px; border: 0; cellpadding: 0; cellspacing: 0; width: 990px;"
                class="bottom">
                <tr>
                    <td>
                        <sps:label name="INVOICE" />
                        &gt; 
                        <sps:label name="INVOICE_MAINTENANCE" />
                    </td>
                    <td colspan="2" class="align-right">
                        <sps:label name="USER" />
                        &nbsp;:
                        <bean:write name="Winv004Form" property="userLogin" />
                    </td>
                </tr>
            </table>
            
            <c:choose>
                <c:when test="${not empty Winv004Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width: 992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg"
                                items="${Winv004Form.applicationMessageList}"
                                begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message} 
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            
            <table style="cellpadding: 0;">
               
                <tr>
                    <td>
                        <fieldset style="padding-top:0px" class="fixwidth">
                            <legend>
                                <sps:label name="DENSO_INFORMATION"/>
                            </legend>
                            <table class="display_criteria" 
                                style="width: 975px; border: 0; cellpadding: 3; cellspacing: 1; margin-top: 3px; table-layout: fixed;">
                                <tr>
                                    <th width="13%" class="align-right">
                                        <sps:label name="DENSO_NAME" />
                                        &nbsp;:
                                    </th>
                                    <td width="42%">
                                        <html:text name="Winv004Form" property="densoName" 
                                            style="width:398px" readonly="true"/>
                                    </td>
                                    <th width="6%">
                                        <sps:label name="D_CD" />
                                        &nbsp;:
                                    </th>
                                    <td width="8%">
                                        <html:text name="Winv004Form" property="DCd" 
                                            style="width:70px" readonly="true"/>
                                    </td>
                                    <th width="6%">
                                        <sps:label name="D_P" />
                                        &nbsp;:
                                    </th>
                                    <td width="5%">
                                        <html:text name="Winv004Form" property="DPcd" 
                                            style="width:41px" readonly="true"/>
                                    </td>
                                    <th width="7%">
                                        <sps:label name="DENSO_TAX_ID" />
                                        &nbsp;:
                                    </th>
                                    <td width="13%">
                                        <html:text name="Winv004Form" property="densoTaxId" 
                                            style="width:100px" readonly="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="align-left">
                                        <sps:label name="ADDRESS" />
                                        &nbsp;:
                                    </th>
                                    <td colspan="5">
                                        <html:text name="Winv004Form" property="densoAddress" 
                                            style="width:643px" readonly="true"/>
                                    </td>
                                    <th class="align-right">
                                        <sps:label name="CURRENCY" />
                                        &nbsp;:
                                    </th>
                                    <td class="align-left">
                                        <html:text name="Winv004Form" property="densoCurrency" 
                                            style="width:100px" readonly="true"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                   <td>
                       <fieldset style="padding-top:0px" class="fixwidth">
                           <legend>
                               <sps:label name="SUPPLIER_INFORMATION"/>
                           </legend>
                           <table class="display_criteria"
                               style="width: 975px; border: 0; cellpadding: 3; cellspacing: 1; 
                               margin-top: 3px; margin-left: 1px; margin-bottom: 0px; 
                               table-layout: fixed; border-spacing: 0;">
                                <col width="13%" />
                                <col width="20%" />
                                <col width="10%" />
                                <col width="14%" />
                                <col width="9%"  />
                                <col width="7%"  />
                                <col width="9%"  />
                                <col width="18%" />
                                <tr>
                                    <th class="align-right">
                                        <sps:label name="SUPPLIER_NAME" />
                                        &nbsp;:
                                    </th>
                                    <td colspan="3">
                                        <html:text name="Winv004Form" property="supplierName" 
                                            style="width:420px" readonly="true"/>
                                    </td>
                                    <th>
                                        <sps:label name="S_CD" />
                                        &nbsp;:
                                    </th>
                                    <td>
                                        <html:text name="Winv004Form" property="vendorCd" 
                                            style="width:60px" readonly="true"/>
                                    </td>
                                    <th>
                                        <sps:label name="S_TAX_ID" />
                                        &nbsp;:
                                    </th>
                                    <td>
                                        <html:text name="Winv004Form" property="supplierTaxId" 
                                            style="width:149px" readonly="true"/>
                                    </td>
                                </tr>
                            </table>
                            
                            <c:if test="${cons.MODE_REGISTER == Winv004Form.mode || cons.MODE_UPLOAD == Winv004Form.mode}">
                                <c:set var="mandatoryFlag">
                                    mandatory
                               </c:set>
                            </c:if>
                            <c:if test="${cons.MODE_VIEW == Winv004Form.mode || cons.MODE_INITIAL == Winv004Form.mode}">
                                <c:set var="readOnlyFlag">
                                   true
                                </c:set>
                                <c:set var="displayFlag">
                                    disabled
                                </c:set>
                            </c:if>
                            <c:if test="${cons.MODE_INITIAL == Winv004Form.mode}">
                                <c:set var="displayFlagOfDownload">
                                    disabled
                                </c:set>
                            </c:if>
                            <table class="display_criteria"
                                style="width: 975px; border: 0; cellpadding: 3; cellspacing: 1;
                                margin-top: 0px; table-layout: fixed;">
                                 <col width="13%" />
                                 <col width="20%" />
                                 <col width="12%" />
                                 <col width="8%"  />
                                 <col width="7%"  />
                                 <col width="5%"  />
                                 <col width="10%" />
                                 <col width="25%" />
                                 <tr>
                                     <th class="align-right">
                                        <sps:label name="ADDRESS" />
                                        &nbsp;:
                                    </th>
                                    <td colspan="5">
                                        <html:text name="Winv004Form" property="supplierAddress" 
                                            style="width:499px" readonly="true"/>
                                    </td>
                                    <th class="align-right">
                                        <sps:label name="CURRENCY" />
                                        &nbsp;:
                                    </th>
                                    <td class="align-left">
                                        <html:text name="Winv004Form" property="supplierCurrency" 
                                            style="width:217px" readonly="true"/>
                                    </td>
                                 </tr>
                                 <tr>
                                     <th class="align-right">
                                        <sps:label name="INVOICE_NO" />
                                        &nbsp;:
                                    </th>
                                    <td>
                                    <html:text name="Winv004Form" property="invoiceNo" 
                                        style="width:185px;" readonly="${readOnlyFlag}" styleClass="${mandatoryFlag}"/>
                                    </td>
                                    <th class="align-right">
                                        <sps:label name="INVOICE_DATE" />
                                        &nbsp;:
                                    </th>
                                    <td colspan="3">
                                        <sps:calendar name="invoiceDate" id="invoiceDateCriteria" 
                                            value="${Winv004Form.invoiceDate}" />
                                    </td>
                                    <th class="align-right">
                                        <sps:label name="VAT" />
                                        &nbsp;:
                                    </th>
                                    <td align="left">
                                        <input name="vatType" type="radio" id="VAT"
                                            value="1" ${displayFlag}
                                            onclick="setMandatory();"
                                            <%-- [IN059] Set checked Radio Button by only vatType --%>
                                            <%--<c:if test="${not empty Winv004Form.vatRate || 1 == Winv004Form.vatType}">--%>
                                            <c:if test="${1 == Winv004Form.vatType}">
                                                checked
                                            </c:if>
                                        />
                                        <sps:label name="VAT" />
                                        
                                        <%-- [IN059] To keep default VAT Rate --%>
                                        <html:hidden name="Winv004Form" property="defaultVatRate" />
                                        
                                        <html:text name="Winv004Form" property="vatRate" styleId="vatRate"
                                            style="width:25px;text-align:right;" styleClass="${mandatoryFlag}" 
                                            readonly="${readOnlyFlag}"
                                            onkeypress="return checkEnter(event);"
                                            onchange="calNewVat();"/>
                                            %
                                        <input name="vatType" type="radio" id="nonVAT"
                                           value="0" ${displayFlag}
                                           onclick="calNonVat();"
                                           <%-- [IN059] Set checked Radio Button by only vatType --%>
                                           <%-- <c:if test="${empty Winv004Form.vatRate && 0 == Winv004Form.vatType}"> --%>
                                           <c:if test="${0 == Winv004Form.vatType}">
                                               checked
                                           </c:if>
                                        />
                                        <sps:label name="NON_VAT_GST" />
                                    </td>
                                 </tr>
                                 <tr>
                                    <th align="right">
                                        <sps:label name="BASE_AMOUNT" />
                                        &nbsp;:
                                    </th>
                                    <td >
                                        <html:text name="Winv004Form" property="supplierBaseAmount"
                                            styleId="supplierBaseAmount" style="width:185px;text-align:right;" 
                                            readonly="${readOnlyFlag}" styleClass="${mandatoryFlag}"
                                            onchange="addDot(this);"
                                            onkeypress="return checkAmount(event,this);"/>
                                    </td>
                                    <th align="right">
                                        <sps:label name="VAT_AMOUNT" />
                                        &nbsp;:
                                    </th>
                                    <td colspan="3">
                                        <html:text name="Winv004Form" property="supplierVatAmount"
                                            styleId="supplierVatAmount" style="width:188px;text-align:right;" 
                                            readonly="${readOnlyFlag}" styleClass="${mandatoryFlag}"
                                            onchange="addDot(this);"
                                            onkeypress="return checkAmount(event,this);"/>
                                    <th align="right">
                                        <sps:label name="TOTAL_AMOUNT" />
                                        &nbsp;:
                                    </th>
                                    <td >
                                        <html:text name="Winv004Form" property="supplierTotalAmount"
                                            styleId="supplierTotalAmount" style="width:216px;text-align:right;" 
                                            readonly="${readOnlyFlag}" styleClass="${mandatoryFlag}"
                                            onchange="addDot(this);"
                                            onkeypress="return checkAmount(event,this);"/>
                                    </td>
                                 </tr>
                            </table>
                       </fieldset>
                    </td>
                 </tr>
                <tr>
                    <td>
                        <fieldset style="padding-top:0px" class="fixwidth">
                            <legend>
                                <sps:label name="CREDIT_NOTE_INFORMATION" />
                            </legend>
                            <table class="display_criteria"
                                style="width: 975px; border: 0; cellpadding: 3; cellspacing: 1; 
                                margin-top: 3px; table-layout: fixed;">
                                <tr>
                                    <th width="13%">
                                        <sps:label name="CREDIT_NOTE_NO" />
                                        &nbsp;:
                                    </th>
                                    <td width="20%">
                                        <html:text name="Winv004Form" property="cnNo" 
                                            readonly="${readOnlyFlag}" style="width:186px"/>
                                    </td>
                                    <th width="12%">
                                        <sps:label name="CREDIT_NOTE_DATE" />
                                        &nbsp;:
                                    </th>
                                    <td width="20%">
                                        <sps:calendar name="cnDate" id="cnDateCriteria" 
                                            value="${Winv004Form.cnDate}"/>
                                    </td>
                                    <td width="10%">
                                    </td>
                                    <td width="25%" style="text-align:right; padding-right: 21px;">
                                        <c:set var="labelCalculate">
                                            <sps:label name="CACULATE_CN_AMOUNT"/>
                                        </c:set>
                                        <input type="button" name="Input" id="btnCalculate" value="${labelCalculate}" 
                                        class="select_button" style="width:147px" ${displayFlag}/>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="align-right">
                                        <sps:label name="BASE_AMOUNT" />
                                        &nbsp;:
                                    </th>
                                    <td>
                                        <html:text name="Winv004Form" property="cnBaseAmount" styleId="cnBaseAmount"
                                            readonly="${readOnlyFlag}" style="width:185px;text-align:right;"
                                            onchange="addDot(this);"
                                            onkeypress="return checkAmountNegative(event,this);"/>
                                    </td>
                                    <th class="align-right">
                                        <sps:label name="VAT_AMOUNT" />
                                        &nbsp;:
                                    </th>
                                    <td>
                                        <html:text name="Winv004Form" property="cnVatAmount" styleId="cnVatAmount"
                                            readonly="${readOnlyFlag}" style="width:186px;text-align:right;"
                                            onchange="addDot(this);"
                                            onkeypress="return checkAmountNegative(event,this);"/>
                                    </td>
                                    <th >
                                        <sps:label name="TOTAL_AMOUNT" />
                                        &nbsp;:
                                    </th>
                                    <td >
                                        <html:text name="Winv004Form" property="cnTotalAmount" styleId="cnTotalAmount"
                                            readonly="${readOnlyFlag}" style="width:216px;text-align:right;"
                                            onchange="addDot(this);"
                                            onkeypress="return checkAmountNegative(event,this);"/>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                 </tr>
            </table>
            <div style="width:width: 98%;height: 350px;position: absolute;">
                <table class="display_data" id="tblBody" 
                    style="table-layout: fixed;width: 1320px;word-wrap: break-word;">
                    <tr>
                        <th width="20px"  scope="col">
                            <sps:label name="NO" />
                        </th>
                        <th width="105px" scope="col">
                            <sps:label name="ASN_NO" />
                        </th>
                        <th width="25px"  scope="col">
                            <sps:label name="TRIP_NO" />
                        </th>
                        <th width="29px"  scope="col">
                            <sps:label name="ASN_STATUS" />
                        </th>
                        <th width="103px" scope="col">
                            <sps:label name="D_PART_NO" />
                        </th>
                        <th width="25px" scope="col">
                            <sps:label name="D_U/M" />
                        </th>
                        <th width="85px" scope="col">
                            <sps:label name="D_U/P" />
                        </th>
                        <th width="10px" scope="col">
                            <sps:label name="TEMP_PRICE" />
                        </th>
                        <th width="25px" scope="col">
                            <sps:label name="S_U/M" />
                        </th>
                        <th width="85px" scope="col">
                            <sps:label name="S_U/P" />
                        </th>
                        <th width="60px" scope="col">
                            <sps:label name="SHIPPING_QTY" />
                        </th>
                        <th width="80px" scope="col">
                            <sps:label name="DIFFERENCE_BASE_AMOUNT" />
                        </th>
                        <th width="80px" scope="col">
                            <sps:label name="D_BASE_AMOUNT" />
                        </th>
                        <th width="125px" scope="col">
                            <sps:label name="S_PART_NO" />
                        </th>
                        <th width="73px" scope="col">
                            <sps:label name="D/O_NO" />
                        </th>
                        <th width="20px" scope="col">
                            <sps:label name="REV" />
                        </th>
                    </tr>
                    <c:forEach var="asnDetail" 
                        items="${Winv004Form.asnDetailList}" begin="0" 
                        step="1" varStatus="status" >
                        <tr>
                            <td class="align-right" >
                                ${status.index+1}
                            </td>
                            <td class="align-left" >
                                <bean:write name="asnDetail" property="asnNo" />
                            </td>
                            <td class="align-left" >
                                <bean:write name="asnDetail" property="tripNo" />
                            </td>
                            <td class="align-left" >
                                <bean:write name="asnDetail" property="asnStatus" />
                            </td>
                            <td class="align-left" >
                                <bean:write name="asnDetail" property="DPn" />
                            </td>
                            <td class="align-left" >
                                <bean:write name="asnDetail" property="densoUnitOfMeasure" />
                            </td>
                            <td class="align-right" >
                                <bean:write name="asnDetail" property="densoUnitPrice" />
                                <html:hidden name="asnDetail" property="densoUnitPrice" styleId="densoUnitPrice${status.index}" indexed="true" />
                            </td>
                            <td>
                                <bean:write name="asnDetail" property="tempPriceFlag" />
                            </td>
                            <td class="align-left" >
                                <bean:write name="asnDetail" property="supplierUnitOfMeasure" />
                            </td>
                            <td class="align-right" >
                                <html:hidden name="asnDetail" property="supplierUnitPrice" indexed="true" styleId="supplierUnitPrice${status.index}" />
                                <c:choose>
                                    <c:when test="${cons.MODE_REGISTER == Winv004Form.mode}">
                                        <input type="text" value="${asnDetail.supplierUnitPrice}" name="supplierUnitPriceTxt" id="supplierUnitPriceTxt${status.index}"
                                            style="width:100px" class="number"
                                            onkeypress="return PositiveDoubleFilter(event)"
                                            onchange="return winv004CalculateDiffAmount('${status.index}')"
                                            onfocus="removeComma('supplierUnitPriceTxt${status.index}')"/>
                                    </c:when>
                                    <c:otherwise>
                                        <input type="text" value="${asnDetail.supplierUnitPrice}" name="supplierUnitPriceTxt" id="supplierUnitPriceTxt${status.index}"
                                            style="width:100px;text-align:right" class="readonly" readonly/>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td class="align-right" >
                                <bean:write name="asnDetail" property="shippingQty" />
                                <html:hidden name="asnDetail" property="shippingQty" styleId="shippingQty${status.index}" indexed="true" />
                            </td>
                            <td class="align-right" >
                                <html:hidden name="asnDetail" property="diffBaseAmount" indexed="true" styleId="diffBaseAmount${status.index}" />
                                <html:hidden name="asnDetail" property="bdDiffBaseAmount" indexed="true" styleId="bdDiffBaseAmount${status.index}" />
                            
                                <c:choose>
                                    <c:when test="${0.00 != asnDetail.bdDiffBaseAmount}">
                                        <c:set var="diffAmountColor">
                                            class="textred"
                                        </c:set>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="diffAmountColor">
                                            
                                        </c:set>
                                    </c:otherwise>
                                </c:choose>
                                <span id="spanDiffBaseAmount${status.index}" ${diffAmountColor} >
                                    ${asnDetail.diffBaseAmount}
                                </span>
                            </td>
                            <td class="align-right" >
                                ${asnDetail.densoBaseAmount}
                            </td>
                            <td class="align-left" >
                                <bean:write name="asnDetail" property="SPn" />
                                <html:hidden name="asnDetail" property="asnNo" indexed="true" />
                                <html:hidden name="asnDetail" property="tripNo" indexed="true" />
                                <html:hidden name="asnDetail" property="asnStatus" indexed="true" />
                                <html:hidden name="asnDetail" property="DPn" indexed="true" />
                                <html:hidden name="asnDetail" property="densoUnitOfMeasure" indexed="true" />
                                <html:hidden name="asnDetail" property="tempPriceFlag" indexed="true" />
                                <html:hidden name="asnDetail" property="supplierUnitOfMeasure" indexed="true" />
                                <html:hidden name="asnDetail" property="densoBaseAmount" indexed="true" />
                                <html:hidden name="asnDetail" property="SPn" indexed="true" />
                                <html:hidden name="asnDetail" property="doId" indexed="true" />
                                <html:hidden name="asnDetail" property="spsTAsnDomain.createDatetime" indexed="true" />
                                <html:hidden name="asnDetail" property="spsDoNo" indexed="true" />
                                <html:hidden name="asnDetail" property="revision" indexed="true" />
                            </td>
                            <td class="align-left" >
                                <bean:write name="asnDetail" property="spsDoNo" />
                            </td>
                            <td class="align-left" >
                                <bean:write name="asnDetail" property="revision" />
                            </td>
                        </tr>
                    </c:forEach>
                </table>
                <table class="display_data" id="tblBody">
                    <tr>
                        <td width="769px" class="align-right" bgcolor="white">
                            <b>
                                <sps:label name="TOTAL_BASE_AMOUNT" />
                            </b>
                            &nbsp;
                        </td>
                        <td width="80px" class="align-right" bgcolor="white" 
                            style="padding-left:0px;padding-right:0px;">
                            <html:text name="Winv004Form" property="totalDiffBaseAmount" styleId="totalDiffBaseAmount"
                                style="width:130px;text-align:right;color:red;" readonly="true"/>
                        </td>
                        <td width="80px" class="align-right" bgcolor="white" 
                            style="padding-left:0px;padding-right:0px;">
                            <html:text name="Winv004Form" property="totalBaseAmount" styleId="totalBaseAmount"
                                style="width:130px;text-align:right;" readonly="true"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-right" bgcolor="white">
                            <b>
                               <sps:label name="VAT_AMOUNT" />
                            </b>
                            &nbsp;
                        </td>
                        <td class="align-right" bgcolor="white" 
                            style="padding-left:0px;padding-right:0px;">
                            <html:text name="Winv004Form" property="totalDiffVatAmount"  styleId="totalDiffVatAmount"
                                style="width:130px;text-align:right;color:red;" readonly="true"/>
                        </td>
                        <td class="align-right" bgcolor="white" 
                            style="padding-left:0px;padding-right:0px;">
                            <html:text name="Winv004Form" property="totalVatAmount" styleId="totalVatAmount"
                                style="width:130px;text-align:right;" readonly="true"/>
                       </td>
                    </tr>
                    <tr>
                        <td class="align-right" bgcolor="white">
                            <b>
                                <sps:label name="TOTAL_AMOUNT" />
                            </b>
                            &nbsp;
                        </td>
                        <td class="align-right" bgcolor="white" 
                            style="padding-left:0px;padding-right:0px;">
                            <html:text name="Winv004Form" property="totalDiffAmount" styleId="totalDiffAmount"
                                style="width:130px;text-align:right;color:red;" readonly="true"/>
                        </td>
                        <td class="align-right" bgcolor="white" 
                            style="padding-left:0px;padding-right:0px;">
                            <html:text name="Winv004Form" property="totalAmount" styleId="totalAmount"
                                style="width:130px;text-align:right;" readonly="true"/>
                        </td>
                    </tr>
                </table>
                <div style="width:98%;height:25px;white-space:nowrap;">
                    &nbsp;
                </div>
                <table style="width: 994px;">
                   <c:set var="labelPreviewCoverPage">
                       <sps:label name="PREVIEW_PDF"/>
                   </c:set>
                   <c:set var="labelDownloadPdf">
                       <sps:label name="DOWNLOAD_PDF"/>
                   </c:set>
                   <c:set var="labelDownload">
                       <sps:label name="DOWNLOAD_CSV"/>
                   </c:set>
                   <c:set var="labelRegister">
                       <sps:label name="REGISTER"/>
                   </c:set>
                   <c:set var="labelReturn">
                       <sps:label name="RETURN"/>
                   </c:set>
                   <tr>
                       <td class="align-right">
                           <input type="button" name="Input" id="btnPreview" 
                               value="${labelPreviewCoverPage}" class="select_button" ${displayFlag}/>
                           <input type="button" name="Input" id="btnDownloadPdf" 
                               value="${labelDownloadPdf}" class="select_button"/>
                           <input type="button" name="Input" id="btnDownload" value="${labelDownload}" 
                               class="button2" style="width:110px"  ${displayFlagOfDownload}/>
                           <input type="button" name="Input" id="btnRegister" value="${labelRegister}" 
                               class="select_button" ${displayFlag}/>
                           <input type="button" name="Input" id="btnReturn" value="${labelReturn}" 
                               class="select_button"/>
                       </td>
                   </tr>
                </table>
                <table bgcolor="#EEEEEE" style="width:994px;border:0;">
                    <tr>
                        <td align="left" style="font-size:10px;">
                            <span style="font-weight: bold;">
                                <sps:label name="LEGEND" />
                                <span style="text-decoration: underline;">
                                    <sps:label name="ASN_STATUS" />
                                </span>
                            </span>
                            <sps:label name="ISS_CCL_RCP_RPT" />
                        </td>
                        <td class="align-right">
                            <a href="#" style="font-size:10px;" id="moreLegend">
                                &gt;&gt; 
                                <sps:label name="MORE" />
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </html:form>
    </body>
</html>
