<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wadm005_DensoUserInformation.js"></script>
        <script type="text/javascript">
            var labelAll = '<sps:label name="ALL_CRITERIA" />';
            var labelUndefined = '<sps:label name="UNDEFINED" />';
            var labelPleaseSelect = '<sps:label name="PLEASE_SELECT_CRITERIA" />';
            var labelDelete = '<sps:label name="DELETE_SELECTED_ITEMS" />';
            var msgConfirmDelete = '<sps:message name="SP-W6-0001"/>';
        </script>
        <title>
            <sps:label name="DENSO_USER_INFORMATION" />
        </title>
    </head>
    <body class="hideHorizontal" style="width:98%">
        <html:form styleId="Wadm005FormCriteria" action="/DensoUserInformationAction.do">
        <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden styleId="method" property="method" />
            <!-- Key for paging. -->
            <html:hidden styleId="currentPage" property="currentPage"/>
            <html:hidden styleId="row"  property="row"/>
            <html:hidden styleId="totalCount" property="totalCount"/>
            <!-- Key for button. -->
            <c:set var="btnSearch">
                <sps:label name="SEARCH" />
            </c:set>
            <c:set var="btnReset">
                <sps:label name="RESET" />
            </c:set>
            <c:set var="btnDownload">
                <sps:label name="DOWNLOAD" />
            </c:set>
            <c:set var="btnDelete">
                <sps:label name="DELETE" />
            </c:set>
            <!-- Parameter for selected row and link.-->
            <html:hidden styleId="userLogin" property="userLogin" />
            <html:hidden property="cannotResetMessage" styleId="cannotResetMessage"/>
            <html:hidden property="cannotDownloadMessage" styleId="cannotDownloadMessage"/>
            
            <logic:notEmpty name="Wadm005Form" property="companyDensoList">
                <c:forEach var="companyDensoItem" items="${Wadm005Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoItem" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wadm005Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wadm005Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            
            <table class="bottom" style="height: 30px; width: 990px;">
                <tr>
                    <td>
                        <sps:label name="ADMIN_CONFIG" />
                        &gt;
                        <sps:label name="DENSO_USER_INFORMATION" />
                        &gt;
                        <sps:label name="DENSO_USER_INFORMATION" />
                    </td>
                    <td colspan="2" class="align-right" >
                        <sps:label name="USER" />
                        &nbsp; : <bean:write name="Wadm005Form" property="userLogin" />
                        <html:hidden name="Wadm005Form" property="userLogin" />
                    </td>
                </tr>
            </table>
            
            <c:choose>
                <c:when test="${not empty Wadm005Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width:992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg" items="${Wadm005Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message}
                                </span>
                                <br/>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
                
            <fieldset class="fixwidth">
                <table style="width:975px; border:0px; cellpadding:3px; cellspacing:1px;" class="display_criteria" >
                    <tr>
                        <th style="width:10%" class="align-right">
                            <sps:label name="DSC_ID" />
                            &nbsp;:
                        </th>
                        <td style="width:160px">
                            <html:text name="Wadm005Form" property="dscId" styleId="dscId" size="15" maxlength="30" style="width:150px"/>
                        </td>
                        <th style="width:11%" class="align-right">
                            <sps:label name="EMPLOYEE_CODE" />
                            &nbsp;:
                        </th>
                        <td style="width:10%">
                            <html:text name="Wadm005Form" property="employeeCode" styleId="employeeCode" size="15" maxlength="30" style="width:120px"/>
                        </td>
                    </tr>
                    <tr>
                        <th >
                            <sps:label name="D_CD" />
                            &nbsp;:
                        </th>
                        <td >
                            <html:select name="Wadm005Form" property="DCd" styleId="DCd" style="width:150px" styleClass="mandatory">
                                <html:option value="ALL">
                                    <sps:label name="ALL_CRITERIA" />
                                </html:option>
                                <c:forEach var="companyDenso" items="${Wadm005Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${companyDenso.DCd}">
                                        ${companyDenso.DCd}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th >
                            <sps:label name="D_P" />
                            &nbsp;:
                        </th>
                        <td >
                            <html:select name="Wadm005Form" property="DPcd" styleId="DPcd" style="width:120px" styleClass="mandatory">
                                <html:option value="ALL">
                                    <sps:label name="ALL_CRITERIA" />
                                </html:option>
                                <html:option value="99">
                                    <sps:label name="UNDEFINED" />
                                </html:option>
                                <c:forEach var="plantDenso" items="${Wadm005Form.plantDensoList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${plantDenso.DPcd}">
                                        ${plantDenso.DPcd}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th style="width:10%">
                            <sps:label name="REGISTER_DATE" />
                            &nbsp;:
                        </th>
                        <td >
                            <sps:calendar name="registerDateFrom" id="registerDateFrom" value="${Wadm005Form.registerDateFrom}" />
                            -
                            <sps:calendar name="registerDateTo" id="registerDateTo" value="${Wadm005Form.registerDateTo}" />
                        </td>
                    </tr>
                </table>
                <table class="display_criteria"  style="width: 975px;border:0px; cellpadding:3px; cellspacing:1px;">
                    <tr>
                        <th style="width:10%">
                            <sps:label name="FIRST_NAME" />
                            &nbsp;:
                        </th>
                        <td >
                            <html:text name="Wadm005Form" property="firstName" styleId="firstName" size="15" maxlength="50" style="width:240px"/>
                        </td>
                        <th >
                            <sps:label name="MIDDLE_NAME" />
                            &nbsp;:
                        </th>
                        <td >
                            <html:text name="Wadm005Form" property="middleName" styleId="middleName" size="15" maxlength="50" style="width:180px"/>
                        </td>
                        <th >
                            <sps:label name="LAST_NAME" />
                            &nbsp;:
                        </th>
                        <td >
                            <html:text name="Wadm005Form" property="lastName" styleId="lastName" size="15" maxlength="50" style="width:220px"/>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <table style="width:994px">
                <tr>
                    <td class="align-left" style="vertical-align:bottom;font-size:10px;">
                        <c:if test="${not empty Wadm005Form.userDensoInfoList}">
                            <sps:label name="SYSTEM_GET_SELECTED_REC_EVERY_PAGE"/>
                        </c:if>
                    </td>
                    <td class="align-right">
                       <input type="button" id="btnSearch"  value="${btnSearch}"  class="select_button" />
                       <input type="button" id="btnReset"   value="${btnReset}"   class="select_button" />
                       <input type="button" id="btnDownload" value="${btnDownload}" class="select_button" />
                    </td>
                </tr>
            </table>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        <html:form styleId="Wadm005FormResult" action="/DensoUserInformationAction.do">
            <html:hidden property="method" styleId="methodResult" value="Search"/>
            <html:hidden property="DCdSelected" styleId="DCdSelected"/>
            <html:hidden property="DPcdSelected" styleId="DPcdSelected"/>
            <html:hidden property="dscIdSelected" styleId="dscIdSelected"/>
            <html:hidden property="firstNameSelected" styleId="firstNameSelected"/>
            <html:hidden property="middleNameSelected" styleId="middleNameSelected"/>
            <html:hidden property="lastNameSelected" styleId="lastNameSelected"/>
            <html:hidden property="countDelete" styleId="countDelete"/>
            <html:hidden property="userLogin"/>
            <!-- Start : Parameter for search criteria.-->
            <html:hidden property="dscId"/>
            <html:hidden property="employeeCode"/>
            <html:hidden property="DCd"/>
            <html:hidden property="DPcd"/>
            <html:hidden property="firstName"/>
            <html:hidden property="middleName"/>
            <html:hidden property="lastName"/>
            <html:hidden property="registerDateFrom"/>
            <html:hidden property="registerDateTo"/>
            <!-- End : Parameter for search criteria.-->
            <html:hidden property="cannotResetMessage"/>
            <html:hidden property="cannotDownloadMessage"/>
            
            <logic:notEmpty name="Wadm005Form" property="companyDensoList">
                <c:forEach var="companyDensoItem" items="${Wadm005Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoItem" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wadm005Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wadm005Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            
            <c:choose>
                <c:when test="${0 == Wadm005Form.max}">
                    <c:set var="visibilityFlag">
                        hidden
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="visibilityFlag">
                        visible
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="result" style="visibility: ${visibilityFlag};">
                <div style="width:98%;height:350px;position:absolute; overflow-x:auto; overflow-y:auto;">
                    <table class="display_data" id="tblBody" style="table-layout:fixed;width:1235px;word-wrap:break-word;">
                        <tr>
                            <th style="width:20px;" scope="col" class="checkbox">
                                <sps:label name="ALL" />
                                <br />
                                <input type="checkbox" name="checkbox" value="checkbox" onclick="densoCheckAll(this);"/>
                            </th>
                            <th style="width:20px;" class="align-center">
                                <sps:label name="NO" />
                            </th>
                            <th style="width:150px;" scope="col">
                                <sps:label name="DSC_ID" />
                            </th>
                            <th style="width:70px;" scope="col">
                                <sps:label name="D_CD" />
                            </th>
                            <th style="width:70px;" scope="col">
                                <sps:label name="D_P" />
                            </th>
                            <th style="width:140px;" scope="col">
                                <sps:label name="DEPARTMENT" />
                            </th>
                            <th style="width:130px;" scope="col" class="align-center">
                                <sps:label name="FIRST_NAME" />
                            </th>
                            <th style="width:100px;" scope="col" class="align-center">
                                <sps:label name="MIDDLE_NAME" />
                            </th>
                            <th style="width:130px;" scope="col" class="align-center">
                                <sps:label name="LAST_NAME" />
                            </th>
                            <th style="width:55px;" class="align-center">
                                <sps:label name="ASSIGN" />
                            </th>
                            <th style="width:110px;" scope="col" class="align-center">
                                <sps:label name="TELEPHONE" />
                            </th>
                            <th style="width:240px;" scope="col" class="align-center">
                                <sps:label name="EMAIL" />
                            </th>
                        </tr>
                        <c:set var="startRow">
                            ${(Wadm005Form.pageNo * Wadm005Form.count) - Wadm005Form.count}
                        </c:set>
                        <c:forEach var="userDenso" items="${Wadm005Form.userDensoInfoList}" begin="0" step="1" varStatus="status">
                        <c:choose>
                            <c:when test="${(status.index+1)% 2 == 0}">
                                <c:set var="rowColor">
                                    blueLight
                                </c:set>
                            </c:when>
                            <c:otherwise>
                                <c:set var="rowColor">
                                    
                                </c:set>
                            </c:otherwise>
                        </c:choose>
                            <tr class="${rowColor}">
                                <td class="align-center" style="width:20px">
                                    <input type="checkbox" name="densoChkbox" id="checkDenso${status.index}" onclick="setDensoList('${status.index}')"
                                        <c:if test="${not empty userDenso.dscIdSelected}">
                                            checked
                                        </c:if>
                                    />
                                    <html:hidden name="userDenso"  property="dscIdSelected" indexed="true" styleId="dscIdSelected${status.index}"/>
                                    <!-- Parameter for selected value -->
                                    <html:hidden name="userDenso" property="dscId" indexed="true" value="${userDenso.dscId}" styleId="dscId${status.index}"/>
                                    <html:hidden name="userDenso" property="lastUpdateDatetimeScreen" indexed="true" value="${userDenso.lastUpdateDatetimeScreen}" styleId="lastUpdateDatetimeScreen${status.index}"/>
                                </td>
                                <td class="align-right">
                                    ${status.index + startRow + 1}
                                </td>
                                <td style="width:12%">
                                    <a href="javascript:void(0)" onclick="return densoDscIdLink('${userDenso.dscId}'); return false;" >
                                        ${userDenso.dscId}
                                    </a>
                                </td>
                                <td style="width:6%">
                                    ${userDenso.DCd}
                                    <html:hidden name="userDenso" property="DCd" indexed="true"/>
                                    <html:hidden name="userDenso" property="DPcd" indexed="true"/>
                                </td>
                                <c:choose>
                                    <c:when test="${'99' == userDenso.DPcd}">
                                        <td class="align-center">
                                            <sps:label name="UNDEFINED" />
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="align-left">
                                            ${userDenso.DPcd}
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${null == userDenso.userDomain.departmentName}">
                                        <td class="align-center">
                                            -
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td style="width:10%">
                                            <bean:write name="userDenso" property="userDomain.departmentName" />
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                                <td style="width:20%">
                                    <bean:write name="userDenso" property="userDomain.firstName" />
                                </td>
                                <c:choose>
                                    <c:when test="${null == userDenso.userDomain.middleName}">
                                        <td class="align-center">
                                            -
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td style="width:20%">
                                            <bean:write name="userDenso" property="userDomain.middleName" />
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                                <td style="width:20%">
                                    <bean:write name="userDenso" property="userDomain.lastName" />
                                </td>
                                <td class="align-left">
                                    <a href="javascript:void(0)" onclick="return densoRoleLink('${userDenso.dscId}'
                                    ,'${userDenso.DCd}'
                                    ,'${userDenso.DPcd}','${userDenso.userDomain.firstName}'
                                    ,'${userDenso.userDomain.middleName}','${userDenso.userDomain.lastName}'); return false;">
                                        <sps:label name="ROLE" />
                                    </a>
                                </td>
                                <c:choose>
                                    <c:when test="${null == userDenso.userDomain.telephone}">
                                        <td class="align-center">
                                            -
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td style="width:25%">
                                            <bean:write name="userDenso" property="userDomain.telephone" />
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                                <td style="width:12%">
                                    ${userDenso.userDomain.email}
                                    <html:hidden name="userDenso" property="userDomain.departmentName" indexed="true"/>
                                    <html:hidden name="userDenso" property="userDomain.firstName" indexed="true"/>
                                    <html:hidden name="userDenso" property="userDomain.middleName" indexed="true"/>
                                    <html:hidden name="userDenso" property="userDomain.lastName" indexed="true"/>
                                    <html:hidden name="userDenso" property="userDomain.telephone" indexed="true"/>
                                    <html:hidden name="userDenso" property="userDomain.email" indexed="true"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div style="width:99%;height:360px">
                    &nbsp;
                </div>
                <table style="width: 994px;">
                    <tr>
                        <td align="left">
                            <ai:paging action="/SPS/DensoUserInformationAction.do" formId="Wadm005FormResult" jump="true" />
                        </td>
                        <td align="right">
                            <input type="button" id="btnDelete" value="${btnDelete}" class="select_button" />
                        </td>
                    </tr>
                </table>
            </div>
        </html:form>
    </body>
</html>
