<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wadm002_SupplierUserRegistration.js">
        </script>
        <script type="text/javascript">
            var labelAll = '<sps:label name="ALL_CRITERIA" />';
            var labelPleaseSelect = '<sps:label name="PLEASE_SELECT_CRITERIA" />';
            var labelUndefined = '<sps:label name="UNDEFINED" />';
            var labelReturn = '<sps:label name="RETURN" />';
            var msgConfirmReturn = '<sps:message name="SP-W6-0001"/>';
            var undefinedValue = '${cons.UNDEFINED_FOR_SUPPLIER}';
        </script>
        <title>
            <sps:label name="SUPPLIER_USER_REGISTRATION" />
        </title>
    </head>
    
    <body class="hideHorizontal">
        <html:form styleId="Wadm002Form" action="/SupplierUserRegistrationAction.do">
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden styleId="method" property="method" />
            <html:hidden name="Wadm002Form" property="isDCompany" styleId="isDCompany" />
            <html:hidden name="Wadm002Form" property="mode" styleId="mode" />
            <!-- Key for button. -->
            <c:set var="btnRegister">
                <sps:label name="REGISTER" />
            </c:set>
            <c:set var="btnReset">
                <sps:label name="RESET" />
            </c:set>
            <c:set var="btnReturn">
                <sps:label name="RETURN" />
            </c:set>
            <!-- Parameter for selected row and link.-->
            <html:hidden property="isReset" styleId="isReset" />
            <html:hidden property="emlInfoFlag" styleId="emlInfoFlag" />
            <html:hidden property="emlUrgentFlag" styleId="emlUrgentFlag" />
            <html:hidden property="emlAllowReviseFlag" styleId="emlAllowReviseFlag"/>
            <html:hidden property="emlCancelInvoiceFlag" styleId="emlCancelInvoiceFlag"/>
            <html:hidden property="maxDepartment" styleId="maxDepartment" />
            <html:hidden property="lastUpdateDatetimeUser" styleId="lastUpdateDatetimeUser" value="${Wadm002Form.lastUpdateDatetimeUser}"/>
            <html:hidden property="lastUpdateDatetimeSupplier" styleId="lastUpdateDatetimeSupplier" value="${Wadm002Form.lastUpdateDatetimeSupplier}"/>
            <html:hidden property="maxVendorCd" styleId="maxVendorCd" value="${Wadm002Form.maxVendorCd}"/>
            <html:hidden styleId="cannotResetMessage" property="cannotResetMessage"/>
            
            <%-- <c:forEach var="department" items="${Wadm002Form.departmentNameList}" step="1" varStatus="status">
                <input type="hidden" name="department" indexed="true" id="dept${status.index}" value="${department}"/>
            </c:forEach> --%>
            <%-- <logic:notEmpty name="Wadm002Form" property="vendorCdList">
                <c:forEach var="vendorCode" items="${Wadm002Form.vendorCdList}" begin="0" step="1" varStatus="status">
                    <input type="hidden" name="vendorCode" indexed="true" id="vendorCode${status.index}"
                        value="${vendorCode.vendorCd}"/>
                    <input type="hidden" name="companyDensoCode" indexed="true" id="companyDensoCode${status.index}"
                        value="${vendorCode.DCd}"/>
                </c:forEach>
            </logic:notEmpty> --%>
            <c:forEach var="companyDensoItem" items="${Wadm002Form.DCdList}" begin="0" step="1" varStatus="status">
                <html:hidden name="companyDensoItem" property="DCd" indexed="true"/>
            </c:forEach>
            <c:forEach var="companySupplierItem" items="${Wadm002Form.companySupplierList}" begin="0" step="1" varStatus="status">
                <html:hidden name="companySupplierItem" property="SCd" indexed="true"/>
                <html:hidden name="companySupplierItem" property="companyName" indexed="true"/>
            </c:forEach>
            <logic:notEmpty name="Wadm002Form" property="vendorCdList">
                <c:forEach var="vendorCodeItem" items="${Wadm002Form.vendorCdList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="vendorCodeItem" property="vendorCd" value="${vendorCodeItem.vendorCd}" styleId="vendorCode${status.index}" indexed="true"/>
                    <html:hidden name="vendorCodeItem" property="DCd" value="${vendorCodeItem.DCd}" styleId="companyDensoCode${status.index}" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <c:forEach var="departmentItem" items="${Wadm002Form.departmentList}" begin="0" step="1" varStatus="status">
                <html:hidden name="departmentItem" property="departmentName" value="${departmentItem.departmentName}" styleId="dept${status.index}" indexed="true"/>
            </c:forEach>
            
            <table style="height:30px; border:0px; cellpadding:0px; cellspacing:0px; width:990px;" class="bottom">
                <tr>
                    <td>
                        <sps:label name="ADMIN_CONFIG" />
                        &gt;
                        <sps:label name="SUPPLIER_USER_INFORMATION" />
                        &gt;
                        <c:choose>
                            <c:when test="${cons.MODE_REGISTER == Wadm002Form.mode}">
                                <sps:label name="SUPPLIER_USER_REGISTRATION" />
                            </c:when>
                            <c:otherwise>
                                <sps:label name="SUPPLIER_USER_INFORMATION" />
                                &gt;
                                <sps:label name="SUPPLIER_USER_REGISTRATION" />
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td colspan="2" class="align-right">
                        <sps:label name="USER" />
                        &nbsp; : 
                        <bean:write name="Wadm002Form" property="userLogin" />
                        <html:hidden name="Wadm002Form" property="userLogin" />
                    </td>
                </tr>
            </table>
            <c:choose>
                <c:when test="${not empty Wadm002Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}" >
                <table style="width: 992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg" items="${Wadm002Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}" >
                                    ${appMsg.message} 
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            <c:choose>
                <c:when test="${cons.MODE_REGISTER == Wadm002Form.mode}">
                    <fieldset class="fixwidth">
                        <table class="display_criteria" style="width:975px; border:0px; cellpadding:3px; cellspacing:1px;">
                            <tr>
                                <th style="width:86px;">
                                    <sps:label name="DSC_ID" />
                                    &nbsp;:
                                </th>
                                <td style="width:330px;">
                                    <html:text name="Wadm002Form" property="dscId" styleId="dscId" size="15" style="width: 320px" styleClass="mandatory"/>
                                </td>
                                <th style="width:108px;">
                                    <sps:label name="DENSO_OWNER" />
                                    &nbsp;:
                                </th>
                                <td style="width:176px;">
                                    <html:select name="Wadm002Form" property="densoOwner" styleClass="mandatory" style="width: 171px">
                                        <%-- [IN052] Set --Please Select-- as default. --%>
                                        <html:option value="">
                                            <sps:label name="PLEASE_SELECT_CRITERIA" />
                                        </html:option>
                                    
                                        <c:forEach var="companyDenso" items="${Wadm002Form.DCdList}" begin="0" step="1" varStatus="status">
                                            <html:option value="${companyDenso.DCd}">
                                                ${companyDenso.DCd}
                                            </html:option>
                                        </c:forEach>
                                    </html:select>
                                </td>
                                <th style="width:98px" class="align-right">
                                    <sps:label name="DEPARTMENT" />
                                    &nbsp;:
                                </th>
                                <td style="width:176px;" class="align-left">
                                    <html:text name="Wadm002Form" property="departmentName" styleId="departmentName" style="width: 169px" />
                                </td>
                            </tr>
                            <tr>
                                <th class="align-right">
                                    <sps:label name="S_COMPANY_NAME" />
                                    &nbsp;:
                                </th>
                                <td colspan="3">
                                    <html:select name="Wadm002Form" property="SCd" styleId="SCd" styleClass="mandatory" style="width: 600px">
                                        <%-- [IN052] Set --Please Select-- as default. --%>
                                        <html:option value="">
                                            <sps:label name="PLEASE_SELECT_CRITERIA" />
                                        </html:option>
                                    
                                        <c:forEach var="companySupplier" items="${Wadm002Form.companySupplierList}" begin="0" step="1" varStatus="status">
                                            <html:option value="${companySupplier.SCd}">
                                                ${companySupplier.SCd} - ${companySupplier.companyName}
                                            </html:option>
                                        </c:forEach>
                                    </html:select>
                                </td>
                                <th class="align-right">
                                    <sps:label name="S_P" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <html:select name="Wadm002Form" property="SPcd" styleId="SPcd" styleClass="mandatory" style="width: 171px">
                                        <%-- [IN052] Do not set Undefined as default Supplier Plant. --%>
                                        <%--
                                            <html:option value="${cons.UNDEFINED_FOR_SUPPLIER}">
                                                <sps:label name="UNDEFINED" />
                                            </html:option>
                                        --%>
                                        <html:option value="">
                                            <sps:label name="PLEASE_SELECT_CRITERIA" />
                                        </html:option>
                                        
                                        <c:forEach var="plantSupplier" items="${Wadm002Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                                            <html:option value="${plantSupplier.SPcd}">
                                                ${plantSupplier.SPcd}
                                            </html:option>
                                        </c:forEach>
                                        
                                        <%-- [IN052] Do not set Undefined as default Supplier Plant. --%>
                                        <html:option value="${cons.UNDEFINED_FOR_SUPPLIER}">
                                            <sps:label name="UNDEFINED" />
                                        </html:option>
                                        
                                    </html:select>
                                </td>
                                
                            </tr>
                            <tr>
                                <th rowspan="3" class="align-right">
                                    <sps:label name="S_CD" />
                                    &nbsp;:
                                </th>
                                <td rowspan="3">
                                    <html:textarea name="Wadm002Form" property="vendorCd" styleId="vendorCd" cols="80" 
                                        style="width:320px; height:64px" readonly="true">
                                    </html:textarea>
                                </td>
                                <th class="align-right">
                                    <sps:label name="FIRST_NAME" />
                                    &nbsp;:
                                </th>
                                <td class="align-left">
                                    <html:text name="Wadm002Form" property="firstName" styleId="firstName" size="15"
                                    maxlength="50" style="width: 169px" styleClass="mandatory"/>
                                </td>
                                <th class="align-right">
                                    <sps:label name="MIDDLE_NAME" />
                                    &nbsp;:
                                </th>
                                <td class="align-left">
                                    <html:text name="Wadm002Form" property="middleName" styleId="middleName" size="15"
                                    maxlength="50" style="width: 169px" />
                                </td>
                        </tr>
                            <tr>
                                <th class="align-right">
                                    <sps:label name="LAST_NAME" />
                                    &nbsp;:
                                </th>
                                <td class="align-left">
                                    <html:text name="Wadm002Form" property="lastName" styleId="lastName" size="15" maxlength="50"
                                    style="width: 169px" styleClass="mandatory"/>
                                </td>
                                <th class="align-right">
                                    <sps:label name="TELEPHONE" />
                                    &nbsp;:
                                </th>
                                <td class="align-left">
                                    <html:text name="Wadm002Form" property="telephone" styleId="telephone" style="width: 169px" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <sps:label name="EMAIL" />
                                    &nbsp;:
                                </th>
                                <td colspan="3">
                                    <html:text name="Wadm002Form" property="email" styleId="email" styleClass="mandatory"
                                    style="width: 445px" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <sps:label name="EMAIL_FOR" />
                                    &nbsp;:
                                </th>
                                <td colspan="4" style="font-weight:bold">
                                    <c:if test="${Wadm002Form.isDCompany == true}">
                                        <c:if test="${'1' == Wadm002Form.emlUrgentFlag}">
                                            <input type="checkbox" name="emlUrgentchk" id="emlUrgentchk" checked="checked" />
                                        </c:if>
                                        <c:if test="${'0' == Wadm002Form.emlUrgentFlag}">
                                            <input type="checkbox" name="emlUrgentchk" id="emlUrgentchk" />
                                        </c:if>
                                        <%-- [IN054] Change caption for email flag Urgent Order --%>
                                        <%-- <sps:label name="URGENT_ORDER" /> --%>
                                        <sps:label name="URGENT_ORDER_DENSO_REPLY" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;<br />
                                        <c:if test="${'1' == Wadm002Form.emlInfoFlag}">
                                            <input type="checkbox" name="emlInfochk" id="emlInfochk" checked="checked" />
                                        </c:if>
                                        <c:if test="${'0' == Wadm002Form.emlInfoFlag}">
                                            <input type="checkbox" name=emlInfochk id="emlInfochk" />
                                        </c:if>
                                        <sps:label name="SUPPLIER_INFORMATION_NOTFOUND" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;<br />
                                        <input type="checkbox" id="emlAllowReviseChg"
                                            <c:if test="${'1' == Wadm002Form.emlAllowReviseFlag}">
                                                checked
                                            </c:if>
                                        />
                                        <sps:label name="ALLOW_REVISE" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;<br />
                                        <input type="checkbox" id="emlCancelInvoiceChg"
                                            <c:if test="${'1' == Wadm002Form.emlCancelInvoiceFlag}">
                                                checked
                                            </c:if>
                                        />
                                        <sps:label name="CANCEL_INVOICE" />
                                    </c:if>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </c:when>
                <c:otherwise>
                    <fieldset class="fixwidth">
                        <table class="display_criteria" style="border:0px; cellpadding:3px; cellspacing:1px; width:975px;">
                            <tr>
                                <th style="width:86px;" class="align-right">
                                    <sps:label name="DSC_ID" />
                                    &nbsp;:
                                </th>
                                <td style="width:330px;">
                                    <html:text name="Wadm002Form" property="dscId" styleId="dscId" value="${Wadm002Form.dscId}"
                                    style="width: 320px" readonly="true" />
                                </td>
                                <th style="width:108px;">
                                    <sps:label name="DENSO_OWNER" />
                                    &nbsp;:
                                </th>
                                <td style="width:176px;">
                                    <c:if test="${Wadm002Form.isDCompany == true}">
                                        <html:select name="Wadm002Form" property="densoOwner" styleClass="mandatory" style="width: 171px">
                                            <%-- [IN052] Set --Please Select-- as default. --%>
                                            <html:option value="">
                                                <sps:label name="PLEASE_SELECT_CRITERIA" />
                                            </html:option>
                                    
                                            <c:forEach var="companyDenso" items="${Wadm002Form.DCdList}" begin="0" step="1" varStatus="status">
                                                <html:option value="${companyDenso.DCd}">
                                                    ${companyDenso.DCd}
                                                </html:option>
                                            </c:forEach>
                                        </html:select>
                                    </c:if>
                                    <c:if test="${Wadm002Form.isDCompany == false}">
                                        <html:select name="Wadm002Form" property="densoOwner" styleClass="mandatory" style="width: 171px" disabled="true">
                                            <%-- [IN052] Set --Please Select-- as default. --%>
                                            <html:option value="">
                                                <sps:label name="PLEASE_SELECT_CRITERIA" />
                                            </html:option>
                                    
                                            <c:forEach var="companyDenso" items="${Wadm002Form.DCdList}" begin="0" step="1" varStatus="status">
                                                <html:option value="${companyDenso.DCd}">
                                                    ${companyDenso.DCd}
                                                </html:option>
                                            </c:forEach>
                                        </html:select>
                                    </c:if>
                                </td>
                                <th style="width:98px;" class="align-right">
                                    <sps:label name="DEPARTMENT" />
                                    &nbsp;:
                                </th>
                                <td style="width: 176px;" class="align-left">
                                    <c:if test="${Wadm002Form.isDCompany == true}">
                                        <html:text name="Wadm002Form" property="departmentName" styleId="departmentName"
                                             value="${Wadm002Form.departmentName}" style="width: 169px;" />
                                    </c:if>
                                    <c:if test="${Wadm002Form.isDCompany == false}">
                                        <html:text name="Wadm002Form" property="departmentName" styleId="departmentName"
                                             value="${Wadm002Form.departmentName}" style="width: 169px" readonly="true"/>
                                    </c:if>
                                </td>
                            </tr>
                            <tr>
                                <th class="align-right">
                                    <sps:label name="S_COMPANY_NAME" />
                                    &nbsp;:
                                </th>
                                <td colspan="3">
                                    <c:if test="${Wadm002Form.isDCompany == true}">
                                        <html:select name="Wadm002Form" property="SCd" styleId="SCd" styleClass="mandatory" 
                                        style="width: 600px" value="${Wadm002Form.SCd}" >
                                            <%-- [IN052] Set --Please Select-- as default. --%>
                                            <html:option value="">
                                            <sps:label name="PLEASE_SELECT_CRITERIA" />
                                            </html:option>
                                    
                                            <c:forEach var="companySupplier" items="${Wadm002Form.companySupplierList}" begin="0" step="1" varStatus="status">
                                                <html:option value="${companySupplier.SCd}">
                                                    ${companySupplier.SCd} - ${companySupplier.companyName}
                                                </html:option>
                                            </c:forEach>
                                        </html:select>
                                    </c:if>
                                    <c:if test="${Wadm002Form.isDCompany == false}">
                                        <html:select name="Wadm002Form" property="SCd" styleId="SCd" styleClass="mandatory" 
                                        style="width: 606px" value="${Wadm002Form.SCd}" disabled="true">
                                            <%-- [IN052] Set --Please Select-- as default. --%>
                                            <html:option value="">
                                                <sps:label name="PLEASE_SELECT_CRITERIA" />
                                            </html:option>
                                    
                                            <c:forEach var="companySupplier" items="${Wadm002Form.companySupplierList}" begin="0" step="1" varStatus="status">
                                                <html:option value="${companySupplier.SCd}">
                                                    ${companySupplier.SCd} - ${companySupplier.companyName}
                                                </html:option>
                                            </c:forEach>
                                        </html:select>
                                    </c:if>
                                </td>
                                <th>
                                    <sps:label name="S_P" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <c:if test="${Wadm002Form.isDCompany == true}">
                                        <html:select name="Wadm002Form" property="SPcd" styleId="SPcd" styleClass="mandatory"
                                            style="width: 171px" value="${Wadm002Form.SPcd}" >
                                            <%-- [IN052] Do not set Undefined as default Supplier Plant. --%>
                                            <%--
                                                <html:option value="${cons.UNDEFINED_FOR_SUPPLIER}">
                                                    <sps:label name="UNDEFINED" />
                                                </html:option>
                                            --%>
                                            <html:option value="">
                                                <sps:label name="PLEASE_SELECT_CRITERIA" />
                                            </html:option>
                                        
                                            <c:forEach var="plantSupplier" items="${Wadm002Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                                                <html:option value="${plantSupplier.SPcd}">
                                                    ${plantSupplier.SPcd}
                                                </html:option>
                                            </c:forEach>
                                            
                                            <%-- [IN052] Do not set Undefined as default Supplier Plant. --%>
                                            <html:option value="${cons.UNDEFINED_FOR_SUPPLIER}">
                                                <sps:label name="UNDEFINED" />
                                            </html:option>
                                            
                                        </html:select>
                                    </c:if>
                                    <c:if test="${Wadm002Form.isDCompany == false}">
                                        <html:select name="Wadm002Form" property="SPcd" styleId="SPcd" styleClass="mandatory"
                                            style="width: 171px" value="${Wadm002Form.SPcd}" disabled="true">
                                            <%-- [IN052] Do not set Undefined as default Supplier Plant. --%>
                                            <%--
                                                <html:option value="${cons.UNDEFINED_FOR_SUPPLIER}">
                                                    <sps:label name="UNDEFINED" />
                                                </html:option>
                                            --%>
                                            <html:option value="">
                                                <sps:label name="PLEASE_SELECT_CRITERIA" />
                                            </html:option>
                                        
                                            <c:forEach var="plantSupplier" items="${Wadm002Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                                                <html:option value="${plantSupplier.SPcd}">
                                                    ${plantSupplier.SPcd}
                                                </html:option>
                                            </c:forEach>
                                            
                                            <%-- [IN052] Do not set Undefined as default Supplier Plant. --%>
                                            <html:option value="${cons.UNDEFINED_FOR_SUPPLIER}">
                                                <sps:label name="UNDEFINED" />
                                            </html:option>
                                            
                                        </html:select>
                                    </c:if>
                                </td>
                            </tr>
                            <tr>
                                <th rowspan="3" class="align-right">
                                    <sps:label name="S_CD" />
                                    &nbsp;:
                                </th>
                                <td rowspan="3">
                                    <html:textarea name="Wadm002Form" property="vendorCd" styleId="vendorCd" cols="80" 
                                        style="width:320px; height:64px" readonly="true">
                                        
                                    </html:textarea>
                                </td>
                                <th class="align-right">
                                    <sps:label name="FIRST_NAME" />
                                    &nbsp;:
                                </th>
                                <td class="align-left">
                                    <c:if test="${Wadm002Form.isDCompany == true}">
                                        <html:text name="Wadm002Form" property="firstName" styleId="firstName"
                                    value="${Wadm002Form.firstName}" style="width: 169px" styleClass="mandatory" />
                                    </c:if>
                                    <c:if test="${Wadm002Form.isDCompany == false}">
                                        <html:text name="Wadm002Form" property="firstName" styleId="firstName"
                                    value="${Wadm002Form.firstName}" style="width: 169px" styleClass="mandatory" readonly="true"/>
                                    </c:if>
                                </td>
                                <th class="align-right">
                                    <sps:label name="MIDDLE_NAME" />
                                    &nbsp;:
                                </th>
                                <td class="align-left">
                                    <c:if test="${Wadm002Form.isDCompany == true}">
                                        <html:text name="Wadm002Form" property="middleName" styleId="middleName"
                                    value="${Wadm002Form.middleName}" style="width: 169px" />
                                    </c:if>
                                    <c:if test="${Wadm002Form.isDCompany == false}">
                                        <html:text name="Wadm002Form" property="middleName" styleId="middleName"
                                    value="${Wadm002Form.middleName}" style="width: 169px" readonly="true"/>
                                    </c:if>
                                </td>
                            </tr>
                            <tr>
                                <th class="align-right">
                                    <sps:label name="LAST_NAME" />
                                    &nbsp;:
                                </th>
                                <td class="align-left">
                                    <c:if test="${Wadm002Form.isDCompany == true}">
                                        <html:text name="Wadm002Form" property="lastName" styleId="lastName"
                                    value="${Wadm002Form.lastName}" style="width: 169px" styleClass="mandatory" />
                                    </c:if>
                                    <c:if test="${Wadm002Form.isDCompany == false}">
                                        <html:text name="Wadm002Form" property="lastName" styleId="lastName"
                                    value="${Wadm002Form.lastName}" style="width: 169px" styleClass="mandatory" readonly="true"/>
                                    </c:if>
                                    
                                </td>
                                <th class="align-right">
                                    <sps:label name="TELEPHONE" />
                                    &nbsp;:
                                </th>
                                <td class="align-left">
                                    <c:if test="${Wadm002Form.isDCompany == true}">
                                        <html:text name="Wadm002Form" property="telephone" styleId="telephone"
                                        value="${Wadm002Form.telephone}" style="width: 169px" />
                                    </c:if>
                                    <c:if test="${Wadm002Form.isDCompany == false}">
                                        <html:text name="Wadm002Form" property="telephone" styleId="telephone"
                                        value="${Wadm002Form.telephone}" style="width: 169px" readonly="true"/>
                                    </c:if>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <sps:label name="EMAIL" />
                                    &nbsp;:
                                </th>
                                <td colspan="3">
                                    <c:if test="${Wadm002Form.isDCompany == true}">
                                        <html:text name="Wadm002Form" property="email" styleId="email" value="${Wadm002Form.email}"
                                    styleClass="mandatory" style="width: 450px" />
                                    </c:if>
                                    <c:if test="${Wadm002Form.isDCompany == false}">
                                        <html:text name="Wadm002Form" property="email" styleId="email" value="${Wadm002Form.email}"
                                    styleClass="mandatory" style="width: 450px" readonly="true"/>
                                    </c:if>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <sps:label name="EMAIL_FOR" />
                                    &nbsp;:
                                </th>
                                <td colspan="4" style="font-weight:bold">
                                    <c:if test="${Wadm002Form.isDCompany == true}">
                                        <c:if test="${'1' == Wadm002Form.emlUrgentFlag}">
                                            <input type="checkbox" name="emlUrgentchk" id="emlUrgentchk" checked="checked" />
                                        </c:if>
                                        <c:if test="${'0' == Wadm002Form.emlUrgentFlag}">
                                            <input type="checkbox" name="emlUrgentchk" id="emlUrgentchk" />
                                        </c:if>
                                        <%-- [IN054] Change caption for email flag Urgent Order --%>
                                        <%-- <sps:label name="URGENT_ORDER" /> --%>
                                        <sps:label name="URGENT_ORDER_DENSO_REPLY" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;<br />
                                        <c:if test="${'1' == Wadm002Form.emlInfoFlag}">
                                            <input type="checkbox" name="emlInfochk" id="emlInfochk" checked="checked" />
                                        </c:if>
                                        <c:if test="${'0' == Wadm002Form.emlInfoFlag}">
                                            <input type="checkbox" name=emlInfochk id="emlInfochk" />
                                        </c:if>
                                        <sps:label name="SUPPLIER_INFORMATION_NOTFOUND" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;<br />
                                        <input type="checkbox" id="emlAllowReviseChg"
                                            <c:if test="${'1' == Wadm002Form.emlAllowReviseFlag}">
                                                checked
                                            </c:if>
                                        />
                                        <sps:label name="ALLOW_REVISE" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;<br /> 
                                        <input type="checkbox" id="emlCancelInvoiceChg"
                                            <c:if test="${'1' == Wadm002Form.emlCancelInvoiceFlag}">
                                                checked
                                            </c:if>
                                        />
                                        <sps:label name="CANCEL_INVOICE" />
                                    </c:if>
                                    <c:if test="${Wadm002Form.isDCompany == false}">
                                       <c:if test="${'1' == Wadm002Form.emlUrgentFlag}">
                                            <input type="checkbox" name="emlUrgentchk" id="emlUrgentchk" checked="checked" disabled="disabled"/>
                                        </c:if>
                                        <c:if test="${'0' == Wadm002Form.emlUrgentFlag}">
                                            <input type="checkbox" name="emlUrgentchk" id="emlUrgentchk" disabled="disabled"/>
                                        </c:if>
                                        <%-- [IN054] Change caption for email flag Urgent Order --%>
                                        <%-- <sps:label name="URGENT_ORDER" /> --%>
                                        <sps:label name="URGENT_ORDER_DENSO_REPLY" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;<br />
                                        <c:if test="${'1' == Wadm002Form.emlInfoFlag}">
                                            <input type="checkbox" name="emlInfochk" id="emlInfochk" checked="checked" disabled="disabled"/>
                                        </c:if>
                                        <c:if test="${'0' == Wadm002Form.emlInfoFlag}">
                                            <input type="checkbox" name=emlInfochk id="emlInfochk" disabled="disabled"/>
                                        </c:if>
                                        <sps:label name="SUPPLIER_INFORMATION_NOTFOUND" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;<br />
                                        <c:if test="${'1' == Wadm002Form.emlAllowReviseChg}">
                                            <input type="checkbox" checked="checked" disabled="disabled"/>
                                        </c:if>
                                        <c:if test="${'0' == Wadm002Form.emlAllowReviseChg}">
                                            <input type="checkbox" disabled="disabled"/>
                                        </c:if>
                                        <sps:label name="ALLOW_REVISE" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;<br />
                                        <c:if test="${'1' == Wadm002Form.emlCancelInvoiceFlag}">
                                            <input type="checkbox" checked="checked" disabled="disabled"/>
                                        </c:if>
                                        <c:if test="${'0' == Wadm002Form.emlCancelInvoiceFlag}">
                                            <input type="checkbox" disabled="disabled"/>
                                        </c:if>
                                        <sps:label name="CANCEL_INVOICE" />
                                    </c:if>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </c:otherwise>
            </c:choose>
            <div id="button">
                <table  style="width:992px; padding-top: 4px; padding-bottom: 2px; cellspacing:0px;">
                    <tr>
                        <td align="right">
                            <c:if test="${Wadm002Form.isDCompany == true}">
                                <input name="Input" type="button" value="${btnRegister}" id="btnRegister" class="select_button" />
                                <input name="Input" type="button" value="${btnReset}"    id="btnReset"    class="select_button" />
                            </c:if>
                            <c:if test="${Wadm002Form.isDCompany == false}">
                                <input name="Input" type="button" value="${btnRegister}" id="btnRegister" class="select_button" disabled="disabled"/>
                                <input name="Input" type="button" value="${btnReset}"    id="btnReset"    class="select_button" disabled="disabled"/>
                            </c:if>
                            <c:choose>
                                <c:when test="${cons.MODE_REGISTER == Wadm002Form.mode}">
                                    <input name="Input" type="button" value="${btnReturn}"   id="btnReturn"
                                    class="select_button" disabled="disabled"/>
                                </c:when>
                                <c:otherwise>
                                    <input name="Input" type="button" value="${btnReturn}"   id="btnReturn"
                                    class="select_button" />
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </table>
            </div>
        </html:form>
    </body>
</html>