<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wadm008_Popup.js">
        </script>
        <script type="text/javascript">
            var labelPleaseSelect = '<sps:label name="PLEASE_SELECT_CRITERIA" />';
            var labelUndefined = '<sps:label name="UNDEFINED" />';
        </script>
        <title>
            <sps:label name="ROLE_ASSIGNMENT" />
        </title>
    </head>
    <body class="hideHorizontal" style="width:400px;height:270px;">
        <html:form styleId="Wadm008Form" action="/DensoUserRoleAssignmentAction.do">
            <html:hidden styleId="method" property="method" />
            <html:hidden styleId="cannotOkMessage" property="cannotOkMessage" />
            <html:hidden styleId="cannotCancelMessage" property="cannotCancelMessage" />
            <!-- Key for button. -->
            <c:set var="btnOK">
                <sps:label name="OK" />
            </c:set>
            <c:set var="btnCancel">
                <sps:label name="CANCEL" />
            </c:set>
            <!-- Parameter for selected row and link.-->
            <html:hidden styleId="mode" property="mode" />
            <html:hidden styleId="dscId" property="dscId"/>
            <html:hidden styleId="roleNameSelected" property="roleNameSelected"/>
            <table style="width:400px;">
                <tr style="height: 50px">
                    <td background="css/images/bgHeader.jpg">
                        <img src="css/images/denso_logo.jpg" style="width:143px; height:20px;"
                        alt="${logoDenso}" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="font-weight:bold">
                            <sps:label name="ROLE_ASSIGNMENT" />
                        </div>
                        
                        <c:choose>
                            <c:when test="${not empty Wadm008Form.applicationMessageList}">
                                <c:set var="displayErrorMessage">
                                    
                                </c:set>
                            </c:when>
                            <c:otherwise>
                                <c:set var="displayErrorMessage">
                                    display: none;
                                </c:set>
                            </c:otherwise>
                        </c:choose>
                        <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                            <table style="width:400px;" class="tableMessage">
                                <tr>
                                    <td id="errorTotal">
                                        <c:forEach var="appMsg" items="${Wadm008Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                            <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                                <c:set var="messageClass">
                                                    ui-messageSuccess
                                                </c:set>
                                            </c:if>
                                            <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                                <c:set var="messageClass">
                                                    ui-messageError
                                                </c:set>
                                            </c:if>
                                            <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                                <c:set var="messageClass">
                                                    ui-messageWarning
                                                </c:set>
                                            </c:if>
                                            <span class="${messageClass}">
                                                ${appMsg.message}
                                            </span>
                                            <br/>
                                        </c:forEach>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <fieldset style="margin-top: 5px;">
                            <table style="width:100%; border:0px; cellpadding:3px; cellspacing:1px;" class="display_criteria" >
                                <c:choose>
                                    <c:when test="${cons.MODE_REGISTER == Wadm008Form.mode}">
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="ROLE_NAME" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%" class="align-left">
                                                <html:select name="Wadm008Form" property="roleCode" styleId="roleCode" styleClass="mandatory" style="width:150px;">
                                                    <c:forEach var="role" items="${Wadm008Form.roleList}" begin="0" step="1" varStatus="status">
                                                        <html:option value="${role.roleCd}">
                                                            ${role.roleName}
                                                        </html:option>
                                                    </c:forEach>
                                                </html:select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="EFFECTIVE_START" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%" class="align-left">
                                                <sps:calendar name="effectStart" id="effectStart" value="" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="EFFECTIVE_END" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%" class="align-left">
                                                <sps:calendar name="effectEnd" id="effectEnd" value="" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="D_CD" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%">
                                                <html:select name="Wadm008Form" property="DCd" styleId="DCd" style="width:150px" styleClass="mandatory">
                                                    <c:forEach var="companyDenso" items="${Wadm008Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                                        <html:option value="${companyDenso.DCd}">
                                                            ${companyDenso.DCd}
                                                        </html:option>
                                                    </c:forEach>
                                                </html:select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="D_P" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%">
                                                <html:select name="Wadm008Form" property="DPcd" styleId="DPcd" style="width:150px" styleClass="mandatory">
                                                    <!-- [IN052] Undefined must not set as default -->
                                                    <!--
                                                    <html:option value="99">
                                                        <sps:label name="UNDEFINED" />
                                                    </html:option>
                                                    -->
                                                    <html:option value="">
                                                        <sps:label name="PLEASE_SELECT_CRITERIA" />
                                                    </html:option>
                                                    
                                                    <c:forEach var="plantDenso" items="${Wadm008Form.plantDensoList}" begin="0" step="1" varStatus="status">
                                                        <html:option value="${plantDenso.DPcd}">
                                                            ${plantDenso.DPcd}
                                                        </html:option>
                                                    </c:forEach>
                                                    <!-- [IN052] Undefined must not set as default -->
                                                    <html:option value="99">
                                                        <sps:label name="UNDEFINED" />
                                                    </html:option>
                                                </html:select>
                                            </td>
                                        </tr>
                                    </c:when>
                                    <c:otherwise>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="ROLE_NAME" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%" class="align-left">
                                                <html:select name="Wadm008Form" property="roleCode" styleId="roleCode" styleClass="mandatory" style="width:150px;">
                                                    <c:forEach var="role" items="${Wadm008Form.roleList}" begin="0" step="1" varStatus="status">
                                                        <html:option value="${role.roleCd}">
                                                            ${role.roleName}
                                                        </html:option>
                                                    </c:forEach>
                                                </html:select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="EFFECTIVE_START" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%" class="align-left">
                                                <sps:calendar name="effectStart" id="effectStart" value="${Wadm008Form.effectStart}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="EFFECTIVE_END" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%" class="align-left">
                                                <sps:calendar name="effectEnd" id="effectEnd" value="${Wadm008Form.effectEnd}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="D_CD" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%">
                                                <html:select name="Wadm008Form" property="DCd" styleId="DCd" style="width:150px" styleClass="mandatory">
                                                    <c:forEach var="companyDenso" items="${Wadm008Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                                        <html:option value="${companyDenso.DCd}">
                                                            ${companyDenso.DCd}
                                                        </html:option>
                                                    </c:forEach>
                                                </html:select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="D_P" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%">
                                                <html:select name="Wadm008Form" property="DPcd" styleId="DPcd" style="width:150px" styleClass="mandatory">
                                                    <!-- [IN052] Undefined must not set as default -->
                                                    <!--
                                                    <html:option value="99">
                                                        <sps:label name="UNDEFINED" />
                                                    </html:option>
                                                    -->
                                                    <html:option value="">
                                                        <sps:label name="PLEASE_SELECT_CRITERIA" />
                                                    </html:option>
                                                    
                                                    <c:forEach var="plantDenso" items="${Wadm008Form.plantDensoList}" begin="0" step="1" varStatus="status">
                                                        <html:option value="${plantDenso.DPcd}">
                                                            ${plantDenso.DPcd}
                                                        </html:option>
                                                    </c:forEach>
                                                    <!-- [IN052] Undefined must not set as default -->
                                                    <html:option value="99">
                                                        <sps:label name="UNDEFINED" />
                                                    </html:option>
                                                </html:select>
                                            </td>
                                        </tr>
                                    </c:otherwise>
                                </c:choose>
                            </table>
                        </fieldset>
                        <table style="width:100%;">
                            <tr>
                                <td colspan="2" class="align-right">
                                    <input type="button" name="Input" id="btnOK"      value="${btnOK}"     class="select_button" />
                                    <input type="button" name="Input" id="btnCancel"  value="${btnCancel}" class="select_button" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </html:form>
    </body>
</html>