<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wadm001_SupplierUserInformation.js" >
        </script>
        <script type="text/javascript">
            var labelAll = '<sps:label name="ALL_CRITERIA" />';
            var labelUndefined = '<sps:label name="UNDEFINED" />';
            var labelDelete = '<sps:label name="DELETE_SELECTED_ITEMS" />';
            var labelPleaseSelect = '<sps:label name="PLEASE_SELECT_CRITERIA" />';
            var msgConfirmDelete = '<sps:message name="SP-W6-0001"/>';
            var undefinedValue = '${cons.UNDEFINED_FOR_SUPPLIER}';
        </script>
        <title>
            <sps:label name="SUPPLIER_USER_INFORMATION" />
        </title>
    </head>
    <body class="hideHorizontal" style="width:98%">
        <html:form styleId="Wadm001FormCriteria" action="/SupplierUserInformationAction.do" >
        <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
        <html:hidden styleId="method" property="method" />
        <html:hidden property="mode" styleId="mode"/>
        <!-- Key for button. -->
        <c:set var="btnSearch">
            <sps:label name="SEARCH" />
        </c:set>
        <c:set var="btnReset">
            <sps:label name="RESET" />
        </c:set>
        <c:set var="btnDownload">
            <sps:label name="DOWNLOAD" />
        </c:set>
        <c:set var="btnDelete">
            <sps:label name="DELETE" />
        </c:set>
        <!-- Key for paging. -->
        <html:hidden styleId="currentPage" property="currentPage"/>
        <html:hidden styleId="row"  property="row"/>
        <html:hidden styleId="totalCount" property="totalCount"/>
        <!-- Parameter for selected row and link.-->
        <html:hidden property="dscIdSelected"/>
        <html:hidden styleId="mode" property="mode"/>
        <html:hidden styleId="userLogin" property="userLogin" />
        <html:hidden property="SCdSelected" />
        <html:hidden property="SPcdSelected" />
        <html:hidden property="firstNameSelected" />
        <html:hidden property="middleNameSelected" />
        <html:hidden property="lastNameSelected" />
        <html:hidden property="companyName" />
        <html:hidden styleId="cannotResetMessage" property="cannotResetMessage"/>
        <html:hidden styleId="cannotDownloadCsvMessage" property="cannotDownloadCsvMessage"/>
        
        <logic:notEmpty name="Wadm001Form" property="companySupplierList">
             <c:forEach var="companySupplierItem" items="${Wadm001Form.companySupplierList}" begin="0" step="1" varStatus="status">
                 <html:hidden name="companySupplierItem" property="SCd" indexed="true"/>
                 <html:hidden name="companySupplierItem" property="companyName" indexed="true"/>
             </c:forEach>
        </logic:notEmpty>
        <logic:notEmpty name="Wadm001Form" property="supplierAuthenList">
            <c:forEach var="supplierAuthen" items="${Wadm001Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
            </c:forEach>
        </logic:notEmpty>
        
        <table class="bottom" style="height:30px; border:0px; cellpadding:0px; cellspacing:0px; width:990px;">
            <tr>
                <td>
                    <sps:label name="ADMIN_CONFIG" />
                    &gt;
                    <sps:label name="SUPPLIER_USER_INFORMATION" />
                    &gt;
                    <sps:label name="SUPPLIER_USER_INFORMATION" />
                </td>
                <td colspan="2" class="align-right" >
                   <sps:label name="USER" />
                   &nbsp; : <bean:write name="Wadm001Form" property="userLogin" />
                        <html:hidden name="Wadm001Form" property="userLogin" />
                </td>
            </tr>
        </table>
        <c:choose>
            <c:when test="${not empty Wadm001Form.applicationMessageList}">
                <c:set var="displayErrorMessage">
                    
                </c:set>
            </c:when>
            <c:otherwise>
                <c:set var="displayErrorMessage">
                    display: none;
                </c:set>
            </c:otherwise>
        </c:choose>
        <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}" >
            <table style="width: 992px" class="tableMessage">
                <tr>
                    <td id="errorTotal">
                        <c:forEach var="appMsg"
                            items="${Wadm001Form.applicationMessageList}"
                            begin="0" step="1" varStatus="status">
                            <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                <c:set var="messageClass">
                                    ui-messageSuccess
                                </c:set>
                            </c:if>
                            <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                <c:set var="messageClass">
                                    ui-messageError
                                </c:set>
                            </c:if>
                            <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                <c:set var="messageClass">
                                    ui-messageWarning
                                </c:set>
                            </c:if>
                            <span class="${messageClass}" >
                                ${appMsg.message} 
                            </span>
                            <br>
                        </c:forEach>
                    </td>
                </tr>
            </table>
        </div>
        <fieldset class="fixwidth">
            <table class="display_criteria"  style="width:975px; border:0px; cellpadding:3px; cellspacing:1px;">
                <tr>
                    <th style="width:100px;" class="align-right">
                        <sps:label name="DSC_ID" />
                        &nbsp;:
                    </th>
                    <td style="width:220px;">
                        <html:text name="Wadm001Form" property="dscId" styleId="dscId" size="15" maxlength="30" style="width:220px"/>
                    </td>
                    <th style="width:100px;">
                        <sps:label name="REGISTER_DATE" />
                        &nbsp;:
                    </th>
                    <td colspan="3">
                        <sps:calendar name="registerDateFrom" id="registerDateFrom" value="${Wadm001Form.registerDateFrom}"/>
                        -
                        <sps:calendar name="registerDateTo" id="registerDateTo" value="${Wadm001Form.registerDateTo}" />
                    </td>
               </tr>
               <tr>
                    <th>
                        <sps:label name="S_COMPANY_NAME" />
                        &nbsp;:
                    </th>
                    <td colspan="3">
                        <html:select name="Wadm001Form" property="SCd" styleId="SCd" styleClass="mandatory" style="width:528px">
                            <html:option value="ALL">
                                <sps:label name="ALL_CRITERIA" />
                            </html:option>
                            <c:forEach var="companySupplier" items="${Wadm001Form.companySupplierList}" begin="0" step="1" varStatus="status">
                                <html:option value="${companySupplier.SCd}">
                                    ${companySupplier.SCd} - ${companySupplier.companyName}
                                </html:option>
                            </c:forEach>
                        </html:select>
                    </td>
                    <th>
                        <sps:label name="S_P" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:select name="Wadm001Form" property="SPcd" styleId="SPcd"  styleClass="mandatory" style="width:110px">
                            <html:option value="ALL">
                                <sps:label name="ALL_CRITERIA" />
                            </html:option>
                            <html:option value="-">
                                <sps:label name="UNDEFINED" />
                            </html:option>
                            <c:forEach var="plantSupplier" items="${Wadm001Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                                <html:option value="${plantSupplier.SPcd}">
                                    ${plantSupplier.SPcd}
                                </html:option>
                            </c:forEach>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <th>
                        <sps:label name="FIRST_NAME" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:text name="Wadm001Form" property="firstName" styleId="firstName" size="15"
                            maxlength="50" style="width:220px"/>
                    </td>
                    <th>
                        <sps:label name="MIDDLE_NAME" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:text name="Wadm001Form" property="middleName" styleId="middleName" size="15"
                            maxlength="50" style="width:180px"/>
                    </td>
                    <th style="width:100px;">
                        <sps:label name="LAST_NAME" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:text name="Wadm001Form" property="lastName" styleId="lastName" size="15"
                            maxlength="50" style="width:220px"/>
                    </td>
                </tr>
            </table>
        </fieldset>
        <table style="padding-top:4px; padding-bottom:2px; width:992px; cellspacing:0px;">
            <tr>
                <td class="align-left" style="vertical-align:bottom;font-size:10px;">
                    <c:if test="${not empty Wadm001Form.userSupplierInfoList}">
                        <sps:label name="SYSTEM_GET_SELECTED_REC_EVERY_PAGE"/>
                    </c:if>
                </td>
                <td class="align-right">
                    <input type="button" name="Input" id="btnSearch"    value="${btnSearch}" class="select_button" />
                    <input type="button" name="Input" id="btnReset"     value="${btnReset}" class="select_button" />
                    <input type="button" name="Input" id="btnDownload"  value="${btnDownload}" class="select_button" />
                </td>
            </tr>
        </table>
        <ai:pagingoption maxRow="5"/>
        </html:form>
        <c:choose>
            <c:when test="${0 == Wadm001Form.max}">
                <c:set var="visibilityFlag">
                    hidden
                </c:set>
            </c:when>
            <c:otherwise>
                <c:set var="visibilityFlag">
                    visible
                </c:set>
            </c:otherwise>
        </c:choose>
        <div id="result" style="visibility:${visibilityFlag};">
            <html:form styleId="Wadm001FormResult" action="/SupplierUserInformationAction.do" >
                <html:hidden property="method" styleId="methodResult" value="Search"/>
                <html:hidden property="mode" styleId="modeResult"/>
                <html:hidden property="dscId" styleId="dscId"/>
                <html:hidden property="SCd" styleId="SCd"/>
                <html:hidden property="SPcd" styleId="SPcd"/>
                <html:hidden property="SCdSelected" styleId="SCdSelected"/>
                <html:hidden property="SPcdSelected" styleId="SPcdSelected"/>
                <html:hidden property="firstName"/>
                <html:hidden property="middleName"/>
                <html:hidden property="lastName"/>
                <html:hidden property="firstNameSelected" styleId="firstNameSelected"/>
                <html:hidden property="middleNameSelected" styleId="middleNameSelected"/>
                <html:hidden property="lastNameSelected" styleId="lastNameSelected"/>
                <html:hidden property="companyName" styleId="companyName"/>
                <html:hidden property="registerDateFrom" styleId="registerDateFrom"/>
                <html:hidden property="registerDateTo" styleId="registerDateTo"/>
                <html:hidden property="dscIdSelected" styleId="dscIdSelected"/>
                <html:hidden property="SPcd" styleId="sPcdResult"/>
                <html:hidden property="countDelete" styleId="countDelete"/>
                <html:hidden property="cannotResetMessage"/>
                <html:hidden property="cannotDownloadCsvMessage"/>
                
                <logic:notEmpty name="Wadm001Form" property="companySupplierList">
                     <c:forEach var="companySupplierItem" items="${Wadm001Form.companySupplierList}" begin="0" step="1" varStatus="status">
                         <html:hidden name="companySupplierItem" property="SCd" indexed="true"/>
                         <html:hidden name="companySupplierItem" property="companyName" indexed="true"/>
                     </c:forEach>
                </logic:notEmpty>
                <logic:notEmpty name="Wadm001Form" property="supplierAuthenList">
                   <c:forEach var="supplierAuthen" items="${Wadm001Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                        <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                        <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
                    </c:forEach>
                </logic:notEmpty>
                
                <div style="width:98%;height:350px;position:absolute; overflow-x:auto; overflow-y:auto;">
                    <table class="display_data" id="tblBody" style="table-layout:fixed;width:1295px;word-wrap:break-word;">
                        <tr>
                            <th scope="col" class="align-center" style="width:20px;">
                                <sps:label name="ALL" />
                                <br />
                                <input type="checkbox" name="checkbox" value="checkbox" onclick="wadm001CheckAll(this);"/>
                            </th>
                            <th class="align-center" style="width:20px;">
                                <sps:label name="NO" />
                            </th>
                            <th scope="col" class="align-center" style="width:150px;">
                                <sps:label name="DSC_ID" />
                            </th>
                            <th scope="col" class="align-center" style="width:230px;">
                                <sps:label name="S_COMPANY_NAME" />
                            </th>
                            <th scope="col" class="align-center" style="width:70px;">
                                <sps:label name="S_P" />
                            </th>
                            <th scope="col" class="align-center" style="width:100px;">
                                <sps:label name="DEPARTMENT" />
                            </th>
                            <th scope="col" class="align-center" style="width:120px;">
                                <sps:label name="FIRST_NAME" />
                            </th>
                            <th scope="col" class="align-center" style="width:90px;">
                                <sps:label name="MIDDLE_NAME" />
                            </th>
                            <th scope="col" class="align-center" style="width:120px;">
                                <sps:label name="LAST_NAME" />
                            </th>
                            <th class="align-center" style="width:55px;">
                                <sps:label name="ASSIGN" />
                            </th>
                            <th scope="col" class="align-center" style="width:90px;">
                                <sps:label name="TELEPHONE" />
                            </th>
                            <th scope="col" class="align-center" style="width:230px;">
                                <sps:label name="EMAIL" />
                            </th>
                        </tr>
                        <c:set var="startRow">
                            ${(Wadm001Form.pageNo * Wadm001Form.count) - Wadm001Form.count}
                        </c:set>
                        <c:forEach var="userSupplierInfo" items="${Wadm001Form.userSupplierInfoList}" step="1" varStatus="status">
                            <c:choose>
                                <c:when test="${(status.index+1)% 2 == 0}">
                                    <c:set var="rowColor">
                                        blueLight
                                    </c:set>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="rowColor">
                                        
                                    </c:set>
                                </c:otherwise>
                            </c:choose>
                            <tr class="${rowColor}">
                                <td class="align-center" style="width:20px;">
                                    <input type="checkbox" name="supplierChkbox" id="checkSupplier${status.index}"
                                    onclick="wadm001setSupplierList('${status.index}')"
                                        <c:if test="${not empty userSupplierInfo.dscIdSelected}">
                                            checked
                                        </c:if>
                                    />
                                    <html:hidden name="userSupplierInfo"  property="dscIdSelected" indexed="true" styleId="dscIdSelected${status.index}"/>
                                    <!-- Parameter for selected value -->
                                    <html:hidden name="userSupplierInfo" property="dscId" indexed="true" value="${userSupplierInfo.dscId}" styleId="dscId${status.index}"/>
                                    <html:hidden name="userSupplierInfo" property="updateDatetime" indexed="true" value="${userSupplierInfo.updateDatetime}" styleId="updateDatetime${status.index}"/>
                                </td>
                                <td class="align-right">
                                    ${status.index + startRow + 1}
                                </td>
                                <td class="align-left">
                                    <a href="javascript:void(0)" onclick="return wadm001DscIdLink('${userSupplierInfo.dscId}'); return false;" >
                                        ${userSupplierInfo.dscId}
                                    </a>
                                </td>
                                <td class="align-left">
                                    (${userSupplierInfo.SCd}) ${userSupplierInfo.userSupplierDetailDomain.spsMCompanySupplierDomain.companyName}
                                    <html:hidden name="userSupplierInfo" property="SCd" indexed="true" />
                                    <html:hidden name="userSupplierInfo" property="userSupplierDetailDomain.spsMCompanySupplierDomain.companyName" indexed="true"/>
                                </td>
                                <c:choose>
                                    <c:when test="${cons.UNDEFINED_FOR_SUPPLIER == userSupplierInfo.SPcd}">
                                        <td class="align-left">
                                            <sps:label name="UNDEFINED" />
                                            <html:hidden name="userSupplierInfo" property="SPcd" indexed="true" />
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="align-left">
                                            ${userSupplierInfo.SPcd}
                                            <html:hidden name="userSupplierInfo" property="SPcd" indexed="true" />
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${null == userSupplierInfo.userDomain.departmentName}">
                                        <td class="align-center">
                                            -
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="align-left">
                                            <bean:write name="userSupplierInfo" property="userDomain.departmentName" />
                                            <html:hidden name="userSupplierInfo" property="userDomain.departmentName" indexed="true" />
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                                <td class="align-left">
                                    <bean:write name="userSupplierInfo" property="userDomain.firstName" />
                                    <html:hidden name="userSupplierInfo" property="userDomain.firstName" indexed="true" />
                                </td>
                                <c:choose>
                                    <c:when test="${null == userSupplierInfo.userDomain.middleName}">
                                        <td class="align-center">
                                            -
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="align-left">
                                            <bean:write name="userSupplierInfo" property="userDomain.middleName" />
                                            <html:hidden name="userSupplierInfo" property="userDomain.middleName" indexed="true" />
                                         </td>
                                    </c:otherwise>
                                </c:choose>
                                <td class="align-left">
                                    <bean:write name="userSupplierInfo" property="userDomain.lastName" />
                                    <html:hidden name="userSupplierInfo" property="userDomain.lastName" indexed="true" />
                                </td>
                                <td class="align-left">
                                    <a href="javascript:void(0)" onclick="return wadm001RoleLink('${userSupplierInfo.dscId}','${userSupplierInfo.SCd}'
                                    ,'${userSupplierInfo.SPcd}','${userSupplierInfo.userDomain.firstName}'
                                    ,'${userSupplierInfo.userDomain.middleName}','${userSupplierInfo.userDomain.lastName}'
                                    ,'${userSupplierInfo.userSupplierDetailDomain.spsMCompanySupplierDomain.companyName}'); return false;";>
                                        <sps:label name="ROLE" />
                                    </a>
                                </td>
                                <c:choose>
                                    <c:when test="${null == userSupplierInfo.userDomain.telephone}">
                                        <td class="align-center">
                                            -
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="align-left">
                                            <bean:write name="userSupplierInfo" property="userDomain.telephone" />
                                            <html:hidden name="userSupplierInfo" property="userDomain.telephone" indexed="true" />
                                        </td>
                                    </c:otherwise>
                                </c:choose>
                                <td class="align-left">
                                    ${userSupplierInfo.userDomain.email}
                                    <html:hidden name="userSupplierInfo" property="userDomain.email" indexed="true" />
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div style="width:98%;height:355px">
                    &nbsp;
                </div>
                <table style="width: 994px; border:0px;" >
                    <tr>
                        <td class="align-left">
                            <ai:paging action="/SPS/SupplierUserInformationAction.do" formId="Wadm001FormResult" jump="true" />
                        </td>
                        <td class="align-right">
                            <input type="button" name="Input" id="btnDelete"  value="${btnDelete}" class="select_button" />
                        </td>
                    </tr>
                </table>
            </html:form>
        </div>
    </body>
</html>
