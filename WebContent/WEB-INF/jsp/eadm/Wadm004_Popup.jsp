<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wadm004_Popup.js">
        </script>
        <title>
            <sps:label name="ROLE_ASSIGNMENT" />
        </title>
    </head>
    <body class="hideHorizontal" style="width:400px;height:250px;">
        <html:form styleId="Wadm004Form" action="/SupplierUserRoleAssignmentAction.do">
            <html:hidden styleId="method" property="method" />
            <html:hidden styleId="cannotOkMessage" property="cannotOkMessage" />
            <html:hidden styleId="cannotCancelMessage" property="cannotCancelMessage" />
            
            <!-- Key for button. -->
            <c:set var="btnOK">
                <sps:label name="OK" />
            </c:set>
            <c:set var="btnCancel">
                <sps:label name="CANCEL" />
            </c:set>
            <c:set var="logoDenso">
                <sps:label name="DENSO_LOGO" />
            </c:set>
            <!-- Parameter for selected row and link.-->
            <html:hidden styleId="mode" property="mode" />
            <html:hidden styleId="dscId" property="dscId"/>
            <html:hidden styleId="SCd" property="SCd"/>
            <html:hidden styleId="roleNameSelected" property="roleNameSelected"/>
            <table style="width:395px">
                <tr style="height: 50px">
                    <td background="css/images/bgHeader.jpg">
                        <img src="css/images/denso_logo.jpg" style="width:143px; height:20px;"
                        alt="${logoDenso}" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="font-weight:bold">
                            <sps:label name="ROLE_ASSIGNMENT" />
                        </div>
                        <c:choose>
                            <c:when test="${not empty Wadm004Form.applicationMessageList}">
                                <c:set var="displayErrorMessage">
                                    
                                </c:set>
                            </c:when>
                            <c:otherwise>
                                <c:set var="displayErrorMessage">
                                    display: none;
                                </c:set>
                            </c:otherwise>
                        </c:choose>
                        <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}" >
                            <table style="width: 395px" class="tableMessage">
                                <tr>
                                    <td id="errorTotal">
                                        <c:forEach var="appMsg"
                                            items="${Wadm004Form.applicationMessageList}"
                                            begin="0" step="1" varStatus="status">
                                            <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                                <c:set var="messageClass">
                                                    ui-messageSuccess
                                                </c:set>
                                            </c:if>
                                            <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                                <c:set var="messageClass">
                                                    ui-messageError
                                                </c:set>
                                            </c:if>
                                            <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                                <c:set var="messageClass">
                                                    ui-messageWarning
                                                </c:set>
                                            </c:if>
                                            <span class="${messageClass}" >
                                                ${appMsg.message} 
                                            </span>
                                            <br>
                                        </c:forEach>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <fieldset style="margin-top: 5px;">
                            <table style="width:100%;border:0px; cellpadding:3px; cellspacing:1px;" class="display_criteria" >
                                <c:choose>
                                    <c:when test="${cons.MODE_REGISTER == Wadm004Form.mode}">
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="ROLE_NAME" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%" class="align-left">
                                                <html:select name="Wadm004Form" property="roleCode" styleId="roleCode" styleClass="mandatory" style="width:150px;">
                                                    <c:forEach var="role" items="${Wadm004Form.roleList}" begin="0" step="1" varStatus="status">
                                                        <html:option value="${role.roleCd}">
                                                            ${role.roleName}
                                                        </html:option>
                                                    </c:forEach>
                                                </html:select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="EFFECTIVE_START" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%" class="align-left">
                                                <sps:calendar name="effectStart" id="effectStart" value="" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="EFFECTIVE_END" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%" class="align-left">
                                                <sps:calendar name="effectEnd" id="effectEnd" value="" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="S_P" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%">
                                                <html:select name="Wadm004Form" property="SPcd" styleId="SPcd" styleClass="mandatory" style="width:150px;">
                                                    <c:forEach var="plantSupplier" items="${Wadm004Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                                                        <html:option value="${plantSupplier.SPcd}">
                                                            ${plantSupplier.SPcd}
                                                        </html:option>
                                                    </c:forEach>
                                                </html:select>
                                            </td>
                                        </tr>
                                    </c:when>
                                    <c:otherwise>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="ROLE_NAME" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%" class="align-left">
                                                <html:select name="Wadm004Form" property="roleCode" styleId="roleCode" styleClass="mandatory" style="width:150px;">
                                                    <c:forEach var="role" items="${Wadm004Form.roleList}" begin="0" step="1" varStatus="status">
                                                        <html:option value="${role.roleCd}">
                                                            ${role.roleName}
                                                        </html:option>
                                                    </c:forEach>
                                                </html:select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="EFFECTIVE_START" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%" class="align-left">
                                                <sps:calendar name="effectStart" id="effectStart" value="${Wadm004Form.effectStart}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="EFFECTIVE_END" />
                                                &nbsp;:
                                                </th>
                                                <td style="width:50%" class="align-left">
                                                    <sps:calendar name="effectEnd" id="effectEnd" value="${Wadm004Form.effectEnd}" />
                                                </td>
                                        </tr>
                                        <tr>
                                            <th style="width:40%" class="align-right">
                                                <sps:label name="S_P" />
                                                &nbsp;:
                                            </th>
                                            <td style="width:50%">
                                                <html:select name="Wadm004Form" property="SPcd" styleId="SPcd" styleClass="mandatory" style="width:150px;">
                                                    <c:forEach var="plantSupplier" items="${Wadm004Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                                                        <html:option value="${plantSupplier.SPcd}">
                                                            ${plantSupplier.SPcd}
                                                        </html:option>
                                                    </c:forEach>
                                                </html:select>
                                            </td>
                                        </tr>
                                    </c:otherwise>
                                </c:choose>
                            </table>
                        </fieldset>
                        <table style="width:100%">
                            <tr>
                                <td colspan="2" class="align-right">
                                    <input name="Input" id="btnOK"      type="button" value="${btnOK}"     class="select_button" />
                                    <input name="Input" id="btnCancel"  type="button" value="${btnCancel}" class="select_button" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </html:form>
    </body>
</html>