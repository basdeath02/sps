<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wadm006_DensoUserRegistration.js">
        </script>
        <script type="text/javascript">
            var labelAll = '<sps:label name="ALL_CRITERIA" />';
            var labelPleaseSelect = '<sps:label name="PLEASE_SELECT_CRITERIA" />';
            var labelUndefined = '<sps:label name="UNDEFINED" />';
            var labelReturn = '<sps:label name="RETURN" />';
            var msgConfirmReturn = '<sps:message name="SP-W6-0001"/>';
        </script>
        <title>
            <sps:label name="DENSO_USER_REGISTRATION" />
        </title>
    </head>
    <body class="hideHorizontal">
        <html:form styleId="Wadm006Form" action="/DensoUserRegistrationAction.do">
        <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden property="method" styleId="method" />
            <html:hidden property="mode" styleId="mode" />
            <html:hidden property="cannotResetMessage" styleId="cannotResetMessage" />
            
            <!-- Key for button. -->
            <c:set var="btnRegister">
                <sps:label name="REGISTER" />
            </c:set>
            <c:set var="btnReset">
                <sps:label name="RESET" />
            </c:set>
            <c:set var="btnReturn">
                <sps:label name="RETURN" />
            </c:set>
            
            <!-- Parameter for selected row and link.-->
            <html:hidden property="emlCreateCancelAsnFlag" styleId="emlCreateCancelAsnFlag" />
            <html:hidden property="emlReviseAsnFlag" styleId="emlReviseAsnFlag" />
            <html:hidden property="emlCreateInvoiceFlag" styleId="emlCreateInvoiceFlag" />
            
            <%-- Start : [IN049] Change Column Name --%>
            <%-- 
            <html:hidden property="emlAbnormalTransferData1Flag" styleId="emlAbnormalTransferData1Flag" />
            <html:hidden property="emlAbnormalTransferData2Flag" styleId="emlAbnormalTransferData2Flag" />
             --%>
            <html:hidden property="emlPedpoDoalcasnUnmatpnFlg" styleId="emlPedpoDoalcasnUnmatpnFlg" />
            <html:hidden property="emlAbnormalTransferFlg" styleId="emlAbnormalTransferFlg" />
            <!-- End : [IN049] Change Column Name -->
            
            <html:hidden property="maxDepartment" styleId="maxDepartment" />
            <html:hidden property="updateDatetime" styleId="updateDatetime" value="${Wadm006Form.updateDatetime}"/>
            <html:hidden property="updateDatetimeDenso" styleId="updateDatetimeDenso" value="${Wadm006Form.updateDatetimeDenso}"/>
            
            <logic:notEmpty name="Wadm006Form" property="departmentList">
                <c:forEach var="departmentItem" items="${Wadm006Form.departmentList}" step="1" varStatus="status">
                    <html:hidden name="departmentItem" property="departmentName" value="${departmentItem.departmentName}" styleId="dept${status.index}" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            
            <logic:notEmpty name="Wadm006Form" property="companyDensoList">
                <c:forEach var="companyDensoItem" items="${Wadm006Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoItem" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            
            <table class="bottom" style="height: 30px; width: 994px; border:0px; cellpadding:0px; cellspacing:0px;">
                <tr>
                    <td>
                        <sps:label name="ADMIN_CONFIG" />
                        &gt;
                        <sps:label name="DENSO_USER_INFORMATION" />
                        &gt;
                        <c:choose>
                            <c:when test="${cons.MODE_REGISTER == Wadm006Form.mode}">
                                <sps:label name="DENSO_USER_REGISTRATION" />
                            </c:when>
                            <c:otherwise>
                                <sps:label name="DENSO_USER_INFORMATION" />
                                &gt;
                                <sps:label name="DENSO_USER_REGISTRATION" />
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td colspan="2" class="align-right">
                        <sps:label name="USER" />
                        &nbsp; : <bean:write name="Wadm006Form" property="userLogin" />
                        <html:hidden name="Wadm006Form" property="userLogin" />
                    </td>
                </tr>
            </table>
            
            <c:choose>
                <c:when test="${not empty Wadm006Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width:992px;" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg" items="${Wadm006Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message}
                                </span>
                                <br/>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
                
            <c:choose>
                <c:when test="${cons.MODE_REGISTER == Wadm006Form.mode}">
                    <fieldset class="fixwidth">
                        <table class="display_criteria" style="table-layout: fixed; border:0px; cellpadding:3px; cellspacing:1px; width:975px;">
                            <tr>
                                <th style="width:15%" class="align-right">
                                    <sps:label name="DSC_ID" />
                                    &nbsp;:
                                </th>
                                <td style="width:19%">
                                    <html:text name="Wadm006Form" property="dscId" styleId="dscId"
                                    styleClass="mandatory" style="width: 173px" />
                                </td>
                                <th style="width:15%" class="align-right">
                                    <sps:label name="EMPLOYEE_CODE" />
                                    &nbsp;:
                                </th>
                                <td style="width:19%">
                                    <html:text name="Wadm006Form" property="employeeCode" styleId="employeeCode"
                                    styleClass="mandatory" style="width: 168px" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:13%">
                                    <sps:label name="D_CD" />
                                    &nbsp;:
                                </th>
                                <td style="width:19%">
                                    <html:select name="Wadm006Form" property="DCd" styleId="DCd" styleClass="mandatory" style="width: 175px">
                                        <%-- [IN052] Set --Please Select-- as default. --%>
                                        <html:option value="">
                                            <sps:label name="PLEASE_SELECT_CRITERIA" />
                                        </html:option>
                                    
                                        <c:forEach var="companyDenso" items="${Wadm006Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                            <html:option value="${companyDenso.DCd}">
                                                ${companyDenso.DCd}
                                            </html:option>
                                        </c:forEach>
                                    </html:select>
                                </td>
                                <th class="align-right">
                                    <sps:label name="D_P" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <html:select name="Wadm006Form" property="DPcd" styleId="DPcd" styleClass="mandatory" style="width: 170px">
                                       <%-- [IN052] Do not set Undefined as default DENSO Plant. --%>
                                       <%-- 
                                           <html:option value="99">
                                              <sps:label name="UNDEFINED"/>
                                           </html:option>
                                        --%>
                                        <html:option value="">
                                            <sps:label name="PLEASE_SELECT_CRITERIA" />
                                        </html:option>
                                        
                                        <c:forEach var="plantDenso" items="${Wadm006Form.plantDensoList}" begin="0" step="1" varStatus="status">
                                            <html:option value="${plantDenso.DPcd}">
                                                ${plantDenso.DPcd}
                                            </html:option>
                                        </c:forEach>
                                        
                                        <%-- [IN052] Do not set Undefined as default DENSO Plant. --%>
                                        <html:option value="99">
                                           <sps:label name="UNDEFINED"/>
                                        </html:option>            
                                        
                                    </html:select>
                                </td>
                                <th style="width:10%">
                                    <sps:label name="DEPARTMENT" />
                                    &nbsp;:
                                </th>
                                <td style="width:10%">
                                    <html:text name="Wadm006Form" property="departmentName" styleId="departmentName" style="width: 148px" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <sps:label name="FIRST_NAME" />
                                    &nbsp;:                 
                                </th>
                                <td>
                                    <html:text name="Wadm006Form" property="firstName" styleId="firstName"
                                    styleClass="mandatory" size="15" maxlength="50" style="width: 173px" />
                                </td>
                                <th>
                                    <sps:label name="MIDDLE_NAME" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <html:text name="Wadm006Form" property="middleName"  styleId="middleName"
                                    size="15" maxlength="50"  style="width: 168px" />
                                </td>
                                <th>
                                    <sps:label name="LAST_NAME" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <html:text name="Wadm006Form" property="lastName" styleId="lastName"
                                    styleClass="mandatory" size="15" maxlength="50" style="width: 148px" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <sps:label name="EMAIL" />
                                    &nbsp;:
                                </th>
                                <td colspan="3">
                                    <html:text name="Wadm006Form" property="email" styleId="email"
                                    styleClass="mandatory" style="width: 500px" />
                                </td>
                                <th>
                                    <sps:label name="TELEPHONE" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <html:text name="Wadm006Form" property="telephone" styleId="telephone" style="width: 148px" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <sps:label name="EMAIL_FOR" />
                                    &nbsp;:
                                </th>
                                <td colspan="5" style="font-weight:bold">&nbsp;&nbsp;
                                    <input type="checkbox" id="emlCreateCancelAsnChk"
                                    <%-- [FIX] Checkbox cleared after submit and error --%>
                                        <c:if test="${'1' == Wadm006Form.emlCreateCancelAsnFlag}">
                                            checked
                                        </c:if>
                                    />
                                    <sps:label name="CREATE_CANCEL_ASN" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    
                                    <input type="checkbox" id="emlReviseAsnChk"
                                    <%-- [FIX] Checkbox cleared after submit and error --%>
                                        <c:if test="${'1' == Wadm006Form.emlReviseAsnFlag}">
                                            checked
                                        </c:if>
                                    />
                                    <sps:label name="REVISE_SHIPPING_QTY" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    
                                    <input type="checkbox" id="emlCreateInvoiceChk"
                                    <%-- [FIX] Checkbox cleared after submit and error --%>
                                        <c:if test="${'1' == Wadm006Form.emlCreateInvoiceFlag}">
                                            checked
                                        </c:if>
                                    />
                                    <sps:label name="CREATE_INVOICE_WITH_CN" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    
                                    <br/>
                                    &nbsp;&nbsp;
                                    
                                    <%-- Start : [IN049] Change Column Name --%>
                                    <%-- <input type="checkbox" id="emlAbnormalTransferData1Chk"/> --%>
                                    <input type="checkbox" id="emlPedpoDoalcasnUnmatpnChk"
                                        <c:if test="${'1' == Wadm006Form.emlPedpoDoalcasnUnmatpnFlg}">
                                            checked
                                        </c:if>
                                    />
                                    
                                    <sps:label name="ABNORMAL_TRANSFER_DATA1" />
                                    &nbsp;&nbsp;&nbsp;
                                    
                                    <%-- Start : [IN049] Change Column Name --%>
                                    <%-- <input type="checkbox" id="emlAbnormalTransferData2Chk"/> --%>
                                    <input type="checkbox" id="emlAbnormalTransferChk"
                                        <c:if test="${'1' == Wadm006Form.emlAbnormalTransferFlg}">
                                            checked
                                        </c:if>
                                    />
                                    
                                    <sps:label name="ABNORMAL_TRANSFER_DATA2" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </c:when>
                <c:otherwise>
                    <fieldset class="fixwidth">
                        <table class="display_criteria" style="table-layout: fixed;  border:0px; cellpadding:3px; cellspacing:1px; width:975px;">
                            <tr>
                                <th style="width:15%" class="align-right">
                                    <sps:label name="DSC_ID" />
                                    &nbsp;:
                                </th>
                                <td style="width:19%">
                                    <html:text name="Wadm006Form" property="dscId" styleId="dscId"
                                    styleClass="mandatory" value="${Wadm006Form.dscId}" style="width: 168px" readonly="true"/>
                                </td>
                                <th style="width:15%" class="align-right">
                                    <sps:label name="EMPLOYEE_CODE" />
                                    &nbsp;:
                                </th>
                                <td style="width:19%">
                                    <html:text name="Wadm006Form" property="employeeCode" styleId="employeeCode"
                                    styleClass="mandatory" value="${Wadm006Form.employeeCode}" style="width: 168px" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:13%">
                                    <sps:label name="D_CD" />
                                    &nbsp;:
                                </th>
                                <td style="width:19%">
                                    <html:select name="Wadm006Form" property="DCd" styleId="DCd" styleClass="mandatory" 
                                    style="width: 175px" value="${Wadm006Form.DCd}">
                                        <%-- [IN052] Set --Please Select-- as default. --%>
                                        <html:option value="">
                                            <sps:label name="PLEASE_SELECT_CRITERIA" />
                                        </html:option>
                                    
                                       <c:forEach var="companyDenso" items="${Wadm006Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                           <html:option value="${companyDenso.DCd}">
                                               ${companyDenso.DCd}
                                           </html:option>
                                       </c:forEach>
                                    </html:select>
                                </td>
                                <th class="align-right">
                                    <sps:label name="D_P" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <html:select name="Wadm006Form" property="DPcd" styleId="DPcd" styleClass="mandatory" 
                                        style="width: 170px" value="${Wadm006Form.DPcd}">
                                        
                                        <%-- [IN052] Do not set Undefined as default DENSO Plant. --%>
                                        <%--
                                            <html:option value="99">
                                               <sps:label name="UNDEFINED" />
                                            </html:option>
                                        --%>
                                        <html:option value="">
                                            <sps:label name="PLEASE_SELECT_CRITERIA" />
                                        </html:option>
                                        
                                        <c:forEach var="plantDenso" items="${Wadm006Form.plantDensoList}" begin="0" step="1" varStatus="status">
                                            <html:option value="${plantDenso.DPcd}">
                                                ${plantDenso.DPcd}
                                            </html:option>
                                        </c:forEach>
                                        
                                        <%-- [IN052] Do not set Undefined as default DENSO Plant. --%>
                                        <html:option value="99">
                                           <sps:label name="UNDEFINED" />
                                        </html:option>
                                        
                                    </html:select>
                                </td>
                                <th style="width:10%">
                                    <sps:label name="DEPARTMENT" />
                                    &nbsp;:
                                </th>
                                <td style="width:10%">
                                    <html:text name="Wadm006Form" property="departmentName" styleId="departmentName"
                                    value="${Wadm006Form.departmentName}"
                                    style="width: 148px" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <sps:label name="FIRST_NAME" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <html:text name="Wadm006Form" property="firstName" styleId="firstName" styleClass="mandatory"
                                    value="${Wadm006Form.firstName}" style="width: 173px" />
                                </td>
                                <th>
                                    <sps:label name="MIDDLE_NAME" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <html:text name="Wadm006Form" property="middleName" styleId="middleName" value="${Wadm006Form.middleName}"
                                    style="width: 168px" />
                                </td>
                                <th>
                                    <sps:label name="LAST_NAME" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <html:text name="Wadm006Form" property="lastName" styleId="lastName" styleClass="mandatory"
                                    value="${Wadm006Form.lastName}" style="width: 148px" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <sps:label name="EMAIL" />
                                    &nbsp;:
                                </th>
                                <td colspan="3">
                                    <html:text name="Wadm006Form" property="email" styleId="email" value="${Wadm006Form.email}"
                                    styleClass="mandatory" style="width: 500px" />
                                </td>
                                <th>
                                    <sps:label name="TELEPHONE" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <html:text name="Wadm006Form" property="telephone" styleId="telephone" value="${Wadm006Form.telephone}"
                                    style="width: 148px" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <sps:label name="EMAIL_FOR" />
                                    &nbsp;:
                                </th>
                                <td colspan="5" style="font-weight:bold">&nbsp;&nbsp;
                                    <input type="checkbox" id="emlCreateCancelAsnChk"
                                        <c:if test="${'1' == Wadm006Form.emlCreateCancelAsnFlag}">
                                            checked
                                        </c:if>
                                    />
                                    <sps:label name="CREATE_CANCEL_ASN" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    
                                    <input type="checkbox" id="emlReviseAsnChk"
                                        <c:if test="${'1' == Wadm006Form.emlReviseAsnFlag}">
                                            checked
                                        </c:if>
                                    />
                                    <sps:label name="REVISE_SHIPPING_QTY" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    
                                    <input type="checkbox" id="emlCreateInvoiceChk"
                                        <c:if test="${'1' == Wadm006Form.emlCreateInvoiceFlag}">
                                            checked
                                        </c:if>
                                    />
                                    <sps:label name="CREATE_INVOICE_WITH_CN" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    
                                    <br/>
                                    &nbsp;&nbsp;
                                    
                                    <%-- Start : [IN049] Change Column Name --%>
                                    <%--
                                    <input type="checkbox" id="emlAbnormalTransferData1Chk"
                                        <c:if test="${'1' == Wadm006Form.emlAbnormalTransferData1Flag}">
                                            checked
                                        </c:if>
                                    />
                                    --%>
                                    <input type="checkbox" id="emlPedpoDoalcasnUnmatpnChk"
                                        <c:if test="${'1' == Wadm006Form.emlPedpoDoalcasnUnmatpnFlg}">
                                            checked
                                        </c:if>
                                    />
                                    <%-- End : [IN049] Change Column Name --%>
                                    
                                    <sps:label name="ABNORMAL_TRANSFER_DATA1" />
                                    &nbsp;&nbsp;&nbsp;
                                    
                                    <%-- Start : [IN049] Change Column Name --%>
                                    <%--  
                                    <input type="checkbox" id="emlAbnormalTransferData2Chk"
                                        <c:if test="${'1' == Wadm006Form.emlAbnormalTransferData2Flag}">
                                            checked
                                        </c:if>
                                    />
                                    --%>
                                    <input type="checkbox" id="emlAbnormalTransferChk"
                                        <c:if test="${'1' == Wadm006Form.emlAbnormalTransferFlg}">
                                            checked
                                        </c:if>
                                    />
                                    <%-- End : [IN049] Change Column Name --%>
                                    
                                    <sps:label name="ABNORMAL_TRANSFER_DATA2" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </c:otherwise>
            </c:choose>
            <div id="button">
                <table style="padding-top: 4px; padding-bottom: 2px; width:992px; cellspacing:0px;">
                    <tr>
                        <td class="align-right">
                            <input name="Input" type="button" value="${btnRegister}" id="btnRegister" class="select_button" />
                            <input name="Input" type="button" value="${btnReset}"    id="btnReset"    class="select_button" />
                            <c:choose>
                                <c:when test="${cons.MODE_REGISTER == Wadm006Form.mode}">
                                    <input name="Input" type="button" value="${btnReturn}" id="btnReturn"
                                    class="select_button" disabled="disabled"/>
                                </c:when>
                                <c:otherwise>
                                    <input name="Input" type="button" value="${btnReturn}" id="btnReturn"
                                    class="select_button" />
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </table>
            </div>
        </html:form>
    </body>
</html>
