<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wadm003_SupplierUserUploading.js" >
        </script>
        <script type="text/javascript">
            var labelRegisterProcess = '<sps:label name="REGISTER_PROCESS" />';
            var msgConfirmRegister = '<sps:message name="SP-W6-0001"/>';
        </script>
        <title>
            <sps:label name="SUPPLIER_USER_UPLOADING" />
        </title>
    </head>
    <body class="hideHorizontal">
        <html:form styleId="Wadm003FormCriteria" enctype="multipart/form-data" action="/SupplierUserUploadingAction.do" >
        <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden styleId="method" property="method" />
            <html:hidden styleId="totalRecord" property="totalRecord" />
            <html:hidden styleId="correctRecord" property="correctRecord" />
            <html:hidden styleId="warningRecord" property="warningRecord" />
            <html:hidden styleId="incorrectRecord" property="incorrectRecord" />
            <html:hidden styleId="cannotResetMessage" property="cannotResetMessage"/>
            <!-- Key for paging. -->
            <html:hidden styleId="currentPage" property="currentPage"/>
            <html:hidden styleId="row"  property="row"/>
            <html:hidden styleId="totalCount" property="totalCount"/>
            <!-- Key for button. -->
            <c:set var="btnRegister">
                <sps:label name="REGISTER" />
            </c:set>
            <c:set var="btnReset">
                <sps:label name="RESET" />
            </c:set>
            <c:set var="btnBrowse">
                <sps:label name="BROWSE" />
            </c:set>
            <c:set var="btnUpload">
                <sps:label name="UPLOAD" />
            </c:set>
            <table style="height:30px; border:0px; cellpadding:0px; cellspacing:0px; width:990px;" class="bottom">
                <tr>
                    <td>
                        <sps:label name="ADMIN_CONFIG" />
                        &gt;
                        <sps:label name="SUPPLIER_USER_INFORMATION" />
                        &gt;
                        <sps:label name="SUPPLIER_USER_UPLOADING" />
                    </td>
                    <td colspan="2" class="align-right" >
                        <sps:label name="USER" />
                        &nbsp; : <bean:write name="Wadm003Form" property="userLogin" />
                        <html:hidden name="Wadm003Form" property="userLogin" />
                    </td>
                </tr>
            </table>
            <c:choose>
                <c:when test="${not empty Wadm003Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}" >
                <table style="width: 992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg"
                                items="${Wadm003Form.applicationMessageList}"
                                begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}" >
                                    ${appMsg.message} 
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            <fieldset class="fixwidth">
                <table style="width:975px; border:0px; cellpadding:3px; cellspacing:1px; margin-top:10px;" class="display_criteria">
                    <tr>
                        <th style="width:150px;">
                            <sps:label name="FILE_NAME" />
                            &nbsp;:
                        </th>
                        <td style="width:530px;">
                            <html:text name="Wadm003Form" property="fileName"
                                styleId="txtUpload" styleClass="txtUpload2" readonly="true"/>
                        </td>
                        <td>
                            <div style="position:absolute; left:10; filter:alpha(opacity=0); opacity:0;">
                                &nbsp;
                                <html:file property="fileData" styleId="fileData"
                                    maxlength="300" onchange="wadm003SetPathFile();" 
                                    style="width: 90px; height: 25px; margin-left: -7px;" />
                            </div>
                            <input type="button" name="Input" id="btnBrowse" value="${btnBrowse}"
                                class="select_button"/>
                            <input type="button" name="Input" id="btnUpload" value="${btnUpload}"
                                class="select_button" />
                            <input type="button" name="Input" id="btnReset" value="${btnReset}"
                                class="select_button" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        <c:choose>
            <c:when test="${0 == Wadm003Form.totalRecord}">
                <c:set var="visibilityFlag">
                    hidden
                </c:set>
            </c:when>
            <c:otherwise>
                <c:set var="visibilityFlag">
                    visible
                </c:set>
            </c:otherwise>
        </c:choose>
        <html:form styleId="Wadm003FormResult" action="/SupplierUserUploadingAction.do" >
            <div id="result" style="visibility: ${visibilityFlag};">
                <html:hidden styleId="methodResult" property="method" value="doPaging"/>
                <html:hidden property="totalRecord" />
                <html:hidden property="correctRecord" />
                <html:hidden property="warningRecord" />
                <html:hidden property="incorrectRecord" />
                <html:hidden property="fileName" styleId="fileName"/>
                <html:hidden property="cannotResetMessage"/>
                <table class="display_criteria" style="border:0px; cellpadding:3px; cellspacing:1px; width:975px;">
                    <tr>
                        <td colspan="9" class="align-left">
                            <span style="font-weight: bold; text-decoration: underline;">
                                <sps:label name="UPLOAD_RESULT" />
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:15%" class="align-right">
                            <sps:label name="TOTAL_RECORD" />
                            &nbsp;:
                        </th>
                        <td style="width:3%"  class="align-right">
                            ${Wadm003Form.totalRecord}
                            &nbsp;
                        </td>
                        <td style="width:8%" class="align-left">
                            <sps:label name="RECORD" />
                        </td>
                        <th style="width:10%" class="align-right">
                            <sps:label name="CORRECT" />
                            &nbsp;:
                        </th>
                        <td style="width:3%"  class="align-right">
                            ${Wadm003Form.correctRecord}
                            &nbsp;
                        </td>
                        <td style="width:8%" class="align-left">
                            <sps:label name="RECORD" />
                        </td>
                        <th style="width:10%" class="align-right">
                            <sps:label name="WARNING" />
                            &nbsp;:
                        </th>
                        <td style="width:3%"  class="align-right">
                           ${Wadm003Form.warningRecord}
                           &nbsp;
                        </td>
                        <td style="width:8%" class="align-left">
                            <sps:label name="RECORD" />
                        </td>
                        <th style="width:10%"  class="align-right">
                            <sps:label name="INCORRECT" />
                            &nbsp;:
                        </th>
                        <td style="width:3%" class="align-right">
                            ${Wadm003Form.incorrectRecord}
                            &nbsp;
                        </td>
                        <td style="width:51%" class="align-left">
                            <sps:label name="RECORD" />
                        </td>
                    </tr>
                </table>
            </div>
            
            <c:choose>
                <c:when test="${0 == Wadm003Form.totalRecord}">
                    <c:set var="errorVisibilityFlag">
                        hidden
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="errorVisibilityFlag">
                        visible
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="result" style="visibility: ${errorVisibilityFlag};">
                <div style="width:994px;">
                    <table class="display_data" id="tblbody" style="table-layout:fixed;width:974px;word-wrap:break-word;">
                        <tr>
                            <th style="width:5%" >
                                <sps:label name="NO" />
                            </th>
                            <th style="width:7%">
                                <sps:label name="TYPE" />
                            </th>
                            <th style="width:50%">
                                <sps:label name="MESSAGE" />
                            </th>
                            <th style="width:40%">
                                <sps:label name="CSV_LINE_NO" />
                            </th>
                        </tr>
                    </table>
                </div>
                <div style="width:994px;height:315px;overflow-y:scroll">
                    <table class="display_data" id="tblbody" style="table-layout:fixed;width:974px;word-wrap:break-word;">
                        <c:set var="startRow">
                            ${(Wadm003Form.pageNo * Wadm003Form.count) - Wadm003Form.count}
                        </c:set>
                        <c:forEach var="uploaderror" items="${Wadm003Form.uploadErrorDetailDomain}" step="1" varStatus="status">
                            <c:choose>
                                <c:when test="${(status.index+1)% 2 == 0}">
                                    <c:set var="rowColor">
                                        blueLight
                                    </c:set>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="rowColor">
                                        
                                    </c:set>
                                </c:otherwise>
                            </c:choose>
                            <tr class="${rowColor}">
                                <td style="width:5%" class="align-right">
                                    ${status.index + startRow + 1}
                                </td>
                                <td style="width:7%" class="align-left">
                                    <c:if test="${cons.MESSAGE_TYPE_ERROR == uploaderror.messageType}">
                                        <span style="color:red;">
                                            <sps:label name="ERROR" />
                                        </span>
                                    </c:if>
                                    <c:if test="${cons.MESSAGE_TYPE_WARNING == uploaderror.messageType}">
                                        <sps:label name="WARNING" />
                                    </c:if>
                                    <html:hidden name="uploaderror" property="messageType" indexed="true" />
                                </td>
                                <td style="width:50%" class="align-left">
                                    ${uploaderror.description}
                                    <html:hidden name="uploaderror" property="description" indexed="true" />
                                </td>
                                <td style="width:40%" class="align-left">
                                    ${uploaderror.lineNo}
                                    <html:hidden name="uploaderror" property="lineNo" indexed="true" />
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
            <c:if test="${0 != Wadm003Form.totalRecord}">
                <c:if test="${0 != Wadm003Form.correctRecord || 0 != Wadm003Form.warningRecord}">
                    <c:choose>
                        <c:when test="${0 == Wadm003Form.incorrectRecord && 0 == Wadm003Form.warningRecord}">
                            <table style="width:994px">
                                <tr>
                                    <td align="right">
                                        <input type="button" value="${btnRegister}" id="btnRegister" class="select_button" />
                                    </td>
                                </tr>
                            </table>
                        </c:when>
                        <c:otherwise>
                            <table style="width:994px;">
                                <tr>
                                    <td align="left">
                                        <ai:paging action="/SPS/SupplierUserUploadingAction.do" formId="Wadm003FormResult" jump="true" />
                                    </td>
                                    <td align="right">
                                        <input type="button" value="${btnRegister}" id="btnRegister" class="select_button"/>
                                    </td>
                                </tr>
                            </table>
                        </c:otherwise>
                    </c:choose>
                </c:if>
                <c:if test="${0 == Wadm003Form.correctRecord && 0 == Wadm003Form.warningRecord && 0 == Wadm003Form.incorrectRecord}">
                    <table style="width:994px;">
                        <tr>
                            <td align="left">
                                <ai:paging action="/SPS/SupplierUserUploadingAction.do" formId="Wadm003FormResult" jump="true" />
                             </td>
                             <td align="right">
                                <input type="button" value="${btnRegister}" id="btnRegister" class="select_button" disabled="true"/>
                             </td>
                        </tr>
                    </table>
                </c:if>
                <c:if test="${0 == Wadm003Form.correctRecord && 0 == Wadm003Form.warningRecord && 0 != Wadm003Form.incorrectRecord}">
                    <table style="width:994px;">
                        <tr>
                            <td align="left">
                                <ai:paging action="/SPS/SupplierUserUploadingAction.do" formId="Wadm003FormResult" jump="true" />
                             </td>
                             <td align="right">
                                <input type="button" value="${btnRegister}" id="btnRegister" class="select_button" disabled="true"/>
                             </td>
                        </tr>
                    </table>
                </c:if>
            </c:if>
        </html:form>
    </body>
</html>
