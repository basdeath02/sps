<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wadm008_DensoUserRoleAssignment.js" >
        </script>
        <script type="text/javascript">
            var labelReturn = '<sps:label name="RETURN" />';
            var labelDelete = '<sps:label name="DELETE_SELECTED_ITEMS" />';
            var msgConfirm = '<sps:message name="SP-W6-0001"/>';
        </script>
        <title>
            <sps:label name="DENSO_USER_ROLE_ASSIGNMENT" />
        </title>
    </head>
    <body class="hideHorizontal">
        <html:form styleId="Wadm008FormCriteria" action="/DensoUserRoleAssignmentAction.do" >
        <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden styleId="method"       property="method" />
            <!-- Key for button. -->
            <c:set var="btnAdd">
                <sps:label name="ADD" />
            </c:set>
            <c:set var="btnReturn">
                <sps:label name="RETURN" />
            </c:set>
            <c:set var="btnDelete">
                <sps:label name="DELETE" />
            </c:set>
            <html:hidden styleId="dscId" property="dscId"/>
            <html:hidden styleId="densoUserName" property="densoUserName"/>
            <html:hidden styleId="firstName" property="firstName"/>
            <html:hidden styleId="middleName" property="middleName"/>
            <html:hidden styleId="lastName" property="lastName"/>
            <html:hidden styleId="mode" property="mode" />
            <html:hidden styleId="returnMode" property="returnMode" />
            <html:hidden styleId="roleCode" property="roleCode" />
            <html:hidden styleId="effectEnd" property="effectEnd" />
            <html:hidden styleId="effectStart" property="effectStart" />
            <html:hidden styleId="DCd" property="DCd" />
            <html:hidden styleId="DPcd" property="DPcd" />
            <html:hidden styleId="roleCdSelected" property="roleCdSelected" />
            <html:hidden styleId="roleNameSelected" property="roleNameSelected" />
            <html:hidden styleId="lastUpdateDatetimeScreen" property="lastUpdateDatetimeScreen" />
            <html:hidden styleId="seqNo" property="seqNo" />
            
            <html:hidden property="countDelete" />
            <html:hidden styleId="cannotAddMessage" property="cannotAddMessage" />
            <html:hidden styleId="cannotUpdateMessage" property="cannotUpdateMessage" />
            
            <table style="height:30px; border:0px; cellpadding:0px; cellspacing:0px; width:990px;" class="bottom">
                <tr>
                    <td>
                        <sps:label name="ADMIN_CONFIG" />
                        &gt;
                        <sps:label name="DENSO_USER_INFORMATION" />
                        &gt;
                        <sps:label name="DENSO_USER_INFORMATION" />
                        &gt;
                        <sps:label name="DENSO_USER_ROLE_ASSIGNMENT" />
                    </td>
                    <td colspan="2" class="align-right">
                        <sps:label name="USER" />
                        &nbsp; : <bean:write name="Wadm008Form" property="userLogin" />
                        <html:hidden name="Wadm008Form" property="userLogin" />
                    </td>
                </tr>
            </table>
            
            <c:choose>
                <c:when test="${not empty Wadm008Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width:992px;" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg" items="${Wadm008Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message}
                                </span>
                                <br/>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
                
            <fieldset class="fixwidth">
                <table class="display_criteria" style="border:0px; cellpadding:3px; cellspacing:1px; width:975px;" >
                    <tr>
                        <th style="width:6%" class="align-right">
                            <sps:label name="DSC_ID" />
                            &nbsp;:
                        </th>
                        <td style="width:15%">
                            <html:text name="Wadm008Form" property="dscId"  styleId="DSC_ID" value="${Wadm008Form.dscId}"
                            styleClass="mandatory" size="15" style="width: 140px" readonly="true" />
                        </td>
                        <th style="width:9%">
                            <sps:label name="USER_NAME" />
                            &nbsp;:
                        </th>
                        <td style="width:40%">
                            <html:text name="Wadm008Form" property="densoUserName" styleId="densoUserName" value="${Wadm008Form.densoUserName}" style="width: 440px" readonly="true" />
                        </td>
                        <th style="width:5%" class="align-right">
                            <sps:label name="D_CD" />
                            &nbsp;:
                        </th>
                        <td style="width:5%">
                            <html:text name="Wadm008Form" property="DCd" styleId="DCd" value="${Wadm008Form.DCd}" style="width: 50px" readonly="true" />
                        </td>
                        <th style="width:7%" class="align-right">
                            <sps:label name="D_P" />
                            &nbsp;:
                        </th>
                        <td style="width:3%">
                            <c:choose>
                                <c:when test="${'99' == Wadm008Form.DPcd}">
                                    <c:set var="densoPlantName">
                                        <sps:label name="UNDEFINED" />
                                    </c:set>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="densoPlantName">
                                        ${Wadm008Form.DPcd}
                                    </c:set>
                                </c:otherwise>
                            </c:choose>
                            <html:text name="Wadm008Form" property="DPcd" styleId="DPcd" value="${densoPlantName}"
                            style="width: 60px" readonly="true" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        <html:form styleId="Wadm008FormResult" action="/DensoUserRoleAssignmentAction.do" >
            <html:hidden styleId="methodResult"             property="method" value="doInitial" />
            <html:hidden styleId="dscId"                    property="dscId" />
            <html:hidden styleId="mode"                     property="mode" />
            <html:hidden styleId="DCd"                      property="DCd" />
            <html:hidden styleId="DPcd"                     property="DPcd" />
            <html:hidden styleId="roleCode"                 property="roleCode" />
            <html:hidden styleId="effectEnd"                property="effectEnd" />
            <html:hidden styleId="effectStart"              property="effectStart" />
            <html:hidden styleId="densoUserName"            property="densoUserName"/>
            <html:hidden styleId="firstName"                property="firstName" />
            <html:hidden styleId="middleName"               property="middleName"/>
            <html:hidden styleId="lastName"                 property="lastName" />
            <html:hidden styleId="roleNameSelected"         property="roleNameSelected" />
            <html:hidden styleId="roleCdSelected"           property="roleCdSelected" />
            <html:hidden styleId="countDelete"              property="countDelete" />
            <html:hidden property="returnMode" />
            <html:hidden property="cannotAddMessage" />
            <html:hidden property="cannotUpdateMessage" />
            
            <table style="cellspacing:0px;">
                <tr>
                    <th class="align-left">
                        <u>
                            <sps:label name="ASSIGN_ROLE_INFORMATION" />
                        </u>
                    </th>
                </tr>
                <c:if test="${not empty Wadm008Form.userRoleDetailList}">
                    <tr>
                        <td class="align-left" style="font-size:10px;">
                            <sps:label name="SYSTEM_GET_SELECTED_REC_EVERY_PAGE"/>
                        </td>
                    </tr>
                </c:if>
            </table>
            <div style="width: 994px;">
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:974px;word-wrap:break-word;">
                    <col style="width:25px;"/>
                    <col style="width:30px;"/>
                    <col style="width:100px;"/>
                    <col style="width:75px;"/>
                    <col style="width:75px;"/>
                    <col style="width:50px;"/>
                    <col style="width:65px;"/>
                    <col style="width:400px;"/>
                    <col style="width:75px;"/>
                    <col style="width:47px;"/>
                    <tr>
                        <th>
                            <sps:label name="ALL" />
                            <br />
                            <input type="checkbox" name="checkbox" value="checkbox" onclick="densoRoleCheckAll(this);"/>
                        </th>
                        <th class="align-center">
                            <sps:label name="NO" />
                        </th>
                        <th>
                            <sps:label name="ROLE_NAME" />
                        </th>
                        <th>
                            <sps:label name="EFFECTIVE_START" />
                        </th>
                        <th>
                            <sps:label name="EFFECTIVE_END" />
                        </th>
                        <th>
                            <sps:label name="ROLE_D_CD" />
                        </th>
                        <th>
                            <sps:label name="ROLE_D_P_CD" />
                        </th>
                        <th>
                            <sps:label name="ASSIGN_BY" />
                        </th>
                        <th>
                            <sps:label name="ASSIGN_DATE" />
                        </th>
                        <th>
                            <sps:label name="ACTION" />
                        </th>
                    </tr>
                </table>
            </div>
            <div style="width: 994px; height: 370px; overflow-y: scroll">
                <table class="display_data" id="tblBody" style="table-layout: fixed; width: 974px; word-wrap: break-word;" border="0">
                    <col style="width:25px;"/>
                    <col style="width:30px;"/>
                    <col style="width:100px;"/>
                    <col style="width:75px;"/>
                    <col style="width:75px;"/>
                    <col style="width:50px;"/>
                    <col style="width:65px;"/>
                    <col style="width:400px;"/>
                    <col style="width:75px;"/>
                    <col style="width:47px;"/>
                    <c:set var="startRow">
                        ${(Wadm008Form.pageNo * Wadm008Form.count) - Wadm008Form.count}
                    </c:set>
                    <c:forEach var="roleDenso" items="${Wadm008Form.userRoleDetailList}" begin="0" step="1" varStatus="status">
                        <c:choose>
                            <c:when test="${(status.index+1)% 2 == 0}">
                                <c:set var="rowColor">
                                    blueLight
                                </c:set>
                            </c:when>
                            <c:otherwise>
                                <c:set var="rowColor">
                                    
                                </c:set>
                            </c:otherwise>
                        </c:choose>
                        <tr class="${rowColor}">
                            <td class="align-center">
                                <input type="checkbox" name="userRoleChkbox" id="checkUserRole${status.index}"
                                    onclick="densoSetUserRoleList('${status.index}')"
                                    <c:if test="${not empty roleDenso.roleCdSelected}">
                                        checked
                                    </c:if>
                                />
                                <html:hidden name="roleDenso"  property="roleCdSelected" indexed="true" styleId="roleCdSelected${status.index}"/>
                                <!-- Parameter for selected value -->
                                <html:hidden name="roleDenso" property="dscId" indexed="true"/>
                                <html:hidden name="roleDenso" property="roleCd" indexed="true"/>
                                <html:hidden name="roleDenso" property="DCd" indexed="true"/>
                                <html:hidden name="roleDenso" property="DPcd" indexed="true"/>
                                <html:hidden name="roleDenso" property="lastUpdateDscId" indexed="true"/>
                                <html:hidden name="roleDenso" property="lastUpdateDatetimeScreen" indexed="true"/>
                                <html:hidden name="roleDenso" property="seqNo" indexed="true"/>
                            </td>
                            <td class="align-right">
                                ${status.index + startRow + 1}
                            </td>
                            <td>
                                ${roleDenso.roleName}
                            </td>
                            <td>
                                ${roleDenso.effectStartScreen}
                            </td>
                            <td>
                                ${roleDenso.effectEndScreen}
                            </td>
                            <td>
                                ${roleDenso.DCd}
                            </td>
                            <c:choose>
                                <c:when test="${'99' == roleDenso.DPcd}">
                                    <td class="align-left">
                                        <sps:label name="UNDEFINED" />
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td class="align-left">
                                        ${roleDenso.DPcd}
                                    </td>
                                </c:otherwise>
                            </c:choose>
                            <td>
                                <bean:write name="roleDenso" property="firstName" />
                                <c:if test="${not empty roleDenso.middleName}">
                                &nbsp;
                                    <bean:write name="roleDenso" property="middleName" />
                                </c:if>
                                <bean:write name="roleDenso" property="lastName" />
                            </td>
                            <td>
                                ${roleDenso.createDateScreen}
                            </td>
                            <td>
                                <a href="javascript:void(0)" onclick="return densoUpdateLink('Edit','${roleDenso.roleCd}',
                                '${roleDenso.effectStartScreen}','${roleDenso.effectEndScreen}',
                                '${roleDenso.DPcd}',
                                '${roleDenso.DCd}','${roleDenso.lastUpdateDatetimeScreen}',
                                '${roleDenso.seqNoScreen}'); return false;">
                                    <sps:label name="UPDATE" />
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
            <table class="display_criteria" style="width: 978px;">
                <tr>
                    <td class="align-left">
                        <ai:paging action="/SPS/DensoUserRoleAssignmentAction.do" formId="Wadm008FormResult" jump="true" />
                    </td>
                    <td class="align-right">
                        <input type="button" id="btnAdd"    value="${btnAdd}" class="select_button"" />
                        <input type="button" id="btnDelete" value="${btnDelete}" class="select_button" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="button" id="btnReturn" value="${btnReturn}" class="select_button" />
                    </td>
                </tr>
            </table>
        </html:form>
    </body>
</html>