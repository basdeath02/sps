<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wadm004_SupplierUserRoleAssignment.js" >
        </script>
        <script type="text/javascript">
            var labelReturn = '<sps:label name="RETURN" />';
            var labelDelete = '<sps:label name="DELETE_SELECTED_ITEMS" />';
            var msgConfirm= '<sps:message name="SP-W6-0001"/>';
        </script>
        <title>
            <sps:label name="SUPPLIER_USER_ROLE_ASSIGNMENT" />
        </title>
    </head>
    <body class="hideHorizontal">
        <html:form styleId="Wadm004FormCriteria" action="/SupplierUserRoleAssignmentAction.do" >
        <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden styleId="method" property="method" />
            <!-- Key for button. -->
            <c:set var="btnAdd">
                <sps:label name="ADD" />
            </c:set>
            <c:set var="btnReturn">
                <sps:label name="RETURN" />
            </c:set>
            <c:set var="btnDelete">
                <sps:label name="DELETE" />
            </c:set>
            <html:hidden styleId="dscId" property="dscId"/>
            <html:hidden styleId="supplierUserName" property="supplierUserName"/>
            <html:hidden styleId="firstName" property="firstName"/>
            <html:hidden styleId="middleName" property="middleName"/>
            <html:hidden styleId="lastName" property="lastName"/>
            <html:hidden styleId="mode" property="mode" />
            <html:hidden styleId="returnMode" property="returnMode" />
            <html:hidden styleId="roleCode" property="roleCode" />
            <html:hidden styleId="effectEnd" property="effectEnd" />
            <html:hidden styleId="effectStart" property="effectStart" />
            <html:hidden styleId="SPcd" property="SPcd" />
            <html:hidden styleId="SCd" property="SCd" value="${Wadm004Form.SCd}"/>
            <html:hidden styleId="companyName" property="companyName" value="${Wadm004Form.companyName}"/>
            <html:hidden styleId="supplierCodeName" property="supplierCodeName" />
            <html:hidden styleId="roleCdSelected" property="roleCdSelected" />
            <html:hidden styleId="roleNameSelected" property="roleNameSelected" />
            <html:hidden styleId="lastUpdateDatetimeScreen" property="lastUpdateDatetimeScreen" />
            <html:hidden styleId="countDelete" property="countDelete" />
            <html:hidden styleId="seqNo" property="seqNo" />
            <html:hidden styleId="cannotUpdateMessage" property="cannotUpdateMessage" />
            <html:hidden styleId="cannotAddMessage" property="cannotAddMessage" />
            <html:hidden property="isDCompany" />
            
            <table style="height:30px; border:0px; cellpadding:0px; cellspacing:0px; width:990px;" class="bottom">
                <tr>
                    <td>
                        <sps:label name="ADMIN_CONFIG" />
                        &gt;
                        <sps:label name="SUPPLIER_USER_INFORMATION" />
                        &gt;
                        <sps:label name="SUPPLIER_USER_INFORMATION" />
                        &gt;
                        <sps:label name="SUPPLIER_USER_ROLE_ASSIGNMENT" />
                    </td>
                    <td colspan="2" class="align-right">
                        <sps:label name="USER" />
                        &nbsp; : <bean:write name="Wadm004Form" property="userLogin" />
                        <html:hidden name="Wadm004Form" property="userLogin" />
                    </td>
                </tr>
            </table>
            <c:choose>
                <c:when test="${not empty Wadm004Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}" >
                <table style="width: 992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg"
                                items="${Wadm004Form.applicationMessageList}"
                                begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}" >
                                    ${appMsg.message} 
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            <fieldset class="fixwidth">
                <table class="display_criteria" style="border:0px; cellpadding:3px; cellspacing:1px; width:975px;">
                    <tr>
                        <th style="width:8%" class="align-right">
                            <sps:label name="DSC_ID" />
                            &nbsp;:
                        </th>
                        <td style="width:10%">
                            <html:text name="Wadm004Form" property="dscId" styleId="dscId" value="${Wadm004Form.dscId}"
                            styleClass="mandatory" size="15" style="width: 150px" readonly="true" />
                        </td>
                        <th style="width:9%">
                            <sps:label name="USER_NAME" />
                            &nbsp;:
                        </th>
                        <td style="width:50%">
                            <html:text name="Wadm004Form" property="supplierUserName"  styleId="supplierUserName" value="${Wadm004Form.supplierUserName}"
                            style="width: 640px" readonly="true" />
                        </td>
                    </tr>
                </table>
                <table class="display_criteria" style="border:0px; cellpadding:3px; cellspacing:1px; width:975px;">
                    <tr>
                        <th style="width:11%" class="align-right">
                            <sps:label name="S_COMPANY_NAME" />
                            &nbsp;:
                        </th>
                        <td style="width:45%">
                            <html:text name="Wadm004Form" property="companyName" styleId="companyName" 
                            value="${Wadm004Form.supplierCodeName} ${Wadm004Form.companyName}"
                            style="width: 745px" readonly="true" />
                        </td>
                        <th style="width:10%" class="align-right">
                            <sps:label name="S_P" />
                            &nbsp;:
                        </th>
                        <td style="width:10%">
                            <c:choose>
                                <c:when test="${'99' == Wadm004Form.SPcd}">
                                    <c:set var="supplierPlantName">
                                        <sps:label name="UNDEFINED" />
                                    </c:set>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="supplierPlantName">
                                        ${Wadm004Form.SPcd}
                                    </c:set>
                                </c:otherwise>
                            </c:choose>
                            <html:text name="WADM004Form" property="SPcd"  styleId="SPcd" value="${supplierPlantName}"
                            style="width: 70px" readonly="true" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        <html:form styleId="Wadm004FormResult"  action="/SupplierUserRoleAssignmentAction.do" >
            <html:hidden styleId="methodResult" property="method" />
            <html:hidden property="dscId" />
            <html:hidden property="mode" />
            <html:hidden property="supplierUserName"/>
            <html:hidden property="companyName" />
            <html:hidden property="supplierCodeName" />
            <html:hidden property="firstName"/>
            <html:hidden property="middleName"/>
            <html:hidden property="lastName"/>
            <html:hidden property="companyName"/>
            <html:hidden property="SCd" />
            <html:hidden property="SPcd" />
            <html:hidden property="roleCode" />
            <html:hidden property="effectEnd" />
            <html:hidden property="effectStart" />
            <html:hidden property="roleNameSelected" />
            <html:hidden property="roleCdSelected" />
            <html:hidden property="countDelete" />
            <html:hidden property="cannotUpdateMessage" />
            <html:hidden property="cannotAddMessage" />
            <html:hidden property="isDCompany" />
            <html:hidden property="seqNo" />
            <html:hidden property="lastUpdateDatetimeScreen" />
            <html:hidden property="returnMode" />
            
            <table cellspacing="0">
                <tr>
                    <th class="align-left">
                        <u>
                            <sps:label name="USER_ROLE_INFORMATION" />
                        </u>
                    </th>
                </tr>
                <c:if test="${not empty Wadm004Form.userRoleDetailList}">
                    <tr>
                        <td class="align-left" style="font-size:10px;">
                            <sps:label name="SYSTEM_GET_SELECTED_REC_EVERY_PAGE"/>
                        </td>
                    </tr>
                </c:if>
            </table>
            <div style="width: 994px;">
                <table class="display_data" id="tblBody" style="table-layout: fixed; width: 974px; word-wrap: break-word;">
                    <tr>
                        <th style="width:20px">
                            <sps:label name="ALL" />
                            <br />
                            <c:if test="${Wadm004Form.isDCompany == false}">
                                <input type="checkbox" name="userRoleAllChkbox" disabled="disabled"/>
                            </c:if>
                            <c:if test="${Wadm004Form.isDCompany == true}">
                                <input type="checkbox" name="checkbox" value="checkbox" onclick="wadm004CheckAll(this);"/>
                            </c:if>
                        </th>
                        <th style="width:5%" class="align-center">
                            <sps:label name="NO" />
                        </th>
                        <th style="width:10%">
                            <sps:label name="ROLE_NAME" />
                        </th>
                        <th style="width:8%">
                            <sps:label name="EFFECTIVE_START" />
                        </th>
                        <th style="width:8%">
                            <sps:label name="EFFECTIVE_END" />
                        </th>
                        <th style="width:5%">
                            <sps:label name="ROLE_S_P_CD" />
                        </th>
                        <th style="width:50%">
                            <sps:label name="ASSIGN_BY" />
                        </th>
                        <th style="width:8%">
                            <sps:label name="ASSIGN_DATE" />
                        </th>
                        <th style="width:5%">
                            <sps:label name="ACTION" />
                        </th>
                    </tr>
                </table>
            </div>
            <div style="width: 994px; height: 350px; overflow-y: scroll">
                <table class="display_data" id="tblBody" style="table-layout: fixed; width: 974px; word-wrap: break-word;">
                    <c:set var="startRow">
                        ${(Wadm004Form.pageNo * Wadm004Form.count) - Wadm004Form.count}
                    </c:set>
                    <c:forEach var="roleSupplier" items="${Wadm004Form.userRoleDetailList}" begin="0" step="1" varStatus="status">
                        <c:choose>
                            <c:when test="${(status.index+1)% 2 == 0}">
                                <c:set var="rowColor">
                                    blueLight
                                </c:set>
                            </c:when>
                            <c:otherwise>
                                <c:set var="rowColor">
                                    
                                </c:set>
                            </c:otherwise>
                        </c:choose>
                        <tr class="${rowColor}">
                            <td align="center" style="width:20px">
                                <c:if test="${Wadm004Form.isDCompany == false}">
                                    <input type="checkbox" name="userRoleChkbox" disabled="disabled"/>
                                </c:if>
                                <c:if test="${Wadm004Form.isDCompany == true}">
                                    <input type="checkbox" name="userRoleChkbox" id="checkUserRole${status.index}"
                                    onclick="wadm004setUserRoleList('${status.index}')"
                                        <c:if test="${not empty roleSupplier.roleCdSelected}">
                                            checked
                                        </c:if>
                                    />
                                </c:if>
                                <html:hidden name="roleSupplier"  property="roleCdSelected" indexed="true"  styleId="roleCdSelected${status.index}"/>
                                <!-- Parameter for selected value -->
                                <html:hidden name="roleSupplier" property="dscId"                   indexed="true"  value="${roleSupplier.dscId}"               styleId="dscId${status.index}"/>
                                <html:hidden name="roleSupplier" property="roleCd"                  indexed="true"  value="${roleSupplier.roleCd}"              styleId="roleCd${status.index}"/>
                                <html:hidden name="roleSupplier" property="SCd"                     indexed="true"  value="${roleSupplier.SCd}"                 styleId="SCd${status.index}"/>
                                <html:hidden name="roleSupplier" property="SPcd"                    indexed="true"  value="${roleSupplier.SPcd}"                styleId="SPcd${status.index}"/>
                                <html:hidden name="roleSupplier" property="lastUpdateDscId"         indexed="true"  value="${roleSupplier.lastUpdateDscId}"     styleId="lastUpdateDscId${status.index}"/>
                                <html:hidden name="roleSupplier" property="lastUpdateDatetimeScreen"        indexed="true"  value="${roleSupplier.lastUpdateDatetimeScreen}"    styleId="lastUpdateDatetimeScreen${status.index}"/>
                                <html:hidden name="roleSupplier" property="seqNo"                   indexed="true"  value="${roleSupplier.seqNo}"               styleId="seqNo${status.index}"/>
                            </td>
                            <td style="width:5%" class="align-right">
                                ${status.index + startRow + 1}
                            </td>
                            <td style="width:10%">
                                ${roleSupplier.roleName}
                                <html:hidden name="roleSupplier" property="roleName" indexed="true" />
                            </td>
                            <td style="width:8%">
                                ${roleSupplier.effectStartScreen}
                                <html:hidden name="roleSupplier" property="effectStartScreen" indexed="true" />
                            </td>
                            <td style="width:8%">
                                ${roleSupplier.effectEndScreen}
                                <html:hidden name="roleSupplier" property="effectEndScreen" indexed="true" />
                            </td>
                            <c:choose>
                                <c:when test="${'99' == roleSupplier.SPcd}">
                                    <td style="width:5%;" class="align-left">
                                        <sps:label name="UNDEFINED" />
                                        <html:hidden name="roleSupplier" property="SPcd" indexed="true" />
                                    </td>
                                </c:when>
                                <c:otherwise>
                                    <td class="align-left" style="width:5%;">
                                        ${roleSupplier.SPcd}
                                        <html:hidden name="roleSupplier" property="SPcd" indexed="true" />
                                    </td>
                                </c:otherwise>
                            </c:choose>
                            <td style="width:50%">
                                <bean:write name="roleSupplier" property="firstName" />
                                <c:if test="${not empty roleSupplier.middleName}">
                                &nbsp;
                                    <bean:write name="roleSupplier" property="middleName" />
                                </c:if>
                                <bean:write name="roleSupplier" property="lastName" />
                                
                                <html:hidden name="roleSupplier" property="firstName" indexed="true" />
                                <html:hidden name="roleSupplier" property="middleName" indexed="true" />
                                <html:hidden name="roleSupplier" property="lastName" indexed="true" />
                            </td>
                            <td style="width:8%">
                                ${roleSupplier.createDateScreen}
                                <html:hidden name="roleSupplier" property="createDateScreen" indexed="true" />
                            </td>
                            <td style="width:5%">
                                <c:if test="${Wadm004Form.isDCompany == false}">
                                    <sps:label name="UPDATE" />
                                </c:if>
                                <c:if test="${Wadm004Form.isDCompany == true}">
                                    <a href="javascript:void(0)" onclick="return wadm004UpdateLink('Edit','${roleSupplier.roleCd}',
                                    '${roleSupplier.effectStartScreen}','${roleSupplier.effectEndScreen}',
                                    '${roleSupplier.SPcd}',
                                    '${roleSupplier.SCd}','${roleSupplier.lastUpdateDatetimeScreen}','${roleSupplier.seqNoScreen}'); return false;">
                                        <sps:label name="UPDATE" />
                                    </a>
                                </c:if>
                                
                                <html:hidden name="roleSupplier" property="seqNoScreen" indexed="true" />
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
            <table style="width: 978px;">
                <tr>
                    <td align="left">
                        <ai:paging action="/SPS/SupplierUserRoleAssignmentAction.do" formId="Wadm004FormResult" jump="true" />
                    </td>
                    <td class="align-right">
                        <c:if test="${Wadm004Form.isDCompany == true}">
                            <input type="button" value="${btnAdd}"    id="btnAdd"    class="select_button" />
                            <input type="button" value="${btnDelete}" id="btnDelete" class="select_button" />
                        </c:if>
                        <c:if test="${Wadm004Form.isDCompany == false}">
                            <input type="button" value="${btnAdd}"    id="btnAdd"    class="select_button" disabled="disabled"/>
                            <input type="button" value="${btnDelete}" id="btnDelete" class="select_button" disabled="disabled"/>
                        </c:if>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="button" value="${btnReturn}" id="btnReturn" class="select_button"  />
                    </td>
                </tr>
            </table>
        </html:form>
    </body>
</html>
