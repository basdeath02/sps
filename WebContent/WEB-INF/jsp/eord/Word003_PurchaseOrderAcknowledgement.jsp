<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <jsp:useBean id="spsCons" class="com.globaldenso.asia.sps.common.constant.SupplierPortalConstant" />
        <script type="text/javascript" src="./js/business/Word003_PurchaseOrderAcknowledgement.js" ></script>
        <script type="text/javascript">
            var msgConfirmSaveAndSend = '<sps:message name="SP-W6-0009"/>';
        </script>
        <title><sps:label name="PURCHASE_ORDER_ACKNOWLEDGEMENT" /></title>
    </head>
    <body class="hideHorizontal">
        <html:form styleId="Word003Form" action="/PurchaseOrderAcknowledgementAction.do" >
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden styleId="method" property="method" value="doInitial" />
            <!-- Key for paging. -->
            <html:hidden styleId="currentPage" property="currentPage"/>
            <html:hidden styleId="row"  property="row"/>
            <html:hidden styleId="totalCount" property="totalCount"/>
            <html:hidden styleId="poId" property="poId"/>
            <html:hidden styleId="periodType" property="periodType"/>
            <html:hidden styleId="selectedSPn" property="selectedSPn"/>
            <html:hidden styleId="selectedDPn" property="selectedDPn"/>
            <html:hidden styleId="selectedActionMode" property="selectedActionMode"/>
            <html:hidden styleId="poStatus" property="poStatus"/>
            <html:hidden styleId="enablePreviewPending" property="enablePreviewPending"/>
            <html:hidden styleId="cannotPreviewPendingMessage" property="cannotPreviewPendingMessage"/>
            
            <c:set var="btnPreviewPending">
                <sps:label name="PREVIEW_PENDING" />
            </c:set>
            <c:set var="btnSaveSend">
                <sps:label name="SAVE_AND_SEND" />
            </c:set>
            <c:set var="btnReturn">
                <sps:label name="RETURN" />
            </c:set>
            
            <%-- [IN054] add new buton DENSO Reply --%>
            <c:set var="btnDensoReply">
                <sps:label name="DENSO_REPLY" />
            </c:set>
            
            
            <c:choose>
                <c:when test="${1 == Word003Form.periodType && spsCons.PO_STATUS_ISSUED == Word003Form.poStatus}">
                    <c:set var="disableSaveAndSend">
                    
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="disableSaveAndSend">
                        disabled
                    </c:set>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${1 == Word003Form.enablePreviewPending && spsCons.PO_STATUS_ISSUED == Word003Form.poStatus}">
                    <c:set var="disablePreviewPending">
                    
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="disablePreviewPending">
                        disabled
                    </c:set>
                </c:otherwise>
            </c:choose>
            
            <%-- [IN059] Check enable DENSO Reply button --%>
            <c:choose>
                <c:when test="${1 == Word003Form.enableDensoReply}">
                    <c:set var="disableDensoReply">
                    
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="disableDensoReply">
                        disabled
                    </c:set>
                </c:otherwise>
            </c:choose>
            
            <table style="height:30px;width:990px;border-width:0px;padding:0px" class="bottom">
                <tr>
                    <td>
                        <sps:label name="ORDERING" />
                        &gt;
                        <sps:label name="PURCHASE_ORDER_INFORMATIOIN" />
                        &gt;
                        <sps:label name="PURCHASE_ORDER_ACKNOWLEDGEMENT" />
                    </td>
                    <td colspan="2" class="align-right" >
                        <sps:label name="USER" />
                        &nbsp; : <bean:write name="Word003Form" property="userLogin" />
                        <html:hidden name="Word003Form" property="userLogin" />
                    </td>
                </tr>
            </table>
            <c:choose>
                <c:when test="${not empty Word003Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}" >
                <table style="width: 992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg"
                                items="${Word003Form.applicationMessageList}"
                                begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}" >
                                    ${appMsg.message} 
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            <fieldset class="fixwidth">
                <table style="width:540px; border-width:0px; padding:3px; cellspacing:1px" class="display_criteria" >
                    <tr>
                        <th style="width:100px" >
                            <sps:label name="SPS_PO_NO" />
                            &nbsp;:
                        </th>
                        <td style="width:80px" >
                            <html:text name="Word003Form" property="spsPoNo" value="${Word003Form.spsPoNo}" style="width:80px" styleId="spsPoNo" readonly="true"/>
                        </td>
                        <th style="width:100px" >
                            <sps:label name="PERIOD_TYPE" />
                            &nbsp;:
                        </th>
                        <td style="width:80px" >
                            <html:text name="Word003Form" property="periodTypeName" value="${Word003Form.periodTypeName}" style="width:80px" readonly="true"/>
                        </td>
                        <th style="width:100px" >
                            <sps:label name="PO_TYPE" />
                            &nbsp;:
                        </th>
                        <td style="width:80px" >
                            <html:text name="Word003Form" property="poType" value="${Word003Form.poType}" style="width:80px" readonly="true"/>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <c:choose>
                <c:when test="${0 == Word003Form.max}">
                    <c:set var="resultStyle">
                        visibility: hidden;
                    </c:set>
                    <c:set var="legendStyle">
                        display:none;margin:0;padding-top:2px;
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="resultStyle">
                        visibility: visible;
                    </c:set>
                    <c:set var="legendStyle">
                        margin:0;padding-top:2px;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="result" style="${resultStyle}">
                <div style="width:994px;">
                    <table class="display_data" id="tblBody" style="table-layout:fixed;width:974px;word-wrap:break-word;">
                        <tr>
                            <th style="width:4%" >
                                <sps:label name="NO" />
                            </th>
                            <th style="width:12%" scope="col">
                                <sps:label name="D_PART_NO" />
                            </th>
                            <th style="width:16%" scope="col">
                                <sps:label name="S_PART_NO" />
                            </th>
                            <th style="width:8%" scope="col">
                                <sps:label name="ORDER_METHOD" />
                            </th>
                            <th style="width:8%" scope="col">
                                <sps:label name="PO_STATUS_BY_PART_NO" />
                            </th>
                            <th style="width:8%" scope="col">
                                <sps:label name="ORDER_QTY" />
                            </th>
                            <th style="width:4%" scope="col">
                                <sps:label name="UM" />
                            </th>
                            <th style="width:8%" scope="col">
                                <sps:label name="FIRM_QTY" />
                            </th>
                            <th style="width:8%" scope="col">
                                <sps:label name="FORECAST_QTY" />
                            </th>
                            <th style="width:8%" scope="col">
                                <sps:label name="START_PERIOD" />
                            </th>
                            <th style="width:8%" scope="col">
                                <sps:label name="END_PERIOD" />
                            </th>
                            <th style="width:8%">
                                <sps:label name="ACTION" />
                            </th>
                        </tr>
                    </table>
                </div>
                <div style="width:994px;height:360px;overflow-y:scroll">
                    <table class="display_data" id="tblBody" style="table-layout:fixed;width:974px;word-wrap:break-word;border-width:0px">
                        <c:set var="startRow">
                            ${(Word003Form.pageNo * Word003Form.count) - Word003Form.count + 1}
                        </c:set>
                        <c:forEach var="poDetail" items="${Word003Form.poDetailList}" begin="0" step="1" varStatus="status">
                            <c:choose>
                                <c:when test="${(status.index+1)% 2 == 0}">
                                    <c:set var="rowColor">
                                        blueLight
                                    </c:set>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="rowColor">
                                        
                                    </c:set>
                                </c:otherwise>
                            </c:choose>
                            <tr class="${rowColor}">
                                <td align="center" style="width:4%">
                                    ${startRow + status.index}
                                    <!-- Parameter for Acknowledge -->
                                    <html:hidden name="poDetail" property="poId" indexed="true" />
                                    <html:hidden name="poDetail" property="DCd" indexed="true" />
                                    <html:hidden name="poDetail" property="DPcd" indexed="true" />
                                    <html:hidden name="poDetail" property="lastUpdateDatetime" indexed="true" />
                                    <html:hidden name="poDetail" property="actionMode" indexed="true" />
                                </td>
                                <td style="width:12%">
                                    <html:hidden name="poDetail" property="DPn" indexed="true" write="true" />
                                </td>
                                <td style="width:16%">
                                    <html:hidden name="poDetail" property="SPn" indexed="true" write="true" />
                                </td>
                                <td style="width:8%">
                                    <html:hidden name="poDetail" property="orderMethodName" indexed="true" write="true" />
                                    <html:hidden name="poDetail" property="orderMethod" indexed="true" />
                                </td>
                                <td style="width:8%">
                                    <html:hidden name="poDetail" property="pnStatus" indexed="true" write="true" />
                                </td>
                                <td style="width:8%;text-align:right">
                                    <fmt:formatNumber type="number" pattern="#,###.##" value="${poDetail.orderQty}" />
                                    <html:hidden name="poDetail" property="orderQty" indexed="true" />
                                </td>
                                <td style="width:4%">
                                    <html:hidden name="poDetail" property="unitOfMeasure" indexed="true" write="true" />
                                </td>
                                <td style="width:8%;text-align:right">
                                    <logic:notEmpty name="poDetail" property="firmQty">
                                        <fmt:formatNumber type="number" pattern="#,###.##" value="${poDetail.firmQty}" />
                                    </logic:notEmpty>
                                    <html:hidden name="poDetail" property="firmQty" indexed="true" />
                                </td>
                                <td style="width:8%;text-align:right">
                                    <logic:notEmpty name="poDetail" property="forecastQty">
                                        <fmt:formatNumber type="number" pattern="#,###.##" value="${poDetail.forecastQty}" />
                                    </logic:notEmpty>
                                    <html:hidden name="poDetail" property="forecastQty" indexed="true" />
                                </td>
                                <td style="width:8%">
                                    <%-- FIX : wrong dateformat --%>
                                    <%-- <fmt:formatDate pattern="yyyy/MM/dd" value="${poDetail.startPeriodDate}"/> --%>
                                    ${poDetail.startPeriodDateShow}
                                    <html:hidden name="poDetail" property="startPeriodDateShow" indexed="true" />
                                    
                                    <html:hidden name="poDetail" property="startPeriodDate" indexed="true" />
                                </td>
                                <td style="width:8%">
                                    <%-- FIX : wrong dateformat --%>
                                    <%-- <fmt:formatDate pattern="yyyy/MM/dd" value="${poDetail.endPeriodDate}"/> --%>
                                    ${poDetail.endPeriodDateShow}
                                    <html:hidden name="poDetail" property="endPeriodDateShow" indexed="true" />
                                    
                                    <html:hidden name="poDetail" property="endPeriodDate" indexed="true" />
                                </td>
                                <td style="width:8%" class="align-left">
                                    <a href="javascript:void(0)" onclick="return word003OpenWORD004('${poDetail.SPn}','${poDetail.DPn}','${poDetail.actionMode}');return false;" >
                                        <%-- Start : [IN054] Multi language for link --%>
                                        <%-- ${poDetail.actionMode} --%>
                                        <c:choose>
                                            <c:when test="${cons.MODE_VIEW == poDetail.actionMode}">
                                                <sps:label name="VIEW" />
                                            </c:when>
                                            <c:otherwise>
                                                <sps:label name="EDIT" />
                                            </c:otherwise>
                                        </c:choose>
                                        <%-- End : [IN054] Multi language for link --%>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
            <table style="width: 994px;" >
                <tr>
                    <td align="left">
                        <ai:paging action="/SPS/PurchaseOrderAcknowledgementAction.do" formId="Word003Form" jump="true" />
                    </td>
                    <td class="align-right">
                        <input type="button" id="btnPreviewPending" value="${btnPreviewPending}" class="select_button" style="width:120px;" ${disablePreviewPending} />
                        <input type="button" id="btnDensoReply"     value="${btnDensoReply}"     class="select_button" ${disableDensoReply} />
                        <input type="button" id="btnSaveSend"       value="${btnSaveSend}"       class="select_button" ${disableSaveAndSend} />
                        <input type="button" id="btnReturn"         value="${btnReturn}"         class="select_button" />
                    </td>
                </tr>
            </table>
            <div id="displayLegend" style="${legendStyle}">
                <table bgcolor="#EEEEEE" style="width:994px;border:0;">
                    <tr>
                        <td style="font-size:10px;">
                            <span style="font-weight: bold;">
                                <sps:label name="LEGEND" />
                                <span style="text-decoration: underline;">
                                    <sps:label name="INVOICE_STATUS"/>
                                </span>
                            </span>
                            <sps:label name="STATUS_ISS" />,&nbsp;<sps:label name="STATUS_PED" />,&nbsp;<sps:label name="STATUS_ACK" />,&nbsp;<sps:label name="STATUS_FAC" />
                        </td>
                        <td class="align-right">
                            <a href="javascript:void(0)"  onclick="word003DownloadLegendFile();return false;" style="font-size:10px;">
                                &gt;&gt;&nbsp;<sps:label name="MORE" />
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </html:form>
        
        <html:form styleId="Word003FormDownload" action="/PurchaseOrderAcknowledgementAction.do" >
            <html:hidden styleId="methodDownload" property="method" />
            
            <html:hidden property="currentPage"/>
            <html:hidden property="row"/>
            <html:hidden property="totalCount"/>
            <html:hidden property="poId"/>
            <html:hidden property="periodType"/>
            <html:hidden property="selectedSPn"/>
            <html:hidden property="selectedDPn"/>
            <html:hidden property="selectedActionMode"/>
            <html:hidden property="poStatus"/>
            <html:hidden property="userLogin" />
            
            <html:hidden property="spsPoNo"/>
            <html:hidden property="periodTypeName"/>
            <html:hidden property="poType"/>
            
            <html:hidden property="pageNo"/>
            <html:hidden property="enablePreviewPending"/>
            
            <html:hidden property="cannotPreviewPendingMessage"/>
        </html:form>
    </body>
</html>
