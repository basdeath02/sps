<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Word004_SupplierPromisedDueSubmittion.js" ></script>
        <script type="text/javascript">
            var modeEdit = '${cons.MODE_EDIT}';
            var msgConfirmReturn = '<sps:message name="SP-W6-0008"/>';
            var varEnableCheckPoDueId = "${Word004Form.enableCheckPoDueId}";
            // [IN054] add new mode for screen Supplier Promised Due Submission
            var modeView = '${cons.MODE_VIEW}';
            var modeReply = '${cons.MODE_REPLY}';
        </script>
        <title><sps:label name="SUPPLIER_PROMISED_DUE_SUBMITTION" /></title>
    </head>
    
    <body oncontextmenu="return false">
        <html:form styleId="Word004Form" action="/SupplierPromisedDueSubmittionAction.do" >
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden styleId="method" property="method" />
            <html:hidden styleId="currentPage" property="currentPage"/>
            <html:hidden styleId="row"  property="row"/>
            <html:hidden styleId="max"  property="max"/>
            <html:hidden styleId="totalCount" property="totalCount"/>
            <html:hidden styleId="actionMode" property="actionMode"/>
            
            <html:hidden styleId="poId" property="poId"/>
            <html:hidden styleId="spsPoNo" property="spsPoNo"/>
            <html:hidden styleId="periodType" property="periodType"/>
            
            <c:forEach var="pendingReasonCode" items="${Word004Form.pendingReasonCodeList}" begin="0" step="1" varStatus="subStatus">
                <html:hidden name="pendingReasonCode" property="miscCode" indexed="true"/>
                <html:hidden name="pendingReasonCode" property="miscValue" indexed="true"/>
            </c:forEach>
            
            <%-- [IN054] Save combobox item in Action Form --%>
            <c:forEach var="acceptRejectCode" items="${Word004Form.acceptRejectCodeList}" begin="0" step="1" varStatus="subStatus">
                <html:hidden name="acceptRejectCode" property="miscCode" indexed="true"/>
                <html:hidden name="acceptRejectCode" property="miscValue" indexed="true"/>
            </c:forEach>
            
            <c:set var="btnReturn">
                <sps:label name="RETURN" />
            </c:set>
            <c:set var="btnRegister">
                <sps:label name="REGISTER" />
            </c:set>
            
            <%-- [IN054] Add new button. --%>
            <c:set var="btnSave">
                <sps:label name="SAVE" />
            </c:set>
            
            <table style="height:30px; width:990px; border-width:0px; padding:0px; cellspacing:0px;" class="bottom">
                <tr>
                    <td>
                        <sps:label name="ORDERING" />
                        &gt; <sps:label name="PURCHASE_ORDER_INFORMATIOIN" />
                        &gt; <sps:label name="SUPPLIER_PROMISED_DUE_SUBMITTION" />
                    </td>
                    <td colspan="2" class="align-right" >
                        <sps:label name="USER" />
                        &nbsp; : <bean:write name="Word004Form" property="userLogin" />
                        <html:hidden name="Word004Form" property="userLogin" />
                    </td>
                </tr>
            </table>

            <logic:notEmpty name="Word004Form" property="applicationMessageList">
                <div style="margin-bottom:5px">
                    <table style="width:992px;" class="tableMessage">
                        <tr>
                            <td>
                                <c:forEach var="appMsg" items="${Word004Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                    <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                        <c:set var="messageClass">
                                            ui-messageSuccess
                                        </c:set>
                                    </c:if>
                                    <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                        <c:set var="messageClass">
                                            ui-messageError
                                        </c:set>
                                    </c:if>
                                    <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                        <c:set var="messageClass">
                                            ui-messageWarning
                                        </c:set>
                                    </c:if>
                                    <span class="${messageClass}">
                                        ${appMsg.message} 
                                        <br />
                                    </span>
                                </c:forEach>
                            </td>
                        </tr>
                    </table>
                </div>
            </logic:notEmpty>
            <fieldset class="fixwidth">
                <table style="width:920px; border:0px; padding:3px; cellspacing=1px;" class="display_criteria" >
                    <tr>
                        <th style="width:100px;">
                            <sps:label name="SPS_PO_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Word004Form" property="spsPoNo" style="width:80px" styleId="spsPoNoText" readonly="true"/>
                        </td>
                        <th style="width:80px;">
                            <sps:label name="D_PART_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Word004Form" property="DPn" style="width:120px" styleId="DPnText" readonly="true"/>
                        </td>
                        <th style="width:80px;">
                            <sps:label name="S_PART_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Word004Form" property="SPn" style="width:120px" styleId="SPnText" readonly="true"/>
                        </td>
                        <th style="width:100px;">
                            <sps:label name="PERIOD_TYPE" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Word004Form" property="periodTypeName" style="width:70px" styleId="periodTypeNameText" readonly="true"/>
                        </td>
                        <th style="width:80px;">
                            <sps:label name="PO_TYPE" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Word004Form" property="poType" style="width:70px" styleId="poTypeText" readonly="true"/>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <div style="width:994px;">
                <%-- Start : [IN054] set Reason column width --%>
                <c:choose>
                    <c:when test="${cons.MODE_REPLY == Word004Form.actionMode}">
                        <c:set var="reasonStyle">
                            width:40%;
                        </c:set>
                        <c:set var="reasonCboStyle">
                            width:335px;
                        </c:set>
                    </c:when>
                    <c:otherwise>
                        <c:set var="reasonStyle">
                            width:54%
                        </c:set>
                        <c:set var="reasonCboStyle">
                            width:450px;
                        </c:set>
                    </c:otherwise>
                </c:choose>
                <%-- End : [IN054] add control to reply Pending P/O --%>
                <table style="width:994px">
                    <tr>
                        <td class="align-left" style="vertical-align:bottom;font-size:10px;">
                            <c:if test="${not empty Word004Form.purchaseOrderDueList}">
                                <sps:label name="SYSTEM_GET_SELECTED_REC_ONLY_CURRENT_PAGE"/>
                            </c:if>
                        </td>
                    </tr>
                </table>
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:974px;word-wrap:break-word;">
                    <tr>
                        <th style="width:20px;">
                            <sps:label name="ALL" />
                            <br />
                            <input type="checkbox" name="checkbox" id="chkall" value="checkbox" />
                        </th>
                        <th style="width:10%;" scope="col" >
                            <sps:label name="DELIVERY_DATE" />
                        </th>
                        <th style="width:5%;" scope="col" >
                            <sps:label name="FLAG" />
                        </th>
                        <th style="width:10%;" scope="col" >
                            <sps:label name="ORDER_QTY" />
                        </th>
                        <th style="width:13%;" scope="col" >
                            <sps:label name="PROPOSED_DATE" />
                        </th>
                        <th style="width:12%;" scope="col" >
                            <sps:label name="PROPOSED_QTY" />
                        </th>
                        <%-- Start : [IN054] add control to reply Pending P/O --%>
                        <%--
                        <th style="width:54%;" scope="col" >
                            <sps:label name="REASON" />
                        </th>
                        --%>
                        <th style="width:40%;" scope="col" >
                            <sps:label name="REASON" />
                        </th>
                        <th style="width:13%;" scope="col" >
                            <sps:label name="ACCEPT_REJECT" />
                        </th>
                        <%-- End : [IN054] add control to reply Pendin P/O --%>
                    </tr>
                </table>
            </div>
            <c:choose>
                <c:when test="${0 == Word004Form.max}">
                    <c:set var="resultStyle">
                        visibility: hidden;
                    </c:set>
                    <c:set var="legendStyle">
                        display:none;margin:0;padding-top:2px;
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="resultStyle">
                        visibility: visible;
                    </c:set>
                    <c:set var="legendStyle">
                        margin:0;padding-top:2px;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="result" style="${resultStyle}">
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:974px;word-wrap:break-word;">
                    <c:forEach var="purchaseOrderDue" items="${Word004Form.purchaseOrderDueList}" begin="0" step="1" varStatus="status">
                        <c:choose>
                            <c:when test="${(status.index+1)% 2 == 0}">
                                <c:set var="rowColor">
                                    blueLight
                                </c:set>
                            </c:when>
                            <c:otherwise>
                                <c:set var="rowColor">
                                        
                                </c:set>
                            </c:otherwise>
                        </c:choose>
                        <tr class="${rowColor}">
                            <td style="width:20px;" class="align-center">
                                <input type="checkbox" name="chkPoDue" id="chkPoDue${status.index}" onclick="word004SetActivePoDue('${status.index}')"/>
                                
                                <html:hidden name="purchaseOrderDue" property="changePoDueFlag" indexed="true" styleId="changePoDueFlag${status.index}" />
                                <html:hidden name="purchaseOrderDue" property="spsTPoDomain.poId" indexed="true" />
                                <html:hidden name="purchaseOrderDue" property="spsTPoDomain.spsPoNo" indexed="true" />
                                <html:hidden name="purchaseOrderDue" property="spsTPoDomain.lastUpdateDatetime" indexed="true" />
                                <html:hidden name="purchaseOrderDue" property="DPn" indexed="true" />
                                <html:hidden name="purchaseOrderDue" property="SPn" indexed="true" />
                                <html:hidden name="purchaseOrderDue" property="spsTPoDueDomain.dueId" indexed="true" />
                                <html:hidden name="purchaseOrderDue" property="spsTPoDueDomain.lastUpdateDatetime" indexed="true" />
                                <html:hidden name="purchaseOrderDue" property="detailUpdateDatetime" indexed="true" />
                                
                                <%-- [IN054] Get Mark Pending Flag to enable Accept/Reject --%>
                                <html:hidden name="purchaseOrderDue" property="spsTPoDueDomain.markPendingFlg" styleId="markPendingFlg${status.index}" indexed="true" />

                            </td>
                            <td style="width:10%;" class="align-left">
                                <%-- FIX : wrong dateformat --%>
                                <%-- <fmt:formatDate pattern="yyyy/MM/dd" value="${purchaseOrderDue.spsTPoDueDomain.etd}"/> --%>
                                ${purchaseOrderDue.etdShow}
                                <html:hidden name="purchaseOrderDue" property="etdShow" indexed="true" />
                                
                                <logic:notEmpty name="purchaseOrderDue" property="spsTPoDueDomain.dueDate">
                                    <html:hidden name="purchaseOrderDue" property="spsTPoDueDomain.dueDate" indexed="true" />
                                </logic:notEmpty>
                                <html:hidden name="purchaseOrderDue" property="spsTPoDueDomain.etd" indexed="true" />
                            </td>
                            <td style="width:5%;" class="align-left">
                                ${purchaseOrderDue.reportTypeFlgDisplay}
                                <html:hidden name="purchaseOrderDue" property="spsTPoDueDomain.reportTypeFlg" indexed="true" />
                                <html:hidden name="purchaseOrderDue" property="reportTypeFlgDisplay" indexed="true" />
                            </td>
                            <td style="width:10%;" class="align-right">
                                <logic:notEmpty name="purchaseOrderDue" property="spsTPoDueDomain.orderQty">
                                    <fmt:formatNumber type="number" pattern="#,###.##" value="${purchaseOrderDue.spsTPoDueDomain.orderQty}" />
                                    <html:hidden name="purchaseOrderDue" property="spsTPoDueDomain.orderQty" indexed="true" />
                                </logic:notEmpty>
                            </td>
                            <td style="width:13%;" class="align-right">
                                <c:choose>
                                    <c:when test="${empty purchaseOrderDue.stringProposedDateBackup}">
                                        <c:set var="varProposedDate">
                                            ${purchaseOrderDue.stringProposedDate}
                                        </c:set>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="varProposedDate">
                                            ${purchaseOrderDue.stringProposedDateBackup}
                                        </c:set>
                                    </c:otherwise>
                                </c:choose>
                                <sps:calendar id="stringProposedDate${status.index}" 
                                    name="purchaseOrderDue[${status.index}].stringProposedDate"
                                    value="${varProposedDate}"/>
                                <logic:notEmpty name="purchaseOrderDue" property="spsTPoDueDomain.spsProposedDueDate">
                                    <html:hidden name="purchaseOrderDue" property="spsTPoDueDomain.spsProposedDueDate" indexed="true" />
                                </logic:notEmpty>
                                <html:hidden name="purchaseOrderDue" property="stringProposedDateBackup" styleId="stringProposedDateBackup${status.index}" indexed="true" />
                            </td>
                            <td style="width:12%;" class="align-right">
                                <c:choose>
                                    <c:when test="${empty purchaseOrderDue.stringProposedQtyBackup}">
                                        <c:set var="varProposedQty">
                                            ${purchaseOrderDue.stringProposedQty}
                                        </c:set>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="varProposedQty">
                                            ${purchaseOrderDue.stringProposedQtyBackup}
                                        </c:set>
                                    </c:otherwise>
                                </c:choose>
                                <input type="text" value="${varProposedQty}" name="purchaseOrderDue[${status.index}].stringProposedQty"
                                    id="stringProposedQty${status.index}" style="width:95px;text-align:right;padding:2px" class="number"
                                    onblur="addComma(this);"
                                    onkeypress="return PositiveDoubleFilter(event);"
                                    onfocus="removeComma('stringProposedQty${status.index}');" />
                                <logic:notEmpty name="purchaseOrderDue" property="spsTPoDueDomain.spsProposedQty">
                                    <html:hidden name="purchaseOrderDue" property="spsTPoDueDomain.spsProposedQty" indexed="true" />
                                </logic:notEmpty>
                                <html:hidden name="purchaseOrderDue" property="stringProposedQtyBackup" styleId="stringProposedQtyBackup${status.index}" indexed="true" />
                            </td>
                            
                            <%-- [IN054] custom style for Reason column --%>
                            <%-- <td style="width:54%;" class="align-left"> --%>
                            <td style="width:40%" class="align-left">
                                <c:choose>
                                    <c:when test="${empty purchaseOrderDue.pendingReasonBackup}">
                                        <c:set var="varPendingReason">
                                            ${purchaseOrderDue.spsTPoDueDomain.spsPendingReasonCd}
                                        </c:set>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="varPendingReason">
                                            ${purchaseOrderDue.pendingReasonBackup}
                                        </c:set>
                                    </c:otherwise>
                                </c:choose>
                                
                                <%-- [IN054] custom style for Reason column --%>
                                <%--
                                <html:select name="purchaseOrderDue" property="spsTPoDueDomain.spsPendingReasonCd" value="${varPendingReason}"
                                    styleId="spsPendingReasonCd${status.index}" style="width:450px" indexed="true" >
                                --%>
                                <html:select name="purchaseOrderDue" property="spsTPoDueDomain.spsPendingReasonCd" value="${varPendingReason}"
                                    styleId="spsPendingReasonCd${status.index}" style="width:335px;" indexed="true" >
                                    <c:forEach var="pendingReasonCode" items="${Word004Form.pendingReasonCodeList}" begin="0" step="1" varStatus="subStatus">
                                        <html:option value="${pendingReasonCode.miscCode}">
                                            ${pendingReasonCode.miscCode}:${pendingReasonCode.miscValue}
                                        </html:option>
                                    </c:forEach>
                                </html:select>
                                <html:hidden name="purchaseOrderDue" property="pendingReason" indexed="true" />
                                <html:hidden name="purchaseOrderDue" property="pendingReasonBackup" styleId="pendingReasonBackup${status.index}" indexed="true" />
                            </td>
                            <%-- Start : [IN054] add control to reply Pendin P/O --%>
                            <td style="width:13%;" class="align-left">
                                <c:choose>
                                    <c:when test="${empty purchaseOrderDue.acceptRejectBackup}">
                                        <c:set var="varAcceptReject">
                                            ${purchaseOrderDue.spsTPoDueDomain.densoReplyFlg}
                                        </c:set>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="varAcceptReject">
                                            ${purchaseOrderDue.acceptRejectBackup}
                                        </c:set>
                                    </c:otherwise>
                                </c:choose>
                                <html:select name="purchaseOrderDue" property="spsTPoDueDomain.densoReplyFlg" value="${varAcceptReject}"
                                    styleId="densoReplyFlg${status.index}"  styleClass="mandatory"  style="width:115px" indexed="true" >
                                    <html:option value="">
                                        <sps:label name="PLEASE_SELECT_CRITERIA" />
                                    </html:option>
                                    <c:forEach var="acceptRejectCode" items="${Word004Form.acceptRejectCodeList}" begin="0" step="1" varStatus="subStatus">
                                        <html:option value="${acceptRejectCode.miscCode}">
                                            ${acceptRejectCode.miscValue}
                                        </html:option>
                                    </c:forEach>
                                </html:select>
                                <html:hidden name="purchaseOrderDue" property="acceptReject" indexed="true" />
                                <html:hidden name="purchaseOrderDue" property="acceptRejectBackup" styleId="acceptRejectBackup${status.index}" indexed="true" />
                            </td>
                            <%-- End : [IN054] add control to reply Pendin P/O --%>
                        </tr>
                    </c:forEach>
                </table>
            </div>
            <table style="width: 994px;">
                <tr>
                    <td class="align-right">
                        <%-- [IN054] Add new button --%>
                        <input type="button" id="btnSave" value="${btnSave}" class="select_button" />
                        
                        <input type="button" id="btnRegister" value="${btnRegister}" class="select_button" />
                        <input type="button" id="btnReturn" value="${btnReturn}" class="select_button" />
                    </td>
                </tr>
            </table>
            <div id="displayLegend" style="${legendStyle}">
                <table bgcolor="#EEEEEE" style="width:994px;border:0;">
                    <tr>
                        <td style="font-size:10px;">
                            <span style="font-weight: bold;">
                                <sps:label name="LEGEND"/>
                                <span style="text-decoration: underline;">
                                    <sps:label name="FLAG"/>
                                </span>
                            </span>
                            <sps:label name="FLAG_D" />,&nbsp;<sps:label name="FLAG_W" />,&nbsp;<sps:label name="FLAG_M" />&nbsp;
                        </td>
                        <td class="align-right">
                            <a href="javascript:void(0)"  onclick="return word004DownloadLegendFile();return false;" style="font-size:10px;">
                                &gt;&gt; <sps:label name="MORE" />
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </html:form>
    </body>
</html>
