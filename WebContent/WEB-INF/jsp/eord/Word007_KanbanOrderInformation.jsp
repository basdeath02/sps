<%@ include file="../includes/jsp_header.jspf"%>
<html>
<head>
<%@ include file="../includes/html_header.jspf"%>
<jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
<script type="text/javascript" src="./js/business/Word007_KanbanOrder.js">
</script>
<title>
    <sps:label name="KANBAN_ORDER_INFO" />
</title>
<script type="text/javascript">
    var labelAll = '<sps:label name="ALL_CRITERIA" />';
    var msgPrinted = '<sps:message name="SP-W2-0006"/>';
</script>
</head>
<body class="hideHorizontal">
    <html:form styleId="Word007FormCriteria"
        action="/KanbanOrderInformationAction.do">
        <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
        <html:hidden styleId="method" property="method" />
        <html:hidden property="doId" styleId="doId" />
        <html:hidden property="pdfFileId" styleId="pdfFileId" />
        <html:hidden property="pdfType" styleId="pdfType" />
        <html:hidden property="pdfFileKbTagPrintDate" styleId="pdfFileKbTagPrintDate" />
        <html:hidden property="mode" styleId="mode" />
        <!-- Key for paging. -->
        <html:hidden styleId="currentPage" property="currentPage" />
        <html:hidden styleId="row" property="row" />
        <html:hidden styleId="totalCount" property="totalCount" />
        
        <html:hidden styleId="cannotResetMessage" property="cannotResetMessage"/>
        <html:hidden styleId="cannotDownloadCsvMessage" property="cannotDownloadCsvMessage"/>
        
        <c:forEach var="supplierAuthen" items="${Word007Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
            <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
            <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="densoAuthen" items="${Word007Form.densoAuthenList}" begin="0" step="1" varStatus="status">
            <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
            <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
        </c:forEach>
        
        <c:forEach var="revisionItem" items="${Word007Form.revisionList}" begin="0" step="1" varStatus="status">
            <html:hidden name="revisionItem" property="miscCode" indexed="true"/></input>
            <html:hidden name="revisionItem" property="miscValue" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="shipmentStatusItem" items="${Word007Form.shipmentStatusList}" begin="0" step="1" varStatus="status">
            <html:hidden name="shipmentStatusItem" property="miscCode" indexed="true"/></input>
            <html:hidden name="shipmentStatusItem" property="miscValue" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="transportModeItem" items="${Word007Form.transportModeList}" begin="0" step="1" varStatus="status">
            <html:hidden name="transportModeItem" property="miscCode" indexed="true"/></input>
            <html:hidden name="transportModeItem" property="miscValue" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="companySupplierItem" items="${Word007Form.companySupplierList}" begin="0" step="1" varStatus="status">
            <html:hidden name="companySupplierItem" property="vendorCd" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="companyDensoItem" items="${Word007Form.companyDensoList}" begin="0" step="1" varStatus="status">
            <html:hidden name="companyDensoItem" property="DCd" indexed="true"/></input>
            <html:hidden name="companyDensoItem" property="companyName" indexed="true"/></input>
        </c:forEach>
        
        <table style="margin-left: -2px; margin-bottom: -4px;">
            <tr>
                <td>
                    <table style="height: 30px; width: 990px; border-width: 0px; padding: 0px; margin-top: -1.8px; margin-bottom: -1.8px;" class="bottom">
                        <tr>
                            <td style="padding-bottom: 1px;">
                                <sps:label name="ORDERING" />
                                &gt; 
                                <sps:label name="KANBAN_ORDER_INFORMATION" />
                            </td>
                            <td colspan="2" class="align-right" style="padding-bottom: 1px;">
                                <sps:label name="USER" />
                                &nbsp; : 
                                <bean:write name="Word007Form" property="userLogin" />
                            </td>
                        </tr>
                    </table>
                    <c:choose>
                        <c:when test="${not empty Word007Form.applicationMessageList}">
                            <c:set var="displayErrorMessage">
                                
                            </c:set>
                        </c:when>
                        <c:otherwise>
                            <c:set var="displayErrorMessage">
                                display: none;
                            </c:set>
                        </c:otherwise>
                    </c:choose>
                    <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}" >
                        <table style="width: 992px" class="tableMessage">
                            <tr>
                                <td id="errorTotal">
                                    <c:forEach var="appMsg" items="${Word007Form.applicationMessageList}"
                                        begin="0" step="1" varStatus="status">
                                        <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                            <c:set var="messageClass">
                                                ui-messageSuccess
                                            </c:set>
                                        </c:if>
                                        <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                            <c:set var="messageClass">
                                                ui-messageError
                                            </c:set>
                                        </c:if>
                                        <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                            <c:set var="messageClass">
                                                ui-messageWarning
                                            </c:set>
                                        </c:if>
                                        <span class="${messageClass}" >
                                            ${appMsg.message} 
                                        </span>
                                        <br />
                                    </c:forEach>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset class="fixwidth">
                        <c:set var="lbAll">
                            <sps:label name="ALL_CRITERIA" />
                        </c:set>
                        <c:set var="lbSearch">
                            <sps:label name="SEARCH" />
                        </c:set>
                        <c:set var="lbReset">
                            <sps:label name="RESET" />
                        </c:set>
                        <c:set var="lbDownload">
                            <sps:label name="DOWNLOAD" />
                        </c:set>
                        <table class="display_criteria" id="ui-displayCriteria">
                            <tr>
                                <th style="width: 100px;" class="align-right">
                                    <sps:label name="S_CD" />
                                    &nbsp;:
                                </th>
                                <td style="width: 140px">
                                    <html:select style="width: 135px" name="Word007Form" styleId="sCd" property="vendorCd" styleClass="mandatory">
                                        <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
			                            <c:forEach var="companySupplierItem" items="${Word007Form.companySupplierList}" begin="0" step="1" varStatus="status">
			                                <html:option value="${companySupplierItem.vendorCd}">
			                                    ${companySupplierItem.vendorCd}
			                                </html:option>
			                            </c:forEach>
                                    </html:select>
                                </td>
                                <th style="width: 100px" class="align-right">
                                    <sps:label name="S_P" />
                                    &nbsp;:
                                </th>
                                <td style="width: 140px">
                                    <html:select styleId="sPcd" style="width: 135px" name="Word007Form" styleClass="mandatory" property="SPcd">
			                            <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
			                            <c:forEach var="plantSupplierItem" items="${Word007Form.plantSupplierList}" begin="0" step="1" varStatus="status">
			                                <html:option value="${plantSupplierItem.SPcd}">
			                                    ${plantSupplierItem.SPcd}
			                                </html:option>
			                            </c:forEach>
                                    </html:select>
                                </td>
                                <th style="width: 100px" class="align-right">
                                    <sps:label name="D_CD" />
                                    &nbsp;:
                                </th>
                                <td style="width: 140px">
                                    <html:select styleId="dCd" style="width: 135px" name="Word007Form" styleClass="mandatory" property="DCd">
			                            <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
			                            <c:forEach var="companyDensoItem" items="${Word007Form.companyDensoList}" begin="0" step="1" varStatus="status">
			                                <html:option value="${companyDensoItem.DCd}">
			                                    ${companyDensoItem.DCd}
			                                </html:option>
			                            </c:forEach>
                                    </html:select>
                                </td>
                                <th style="width: 100px" class="align-right">
                                    <sps:label name="D_P" />
                                    &nbsp;:
                                </th>
                                <td style="width: 140px">
                                    <html:select styleId="dPcd" style="width: 140px" name="Word007Form" styleClass="mandatory" property="DPcd">
			                            <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
			                            <c:forEach var="plantDensoItem" items="${Word007Form.plantDensoList}" begin="0" step="1" varStatus="status">
			                                <html:option value="${plantDensoItem.DPcd}">
			                                    ${plantDensoItem.DPcd}
			                                </html:option>
			                            </c:forEach>
                                    </html:select>
                                </td>
                            </tr>
                        </table>
                        <table class="display_criteria" id="ui-displayCriteria" style="padding-left:1px;border-spacing:0;">
                            <tr>
                                <th style="width: 100px" class="align-right">
                                    <sps:label name="ISSUE_DATE" />
                                    &nbsp;:
                                </th>
                                <td style="width: 241px;">
                                    <sps:calendar name="issueDateFrom" id="issueDateFrom" value="${Word007Form.issueDateFrom}" />
                                    -
                                    <sps:calendar name="issueDateTo" id="issueDateTo" value="${Word007Form.issueDateTo}" />
                                </td>
                                <th style="width: 87px" class="align-right">
                                    <sps:label name="DELIVERY_DATE" />
                                    &nbsp;:
                                </th>
                                <td style="width: 235px;">
                                    <sps:calendar name="deliveryDateFrom" id="deliveryDateFrom" value="${Word007Form.deliveryDateFrom}" />
                                    -
                                    <sps:calendar name="deliveryDateTo" id="deliveryDateTo" value="${Word007Form.deliveryDateTo}" />
                                </td>
                                <th style="width: 87px;" class="align-right">
                                    <sps:label name="DELIVERY_TIME" />
                                    &nbsp;:
                                </th>
                                <td style="width:235px">
                                    <html:text name="Word007Form" property="deliveryTimeFrom" style="width:50px"
                                        onfocus="removeColonFromTime('deliveryTimeFrom');return false;"
                                        onblur="addColonToTime('deliveryTimeFrom');return false;" styleId="deliveryTimeFrom"/>
                                        -
                                    <html:text name="Word007Form" property="deliveryTimeTo" style="width:50px"
                                        onfocus="removeColonFromTime('deliveryTimeTo');return false;"
                                        onblur="addColonToTime('deliveryTimeTo');return false;" styleId="deliveryTimeTo"/>
                                </td>
                            </tr>
                        </table>
                        <table class="display_criteria" id="ui-displayCriteria">
                            <tr>
                                <th style="width: 100px" class="align-right">
                                    <sps:label name="SHIP_DATE" />
                                    &nbsp;:
                                </th>
                                <td style="width: 241px;">
                                    <sps:calendar name="shipDateFrom" id="shipDateFrom" value="${Word007Form.shipDateFrom}" />
                                    -
                                    <sps:calendar name="shipDateTo" id="shipDateTo" value="${Word007Form.shipDateTo}" />
                                </td>
                                <th style="width: 50px">
                                    <sps:label name="REV" />
                                    &nbsp;:
                                </th>
                                <td style="width: 90px">
                                    <html:select styleId="revision" name="Word007Form" property="revision" style="width: 85px">
                                        <html:option value="">${lbAll}</html:option>
			                            <c:forEach var="revisionItem" items="${Word007Form.revisionList}" begin="0" step="1" varStatus="status">
			                                <html:option value="${revisionItem.miscCode}">
			                                    ${revisionItem.miscValue}
			                                </html:option>
			                            </c:forEach>
                                    </html:select>
                                </td>
                                <th style="width: 115px">
                                    <sps:label name="SHIPMENT_STATUS" />
                                    &nbsp;:
                                </th>
                                <td style="width: 130px">
                                    <html:select styleId="shipmentStatus" name="Word007Form" property="shipmentStatus" style="width: 135px">
                                        <html:option value="">${lbAll}</html:option>
			                            <c:forEach var="shipmentStatusItem" items="${Word007Form.shipmentStatusList}" begin="0" step="1" varStatus="status">
			                                <html:option value="${shipmentStatusItem.miscCode}">
			                                    ${shipmentStatusItem.miscValue}
			                                </html:option>
			                            </c:forEach>
                                    </html:select>
                                </td>
                                <th style="width: 85px">
                                    <sps:label name="TM" />
                                    &nbsp;:
                                </th>
                                <td style="width: 140px">
                                    <html:select styleId="transportMode" name="Word007Form" property="transportMode" style="width: 135px">
                                        <html:option value="">${lbAll}</html:option>
			                            <c:forEach var="transportModeItem" items="${Word007Form.transportModeList}" begin="0" step="1" varStatus="status">
			                                <html:option value="${transportModeItem.miscCode}">
			                                    ${transportModeItem.miscValue}
			                                </html:option>
			                            </c:forEach>
                                    </html:select>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <sps:label name="ROUTE" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <html:text name="Word007Form" property="routeNo" style="width: 134px;" />
                                </td>
                                <th>
                                    <sps:label name="DEL" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <html:text name="Word007Form" property="del" style="width: 84px;" />
                                </td>
                                <th>
                                    <sps:label name="D/O_NO" />
                                    &nbsp;:
                                </th>
                                <td>
                                    <html:text name="Word007Form" property="spsDoNo" style="width: 134px;" />
                                </td>
                                <th style="width:100px">
                                    <sps:label name="CIGMA_DO#" />
                                    &nbsp;:
                                </th>
                                <td style="width:145px">
                                    <html:text name="Word007Form" property="cigmaDoNo" style="width: 134px;" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <table style="width: 994px">
                        <tr>
                            <td colspan="6" class="align-right">
                                <input name="Input"
                                    type="button" value="${lbSearch}" class="select_button" id="btnSearch" />
                                <input name="Input" type="button" id="btnReset" value="${lbReset}"
                                    class="select_button" />
                                <input name="btnDownload"
                                    id="btnDownload" type="button" value="${lbDownload }"
                                    class="select_button" />
                            </td>
                        </tr>
                    </table>
                    <ai:pagingoption maxRow="5" />
    </html:form>
    <html:form styleId="KanbanOrderInformationActionResultForm"
        action="/KanbanOrderInformationAction.do">
        <html:hidden property="method" styleId="methodResult" />
        <html:hidden property="doId" styleId="doIdResult" />
        <html:hidden property="vendorCd" styleId="sCdResult" />
        <html:hidden property="SPcd"
            styleId="sPcdResult" />
        <html:hidden property="DCd" styleId="dCdResult" />
        <html:hidden property="DPcd" styleId="dPcdResult" />
        <html:hidden property="issueDateFrom" styleId="issueDateFromResult" />
        <html:hidden property="issueDateTo" styleId="issueDateToResult" />
        <html:hidden property="deliveryDateFrom"
            styleId="deliveryDateFromResult" />
        <html:hidden property="deliveryDateTo" styleId="deliveryDateToResult" />
        <html:hidden property="shipDateFrom" styleId="shipDateFromResult" />
        <html:hidden property="shipDateTo" styleId="shipDateToResult" />
        <html:hidden property="revision" styleId="revisionResult" />
        <html:hidden property="shipmentStatus" styleId="shipmentStatusResult" />
        <html:hidden property="transportMode" styleId="transportModeResult" />
        <html:hidden property="routeNo" styleId="routeNoResult" />
        <html:hidden property="del" styleId="delNoResult" />
        <html:hidden property="spsDoNo" styleId="spsDoNoResult" />
        <html:hidden property="cigmaDoNo" styleId="cigmaDoNoResult" />
        <html:hidden property="pdfFileId" styleId="pdfFileIdResult" />
        <html:hidden property="pdfType" styleId="pdfTypeResult" />
        <html:hidden property="pdfFileKbTagPrintDate" styleId="pdfFileKbTagPrintDateResult" />
        
        <html:hidden property="cannotResetMessage"/>
        <html:hidden property="cannotDownloadCsvMessage"/>
        
        <c:forEach var="supplierAuthen" items="${Word007Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
            <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
            <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="densoAuthen" items="${Word007Form.densoAuthenList}" begin="0" step="1" varStatus="status">
            <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
            <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
        </c:forEach>
        
        <c:forEach var="revisionItem" items="${Word007Form.revisionList}" begin="0" step="1" varStatus="status">
            <html:hidden name="revisionItem" property="miscCode" indexed="true"/></input>
            <html:hidden name="revisionItem" property="miscValue" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="shipmentStatusItem" items="${Word007Form.shipmentStatusList}" begin="0" step="1" varStatus="status">
            <html:hidden name="shipmentStatusItem" property="miscCode" indexed="true"/></input>
            <html:hidden name="shipmentStatusItem" property="miscValue" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="transportModeItem" items="${Word007Form.transportModeList}" begin="0" step="1" varStatus="status">
            <html:hidden name="transportModeItem" property="miscCode" indexed="true"/></input>
            <html:hidden name="transportModeItem" property="miscValue" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="companySupplierItem" items="${Word007Form.companySupplierList}" begin="0" step="1" varStatus="status">
            <html:hidden name="companySupplierItem" property="vendorCd" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="companyDensoItem" items="${Word007Form.companyDensoList}" begin="0" step="1" varStatus="status">
            <html:hidden name="companyDensoItem" property="DCd" indexed="true"/></input>
            <html:hidden name="companyDensoItem" property="companyName" indexed="true"/></input>
        </c:forEach>
        
        <c:choose>
            <c:when
                test="${null == Word007Form.kanbanInformation}">
                <c:set var="visibilityFlag">
                    hidden
                </c:set>
            </c:when>
            <c:otherwise>
                <c:set var="visibilityFlag">
                    visible
                </c:set>
            </c:otherwise>
        </c:choose>
        <div id="result" style="visibility: ${visibilityFlag}; padding: 0px;">
            <div style="width: 1075px;">
                <table class="display_data" id="tblBody"
                    style="table-layout: fixed; word-wrap: break-word;">
                    <tr>
                        <th style="width: 20px;">
                            <sps:label name="NO" />
                        </th>
                        <th style="width: 70px;" scope="col">
                            <sps:label name="ISSUE_DATE" />
                        </th>
                        <th style="width: 95px;" scope="col">
                            <sps:label name="DELIVERY_DATE" />
                        </th>
                        <th style="width: 95px;" scope="col">
                            <sps:label name="SHIP_DATE" />
                        </th>
                        <th style="width: 90px;" scope="col">
                            <sps:label name="D/O_NO" />
                        </th>
                        <th style="width: 25px;" scope="col">
                            <sps:label name="REV" />
                        </th>
                        <th style="width: 90px;" scope="col">
                            <sps:label name="CIGMA_DO#" />
                        </th>
                        <th style="width: 40px;" scope="col">
                            <sps:label name="ROUTE" />
                        </th>
                        <th style="width: 40px;" scope="col">
                            <sps:label name="DEL" />
                        </th>
                        <th style="width: 45px;" scope="col">
                            <sps:label name="S_CD" />
                        </th>
                        <th style="width: 45px;" scope="col">
                            <sps:label name="S_P" />
                        </th>
                        <th style="width: 45px;" scope="col">
                            <sps:label name="D_CD" />
                        </th>
                        <th style="width: 45px;" scope="col">
                            <sps:label name="D_P" />
                        </th>
                        <th style="width: 40px;" scope="col">
                            <sps:label name="TM" />
                        </th>
                        <th style="width: 70px;" scope="col">
                            <sps:label name="SHIPMENT_STATUS" />
                        </th>
                        <th style="width: 30px;" scope="col">
                            <sps:label name="DO" />
                        </th>
                        <th style="width: 30px;" scope="col">
                            <sps:label name="KB" />
                        </th>
                    </tr>
                </table>
            </div>
            <c:set var="startRow">
                ${(Word007Form.pageNo * Word007Form.count) - Word007Form.count}
            </c:set>
            <div style="width: 1075px; height: 278px; overflow-y: scroll;">
                <table class="display_data" id="tblBody"
                    style="table-layout: fixed; word-wrap: break-word;"
                    border="0">
                    <c:forEach var="kanbanInformationList"
                        items="${Word007Form.kanbanInformation}" begin="0"
                        step="1" varStatus="status">
                        <c:set var="numbercount">
                            ${numbercount + 1}
                        </c:set>
                        <c:choose>
                            <c:when test="${numbercount % 2 == 0}">
                                <c:set var="color">
                                    blueLight
                                </c:set>
                            </c:when>
                            <c:otherwise>
                                <c:set var="color">
                                </c:set>
                            </c:otherwise>
                        </c:choose>
                    <tr class="${color}">
                        <td style="width: 20px;" class="align-right">
                            ${startRow + (status.index + 1)}
                        </td>
                        <td style="width: 70px;" class="align-left">
                            <%-- FIX : wrong dateformat --%>
                            <%-- <fmt:formatDate pattern="yyyy/MM/dd" value="${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.doIssueDate}"/> --%>
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.doIssueDateShow}
                        </td>
                        <td style="width: 95px;" class="align-left">
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.deliveryDatetimeUtc}
                        </td>
                        <td style="width: 95px;" class="align-left">
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.shipDatetimeUtc}
                        </td>
                        <td style="width: 90px;" class="align-left">
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.spsDoNo}
                        </td>
                        <td style="width: 25px;" class="align-left">
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.revision}
                        </td>
                        <td style="width: 90px;" class="align-left">
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.cigmaDoNo}
                        </td>
                        <td style="width: 40px;" class="align-left">
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.truckRoute}
                        </td>
                        <td style="width: 40px;" class="align-left">
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.del}
                        </td>
                        <td style="width: 45px;" class="align-left">
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.vendorCd}
                        </td>
                        <td style="width: 45px;" class="align-left">
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.SPcd}
                        </td>
                        <td style="width: 45px;" class="align-left">
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.DCd}
                        </td>
                        <td style="width: 45px;" class="align-left">
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.DPcd}
                        </td>
                        <td style="width: 40px;" class="align-left">
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.tm}
                        </td>
                        <td style="width: 70px;" class="align-left">
                            ${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.shipmentStatus}
                        </td>
                        <td style="width: 30px;" class="align-center">
                            <a href="#">
                                <img
                                    src="css/images/pdf.jpg"  alt="${lbDownload }" style="width: 19" height="20" onclick="word007ViewPdf('${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.doId}'); return false;"/>
                            </a>
                        </td>
                        <td style="width: 30px;" class="align-center">
                           <c:choose>
                                <c:when test="${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.oneWayKanbanTagFlag == '1'}">
                                    <c:choose>
                                        <c:when test="${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.partTag > '0' && kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.tagOutput != '4'}">
                                            <c:choose>
                                                <c:when test="${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.shipmentStatus == 'CPS' && kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.receiveStatus == 'RCP'}">
                                                    <img src="css/images/pdfGray.jpg" style="width: 19" height="20" alt="${lbDownload }" />
                                                </c:when>
                                                <c:otherwise>
                                                    <a href="#">
                                                        <img src="css/images/pdf.jpg" style="width: 19" height="20" alt="${lbDownload }" onclick="word007ViewKanbanPdf('${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.doId}','KB','${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.pdfFileKbTagPrintDate}'); return false;" />
                                                    </a>
                                                   <html:hidden property="pdfFileKbTagPrintDateFlag" value="${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.pdfFileKbTagPrintDate == null ? 'N' : 'Y'}" styleId="pdfFileKbTagPrintDateFlag${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.doId}" />
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                            -
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${kanbanInformationList.kanbanOrderInformationDoDetailReturnDomain.tagOutput == '4'}">
                                            -
                                        </c:when>
                                        <c:otherwise>
                                            &nbsp;
                                        </c:otherwise>
                                    </c:choose>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    </c:forEach>
                </table>
            </div>
            <table style="width: 994">
                <tr>
                    <td>
                        <ai:paging
                            action="/SPS/KanbanOrderInformationAction.do"
                            formId="KanbanOrderInformationActionResultForm" jump="true" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="displayLegend" style="visibility: ${visibilityFlag}; margin: 0; padding: 0;">
            <table style="width: 994px; border: 0; background-color: #EEEEEE">
                <tr>
                    <td style="font-size: 10px;">
                        <b>
                            <sps:label name="LEGEND" />
                            <span
                                style="text-decoration: underline">
                                <sps:label name="SHIPMENT_STATUS" />
                            </span>
                        </b>
                        <%--LEGEND FIXED --%>
                        <sps:label name="ISS_PTS_CPS_CCL" />
                    </td>
                    <td class="align-right">
                        <a href="#" style="font-size: 10px;" id="moreLegend">
                            &gt;&gt; 
                            <sps:label name="MORE" />
                        </a>
                    </td>
                </tr>
            </table>
        </div>
        </td>
        </tr>
        </table>
    </html:form>
</body>
</html>
