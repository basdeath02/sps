<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Word005_PurchaseOrderNotification.js" ></script>
        <title><sps:label name="PURCHASE_ORDER_NOTIFICATION" /></title>
    </head>
    <body style="width:930px;height:300px;" oncontextmenu="return false">
        <table style="width:924px;">
            <tr style="height:50px">
                <td background="css/images/bgHeader.jpg">
                    <img src="css/images/denso_logo.jpg" style="width:143px;height:20px;" />
                </td>
            </tr>
        </table>
        <table style="width:800px; height:30px; padding:0px; cellspacing:0px;" class="bottom">
            <tr>
                <td>
                    <sps:label name="ORDERING" />
                    &gt; <sps:label name="PURCHASE_ORDER_INFORMATIOIN" />
                    &gt; <sps:label name="PURCHASE_ORDER_NOTIFICATION" />
                </td>
            </tr>
        </table>
        <html:form styleId="Word005Form" action="/PurchaseOrderNotificationAction.do" >
            <html:hidden styleId="returnFlag"  property="returnFlag"/>
            <html:hidden styleId="cannotReturnMessage"  property="cannotReturnMessage"/>
            <c:set var="btnReturn">
                <sps:label name="RETURN" />
            </c:set>
            <c:choose>
                <c:when test="${not empty Word005Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}" >
                <table style="width: 992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg"
                                items="${Word005Form.applicationMessageList}"
                                begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}" >
                                    ${appMsg.message} 
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            <table  style="width:400px; padding:3px; cellspacing:1px;" class="display_criteria">
                <tr>
                    <th style="width:80px;">
                        <sps:label name="SPS_PO_NO" />
                        &nbsp;:
                    </th>
                    <td>
                        <html:text name="Word005Form" property="spsPoNo" style="width:80px" styleId="spsPoNoText" readonly="true"/>
                    </td>
                </tr>
            </table>
            <div style="width:924px;">
                <table class="display_data" id="tblBody" style="table-layout:fixed; width:900px; word-wrap:break-word;">
                    <tr>
                        <th style="width:12%;" >
                            <sps:label name="D_PART_NO" />
                        </th>
                        <th  style="width:15%;" >
                            <sps:label name="S_PART_NO" />
                        </th>
                        <th style="width:8%;" scope="col">
                            <sps:label name="DELIVERY_DATE" />
                        </th>
                        <th style="width:8%;" scope="col">
                            <sps:label name="ORDER_QTY" />
                        </th>
                        <th style="width:8%;" scope="col">
                            <sps:label name="PROPOSED_DATE" />
                        </th>
                        <th style="width:8%;" scope="col">
                            <sps:label name="PROPOSED_QTY" />
                        </th>
                        <th style="width:30%;" scope="col">
                            <sps:label name="REASON" />
                        </th>
                    </tr>
                </table>
            </div>
            <c:choose>
                <c:when test="${0 == Word005Form.max}">
                    <c:set var="resultStyle">
                        visibility: hidden; width:924px;height:200px;overflow-y:scroll
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="resultStyle">
                        visibility: visible; width:924px;height:200px;overflow-y:scroll
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="result" style="${resultStyle}">
                <table class="display_data" id="tblBody" style="table-layout:fixed; width:900px; word-wrap:break-word;">
                    <c:forEach var="notification" items="${Word005Form.purchaseOrderNotificationList}" begin="0" step="1" varStatus="status">
                        <c:choose>
                            <c:when test="${(status.index+1)% 2 == 0}">
                                <c:set var="rowColor">
                                    blueLight
                                </c:set>
                            </c:when>
                            <c:otherwise>
                                <c:set var="rowColor">
                                        
                                </c:set>
                            </c:otherwise>
                        </c:choose>
                        <tr class="${rowColor}">
                            <td style="width:12%;" class="align-left">
                                ${notification.spsTPoDetail.DPn}
                            </td>
                            <td style="width:15%;" class="align-left">
                                ${notification.spsTPoDetail.SPn}
                            </td>
                            <td style="width:8%;" class="align-left">
                                ${notification.stringDueDate}
                            </td>
                            <td style="width:8%;" class="align-right">
                                ${notification.stringOrderQty}
                            </td>
                            <td style="width:8%;" class="align-left">
                                ${notification.stringProposedDueDate}
                            </td>
                            <td style="width:8%;" class="align-right">
                                ${notification.stringProposedQty}
                            </td>
                            <td style="width:30%;" class="align-left">
                                ${notification.spsTPoDue.spsPendingReasonCd} : ${notification.pendingReason}
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
            <table style="width: 900px;">
                <tr>
                    <td class="align-right" colspan="6">
                        <input type="button" id="btnReturn" value="${btnReturn}" class="select_button" />
                    </td>
                </tr>
            </table>
        </html:form>
    </body>
</html>
