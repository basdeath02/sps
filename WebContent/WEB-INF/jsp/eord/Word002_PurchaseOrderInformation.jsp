<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Word002_PurchaseOrderInformation.js" ></script>
        <script type="text/javascript">
            var labelAll = '<sps:label name="ALL_CRITERIA" />';
            var labelPleaseSelect = '<sps:label name="PLEASE_SELECT_CRITERIA" />';
            var miscCodeAll = '${cons.MISC_CODE_ALL}';
        </script>
        <title><sps:label name="PURCHASE_ORDER" /></title>
    </head>
    <body class="hideHorizontal">
        <html:form styleId="Word002FormCriteria" action="/PurchaseOrderInformationAction.do" >
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden styleId="method" property="method" />
            <!-- Key for paging. -->
            <html:hidden styleId="currentPage" property="currentPage"/>
            <html:hidden styleId="row"  property="row"/>
            <html:hidden styleId="totalCount" property="totalCount"/>
            
            <c:forEach var="supplierAuthen" items="${Word002Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
            </c:forEach>
            <c:forEach var="densoAuthen" items="${Word002Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
            </c:forEach>
            
            <html:hidden styleId="cannotResetMessage" property="cannotResetMessage"/>
            <html:hidden styleId="cannotDownloadCsvMessage" property="cannotDownloadCsvMessage"/>
    
            <c:forEach var="poStatusItem" items="${Word002Form.poStatusList}" begin="0" step="1" varStatus="status">
                <html:hidden name="poStatusItem" property="miscCode" indexed="true"/></input>
                <html:hidden name="poStatusItem" property="miscValue" indexed="true"/></input>
            </c:forEach>
            <c:forEach var="poTypeItem" items="${Word002Form.poTypeList}" begin="0" step="1" varStatus="status">
                <html:hidden name="poTypeItem" property="miscCode" indexed="true"/>
                <html:hidden name="poTypeItem" property="miscValue" indexed="true"/>
            </c:forEach>
            <c:forEach var="periodTypeItem" items="${Word002Form.periodTypeList}" begin="0" step="1" varStatus="status">
                <html:hidden name="periodTypeItem" property="miscCode" indexed="true"/>
                <html:hidden name="periodTypeItem" property="miscValue" indexed="true"/>
            </c:forEach>
            <c:forEach var="viewPdfItem" items="${Word002Form.viewPdfList}" begin="0" step="1" varStatus="status">
                <html:hidden name="viewPdfItem" property="miscCode" indexed="true"/>
                <html:hidden name="viewPdfItem" property="miscValue" indexed="true"/>
            </c:forEach>
            <c:forEach var="companySupplierItem" items="${Word002Form.companySupplierList}" begin="0" step="1" varStatus="status">
                <html:hidden name="companySupplierItem" property="vendorCd" indexed="true"/>
            </c:forEach>
            <c:forEach var="companyDensoItem" items="${Word002Form.companyDensoList}" begin="0" step="1" varStatus="status">
                <html:hidden name="companyDensoItem" property="DCd" indexed="true"/>
                <html:hidden name="companyDensoItem" property="companyName" indexed="true"/>
            </c:forEach>
            <c:set var="btnSearch">
                <sps:label name="SEARCH" />
            </c:set>
            <c:set var="btnReset">
                <sps:label name="RESET" />
            </c:set>
            <c:set var="btnDownload">
                <sps:label name="DOWNLOAD" />
            </c:set>
            <c:set var="btnAck">
                <sps:label name="ACKNOWLEDGE" />
            </c:set>
            <table style="height:30px;width:990px;border-width:0px;padding:0px" class="bottom">
                <tr>
                    <td>
                        <sps:label name="ORDERING" />
                        &gt;
                        <sps:label name="PURCHASE_ORDER_INFORMATIOIN" />
                    </td>
                    <td colspan="2" class="align-right" >
                        <sps:label name="USER" />
                        &nbsp; : <bean:write name="Word002Form" property="userLogin" />
                        <html:hidden name="Word002Form" property="userLogin" />
                    </td>
                </tr>
            </table>
            <c:choose>
                <c:when test="${not empty Word002Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}" >
                <table style="width: 992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg"
                                items="${Word002Form.applicationMessageList}"
                                begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}" >
                                    ${appMsg.message} 
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            <fieldset class="fixwidth">
                <c:set var="lbAll">
                    <sps:label name="ALL_CRITERIA" />
                </c:set>
                <table style="width:975px;border-width:0px;padding:0px" class="display_criteria">
                    <tr>
                        <th style="width:100px">
                            <sps:label name="S_CD" />
                            &nbsp;:
                        </th>
                        <td style="width:281px">
                            <html:select name="Word002Form" property="vendorCd" styleId="vendorCd" styleClass="mandatory" style="width:120px">
                                <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
                                <c:forEach var="companySupplierItem" items="${Word002Form.companySupplierList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${companySupplierItem.vendorCd}">
                                        ${companySupplierItem.vendorCd}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th style="width:121px">
                            <sps:label name="S_P" />
                            &nbsp;:
                        </th>
                        <td style="width:150px">
                            <html:select name="Word002Form" property="SPcd" styleId="SPcd" styleClass="mandatory" style="width:120px">
                                <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
                                <c:forEach var="plantSupplierItem" items="${Word002Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${plantSupplierItem.SPcd}">
                                        ${plantSupplierItem.SPcd}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th style="width:120px">
                            <sps:label name="D_CD" />
                            &nbsp;:
                        </th>
                        <td style="width:120px">
                            <html:select name="Word002Form" property="DCd" styleId="DCd" styleClass="mandatory" style="width:120px">
                                <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
                                <c:forEach var="companyDensoItem" items="${Word002Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${companyDensoItem.DCd}">
                                        ${companyDensoItem.DCd}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th style="width:112px">
                            <sps:label name="D_P" />
                            &nbsp;:
                        </th>
                        <td style="width:120px">
                            <html:select name="Word002Form" property="DPcd" styleId="DPcd" styleClass="mandatory" style="width:120px">
                                <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
                                <c:forEach var="plantDensoItem" items="${Word002Form.plantDensoList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${plantDensoItem.DPcd}">
                                        ${plantDensoItem.DPcd}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                    </tr>
                </table>
                <table style="width:975px;border-width:0px;padding:0px" class="display_criteria">
                    <tr>
                        <th style="width:100px">
                            <sps:label name="ISSUE_DATE" />
                            &nbsp;:
                        </th>
                        <td style="width:290px">
                            <sps:calendar name="issuedDateFrom" id="issuedDateFrom"  value="${Word002Form.issuedDateFrom}"/>
                            -<sps:calendar name="issuedDateTo" id="issuedDateTo" value="${Word002Form.issuedDateTo}"/>
                        </td>
                        <th style="width:112px">
                            <sps:label name="PO_STATUS" />
                            &nbsp;:
                        </th>
                        <td style="width:150px">
                            <html:select name="Word002Form" property="poStatus" style="width:140px" styleClass="mandatory" >
                                <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
                                <c:forEach var="poStatusItem" items="${Word002Form.poStatusList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${poStatusItem.miscCode}">
                                        ${poStatusItem.miscCode}:${poStatusItem.miscValue}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th style="width:120px" align="right">
                            <sps:label name="PO_TYPE" />
                            &nbsp;:
                        </th>
                        <td style="width:150px" >
                            <html:select name="Word002Form" property="poType" style="width:140px">
                                <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
                                <c:forEach var="poTypeItem" items="${Word002Form.poTypeList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${poTypeItem.miscCode}">
                                        ${poTypeItem.miscCode}:${poTypeItem.miscValue}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th style="width:120px" align="right">
                            <sps:label name="PERIOD_TYPE" />
                            &nbsp;:
                        </th>
                        <td style="width:90px">
                            <html:select name="Word002Form" property="periodType" style="width:85px">
                                <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
                                <c:forEach var="periodTypeItem" items="${Word002Form.periodTypeList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${periodTypeItem.miscCode}">
                                        ${periodTypeItem.miscValue}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                    </tr>
                    <tr>
                        <th align="right">
                            <sps:label name="DATA_TYPE" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Word002Form" property="dataType" styleId="viewPdfCombo" style="width:100px" >
                                <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
                                <c:forEach var="viewPdfItem" items="${Word002Form.viewPdfList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${viewPdfItem.miscCode}">
                                        ${viewPdfItem.miscValue}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                            &nbsp;
                            <input type="checkbox" value="checkbox" id="viewPdfChk"/>
                            &nbsp;
                            <sps:label name="VIEW_PDF"/>
                            <html:hidden property="viewPdf" value="${Word002Form.viewPdf}" styleId="viewPdfCriteria"/>
                        </td>
                        <th  align="right">
                            <sps:label name="SPS_PO_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Word002Form" property="spsPoNo" style="width:129px" />
                        </td>
                        <th align="right">
                            <sps:label name="CIGMA_PO_NO" />
                            &nbsp;
                        </th>
                        <td>
                            <html:text name="Word002Form" property="cigmaPoNo" style="width:115px" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <table style="width:994px">
                <tr>
                    <td class="align-left" style="vertical-align:bottom;font-size:10px;">
                        <c:if test="${not empty Word002Form.poList}">
                            <sps:label name="SYSTEM_GET_SELECTED_REC_ONLY_CURRENT_PAGE"/>
                        </c:if>
                    </td>
                    <td class="align-right">
                        <input type="button" id="btnSearch" value="${btnSearch}" class="select_button"/>
                        &nbsp;
                        <input type="button" id="btnReset" value="${btnReset}" class="select_button"/>
                        &nbsp;
                        <input type="button" id="btnDownload"  value="${btnDownload}" class="select_button"/>
                    </td>
                </tr>
            </table>
            <ai:pagingoption maxRow="5"/>
        </html:form>

        <html:form styleId="Word002FormResult" action="/PurchaseOrderInformationAction.do" >
            <html:hidden styleId="methodResult" property="method" value="Search" />
            <html:hidden styleId="fileIdSelected" property="fileIdSelected"/>
            
            <html:hidden property="vendorCd"/>
            <html:hidden property="SPcd"/>
            <html:hidden property="DCd"/>
            <html:hidden property="DPcd"/>
            <html:hidden property="userLogin" />
            <html:hidden property="issuedDateFrom" styleId="issuedDateFromResult" />
            <html:hidden property="issuedDateTo" styleId="issuedDateToResult" />
            <html:hidden styleId="poStatus" property="poStatus"/>
            <html:hidden styleId="poType" property="poType"/>
            <html:hidden styleId="periodType" property="periodType"/>
            <html:hidden styleId="periodTypeName" property="periodTypeName"/>
            <html:hidden styleId="dataType" property="dataType"/>
            <html:hidden styleId="viewPdf" property="viewPdf"/>
            <html:hidden styleId="spsPoNo" property="spsPoNo"/>
            <html:hidden styleId="cigmaPoNo" property="cigmaPoNo"/>
            
            <html:hidden styleId="clickedPoId" property="poId"/>
            <html:hidden styleId="clickedPoType" property="clickedPoType"/>
            <html:hidden styleId="clickedPeriodType" property="clickedPeriodType"/>
            <html:hidden styleId="clickedSpsPoNo" property="clickedSpsPoNo"/>
            
            <c:forEach var="supplierAuthen" items="${Word002Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
            </c:forEach>
            <c:forEach var="densoAuthen" items="${Word002Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
            </c:forEach>
            
            <html:hidden property="cannotResetMessage"/>
            <html:hidden property="cannotDownloadCsvMessage"/>
    
            <c:forEach var="poStatusItem" items="${Word002Form.poStatusList}" begin="0" step="1" varStatus="status">
                <html:hidden name="poStatusItem" property="miscCode" indexed="true"/></input>
                <html:hidden name="poStatusItem" property="miscValue" indexed="true"/></input>
            </c:forEach>
            <c:forEach var="poTypeItem" items="${Word002Form.poTypeList}" begin="0" step="1" varStatus="status">
                <html:hidden name="poTypeItem" property="miscCode" indexed="true"/>
                <html:hidden name="poTypeItem" property="miscValue" indexed="true"/>
            </c:forEach>
            <c:forEach var="periodTypeItem" items="${Word002Form.periodTypeList}" begin="0" step="1" varStatus="status">
                <html:hidden name="periodTypeItem" property="miscCode" indexed="true"/>
                <html:hidden name="periodTypeItem" property="miscValue" indexed="true"/>
            </c:forEach>
            <c:forEach var="viewPdfItem" items="${Word002Form.viewPdfList}" begin="0" step="1" varStatus="status">
                <html:hidden name="viewPdfItem" property="miscCode" indexed="true"/>
                <html:hidden name="viewPdfItem" property="miscValue" indexed="true"/>
            </c:forEach>
            <c:forEach var="companySupplierItem" items="${Word002Form.companySupplierList}" begin="0" step="1" varStatus="status">
                <html:hidden name="companySupplierItem" property="vendorCd" indexed="true"/>
            </c:forEach>
            <c:forEach var="companyDensoItem" items="${Word002Form.companyDensoList}" begin="0" step="1" varStatus="status">
                <html:hidden name="companyDensoItem" property="DCd" indexed="true"/>
                <html:hidden name="companyDensoItem" property="companyName" indexed="true"/>
            </c:forEach>
    
            <c:choose>
                <c:when test="${0 == Word002Form.max}">
                    <c:set var="resultStyle">
                        visibility: hidden;
                    </c:set>
                    <c:set var="legendStyle">
                        display:none;margin:0;padding-top:2px;
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="resultStyle">
                        visibility: visible;
                    </c:set>
                    <c:set var="legendStyle">
                        margin:0;padding-top:2px;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="result" style="${resultStyle}">
                <div style="width:994px;">
                    <table class="display_data" id="tblBody" style="table-layout:fixed;width:974px;word-wrap:break-word;">
                        <tr>
                            <th style="width:20px" scope="col" class="checkbox">
                                <sps:label name="ALL" /><br />
                                <input type="checkbox" name="checkbox" value="checkbox" id="checkAll" />
                            </th>
                            <th style="width:10%">
                                <sps:label name="ISSUE_DATE" />
                            </th>
                            <th style="width:14%">
                                <sps:label name="SPS_PO_NO" />
                            </th>
                            <th style="width:10%" scope="col" >
                                <sps:label name="CIGMA_PO_NO" />
                            </th>
                            <th style="width:7%" >
                                <sps:label name="S_CD" />
                            </th>
                            <th style="width:7%" >
                                <sps:label name="S_P" />
                            </th>
                            <th style="width:7%" >
                                <sps:label name="D_CD" />
                            </th>
                            <th style="width:7%" >
                                <sps:label name="D_P" />
                            </th>
                            <th style="width:7%" >
                                <sps:label name="PO_TYPE" />
                            </th>
                            <th style="width:8%" >
                                <sps:label name="PERIOD_TYPE" />
                            </th>
                            <th style="width:10%" >
                                <sps:label name="PO_STATUS" />
                            </th>
                            <th style="width:7%" >
                                <sps:label name="PDF_ORG" />
                            </th>
                            <th style="width:7%" >
                                <sps:label name="PDF_CHG" />
                            </th>
                        </tr>
                    </table>
                </div>
                <div style="width:994px;height:300px;overflow-y:scroll">
                    <table class="display_data" id="tblBody"
                        style="table-layout:fixed;width:974px;word-wrap:break-word;border-width:0px">
                        <c:set var="startRow">
                            ${(Word002Form.pageNo * Word002Form.count) - Word002Form.count}
                        </c:set>
                        <c:forEach var="po" items="${Word002Form.poList}" begin="0" step="1" varStatus="status">
                            <c:choose>
                                <c:when test="${(status.index+1)% 2 == 0}">
                                    <c:set var="rowColor">
                                        blueLight
                                    </c:set>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="rowColor">
                                        
                                    </c:set>
                                </c:otherwise>
                            </c:choose>
                            <c:choose>
                                <c:when test="${empty po.poIdSelected}">
                                    <c:set var="isPoChecked">
                                        
                                    </c:set>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="isPoChecked">
                                        checked
                                    </c:set>
                                </c:otherwise>
                            </c:choose>
                            <tr class="${rowColor}">
                                <td align="center" style="width:20px">
                                    <input type="checkbox" name="chkSelectPo" id="chkPurchaseOrder${status.index}" onclick="word002SetAckPOList('${status.index}');" ${isPoChecked} />
                                    <!-- Parameter -->
                                    <html:hidden name="po" property="spsTPoDomain.poId"               value="${po.spsTPoDomain.poId}"               indexed="true" styleId="poId${status.index}"/>
                                    <html:hidden name="po" property="spsTPoDomain.poStatus"           value="${po.spsTPoDomain.poStatus}"           indexed="true" styleId="poStatus${status.index}"/>
                                    <html:hidden name="po" property="spsTPoDomain.lastUpdateDatetime" value="${po.spsTPoDomain.lastUpdateDatetime}" indexed="true" styleId="lastUpdateDatetime${status.index}"/>
                                    
                                    <html:hidden name="po" property="poIdSelected"       indexed="true" styleId="poIdSelected${status.index}"/>
                                    <html:hidden name="po" property="poStatusSelected"   indexed="true" styleId="poStatusSelected${status.index}"/>
                                </td>
                                <td style="width:10%">
                                    <%-- FIX : wrong dateformat --%>
                                    <%-- <fmt:formatDate pattern="yyyy/MM/dd" value="${po.spsTPoDomain.poIssueDate}"/> --%>
                                    ${po.poIssuedDateShow}
                                    <html:hidden name="po" property="poIssuedDateShow" indexed="true" />
                                    
                                    <html:hidden name="po" property="spsTPoDomain.poIssueDate" indexed="true" />
                                </td>
                                <td style="width:14%">
                                    <a href="javascript:void(0)" onclick="word002OpenWORD003('${po.spsTPoDomain.poId}','${po.spsTPoDomain.spsPoNo}'
                                    ,'${po.spsTPoDomain.periodType}','${po.periodTypeName}','${po.spsTPoDomain.poType}');return false;" >
                                        ${po.spsTPoDomain.spsPoNo}
                                    </a>
                                    <html:hidden name="po" property="spsTPoDomain.spsPoNo" indexed="true" />
                                    <html:hidden name="po" property="spsTPoDomain.periodType" indexed="true" />
                                    <html:hidden name="po" property="periodTypeName" indexed="true" />
                                    <html:hidden name="po" property="spsTPoDomain.poType" indexed="true" />
                                </td>
                                <td style="width:10%">
                                    <html:hidden name="po" property="spsTPoDomain.cigmaPoNo" indexed="true" write="true" />
                                </td>
                                <td style="width:7%">
                                    <html:hidden name="po" property="spsTPoDomain.vendorCd" indexed="true" write="true" />
                                </td>
                                <td style="width:7%">
                                    <html:hidden name="po" property="spsTPoDomain.SPcd" indexed="true" write="true" />
                                </td>
                                <td style="width:7%">
                                    <html:hidden name="po" property="spsTPoDomain.DCd" indexed="true" write="true" />
                                </td>
                                <td style="width:7%">
                                    <html:hidden name="po" property="spsTPoDomain.DPcd" indexed="true" write="true" />
                                </td>
                                <td style="width:7%">
                                    ${po.spsTPoDomain.poType}
                                </td>
                                <td style="width:8%">
                                    ${po.periodTypeName}
                                </td>
                                <td style="width:10%">
                                    ${po.spsTPoDomain.poStatus}
                                </td>
                                <td style="width:7%" class="align-center">
                                    <c:if test="${('0' == po.spsTPoDomain.changeFlg && po.spsTPoDomain.cigmaPoNo != '9999999999') || ('0' == po.spsTPoDomain.changeFlg && po.spsTPoDomain.poType == 'DKO' && po.spsTPoDomain.cigmaPoNo == '9999999999')}">
                                        <a href="javascript:void(0)"  onclick="word002DownloadPdfOriginal('${po.spsTPoDomain.poId}');return false;" style="font-size:10px;">
                                            <img src="css/images/pdf.jpg" style="width:19px; height:20px" />
                                        </a>
                                    </c:if>
                                    <c:if test="${'1' == po.spsTPoDomain.changeFlg || ('0' == po.spsTPoDomain.changeFlg && po.spsTPoDomain.poType == 'DPO' && po.spsTPoDomain.cigmaPoNo == '9999999999')}">
                                        -
                                    </c:if>
                                    <html:hidden name="po" property="pdfOriginal" indexed="true" />
                                    <html:hidden name="po" property="spsTPoDomain.pdfOriginalFileId" indexed="true" />
                                </td>
                                <td style="width:7%" class="align-center">
                                    <c:if test="${'1' == po.spsTPoDomain.changeFlg}">
                                        <a href="javascript:void(0)"  onclick="word002DownloadPdfChange('${po.spsTPoDomain.poId}');return false;" style="font-size:10px;">
                                            <img src="css/images/pdf.jpg" style="width:19px; height:20px" />
                                        </a>
                                    </c:if>
                                    <c:if test="${'0' == po.spsTPoDomain.changeFlg}">
                                        -
                                    </c:if>
                                    <html:hidden name="po" property="pdfChange" indexed="true" />
                                    <html:hidden name="po" property="spsTPoDomain.pdfChangeFileId" indexed="true" />
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <table style="width:994px;border-width:0px">
                    <tr>
                        <td align="left">
                            <ai:paging action="/SPS/PurchaseOrderInformationAction.do" formId="Word002FormResult" jump="true" />
                        </td>
                        <td class="align-right">
                            <input type="button" id="btnAck" value="${btnAck}" class="select_button"/>
                        </td>
                    </tr>
                </table>
                <div id="displayLegend" style="${legendStyle}">
                    <table bgcolor="#EEEEEE" style="width:994px;border:0;">
                        <tr>
                            <td style="font-size:10px;">
                                <%--LEGEND FIXED --%>
                                <span style="font-weight: bold;">
                                    <sps:label name="LEGEND" />
                                    <span style="text-decoration: underline;">
                                        <sps:label name="PO_STATUS" />
                                    </span>
                                </span>
                                <sps:label name="STATUS_ISS" />,&nbsp;
                                <sps:label name="STATUS_PED" />,&nbsp;
                                <sps:label name="STATUS_ACK" />,&nbsp;
                                <sps:label name="STATUS_FAC" />,&nbsp;
                                <sps:label name="STATUS_RPL" />;
                                <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span style="font-weight: bold;">
                                    <span style="text-decoration: underline;">
                                        <sps:label name="PO_TYPE" />
                                    </span>
                                </span>
                                <sps:label name="TYPE_DPO" />,&nbsp;
                                <sps:label name="TYPE_IPO" />,&nbsp;
                                <sps:label name="TYPE_DKO" />,&nbsp;
                                <sps:label name="TYPE_IKO" />;
                                <%--LEGEND FIXED --%>
                            </td>
                            <td class="align-right">
                                <a href="javascript:void(0)"  onclick="word002DownloadLegendFile();return false;" style="font-size:10px;">
                                    &gt;&gt; <sps:label name="MORE" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </html:form>
    </body>
</html>
