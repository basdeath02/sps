<%@ include file="../includes/jsp_header.jspf"%>
<html>
<head>
<%@ include file="../includes/html_header.jspf"%>
<jsp:useBean id="cons"
    class="com.globaldenso.asia.sps.common.constant.Constants" />
<script type="text/javascript" src="./js/business/Word006_FixOrder.js">
</script>
<title>
    <sps:label name="DELIVERY_ORDER_INFO" />
</title>
<script type="text/javascript">
    var labelAll = '<sps:label name="ALL_CRITERIA" />';
    var msgPrinted = '<sps:message name="SP-W2-0006"/>';
</script>
</head>
<body class="hideHorizontal">
    <html:form styleId="Word006FormCriteria"
        action="/DeliveryOrderInformationAction.do">
        <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
        <html:hidden styleId="method" property="method" />
        <html:hidden property="doId" styleId="doId" />
        <html:hidden property="pdfFileId" styleId="pdfFileId" />
        <html:hidden property="pdfType" styleId="pdfType" />
        <html:hidden property="pdfFileKbTagPrintDate" styleId="pdfFileKbTagPrintDate" />
        <html:hidden property="mode" styleId="mode" />
        <!-- Key for paging. -->
        <html:hidden styleId="currentPage" property="currentPage" />
        <html:hidden styleId="row" property="row" />
        <html:hidden styleId="totalCount" property="totalCount" />
        
        <c:forEach var="revisionItem" items="${Word006Form.revisionList}" begin="0" step="1" varStatus="status">
            <html:hidden name="revisionItem" property="miscCode" indexed="true"/></input>
            <html:hidden name="revisionItem" property="miscValue" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="shipmentStatusItem" items="${Word006Form.shipmentStatusList}" begin="0" step="1" varStatus="status">
            <html:hidden name="shipmentStatusItem" property="miscCode" indexed="true"/></input>
            <html:hidden name="shipmentStatusItem" property="miscValue" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="transportModeItem" items="${Word006Form.transportModeList}" begin="0" step="1" varStatus="status">
            <html:hidden name="transportModeItem" property="miscCode" indexed="true"/></input>
            <html:hidden name="transportModeItem" property="miscValue" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="companySupplierItem" items="${Word006Form.companySupplierList}" begin="0" step="1" varStatus="status">
            <html:hidden name="companySupplierItem" property="vendorCd" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="companyDensoItem" items="${Word006Form.companyDensoList}" begin="0" step="1" varStatus="status">
            <html:hidden name="companyDensoItem" property="DCd" indexed="true"/></input>
            <html:hidden name="companyDensoItem" property="companyName" indexed="true"/></input>
        </c:forEach>
        
        <c:forEach var="supplierAuthen" items="${Word006Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
            <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
            <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="densoAuthen" items="${Word006Form.densoAuthenList}" begin="0" step="1" varStatus="status">
            <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
            <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
        </c:forEach>
        
        <html:hidden styleId="cannotResetMessage" property="cannotResetMessage" />
        <html:hidden styleId="cannotDownloadCsvMessage" property="cannotDownloadCsvMessage"/>
        
        <table
            style="height: 30px; width: 990px; border-width: 0px; padding: 0px"
            class="bottom">
            <tr>
                <td>
                    <sps:label name="ORDERING" />
                    &gt;
                    <sps:label name="DELIVERY_ORDER_INFORMATION" />
                </td>
                <td colspan="2" class="align-right">
                    <sps:label name="USER" />
                    &nbsp; : <bean:write name="Word006Form" property="userLogin" />
                </td>
            </tr>
        </table>
        <c:choose>
            <c:when test="${not empty Word006Form.applicationMessageList}">
                <c:set var="displayErrorMessage">
                    
                </c:set>
            </c:when>
            <c:otherwise>
                <c:set var="displayErrorMessage">
                    display: none;
                </c:set>
            </c:otherwise>
        </c:choose>
        <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}" >
            <table style="width: 992px" class="tableMessage">
                <tr>
                    <td id="errorTotal">
                        <c:forEach var="appMsg"
                            items="${Word006Form.applicationMessageList}"
                            begin="0" step="1" varStatus="status">
                            <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                <c:set var="messageClass">
                                    ui-messageSuccess
                                </c:set>
                            </c:if>
                            <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                <c:set var="messageClass">
                                    ui-messageError
                                </c:set>
                            </c:if>
                            <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                <c:set var="messageClass">
                                    ui-messageWarning
                                </c:set>
                            </c:if>
                            <span class="${messageClass}" >
                                ${appMsg.message} 
                            </span>
                            <br>
                        </c:forEach>
                    </td>
                </tr>
            </table>
        </div>
        <fieldset class="fixwidth">
            <c:set var="lbAll">
                <sps:label name="ALL_CRITERIA" />
            </c:set>
            <c:set var="lbSearch">
                <sps:label name="SEARCH" />
            </c:set>
            <c:set var="lbReset">
                <sps:label name="RESET" />
            </c:set>
            <c:set var="lbDownload">
                <sps:label name="DOWNLOAD" />
            </c:set>
            <table class="display_criteria" id="ui-displayCriteria">
                <tr>
                    <th style="width: 100px; padding-top: 1px;" class="align-right">
                        <sps:label name="S_CD" />
                        :
                    </th>
                    <td style="width: 140px;">
                        <html:select style="width: 135px" name="Word006Form" styleId="sCd" property="vendorCd" styleClass="mandatory">
                            <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
                            <c:forEach var="companySupplierItem" items="${Word006Form.companySupplierList}" begin="0" step="1" varStatus="status">
                                <html:option value="${companySupplierItem.vendorCd}">
                                    ${companySupplierItem.vendorCd}
                                </html:option>
                            </c:forEach>
                        </html:select>
                    </td>
                    <th style="width: 100px; padding-top: 1px;" class="align-right">
                        <sps:label name="S_P" />
                        :
                    </th>
                    <td style="width: 140px;">
                        <html:select styleId="sPcd" style="width: 135px" name="Word006Form" styleClass="mandatory" property="SPcd">
                            <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
                            <c:forEach var="plantSupplierItem" items="${Word006Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                                <html:option value="${plantSupplierItem.SPcd}">
                                    ${plantSupplierItem.SPcd}
                                </html:option>
                            </c:forEach>
                        </html:select>
                    </td>
                    <th style="width: 100px; padding-top: 1px;" class="align-right">
                        <sps:label name="D_CD" />
                        :
                    </th>
                    <td style="width: 140px;">
                        <html:select styleId="dCd" style="width: 135px" name="Word006Form" styleClass="mandatory" property="DCd">
                            <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
                            <c:forEach var="companyDensoItem" items="${Word006Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                <html:option value="${companyDensoItem.DCd}">
                                    ${companyDensoItem.DCd}
                                </html:option>
                            </c:forEach>
                        </html:select>
                    </td>
                    <th style="width: 100px; padding-top: 1px;" class="align-right">
                        <sps:label name="D_P" />
                        :
                    </th>
                    <td style="width: 140px;">
                        <html:select styleId="dPcd" style="width: 135px" name="Word006Form" styleClass="mandatory" property="DPcd">
                            <html:option value="${cons.MISC_CODE_ALL}">${lbAll}</html:option>
                            <c:forEach var="plantDensoItem" items="${Word006Form.plantDensoList}" begin="0" step="1" varStatus="status">
                                <html:option value="${plantDensoItem.DPcd}">
                                    ${plantDensoItem.DPcd}
                                </html:option>
                            </c:forEach>
                        </html:select>
                    </td>
                </tr>
            </table>
            <table class="display_criteria" id="ui-displayCriteria" style="padding-left:1px;border-spacing:0;">
                <tr>
                    <th style="width: 100px;" class="align-right">
                        <sps:label name="ISSUE_DATE" />
                        :
                    </th>
                    <td style="width:241px">
                        <sps:calendar name="issueDateFrom" id="issueDateFrom" value="${Word006Form.issueDateFrom}" />
                        -
                        <sps:calendar name="issueDateTo" id="issueDateTo" value="${Word006Form.issueDateTo}" />
                    </td>
                    <th style="width: 87px;" class="align-right">
                        <sps:label name="DELIVERY_DATE" />
                        :
                    </th>
                    <td style="width:235px">
                        <sps:calendar name="deliveryDateFrom" id="deliveryDateFrom" value="${Word006Form.deliveryDateFrom}" />
                        -
                        <sps:calendar name="deliveryDateTo" id="deliveryDateTo" value="${Word006Form.deliveryDateTo}" />
                    </td>
                    <th style="width: 87px;" class="align-right">
                        <sps:label name="DELIVERY_TIME" />
                        :
                    </th>
                    <td style="width:235px">
                        <html:text name="Word006Form" property="deliveryTimeFrom" style="width:50px"
                            onfocus="removeColonFromTime('deliveryTimeFrom');return false;"
                            onblur="addColonToTime('deliveryTimeFrom');return false;" styleId="deliveryTimeFrom"/>
                            -
                        <html:text name="Word006Form" property="deliveryTimeTo" style="width:50px"
                            onfocus="removeColonFromTime('deliveryTimeTo');return false;"
                            onblur="addColonToTime('deliveryTimeTo');return false;" styleId="deliveryTimeTo"/>
                    </td>
                </tr>
            </table>
            <table class="display_criteria" id="ui-displayCriteria">
                <tr>
                    <th style="width: 100px;" class="align-right">
                        <sps:label name="SHIP_DATE" />
                        :
                    </th>
                    <td style="width:241px">
                        <sps:calendar name="shipDateFrom" id="shipDateFrom" value="${Word006Form.shipDateFrom}" />
                        -
                        <sps:calendar name="shipDateTo" id="shipDateTo" value="${Word006Form.shipDateTo}" />
                    </td>
                    <th style="width: 50px;">
                        <sps:label name="REV" />
                        :
                    </th>
                    <td style="width: 90px;">
                        <html:select styleId="revision" property="revision" style="width: 85px">
                            <html:option value="">${lbAll}</html:option>
                            <c:forEach var="revisionItem" items="${Word006Form.revisionList}" begin="0" step="1" varStatus="status">
                                <html:option value="${revisionItem.miscCode}">
                                    ${revisionItem.miscValue}
                                </html:option>
                            </c:forEach>
                        </html:select>
                    </td>
                    <th style="width: 115px;">
                        <sps:label name="SHIPMENT_STATUS" />
                        :
                    </th>
                    <td style="width: 130px;">
                        <html:select styleId="shipmentStatus" property="shipmentStatus" style="width: 135px">
                            <html:option value="">${lbAll}</html:option>
                            <c:forEach var="shipmentStatusItem" items="${Word006Form.shipmentStatusList}" begin="0" step="1" varStatus="status">
                                <html:option value="${shipmentStatusItem.miscCode}">
                                    ${shipmentStatusItem.miscValue}
                                </html:option>
                            </c:forEach>
                        </html:select>
                    </td>
                    <th style="width: 85px;">
                        <sps:label name="TM" />
                        :
                    </th>
                    <td style="width: 140px;">
                        <html:select styleId="transportMode" property="transportMode" style="width: 135px">
                            <html:option value="">${lbAll}</html:option>
                            <c:forEach var="transportModeItem" items="${Word006Form.transportModeList}" begin="0" step="1" varStatus="status">
                                <html:option value="${transportModeItem.miscCode}">
                                    ${transportModeItem.miscValue}
                                </html:option>
                            </c:forEach>
                        </html:select>
                    </td>
                </tr>
                <tr>
                    <th>
                        <sps:label name="ROUTE" />
                        :
                    </th>
                    <td>
                        <html:text name="Word006Form" property="routeNo" style="width: 134px" />
                    </td>
                    <th>
                        <sps:label name="DEL" />
                        :
                    </th>
                    <td>
                        <html:text name="Word006Form" property="delNo" style="width: 84px" />
                    </td>
                    <th>
                        <sps:label name="D/O_NO" />
                        :
                    </th>
                    <td>
                        <html:text name="Word006Form" property="spsDoNo" style="width: 134px" />
                    </td>
                    <th style="width:100px">
                        <sps:label name="CIGMA_DO#" />
                        :
                    </th>
                    <td style="width:145px">
                        <html:text name="Word006Form" property="cigmaDoNo" style="width: 134px" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <table style="width: 994px;">
            <tr>
                <td colspan="6" class="align-right">
                    <input name="Input"
                        type="button" value="${lbSearch}" class="select_button" id="btnSearch" />
                    <input name="Input" type="button" id="btnReset" value="${lbReset}"
                        class="select_button" />
                    <input name="btnDownload"
                        id="btnDownload" type="button" value="${lbDownload }"
                        class="select_button" />
                </td>
            </tr>
        </table>
        <ai:pagingoption maxRow="5" />
    </html:form>
    <html:form styleId="DeliveryOrderInformationActionResultForm"
        action="/DeliveryOrderInformationAction.do">
        <html:hidden property="method" styleId="methodResult" />
        <html:hidden property="doId" styleId="doIdResult" />
        <html:hidden property="vendorCd" styleId="sCdResult" />
        <html:hidden property="SPcd"
            styleId="sPcdResult" />
        <html:hidden property="DCd" styleId="dCdResult" />
        <html:hidden property="DPcd" styleId="dPcdResult" />
        <html:hidden property="issueDateFrom" styleId="issueDateFromResult" />
        <html:hidden property="issueDateTo" styleId="issueDateToResult" />
        <html:hidden property="deliveryDateFrom"
            styleId="deliveryDateFromResult" />
        <html:hidden property="deliveryDateTo" styleId="deliveryDateToResult" />
        <html:hidden property="shipDateFrom" styleId="shipDateFromResult" />
        <html:hidden property="shipDateTo" styleId="shipDateToResult" />
        <html:hidden property="revision" styleId="revisionResult" />
        <html:hidden property="shipmentStatus" styleId="shipmentStatusResult" />
        <html:hidden property="transportMode" styleId="transportModeResult" />
        <html:hidden property="routeNo" styleId="routeNoResult" />
        <html:hidden property="delNo" styleId="delNoResult" />
        <html:hidden property="spsDoNo" styleId="spsDoNoResult" />
        <html:hidden property="cigmaDoNo" styleId="cigmaDoNoResult" />
        <html:hidden property="pdfFileId" styleId="pdfFileIdResult" />
        <html:hidden property="pdfType" styleId="pdfTypeResult" />
        <html:hidden property="pdfFileKbTagPrintDate" styleId="pdfFileKbTagPrintDateResult" />
        
        <html:hidden property="cannotResetMessage" />
        <html:hidden property="cannotDownloadCsvMessage"/>
        
        <c:forEach var="revisionItem" items="${Word006Form.revisionList}" begin="0" step="1" varStatus="status">
            <html:hidden name="revisionItem" property="miscCode" indexed="true"/></input>
            <html:hidden name="revisionItem" property="miscValue" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="shipmentStatusItem" items="${Word006Form.shipmentStatusList}" begin="0" step="1" varStatus="status">
            <html:hidden name="shipmentStatusItem" property="miscCode" indexed="true"/></input>
            <html:hidden name="shipmentStatusItem" property="miscValue" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="transportModeItem" items="${Word006Form.transportModeList}" begin="0" step="1" varStatus="status">
            <html:hidden name="transportModeItem" property="miscCode" indexed="true"/></input>
            <html:hidden name="transportModeItem" property="miscValue" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="companySupplierItem" items="${Word006Form.companySupplierList}" begin="0" step="1" varStatus="status">
            <html:hidden name="companySupplierItem" property="vendorCd" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="companyDensoItem" items="${Word006Form.companyDensoList}" begin="0" step="1" varStatus="status">
            <html:hidden name="companyDensoItem" property="DCd" indexed="true"/></input>
            <html:hidden name="companyDensoItem" property="companyName" indexed="true"/></input>
        </c:forEach>
        
        <c:forEach var="supplierAuthen" items="${Word006Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
            <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
            <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
        </c:forEach>
        <c:forEach var="densoAuthen" items="${Word006Form.densoAuthenList}" begin="0" step="1" varStatus="status">
            <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
            <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
        </c:forEach>
        
        <c:choose>
                <c:when test="${null == Word006Form.doInfoList}">
                    <c:set var="visibilityFlag">
                        hidden
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="visibilityFlag">
                        visible
                    </c:set>
                </c:otherwise>
        </c:choose>
        <div id="result" style="visibility: ${visibilityFlag};">
            <div style="width: 1075px;">
                <table class="display_data" id="tblBody"
                    style="table-layout: fixed; word-wrap: break-word;">
                    <tr>
                        <th style="width: 20px;">
                            <sps:label name="NO" />
                        </th>
                        <th style="width: 70px;" scope="col">
                            <sps:label name="ISSUE_DATE" />
                        </th>
                        <th style="width: 95px;" scope="col">
                            <sps:label name="DELIVERY_DATE" />
                        </th>
                        <th style="width: 95px;" scope="col">
                            <sps:label name="SHIP_DATE" />
                        </th>
                        <th style="width: 90px;" scope="col">
                            <sps:label name="D/O_NO" />
                        </th>
                        <th style="width: 25px;" scope="col">
                            <sps:label name="REV" />
                        </th>
                        <th style="width: 90px;" scope="col">
                            <sps:label name="CIGMA_DO#" />
                        </th>
                        <th style="width: 40px;" scope="col">
                            <sps:label name="ROUTE" />
                        </th>
                        <th style="width: 40px;" scope="col">
                            <sps:label name="DEL" />
                        </th>
                        <th style="width: 45px;" scope="col">
                            <sps:label name="S_CD" />
                        </th>
                        <th style="width: 45px;" scope="col">
                            <sps:label name="S_P" />
                        </th>
                        <th style="width: 45px;" scope="col">
                            <sps:label name="D_CD" />
                        </th>
                        <th style="width: 45px;" scope="col">
                            <sps:label name="D_P" />
                        </th>
                        <th style="width: 40px;" scope="col">
                            <sps:label name="TM" />
                        </th>
                        <th style="width: 70px;" scope="col">
                            <sps:label name="SHIPMENT_STATUS" />
                        </th>
                        <th style="width: 30px;" scope="col">
                            <sps:label name="DO" />
                        </th>
                        <th style="width: 30px;" scope="col">
                            <sps:label name="KB" />
                        </th>
                    </tr>
                </table>
            </div>
            <c:set var="startRow">
                ${(Word006Form.pageNo * Word006Form.count) - Word006Form.count}
            </c:set>
            <div style="width: 1075px; height: 278px; overflow-y: scroll;">
                <table class="display_data" id="tblBody"
                    style="table-layout: fixed; word-wrap: break-word;"
                    border="0">
                    <c:forEach var="doInfoList"
                        items="${Word006Form.doInfoList}" begin="0"
                        step="1" varStatus="status">
                        <c:set var="numbercount">
                            ${numbercount + 1}
                        </c:set>
                        <c:choose>
                            <c:when test="${numbercount % 2 == 0}">
                                <c:set var="color">
                                    blueLight
                                </c:set>
                            </c:when>
                            <c:otherwise>
                                <c:set var="color">
                                </c:set>
                            </c:otherwise>
                        </c:choose>
                        <tr class="${color}">
                        <td style="width: 20px;" class="align-right">
                            ${startRow + (status.index + 1)}
                        </td>
                        <td style="width: 70px;" class="align-left">
                            <%-- FIX : wrong dateformat --%>
                            <%-- <fmt:formatDate pattern="yyyy/MM/dd" value="${doInfoList.deliveryOrderSubDetailDomain.doIssueDate}"/> --%>
                            ${doInfoList.deliveryOrderSubDetailDomain.doIssueDateShow}
                        </td>
                        <td style="width: 95px;" class="align-left">
                            ${doInfoList.deliveryOrderSubDetailDomain.deliveryDatetimeUtc}
                        </td>
                        <td style="width: 95px;" class="align-left">
                            ${doInfoList.deliveryOrderSubDetailDomain.shipDatetimeUtc}
                        </td>
                        <td style="width: 90px;" class="align-left">
                            ${doInfoList.deliveryOrderSubDetailDomain.spsDoNo}
                        </td>
                        <td style="width: 25px;" class="align-left">
                            ${doInfoList.deliveryOrderSubDetailDomain.revision}
                        </td>
                        <td style="width: 90px;" class="align-left">
                            ${doInfoList.deliveryOrderSubDetailDomain.cigmaDoNo}
                        </td>
                        <td style="width: 40px;" class="align-left">
                            ${doInfoList.deliveryOrderSubDetailDomain.truckRoute}
                        </td>
                        <td style="width: 40px;" class="align-left">
                            ${doInfoList.deliveryOrderSubDetailDomain.del}
                        </td>
                        <td style="width: 45px;" class="align-left">
                            ${doInfoList.deliveryOrderSubDetailDomain.vendorCd}
                        </td>
                        <td style="width: 45px;" class="align-left">
                            ${doInfoList.deliveryOrderSubDetailDomain.SPcd}
                        </td>
                        <td style="width: 45px;" class="align-left">
                            ${doInfoList.deliveryOrderSubDetailDomain.DCd}
                        </td>
                        <td style="width: 45px;" class="align-left">
                            ${doInfoList.deliveryOrderSubDetailDomain.DPcd}
                        </td>
                        <td style="width: 40px;" class="align-left">
                            ${doInfoList.deliveryOrderSubDetailDomain.tm}
                        </td>
                        <td style="width: 70px;" class="align-left">
                            ${doInfoList.deliveryOrderSubDetailDomain.shipmentStatus}
                        </td>
                        <td style="width: 30px;" class="align-center">
                            <a href="#">
                                <img src="css/images/pdf.jpg" style="width: 19" height="20" alt="${lbDownload }"
                                    onclick="word006ViewPdf('${doInfoList.deliveryOrderSubDetailDomain.doId}'); return false;" />
                            </a>
                        </td>
                        <td style="width: 30px;" class="align-center">
                            <c:choose>
                                <c:when test="${doInfoList.deliveryOrderSubDetailDomain.oneWayKanbanTagFlag == '1'}">
                                    <c:choose>
                                        <c:when test="${doInfoList.deliveryOrderSubDetailDomain.partTag > '0' && doInfoList.deliveryOrderSubDetailDomain.tagOutput != '4'}">
                                            <c:choose>
                                                <c:when test="${doInfoList.deliveryOrderSubDetailDomain.shipmentStatus == 'CPS' && doInfoList.deliveryOrderSubDetailDomain.receiveStatus == 'RCP'}">
                                                    <img src="css/images/pdfGray.jpg" style="width: 19" height="20" alt="${lbDownload }" />
                                                </c:when>
                                                <c:otherwise>
                                                    <a href="#">
                                                        <img src="css/images/pdf.jpg" style="width: 19" height="20" alt="${lbDownload }" onclick="word006ViewKanbanPdf('${doInfoList.deliveryOrderSubDetailDomain.doId}','KB','${doInfoList.deliveryOrderSubDetailDomain.pdfFileKbTagPrintDate}'); return false;" />
                                                    </a>
                                                   <html:hidden property="pdfFileKbTagPrintDateFlag" value="${doInfoList.deliveryOrderSubDetailDomain.pdfFileKbTagPrintDate == null ? 'N' : 'Y'}" styleId="pdfFileKbTagPrintDateFlag${doInfoList.deliveryOrderSubDetailDomain.doId}" />
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                            -
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${doInfoList.deliveryOrderSubDetailDomain.tagOutput == '4'}">
                                            -
                                        </c:when>
                                        <c:otherwise>
                                            &nbsp;
                                        </c:otherwise>
                                    </c:choose>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
            <table style="width: 994px;">
                <tr>
                    <td>
                        <ai:paging action="/SPS/DeliveryOrderInformationAction.do"
                            formId="DeliveryOrderInformationActionResultForm" jump="true" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="displayLegend" style="visibility: ${visibilityFlag}; margin: 0; padding: 0;">
            <table style="width: 994px; border: 0; background-color: #EEEEEE;">
                <tr>
                    <td style="font-size: 10px;">
                        <b>
                            <sps:label name="LEGEND" />
                            <span
                                style="text-decoration: underline">
                                <sps:label name="SHIPMENT_STATUS" />
                            </span>
                        </b>
                        <%--LEGEND FIXED --%>
                        <sps:label name="ISS_PTS_CPS_CCL" />
                    </td>
                    <td class="align-right">
                        <a href="#" style="font-size: 10px;" id="moreLegend">
                            &gt;&gt; 
                            <sps:label name="MORE" />
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </html:form>
</body>
</html>