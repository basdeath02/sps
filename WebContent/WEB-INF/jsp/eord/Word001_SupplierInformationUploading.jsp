<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Word001_SupplierInformationUploading.js" >
        </script>
        <script type="text/javascript">
            var labelRegisterProcess = '<sps:label name="REGISTER_PROCESS" />';
            var msgConfirmRegister = '<sps:message name="SP-W6-0001"/>';
        </script>
        <title>
            <sps:label name="SUPPLIER_INFORMATION_UPLOADING" />
        </title>
    </head>
    <body class="hideHorizontal">
         <html:form styleId="Word001FormCriteria" enctype="multipart/form-data" action="/SupplierInformationUploadingAction.do" >
        <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden styleId="method" property="method" />
            <html:hidden styleId="totalRecord" property="totalRecord" />
            <html:hidden styleId="correctRecord" property="correctRecord" />
            <html:hidden styleId="warningRecord" property="warningRecord" />
            <html:hidden styleId="incorrectRecord" property="incorrectRecord" />
            <!-- Key for paging. -->
            <html:hidden styleId="currentPage" property="currentPage"/>
            <html:hidden styleId="row"  property="row"/>
            <html:hidden styleId="totalCount" property="totalCount"/>
            <html:hidden styleId="companySupplier" property="companySupplier"/>
            <html:hidden styleId="cannotResetMessage" property="cannotResetMessage"/>
            <html:hidden styleId="cannotExportMessage" property="cannotExportMessage"/>
            <html:hidden property="maxPlantSupplierList" styleId="maxPlantSupplierList" value="${Word001Form.maxPlantSupplierList}"/>
            <logic:notEmpty name="Word001Form" property="plantSupplierList">
                <c:forEach var="plantSupplierCode" items="${Word001Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                    <input type="hidden" name=plantSupplierCode indexed="true" id="plantSupplierCode${status.index}"
                        value="${plantSupplierCode.SPcd}"/>
                </c:forEach>
            </logic:notEmpty>
            <c:set var="btnRegister">
                <sps:label name="REGISTER" />
            </c:set>
            <c:set var="btnReset">
                <sps:label name="RESET" />
            </c:set>
            <c:set var="btnBrowse">
                <sps:label name="BROWSE" />
            </c:set>
            <c:set var="btnUpload">
                <sps:label name="UPLOAD" />
            </c:set>
            <c:set var="btnExport">
                <sps:label name="EXPORT" />
            </c:set>
            <table style="height:30px; width:990px;" class="bottom">
                <tr>
                    <td>
                        <sps:label name="ORDERING" />
                        &gt;
                        <sps:label name="SUPPLIER_INFORMATION_UPLOADING" />
                    </td>
                    <td colspan="2" class="align-right" >
                        <sps:label name="USER" />
                        &nbsp; : <bean:write name="Word001Form" property="userLogin" />
                        <html:hidden name="Word001Form" property="userLogin" />
                    </td>
                </tr>
            </table>
            <c:choose>
                <c:when test="${not empty Word001Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}" >
                <table style="width: 992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg"
                                items="${Word001Form.applicationMessageList}"
                                begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}" >
                                    ${appMsg.message} 
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            <fieldset class="fixwidth">
                <table class="display_criteria" style="margin-top:10px; cellpadding:3; cellspacing:1; width:975px;">
                    <tr>
                        <th class="align-right" style="width:80px">
                            <sps:label name="FILE_NAME" />
                            &nbsp;:
                        </th>
                        <td style="width:500px" class="align-center">
                            <html:text name="Word001Form" property="fileName" 
                                styleId="txtUpload" style="width:500px" readonly="true"/>
                        </td>
                        <td style="width:400px" >
                            <div class="align-left" style="position: absolute;filter: alpha(opacity=0);opacity:0;">
                                &nbsp;
                                <html:file property="fileData" styleId="fileData"
                                    maxlength="300" onchange="supplierInfoSetPathFile();"
                                    style="width: 90px; height: 25px; margin-left: -7px;"/>
                            </div>
                            <input name="input" type="button" value="${btnBrowse}" id="btnBrowse" class="select_button" />
                            <input name="input" type="button" value="${btnUpload}" id="btnUpload" class="select_button" />
                            <input name="input" type="button" value="${btnExport}" id="btnExport" class="select_button" />
                            <input name="input" type="button" value="${btnReset}"  id="btnReset" class="select_button" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        <c:choose>
            <c:when test="${0 == Word001Form.totalRecord}">
                <c:set var="visibilityFlag">
                    hidden
                </c:set>
            </c:when>
            <c:otherwise>
                <c:set var="visibilityFlag">
                    visible
                </c:set>
            </c:otherwise>
        </c:choose>
        <html:form styleId="Word001FormResult" action="/SupplierInformationUploadingAction.do" >
            <div id="result" style="visibility: ${visibilityFlag};">
                <html:hidden styleId="methodResult" property="method" value="doPaging"/>
                <html:hidden styleId="fileName" property="fileName"/>
                <html:hidden property="cannotResetMessage"/>
                <html:hidden property="cannotExportMessage"/>
                        <table class="display_criteria" style="width:950px; cellpadding:3; cellspacing:1;">
                            <tr>
                                <td class="align-left">
                                    <b>
                                        <u>
                                            <sps:label name="UPLOAD_RESULT" />
                                        </u>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <th style="width:20%" class="align-left">
                                    <sps:label name="SUPPLIER_COMPANY" />
                                    &nbsp;:
                                </th>
                                <td colspan="5">
                                    <html:text name="Word001Form" property="companySupplier" styleId="companySupplier" size="15" style="width:400px;" readonly="true" />
                                </td>
                            </tr>
                            <tr>
                                <th style="width:20%" class="align-left" style="vertical-align:top;">
                                    <sps:label name="SUPPLIER_PLANT" />
                                    &nbsp;:
                                </th>
                                <td colspan="3">
                                    <html:textarea name="Word001Form" property="plantSupplier" styleId="plantSupplier" style="width:150px" readonly="true" >
                                    </html:textarea>
                                </td>
                            </tr>
                            <tr>
                                <th style="width:12%" class="align-left">
                                    <sps:label name="TOTAL_RECORD" />
                                    &nbsp;:
                                </th>
                                <td style="width:5%" class="align-right">
                                    &nbsp;
                                    ${Word001Form.totalRecord}
                                    &nbsp;
                                </td>
                                <td style="width:8%" class="align-left">
                                    <sps:label name="RECORD" />
                                </td>
                                <th style="width:8%"class="align-right">
                                    <sps:label name="CORRECT" />
                                    &nbsp;:
                                </th>
                                <td style="width:5%"class="align-right">
                                    &nbsp;
                                    ${Word001Form.correctRecord}
                                    &nbsp;
                                </td>
                                <td style="width:10%"  class="align-left">
                                    <sps:label name="RECORD" />
                                </td>
                                <th style="width:8%" class="align-right">
                                    <sps:label name="WARNING" />
                                    &nbsp;:
                                </th>
                                <td style="width:5%"  class="align-right">
                                    ${Word001Form.warningRecord}
                                    &nbsp;
                                </td>
                                <td style="width:9%" class="align-left">
                                    <sps:label name="RECORD" />
                                </td>
                                <th style="width:9%"class="align-right">
                                    <sps:label name="INCORRECT" />
                                    &nbsp;:
                                </th>
                                <td style="width:5%"class="align-right">
                                    &nbsp;
                                    ${Word001Form.incorrectRecord}
                                    &nbsp;
                                </td>
                                <td style="width:22%"  class="align-left">
                                    <sps:label name="RECORD" />
                                </td>
                            </tr>
                        </table>
                    </div>
                <c:choose>
                    <c:when test="${0 == Word001Form.totalRecord}">
                        <c:set var="errorVisibilityFlag">
                            hidden
                        </c:set>
                    </c:when>
                    <c:otherwise>
                        <c:set var="errorVisibilityFlag">
                            visible
                        </c:set>
                    </c:otherwise>
                </c:choose>
                <div id="result" style="visibility: ${errorVisibilityFlag};">
                    <div style="width:994px;">
                        <table class="display_data" id="tblBody" style="table-layout:fixed;width:974px;word-wrap:break-word;">
                            <tr>
                                <th style="width:5%" >
                                    <sps:label name="NO" />
                                </th>
                                <th style="width:10%" >
                                    <sps:label name="TYPE" />
                                </th>
                                <th style="width:50%">
                                    <sps:label name="ERROR_MESSAGE" />
                                </th>
                                <th style="width:40%">
                                    <sps:label name="CSV_LINE_NO" />
                                </th>
                            </tr>
                        </table>
                    </div>
                    <div style="width:994px;height:248px;overflow-y:scroll">
                        <table class="display_data" id="tblbody" style="table-layout:fixed;width:974px;word-wrap:break-word;" border="0">
                        <c:set var="startRow">
                            ${(Word001Form.pageNo * Word001Form.count) - Word001Form.count}
                        </c:set>
                        <c:forEach var="uploaderror" items="${Word001Form.uploadErrorDetailDomain}" begin="0" step="1" varStatus="status">
                            <c:choose>
                                <c:when test="${(status.index+1)% 2 == 0}">
                                    <c:set var="rowColor">
                                        blueLight
                                    </c:set>
                                </c:when>
                                <c:otherwise>
                                    <c:set var="rowColor">
                                        
                                    </c:set>
                                </c:otherwise>
                            </c:choose>
                            <tr class="${rowColor}">
                                <td style="width:5%" class="align-right">
                                    ${status.index + startRow + 1}
                                </td>
                                <td style="width:10%" class="align-left">
                                    <c:if test="${cons.MESSAGE_TYPE_ERROR == uploaderror.messageType}">
                                        <span style="color:red;">
                                            <sps:label name="ERROR" />
                                        </span>
                                    </c:if>
                                    <c:if test="${cons.MESSAGE_TYPE_WARNING == uploaderror.messageType}">
                                        <sps:label name="WARNING" />
                                    </c:if>
                                </td>
                                <td style="width:50%" class="align-left">
                                    ${uploaderror.description}
                                </td>
                                <td style="width:40%" class="align-left">
                                    ${uploaderror.lineNo}
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
            <c:if test="${0 != Word001Form.totalRecord}">
                <c:if test="${0 != Word001Form.correctRecord || 0 != Word001Form.warningRecord}">
                    <c:choose>
                        <c:when test="${0 == Word001Form.incorrectRecord && 0 == Word001Form.warningRecord}">
                            <table style="width:994px">
                                <tr>
                                    <td align="right">
                                        <input type="button" value="${btnRegister}" id="btnRegister" class="select_button" />
                                    </td>
                                </tr>
                            </table>
                        </c:when>
                        <c:otherwise>
                            <table style="width:994px;">
                                <tr>
                                    <td align="left">
                                        <ai:paging action="/SPS/SupplierInformationUploadingAction.do" formId="Word001FormResult" jump="true" />
                                    </td>
                                    <td align="right">
                                        <input type="button" value="${btnRegister}" id="btnRegister" class="select_button"/>
                                    </td>
                                </tr>
                            </table>
                        </c:otherwise>
                    </c:choose>
                </c:if>
                <c:if test="${0 == Word001Form.correctRecord && 0 == Word001Form.warningRecord && 0 == Word001Form.incorrectRecord}">
                    <table style="width:994px;">
                        <tr>
                            <td align="left">
                                <ai:paging action="/SPS/SupplierInformationUploadingAction.do" formId="Word001FormResult" jump="true" />
                             </td>
                             <td align="right">
                                <input type="button" value="${btnRegister}" id="btnRegister" class="select_button" disabled="true"/>
                             </td>
                        </tr>
                    </table>
                </c:if>
                <c:if test="${0 == Word001Form.correctRecord && 0 == Word001Form.warningRecord && 0 != Word001Form.incorrectRecord}">
                    <table style="width:994px;">
                        <tr>
                            <td align="left">
                                <ai:paging action="/SPS/SupplierInformationUploadingAction.do" formId="Word001FormResult" jump="true" />
                             </td>
                             <td align="right">
                                <input type="button" value="${btnRegister}" id="btnRegister" class="select_button" disabled="true"/>
                             </td>
                        </tr>
                    </table>
                </c:if>
            </c:if>
        </html:form>
    </body>
</html>
