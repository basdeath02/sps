<%--
 * header.jsp
 *
 * [JP] 全画面のヘッダエリア共通のJSPです。
 * [JP]
 * [JP] 各アプリの要件に応じて実装してください。
 *
 * [EN] JSP is a common header area of the entire screen.
 * [EN]
 * [EN] Please implement depending on the requirements of each application.
 *
 * $ header.jsp 4294 2013-05-15 12:30:57Z HIDETOSHI_NAKATANI@denso.co.jp $
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
--%>
<%@ page import="com.globaldenso.ai.aij2.core.constant.Aij2Globals" %>

<%-- [JP] 変数定義 ----------------------------------------------------------------%>
<%-- [EN] Variable definition ----------------------------------------------------------------%>

<%-- [JP] 画面種別 --%>
<%-- [EN] Screen type --%>
<c:set var="type"><tiles:getAsString name="type" /></c:set>

<%-- [JP] 画面ID --%>
<%-- [EN] ID screen --%>
<c:set var="screenId" value="<%= request.getAttribute(Aij2Globals.AIJ2_SCREEN_ID_KEY) %>"/>


<%-- [JP] 画面描画 ----------------------------------------------------------------%>
<%-- [EN] Screen drawing ----------------------------------------------------------------%>


