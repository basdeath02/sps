<%--
 * coda.jsp
 *
 * [JP] 全 JSP の末尾に自動的に挿入されるフッタ情報です。
 * [JP]
 * [JP] JSP コンフィギュレーションを利用しています。( web.xml にて設定)
 *
 * [EN] It is the footer information is automatically inserted at the end of all JSP.
 * [EN]
 * [EN] I'm using the JSP configuration. (Set in web.xml)
 *
 * $ coda.jsp 4294 2013-05-15 12:30:57Z HIDETOSHI_NAKATANI@denso.co.jp $
 *
 * Copyright (c) 2012 DENSO IT SOLUTIONS. All rights reserved.
--%>
