<%--
 * base.jsp
 *
 * [JP] すべての画面レイアウトの最下層となるベースのJSPです。
 * [JP] サンプルとして、シンプル構成(ヘッダ、ボディ、フッタ)のレイアウトを定義しています。
 * [JP] 各アプリの要件に応じて、レイアウトを定義して利用してください。
 *
 * [EN] It is a JSP-based screen layout which is the lowest level of all.
 * [EN] As an example, we define a simple configuration layout (header, body, footer) of.
 * [EN] Depending on the requirements of each application, please use to define the layout.
 *
 * $ base.jsp 4294 2013-05-15 12:30:57Z HIDETOSHI_NAKATANI@denso.co.jp $
 *
 * Copyright (c) 2012 DENSO CORPORATION. All rights reserved.
--%>
<%@ page import="com.globaldenso.ai.common.core.context.DensoContext" %>

<%-- [JP] 変数定義 ----------------------------------------------------------------%>
<%-- [EN] Variable definition ----------------------------------------------------------------%>

<%-- [JP] 画面種別 --%>
<%-- [EN] Screen type --%>
<c:set var="type"><tiles:getAsString name="type" /></c:set>

<%-- [JP] テーブル幅 --%>
<%-- [EN] Table width --%>
<c:choose>
    <c:when test="${type == 'menu' || type == 'main'}">
        <c:set var="tableWidth" scope="request">980px</c:set>
    </c:when>
    <c:otherwise>
        <c:set var="tableWidth" scope="request">100%</c:set>
    </c:otherwise>
</c:choose>

<%-- [JP] 画面描画 ----------------------------------------------------------------%>
<%-- [EN] Screen drawing ----------------------------------------------------------------%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html>
    <head>
        <meta http-equiv="pragma"        content="no-cache" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="Content-Type"  content="text/html; charset=${pageContext.request.characterEncoding}" />

        <%-- [JP] ベース URL の設定 (showModalDialog の SUBMIT 対応も兼ねて、target を明示的に設定) --%>
        <%-- [EN] Setting the base URL (also serves as a support of SUBMIT showModalDialog, explicitly set the target) --%>
        <html:base ref="site" target="_self" />

        <%-- [JP] タイトル  --%>
        <%-- [EN] Title  --%>
        <title>ai_blank</title>

        <%-- CSS --%>
        <%-- 
        <link rel="stylesheet" href="css/xxx.css" />
        --%>

        <%-- JavaScript --%>
        <%--
        <script type="text/javascript" src="js/xxx.js"></script>
         --%>
    </head>
    <body>
        <div>
            <%-- [JP] ヘッダエリア(メッセージエリア含む) --%>
            <%-- [EN] (Including the message area) header area --%>
            <tiles:insert attribute="header" flush="false">
                <tiles:put name="type"><tiles:getAsString name="type" /></tiles:put>
            </tiles:insert>

            <%-- [JP] 業務エリア --%>
            <%-- [EN] Business area --%>
            <tiles:insert attribute="body" flush="false" />

            <%-- [JP] フッタ --%>
            <%-- [EN] Footer --%>
            <tiles:insert attribute="footer" flush="false">
                <tiles:put name="type"><tiles:getAsString name="type" /></tiles:put>
            </tiles:insert>
        </div>
    </body>
</html:html>