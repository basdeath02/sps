<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wcom002_MainScreen.js">
        </script>
        <title>
            <sps:label name="HELP"/>
        </title>
    </head>
    <body class="hideHorizontal">
    <html:form styleId="Wcom002Form" action="/MainScreenAction.do">
        <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
        <html:hidden property="userLogin" styleId="userLogin"/>
        <html:hidden property="userType" styleId="userType"/>
        
        <table style="height: 30px; width: 990px; border-width: 0px; padding: 0px" class="bottom">
            <tr>
                <td>
                    <sps:label name="HELP"/>
                </td>
                <td colspan="2" class="align-right">
                    <sps:label name="USER"/>
                    &nbsp; : <bean:write name="Wcom002Form" property="userLogin" />
                    <html:hidden name="Wcom002Form" property="userLogin" />
                </td>
            </tr>
        </table>
        
        <br />
        <sps:label name="CLICK_ICON_TO_OPEN_DOCUMENT" />
        <c:set var="lblDownloadManual">
            <sps:label name="DOWNLOAD_MANUAL"/>
        </c:set>
        <c:if test="${Wcom002Form.userType == 'DENSO'}">
            <div style="width:320px;">
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:300px;word-wrap:break-word;">
                    <tr>
                        <th>
                            <sps:label name="AVAILABLE_TO_DOWNLOAD" />
                        </th>
                    </tr>
                </table>
            </div>
            <div style="width:320px;height:100px;overflow-y:scroll">
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:300px;word-wrap:break-word;">
                    <tr>
                        <td class="align-left" style="width:240px;">
                            <sps:label name="DENSO_USER_MANUAL"/>
                        </td>
                        <c:set var="densoUrl">
                            <sps:label name="DENSO_MANUAL_URL"/>
                        </c:set>
                        <td class="align-center" style="width:20px;">
                           <a href="${densoUrl}" target="_self">
                               <img src="css/images/pdf.jpg" width="19" height="20" alt="${lblDownloadManual}"/>
                           </a>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-left" style="width:240px;">
                            <sps:label name="SUPPLIER_MANUAL"/>
                        </td>
                        <c:set var="supplierForDensoUrl">
                            <sps:label name="SUPP_MANUAL_FOR_DENSO_URL"/>
                        </c:set>
                        <td class="align-center" style="width:20px;">
                            <a href="${supplierForDensoUrl}" target="_self">
                                <img src="css/images/pdf.jpg" width="19" height="20" alt="${lblDownloadManual}"/>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </c:if>
        <c:if test="${Wcom002Form.userType == 'Supplier'}">
            <div style="width:320px;">
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:300px;word-wrap:break-word;">
                    <tr>
                        <th colspan="2">
                            <sps:label name="AVAILABLE_TO_DOWNLOAD"/>
                        </th>
                    </tr>
                </table>
            </div>
            <div style="width:320px;height:100px;overflow-y:scroll">
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:300px;word-wrap:break-word;">
                    <tr>
                        <td class="align-left" style="width:240px;">
                            <sps:label name="SUPPLIER_MANUAL"/>
                        </td>
                        <c:set var="supplierForSuppUrl">
                            <sps:label name="SUPP_MANUAL_FOR_SUPP_URL"/>
                        </c:set>
                        <td class="align-center" style="width:20px;">
                            <a href="${supplierForSuppUrl}" target="_self">
                                <img src="css/images/pdf.jpg" width="19" height="20" alt="${lblDownloadManual}"/>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </c:if>
        </html:form>
    </body>
</html>
