<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons"
            class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wcom002_MainScreen.js">
        </script>
        <title>
            <sps:label name="MAIN_SCREEN" />
        </title>
    </head>
    <% String user = request.getParameter("dscId"); %>
    <body class="hideHorizontal">
    <html:form styleId="Wcom002Form" action="/MainScreenAction.do" >
        <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
        <html:hidden styleId="userLogin" property="userLogin" />
        <html:hidden styleId="method" property="method" />
        <input type="hidden" name="targetLocale" id="targetLocale"/>
        <table
            style="height: 30px; width: 990px; border-width: 0px; padding: 0px"
            class="bottom">
            <tr>
                <td>
                    <sps:label name="HOME" />
                    &gt; 
                    <sps:label name="MAIN_SCREEN" />
                </td>
                <td colspan="2" class="align-right">
                    <c:if test="${not empty Wcom002Form.userLogin}">
                        <sps:label name="USER" />
                        &nbsp; : <bean:write name="Wcom002Form" property="userLogin" />
                        <html:hidden name="Wcom002Form" property="userLogin" />
                    </c:if>
                    <c:if test="${empty Wcom002Form.userLogin}">
                        <sps:label name="USER" />
                        &nbsp; : ${fn:escapeXml(Wcom002Form.dscId)}
                    </c:if>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="right">
                    <span style="font-weight: bold;">
                        <sps:label name="LANGUAGE" />
                    </span>
                    <html:select styleId="lang" styleClass="txtbox2" property="lang">
                            <html:option value="en_US">
                                <sps:label name="SELECT_ENGLISH" />
                            </html:option>
                            <html:option value="th_TH">
                                <sps:label name="SELECT_THAI" />
                            </html:option>
                            <html:option value="ja_JP">
                                <sps:label name="SELECT_JAPANESE" />
                            </html:option>
                    </html:select>
                </td>
            </tr>
        </table>
        <logic:notEmpty name="Wcom002Form"
            property="applicationMessageList">
            <div style="margin-bottom: 5px">
                <table style="width: 992px" class="tableMessage">
                    <tr>
                        <td>
                            <c:forEach var="appMsg"
                                items="${Wcom002Form.applicationMessageList}"
                                begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${fn:escapeXml(appMsg.message)} 
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
        </logic:notEmpty>
        <div style="width:990px;">
            <table class="display_data" id="tblBody" style="table-layout:fixed;width:970px;word-wrap:break-word;">
                <tr>
                    <th style="width: 140px">
                        <sps:label name="D_CD" />
                    </th>
                    <th style="width: 700px">
                        <sps:label name="MESSAGE" />
                    </th>
                    <th style="width: 100px">
                        <sps:label name="ANNOUNCE_DATE" />
                    </th>
                </tr>
            </table>
        </div>
        <div style="width:990px;height:150px;overflow-y:scroll">
            <table class="display_data" id="tblBody" style="table-layout:fixed;width:970px;word-wrap:break-word;">
                <c:if test="${not empty Wcom002Form.announceMessageDomainList}">
                    <c:forEach var="announceMessage"
                            items="${Wcom002Form.announceMessageDomainList}" begin="0"
                            step="1" varStatus="status">
                        <tr>
                            <td style="width: 140px">
                                <bean:write name="announceMessage" property="DCd" />
                            </td>
                            <td style="width: 700px">
                                <bean:write name="announceMessage" property="announceMessage" />
                            </td>
                            <td style="width: 100px">
                                <%-- FIX : wrong dateformat --%>
                                <%-- <fmt:formatDate pattern="yyyy/MM/dd" value="${announceMessage.effectStart}"/> --%>
                                <bean:write name="announceMessage" property="effectStartShow" />
                            </td>
                        </tr>
                    </c:forEach>
                </c:if>
                <c:if test="${empty Wcom002Form.announceMessageDomainList}">
                    <tr>
                        <td style="width: 140px">
                            -
                        </td>
                        <td style="width: 700px">
                            -
                        </td>
                        <td style="width: 100px">
                            -
                        </td>
                    </tr>
                </c:if>
            </table>
        </div>
        <br />
        <br />
        <span style="font-weight:bold;">
            &nbsp;&nbsp;
            <sps:label name="TASK_LIST" />
        </span>
        <c:if test="${empty Wcom002Form.userType}">
            <div style="width:990px;">
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:970px;word-wrap:break-word;">
                    <tr>
                        <th style="width: 20%">
                            <sps:label name="D_CD" />
                        </th>
                        <th style="width: 65%">
                            <sps:label name="CATEGORY" />
                        </th>
                        <th style="width: 15%">
                            <sps:label name="QTY" />
                        </th>
                    </tr>
                </table>
            </div>
            <div style="width:990px;height:240px;overflow-y:scroll">
               <table class="display_data" id="tblBody" style="table-layout:fixed;width:970px;word-wrap:break-word;">
                    <tr>
                        <td style="width: 20%" class="align-left">
                            -
                        </td>
                        <td style="width: 65%" class="align-left">
                            -
                        </td>
                        <td style="width: 15%" class="align-right">
                            -
                        </td>
                    </tr>
                </table>
            </div>
        </c:if>
        <c:if test="${Wcom002Form.userType == 'Supplier'}">
            <div style="width:990px;">
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:970px;word-wrap:break-word;">
                    <tr>
                        <th style="width: 20%">
                            <sps:label name="D_CD" />
                        </th>
                        <th style="width: 65%">
                            <sps:label name="CATEGORY" />
                        </th>
                        <th style="width: 15%">
                            <sps:label name="QTY" />
                        </th>
                    </tr>
                </table>
            </div>
            <div style="width:990px;height:240px;overflow-y:scroll">
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:970px;word-wrap:break-word;">
                <c:if test="${not empty Wcom002Form.mainScreenResultDomain}">
                    <c:forEach var="taskList"
                            items="${Wcom002Form.mainScreenResultDomain}" begin="0"
                            step="1" varStatus="status">
                        <tr>
                            <td style="width: 20%" class="align-left">
                                <bean:write name="taskList" property="DCd" />
                            </td>
                            <td style="width: 65%" class="align-left">
                                <bean:write name="taskList" property="category" />
                            </td>
                            <td style="width: 15%" class="align-right">
                                <bean:write name="taskList" property="quantity" />
                            </td>
                        </tr>
                    </c:forEach>
                </c:if>
                <c:if test="${empty Wcom002Form.mainScreenResultDomain}">
                    <tr>
                        <td style="width: 20%" class="align-left">
                            -
                        </td>
                        <td style="width: 65%" class="align-left">
                            -
                        </td>
                        <td style="width: 15%" class="align-right">
                            -
                        </td>
                    </tr>
                </c:if>
                </table>
            </div>
        </c:if>
        <c:if test="${Wcom002Form.userType == 'DENSO'}">
            <div style="width:990px;">
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:970px;word-wrap:break-word;">
                    <tr>
                        <th style="width: 80%">
                            <sps:label name="CATEGORY" />
                        </th>
                        <th style="width: 20%">
                            <sps:label name="QUANTITY" />
                        </th>
                    </tr>
                </table>
            </div>
            <div style="width:990px;height:240px;overflow-y:scroll">
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:970px;word-wrap:break-word;">
                    <c:if test="${not empty Wcom002Form.mainScreenResultDomain}">
                        <c:forEach var="taskList"
                            items="${Wcom002Form.mainScreenResultDomain}" begin="0"
                            step="1" varStatus="status">
                            <tr>
                                <td style="width: 80%" class="align-left">
                                    ${taskList.category}
                                </td>
                                <td style="width: 20%" class="align-right">
                                    ${taskList.quantity}
                                </td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    <c:if test="${empty Wcom002Form.mainScreenResultDomain}">
                        <tr>
                            <td style="width: 80%" class="align-left">
                                -
                            </td>
                            <td style="width: 20%" class="align-right">
                                -
                            </td>
                        </tr>
                    </c:if>
                </table>
            </div>
        </c:if>
        </html:form>
    </body>
</html>

