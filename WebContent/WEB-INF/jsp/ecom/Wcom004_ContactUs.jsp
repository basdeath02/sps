<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wcom002_MainScreen.js"></script>
        <title>
            <sps:label name="CONTACT_US" />
        </title>
    </head>
    <body class="hideHorizontal">
        <html:form styleId="Wcom002Form" action="/MainScreenAction.do" >
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden styleId="userLogin" property="userLogin" />
            <html:hidden styleId="method" property="method" />
            <html:hidden styleId="userType" property="userType" />
            <c:if test="${'DENSO' == Wcom002Form.userType}">
                <c:set var="filePathTha">
                    <sps:label name="DENSO_COMPANY_THA" />
                </c:set>
                <c:set var="filePathMal">
                    <sps:label name="DENSO_COMPANY_MAL" />
                </c:set>
                <c:set var="filePathInd">
                    <sps:label name="DENSO_COMPANY_IND" />
                </c:set>
                <%-- Start : [FIX] Add new DENSO Company --%>
                <c:set var="filePathTw">
                    <sps:label name="DENSO_COMPANY_TW" />
                </c:set>
                <c:set var="filePathIndia">
                    <sps:label name="DENSO_COMPANY_INDIA" />
                </c:set>
                <%-- End : [FIX] Add new DENSO Company --%>
            </c:if>
            <c:if test="${'Supplier' == Wcom002Form.userType}">
                <c:set var="filePathTha">
                    <sps:label name="DENSO_COMPANY_THA_SUP" />
                </c:set>
                <c:set var="filePathMal">
                    <sps:label name="DENSO_COMPANY_MAL_SUP" />
                </c:set>
                <c:set var="filePathInd">
                    <sps:label name="DENSO_COMPANY_IND_SUP" />
                </c:set>
                <%-- Start : [FIX] Add new DENSO Company --%>
                <c:set var="filePathTw">
                    <sps:label name="DENSO_COMPANY_TW_SUP" />
                </c:set>
                <c:set var="filePathIndia">
                    <sps:label name="DENSO_COMPANY_INDIA_SUP" />
                </c:set>
                <%-- End : [FIX] Add new DENSO Company --%>
            </c:if>
            
            <table
                style="height: 30px; width: 990px; border-width: 0px; padding: 0px"
                class="bottom">
                <tr>
                    <td>
                        <sps:label name="CONTACT_US" />
                    </td>
                    <td colspan="2" class="align-right">
                        <sps:label name="USER" />
                        &nbsp; : <bean:write name="Wcom002Form" property="userLogin" />
                        <html:hidden name="Wcom002Form" property="userLogin" />
                   </td>
                </tr>
            </table>
            <logic:notEmpty name="Wcom002Form"
                property="applicationMessageList">
                <div style="margin-bottom: 5px">
                    <table width="992px" class="tableMessage">
                        <tr>
                            <td>
                                <c:forEach var="appMsg" items="${Wcom002Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                    <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                        <c:set var="messageClass">
                                            ui-messageSuccess
                                        </c:set>
                                    </c:if>
                                    <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                        <c:set var="messageClass">
                                            ui-messageError
                                    </c:set>
                                    </c:if>
                                    <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                        <c:set var="messageClass">
                                            ui-messageWarning
                                    </c:set>
                                    </c:if>
                                        <span class="${messageClass}">
                                            ${appMsg.message} 
                                        </span>
                                    <br>
                                </c:forEach>
                            </td>
                        </tr>
                    </table>
                </div>
            </logic:notEmpty>
            <sps:label name="CLICK_ICON_TO_OPEN_DOCUMENT" />
            <div style="width:435px;">
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:400px;word-wrap:break-word;">
                    <tr>
                        <th width="405px" colspan="2">
                            <sps:label name="COMPANY_LIST" />
                        </th>
                    </tr>
                </table>
            </div>
            <div style="width:435px;height:150px;overflow-y:scroll">
                <table class="display_data" id="tblBody" style="table-layout:fixed;width:400px;word-wrap:break-word;">
                    <tr>
                        <td width="380px">
                            <sps:label name="DENSO_THAILAND" />
                        </td>
                        <td width="20px">
                            <a href="${filePathTha}"  style="font-size:10px;" target="_blank">
                                <img src="css/images/pdf.jpg" width="19" height="20" alt="${filePathTha}"/>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td width="380px">
                            <sps:label name="DENSO_MALAYSIA" />
                        </td>
                        <td width="20px">
                            <a href="${filePathMal}" style="font-size:10px;" target="_blank">
                                <img src="css/images/pdf.jpg" width="19" height="20" alt="${filePathMal}"/>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td width="380px">
                           <sps:label name="DENSO_INDONESIA" />
                        </td>
                        <td width="20px">
                            <a href="${filePathInd}" style="font-size:10px;" target="_blank">
                                <img src="css/images/pdf.jpg" width="19" height="20" alt="${filePathInd}"/>
                            </a>
                        </td>
                    </tr>
                    <%-- Start : [FIX] Add new DENSO Company --%>
                    <tr>
                        <td width="380px">
                           <sps:label name="DENSO_TAIWAN" />
                        </td>
                        <td width="20px">
                            <a href="${filePathTw}" style="font-size:10px;" target="_blank">
                                <img src="css/images/pdf.jpg" width="19" height="20" alt="${filePathTw}"/>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td width="380px">
                           <sps:label name="DENSO_INDIA" />
                        </td>
                        <td width="20px">
                            <a href="${filePathIndia}" style="font-size:10px;" target="_blank">
                                <img src="css/images/pdf.jpg" width="19" height="20" alt="${filePathIndia}"/>
                            </a>
                        </td>
                    </tr>
                    <%-- End : [FIX] Add new DENSO Company --%>
                </table>
            </div>
        </html:form>
    </body>
</html>

