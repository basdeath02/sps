<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Cache-Control" Content="no-cache">
        <meta http-equiv="expires" Content="-1">
        <meta http-equiv="pragma" Content="no-cache">
        <title>Logout</title>
    </head>
    <body>
        <p>Current user already logout.</p>
        <p>
            <a href="https://sps.asia.gscm.globaldenso.com/siteminderagent/forms_ja-JP/login-asps.fcc?TYPE=33554433&REALMOID=06-000e6dc0-24fb-143f-aaa4-16e20a05b004&GUID=&SMAUTHREASON=0&METHOD=GET&SMAGENTNAME=-SM-RgGt4Sdz5Dq%2bXBqjwzSwxG2nX2hItsTUI8KwanRq3Du0drZl2UFRsHFe4ue%2f3v%2f6&TARGET=-SM-https%3a%2f%2fsps%2easia%2egscm%2eglobaldenso%2ecom%2f">
                &gt;&gt; Login to SPS.
            </a>
        </p>
    </body>
</html>