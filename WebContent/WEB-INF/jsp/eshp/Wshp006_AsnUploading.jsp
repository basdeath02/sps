<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wshp006_AsnUploading.js">
        </script>
        <title>
            <sps:label name="ASN_UPLOADING"/>
        </title>
    </head>

    <body class="hideHorizontal">
        <html:form styleId="Wshp006Form" enctype="multipart/form-data" action="/AsnUploadingAction.do">
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden property="userLogin" styleId="userLogin"/>
            <html:hidden styleId="method" property="method" />
            <html:hidden styleId="mode" property="mode" />
            <html:hidden styleId="userLogin" property="userLogin" />
            <html:hidden styleId="sessionId" property="sessionId" />
            <html:hidden styleId="uploadDscId" property="uploadDscId" />
            <html:hidden styleId="hasErrorFlag" property="hasErrorFlag" />
            <html:hidden styleId="cannotResetMessage" property="cannotResetMessage" />
            <table id="ui-displayHeader" class="bottom">
                <tr>
                    <td>
                        <sps:label name="SHIPPING" />
                        &gt; 
                        <sps:label name="ASN_UPLOADING" />
                    </td>
                    <td colspan="2" class="align-right">
                        <sps:label name="USER" />&nbsp;:
                        <bean:write name="Wshp006Form" property="userLogin"/>
                    </td>
                </tr>
            </table>
            
            <c:choose>
                <c:when test="${not empty Wshp006Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width:992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg" items="${Wshp006Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message}
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            
            <fieldset class="fixwidth">
                <table class="display_criteria" id="ui-displayCriteria">
                    <tr>
                        <th class="align-right" style="width:135px;">
                            <sps:label name="FILE_NAME" />
                            &nbsp;:
                        </th>
                        <td class="align-left" style="width:500px;">
                            <div style="position:absolute; left:10; filter:alpha(opacity=0); opacity:0;">
                                <html:file property="fileData" styleId="fileData" style="width:610px;" size="100" maxlength="300" onchange="wshp006SetPathFile();"/>
                            </div>
                            <html:text property="fileNameUpload" value="${Wshp006Form.fileNameUpload}" styleId="fileNameUpload" style="width:500px"/>
                        </td>
                        <td style="width:300px;">
                            <c:set var="lblBrowse">
                                <sps:label name="BROWSE"/>
                            </c:set>
                            <c:set var="lblUpload">
                                <sps:label name="UPLOAD"/>
                            </c:set>
                            <c:set var="lblReset">
                                <sps:label name="RESET"/>
                            </c:set>
                            <input type="button" name="Input" id="btnBrowse" value="${lblBrowse}" class="select_button"/>
                            <input type="button" name="Input" id="btnUpload" value="${lblUpload}" class="select_button" />
                            <input type="button" name="Input" id="btnReset" value="${lblReset}" class="select_button" />
                        </td>
                    </tr>
                </table>
                <logic:notEmpty name="Wshp006Form" property="tmpUploadAsnList">
                    <div id="uploadResult">
                        <table class="display_criteria" id="ui-displayCriteria">
                            <tr>
                                <td colspan="9" class="align-left">
                                    <span style="font-weight:bold; text-decoration:underline;">
                                        <sps:label name="UPLOAD_RESULT" />
                                    </span>
                               </td>
                            </tr>
                            <tr>
                                <th style="width:10%;" class="align-right">
                                    <sps:label name="TOTAL_RECORD"/>
                                    &nbsp;:
                                </th>
                                <td style="width:3%;" class="align-right">
                                    ${Wshp006Form.totalRecord}
                                </td>
                                <td style="width:10%;" class="align-left">
                                    <sps:label name="RECORD" />
                                </td>
                                <th style="width:10%;" class="align-right">
                                    <sps:label name="CORRECT" />
                                    &nbsp;:
                                </th>
                                <td style="width:3%;" class="align-right">
                                    ${Wshp006Form.correctRecord}
                                </td>
                                <td style="width:10%;" class="align-left">
                                    <sps:label name="RECORD" />
                                </td>
                                <th style="width:10%;" class="align-right">
                                    <sps:label name="WARNING" />
                                    &nbsp;:
                                </th>
                                <td style="width:3%;" class="align-right">
                                    ${Wshp006Form.warningRecord}
                                </td>
                                <td style="width:7%;" class="align-left">
                                    <sps:label name="RECORD" />
                                </td>
                                <th style="width:9%;" class="align-right">
                                    <sps:label name="ERROR" />
                                    &nbsp;:
                                </th>
                                <td style="width:3%;" class="align-right">
                                    ${Wshp006Form.errorRecord}
                                </td>
                                <td style="width:7%;" class="align-left">
                                    <sps:label name="RECORD" />
                                </td>
                                <td style="width:6%;" class="align-right">
                                    &nbsp;
                                </td>
                                <td style="width:4%;" class="align-left">
                                    &nbsp;
                                </td>
                                <td style="width:1%;" class="align-left">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <th class="align-right">
                                    <sps:label name="ASN_NO" />
                                    &nbsp;:
                                </th>
                                <td class="align-left" colspan="2">
                                    <html:text property="asnNo" styleId="asnNo" style="width:117px"/>
                                </td>
                                <th class="align-right">
                                    <sps:label name="S_CD" />
                                    &nbsp;:
                                </th>
                                <td class="align-left" colspan="2">
                                    <html:text property="vendorCd" styleId="vendorCd" style="width:100px"/>
                                </td>
                                <th class="align-right">
                                    <sps:label name="S_P" />
                                    &nbsp;:
                                </th>
                                <td class="align-left" colspan="2">
                                    <html:text property="SPcd" styleId="supplierPlantCode" style="width:100px"/>
                                </td>
                                <th class="align-right">
                                    <sps:label name="D_CD" />
                                    &nbsp;:
                                </th>
                                <td class="align-left" colspan="2">
                                    <html:text property="DCd" styleId="densoCode" style="width:100px"/>
                                </td>
                                <th class="align-right">
                                    <sps:label name="D_P" />
                                    &nbsp;:
                                </th>
                                <td class="align-left" colspan="2">
                                    <html:text property="DPcd" styleId="densoPlantCode" style="width:100px"/>
                                </td>
                            </tr>
                         </table>
                    </div>
                </logic:notEmpty>
            </fieldset>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        <html:form styleId="Wshp006FormResult" action="/AsnUploadingAction.do" >
            <html:hidden property="userLogin" styleId="userLogin"/>
            <html:hidden property="method" styleId="methodResult"/>
            <html:hidden property="mode" styleId="modeResult"/>
            <html:hidden property="userLogin"/>
            <html:hidden property="sessionId"/>
            <html:hidden property="uploadDscId"/>
            <html:hidden property="totalRecord"/>
            <html:hidden property="correctRecord"/>
            <html:hidden property="warningRecord"/>
            <html:hidden property="errorRecord"/>
            <html:hidden property="asnNo"/>
            <html:hidden property="vendorCd"/>
            <html:hidden property="SPcd"/>
            <html:hidden property="DCd"/>
            <html:hidden property="DPcd"/>
            <html:hidden property="hasErrorFlag" styleId="hasErrorFlagResult"/>
            <html:hidden property="fileNameUpload" styleId="fileNameUpload"/>
            <html:hidden property="cannotResetMessage" />
            <c:set var="visibilityFlag">
                hidden
            </c:set>
            <logic:notEmpty name="Wshp006Form" property="tmpUploadAsnList">
                <c:set var="visibilityFlag">
                    visible
                </c:set>
            </logic:notEmpty>
            <div id="result" style="visibility: ${visibilityFlag};">
                <div style="width:994px;">
                    <table class="display_data" id="tblBody" style="table-layout:fixed;width:974px;word-wrap:break-word;">
                        <tr>
                            <th style="width:5%;" scope="col">
                                <sps:label name="UPLOAD_RESULT" />
                            </th>
                            <th style="width:3%;" scope="col">
                                <sps:label name="CSV_LINE_NO" />
                            </th>
                            <th style="width:16%;" scope="col">
                                <sps:label name="ERROR_CODE" />
                            </th>
                            <th style="width:8%;" scope="col">
                                <sps:label name="D/O_NO" />
                            </th>
                            <th style="width:3%;" scope="col">
                                <sps:label name="REV" />
                            </th>
                            <th style="width:4%;" scope="col">
                                <sps:label name="RCV_LANE"/>
                            </th>
                            <th style="width:3%;" scope="col">
                                <sps:label name="TM"/>
                            </th>
                            <th style="width:11%;" scope="col">
                                <sps:label name="D_PART_NO" />
                            </th>
                            <th style="width:11%;" scope="col">
                                <sps:label name="S_PART_NO" />
                            </th>
                            <th style="width:7%;" scope="col">
                                <sps:label name="SHIPPING_QTY" />
                            </th>
                            <th style="width:3%;" scope="col">
                                <sps:label name="UM" />
                            </th>
                            <th style="width:5%;" scope="col">
                                <sps:label name="SCAN_FLAG" />
                            </th>
                            <th style="width:3%;" scope="col">
                                <sps:label name="WH" />
                            </th>
                        </tr>
                    </table>
                </div>
                <div style="width:994px; height:330px; overflow-y:scroll">
                    <table class="display_data" id="tblBody" style="table-layout:fixed;width:974px;word-wrap:break-word;">
                        <col style="width:5%;">
                        <col style="width:3%;">
                        <col style="width:16%;">
                        <col style="width:8%;">
                        <col style="width:3%;">
                        <col style="width:4%;">
                        <col style="width:3%;">
                        <col style="width:11%;">
                        <col style="width:11%;">
                        <col style="width:7%;">
                        <col style="width:3%;">
                        <col style="width:5%;">
                        <col style="width:3%;">
                        <c:forEach var="tmpUploadAsnResult" items="${Wshp006Form.tmpUploadAsnList}" begin="0" step="1" varStatus="status">
                            <tr class="${(status.index + 1) % 2 == 0 ? 'blueLight' : ''}">
                                <td class="align-left">
                                    <c:choose>
                                        <c:when test="${'0' == tmpUploadAsnResult.tmpUploadAsnDomain.isErrorFlg}">
                                            <sps:label name="OK" />
                                        </c:when>
                                        <c:otherwise>
                                            <span class="ui-messageError2">
                                                <sps:label name="NG" />
                                            </span>
                                        </c:otherwise>
                                    </c:choose>
                                    <html:hidden name="tmpUploadAsnResult" property="tmpUploadAsnDomain.spsDoNo" indexed="true"/>
                                    <html:hidden name="tmpUploadAsnResult" property="tmpUploadAsnDomain.revision" indexed="true"/>
                                    <html:hidden name="tmpUploadAsnResult" property="tmpUploadAsnDomain.rcvLane" indexed="true"/>
                                    <html:hidden name="tmpUploadAsnResult" property="tmpUploadAsnDomain.tm" indexed="true"/>
                                    <html:hidden name="tmpUploadAsnResult" property="tmpUploadAsnDomain.DPn" indexed="true"/>
                                    <html:hidden name="tmpUploadAsnResult" property="tmpUploadAsnDomain.SPn" indexed="true"/>
                                    <html:hidden name="tmpUploadAsnResult" property="shippingQtyStr" indexed="true"/>
                                    <html:hidden name="tmpUploadAsnResult" property="tmpUploadAsnDomain.unitOfMeasure" indexed="true"/>
                                    <html:hidden name="tmpUploadAsnResult" property="tmpUploadAsnDomain.isErrorFlg" indexed="true"/>
                                    <html:hidden name="tmpUploadAsnResult" property="tmpUploadAsnDomain.csvLineNo" indexed="true"/>
                                    <html:hidden name="tmpUploadAsnResult" property="tmpUploadAsnDomain.errorCode" indexed="true"/>
                                    <html:hidden name="tmpUploadAsnResult" property="receiveByScan" indexed="true"/>
                                    <html:hidden name="tmpUploadAsnResult" property="whPrimeReceiving" indexed="true"/>
                                </td>
                                <td class="align-right">
                                    ${tmpUploadAsnResult.tmpUploadAsnDomain.csvLineNo}
                                </td>
                                <td class="align-left">
                                    ${tmpUploadAsnResult.tmpUploadAsnDomain.errorCode}
                                </td>
                                <td class="align-left">
                                    ${tmpUploadAsnResult.tmpUploadAsnDomain.spsDoNo}
                                </td>
                                <td class="align-left">
                                    ${tmpUploadAsnResult.tmpUploadAsnDomain.revision}
                                </td>
                                <td class="align-left">
                                    ${tmpUploadAsnResult.tmpUploadAsnDomain.rcvLane}
                                </td>
                                <td class="align-left">
                                    ${tmpUploadAsnResult.tmpUploadAsnDomain.tm}
                                </td>
                                <td class="align-left">
                                    ${tmpUploadAsnResult.tmpUploadAsnDomain.DPn}
                                </td>
                                <td class="align-left">
                                    ${tmpUploadAsnResult.tmpUploadAsnDomain.SPn}
                                </td>
                                <td class="align-right">
                                    ${tmpUploadAsnResult.shippingQtyStr}
                                </td>
                                <td class="align-left">
                                    ${tmpUploadAsnResult.tmpUploadAsnDomain.unitOfMeasure}
                                </td>
                                <td class="align-left">
                                    ${tmpUploadAsnResult.receiveByScan}
                                </td>
                                <td class="align-left">
                                    ${tmpUploadAsnResult.whPrimeReceiving}
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <table style="width:994px;border-width:0px">
                    <c:set var="lblDownload">
                        <sps:label name="DOWNLOAD"/>
                    </c:set>
                    <c:set var="labelGroupAsn">
                        <sps:label name="GROUPASN" />
                    </c:set>
                    <tr>
                        <td align="left">
                            <ai:paging action="/SPS/AsnUploadingAction.do" formId="Wshp006FormResult" jump="true"/>
                        </td>
                        <td align="right">
                            <input type="button" name="Input" id="btnDownload" value="${lblDownload}" class="select_button" />
                            <input type="button" name="Input" id="btnRegister" value="${labelGroupAsn}" class="select_button" />
                        </td>
                    </tr>
                </table>
            </div>
        </html:form>
    </body>
</html>
