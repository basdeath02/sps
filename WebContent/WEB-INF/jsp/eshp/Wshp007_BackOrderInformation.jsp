<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wshp007_BackOrderInformation.js" >
        </script>
        <title>
            <sps:label name="BACK_ORDER_INFORMATION" />
        </title>
        <script type="text/javascript">
            var labelAll = '<sps:label name="ALL_CRITERIA" />';
            var labelPleaseSelect = '<sps:label name="PLEASE_SELECT_CRITERIA" />';
        </script>
    </head>
    <body class="hideHorizontal" style="width:99%">
        <html:form styleId="Wshp007FormCriteria" action="/BackOrderInformationAction.do" >
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden property="method" styleId="method"/>
            <html:hidden property="mode" styleId="mode"/>
            <html:hidden property="cannotResetMessage" styleId="cannotResetMessage"/>
            <html:hidden property="cannotDownloadMessage" styleId="cannotDownloadMessage"/>
            <!-- Key for paging. -->
            <html:hidden styleId="currentPage" property="currentPage"/>
            <html:hidden styleId="row" property="row"/>
            <html:hidden styleId="totalCount" property="totalCount"/>
            
            <logic:notEmpty name="Wshp007Form" property="companySupplierList">
                <c:forEach var="companySupplierCode" items="${Wshp007Form.companySupplierList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplierCode" property="vendorCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp007Form" property="companyDensoList">
                <c:forEach var="companyDensoCode" items="${Wshp007Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoCode" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp007Form" property="transList">
                <c:forEach var="transItem" items="${Wshp007Form.transList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="transItem" property="miscCode" indexed="true"/>
                    <html:hidden name="transItem" property="miscValue" indexed="true"/>
                 </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp007Form" property="supplierAuthenList">
                <c:forEach var="supplierAuthen" items="${Wshp007Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                    <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp007Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wshp007Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            
            <table table id="ui-displayHeader" class="bottom">
                <tr>
                    <td>
                        <sps:label name="SHIPPING" />
                        &gt;
                        <sps:label name="BACK_ORDER_INFORMATION" />
                    </td>
                    <td colspan="2" class="align-right" >
                        <sps:label name="USER" />&nbsp;:
                        <bean:write name="Wshp007Form" property="userLogin"/>
                    </td>
                </tr>
            </table>
            
            <c:choose>
                <c:when test="${not empty Wshp007Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width:992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg" items="${Wshp007Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message}
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            
            <fieldset class="fixwidth">
                <c:set var="lbPleaseSelect">
                    <sps:label name="PLEASE_SELECT_CRITERIA" />
                </c:set>
                <c:set var="lbAll">
                    <sps:label name="ALL_CRITERIA" />
                </c:set>
                <table class="display_criteria" id="ui-displayCriteria">
                    <col style="width:95px;"/>
                    <col style="width:243px;"/>
                    <col style="width:41px;"/>
                    <col style="width:112px;"/>
                    <col style="width:53px;"/>
                    <col style="width:112px;"/>
                    <col style="width:41px;"/>
                    <col style="width:112px;"/>
                    <col style="width:54px;"/>
                    <col style="width:112px;"/>
                    <tr>
                        <th>
                            <sps:label name="DELIVERY_DATE" />
                            &nbsp;:
                        </th>
                        <td>
                            <sps:calendar name="deliveryDateFrom" id="deliveryDateFrom" value="${Wshp007Form.deliveryDateFrom}"/>
                            -
                            <sps:calendar name="deliveryDateTo" id="deliveryDateTo" value="${Wshp007Form.deliveryDateTo}"/>
                        </td>
                        <th>
                            <sps:label name="S_CD" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp007Form" property="vendorCd" styleId="SCd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp007Form" property="companySupplierList">
                                    <c:forEach var="companySupplierCode" items="${Wshp007Form.companySupplierList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${companySupplierCode.vendorCd}">
                                            ${companySupplierCode.vendorCd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="S_P" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp007Form" property="SPcd" styleId="SPcd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp007Form" property="plantSupplierList">
                                    <c:forEach var="plantSupplier" items="${Wshp007Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${plantSupplier.SPcd}">
                                             ${plantSupplier.SPcd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="D_CD" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp007Form" property="DCd" styleId="DCd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp007Form" property="companyDensoList">
                                    <c:forEach var="companyDensoCode" items="${Wshp007Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${companyDensoCode.DCd}">
                                            ${companyDensoCode.DCd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="D_P" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp007Form" property="DPcd" styleId="DPcd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp007Form" property="plantDensoList">
                                    <c:forEach var="plantDenso" items="${Wshp007Form.plantDensoList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${plantDenso.DPcd}">
                                            ${plantDenso.DPcd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                    </tr>
                </table>
                <table class="display_criteria" id="ui-displayCriteria">
                    <col style="width:95px;"/>
                    <col style="width:100px;"/>
                    <col style="width:69px;"/>
                    <col style="width:135px;"/>
                    <col style="width:72px;"/>
                    <col style="width:126px;"/>
                    <col style="width:72px;"/>
                    <col style="width:146px;"/>
                    <col style="width:50px;"/>
                    <col style="width:110px;"/>
                    <tr>
                        <th>
                            <sps:label name="D/O_NO" />
                            &nbsp;:
                         </th>
                         <td>
                             <html:text name="Wshp007Form" property="spsDoNo" style="width:100px" />
                         </td>
                         <th>
                             <sps:label name="ASN_NO" />
                             &nbsp;:
                         </th>
                         <td>
                             <html:text name="Wshp007Form" property="asnNo" style="width:130px" />
                         </td>
                         <th>
                             <sps:label name="D_PART_NO" />
                             &nbsp;:
                         </th>
                         <td>
                             <html:text name="Wshp007Form" property="DPn" style="width:120px" />
                         </td>
                         <th>
                             <sps:label name="S_PART_NO" />
                             &nbsp;:
                         </th>
                         <td>
                             <html:text name="Wshp007Form" property="SPn" style="width:140px" />
                         </td>
                         <th>
                             <sps:label name="TM" />
                             &nbsp;:
                         </th>
                         <td>
                             <html:select name="Wshp007Form" property="transportModeCb" styleId="transportationMode" style="width:110px">
                                 <html:option value="">
                                     ${lbAll}
                                 </html:option>
                                 <c:forEach var="transItem" items="${Wshp007Form.transList}" begin="0" step="1" varStatus="status">
                                     <html:option value="${transItem.miscCode}">
                                         ${transItem.miscCode} : ${transItem.miscValue}
                                     </html:option>
                                 </c:forEach>
                             </html:select>
                         </td>
                    </tr>
                </table>
            </fieldset>
            <table style="width:994px">
                <c:set var="labelSearch">
                    <sps:label name="SEARCH"/>
                </c:set>
                <c:set var="labelReset">
                    <sps:label name="RESET"/>
                </c:set>
                <c:set var="labelDownload">
                    <sps:label name="DOWNLOAD"/>
                </c:set>
                <tr>
                    <td class="align-right">
                        <input type="button" id="btnSearch" value="${labelSearch}" class="select_button" />
                        &nbsp;
                        <input type="button" id="btnReset" value="${labelReset}" class="select_button" />
                        &nbsp;
                        <input type="button" id="btnDownload" value="${labelDownload}" class="select_button" />
                    </td>
                </tr>
            </table>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        <html:form styleId="Wshp007FormResult" action="/BackOrderInformationAction.do" >
            <html:hidden property="method" styleId="methodResult" value="Search"/>
            <html:hidden property="deliveryDateFrom" styleId="deliveryDateFrom"/>
            <html:hidden property="deliveryDateTo" styleId="deliveryDateTo"/>
            <html:hidden property="vendorCd" styleId="SCd"/>
            <html:hidden property="SPcd"/>
            <html:hidden property="DCd" styleId="DCd"/>
            <html:hidden property="DPcd"/>
            <html:hidden property="spsDoNo"/>
            <html:hidden property="asnNo"/>
            <html:hidden property="DPn"/>
            <html:hidden property="SPn"/>
            <html:hidden property="transportModeCb"/>
            <html:hidden property="statusIndex" styleId="statusIndex" />
            <html:hidden property="cannotResetMessage"/>
            <html:hidden property="cannotDownloadMessage"/>
            
             <logic:notEmpty name="Wshp007Form" property="companySupplierList">
                <c:forEach var="companySupplierCode" items="${Wshp007Form.companySupplierList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplierCode" property="vendorCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp007Form" property="companyDensoList">
                <c:forEach var="companyDensoCode" items="${Wshp007Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoCode" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp007Form" property="transList">
                <c:forEach var="transItem" items="${Wshp007Form.transList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="transItem" property="miscCode" indexed="true"/>
                    <html:hidden name="transItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp007Form" property="supplierAuthenList">
                <c:forEach var="supplierAuthen" items="${Wshp007Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                    <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp007Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wshp007Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            
            <c:choose>
                <c:when test="${0 == Wshp007Form.max}">
                    <c:set var="visibilityFlag">
                        hidden
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="visibilityFlag">
                        visible
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="result" style="visibility: ${visibilityFlag};">
                <div style="width:994px;height:330px;position:absolute;overflow-x:auto;overflow-y:auto;">
                    <table class="display_data" id="tblBody" style="table-layout:fixed;width:1115px;word-wrap:break-word;">
                        <tr>
                            <th width="20px" >
                                <sps:label name="NO" />
                            </th>
                            <th width="95px">
                                <sps:label name="DELIVERY_DATE" />
                            </th>
                            <th width="95px">
                                <sps:label name="D/O_NO" />
                            </th>
                            <th width="30px">
                                <sps:label name="REV" />
                            </th>
                            <th width="40px">
                                <sps:label name="PN_SHIP_STATUS" />
                            </th>
                            <th width="115px">
                                <sps:label name="D_PART_NO" />
                            </th>
                            <th width="150px">
                                <sps:label name="S_PART_NO" />
                            </th>
                            <th width="65px">
                                <sps:label name="ORDER_QTY" />
                            </th>
                            <th width="90px">
                                <sps:label name="TOTAL_SHIPPING_QTY" />
                            </th>
                            <th width="80px">
                                <sps:label name="TOTAL_RECEIVED_QTY" />
                            </th>
                            <th width="80px">
                                <sps:label name="TOTAL_IN_TRANSIT_QTY" />
                            </th>
                            <th width="85px">
                                <sps:label name="TOTAL_BACK_ORDER_QTY" />
                            </th>
                            <th width="30px">
                                <sps:label name="UM" />
                            </th>
                            <th width="20px">
                                <sps:label name="TM" />
                            </th>
                            <th width="30px">
                                <sps:label name="S_CD" />
                            </th>
                            <th width="25px">
                                <sps:label name="S_P" />
                            </th>
                            <th width="40px">
                                <sps:label name="D_CD" />
                            </th>
                            <th width="25px">
                                <sps:label name="D_P" />
                            </th>
                        </tr>
                        <c:set var="startRow">
                            ${(Wshp007Form.pageNo * Wshp007Form.count) - Wshp007Form.count}
                        </c:set>
                        <c:forEach var="backOrderInformation" items="${Wshp007Form.backOrderInformationList}" begin="0" step="1" varStatus="status" >
                            <tr class="${(status.index + 1) % 2 == 0 ? 'blueLight' : ''}">
                                <td class="align-right">
                                    ${startRow + (status.index+1)}
                                </td>
                                <td class="align-left">
                                    ${backOrderInformation.deliveryDate}
                                </td>
                                <td class="align-left">
                                
                                    <%-- [IN004] : create link in every case. --%>
                                    <%-- <c:choose>
                                        <c:when test="${'1' == backOrderInformation.linkAsnFlag}">
                                            <a href="javascript:void(0)"
                                                onclick="wshp007ForwardPage('${status.index}'); return false;">
                                                ${backOrderInformation.spsTDoDomain.spsDoNo}
                                            </a>
                                        </c:when>
                                        <c:otherwise>
                                            ${backOrderInformation.spsTDoDomain.spsDoNo}
                                        </c:otherwise>
                                    </c:choose> --%>
                                    <a href="javascript:void(0)"
                                        onclick="wshp007ForwardPage('${status.index}'); return false;">
                                        ${backOrderInformation.spsTDoDomain.spsDoNo} <c:if test="${backOrderInformation.spsTDoDetailDomain.backorderFlg == '1'}"><font color="red">*</font></c:if>
                                    </a>
                                    
                                    <html:hidden name="backOrderInformation" property="spsTDoDomain.doId" indexed="true"/>
                                    <html:hidden name="backOrderInformation" property="spsTDoDomain.spsDoNo" indexed="true"/>
                                    <html:hidden name="backOrderInformation" property="spsTDoDomain.revision" indexed="true"/>
                                    <html:hidden name="backOrderInformation" property="spsTDoDomain.truckRoute" indexed="true"/>
                                    <html:hidden name="backOrderInformation" property="spsTDoDomain.truckSeq" indexed="true"/>
                                    <html:hidden name="backOrderInformation" property="spsTDoDomain.DCd" indexed="true"/>
                                </td>
                                <td class="align-left">
                                    ${backOrderInformation.spsTDoDomain.revision}
                                </td>
                                <td class="align-left">
                                    ${backOrderInformation.spsTDoDetailDomain.pnShipmentStatus}
                                </td>
                                <td class="align-left">
                                    ${backOrderInformation.spsTDoDetailDomain.DPn}
                                </td>
                                <td class="align-left">
                                    ${backOrderInformation.spsTDoDetailDomain.SPn}
                                </td>
                                <td class="align-right">
                                    ${backOrderInformation.currentOrderQty}
                                </td>
                                <td class="align-right">
                                    ${backOrderInformation.shippingRoundQty}
                                </td>
                                <td class="align-right">
                                    ${backOrderInformation.totalReceivedQty}
                                </td>
                                <td class="align-right">
                                    ${backOrderInformation.totalInTransitQty}
                                </td>
                                <td class="align-right">
                                    ${backOrderInformation.totalBackOrderQty}
                                </td>
                                <td class="align-left">
                                    ${backOrderInformation.spsTDoDetailDomain.unitOfMeasure}
                                </td>
                                <td class="align-left">
                                    ${backOrderInformation.spsTDoDomain.tm}
                                </td>
                                <td class="align-left">
                                    ${backOrderInformation.spsTDoDomain.vendorCd}
                                </td>
                                <td class="align-left">
                                    ${backOrderInformation.spsTDoDomain.SPcd}
                                </td>
                                <td class="align-left">
                                    ${backOrderInformation.spsTDoDomain.DCd}
                                </td>
                                <td class="align-left">
                                    ${backOrderInformation.spsTDoDomain.DPcd}
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div style="width:98%;height:360px;">
                    &nbsp;
                </div>
                <table style="width:994px;">
                    <tr>
                        <td align="left">
                            <ai:paging action="/SPS/BackOrderInformationAction.do" formId="Wshp007FormResult" jump="true" />
                        </td>
                    </tr>
                </table>
                <div id="displayLegend" style="margin: 0;padding-top: 2px;">
                    <table bgcolor="#EEEEEE" style="width: 994px;border: 0;">
                        <tr>
                            <td align="left" style="font-size:10px;">
                                <span style="font-weight:bold;">
                                    <sps:label name="LEGEND"/>
                                    <span style="text-decoration:underline;">
                                        <sps:label name="PN_SHIP_STATUS"/>
                                    </span>
                                </span>
                                <sps:label name="ISS_PTS_CPS_CCL" />
                            </td>
                            <td class="align-right">
                                <a href="javascript:void(0)" style="font-size:10px;" id="moreLegend">
                                    &gt;&gt;&nbsp;
                                    <sps:label name="MORE"/>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </html:form>
        <html:form styleId="Wshp007FormDownload" action="/BackOrderInformationAction.do">
            <html:hidden property="method" styleId="methodDownload"/>
            <html:hidden property="vendorCd" styleId="SCd"/>
            <html:hidden property="DCd" styleId="DCd"/>
            <html:hidden property="cannotResetMessage"/>
            <html:hidden property="cannotDownloadMessage"/>
            
            <logic:notEmpty name="Wshp007Form" property="companySupplierList">
                <c:forEach var="companySupplierCode" items="${Wshp007Form.companySupplierList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplierCode" property="vendorCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp007Form" property="companyDensoList">
                <c:forEach var="companyDensoCode" items="${Wshp007Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoCode" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp007Form" property="transList">
                <c:forEach var="transItem" items="${Wshp007Form.transList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="transItem" property="miscCode" indexed="true"/>
                    <html:hidden name="transItem" property="miscValue" indexed="true"/>
                 </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp007Form" property="supplierAuthenList">
                <c:forEach var="supplierAuthen" items="${Wshp007Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                    <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp007Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wshp007Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
        </html:form>
    </body>
</html>
