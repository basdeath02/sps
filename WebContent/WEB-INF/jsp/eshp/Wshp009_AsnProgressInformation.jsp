<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wshp009_AsnProgress.js" >
        </script>
        <title>
            <sps:label name="ASN_PROGRESS_INFORMATION" />
        </title>
        <script type="text/javascript">
            var labelAll = '<sps:label name="ALL_CRITERIA" />';
            var labelPleaseSelect = '<sps:label name="PLEASE_SELECT_CRITERIA" />';
        </script>
    </head>
    <body class="hideHorizontal" style="width:99%">
        <html:form styleId="Wshp009FormCriteria" action="/AsnDetailProgressAction.do" >
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden property="userLogin" styleId="userLogin"/>
            <html:hidden property="method" styleId="method"/>
            <html:hidden property="mode" styleId="mode"/>
            <html:hidden property="fileId" styleId="pdfFileId"/>
            <html:hidden property="cannotResetMessage" styleId="cannotResetMessage"/>
            <html:hidden property="cannotDownloadMessage" styleId="cannotDownloadMessage"/>
            
            <!-- Key for paging. -->
            <html:hidden styleId="currentPage" property="currentPage"/>
            <html:hidden styleId="row"  property="row"/>
            <html:hidden styleId="totalCount" property="totalCount"/>
            
            <logic:notEmpty name="Wshp009Form" property="companyDensoList">
                <c:forEach var="companyDenso" items="${Wshp009Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDenso" property="companyName" indexed="true"/>
                    <html:hidden name="companyDenso" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp009Form" property="companySupplierList">
                <c:forEach var="companySupplier" items="${Wshp009Form.companySupplierList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplier" property="companyName" indexed="true"/>
                    <html:hidden name="companySupplier" property="vendorCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp009Form" property="asnStatusList">
                <c:forEach var="asnStatusItem" items="${Wshp009Form.asnStatusList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="asnStatusItem" property="miscCode" indexed="true"/>
                    <html:hidden name="asnStatusItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp009Form" property="invoiceStatusList">
                <c:forEach var="invoiceStatusItem" items="${Wshp009Form.invoiceStatusList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="invoiceStatusItem" property="miscCode" indexed="true"/>
                    <html:hidden name="invoiceStatusItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp009Form" property="supplierAuthenList">
                <c:forEach var="supplierAuthen" items="${Wshp009Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                    <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp009Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wshp009Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            
            <table style="height: 30px;width: 990px; padding:0; cellspacing:0; border:0" class="bottom">
                <tr>
                    <td>
                        <sps:label name="SHIPPING" />
                        &gt;
                        <sps:label name="ASN_PROGRESS_DETAIL" />
                    </td>
                    <td colspan="2" class="align-right" >
                        <sps:label name="USER" />&nbsp;:
                        <bean:write name="Wshp009Form" property="userLogin"/>
                    </td>
                </tr>
            </table>
            
            <c:choose>
                <c:when test="${not empty Wshp009Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width: 990px;" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg" items="${Wshp009Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message}
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
                
            <fieldset class="fixwidth">
                <c:set var="lbPleaseSelect">
                    <sps:label name="PLEASE_SELECT_CRITERIA" />
                </c:set>
                <c:set var="lbAll">
                    <sps:label name="ALL_CRITERIA" />
                </c:set>
                <table class="display_criteria" id="ui-displayCriteria" style="padding-left:1px;border-spacing:0;">
                    <col style="width:8%"/>
                    <col style="width:20%"/>
                    <col style="width:8%"/>
                    <col style="width:20%"/>
                    <col style="width:8%"/>
                    <col style="width:20%"/>
                    <tr>
                        <th>
                            <sps:label name="ACTUAL_ETD" />
                            &nbsp;:
                        </th>
                        <td>
                            <sps:calendar name="actualEtdFrom" id="actualEtdFrom" value="${Wshp009Form.actualEtdFrom}"/>
                                -
                            <sps:calendar name="actualEtdTo" id="actualEtdTo" value="${Wshp009Form.actualEtdTo}"/>
                        </td>
                        <th>
                            <sps:label name="ACTUAL_ETA" />
                            &nbsp;:
                        </th>
                        <td>
                            <sps:calendar name="actualEtaFrom" id="actualEtaFrom" value="${Wshp009Form.actualEtaFrom}"/>
                                -
                            <sps:calendar name="actualEtaTo" id="actualEtaTo" value="${Wshp009Form.actualEtaTo}"/>
                        </td>
                        <th>
                            <sps:label name="INVOICE_DATE" />
                            &nbsp;:
                        </th>
                        <td>
                            <sps:calendar name="invoiceDateFrom" id="invoiceDateFrom" value="${Wshp009Form.invoiceDateFrom}"/>
                                -
                            <sps:calendar name="invoiceDateTo" id="invoiceDateTo" value="${Wshp009Form.invoiceDateTo}"/>
                        </td>
                    </tr>
                </table>
                <table class="display_criteria" id="ui-displayCriteria">
                    <col style="width:10%;"/>
                    <col style="width:23.35%;"/>
                    <col style="width:8%;"/>
                    <col style="width:13%;"/>
                    <col style="width:8%;"/>
                    <col style="width:13%;"/>
                    <col style="width:8%;"/>
                    <col style="width:16%;"/>
                    <tr>
                        <th>
                            <sps:label name="S_CD" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp009Form" property="vendorCd" styleId="vendorCd" styleClass="mandatory" style="width:123px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp009Form" property="companySupplierList">
                                    <c:forEach var="companySupplier" items="${Wshp009Form.companySupplierList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${companySupplier.vendorCd}">
                                            ${companySupplier.vendorCd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="S_P" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp009Form" property="SPcd" styleId="SPcd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp009Form" property="plantSupplierList">
                                    <c:forEach var="plantSupplier" items="${Wshp009Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${plantSupplier.SPcd}">
                                             ${plantSupplier.SPcd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="D_CD" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp009Form" property="DCd" styleId="DCd" styleClass="mandatory" style="width:117px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp009Form" property="companyDensoList">
                                    <c:forEach var="companyDenso" items="${Wshp009Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${companyDenso.DCd}">
                                             ${companyDenso.DCd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="D_P" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp009Form" property="DPcd" styleId="DPcd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp009Form" property="plantDensoList">
                                    <c:forEach var="plantDenso" items="${Wshp009Form.plantDensoList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${plantDenso.DPcd}">
                                             ${plantDenso.DPcd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <sps:label name="ASN_STATUS" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp009Form" property="asnStatus" styleId="asnStatus" style="width:160px">
                                <logic:notEmpty name="Wshp009Form" property="asnStatusList">
                                    <html:option value="ALL">
                                        ${lbAll}
                                    </html:option>
                                    <c:forEach var="asnStatus" items="${Wshp009Form.asnStatusList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${asnStatus.miscCode}">
                                             ${asnStatus.miscValue}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="ASN_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp009Form" property="asnNo" style="width:108px" />
                        </td>
                        <th>
                            <sps:label name="D_PART_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp009Form" property="DPn" style="width:108px" />
                        </td>
                        <th>
                            <sps:label name="S_PART_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp009Form" property="SPn" style="width:158px" />
                        </td>
                    </tr>
                </table>
                <table class="display_criteria" id="ui-displayCriteria">
                    <tr>
                        <th style="width: 97px">
                            <sps:label name="INVOICE_STATUS" />
                            &nbsp;:
                        </th>
                        <td style="width: 227px">
                            <html:select name="Wshp009Form" property="invoiceStatus" styleId="invoiceStatus" style="width:224px">
                                <logic:notEmpty name="Wshp009Form" property="invoiceStatusList">
                                    <html:option value="">
                                        ${lbAll}
                                    </html:option>
                                    <c:forEach var="invStatus" items="${Wshp009Form.invoiceStatusList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${invStatus.miscCode}">
                                            ${invStatus.miscCode} : ${invStatus.miscValue}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th style="width: 77px">
                            <sps:label name="INVOICE_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp009Form" property="invoiceNo" style="width:190px" maxlength="25"/>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <table style="width:994px">
                <c:set var="labelSearch">
                    <sps:label name="SEARCH"/>
                </c:set>
                <c:set var="labelReset">
                    <sps:label name="RESET"/>
                </c:set>
                <c:set var="labelDownload">
                    <sps:label name="DOWNLOAD"/>
                </c:set>
                <tr>
                    <td colspan="6" class="align-right">
                        <input type="button" id="btnSearch" value="${labelSearch}" class="select_button"/>
                        &nbsp;
                        <input type="button" id="btnReset" value="${labelReset}" class="select_button"/>
                        &nbsp;
                        <input type="button" id="btnDownload"  value="${labelDownload}" class="select_button" />
                    </td>
                </tr>
            </table>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        <html:form styleId="Wshp009FormResult" action="/AsnDetailProgressAction.do" >
            <html:hidden property="userLogin" styleId="userLogin"/>
            <html:hidden property="mode" styleId="modeResult"/>
            <html:hidden property="method" styleId="methodResult" value="Search"/>
            <html:hidden property="actualEtdFrom" styleId="actualEtdFrom"/>
            <html:hidden property="actualEtdTo" styleId="actualEtdTo"/>
            <html:hidden property="actualEtaFrom" styleId="actualEtaFrom"/>
            <html:hidden property="actualEtaTo" styleId="actualEtaTo"/>
            <html:hidden property="invoiceDateFrom" styleId="invoiceDateFrom"/>
            <html:hidden property="invoiceDateTo" styleId="invoiceDateTo"/>
            <html:hidden property="asnStatus" styleId="asnStatus"/>
            <html:hidden property="asnNo" styleId="asnNo"/>
            <html:hidden property="invoiceNo" styleId="invoiceNo"/>
            <html:hidden property="invoiceStatus" styleId="invoiceStatus"/>
            <html:hidden property="SPn" styleId="SPn"/>
            <html:hidden property="DPn" styleId="DPn"/>
            <html:hidden property="vendorCd" styleId="vendorCd"/>
            <html:hidden property="SPcd" styleId="SPcd"/>
            <html:hidden property="DCd" styleId="DCd"/>
            <html:hidden property="DPcd" styleId="DPcd"/>
            
            <!-- Parameter for selected row and link.-->
            <html:hidden styleId="invoiceIdCriteria" property="invoiceIdCriteria"/>
            <html:hidden styleId="selectInvoiceDate" property="selectInvoiceDate"/>
            <html:hidden styleId="selectCnDate" property="selectCnDate"/>
            <html:hidden styleId="rcvStatusCriteria" property="rcvStatusCriteria"/>
            <html:hidden styleId="invoiceNoCriteria" property="invoiceNoCriteria"/>
            <html:hidden styleId="cnNoCriteria" property="cnNoCriteria"/>
            <html:hidden styleId="DCdCriteria" property="DCdCriteria"/>
            <html:hidden styleId="DPcdCriteria" property="DPcdCriteria"/>
            <html:hidden styleId="SCdCriteria" property="SCdCriteria"/>
            <html:hidden styleId="SPcdCriteria" property="SPcdCriteria"/>
            <html:hidden styleId="selectedAsnNo" property="selectedAsnNo"/>
            <html:hidden styleId="selectedDCd" property="selectedDCd"/>
            
            <html:hidden property="cannotResetMessage"/>
            <html:hidden property="cannotDownloadMessage"/>
            
            <logic:notEmpty name="Wshp009Form" property="companyDensoList">
                <c:forEach var="companyDenso" items="${Wshp009Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDenso" property="companyName" indexed="true"/>
                    <html:hidden name="companyDenso" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp009Form" property="companySupplierList">
                <c:forEach var="companySupplier" items="${Wshp009Form.companySupplierList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplier" property="companyName" indexed="true"/>
                    <html:hidden name="companySupplier" property="vendorCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp009Form" property="asnStatusList">
                <c:forEach var="asnStatusItem" items="${Wshp009Form.asnStatusList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="asnStatusItem" property="miscCode" indexed="true"/>
                    <html:hidden name="asnStatusItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp009Form" property="invoiceStatusList">
                <c:forEach var="invoiceStatusItem" items="${Wshp009Form.invoiceStatusList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="invoiceStatusItem" property="miscCode" indexed="true"/>
                    <html:hidden name="invoiceStatusItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp009Form" property="supplierAuthenList">
                <c:forEach var="supplierAuthen" items="${Wshp009Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                    <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp009Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wshp009Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            
            <c:choose>
                <c:when test="${0 == Wshp009Form.max}">
                    <c:set var="resultStyle">
                        visibility: hidden;
                    </c:set>
                    <c:set var="legendStyle">
                        display:none;margin:0;padding-top:2px;
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="resultStyle">
                        visibility: visible;
                    </c:set>
                    <c:set var="legendStyle">
                        margin:0;padding-top:2px;
                    </c:set>
                </c:otherwise>
            </c:choose>
            
            <div id="result" style="${resultStyle}">
                <div style="width:994px;height:300px;position:absolute;overflow-y:scroll;overflow-x:scroll">
                    <table class="display_data" id="tblBody" style="table-layout:fixed;width:1341px;word-wrap:break-word;border-width:0px">
                        <col style="width:20px" />
                        <col style="width:75px" />
                        <col style="width:30px" />
                        <col style="width:18px" />
                        <col style="width:65px" />
                        <col style="width:20px" />
                        <col style="width:55px" />
                        <col style="width:60px" />
                        <col style="width:55px" />
                        <col style="width:60px" />
                        <col style="width:25px" />
                        <col style="width:27px" />
                        <col style="width:130px" />
                        <col style="width:50px" />
                        <col style="width:50px" />
                        <col style="width:30px" />
                        <col style="width:20px" />
                        <col style="width:90px" />
                        <tr>
                            <th scope="col">
                                <sps:label name="NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ASN_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="D_CD" />
                            </th>
                            <th scope="col" >
                                <sps:label name="D_P" />
                            </th>
                            <th scope="col" >
                                <sps:label name="D_PART_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="UM" />
                            </th>
                            <th scope="col" >
                                <sps:label name="SHIPPING_QTY" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ACTUAL_ETD" />
                            </th>
                            <th scope="col" >
                                <sps:label name="RECEIVE_QTY" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ACTUAL_ETA" />
                            </th>
                            <th scope="col" >
                                <sps:label name="PN_RCV_STA" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ASN_STATUS" />
                            </th>
                            <th scope="col" >
                                <sps:label name="INVOICE_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="INVOICE_DATE" />
                            </th>
                            <th scope="col" >
                                <sps:label name="INVOICE_STATUS" />
                            </th>
                            <th scope="col" >
                                <sps:label name="S_CD" />
                            </th>
                            <th scope="col" >
                                <sps:label name="S_P" />
                            </th>
                            <th scope="col" >
                                <sps:label name="S_PART_NO" />
                            </th>
                        </tr>
                        <c:set var="startRow">
                            ${(Wshp009Form.pageNo * Wshp009Form.count) - Wshp009Form.count}
                        </c:set>
                        <c:forEach var="asnProgressInformation" items="${Wshp009Form.asnProgressList}" begin="0" step="1" varStatus="status">
                            <tr class="${(status.index + 1) % 2 == 0 ? 'blueLight' : ''}">
                                <td class="align-right">
                                    ${startRow + (status.index + 1)}
                                </td>
                                <td class="align-left">
                                    <a href="javascript:void(0)" onclick="wshp009ViewAsnMaintenance(
                                        '${asnProgressInformation.asnNo}',
                                        '${asnProgressInformation.DCd}'); return false;">
                                        ${asnProgressInformation.asnNo}
                                    </a>
                                </td>
                                <td class="align-left">
                                    ${asnProgressInformation.DCd}
                                </td>
                                <td class="align-left">
                                    ${asnProgressInformation.DPcd}
                                </td>
                                <td class="align-left">
                                    ${asnProgressInformation.DPn}
                                </td>
                                <td class="align-left">
                                    ${asnProgressInformation.um}
                                </td>
                                <td class="align-right">
                                    ${asnProgressInformation.shippingQtyStr}
                                </td>
                                <td class="align-left">
                                    ${asnProgressInformation.actualEtdStr}
                                </td>
                                <td class="align-right">
                                    ${asnProgressInformation.receivedQtyStr}
                                </td>
                                <td class="align-left">
                                    ${asnProgressInformation.actualEtaStr}
                                </td>
                                <td class="align-left">
                                    ${asnProgressInformation.pnRcvStatus}
                                </td>
                                <td class="align-left">
                                    ${asnProgressInformation.asnStatus}
                                </td>
                                <td class="align-left">
                                    <a href="#" onclick="return wshp009ViewInvoiceDetail(
                                    '${asnProgressInformation.invoiceId}', '${asnProgressInformation.invoiceDate}',
                                    '${asnProgressInformation.cnDate}', '${asnProgressInformation.invRcvSta}',
                                    '${asnProgressInformation.invoiceNo}', '${asnProgressInformation.cnNo}',
                                    '${asnProgressInformation.DCd}', '${asnProgressInformation.DPcd}',
                                    '${asnProgressInformation.SCd}', '${asnProgressInformation.SPcd}');">
                                        <c:out value="${asnProgressInformation.invoiceNo}" />
                                    </a>
                                </td>
                                <td class="align-left">
                                    <%-- FIX : wrong dateformat --%>
                                    <%-- <fmt:formatDate pattern="yyyy/MM/dd" value="${asnProgressInformation.invoiceDate}"/> --%>
                                    ${asnProgressInformation.invoiceDateShow}
                                </td>
                                <td class="align-left">
                                    ${asnProgressInformation.invoiceStatus}
                                </td>
                                <td class="align-left">
                                    ${asnProgressInformation.vendorCd}
                                </td>
                                <td class="align-left">
                                    ${asnProgressInformation.SPcd}
                                </td>
                                <td class="align-left">
                                    ${asnProgressInformation.SPn}
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div style="width:98%;height:305px;">
                    &nbsp;
                </div>
                <table style="width: 994px;" border="0">
                        <tr>
                            <td align="left">
                                <ai:paging action="/SPS/AsnDetailProgressAction.do" formId="Wshp009FormResult" jump="true" />
                            </td>
                        </tr>
                </table>
                <div id="displayLegend" style="${legendStyle}">
                    <table bgcolor="#EEEEEE" style="width: 994px;border: 0;">
                        <tr>
                            <td align="left" style="font-size:10px;">
                                <span style="font-weight: bold;">
                                    <sps:label name="LEGEND"/>
                                    <span style="text-decoration: underline;">
                                        <sps:label name="ASN_STATUS"/>
                                    </span>
                                </span>
                                <sps:label name="ISS_CCL_RCP_RPT"/>
                            </td>
                            <td class="align-right">
                                <a href="#" style="font-size:10px;" id="moreLegend">
                                    &gt;&gt;&nbsp;
                                    <sps:label name="MORE"/>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </html:form>
    </body>
</html>
