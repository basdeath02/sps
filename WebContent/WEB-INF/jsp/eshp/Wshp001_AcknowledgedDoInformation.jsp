<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wshp001_AcknowledgedDoInformation.js" >
        </script>
        <title>
            <sps:label name="CREATE_ASN_BY_MANUAL" />
        </title>
        <script type="text/javascript">
            var msgConfirmClearChkbox = '<sps:message name="SP-W2-0003"/>';
            var msgWarningInputValue = '<sps:message name="SP-W2-0005"/>';
            var labelAll = '<sps:label name="ALL_CRITERIA" />';
            var labelPleaseSelect = '<sps:label name="PLEASE_SELECT_CRITERIA" />';
            var msgPrinted = '<sps:message name="SP-W2-0006"/>';
        </script>
    </head>
    <body class="hideHorizontal">
        <html:form styleId="Wshp001FormCriteria" action="/AcknowledgedDoInformationAction.do">
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden property="method" styleId="method"/>
            <html:hidden property="mode" styleId="mode"/>
            <html:hidden property="userLogin" styleId="userLogin"/>
            <html:hidden property="cannotResetMessage" styleId="cannotResetMessage"/>
            <!-- Key for paging. -->
            <html:hidden styleId="currentPage" property="currentPage"/>
            <html:hidden styleId="row"  property="row"/>
            <html:hidden styleId="totalCount" property="totalCount"/>
            
            <logic:notEmpty name="Wshp001Form" property="companySupplierList">
                <c:forEach var="companySupplierItem" items="${Wshp001Form.companySupplierList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplierItem" property="vendorCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="companyDensoList">
                <c:forEach var="companyDensoItem" items="${Wshp001Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoItem" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="shipmentStatusList">
                <c:forEach var="shipmentStatusItem" items="${Wshp001Form.shipmentStatusList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="shipmentStatusItem" property="miscCode" indexed="true"/>
                    <html:hidden name="shipmentStatusItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="transList">
                <c:forEach var="transItem" items="${Wshp001Form.transList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="transItem" property="miscCode" indexed="true"/>
                    <html:hidden name="transItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="orderMethodList">
                <c:forEach var="orderMethodItem" items="${Wshp001Form.orderMethodList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="orderMethodItem" property="miscCode" indexed="true"/>
                    <html:hidden name="orderMethodItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="receiveByScanList">
                <c:forEach var="receiveByScanItem" items="${Wshp001Form.receiveByScanList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="receiveByScanItem" property="miscCode" indexed="true"/>
                    <html:hidden name="receiveByScanItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="ltFlagList">
                <c:forEach var="ltFlagItem" items="${Wshp001Form.ltFlagList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="ltFlagItem" property="miscCode" indexed="true"/>
                    <html:hidden name="ltFlagItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="supplierAuthenList">
                <c:forEach var="supplierAuthen" items="${Wshp001Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                    <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wshp001Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            
            <table id="ui-displayHeader" class="bottom">
                <tr>
                    <td>
                        <sps:label name="SHIPPING" />
                        &gt;
                        <sps:label name="CREATE_ASN_BY_MANUAL" />
                    </td>
                    <td colspan="2" class="align-right" >
                        <sps:label name="USER" />&nbsp;:
                        <bean:write name="Wshp001Form" property="userLogin"/>
                    </td>
                </tr>
            </table>
            
            <c:choose>
                <c:when test="${not empty Wshp001Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width:992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg" items="${Wshp001Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message}
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            
            <fieldset class="fixwidth">
                <c:set var="lbPleaseSelect">
                    <sps:label name="PLEASE_SELECT_CRITERIA" />
                </c:set>
                <c:set var="lbAll">
                    <sps:label name="ALL_CRITERIA" />
                </c:set>
                <table class="display_criteria" id="ui-displayCriteria">
                    <col style="width:125px;"/>
                    <col style="width:245px;"/>
                    <col style="width:100px;"/>
                    <col style="width:155px;"/>
                    <col style="width:50px;"/>
                    <col style="width:125px;"/>
                    <col style="width:50px;"/>
                    <col style="width:125px;"/>
                    <tr>
                        <th>
                            <sps:label name="DELIVERY_DATE" />
                            &nbsp;:
                        </th>
                        <td>
                            <sps:calendar name="deliveryDateFrom" id="deliveryDateFrom" value="${Wshp001Form.deliveryDateFrom}"/>
                            -
                            <sps:calendar name="deliveryDateTo" id="deliveryDateTo" value="${Wshp001Form.deliveryDateTo}"/>
                        </td>
                        <th>
                            <sps:label name="DELIVERY_TIME" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp001Form" property="deliveryTimeFrom"
                                onfocus="removeColonFromTime('deliveryTimeFrom');return false;"
                                onblur="addColonToTime('deliveryTimeFrom');return false;"
                                styleId="deliveryTimeFrom" style="width:50px;"/>
                            -
                            <html:text name="Wshp001Form" property="deliveryTimeTo"
                                onfocus="removeColonFromTime('deliveryTimeTo');return false;"
                                onblur="addColonToTime('deliveryTimeTo');return false;"
                                styleId="deliveryTimeTo" style="width:50px;"/>
                        </td>
                        <th>
                            <sps:label name="S_CD" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp001Form" property="vendorCd" styleId="SCd" styleClass="mandatory" style="width:112px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp001Form" property="companySupplierList">
                                    <c:forEach var="companySupplierItem" items="${Wshp001Form.companySupplierList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${companySupplierItem.vendorCd}">
                                            ${companySupplierItem.vendorCd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="S_P" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp001Form" property="SPcd" styleId="SPcd" styleClass="mandatory" style="width:122px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp001Form" property="plantSupplierList">
                                    <c:forEach var="plantSupplierItem" items="${Wshp001Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${plantSupplierItem.SPcd}">
                                             ${plantSupplierItem.SPcd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                    </tr>
                </table>
                <table class="display_criteria" id="ui-displayCriteria">
                    <col style="width:50px;"/>
                    <col style="width:120px;"/>
                    <col style="width:55px;"/>
                    <col style="width:120px;"/>
                    <col style="width:105px;"/>
                    <col style="width:125px;"/>
                    <col style="width:50px;"/>
                    <col style="width:110px;"/>
                    <col style="width:55px;"/>
                    <col style="width:55px;"/>
                    <col style="width:45px;"/>
                    <col style="width:50px;"/>
                    <tr>
                       <th>
                            <sps:label name="D_CD" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp001Form" property="DCd" styleId="DCd" styleClass="mandatory" style="width:108px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp001Form" property="companyDensoList">
                                    <c:forEach var="companyDensoItem" items="${Wshp001Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${companyDensoItem.DCd}">
                                             ${companyDensoItem.DCd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="D_P" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp001Form" property="DPcd" styleId="DPcd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp001Form" property="plantDensoList">
                                    <c:forEach var="plantDensoItem" items="${Wshp001Form.plantDensoList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${plantDensoItem.DPcd}">
                                             ${plantDensoItem.DPcd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="SHIPMENT_STATUS" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp001Form" property="shipmentStatus" styleId="shipmentStatus" styleClass="mandatory" style="width:120px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp001Form" property="shipmentStatusList">
                                    <c:forEach var="shipmentStatusItem" items="${Wshp001Form.shipmentStatusList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${shipmentStatusItem.miscCode}">
                                            ${shipmentStatusItem.miscValue}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="TM" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp001Form" property="trans" styleId="trans" style="width:100px">
                                <logic:notEmpty name="Wshp001Form" property="transList">
                                    <html:option value="">
                                        ${lbAll}
                                    </html:option>
                                    <c:forEach var="transItem" items="${Wshp001Form.transList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${transItem.miscCode}">
                                            ${transItem.miscValue}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="ROUTE" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp001Form" property="routeNo" style="width:50px" />
                        </td>
                        <th>
                            <sps:label name="DEL" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp001Form" property="del" style="width:50px" />
                        </td>
                    </tr>
                </table>
                <table class="display_criteria" id="ui-displayCriteria">
                    <col style="width:125px;"/>
                    <col style="width:250px;"/>
                    <col style="width:125px;"/>
                    <col style="width:250px;"/>
                    <col style="width:100px;"/>
                    <col style="width:125px;"/>
                    <tr>
                        <th>
                            <sps:label name="SHIP_DATE" />
                            &nbsp;:
                        </th>
                        <td>
                            <sps:calendar name="shipDateFrom" id="shipDateFrom"  value="${Wshp001Form.shipDateFrom}"/>
                            -
                            <sps:calendar name="shipDateTo" id="shipDateTo" value="${Wshp001Form.shipDateTo}"/>
                        </td>
                        <th>
                            <sps:label name="ISSUE_DATE" />
                            &nbsp;:
                        </th>
                        <td>
                            <sps:calendar name="issueDateFrom" id="issueDateFrom"  value="${Wshp001Form.issueDateFrom}"/>
                            -
                            <sps:calendar name="issueDateTo" id="issueDateTo" value="${Wshp001Form.issueDateTo}"/>
                        </td>
                        <th>
                            <sps:label name="ORDER_METHOD" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp001Form" property="orderMethod" styleId="orderMethod" style="width:140px">
                                <logic:notEmpty name="Wshp001Form" property="orderMethodList">
                                    <html:option value="">
                                        ${lbAll}
                                    </html:option>
                                    <c:forEach var="orderMethodList" items="${Wshp001Form.orderMethodList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${orderMethodList.miscCode}">
                                            ${orderMethodList.miscValue}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                    </tr>
                </table>
                <table class="display_criteria" id="ui-displayCriteria">
                    <col style="width:100px;"/>
                    <col style="width:125px;"/>
                    <col style="width:100px;"/>
                    <col style="width:125px;"/>
                    <col style="width:100px;"/>
                    <col style="width:125px;"/>
                    <col style="width:100px;"/>
                    <col style="width:125px;"/>
                    <tr>
                        <th>
                            <sps:label name="SPS_DO_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp001Form" property="spsDoNo" style="width:108px" />
                        </td>
                        <th>
                            <sps:label name="CIGMA_DO#" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp001Form" property="cigmaDoNo" style="width:100px" />
                        </td>
                        <th>
                            <sps:label name="LT_FLAG" />
                            &nbsp;
                        </th>
                        <td>
                            <html:select name="Wshp001Form" property="ltFlag" styleId="ltFlag" style="width:140px" styleClass="mandatory">
                                <logic:notEmpty name="Wshp001Form" property="ltFlagList">
                                    <c:forEach var="ltFlagList" items="${Wshp001Form.ltFlagList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${ltFlagList.miscCode}">
                                            ${ltFlagList.miscValue}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="SCAN_FLAG" />
                            &nbsp;
                        </th>
                        <td>
                            <html:select name="Wshp001Form" property="receiveByScan" styleId="receiveByScan" style="width:140px" styleClass="mandatory">
                                <logic:notEmpty name="Wshp001Form" property="receiveByScanList">
                                    <html:option value="ALL">
                                        ${lbAll}
                                    </html:option>
                                    <c:forEach var="receiveByScanList" items="${Wshp001Form.receiveByScanList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${receiveByScanList.miscCode}">
                                            ${receiveByScanList.miscValue}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <table style="width:994px">
                <c:set var="labelSearch">
                    <sps:label name="SEARCH"/>
                </c:set>
                <c:set var="labelReset">
                    <sps:label name="RESET"/>
                </c:set>
                <tr>
                    <td class="align-left" style="vertical-align:bottom;font-size:10px;">
                        <c:if test="${not empty Wshp001Form.acknowledgedDoInformationList}">
                            <sps:label name="SYSTEM_GET_SELECTED_REC_EVERY_PAGE"/>
                        </c:if>
                    </td>
                    <td class="align-right">
                        <input type="button" id="btnSearch" value="${labelSearch}" class="select_button"/>
                        &nbsp;
                        <input type="button" id="btnReset" value="${labelReset}" class="select_button"/>
                    </td>
                </tr>
            </table>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        
        <html:form styleId="Wshp001FormResult" action="/AcknowledgedDoInformationAction.do">
            <html:hidden property="userLogin"/>
            <html:hidden property="mode" styleId="modeResult"/>
            <html:hidden property="method" styleId="methodResult" value="Search"/>
            <html:hidden property="countGroupAsn" styleId="countGroupAsn"/>
            <!-- Parameter for search criteria.-->
            <html:hidden property="deliveryDateFrom"/>
            <html:hidden property="deliveryDateTo"/>
            <html:hidden property="deliveryTimeFrom"/>
            <html:hidden property="deliveryTimeTo"/>
            <html:hidden property="vendorCd"/>
            <html:hidden property="SPcd"/>
            <html:hidden property="DCd"/>
            <html:hidden property="DPcd"/>
            <html:hidden property="shipmentStatus"/>
            <html:hidden property="trans"/>
            <html:hidden property="routeNo"/>
            <html:hidden property="del"/>
            <html:hidden property="shipDateFrom"/>
            <html:hidden property="shipDateTo"/>
            <html:hidden property="issueDateFrom"/>
            <html:hidden property="issueDateTo"/>
            <html:hidden property="orderMethod"/>
            <html:hidden property="spsDoNo"/>
            <html:hidden property="cigmaDoNo"/>
            <html:hidden property="receiveByScan"/>
            <html:hidden property="ltFlag"/>
            <html:hidden property="cannotResetMessage"/>
            
            <!-- Parameter for selected row and link.-->
            <html:hidden property="selectedDoId" styleId="selectedDoId"/>
            <html:hidden property="selectedDCd" styleId="selectedDCd"/>
            <html:hidden property="selectedDel" styleId="selectedDel"/>
            <html:hidden property="selectedSpsDoNo" styleId="selectedSpsDoNo"/>
            <html:hidden property="selectedRev" styleId="selectedRev"/>
            <html:hidden property="selectedRoute" styleId="selectedRoute"/>
            <html:hidden property="selectedDel" styleId="selectedDel"/>
            <html:hidden property="pdfFileId" styleId="pdfFileId"/>
            <html:hidden property="doId" styleId="doId"/>
            <html:hidden property="pdfFileKbTagPrintDate" styleId="pdfFileKbTagPrintDate"/>
            <html:hidden property="pdfType" styleId="pdfType"/>
            
            <logic:notEmpty name="Wshp001Form" property="companySupplierList">
                <c:forEach var="companySupplierItem" items="${Wshp001Form.companySupplierList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplierItem" property="vendorCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="companyDensoList">
                <c:forEach var="companyDensoItem" items="${Wshp001Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoItem" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="shipmentStatusList">
                <c:forEach var="shipmentStatusItem" items="${Wshp001Form.shipmentStatusList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="shipmentStatusItem" property="miscCode" indexed="true"/>
                    <html:hidden name="shipmentStatusItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="transList">
                <c:forEach var="transItem" items="${Wshp001Form.transList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="transItem" property="miscCode" indexed="true"/>
                    <html:hidden name="transItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="orderMethodList">
                <c:forEach var="orderMethodItem" items="${Wshp001Form.orderMethodList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="orderMethodItem" property="miscCode" indexed="true"/>
                    <html:hidden name="orderMethodItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="receiveByScanList">
                <c:forEach var="receiveByScanItem" items="${Wshp001Form.receiveByScanList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="receiveByScanItem" property="miscCode" indexed="true"/>
                    <html:hidden name="receiveByScanItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="ltFlagList">
                <c:forEach var="ltFlagItem" items="${Wshp001Form.ltFlagList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="ltFlagItem" property="miscCode" indexed="true"/>
                    <html:hidden name="ltFlagItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
             <logic:notEmpty name="Wshp001Form" property="supplierAuthenList">
                <c:forEach var="supplierAuthen" items="${Wshp001Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                    <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp001Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wshp001Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            
            <c:choose>
                <c:when test="${0 == Wshp001Form.max}">
                    <c:set var="visibilityFlag">
                        hidden
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="visibilityFlag">
                        visible
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="result" style="visibility: ${visibilityFlag};">
                <div style="width:994px;">
                    
                </div>
                <div style="width:1060px;">
                    <table class="display_data" id="tblBody" style="table-layout: fixed;word-wrap:break-word;">
                        <tr>
                            <th style="width:20px;padding:0px;" class="checkbox" >
                                <sps:label name="ALL" />
                                <br />
                                <input type="checkbox" name="checkboxAll" value="checkbox" onclick="wshp001AllChecked(this);"/>
                            </th>
                            <th style="width:20px;padding:0px;">
                                <sps:label name="NO" />
                            </th>
                            <th style="width:95px;padding:0px;">
                                <sps:label name="DELIVERY_DATE" />
                            </th>
                            <th style="width:95px;padding:0px;">
                                <sps:label name="SHIP_DATE" />
                            </th>
                            <th style="width:70px;padding:0px;">
                                <sps:label name="ISSUE_DATE" />
                            </th>
                            <th style="width:60px;padding:0px;">
                                <sps:label name="ORDER_METHOD" />
                            </th>
                            <th style="width:45px;padding:0px;">
                                <sps:label name="S_CD" />
                            </th>
                            <th style="width:45px;padding:0px;">
                                <sps:label name="S_P" />
                            </th>
                            <th style="width:45px;padding:0px;">
                                <sps:label name="D_CD" />
                            </th>
                            <th style="width:45px;padding:0px;">
                                <sps:label name="D_P" />
                            </th>
                            <th style="width:90px;padding:0px;">
                                <sps:label name="D/O_NO" />
                            </th>
                            <th style="width:25px;padding:0px;">
                                <sps:label name="LAT"/>
                                <br />
                                <sps:label name="EST"/>
                                <br />
                                <sps:label name="REV"/>.
                            </th>
                            <th style="width:90px;padding:0px;">
                                <sps:label name="CIGMA_DO#" />
                            </th>
                            <th style="width:40px;padding:0px;">
                                <sps:label name="ROUTE" />
                            </th>
                            <th style="width:40px;padding:0px;">
                                <sps:label name="DEL" />
                            </th>
                            <th style="width:40px;padding:0px;">
                                <sps:label name="TM" />
                            </th>
                            <th style="width:35px;padding:0px;">
                                <sps:label name="SCAN_FLAG" />
                            </th>
                            <th style="width:35px;padding:0px;">
                                <sps:label name="SHIP" />
                                <br/>
                                <sps:label name="STAT" />
                                <br/>
                                -
                                <sps:label name="TUS" />
                            </th>
                            <%-- [IN004] : Remove this column --%>
                            <%-- <th style="width:36px;padding:0px;">
                                <sps:label name="SHIP" />
                                <br/>
                                <sps:label name="NO#" />
                                <br/>
                                -
                                <sps:label name="TICE" />
                            </th>--%>
                            
                            <th style="width:30px;padding:0px;">
                                <sps:label name="DO_PDF" />
                            </th>
                            <th style="width:30px;padding:0px;">
                                <sps:label name="TAG_PDF" />
                            </th>
                        </tr>
                    </table>
                </div>
                <div style="width:1060px;height:244px;overflow-y:scroll">
                    <table class="display_data" id="tblBody"
                        style="table-layout: fixed;word-wrap: break-word;border-width: 0px">
                        <c:set var="startRow">
                            ${(Wshp001Form.pageNo * Wshp001Form.count) - Wshp001Form.count}
                        </c:set>
                        <c:forEach var="acknowledgedDoInformationReturn" items="${Wshp001Form.acknowledgedDoInformationList}" step="1" varStatus="status">
                            <tr class="${(status.index + 1) % 2 == 0 ? 'blueLight' : ''}">
                                <td align="center" style="width:20px;padding:0px;">
                                    <input type="checkbox" name="doChkbox" id="chkAcknowledgedDo${status.index}"
                                        onclick="return wshp001SetAcknowlegdedDo('${status.index}')"
                                        <c:if test="${not empty acknowledgedDoInformationReturn.selectedDoId}">
                                            checked
                                        </c:if>
                                    />
                                    <html:hidden name="acknowledgedDoInformationReturn"  property="selectedDoId"
                                        indexed="true" styleId="selectedDoId${status.index}"
                                        value="${acknowledgedDoInformationReturn.selectedDoId}" />
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.doId"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.spsDoNo"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.shipmentStatus"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="SCd"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="SPcd"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="DCd"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="DPcd"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.vendorCd"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.tm"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.orderMethod"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryDateStr"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="shipmentDateStr"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.doIssueDate"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="issuedDateStr"
                                        indexed="true" />
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="miscDomain.miscValue"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.spsDoNo"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.cigmaDoNo"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.revision"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.truckRoute"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.truckSeq"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="trans"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.shipmentStatus"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="shipNotice"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.pdfFileId"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.lastUpdateDatetime"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.whPrimeReceiving"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="receiveByScan"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="isPrintOneWayKanbanTag"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.doId"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.receiveByScan"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.isPrintOneWayKanbanTag"
                                        indexed="true"/>
                                    <html:hidden name="acknowledgedDoInformationReturn"
                                        property="deliveryOrderDomain.oneWayKanbanTagFlag"
                                        indexed="true"/>
                                </td>
                                <td class="align-right" style="width:20px;padding:0px;">
                                    ${startRow + (status.index + 1)}
                                </td>
                                <td class="align-left" style="width:95px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.deliveryDateStr}
                                </td>
                                <td class="align-left" style="width:95px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.shipmentDateStr}
                                </td>
                                <td class="align-left" style="width:70px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.issuedDateStr}
                                </td>
                                <td class="align-left" style="width:60px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.miscDomain.miscValue}
                                </td>
                                <td class="align-left" style="width:45px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.deliveryOrderDomain.vendorCd}
                                </td>
                                <td class="align-left" style="width:45px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.SPcd}
                                </td>
                                <td class="align-left" style="width:45px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.DCd}
                                </td>
                                <td class="align-left" style="width:45px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.DPcd}
                                </td>
                                <td class="align-left" style="width:90px;padding:0px;">
                                    <%-- [IN004] : Create link to WSHP003 --%>
                                    <%-- ${acknowledgedDoInformationReturn.deliveryOrderDomain.spsDoNo} --%>
                                    <a href="javascript:void(0)" onclick="wshp001ViewShipNotice('${acknowledgedDoInformationReturn.deliveryOrderDomain.doId}',
                                            '${acknowledgedDoInformationReturn.deliveryOrderDomain.spsDoNo}',
                                            '${acknowledgedDoInformationReturn.deliveryOrderDomain.revision}',
                                            '${acknowledgedDoInformationReturn.deliveryOrderDomain.truckRoute}',
                                            '${acknowledgedDoInformationReturn.deliveryOrderDomain.truckSeq}',
                                            '${acknowledgedDoInformationReturn.DCd}'); return false;">
                                            ${acknowledgedDoInformationReturn.deliveryOrderDomain.spsDoNo}
                                    </a>
                                </td>
                                <td class="align-left" style="width:25px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.deliveryOrderDomain.revision}
                                </td>
                                <td class="align-left" style="width:90px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.deliveryOrderDomain.cigmaDoNo}
                                </td>
                                <td class="align-left" style="width:40px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.deliveryOrderDomain.truckRoute}
                                </td>
                                <td class="align-left" style="width:40px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.deliveryOrderDomain.truckSeq}
                                </td>
                                <td class="align-left" style="width:40px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.trans}
                                </td>
                                <td class="align-left" style="width:40px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.receiveByScan}
                                </td>
                                <td class="align-left" style="width:30px;padding:0px;">
                                    ${acknowledgedDoInformationReturn.deliveryOrderDomain.shipmentStatus}
                                </td>
                                
                                <%-- [IN004] : Remove this column --%>
                                <%-- <td class="align-left" style="width:36px;padding:0px;">
                                    <c:if test="${acknowledgedDoInformationReturn.shipNotice == '1'}">
                                        <a href="javascript:void(0)" onclick="wshp001ViewShipNotice('${acknowledgedDoInformationReturn.deliveryOrderDomain.doId}',
                                            '${acknowledgedDoInformationReturn.deliveryOrderDomain.spsDoNo}',
                                            '${acknowledgedDoInformationReturn.deliveryOrderDomain.revision}',
                                            '${acknowledgedDoInformationReturn.deliveryOrderDomain.truckRoute}',
                                            '${acknowledgedDoInformationReturn.deliveryOrderDomain.truckSeq}',
                                            '${acknowledgedDoInformationReturn.DCd}'); return false;">
                                            Link
                                        </a>
                                    </c:if>
                                </td>--%>
                                
                                <c:set var="lblDeliveryOrderReport">
                                    <sps:label name="DELIVERY_ORDER_REPORT"/>
                                </c:set>
                                <td class="align-center" style="width:30px;padding:0px;">
                                    <img src="css/images/pdf.jpg" style="width:19px;height:20px;cursor:pointer;" alt="${lblDeliveryOrderReport}"
                                        onclick="wshp001ViewPdf('${acknowledgedDoInformationReturn.deliveryOrderDomain.doId}');return false;"/>
                                </td>
                                <td class="align-center" style="width:30px;padding:0px;">
                                    <c:choose>
		                                <c:when test="${acknowledgedDoInformationReturn.deliveryOrderDomain.oneWayKanbanTagFlag == '1'}">
		                                    <c:choose>
		                                        <c:when test="${acknowledgedDoInformationReturn.partTag > '0' && acknowledgedDoInformationReturn.tagOutput != '4'}">
		                                            <a href="#">
                                                       <img src="css/images/pdf.jpg" style="width: 19" height="20" alt="${lbDownload }"
                                                           onclick="wshp001ViewKanbanPdf('${acknowledgedDoInformationReturn.deliveryOrderDomain.doId}','KB','${acknowledgedDoInformationReturn.deliveryOrderDomain.pdfFileKbTagPrintDate}','${status.index}'); return false;" />
                                                   </a>
                                                   <html:hidden property="pdfFileKbTagPrintDateFlag" value="${acknowledgedDoInformationReturn.deliveryOrderDomain.pdfFileKbTagPrintDate == null ? 'N' : 'Y'}"
                                                      styleId="pdfFileKbTagPrintDateFlag${acknowledgedDoInformationReturn.deliveryOrderDomain.doId}" />
		                                        </c:when>
		                                        <c:otherwise>
		                                            -
		                                        </c:otherwise>
		                                    </c:choose>
		                                </c:when>
		                                <c:otherwise>
		                                    <c:choose>
		                                        <c:when test="${acknowledgedDoInformationReturn.tagOutput == '4'}">
		                                            -
		                                        </c:when>
		                                        <c:otherwise>
		                                            &nbsp;
		                                        </c:otherwise>
		                                    </c:choose>
		                                </c:otherwise>
		                            </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <table style="width:994px;">
                    <c:set var="labelGroupAsn">
                        <sps:label name="GROUPASN" />
                    </c:set>
                    <tr>
                        <td align="left">
                            <ai:paging action="/SPS/AcknowledgedDoInformationAction.do" formId="Wshp001FormResult" jump="true" />
                        </td>
                        <td class="align-right">
                            <input type="button" id="btnGroupAsn" value="${labelGroupAsn}" class="select_button" style="width:120px;"/>
                        </td>
                    </tr>
                </table>
                <div id="displayLegend" style="margin:0;padding-top:2px;">
                    <table bgcolor="#EEEEEE" style="width:994px;border:0;">
                        <tr>
                            <td align="left" style="font-size:10px;">
                                <span style="font-weight: bold;">
                                    <sps:label name="LEGEND"/>
                                    <span style="text-decoration: underline;">
                                        <sps:label name="SHIP_STATUS"/>
                                    </span>
                                </span>
                                <sps:label name="ISS_PTS_CPS"/>
                            </td>
                            <td class="align-right">
                                <a href="javascript:void(0)" style="font-size:10px;" id="moreLegend">
                                    &gt;&gt;&nbsp;
                                    <sps:label name="MORE"/>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </html:form>
    </body>
</html>
