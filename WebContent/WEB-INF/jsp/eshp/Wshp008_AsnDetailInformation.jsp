<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wshp008_AsnDetailInformation.js">
        </script>
        <title>
            <sps:label name="ASN_DETAIL_INFORMATION"/>
        </title>
        <script type="text/javascript">
            var labelAll = '<sps:label name="ALL_CRITERIA" />';
            var labelPleaseSelect = '<sps:label name="PLEASE_SELECT_CRITERIA" />';
        </script>
    </head>
    <body class="hideHorizontal" style="width:99%">
        <html:form styleId="Wshp008FormCriteria" action="/AsnDetailInformationAction.do" >
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden property="userLogin" styleId="userLogin"/>
            <html:hidden property="method" styleId="method"/>
            <html:hidden property="mode" styleId="mode"/>
            <html:hidden property="cannotResetMessage" styleId="cannotResetMessage"/>
            <html:hidden property="cannotDownloadMessage" styleId="cannotDownloadMessage"/>
            <!-- Key for paging. -->
            <html:hidden styleId="currentPage" property="currentPage"/>
            <html:hidden styleId="row" property="row"/>
            <html:hidden styleId="totalCount" property="totalCount"/>
            
            <logic:notEmpty name="Wshp008Form" property="companySupplierList">
                <c:forEach var="companySupplierItem" items="${Wshp008Form.companySupplierList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplierItem" property="vendorCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="companyDensoList">
                <c:forEach var="companyDensoItem" items="${Wshp008Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoItem" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="asnStatusList">
                <c:forEach var="asnStatusItem" items="${Wshp008Form.asnStatusList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="asnStatusItem" property="miscCode" indexed="true"/>
                    <html:hidden name="asnStatusItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="transList">
                <c:forEach var="transItem" items="${Wshp008Form.transList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="transItem" property="miscCode" indexed="true"/>
                    <html:hidden name="transItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="supplierAuthenList">
                <c:forEach var="supplierAuthen" items="${Wshp008Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                    <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wshp008Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            
            <table id="ui-displayHeader" class="bottom">
                <tr>
                    <td>
                        <sps:label name="SHIPPING" />
                        &gt;
                        <sps:label name="ASN_DETAIL_INFORMATION" />
                    </td>
                    <td colspan="2" class="align-right" >
                        <sps:label name="USER" />&nbsp;:
                        <bean:write name="Wshp008Form" property="userLogin"/>
                    </td>
                </tr>
            </table>
            
            <c:choose>
                <c:when test="${not empty Wshp008Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width:992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg" items="${Wshp008Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message}
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            
            <fieldset class="fixwidth">
                <c:set var="lbPleaseSelect">
                    <sps:label name="PLEASE_SELECT_CRITERIA" />
                </c:set>
                <c:set var="lbAll">
                    <sps:label name="ALL_CRITERIA" />
                </c:set>
                <table class="display_criteria" id="ui-displayCriteria" style="padding-left:1px;border-spacing:0;">
                    <col style="width:90px;"/>
                    <col style="width:234px;"/>
                    <col style="width:45px;"/>
                    <col style="width:110px;"/>
                    <col style="width:53px;"/>
                    <col style="width:110px;"/>
                    <col style="width:43px;"/>
                    <col style="width:110px;"/>
                    <col style="width:53px;"/>
                    <col style="width:110px;"/>
                    <tr>
                        <th>
                            <sps:label name="PLAN_ETA" />
                            &nbsp;:
                        </th>
                        <td>
                            <sps:calendar name="planEtaFrom" id="planEtaFrom" value="${Wshp008Form.planEtaFrom}"/>
                            -
                            <sps:calendar name="planEtaTo" id="planEtaTo" value="${Wshp008Form.planEtaTo}"/>
                        </td>
                        <th>
                            <sps:label name="S_CD" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp008Form" property="vendorCd" styleId="vendorCd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp008Form" property="companySupplierList">
                                    <c:forEach var="companySupplierItem" items="${Wshp008Form.companySupplierList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${companySupplierItem.vendorCd}">
                                             ${companySupplierItem.vendorCd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="S_P" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp008Form" property="SPcd" styleId="SPcd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp008Form" property="plantSupplierList">
                                    <c:forEach var="supplierPlantItem" items="${Wshp008Form.plantSupplierList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${supplierPlantItem.SPcd}">
                                             ${supplierPlantItem.SPcd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="D_CD" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp008Form" property="DCd" styleId="DCd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp008Form" property="companyDensoList">
                                    <c:forEach var="companyDensoItem" items="${Wshp008Form.companyDensoList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${companyDensoItem.DCd}">
                                             ${companyDensoItem.DCd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="D_P" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp008Form" property="DPcd" styleId="DPcd" styleClass="mandatory" style="width:110px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <logic:notEmpty name="Wshp008Form" property="plantDensoList">
                                    <c:forEach var="plantDensoItem" items="${Wshp008Form.plantDensoList}" begin="0" step="1" varStatus="status">
                                        <html:option value="${plantDensoItem.DPcd}">
                                             ${plantDensoItem.DPcd}
                                        </html:option>
                                    </c:forEach>
                                </logic:notEmpty>
                            </html:select>
                        </td>
                    </tr>
                </table>
                <table class="display_criteria" id="ui-displayCriteria">
                    <col style="width:95px"/>
                    <col style="width:160px"/>
                    <col style="width:100px"/>
                    <col style="width:100px"/>
                    <col style="width:100px"/>
                    <col style="width:175px"/>
                    <col style="width:100px"/>
                    <col style="width:120px"/>
                    <tr>
                        <th>
                            <sps:label name="ASN_STATUS" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp008Form" property="asnStatus" styleId="asnStatus" style="width:160px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <c:forEach var="asnStatusItem" items="${Wshp008Form.asnStatusList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${asnStatusItem.miscCode}">
                                         ${asnStatusItem.miscCode} : ${asnStatusItem.miscValue}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="TM" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:select name="Wshp008Form" property="trans" styleId="trans" style="width:100px">
                                <html:option value="ALL">
                                    ${lbAll}
                                </html:option>
                                <c:forEach var="transItem" items="${Wshp008Form.transList}" begin="0" step="1" varStatus="status">
                                    <html:option value="${transItem.miscCode}">
                                        ${transItem.miscCode} : ${transItem.miscValue}
                                    </html:option>
                                </c:forEach>
                            </html:select>
                        </td>
                        <th>
                            <sps:label name="ASN_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp008Form" property="asnNo" styleId="asnNo" style="width:170px" />
                        </td>
                        <th>
                            <sps:label name="D/O_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp008Form" property="spsDoNo" styleId="spsDoNo" style="width:124px" />
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <sps:label name="ACTUAL_ETD" />
                            &nbsp;:
                        </th>
                        <td colspan="2">
                            <sps:calendar name="actualEtdFrom" id="actualEtdFrom"  value="${Wshp008Form.actualEtdFrom}"/>
                            -
                            <sps:calendar name="actualEtdTo" id="actualEtdTo" value="${Wshp008Form.actualEtdTo}"/>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <table style="width:994px">
                <c:set var="labelSearch">
                    <sps:label name="SEARCH"/>
                </c:set>
                <c:set var="labelReset">
                    <sps:label name="RESET"/>
                </c:set>
                <c:set var="lblDownload">
                    <sps:label name="DOWNLOAD"/>
                </c:set>
                <tr>
                    <td colspan="6" class="align-right">
                        <input type="button" id="btnSearch" value="${labelSearch}" class="select_button"/>
                        &nbsp;
                        <input type="button" id="btnReset" value="${labelReset}" class="select_button"/>
                        &nbsp;
                        <input type="button" id="btnDownload" value="${lblDownload}" class="select_button"/>
                    </td>
                </tr>
            </table>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        
        <html:form styleId="Wshp008FormResult" action="/AsnDetailInformationAction.do" >
            <html:hidden property="method" styleId="methodResult" value="Search"/>
            <html:hidden property="userLogin" styleId="userLogin"/>
            <html:hidden property="mode" styleId="modeResult"/>
            <html:hidden property="planEtaFrom" styleId="planEtaFromResult"/>
            <html:hidden property="planEtaTo" styleId="planEtaToResult"/>
            <html:hidden property="vendorCd" styleId="vendorCdResult"/>
            <html:hidden property="SPcd" styleId="SPcdResult"/>
            <html:hidden property="DCd" styleId="DCdResult"/>
            <html:hidden property="DPcd" styleId="DPcdResult"/>
            <html:hidden property="asnStatus" styleId="asnStatusResult"/>
            <html:hidden property="trans" styleId="transResult"/>
            <html:hidden property="spsDoNo" styleId="spsDoNoResult"/>
            <html:hidden property="asnNo" styleId="asnNoResult"/>
            <html:hidden property="actualEtdFrom" styleId="actualEtdFromResult"/>
            <html:hidden property="actualEtdTo" styleId="actualEtdToResult"/>
            
            <html:hidden property="selectedAsnNo" styleId="selectedAsnNo"/>
            <html:hidden property="selectedDCd" styleId="selectedDCd"/>
            <html:hidden property="selectedSpsDoNo" styleId="selectedSpsDoNo"/>
            <html:hidden property="selectedDoId" styleId="selectedDoId"/>
            <html:hidden property="selectedRev" styleId="selectedRev"/>
            <html:hidden property="selectedRoute" styleId="selectedRoute"/>
            <html:hidden property="selectedDel" styleId="selectedDel"/>
            <html:hidden property="selectedDPcd" styleId="selectedDPcd"/>
            <html:hidden property="selectedVendorCd" styleId="selectedVendorCd"/>
            <html:hidden property="selectedSPcd" styleId="selectedSPcd"/>
            <html:hidden property="selectedActualEtd" styleId="selectedActualEtd"/>
            
            <html:hidden property="cannotResetMessage"/>
            <html:hidden property="cannotDownloadMessage"/>
            
            <logic:notEmpty name="Wshp008Form" property="companySupplierList">
                <c:forEach var="companySupplierItem" items="${Wshp008Form.companySupplierList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplierItem" property="vendorCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="companyDensoList">
                <c:forEach var="companyDensoItem" items="${Wshp008Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoItem" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="asnStatusList">
                <c:forEach var="asnStatusItem" items="${Wshp008Form.asnStatusList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="asnStatusItem" property="miscCode" indexed="true"/>
                    <html:hidden name="asnStatusItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="transList">
                <c:forEach var="transItem" items="${Wshp008Form.transList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="transItem" property="miscCode" indexed="true"/>
                    <html:hidden name="transItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="supplierAuthenList">
                <c:forEach var="supplierAuthen" items="${Wshp008Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                    <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wshp008Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            
            <c:choose>
                <c:when test="${0 == Wshp008Form.max}">
                    <div id="result" style="visibility:hidden;">
                </c:when>
                <c:otherwise>
                    <div id="result" style="visibility:visible;">
                </c:otherwise>
            </c:choose>
                <div style="width:994px;height:330px;position:absolute;overflow-x:auto;overflow-y:auto;">
                    <table class="display_data" id="tblBody" style="table-layout:fixed;width:1230px;word-wrap:break-word;">
                        <col style="width:27px;"/>
                        <col style="width:33px;"/>
                        <col style="width:128px;"/>
                        <col style="width:59px;"/>
                        <col style="width:25px;"/>
                        <col style="width:50px;"/>
                        <col style="width:25px;"/>
                        <col style="width:25px;"/>
                        <col style="width:28px;"/>
                        <col style="width:105px;"/>
                        <col style="width:90px;"/>
                        <col style="width:90px;"/>
                        <col style="width:28px;"/>
                        <col style="width:49px;"/>
                        <col style="width:28px;"/>
                        <col style="width:85px;"/>
                        <col style="width:85px;"/>
                        <col style="width:150px;"/>
                        <col style="width:60px;"/>
                        <col style="width:30px;"/>
                        <col style="width:30px;"/>
                        <tr>
                            <th scope="col">
                                <sps:label name="NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ASN" />
                                <br />
                                <sps:label name="STATUS" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ASN_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="S_CD" />
                            </th>
                            <th scope="col" >
                                <sps:label name="S_P" />
                            </th>
                            <th scope="col" >
                                <sps:label name="D_CD" />
                            </th>
                            <th scope="col" >
                                <sps:label name="D_P" />
                            </th>
                            <th scope="col" >
                                <sps:label name="TR" />
                                <br />
                                <sps:label name="AN" />
                                <br />
                                <sps:label name="S" />
                            </th>
                            <th scope="col" >
                                <sps:label name="TRIP" />
                                <br />
                                <sps:label name="NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="D_PART_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="SHIPPING_QTY" />
                            </th>
                            <th scope="col" >
                                <sps:label name="D/O_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="REV" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ROUTE" />
                                <br />
                                <sps:label name="NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="DEL" />
                                <br />
                                <sps:label name="NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="PLAN_ETA" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ACTUAL_ETD" />
                            </th>
                            <th scope="col" >
                                <sps:label name="S_PART_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="NO" />
                                <sps:label name="OF" />
                                <br />
                                <sps:label name="BOXES" />
                            </th>
                            <th scope="col" >
                                <sps:label name="UM" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ASN" />
                                <br />
                                <sps:label name="PDF" />
                            </th>
                        </tr>
                        <c:set var="startRow">
                            ${(Wshp008Form.pageNo * Wshp008Form.count) - Wshp008Form.count}
                        </c:set>
                        <c:forEach var="asnDetailInformation" items="${Wshp008Form.asnDetailInformationList}" step="1" varStatus="status">
                            <tr class="${(status.index + 1) % 2 == 0 ? 'blueLight' : ''}">
                                <td class="align-right">
                                    ${startRow + (status.index + 1)}
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.asnDomain.asnStatus}
                                </td>
                                <td class="align-left">
                                    <a href="javascript:void(0)" onclick="wshp008ViewAsnMaintenance(
                                        '${asnDetailInformation.asnDomain.asnNo}',
                                        '${asnDetailInformation.asnDomain.DCd}'); return false;">
                                        ${asnDetailInformation.asnDomain.asnNo}
                                    </a>
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.asnDomain.vendorCd}
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.asnDomain.SPcd}
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.asnDomain.DCd}
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.asnDomain.DPcd}
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.doDomain.tm}
                                </td>
                                <td class="align-left">
                                    <bean:write name="asnDetailInformation" property="asnDomain.tripNo"/>
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.asnDetailDomain.DPn}
                                </td>
                                <td class="align-right">
                                    ${asnDetailInformation.shippingQtyStr}
                                </td>
                                <td class="align-left">
                                    <a href="javascript:void(0)" onclick="wshp008ViewDoDetail('${asnDetailInformation.asnDetailDomain.doId}'
                                        , '${asnDetailInformation.asnDetailDomain.spsDoNo}', '${asnDetailInformation.doDomain.revision}'
                                        , '${asnDetailInformation.doDomain.truckRoute}', '${asnDetailInformation.doDomain.truckSeq}'
                                        , '${asnDetailInformation.asnDomain.DCd}'); return false;">
                                        ${asnDetailInformation.asnDetailDomain.spsDoNo}
                                    </a>
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.doDomain.revision}
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.doDomain.truckRoute}
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.doDomain.truckSeq}
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.planEtaStr}
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.actualEtdStr}
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.asnDetailDomain.SPn}
                                </td>
                                <td class="align-right">
                                    <fmt:formatNumber type="number" pattern="#,###" value="${asnDetailInformation.asnDetailDomain.shippingBoxQty}" />
                                    <html:hidden name="asnDetailInformation" property="asnDetailDomain.shippingBoxQty" indexed="true" />
                                </td>
                                <td class="align-left">
                                    ${asnDetailInformation.asnDetailDomain.unitOfMeasure}
                                </td>
                                <c:set var="lblAdvanceShippingNotice">
                                    <sps:label name="ADVANCE_SHIPPING_NOTICE"/>
                                </c:set>
                                <td class="align-center">
                                    <img src="css/images/pdf.jpg" style="width:19px;height:20px" alt="${lblAdvanceShippingNotice}"
                                        onclick="wshp008DownloadPdf(
                                        '${asnDetailInformation.asnDomain.asnNo}',
                                        '${asnDetailInformation.asnDomain.DCd}'); return false;" onmouseover="style.cursor='hand'"/>
                                
                                <html:hidden name="asnDetailInformation" property="asnDomain.asnNo" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="asnDomain.asnStatus" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="asnDomain.vendorCd" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="asnDomain.SPcd" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="asnDomain.DCd" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="asnDomain.DPcd" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="asnDomain.actualEtd" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="asnDomain.tripNo" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="asnDomain.pdfFileId" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="asnDetailDomain.DPn" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="asnDetailDomain.SPn" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="asnDetailDomain.doId" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="asnDetailDomain.spsDoNo" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="asnDetailDomain.unitOfMeasure" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="doDomain.tm" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="doDomain.revision" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="doDomain.truckRoute" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="doDomain.truckSeq" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="shippingQtyStr" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="planEtaStr" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="actualEtdStr" indexed="true"/>
                                <html:hidden name="asnDetailInformation" property="noOfBoxStr" indexed="true"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div style="width:98%;height:335px;">
                    &nbsp;
                </div>
                <table style="width: 994px;">
                    <tr>
                        <td align="left">
                            <ai:paging action="/SPS/AsnDetailInformationAction.do" formId="Wshp008FormResult" jump="true" />
                        </td>
                    </tr>
                </table>
                <div id="displayLegend" style="margin: 0;padding-top: 2px;">
                    <table bgcolor="#EEEEEE" style="width: 994px;border: 0;">
                        <tr>
                            <td align="left" style="font-size:10px;">
                                <span style="font-weight:bold;">
                                    <sps:label name="LEGEND"/>
                                    <span style="text-decoration:underline;">
                                        <sps:label name="ASN_STATUS"/>
                                    </span>
                                </span>
                                <sps:label name="STATUS_ISS"/>,&nbsp;
                                <sps:label name="STATUS_CCL"/>,&nbsp;
                                <sps:label name="STATUS_RCP"/>,&nbsp;
                                <sps:label name="STATUS_RPT"/>
                            </td>
                            <td class="align-right">
                                <a href="javascript:void(0)" style="font-size:10px;" id="moreLegend">
                                    &gt;&gt;&nbsp;
                                    <sps:label name="MORE"/>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </html:form>
        <html:form styleId="Wshp008FormDownload" action="/AsnDetailInformationAction.do" >
            <html:hidden property="userLogin" styleId="userLogin"/>
            <html:hidden property="method" styleId="methodDownload"/>
            <html:hidden property="pdfFileId" styleId="pdfFileId"/>
            <html:hidden property="selectedAsnNo" styleId="selectedAsnNoDownload"/>
            <html:hidden property="selectedDCd" styleId="selectedDCdDownload"/>
            <html:hidden property="planEtaFrom"/>
            <html:hidden property="planEtaTo"/>
            <html:hidden property="vendorCd"/>
            <html:hidden property="SPcd"/>
            <html:hidden property="DCd"/>
            <html:hidden property="DPcd"/>
            <html:hidden property="asnStatus"/>
            <html:hidden property="trans"/>
            <html:hidden property="spsDoNo"/>
            <html:hidden property="asnNo"/>
            <html:hidden property="actualEtdFrom"/>
            <html:hidden property="actualEtdTo"/>
            <html:hidden property="cannotResetMessage"/>
            <html:hidden property="cannotDownloadMessage"/>
            <!-- Key for paging. -->
            <html:hidden property="pageNo"/>
            
            <logic:notEmpty name="Wshp008Form" property="companySupplierList">
                <c:forEach var="companySupplierItem" items="${Wshp008Form.companySupplierList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companySupplierItem" property="vendorCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="companyDensoList">
                <c:forEach var="companyDensoItem" items="${Wshp008Form.companyDensoList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="companyDensoItem" property="DCd" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="asnStatusList">
                <c:forEach var="asnStatusItem" items="${Wshp008Form.asnStatusList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="asnStatusItem" property="miscCode" indexed="true"/>
                    <html:hidden name="asnStatusItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="transList">
                <c:forEach var="transItem" items="${Wshp008Form.transList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="transItem" property="miscCode" indexed="true"/>
                    <html:hidden name="transItem" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="supplierAuthenList">
                <c:forEach var="supplierAuthen" items="${Wshp008Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                    <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp008Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wshp008Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
        </html:form>
    </body>
</html>
