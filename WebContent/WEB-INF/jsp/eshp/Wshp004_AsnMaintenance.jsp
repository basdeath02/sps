<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants" />
        <script type="text/javascript" src="./js/business/Wshp004_AsnMaintenance.js">
        </script>
        <title>
            <sps:label name="ASN_MAINTENANCE"/>
        </title>
        <script type="text/javascript">
            var labelAll = '<sps:label name="ALL_CRITERIA" />';
            var labelCancelAsn = '<sps:label name="CANCEL_ASN" />';
            var msgConfirmReturn = '<sps:message name="SP-W6-0008"/>';
            var msgConfirmCancel = '<sps:message name="SP-W6-0001"/>';
            var msgConfirmLotSize = '<sps:message name="SP-E7-0039"/>';
        </script>
    </head>

    <body class="hideHorizontal" style="width:99%">
        <html:form styleId="Wshp004FormCriteria" action="/AsnMaintenanceAction.do" enctype="multipart/form-data">
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden property="userLogin" styleId="userLogin"/>
            <html:hidden property="method" styleId="method"/>
            <html:hidden property="mode" styleId="mode"/>
            <html:hidden property="screenId" styleId="screenId"/>
            <html:hidden property="prevScreenId" styleId="prevScreenId"/>
            <html:hidden property="temporaryMode" styleId="temporaryMode"/>
            <html:hidden property="receiveByScan" styleId="receiveByScan"/>
            <html:hidden property="dscId" styleId="dscId"/>
            <html:hidden property="utc" styleId="utc"/>
            <html:hidden property="successFlag" styleId="successFlag"/>
            <html:hidden property="operateReviseQtyMsg" styleId="operateReviseQtyMsg"/>
            <html:hidden property="allowReviseQtyFlag" styleId="allowReviseQtyFlag"/>
            <html:hidden property="skipValidateLotSizeFlag"/>
            <html:hidden property="unmatchedLotSizeFlag"/>
            <html:hidden property="continueOperation"/>
            <!-- For WSHP003 screen -->
            <html:hidden property="DCd" styleId="DCd"/>
            <html:hidden property="doId" styleId="doId"/>
            <html:hidden property="spsDoNo" styleId="spsDoNo"/>
            <html:hidden property="revision" styleId="revision"/>
            <html:hidden property="routeNo" styleId="routeNo"/>
            <html:hidden property="del" styleId="del"/>
            <html:hidden property="sessionId" styleId="sessionId"/>
            
            <table id="ui-displayHeader" class="bottom">
                <tr>
                    <td>
                        <sps:label name="SHIPPING" />
                        &gt;
                        <sps:label name="CREATE_ASN_BY_MANUAL" />
                        &gt;
                        <sps:label name="ASN_MAINTENANCE" />
                    </td>
                    <td colspan="2" class="align-right" >
                        <sps:label name="USER" />&nbsp;:
                        <bean:write name="Wshp004Form" property="userLogin"/>
                    </td>
                </tr>
            </table>
            
            <c:choose>
                <c:when test="${not empty Wshp004Form.applicationMessageList}">
                    <c:set var="displayErrorMessage">
                        
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="displayErrorMessage">
                        display: none;
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="divError" style="margin-bottom: 5px; ${displayErrorMessage}">
                <table style="width:992px" class="tableMessage">
                    <tr>
                        <td id="errorTotal">
                            <c:forEach var="appMsg" items="${Wshp004Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageSuccess
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageError
                                    </c:set>
                                </c:if>
                                <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                    <c:set var="messageClass">
                                        ui-messageWarning
                                    </c:set>
                                </c:if>
                                <span class="${messageClass}">
                                    ${appMsg.message}
                                </span>
                                <br>
                            </c:forEach>
                        </td>
                    </tr>
                </table>
            </div>
            
            <fieldset class="fixwidth">
                <table class="display_criteria" id="ui-displayCriteria" style="padding:0px;">
                    <col style="width:8%;"/>
                    <col style="width:10%;"/>
                    <col style="width:10%;"/>
                    <col style="width:8%;"/>
                    <col style="width:10%;"/>
                    <col style="width:8%;"/>
                    <col style="width:10%;"/>
                    <col style="width:10%;"/>
                    
                    <tr>
                        <th scope="col">
                            <sps:label name="ASN_NO" />&nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp004Form" property="asnNo" styleId="asnNo" style="width:130px"/>
                        </td>
                        <th scope="col" >
                            <sps:label name="ASN_STATUS" />&nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp004Form" property="asnRcvStatus" styleId="asnRcvStatus" style="width:100px"/>
                        </td>
                        <th scope="col" >
                            <sps:label name="INVOICE_STATUS" />&nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp004Form" property="invStatus" styleId="invStatus" style="width:100px"/>
                        </td>
                        <th scope="col" >
                            <sps:label name="LAST_MODIFIED" />&nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp004Form" property="lastModified" styleId="lastModified" style="width:140px"/>
                        </td>
                    </tr>
                </table>
                <table class="display_criteria" id="ui-displayCriteria" style="padding:0px;border-spacing:1px;">
                    <col style="width:138px;"/>
                    <col style="width:115px;"/>
                    <col style="width:58px;"/>
                    <col style="width:138px;"/>
                    <col style="width:115px;"/>
                    <col style="width:58px;"/>
                    <col style="width:90px;"/>
                    <col style="width:122px;"/>
                    <col style="width:77px;"/>
                    <col style="width:60px;"/>
                    <tr>
                        <th scope="col" >
                            <sps:label name="ACTUAL_ETD" /> ${Wshp004Form.utc}
                            &nbsp;:
                        </th>
                        <td>
                            <sps:calendar name="actualEtdDate" id="actualEtdDate" value="${Wshp004Form.actualEtdDate}"/>
                        </td>
                        <td>
                            <html:text name="Wshp004Form" property="actualEtdTime" style="width:50px"
                            onfocus="removeColonFromTime('actualEtdTime');return false;"
                            onblur="addColonToTime('actualEtdTime');return false;" styleId="actualEtdTime"/>
                        </td>
                        <th scope="col" >
                            <sps:label name="PLAN_ETA" /> ${Wshp004Form.utc}
                            &nbsp;:
                        </th>
                        <td>
                            <sps:calendar name="planEtaDate" id="planEtaDate" value="${Wshp004Form.planEtaDate}"/>
                        </td>
                        <td>
                            <html:text name="Wshp004Form" property="planEtaTime" style="width:50px"
                            onfocus="removeColonFromTime('planEtaTime');return false;"
                            onblur="addColonToTime('planEtaTime');return false;" styleId="planEtaTime"/>
                        </td>
                        <th scope="col" >
                            <sps:label name="NUMBER_PALLET" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp004Form" property="noOfPallet" style="width:110px" styleId="noOfPallet"/>
                        </td>
                        <th scope="col" >
                            <sps:label name="TRIP_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp004Form" property="tripNo" style="width:60px" styleId="tripNo"
                                onkeypress="return PositiveIntegerFilter(event);"/>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <table style="width:994px;">
                <c:set var="lblReviseShippingQty">
                    <sps:label name="REVISE_SHIPPING_QTY"/>
                </c:set>
                <c:set var="lblAllowReviseQty">
                    <sps:label name="ALLOW_REVISE"/>
                </c:set>
                <tr>
                    <td class="align-right">
                        <input type="button" value="${lblAllowReviseQty}" id="btnAllowReviseQty" class="select_button" style="width:150px;"/>
                        <input type="button" value="${lblReviseShippingQty}" id="btnReviseShippingQty" class="select_button" style="width:150px;"/>
                    </td>
                </tr>
            </table>
        </html:form>
        
        <html:form styleId="Wshp004FormResult" action="/AsnMaintenanceAction.do" enctype="multipart/form-data">
            <html:hidden property="userLogin" styleId="userLogin"/>
            <html:hidden property="method" styleId="methodResult"/>
            <html:hidden property="mode" styleId="modeResult"/>
            <html:hidden property="screenId" styleId="screenIdResult"/>
            <html:hidden property="prevScreenId" styleId="prevScreenIdResult"/>
            <html:hidden property="temporaryMode" styleId="temporaryModeResult"/>
            <html:hidden property="receiveByScan" styleId="receiveByScanResult"/>
            <html:hidden property="asnNo" styleId="asnNoResult"/>
            <html:hidden property="asnRcvStatus" styleId="asnRcvStatusResult"/>
            <html:hidden property="invStatus" styleId="invStatusResult"/>
            <html:hidden property="lastModified" styleId="lastModifiedResult"/>
            <html:hidden property="actualEtdDate" styleId="actualEtdDateResult"/>
            <html:hidden property="actualEtdTime" styleId="actualEtdTimeResult"/>
            <html:hidden property="planEtaDate" styleId="planEtaDateResult"/>
            <html:hidden property="planEtaTime" styleId="planEtaTimeResult"/>
            <html:hidden property="noOfPallet" styleId="noOfPalletResult"/>
            <html:hidden property="tripNo" styleId="tripNoResult"/>
            <html:hidden property="utc" styleId="utcResult"/>
            <html:hidden property="dscId" styleId="dscId"/>
            <html:hidden property="sessionId" styleId="sessionId"/>
            <html:hidden property="successFlag" styleId="successFlag"/>
            <html:hidden property="operateReviseQtyMsg"/>
            <html:hidden property="allowReviseQtyFlag"/>
            <html:hidden property="skipValidateLotSizeFlag" styleId="skipValidateLotSizeFlag"/>
            <html:hidden property="unmatchedLotSizeFlag" styleId="unmatchedLotSizeFlag"/>
            <html:hidden property="continueOperation" styleId="continueOperation"/>
            <logic:notEmpty name="Wshp004Form" property="chgReasonList">
                <c:forEach var="changeReasonCb" items="${Wshp004Form.chgReasonList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="changeReasonCb" property="miscCd" indexed="true"/>
                    <html:hidden name="changeReasonCb" property="miscValue" indexed="true"/>
                </c:forEach>
            </logic:notEmpty>
            <!-- For WSHP003 screen -->
            <html:hidden property="DCd" styleId="DCd"/>
            <html:hidden property="doId" styleId="doIdResult"/>
            <html:hidden property="spsDoNo" styleId="spsDoNoResult"/>
            <html:hidden property="revision" styleId="revisionResult"/>
            <html:hidden property="routeNo" styleId="routeNoResult"/>
            <html:hidden property="del" styleId="delResult"/>
            
            <c:set var="lbAll">
                <sps:label name="ALL_CRITERIA" />
            </c:set>
            <div id="result" style="visibility:visible;">
                <div style="width:98%;height:330px;position:absolute;overflow-x:auto;overflow-y:auto;margin-top:0px">
                    <table class="display_data" id="tblBody" style="table-layout: fixed;width: 1343px;word-wrap: break-word;">
                        <col style="width:30px;"/>
                        <col style="width:70px;"/>
                        <col style="width:112px;"/>
                        <col style="width:86px;"/>
                        <col style="width:27px;"/>
                        <col style="width:90px;"/>
                        <col style="width:90px;"/>
                        <col style="width:90px;"/>
                        <col style="width:30px;"/>
                        <col style="width:55px;"/>
                        <col style="width:75px;"/>
                        <col style="width:75px;"/>
                        <col style="width:33px;"/>
                        <col style="width:165px;"/>
                        <col style="width:150px;"/>
                        <col style="width:60px;"/>
                        <tr>
                            <th scope="col">
                                <sps:label name="NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="RCV_LANE" />
                            </th>
                            <th scope="col" >
                                <sps:label name="D_PART_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="D/O_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="REV" />
                            </th>
                            <th scope="col">
                                <sps:label name="TOTAL_REMAINING_QTY" />
                            </th>
                            <th scope="col" >
                                <sps:label name="TOTAL_SHIPPED_QTY" />
                            </th>
                            <th scope="col" >
                                <sps:label name="SHIPPING_QTY" />
                            </th>
                            <th scope="col" >
                                <sps:label name="UM" />
                            </th>
                            <th scope="col" >
                                <sps:label name="QTY_BOX" />
                            </th>
                            <th scope="col" >
                                <sps:label name="SHIPPING_BOX_QTY" />
                            </th>
                            <th scope="col" >
                                <sps:label name="THIS_ASN_RECEIVED_QTY" />
                            </th>
                            <th scope="col" >
                                <sps:label name="PN_RCV_STATUS" />
                            </th>
                            <th scope="col" >
                                <sps:label name="CHANGE_REASON" />
                            </th>
                            <th scope="col" >
                                <sps:label name="S_PART_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ORDER_QTY" />
                            </th>
                        </tr>
                        <c:set var="recordGroup" value="">
                        </c:set>
                        <c:set var="rowIndex" value="0">
                        </c:set>
                        <c:set var="indexByPart" value="0">
                        </c:set>
                        <c:set var="indexByLane" value="0">
                        </c:set>
                        <c:set var="lbPleaseSelect">
                            <sps:label name="PLEASE_SELECT_CRITERIA" />
                        </c:set>
                        <c:forEach var="asnMaintenanceReturn" items="${Wshp004Form.doGroupAsnList}" begin="0" step="1" varStatus="status">
                            <tr>
                                <c:choose>
                                    <c:when test="${asnMaintenanceReturn.rowCount > 1}">
                                        <c:set var="verticalAlignStyle">
                                            vertical-align:top;
                                        </c:set>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="verticalAlignStyle">
                                            vertical-align:middle;
                                        </c:set>
                                    </c:otherwise>
                                </c:choose>
                                <td class="align-right">
                                    ${status.index + 1}
                                </td>
                                <c:choose>
                                    <c:when test="${not empty asnMaintenanceReturn.recordGroupByLane}">
                                        <c:choose>
                                            <c:when test="${asnMaintenanceReturn.rowCountByRcvLane > 1}">
                                                <c:set var="verticalAlignByLaneStyle">
                                                    vertical-align:top;
                                                </c:set>
                                            </c:when>
                                            <c:otherwise>
                                                <c:set var="verticalAlignByLaneStyle">
                                                    vertical-align:middle;
                                                </c:set>
                                            </c:otherwise>
                                        </c:choose>
                                        
                                        <td style="text-align:center; ${verticalAlignByLaneStyle}"
                                            rowspan="${asnMaintenanceReturn.rowCountByRcvLane}">
                                            <input type="checkbox" name="chkAllowReviseQty" id="chkAllowReviseQty${indexByLane}"
                                                onclick="wshp004SetAllowReviseFlag('${indexByLane}');"/>
                                            <br />
                                            ${asnMaintenanceReturn.asnDetailDomain.rcvLane}
                                            
                                            <html:hidden name="asnMaintenanceReturn" property="asnDetailDomain.allowReviseFlag"
                                                styleId="allowReviseQtyFlag${indexByLane}" indexed="true"/>
                                            <html:hidden name="asnMaintenanceReturn" property="reviseCompleteFlag"
                                                styleId="reviseCompleteFlag${indexByLane}" indexed="true"/>
                                        </td>
                                        <c:set var="indexByLane"  value="${indexByLane + 1}">
                                        </c:set>
                                    </c:when>
                                    <c:otherwise>
                                    </c:otherwise>
                                </c:choose>
                                <td class="align-left">
                                    ${asnMaintenanceReturn.DPn}
                                </td>
                                <td class="align-left">
                                    ${asnMaintenanceReturn.asnDetailDomain.spsDoNo}
                                </td>
                                <td class="align-left">
                                    ${asnMaintenanceReturn.asnDetailDomain.revision}
                                </td>
                                <td class="align-right">
                                    <div id="remainingQty${status.index}">
                                        ${asnMaintenanceReturn.totalRemainingQtyStr}
                                    </div>
                                </td>
                                <td class="align-right">
                                    ${asnMaintenanceReturn.totalShippedQtyStr}
                                </td>
                                <c:choose>
                                    <c:when test="${'' == recordGroup || '1' == asnMaintenanceReturn.recordGroup}">
                                        <c:set var="recordGroup"
                                            value="${asnMaintenanceReturn.recordGroup}">
                                        </c:set>
                                        <c:set var="rowIndex"
                                            value="${status.index}">
                                        </c:set>
                                        
                                        <td class="align-right" style="padding-left:0px; ${verticalAlignStyle}"
                                            rowspan="${asnMaintenanceReturn.rowCount}">
                                            
                                            
                                            <%-- FIX : modify Qty but not active --%>
                                            <%-- 
                                            <html:text name="asnMaintenanceReturn" property="shippingQtyByPartStr" indexed="true"
                                                value="${asnMaintenanceReturn.shippingQtyByPartStr}" 
                                                styleId="shippingQtyByPartStr${indexByPart}" style="width:90px;" styleClass="number"
                                                onkeypress="return PositiveDoubleFilter(event)"
                                                onblur="addComma(this)"
                                                onchange="return wshp004ReCalculateShipBoxQty('${asnMaintenanceReturn.DPn}${rowIndex}', '${status.index}', '${indexByPart}')"
                                                onfocus="removeComma('shippingQtyByPartStr${indexByPart}');return false;"/>
                                             --%>
                                            <html:text name="asnMaintenanceReturn" property="shippingQtyByPartStr" indexed="true"
                                                value="${asnMaintenanceReturn.shippingQtyByPartStr}" 
                                                styleId="shippingQtyByPartStr${indexByPart}" style="width:90px;" styleClass="number"
                                                onkeypress="return PositiveDoubleFilter(event)"
                                                onblur="wshp004ReCalculateShipBoxQty('${asnMaintenanceReturn.DPn}${rowIndex}', '${status.index}', '${indexByPart}'); addComma(this); return 0;"
                                                onfocus="removeComma('shippingQtyByPartStr${indexByPart}');return false;"/>
                                            
                                            <html:hidden name="asnMaintenanceReturn" property="shippingQtyByPart" indexed="true"
                                                styleId="shippingQtyByPart${indexByPart}"
                                                value="${asnMaintenanceReturn.shippingQtyByPart}"/>
                                            <html:hidden name="asnMaintenanceReturn" property="shippingBoxQtyByPartStr" indexed="true"
                                                styleId="shippingBoxQtyStr${asnMaintenanceReturn.DPn}${status.index}"
                                                value="${asnMaintenanceReturn.shippingBoxQtyByPartStr}"/>
                                            <html:hidden name="asnMaintenanceReturn" property="revisingFlag" indexed="true"
                                                styleId="revisingFlag${indexByPart}" value="${asnMaintenanceReturn.revisingFlag}"/>
                                            <html:hidden name="asnMaintenanceReturn" property="lotSizeNotMatchFlag" indexed="true"
                                                styleId="lotSizeNotMatchFlag${indexByPart}" value="${asnMaintenanceReturn.lotSizeNotMatchFlag}"/>
                                        </td>
                                        <td rowspan="${asnMaintenanceReturn.rowCount}"
                                            style="text-align:left; ${verticalAlignStyle}">
                                            ${asnMaintenanceReturn.asnDetailDomain.unitOfMeasure}
                                        </td>
                                        <td rowspan="${asnMaintenanceReturn.rowCount}"
                                            style="text-align:right; ${verticalAlignStyle}">
                                            <div id="qtyBoxDiv${asnMaintenanceReturn.DPn}${status.index}">
                                                ${asnMaintenanceReturn.qtyBoxStr}
                                            </div>
                                        </td>
                                        <td rowspan="${asnMaintenanceReturn.rowCount}"
                                            style="text-align:right; ${verticalAlignStyle}">
                                            
                                            <%-- Start : [IN039] allow user input Shipping Box Qty when unit is KG or MT --%>
                                            <%--
                                            <div id="shipBoxQtyDiv${asnMaintenanceReturn.DPn}${status.index}">
                                                ${asnMaintenanceReturn.shippingBoxQtyByPartStr}
                                            </div>
                                            --%>
                                            <c:choose>
                                                <c:when test="${'1' == asnMaintenanceReturn.editShippingBoxQtyFlg}">
                                                    <div id="shipBoxQtyDiv${asnMaintenanceReturn.DPn}${status.index}" style="display:none">
                                                        ${asnMaintenanceReturn.shippingBoxQtyByPartStr}
                                                    </div>
                                                    <html:text name="asnMaintenanceReturn" property="shippingBoxQtyByPartInput" indexed="true"
                                                        value="${asnMaintenanceReturn.shippingBoxQtyByPartInput}" 
                                                        styleId="shippingBoxQtyByPartInput${indexByPart}"
                                                        style="width:75px;" styleClass="number"
                                                        onkeypress="return PositiveIntegerFilter(event);"
                                                        onblur="addComma(this); return 0;"
                                                        onfocus="removeComma('shippingBoxQtyByPartInput${indexByPart}');return false;"/>
                                                </c:when>
                                                <c:otherwise>
                                                    <div id="shipBoxQtyDiv${asnMaintenanceReturn.DPn}${status.index}">
                                                        ${asnMaintenanceReturn.shippingBoxQtyByPartStr}
                                                    </div>
                                                    <html:hidden name="asnMaintenanceReturn" property="shippingBoxQtyByPartInput" indexed="true"
                                                        styleId="shippingBoxQtyByPartInput${indexByPart}"
                                                        value="${asnMaintenanceReturn.shippingBoxQtyByPartStr}"/>
                                                </c:otherwise>
                                            </c:choose>
                                            <%-- End : [IN039] allow user input Shipping Box Qty when unit is KG or MT --%>
                                            
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                    </c:otherwise>
                                </c:choose>
                                <td style="text-align:right;">
                                    ${asnMaintenanceReturn.asnReceivedQtyStr}
                                </td>
                                <td style="text-align:left;">
                                    ${asnMaintenanceReturn.asnPnReceivingStatus}
                                </td>
                                <c:choose>
                                    <c:when test="${'' == recordGroup || '1' == asnMaintenanceReturn.recordGroup}">
                                        <td rowspan="${asnMaintenanceReturn.rowCount}"
                                            style="text-align:center; ${verticalAlignStyle}">
                                            
                                            <%-- <html:hidden name="asnMaintenanceReturn" property="changeReasonCd" indexed="true"
                                                styleId="chgReason${indexByPart}" value="${asnMaintenanceReturn.changeReasonCd}"/> --%>
                                            <c:choose>
                                                <c:when test="${empty asnMaintenanceReturn.changeReasonCdBackUp}">
                                                    <c:set var="varChangeReasonCd">
                                                        ${asnMaintenanceReturn.changeReasonCd}
                                                    </c:set>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:set var="varChangeReasonCd">
                                                        ${asnMaintenanceReturn.changeReasonCdBackUp}
                                                    </c:set>
                                                </c:otherwise>
                                            </c:choose>
                                            <html:select name="asnMaintenanceReturn" property="changeReasonCd" value="${varChangeReasonCd}"
                                                styleId="chgReasonTxt${indexByPart}" style="width:163px;" styleClass="mandatory" indexed="true">
                                                <%-- onclick="wshp004SetChangeReasonCbItem(this.value, '${indexByPart}');return false;" --%>
                                                <html:option value="">
                                                    ${lbPleaseSelect}
                                                </html:option>
                                                <c:forEach var="chgReasonItem" items="${Wshp004Form.chgReasonList}" begin="0" step="1" varStatus="status">
                                                    <html:option value="${chgReasonItem.miscCd}">
                                                        ${chgReasonItem.miscValue}
                                                    </html:option>
                                                </c:forEach>
                                            </html:select>
                                            <html:hidden name="asnMaintenanceReturn" property="changeReasonCdBackUp" indexed="true"
                                                styleId="chgReasonBackUp${indexByPart}"/> 
                                            <html:hidden name="asnMaintenanceReturn" property="totalRemainingQtyByPart" indexed="true"
                                                styleId="remainingQtyByPart${indexByPart}" value="${asnMaintenanceReturn.totalRemainingQtyByPart}"/>
                                            
                                            <c:set var="indexByPart"  value="${indexByPart + 1}">
                                            </c:set>
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                    </c:otherwise>
                                </c:choose>
                                <td class="align-left">
                                    ${asnMaintenanceReturn.SPn}
                                </td>
                                <td class="align-right">
                                    <html:hidden name="asnMaintenanceReturn" property="userDomain.firstName" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="userDomain.middleName" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="userDomain.lastName" indexed="true"/>
                                    
                                    <html:hidden name="asnMaintenanceReturn" property="cigmaAsnDomain.pseudoAsnNo" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="cigmaAsnDomain.pseudoAsnStatus" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="cigmaAsnDomain.pseudoCigmaCurDoNo" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="cigmaAsnDomain.pseudoDPn" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="cigmaAsnDomain.pseudoPnReceivingStatus" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="cigmaAsnDomain.pseudoReceivedQty" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="cigmaAsnDomain.pseudoReceivingLane" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="cigmaAsnDomain.pseudoLastUpdate" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="cigmaAsnDomain.pseudoLastUpdateTime" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="cigmaAsnDomain.pseudoPnLastUpdate" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="cigmaAsnDomain.pseudoPnLastUpdateTime" indexed="true"/>
                                    
                                    <html:hidden name="asnMaintenanceReturn" property="SCd" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="SPcd" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="DCd" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="DPcd" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="receiveByScan" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDomain.asnNo" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDomain.SCd" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDomain.SPcd" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDomain.DCd" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDomain.DPcd" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDomain.vendorCd" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDomain.pdfFileId" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDomain.lastUpdateDatetime" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDomain.lastUpdateDatetime" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDomain.orderMethod" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDomain.shipDatetime" indexed="true"/>
                                    
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDomain.truckRoute" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDomain.truckSeq" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDomain.supplierLocation" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDomain.whPrimeReceiving" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDomain.dockCode" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDomain.tm" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDomain.cycle" indexed="true"/>
                                    
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDetailDomain.ctrlNo" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDetailDomain.itemDesc" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDomain.receiveByScan" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDetailDomain.lastUpdateDatetime" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDetailDomain.kanbanType" indexed="true"/>
                                    
                                    <html:hidden name="asnMaintenanceReturn" property="asnDetailDomain.lastUpdateDatetime" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDetailDomain.revision" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDetailDomain.spsDoNo" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDetailDomain.cigmaDoNo" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDetailDomain.doId" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDetailDomain.changeReasonCd" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDetailDomain.chgCigmaDoNo" indexed="true"/>
                                    
                                    <html:hidden name="asnMaintenanceReturn" property="DPn" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="SPn" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnPnReceivingStatus" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="totalShippedQtyStr" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="totalRemainingQtyStr" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="orderQtyStr" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="qtyBoxStr" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="rowCount" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="recordGroup" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDetailDomain.rcvLane" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDetailDomain.unitOfMeasure" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnReceivedQty" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnReceivedQtyStr" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDetailDomain.shippingBoxQty" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="totalShippedQty" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="totalRemainingQty" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDetailDomain.qtyBox" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="groupDoDetailDomain.currentOrderQty" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="asnDetailDomain.shippingQty" indexed="true"/>
                                    
                                    <html:hidden name="asnMaintenanceReturn" property="asnReceivedQtyByPart" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="rowCountByRcvLane" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="recordGroupByLane" indexed="true"/>
                                    <html:hidden name="asnMaintenanceReturn" property="allowReviseFlagBackUp" indexed="true"/>
                                    
                                    <%-- Start : [IN039] allow user input Shipping Box Qty when unit is KG or MT --%>
                                    <html:hidden name="asnMaintenanceReturn" property="editShippingBoxQtyFlg" indexed="true"/>
                                    
                                    <c:if test="${empty asnMaintenanceReturn.recordGroupByLane}">
                                        <html:hidden name="asnMaintenanceReturn" property="asnDetailDomain.allowReviseFlag" indexed="true"/>
                                    </c:if>
                                    <c:if test="${'' != recordGroup && '1' != asnMaintenanceReturn.recordGroup}">
                                        <html:hidden name="asnMaintenanceReturn" property="revisingFlag" indexed="true"/>
                                        <html:hidden name="asnMaintenanceReturn" property="lotSizeNotMatchFlag" indexed="true"/>
                                    </c:if>
                                    
                                    ${asnMaintenanceReturn.orderQtyStr}
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div style="width:98%;height:333px;">
                    &nbsp;
                </div>
                <table style="width:994px;">
                    <c:set var="lblSaveAndSend">
                        <sps:label name="SAVE_AND_SEND"/>
                    </c:set>
                    <c:set var="lblViewPdf">
                        <sps:label name="PREVIEW_PDF"/>
                    </c:set>
                    <c:set var="lblDownloadPdf">
                        <sps:label name="DOWNLOAD_PDF"/>
                    </c:set>
                    <c:set var="lblReturn">
                        <sps:label name="RETURN"/>
                    </c:set>
                    <c:set var="lblCancelAsnAndSend">
                        <sps:label name="CANCEL_ASN_AND_SEND"/>
                    </c:set>
                    <tr>
                        <td class="align-right">
                            <input type="button" value="${lblSaveAndSend}" class="select_button" id="btnSaveAndSend" style="width:130px;"/>
                            <input type="button" value="${lblViewPdf}" class="select_button" id="btnViewPdf"/>
                            <input type="button" value="${lblDownloadPdf}" class="select_button" id="btnDownloadPdf" />
                            <input type="button" value="${lblReturn}" class="select_button" id="btnReturn" />
                            <input type="button" value="${lblCancelAsnAndSend}" class="select_button" style="width:150px;" id="btnCancelAsnAndSend"/>
                        </td>
                    </tr>
                </table>
                <div id="displayLegend" style="margin: 0;padding-top: 2px;">
                    <table bgcolor="#EEEEEE" style="width: 994px;border: 0;">
                        <tr>
                            <td align="left" style="font-size:10px;">
                                <span style="font-weight: bold;">
                                    <sps:label name="LEGEND"/>
                                    <span style="text-decoration: underline;">
                                        <sps:label name="PN_RCV_STATUS"/>
                                    </span>
                                </span>
                                <sps:label name="STATUS_ISS"/>,&nbsp;
                                <sps:label name="STATUS_PTR"/>,&nbsp;
                                <sps:label name="STATUS_RCP"/>,&nbsp;
                                <sps:label name="STATUS_CCL"/>
                            </td>
                            <td class="align-right">
                                <a href="javascript:void(0)" style="font-size:10px;" id="moreLegend">
                                    &gt;&gt;&nbsp;
                                    <sps:label name="MORE"/>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </html:form>
    </body>
</html>
