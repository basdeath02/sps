<%@ include file="../includes/jsp_header.jspf"%>
<html>
    <head>
        <%@ include file="../includes/html_header.jspf"%>
        <jsp:useBean id="cons" class="com.globaldenso.asia.sps.common.constant.Constants"/>
        <script type="text/javascript" src="./js/business/Wshp003_InquiryDoDetail.js">
        </script>
        <title>
            <sps:label name="INQUIRY_DO_DETAIL"/>
        </title>
    </head>

    <body class="hideHorizontal">
        <html:form styleId="Wshp003FormCriteria" action="/InquiryDoDetailAction.do" >
            <%@ include file="../includes/Wcom003_MainMenu.jsp"%>
            <html:hidden property="userLogin" styleId="userLogin"/>
            <html:hidden property="method" styleId="method"/>
            <html:hidden property="mode" styleId="mode"/>
            <html:hidden property="screenId" styleId="screenId"/>
            <html:hidden property="DCd" styleId="DCd"/>
            
            <!-- Key for paging. -->
            <html:hidden styleId="currentPage" property="currentPage"/>
            <html:hidden styleId="row"  property="row"/>
            <html:hidden styleId="totalCount" property="totalCount"/>
            
            <logic:notEmpty name="Wshp003Form" property="supplierAuthenList">
                <c:forEach var="supplierAuthen" items="${Wshp003Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                    <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp003Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wshp003Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            
            <table id="ui-displayHeader" class="bottom">
                <tr>
                    <td>
                        <sps:label name="SHIPPING" />
                        &gt;
                        <sps:label name="CREATE_ASN_BY_MANUAL" />
                        &gt;
                        <sps:label name="INQUIRY_DO_DETAIL" />
                    </td>
                    <td colspan="2" class="align-right" >
                        <sps:label name="USER" />&nbsp;:
                        <bean:write name="Wshp003Form" property="userLogin"/>
                    </td>
                </tr>
            </table>
            
            <logic:notEmpty name="Wshp003Form" property="applicationMessageList">
                <div style="margin-bottom:5px">
                    <table style="width:992px" class="tableMessage">
                        <tr>
                            <td>
                                <c:forEach var="appMsg" items="${Wshp003Form.applicationMessageList}" begin="0" step="1" varStatus="status">
                                    <c:if test="${cons.MESSAGE_SUCCESS == appMsg.messageType}">
                                        <c:set var="messageClass">
                                            ui-messageSuccess
                                        </c:set>
                                    </c:if>
                                    <c:if test="${cons.MESSAGE_FAILURE == appMsg.messageType}">
                                        <c:set var="messageClass">
                                            ui-messageError
                                        </c:set>
                                    </c:if>
                                    <c:if test="${cons.MESSAGE_WARNING == appMsg.messageType}">
                                        <c:set var="messageClass">
                                            ui-messageWarning
                                        </c:set>
                                    </c:if>
                                    <span class="${messageClass}">
                                        ${appMsg.message}
                                    </span>
                                    <br>
                                </c:forEach>
                            </td>
                        </tr>
                    </table>
                </div>
            </logic:notEmpty>
            
            <c:choose>
                <c:when test="${'1' == Wshp003Form.displayResultFlag}">
                    <c:set var="visibilityFlag">
                        hidden
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="visibilityFlag">
                        visible
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="displayDiv" style="visibility: ${visibilityFlag};">
                <table class="display_criteria" id="ui-displayCriteria" style="padding-left:1px;border-spacing:0;">
                    <col style="width:120px;"/>
                    <col style="width:120px;"/>
                    <col style="width:100px;"/>
                    <col style="width:50px;"/>
                    <col style="width:100px;"/>
                    <col style="width:50px;"/>
                    <col style="width:100px;"/>
                    <col style="width:50px;"/>
                    
                    <tr>
                        <th>
                            <sps:label name="D/O_NO" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp003Form" property="spsDoNo" style="width:100px" styleId="spsDoNo"/>
                        </td>
                        <th>
                            <sps:label name="REV" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp003Form" property="revision" style="width:50px" styleId="revision"/>
                        </td>
                        <th>
                            <sps:label name="ROUTE" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp003Form" property="routeNo" style="width:50px" styleId="routeNo" />
                        </td>
                        <th>
                            <sps:label name="DEL" />
                            &nbsp;:
                        </th>
                        <td>
                            <html:text name="Wshp003Form" property="del" style="width:50px" styleId="del"/>
                        </td>
                    </tr>
                </table>
            </div>
            <ai:pagingoption maxRow="5"/>
        </html:form>
        
        <html:form styleId="Wshp003FormResult" action="/InquiryDoDetailAction.do">
            <html:hidden property="method" styleId="methodResult" value="doInitial"/>
            <html:hidden property="userLogin"/>
            <html:hidden property="mode"/>
            <html:hidden property="screenId"/>
            <html:hidden property="doId"/>
            <html:hidden property="spsDoNo"/>
            <html:hidden property="revision"/>
            <html:hidden property="routeNo"/>
            <html:hidden property="del"/>
            <html:hidden property="DCd"/>
            <html:hidden property="selectedAsnNo" styleId="selectedAsnNo"/>
            
            <logic:notEmpty name="Wshp003Form" property="supplierAuthenList">
                <c:forEach var="supplierAuthen" items="${Wshp003Form.supplierAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="supplierAuthen" property="SCd" indexed="true"/></input>
                    <html:hidden name="supplierAuthen" property="SPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            <logic:notEmpty name="Wshp003Form" property="densoAuthenList">
                <c:forEach var="densoAuthen" items="${Wshp003Form.densoAuthenList}" begin="0" step="1" varStatus="status">
                    <html:hidden name="densoAuthen" property="DCd" indexed="true"/></input>
                    <html:hidden name="densoAuthen" property="DPcd" indexed="true"/></input>
                </c:forEach>
            </logic:notEmpty>
            
            <c:choose>
                <c:when test="${1 == Wshp003Form.displayResultFlag}">
                    <c:set var="visibilityFlag">
                        hidden
                    </c:set>
                </c:when>
                <c:otherwise>
                    <c:set var="visibilityFlag">
                        visible
                    </c:set>
                </c:otherwise>
            </c:choose>
            <div id="displayResultDiv" style="visibility: ${visibilityFlag};">
                <div style="width:994px;">
                    <table class="display_data" id="tblBody" style="table-layout:fixed;width:974px;word-wrap:break-word;">
                        <%-- [IN004] : adjust column size --%>
                        <%--
                        <col style="width:10%;"/>
                        <col style="width:6%;"/>
                        <col style="width:10%;"/>
                        <col style="width:12%;"/>
                        <col style="width:5%;"/>
                        <col style="width:8%;"/>
                        <col style="width:8%;"/>
                        <col style="width:8%;"/>
                        --%>
                        <col style="width:10%;"/>
                        <col style="width:10%;"/>
                        <col style="width:3%;"/>
                        <col style="width:5%;"/>
                        <col style="width:8%;"/>
                        <col style="width:6%;"/>
                        <col style="width:8%;"/>
                        <col style="width:8%;"/>
                        <col style="width:8%;"/>
                        
                        <tr>
                            <%-- [IN004] : reorder column --%>
                            <%--
                            <th scope="col">
                                <sps:label name="ASN_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ASN_STATUS" />
                            </th>
                            <th scope="col" >
                                <sps:label name="D_PART_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="S_PART_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="UM" />
                            </th>
                            --%>
                            <th scope="col" >
                                <sps:label name="D_PART_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="S_PART_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="UM" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ORDER_QTY" />
                            </th>
                            <th scope="col">
                                <sps:label name="ASN_NO" />
                            </th>
                            <th scope="col" >
                                <sps:label name="ASN_STATUS" />
                            </th>
                            <th scope="col" >
                                <sps:label name="SHIPPING_QTY" />
                            </th>
                            <th scope="col" >
                                <sps:label name="RECEIVING_QTY" />
                            </th>
                            <th scope="col" >
                                <sps:label name="IN-TRANSIT_QTY" />
                            </th>
                        </tr>
                    </table>
                </div>
                <div style="width:994px;height:420px;overflow-y:scroll">
                    <table class="display_data" id="tblBody"
                        style="table-layout: fixed;width: 974px;word-wrap: break-word;border-width: 0px">
                        <%-- [IN004] : adjust column size --%>
                        <%--
                        <col style="width:10%;"/>
                        <col style="width:6%;"/>
                        <col style="width:10%;"/>
                        <col style="width:12%;"/>
                        <col style="width:5%;"/>
                        <col style="width:8%;"/>
                        <col style="width:8%;"/>
                        <col style="width:8%;"/>
                        --%>
                        <col style="width:10%;"/>
                        <col style="width:10%;"/>
                        <col style="width:3%;"/>
                        <col style="width:5%;"/>
                        <col style="width:8%;"/>
                        <col style="width:6%;"/>
                        <col style="width:8%;"/>
                        <col style="width:8%;"/>
                        <col style="width:8%;"/>
                        <c:forEach var="doDetail" items="${Wshp003Form.inquiryDoDetailList}" begin="0" step="1" varStatus="status">
                            <tr class="${(status.index + 1) % 2 == 0 ? 'blueLight' : ''}">
                            
                                <%-- [IN004] : Check merge cell --%>
                                <c:choose>
                                    <c:when test="${doDetail.DPn ne previousDpn && '1' ne doDetail.partsRank}">
                                        <c:set var="rowSpanCause">
                                            rowspan="${doDetail.partsRank}" style="vertical-align:top;"
                                        </c:set>
                                    </c:when>
                                    <c:otherwise>
                                        <c:set var="rowSpanCause">
                                            
                                        </c:set>
                                    </c:otherwise>
                                </c:choose>
                                <c:choose>
                                    <c:when test="${doDetail.DPn ne previousDpn}">
                                        <td class="align-left" ${rowSpanCause}>
                                            ${doDetail.DPn}
                                        </td>
                                        <td class="align-left" ${rowSpanCause}>
                                            ${doDetail.SPn}
                                        </td>
                                        <td class="align-left" ${rowSpanCause}>
                                            ${doDetail.doDetailDomain.unitOfMeasure}
                                        </td>
                                        <td class="align-right" ${rowSpanCause}>
                                            ${doDetail.orderQtyStr}
                                        </td>
                                    </c:when>
                                    <c:otherwise>
                                        
                                    </c:otherwise>
                                </c:choose>
                                
                                <td class="align-left">
                                    ${doDetail.asnDetailDomain.asnNo}
                                    <html:hidden name="doDetail" property="asnDetailDomain.asnNo" indexed="true"/>
                                    <html:hidden name="doDetail" property="asnDomain.asnStatus" indexed="true"/>
                                    <html:hidden name="doDetail" property="DPn" indexed="true"/>
                                    <html:hidden name="doDetail" property="SPn" indexed="true"/>
                                    <%-- [IN004] : change Domain --%>
                                    <%-- <html:hidden name="doDetail" property="asnDetailDomain.unitOfMeasure" indexed="true"/> --%>
                                    <html:hidden name="doDetail" property="doDetailDomain.unitOfMeasure" indexed="true"/>
                                    
                                    <%-- [IN004] start : add field --%>
                                    <html:hidden name="doDetail" property="orderQtyStr" indexed="true"/>
                                    <html:hidden name="doDetail" property="partsRank" indexed="true"/>
                                    <%-- [IN004] end : add field --%>
                                    <html:hidden name="doDetail" property="shippingQtyStr" indexed="true"/>
                                    <html:hidden name="doDetail" property="receivingQtyStr" indexed="true"/>
                                    <html:hidden name="doDetail" property="inTransitQtyStr" indexed="true"/>
                                </td>
                                <td class="align-left">
                                    ${doDetail.asnDomain.asnStatus}
                                </td>
                                <%-- [IN004] : move to above --%>
                                <%--
                                <td class="align-left">
                                    ${doDetail.DPn}
                                </td>
                                <td class="align-left">
                                    ${doDetail.SPn}
                                </td>
                                <td class="align-left">
                                    ${doDetail.asnDetailDomain.unitOfMeasure}
                                </td>
                                --%>
                                <td class="align-right">
                                    ${doDetail.shippingQtyStr}
                                </td>
                                <td class="align-right">
                                    ${doDetail.receivingQtyStr}
                                </td>
                                <td class="align-right">
                                    ${doDetail.inTransitQtyStr}
                                </td>
                                
                                <%-- [IN004] : for check merge --%>
                                <c:set var="previousDpn">
                                    ${doDetail.DPn}
                                </c:set>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
            <table style="width: 994px;">
                <c:set var="labelReturn">
                    <sps:label name="RETURN"/>
                </c:set>
                <tr>
                    <td class="align-left" style="vertical-align:middle;">
                        <ai:paging action="/SPS/InquiryDoDetailAction.do" formId="Wshp003FormResult" jump="true" />
                    </td>
                    <td class="align-right">
                        <input type="button" id="btnReturn" value="${labelReturn}" class="select_button" style="width:120px;"/>
                    </td>
                </tr>
            </table>
            <div id="displayLegend" style="margin:0; padding-top:2px;">
                <table bgcolor="#EEEEEE" style="width:994px; border:0;">
                    <tr>
                        <td align="left" style="font-size:10px;">
                            <span style="font-weight: bold;">
                                <sps:label name="LEGEND"/>
                                <span style="text-decoration: underline;">
                                    <sps:label name="ASN_STATUS"/>
                                </span>
                            </span>
                            <sps:label name="STATUS_NEW"/>,&nbsp;
                            <sps:label name="STATUS_PTR"/>,&nbsp;
                            <sps:label name="STATUS_RCP"/>,&nbsp;
                            <sps:label name="STATUS_CCL"/>
                        </td>
                        <td class="align-right">
                            <a href="javascript:void(0)" style="font-size:10px;" id="moreLegend">
                                &gt;&gt;&nbsp;
                                <sps:label name="MORE"/>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </html:form>
    </body>
</html>
