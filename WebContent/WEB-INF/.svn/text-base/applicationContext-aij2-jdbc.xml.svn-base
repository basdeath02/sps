<?xml version="1.0" encoding="UTF-8"?>
<!--
 * applicationContext-aij2-jdbc.xml
 * 
 * [JP] AI-J2(Spring) の DB 接続設定ファイルです。(ローカル環境用)
 * [JP]
 * [JP] 以下の DB 接続情報を定義しています。
 * [JP]
 * [JP] ・トランザクションマネージャーの設定
 * [JP] ・データソースの設定
 * [JP] ・jdbc.properties の指定
 * [JP] ・iBatis の設定
 * 
 * [EN] This is the configuration file for DB connection AI-J2(Spring). (For local environment)
 * [EN]
 * [EN] We define the following DB connection information.
 * [EN]
 * [EN] ・Configuration of the transaction manager
 * [EN] ・Configuration of the data source
 * [EN] ・Specify jdbc.properties
 * [EN] ・Configuration of the iBatis
 * 
 * $ applicationContext-aij2-jdbc.xml 4294 2013-05-15 12:30:57Z HIDETOSHI_NAKATANI@denso.co.jp $
 *
 * Copyright (c) 2011 DENSO IT SOLUTIONS. All rights reserved.
-->
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:aop="http://www.springframework.org/schema/aop"
    xmlns:tx="http://www.springframework.org/schema/tx"
    xsi:schemaLocation="
        http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd
        http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-2.5.xsd
        http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-2.5.xsd">

    <!-- [JP] トランザクション管理 ============================================== -->
    <!-- Transaction management ============================================== -->
    <bean id="transactionManager"
        class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="dataSource" />
    </bean>

    <!-- [JP] データソース ==========================================================
         [EN] Data source ==========================================================
         [JP] 【注意】
         [JP]     大人数で開発を行った場合にコネクション数が不足する可能性があるため、
         [JP]     ローカル環境でのコネクション数を制限しています。
         [JP]     本番環境ではこの設定を使用しないでください。
         [EN] [Note]
         [EN]     Because there is a possibility that the number of connections is insufficient when a large number of people and have developed, 
         [EN]     we are to limit the number of connections in the local environment.
         [EN]     Please do not use this setting in a production environment.
    ======================================================================== -->
    <bean id="dataSource"
        class="org.apache.commons.dbcp.BasicDataSource"
        destroy-method="close">
        <property name="driverClassName" value="${jdbc.driver}" />
        <property name="url" value="${jdbc.url}" />
        <property name="username" value="${jdbc.username}" />
        <property name="password" value="${jdbc.password}" />
        <property name="initialSize" value="1" />
        <property name="maxActive" value="10" />
        <property name="maxIdle" value="8" />
        <property name="maxWait" value="100000" />
        <property name="minIdle" value="3" />
        <property name="removeAbandoned" value="true" />
        <property name="removeAbandonedTimeout" value="300" />
        <property name="logAbandoned" value="false" />
        <property name="accessToUnderlyingConnectionAllowed"
            value="true" />
    </bean>

    <!-- [JP] JDBCプロパティファイル ============================================ -->
    <!-- [EN] JDBC properties file ============================================ -->
    <bean id="jdbcConnectionConfigurer"
        class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
        <property name="location" value="classpath:jdbc-aij2.properties" />
    </bean>

    <!-- simpleExtractorのBean定義設定 -->
    <!-- ※ OC4JのJNDIからデータソースを取得する場合は、プロパティはすべてtrueにしておくこと。 -->
    <bean id="simpleExtractor"
        class="org.springframework.jdbc.support.nativejdbc.SimpleNativeJdbcExtractor">
        <property name="nativeConnectionNecessaryForNativeStatements" value="true"/>
        <property name="nativeConnectionNecessaryForNativePreparedStatements" value="true"/>
        <property name="nativeConnectionNecessaryForNativeCallableStatements" value="true"/>
    </bean>

    <!-- LOBフィールドを扱うためのハンドラ -->
    <bean id="oracleLobHandler"
        class="org.springframework.jdbc.support.lob.OracleLobHandler">
        <property name="nativeJdbcExtractor" ref="simpleExtractor"/>
    </bean>

    <!-- [JP] iBATIS 設定 ======================================================= -->
    <!-- [EN] iBATIS configuration ======================================================= -->
    <bean id="sqlMapClient"
        class="org.springframework.orm.ibatis.SqlMapClientFactoryBean">
        <property name="configLocation">
            <value>WEB-INF/SqlMapConfig-aij2.xml</value>
        </property>
        <property name="useTransactionAwareDataSource">
            <value>true</value>
        </property>
        <property name="dataSource">
            <ref bean="dataSource" />
        </property>
        <property name="lobHandler">
            <ref bean="oracleLobHandler" />
        </property>
    </bean>
</beans>