<tr>
    <td width="975">
        <table width="970">
            <tr>
                <td colspan="2" align="right">
                    &nbsp;
                </td>
            </tr>
            <tr>
            <td colspan="2">
                    <b>[Registration Application Form]</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;Please complete the form and send to <a href="#">Z4106 Cl@denso.co.jp</a>                
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;<a href="#">For DENSO group companies outside Japan(English)</a>                
                </td>
            <tr>
                <td colspan="2" align="right">&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>[How to Login]</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;Please input DSC-ID&Password issued by DSC.
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;When changing the password, please change it at the <a href="" target="_self">Change Password Page.</a> 
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>[Help Desk]</b>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp; Lost password, ID locked:
                </td>
                <td>
                   <a href="#">oasec_helpdesk@dnitsol.com</a>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp; DSC-ID Registration:
                </td>
                <td>
                   <a href="#">oasec_helpdesk@dnitsol.com</a>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp; Other system related issues:
                </td>
                <td>
                   <a href="#">Z4106_Cl@denso.com</a>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="textred">
                    [This system recommends Microsoft Internet Explorer 8.0 and above.]
                </td>
            </tr>
        </table>
        <HR SIZE="1">
        <font size=1px>Copyright(C) 2014 DENSO CORPORATION. All rights reserved.</font>
    </td>
</tr>