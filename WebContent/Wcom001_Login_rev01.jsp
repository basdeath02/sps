<%@ include file="./WEB-INF/jsp/includes/jsp_header.jspf"%>
<html>
<head>

<title>
    SUPPLIER PORTAL SYSTEM
</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Cache-Control" Content="no-cache">
<meta http-equiv="pragma" Content="no-cache">
<script type="text/javascript" src="./js/lib/jquery-1.6.2.min.js" ></script>
<script type="text/javascript" src="./js/lib/jquery.json-2.3.min.js" ></script>
<script type="text/javascript" src="./js/lib/jquery-ui-1.8.6.custom.min.js" ></script>
<script type="text/javascript" src="./js/lib/tabs.js" ></script>
<link type="text/css" rel="stylesheet" href="./css/main.css" />
<link type="text/css" rel="stylesheet" href="./css/default-theme.css">
<link type="text/css" rel="stylesheet" href="./css/jquery-ui-1.8.6.custom.css" />  
<link type="text/css" rel="stylesheet" href="./css/UIFilterList.css" />
<link type="text/css" rel="stylesheet" href="./css/dropdown.css" media="screen" />

<script type="text/javascript" src="./js/business/Wcom001_Login.js" ></script>
<script type="text/javascript">
    var labelPassword        = 'Passwoed';
    var msgPasswordMandatory = 'msgPasswordMandatory';
    var msgUserIdNotFound    = 'msgUserIdNotFound';
    var msgPasswordIncorrect = 'msgPasswordIncorrect';
</script>
</head>
<body class="topframe">
<html:form styleId="Wcom001Form" action="/LoginAction.do" >
    <html:hidden styleId="method" property="method" />
    <input type="hidden" name="targetLocale" id="targetLocale"/>
    <table width="975">
        <tr>
            <td>
                <table background="css/images/bgLogin.jpg" width="1000px" style="margin-left:2px" border="0" cellspacing="0">
                    <tr>
                        <td>
                            <img src="css/images/denso.png" width="143" height="20" />
                        </td>
                        <td class="align-right" >
                            <span style="font-weight:bold;">
                                SUPPLIER PORTAL
                                &nbsp;&nbsp;&nbsp;
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;
                        </td>
                        <td class="align-right">
                            <a onclick="changeLang('th_TH')" target="_parent">
                                <img border="0" src="./images/language/th.jpg" alt="Thai" width="20" height="14" />
                            </a>&nbsp;&nbsp;|&nbsp;&nbsp; 
                            <a onclick="changeLang('en_US')" target="_parent">
                                <img border="0" src="./images/language/en.jpg" alt="English" width="20" height="14" />
                            </a>&nbsp;&nbsp;|&nbsp;&nbsp; 
                            <a onclick="changeLang('ja_JP')" target="_parent">
                                <img border="0" src="./images/language/jp.jpg" alt="Japanese" width="20" height="14" />
                            </a>&nbsp;&nbsp;|&nbsp;&nbsp; 
                            <a href="Help.jsp" target="_parent" class="style2">
                                Help
                            </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <!-- <tr bgcolor="#CCCCCC">          
                        <td width="40%">
                            <b>
                                Mocking version 1.0.0
                            </b>
                        </td>
                        <td width="60%">
                            <div align="right">
                                <a href="../index.html" target="_parent"></a>
                            </div>            
                        </td>
                    </tr> -->
                </table>
            </td>
        </tr>
        <tr>
            <td width="30%" valign="top">
                    <table width="30%" class="display_criteria" >
                        <tr>
                            <th class="right" nowrap="nowrap" width="100px">
                                <div align="right">
                                    <sps:label name="USER_ID" />
                                    &nbsp;:
                                </div>
                            </th>
                            <td width="200px">
                                <html:text name="Wcom001Form" property="userName" styleId="userName" 
                                    styleClass="mandatory" style="width:180px;" size="27" value="900616000301"/>
                                    <!-- 99999999990000000001 -->
                            </td>
                        </tr>
                        <tr>
                            <th class="right" nowrap="nowrap">
                                <div align="right">
                                    <sps:label name="PASSWORD" />
                                    &nbsp;:
                                </div>
                            </th>
                            <td >
                                <html:password name="Wcom001Form" property="password" styleId="password" 
                                    styleClass="mandatory" style="width:180px;" size="27"/>
                            </td>
                        </tr>
                    </table>

                    <table width="290">
                        <tr>
                            <td class="align-right" >
                                <input name="btnLogin" type="button" id="btnLogin" style="width:100px;" 
                                    value='Login' />
                                    
                                <!-- Test File Manager stream -->
                                <!-- <input name="btnFileManager" type="button" id="btnFileManager" style="width:100px;" 
                                    value='File Manager' /> -->
                            </td>
                        </tr>
                    </table>
                    <table width="40%">
                        <tr>
                            <td class="align-left">
                                <input name="Favorite" id="Favorite" type="button" value="Add to favorite" 
                                class="select_button2" onclick="favorite();"/>
                            </td>
                            <td >
                                [Click the left button to bookmark this page.]
                            </td>
                        </tr>
                    </table>
            </td>
        </tr>
        <%@ include file="./HowTo.jsp"%>
    </table>
</html:form>
</body>
</html>