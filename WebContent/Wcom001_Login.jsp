<%@ page contentType="text/html; charset=iso-8859-1" language="java"
    import="java.sql.*" errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ include file="./WEB-INF/jsp/includes/html_header.jspf"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="./css/default-theme.css"/>
<link rel="stylesheet" href="./css/main.css"/>
<title>Login</title>
</head>

<body oncontextmenu="return false" class="topframe" style="margin-left:5px;">

<table background="css/images/bgLogin.jpg" width="1000px" style="margin-left:2px" border="0" cellspacing="0">
<tr>
<td>
    <img src="css/images/denso.png" width="143" height="20" /></td>
    <td class="align-right" ><b>SUPPLIER PORTAL&nbsp;&nbsp;&nbsp;</b></td>
    </tr>
</table>
 <div id="errorMsg" style="display:none" class="divErrMsg">
                <table style="margin-left:5px">
                    <tr>
                        <td style="color:#FF0000"><b>Error message.</b></td>
                    </tr>
                </table>
</div>
<table width="1000px" border="0">
<tr>
<th width="20%" align="left">Login</th>

<th width="70%" align="right">Language</th>
<td width="10%"><select id="Language" class="txtbox2">
                    <option value="1">English</option>
                    <option value="2">Thai</option>
                    <option value="3">Japanese</option>                   
                </select></td>
</tr>
</table>
<form name="form" action="Wcom002_MainMenu.jsp" method="get">
<table width="30%" class="display_criteria" border="0">

    <tr>
        <th width="40%" class="align-right">Username :&nbsp;</th>
        <td width="60%" ><input id="username" name="username" type="text" class="txtname"/></td>
    </tr>
    <tr>
        <th class="align-right">Password :&nbsp;</th>
        <td><input name="" type="password" class="txtname"/></td>
    </tr>

</table>

<table width="30%">
    <tr>
        <td class="align-right" ><input name="Login" id="Login" type="submit"
            value="Login" class="select_button" /></td>
    </tr>
</table>
<table width="40%">
    <tr>
        <td class="align-left"><input name="Favorite" id="Favorite" type="button" 
        value="Add to favorite" class="select_button2" onclick="doErr()"/></td>
        <td >[Click the left button to bookmark this page.]</td>
    </tr>
</table>

   <%@ include file="./HowTo.jsp" %>
</form>
</body>

<script language="javascript">
function doErr(){
    document.getElementById("errorMsg").style.display = "";
}
</script>
</html>

