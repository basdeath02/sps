<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://www.denso.co.th/sps" prefix="sps"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head style="margin-top:0px">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="./css/dropdown.css" media="screen" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="./css/main.css"/>
<title>Menu</title>
</head>

<body>
<table background="css/images/bgHeader3.jpg" width="1000px" style="margin-left:2px" border="0" cellspacing="0">
<tr>
<td>
<img src="css/images/denso.png" width="143" height="20" /></td>
 <td class="align-right" ><b>SUPPLIER PORTAL&nbsp;&nbsp;&nbsp;</b></td>
</tr>
<tr>
<td colspan="2" >
  <ul id="nav" class="dropdown dropdown-horizontal">
    <li id="n-sps">Home <img src="images/icons/arrow4.png" align="middle"/>
    <ul>
            <li class="first"><a href="WCOM002_MainMenu.jsp?username=S" class="dir">Main Screen</a></li>
    </ul>
    </li>
    <li id="n-sps">Ordering <img src="images/icons/arrow4.png" align="middle"/>
    <ul>
            <li class="first"><a href="WORD001_SupplierInfo.jsp" class="dir">Supplier Information Uploading</a></li>
            <li class="first"><a href="WORD002_PurchaseOrderInformation.jsp" class="dir">Purchase Order Information</a></li>
            <li class="first"><a href="WORD006_FixOrder.jsp" class="dir">Delivery Order Information</a></li>
            <li class="first"><a href="WORD007_KanbanOrder.jsp" class="dir">KANBAN Order Information</a></li>
            
    </ul>
    </li>
    <li id="n-sps">Shipping <img src="images/icons/arrow4.png" align="middle"/>
       <ul>
            <li class="first"><a href="WSHP001_groupASNbyManual.jsp" class="dir">Acknowledged DO Information</a></li>
            <li class="first"><a href="WSHP006_UploadASNDatabyCSV.jsp" class="dir">ASN Uploading</a></li>
            <li class="first"><a href="WSHP008_ASNInfo.jsp" class="dir">ASN Detail Information</a></li>
            <li class="first"><a href="WSHP009_ASNProgress.jsp" class="dir">ASN Progress Information</a></li>
            <li class="first"><a href="WSHP007_BackOrder.jsp" class="dir">Back Order Information</a></li>
       </ul>
    </li>                   
    <li id="n-sps">Invoice <img src="images/icons/arrow4.png" align="middle"/>
        <ul>
            <li><a href="WINV001_InvInfo.jsp?mode=EDIT" class="dir">Invoice Information</a></li>
            <li><a href="WINV002_ManualGroup.jsp" class="dir">ASN Information</a></li>
            <li><a href="WINV003_UploadCSV.jsp" class="dir">Invoice Information Uploading</a></li>
            <li><a href="WINV005_DiffPriceInfo.jsp" class="dir">Price Difference Information</a></li>
            <li><a href="WINV006_UploadingFile.jsp" class="dir">CN/DN Uploading</a></li>
             <li><a href="WINV007_DownloadingFile.jsp" class="dir">CN/DN Downloading</a></li>
              <!--<ul>
                 <li><a href="WINV002_InquiryInv.jsp?mode=VIEW">Inquiry Invoice</a></li>
                 <li class="first"><a href="WINV005_ManualGroup.jsp">Manual Grouping</a></li>
                 <li class="first"><a href="WINV008_UploadCSV.jsp">Uploading CSV</a></li>
              </ul></li>
            
            <li><a href="#" class="dir">Invoice Submission<img src="images/icons/arrow-right.png" align="right"/></a>
                <ul>
                    <li class="first"><a href="WINV005_ManualGroup.jsp">Manual Grouping</a></li>
                    <li class="first"><a href="WINV008_UploadCSV.jsp">Uploading CSV</a></li>
                </ul>
            </li>
           
            <li><a href="#" class="dir">Unmatched Information<img src="images/icons/arrow-right.png" align="right"/></a>
               <ul>
                 <li><a href="WINV004_TempPriceQTY.jsp">Temp Price and QTY</a></li>
                 <li class="first"><a href="WINV011_DiffQTY.jsp">Difference QTY</a></li>
                 <li><a href="WINV012_CNMatchQTY.jsp">CN for unmatched QTY</a></li>
                 <li><a href="WINV014_PrintCNCoverPage.jsp">Print CN Cover Page</a></li>
              </ul>
            </li>
            
            <li><a href="WINV004_TempPriceQTY.jsp" class="dir">Temp Price Information</a></li>
            <li><a href="#" class="dir">Upload / Download File<img src="images/icons/arrow-right.png" align="right"/></a>
                <ul>
                    <li><a href="WINV016_UploadingFile.jsp">Uploading File</a></li>
                    <li><a href="WINV017_DownloadingFile.jsp">Downloading File</a></li>
                </ul>
            </li>
            -->
        </ul>
    </li>
   <li id="n-sps">Admin Config <img src="images/icons/arrow4.png" align="middle"/>
       <ul>
            <li><a href="#" class="dir">Supplier User Information<img src="images/icons/arrow-right.png" align="right"/></a>
                <ul>
                    <li class="first"><a href="WADM001_InquirySupplierUser.jsp">Supplier User Information</a></li>
                    <li><a href="WADM002_RegSupplierUser.jsp?mode=Register">Supplier User Registration</a></li>
                    <li><a href="WADM003_ImpSupplierUser.jsp">Supplier User Uploading</a></li>
                </ul>
            </li>
            <li><a href="#" class="dir">DENSO User Information<img src="images/icons/arrow-right.png" align="right"/></a>
                <ul>
                    <li class="first"><a href="WADM005_InquiryDENSOUser.jsp">DENSO User Information</a></li>
                    <li ><a href="WADM006_RegDENSOUser.jsp?mode=Register">DENSO User Registration</a></li>
                    <li><a href="WADM007_ImpDENSOUser.jsp">DENSO User Uploading</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li id="n-sps">Contact Us</li>
    <li id="n-sps">Help</li>
    <li id="n-sps" style="width:132px">Logout</li>
</ul>
</td>
</tr>
</table>
</body>
</html>
