<?xml version="1.0" encoding="UTF-8"?>
<!--
 * applicationContext-aijb-AMC.xml (編集不可)
 * 
 * AI-JB(Spring) AijbMainController の AOP 設定ファイルです。
 * AI-JB(Spring) AijbMainController の JDBC 設定ファイルです。
 * 
 * 以下の DB 接続情報を定義しています。
 * 
 *  ・トランザクションマネージャーの設定
 *  ・データソースの設定
 *  ・jdbc.properties の指定
 *  ・iBatis の設定
 * 
 * $ applicationContext-aijb-AMC.xml 6997 2014-02-21 05:58:59Z HIDETOSHI_NAKATANI@denso.co.jp $
 *
 * Copyright (c) 2011 DENSO IT SOLUTIONS. All rights reserved.
-->
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:aop="http://www.springframework.org/schema/aop"
    xmlns:tx="http://www.springframework.org/schema/tx"
    xsi:schemaLocation="
        http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd
        http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-2.5.xsd
        http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-2.5.xsd">


    <!-- AOP設定 =========================================================== -->
    <aop:config>
        <aop:pointcut id="serviceAop"
            expression="execution(* *..*Service.*(..))" />
        <aop:pointcut id="daoAop"
            expression="execution(* *..*Dao.*(..))" />

        <aop:advisor pointcut-ref="serviceAop"
            advice-ref="transactionInterceptor" />
        <aop:advisor pointcut-ref="serviceAop"
            advice-ref="serviceLoggingAdvice" />
        <aop:advisor pointcut-ref="daoAop"
            advice-ref="daoLoggingAdvice" />
        <aop:advisor pointcut-ref="daoAop"
            advice-ref="integrationLayerThrowsAdvice" />

    </aop:config>

    <!-- トランザクション設定 ============================================== -->
    <tx:advice id="transactionInterceptor"
        transaction-manager="transactionManager">
        <tx:attributes>
            <!-- 既存トランザクションを使用する設定 (REQUIRED) -->
            <tx:method name="create*" propagation="REQUIRED"
                isolation="READ_COMMITTED" read-only="false"
                rollback-for="java.lang.Exception" />
            <tx:method name="update*" propagation="REQUIRED"
                isolation="READ_COMMITTED" read-only="false"
                rollback-for="java.lang.Exception" />
            <tx:method name="delete*" propagation="REQUIRED"
                isolation="READ_COMMITTED" read-only="false"
                rollback-for="java.lang.Exception" />
            <tx:method name="search*" propagation="REQUIRED"
                isolation="READ_COMMITTED" read-only="true"
                rollback-for="java.lang.Exception" />
            <tx:method name="transact*" propagation="REQUIRED"
                isolation="READ_COMMITTED" read-only="false"
                rollback-for="java.lang.Exception" />

            <!-- 新規トランザクションを使用する設定 (REQUIRES_NEW) -->
            <tx:method name="aijbCreate*" propagation="REQUIRES_NEW"
                isolation="READ_COMMITTED" read-only="false"
                rollback-for="java.lang.Exception" />
            <tx:method name="aijbUpdate*" propagation="REQUIRES_NEW"
                isolation="READ_COMMITTED" read-only="false"
                rollback-for="java.lang.Exception" />
            <tx:method name="aijbDelete*" propagation="REQUIRES_NEW"
                isolation="READ_COMMITTED" read-only="false"
                rollback-for="java.lang.Exception" />
            <tx:method name="aijbSearch*" propagation="REQUIRES_NEW"
                isolation="READ_COMMITTED" read-only="true"
                rollback-for="java.lang.Exception" />
            <tx:method name="aijbTransact*" propagation="REQUIRES_NEW"
                isolation="READ_COMMITTED" read-only="false"
                rollback-for="java.lang.Exception" />
        </tx:attributes>
    </tx:advice>

    <!-- Bean 定義 ========================================================= -->
    <!-- Serviceログ出力 -->
    <bean id="serviceLoggingAdvice"
        class="com.globaldenso.ai.common.core.logging.ServiceLoggingInterceptor" />

    <!-- Daoログ出力 -->
    <bean id="daoLoggingAdvice"
        class="com.globaldenso.ai.common.core.logging.DaoLoggingInterceptor" />

    <!-- Integrationレイヤの例外ハンドラ  -->
    <bean id="integrationLayerThrowsAdvice"
        class="com.globaldenso.ai.common.core.exception.IntegrationLayerThrowsAdvice" />
    <!-- AOP設定 =========================================================== -->


    <!-- JDBC設定 ======================================================== -->
    <!-- トランザクション管理 ============================================== -->
    <bean id="transactionManager"
        class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="dataSource" />
    </bean>

    <!-- データソース ====================================================== -->
    <bean id="dataSource"
        class="org.apache.commons.dbcp.BasicDataSource"
        destroy-method="close">
        <property name="driverClassName" value="${jdbc.driver}" />
        <property name="url" value="${jdbc.url}" />
        <property name="username" value="${jdbc.username}" />
        <property name="password" value="${jdbc.password}" />
        <property name="initialSize" value="3" />
        <property name="maxActive" value="10" />
        <property name="maxIdle" value="3" />
        <property name="maxWait" value="10000" />
        <property name="minIdle" value="3" />
        <property name="removeAbandoned" value="true" />
        <property name="removeAbandonedTimeout" value="300" />
        <property name="logAbandoned" value="false" />
        <property name="accessToUnderlyingConnectionAllowed"
            value="true" />
    </bean>

    <!-- JDBCプロパティファイル ============================================ -->
    <bean id="jdbcConnectionConfigurer"
        class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
        <property name="location" value="jdbc-aijb.properties" />
    </bean>

    <!-- iBATIS 設定 ======================================================= -->
    <bean id="sqlMapClient"
        class="org.springframework.orm.ibatis.SqlMapClientFactoryBean">
        <property name="configLocation">
            <value>classpath:./SqlMapConfig-aijb-AMC.xml</value>
        </property>
        <property name="useTransactionAwareDataSource">
            <value>true</value>
        </property>
        <property name="dataSource">
            <ref bean="dataSource" />
        </property>
    </bean>
    <!-- JDBC設定 ======================================================== -->


    <!-- キュー実行準備及び実行のためのサービスです。 -->
    <bean id="queueExecute"
        class="com.globaldenso.ai.aijb.core.QueueExecutePrepare">
        <property name="queueService" ref="queueService" />
        <property name="jobManageService" ref="jobManageService" />
        <property name="queueWatchFacadeService" ref="queueWatchFacadeService" />
    </bean>


    <!-- Service実装 -->
    <!-- キューに関するサービスクラス DI用の設定です -->
    <bean id="queueService"
        class="com.globaldenso.ai.aijb.core.business.service.QueueServiceImpl">
        <property name="queueInfDao" ref="queueInfDao" />
        <property name="jobManageDao" ref="jobManageDao" />
        <property name="queueExecuteHistoryService" ref="queueExecuteHistoryService" />
    </bean>

    <!-- ジョブに関するサービスクラス DI用の設定です -->
    <bean id="jobManageService"
        class="com.globaldenso.ai.aijb.core.business.service.JobManageServiceImpl">
        <property name="jobManageDao" ref="jobManageDao" />
    </bean>

    <!-- 常駐プロセス起動時に自身のキューを作成するサービスクラス DI用の設定です -->
    <!-- QueueExecuteSchduleが生成したcontextよりgetbeanして実行しています -->
    <bean id="residensWakeUpService"
        class="com.globaldenso.ai.aijb.core.business.service.ResidensWakeUpServiceImpl">
        <property name="queueService" ref="queueService" />
        <property name="jobManageService" ref="jobManageService" />
        <property name="jobYmdCalculation" ref="jobYmdCalculation" />
    </bean>

    <!-- QueueExecuteProcMainが呼び出すFacadeクラスです。 -->
    <bean id="queueExecuteProcMainFacadeService"
        class="com.globaldenso.ai.aijb.core.business.service.QueueExecuteProcMainFacadeServiceImpl">
        <property name="queueService" ref="queueService" />
        <property name="jobManageService" ref="jobManageService" />
    </bean>

    <!-- キュー監視に関するFacadeクラスです。 -->
    <bean id="queueWatchFacadeService"
        class="com.globaldenso.ai.aijb.core.business.service.QueueWatchFacadeServiceImpl">
        <property name="queueService" ref="queueService" />
    </bean>

    <!-- QUEUE_EXECUTE_HISTORY操作用のService -->
    <bean id="queueExecuteHistoryService"
        class="com.globaldenso.ai.aijb.core.business.service.QueueExecuteHistoryServiceImpl">
        <property name="queueExecuteHistoryInfDao"
            ref="queueExecuteHistoryInfDao" />
        <property name="queueService" ref="queueService" />
    </bean>

    <!-- System Master Service -->
    <bean id="aijbSysManageService" class="com.globaldenso.ai.aijb.auto.business.service.AijbSysManageServiceImpl">
        <property name="aijbSysManageDao" ref="aijbSysManageDao" />
    </bean>

    <!-- DAO実装 -->
    <!-- ======================================================================================== -->
    <bean id="queueInfDao"
        class="com.globaldenso.ai.aijb.core.integration.dao.QueueInfDaoImpl">
        <property name="sqlMapClient">
            <ref bean="sqlMapClient" />
        </property>
    </bean>

    <bean id="jobManageDao"
        class="com.globaldenso.ai.aijb.core.integration.dao.JobManageDaoImpl">
        <property name="sqlMapClient">
            <ref bean="sqlMapClient" />
        </property>
    </bean>
    
    <bean id="queueExecuteHistoryInfDao"
        class="com.globaldenso.ai.aijb.core.integration.dao.QueueExecuteHistoryInfDaoImpl">
        <property name="sqlMapClient">
            <ref bean="sqlMapClient" />
        </property>
    </bean>

    <!-- System Master DAO -->
    <bean id="aijbSysManageDao" class="com.globaldenso.ai.aijb.auto.integration.AijbSysManageDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>

    <!-- Utility実装 -->
    <!-- ======================================================================================== -->
    <!-- JobYmdCalculation -->
    <bean id="jobYmdCalculation" class="com.globaldenso.ai.aijb.core.util.JobYmdCalculationImpl">
        <property name="aijbSysManageService" ref="aijbSysManageService" />
    </bean>

</beans>
