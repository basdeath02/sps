<?xml version="1.0" encoding="UTF-8"?>
<!--
 * [JP] applicationContext-aijb-job.xml (編集不可)
 * [JP]
 * [JP] AI-JB(Spring) スケジュール/リクエストジョブ用の基底設定ファイルです。
 * [JP]
 * [JP] スケジュールおよびリクエストジョブを実装する場合は、この設定ファイルを import
 * [JP] して各ジョブの設定ファイルを作成してください。
 * 
 * [EN] applicationContext-aijb-job.xml (Not editable)
 * [EN]
 * [EN] This is the base configuration file for the Schedule / Request job AI-JB (Spring).
 * [EN]
 * [EN] If you want to implement job scheduling and requests, 
 * [EN] please create a configuration file for each job to import this configuration file.
 * 
 * $ applicationContext-aijb-job.xml 6997 2014-02-21 05:58:59Z HIDETOSHI_NAKATANI@denso.co.jp $
 *
 * Copyright (c) 2011 DENSO IT SOLUTIONS. All rights reserved.
-->
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:aop="http://www.springframework.org/schema/aop"
    xmlns:tx="http://www.springframework.org/schema/tx"
    xsi:schemaLocation="
        http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd
        http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop-2.5.xsd
        http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-2.5.xsd">

    <!-- AI-JB setting -->
    <import resource="classpath:applicationContext-aijb-job.xml" />

    <!-- Start : Update Payment Status From JDE -->
    <bean id="BADM002ParentJob" class="com.globaldenso.ai.aijb.core.job.jobbase.SimpleParentJob" >
    </bean>
    
    <bean id="BADM002ChildJob" class="com.globaldenso.asia.sps.jobcontrol.PurgeDataProcessChildJob">
        <property name="purgeDataProcessFacadeService" ref="purgeDataProcessFacadeService" />
        
        <property name="childJobId">
            <value>1</value>
        </property>
        <property name="childJobName">
            <value>BADM002ChildJob01</value>
        </property>
    </bean>
    
    <bean id="parentExtend" parent="parent">
        <property name="parentJob" ref="BADM002ParentJob" />
        <property name="listChild">
            <list>
                <ref bean="BADM002ChildJob" />
            </list>
        </property>
    </bean>
    <!-- End : Update Payment Status From JDE -->
    
    <!-- Facade -->
        <bean id="purgeDataProcessFacadeService"
        class="com.globaldenso.asia.sps.business.service.PurgeDataProcessFacadeServiceImpl">
        <property name="companyDensoService" ref="companyDensoService" />
        <property name="purchaseOrderService" ref="purchaseOrderService" />
        <property name="commonService" ref="commonService" />
        <property name="deliveryOrderService" ref="deliveryOrderService" />
        <property name="asnService" ref="asnService" />
        <property name="invoiceService" ref="invoiceService" />
        <property name="fileManagementService" ref="fileManagementService" />
        
        <property name="spsTAsnService" ref="spsTAsnService" />
        <property name="spsTAsnRunnoService" ref="spsTAsnRunnoService" />
        <property name="spsTAsnDetailService" ref="spsTAsnDetailService" />
        <property name="spsTDoKanbanSeqService" ref="spsTDoKanbanSeqService" />
        <property name="spsTDoService" ref="spsTDoService" />
        <property name="spsTDoDetailService" ref="spsTDoDetailService" />
        <property name="spsTPoCoverPageService" ref="spsTPoCoverPageService" />
        <property name="spsTPoCoverPageDetailService" ref="spsTPoCoverPageDetailService" />
        <property name="spsTPoDueService" ref="spsTPoDueService" />
        <property name="spsTPoService" ref="spsTPoService" />
        <property name="spsTPoDetailService" ref="spsTPoDetailService" />
        <property name="spsTInvoiceService" ref="spsTInvoiceService" />
        <property name="spsTInvoiceDetailService" ref="spsTInvoiceDetailService" />
        <property name="spsTCnService" ref="spsTCnService" />
        <property name="spsTCnDetailService" ref="spsTCnDetailService" />
        <property name="spsTInvCoverpageRunnoService" ref="spsTInvCoverpageRunnoService" />
    </bean>
    
    <!-- Service -->
    <bean name="companyDensoService" class="com.globaldenso.asia.sps.business.service.CompanyDensoServiceImpl">
        <property name="companyDensoDao" ref="companyDensoDao" />
    </bean>
    <bean name="purchaseOrderService" class="com.globaldenso.asia.sps.business.service.PurchaseOrderServiceImpl">
        <property name="purchaseOrderDao" ref="purchaseOrderDao" />
    </bean>
    <bean name="commonService" class="com.globaldenso.asia.sps.business.service.CommonServiceImpl">
        <property name="commonDao" ref="commonDao" />
    </bean>
    <bean name="deliveryOrderService" class="com.globaldenso.asia.sps.business.service.DeliveryOrderServiceImpl">
        <property name="deliveryOrderDao" ref="deliveryOrderDao" />
    </bean>
    <bean name="asnService" class="com.globaldenso.asia.sps.business.service.AsnServiceImpl">
        <property name="asnDao" ref="asnDao" />
    </bean>
    <bean name="invoiceService" class="com.globaldenso.asia.sps.business.service.InvoiceServiceImpl">
        <property name="invoiceDao" ref="invoiceDao" />
    </bean>
    <bean id="fileManagementService" class="com.globaldenso.ai.library.filemanagerstream.business.service.FileManagementServiceImpl">
        <property name="fileManagerDaoForStream" ref="fileManagerDaoForStream"/>
    </bean>
    <bean id="spsTAsnService" class="com.globaldenso.asia.sps.auto.business.service.SpsTAsnServiceImpl" >
        <property name="spsTAsnDao" ref="spsTAsnDao" />
    </bean>
    <bean id="spsTAsnRunnoService" class="com.globaldenso.asia.sps.auto.business.service.SpsTAsnRunnoServiceImpl" >
        <property name="spsTAsnRunnoDao" ref="spsTAsnRunnoDao" />
    </bean>
    <bean id="spsTAsnDetailService" class="com.globaldenso.asia.sps.auto.business.service.SpsTAsnDetailServiceImpl" >
        <property name="spsTAsnDetailDao" ref="spsTAsnDetailDao" />
    </bean>
    <bean id="spsTDoKanbanSeqService" class="com.globaldenso.asia.sps.auto.business.service.SpsTDoKanbanSeqServiceImpl" >
        <property name="spsTDoKanbanSeqDao" ref="spsTDoKanbanSeqDao" />
    </bean>
    <bean id="spsTDoService" class="com.globaldenso.asia.sps.auto.business.service.SpsTDoServiceImpl" >
        <property name="spsTDoDao" ref="spsTDoDao" />
    </bean>
    <bean id="spsTDoDetailService" class="com.globaldenso.asia.sps.auto.business.service.SpsTDoDetailServiceImpl" >
        <property name="spsTDoDetailDao" ref="spsTDoDetailDao" />
    </bean>
    <bean id="spsTPoCoverPageService" class="com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageServiceImpl" >
        <property name="spsTPoCoverPageDao" ref="spsTPoCoverPageDao" />
    </bean>
    <bean id="spsTPoCoverPageDetailService" class="com.globaldenso.asia.sps.auto.business.service.SpsTPoCoverPageDetailServiceImpl" >
        <property name="spsTPoCoverPageDetailDao" ref="spsTPoCoverPageDetailDao" />
    </bean>
    <bean id="spsTPoDueService" class="com.globaldenso.asia.sps.auto.business.service.SpsTPoDueServiceImpl" >
        <property name="spsTPoDueDao" ref="spsTPoDueDao" />
    </bean>
    <bean id="spsTPoService" class="com.globaldenso.asia.sps.auto.business.service.SpsTPoServiceImpl" >
        <property name="spsTPoDao" ref="spsTPoDao" />
    </bean>
    <bean id="spsTPoDetailService" class="com.globaldenso.asia.sps.auto.business.service.SpsTPoDetailServiceImpl" >
        <property name="spsTPoDetailDao" ref="spsTPoDetailDao" />
    </bean>
    <bean id="spsTInvoiceService" class="com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceServiceImpl" >
        <property name="spsTInvoiceDao" ref="spsTInvoiceDao" />
    </bean>
    <bean id="spsTInvoiceDetailService" class="com.globaldenso.asia.sps.auto.business.service.SpsTInvoiceDetailServiceImpl" >
        <property name="spsTInvoiceDetailDao" ref="spsTInvoiceDetailDao" />
    </bean>
    <bean id="spsTCnService" class="com.globaldenso.asia.sps.auto.business.service.SpsTCnServiceImpl" >
        <property name="spsTCnDao" ref="spsTCnDao" />
    </bean>
    <bean id="spsTCnDetailService" class="com.globaldenso.asia.sps.auto.business.service.SpsTCnDetailServiceImpl" >
        <property name="spsTCnDetailDao" ref="spsTCnDetailDao" />
    </bean>
    <bean id="spsTInvCoverpageRunnoService" class="com.globaldenso.asia.sps.auto.business.service.SpsTInvCoverpageRunnoServiceImpl" >
        <property name="spsTInvCoverpageRunnoDao" ref="spsTInvCoverpageRunnoDao" />
    </bean>
    
    <!-- Dao -->
    <bean id="companyDensoDao" class="com.globaldenso.asia.sps.integration.CompanyDensoDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="purchaseOrderDao" class="com.globaldenso.asia.sps.integration.PurchaseOrderDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="commonDao" class="com.globaldenso.asia.sps.integration.CommonDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="deliveryOrderDao" class="com.globaldenso.asia.sps.integration.DeliveryOrderDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="asnDao" class="com.globaldenso.asia.sps.integration.AsnDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean id="invoiceDao" class="com.globaldenso.asia.sps.integration.InvoiceDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean id="fileManagerDaoForStream" class="com.globaldenso.ai.library.filemanagerstream.integration.FileManagementDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTAsnRunnoDao" class="com.globaldenso.asia.sps.auto.integration.SpsTAsnRunnoDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTAsnDetailDao" class="com.globaldenso.asia.sps.auto.integration.SpsTAsnDetailDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTDoKanbanSeqDao" class="com.globaldenso.asia.sps.auto.integration.SpsTDoKanbanSeqDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTDoDao" class="com.globaldenso.asia.sps.auto.integration.SpsTDoDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTDoDetailDao" class="com.globaldenso.asia.sps.auto.integration.SpsTDoDetailDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTPoCoverPageDao" class="com.globaldenso.asia.sps.auto.integration.SpsTPoCoverPageDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTPoCoverPageDetailDao" class="com.globaldenso.asia.sps.auto.integration.SpsTPoCoverPageDetailDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTPoDueDao" class="com.globaldenso.asia.sps.auto.integration.SpsTPoDueDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTPoDao" class="com.globaldenso.asia.sps.auto.integration.SpsTPoDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTPoDetailDao" class="com.globaldenso.asia.sps.auto.integration.SpsTPoDetailDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTInvoiceDao" class="com.globaldenso.asia.sps.auto.integration.SpsTInvoiceDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTInvoiceDetailDao" class="com.globaldenso.asia.sps.auto.integration.SpsTInvoiceDetailDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTCnDao" class="com.globaldenso.asia.sps.auto.integration.SpsTCnDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTCnDetailDao" class="com.globaldenso.asia.sps.auto.integration.SpsTCnDetailDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
    <bean name="spsTInvCoverpageRunnoDao" class="com.globaldenso.asia.sps.auto.integration.SpsTInvCoverpageRunnoDaoImpl">
        <property name="sqlMapClient" ref="sqlMapClient" />
    </bean>
</beans>